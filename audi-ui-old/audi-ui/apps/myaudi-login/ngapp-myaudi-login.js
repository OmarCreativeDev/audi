angular.module('myAudiLogin', ['ui.router', 'ngResource', 'ngAnimate', 'ngSanitize'])

    .config(function ($stateProvider, $urlRouterProvider) {

        //https://github.com/angular-ui/ui-router/wiki/Nested-States-%26-Nested-Views
        //https://github.com/angular-ui/ui-router/wiki/Multiple-Named-Views
        $stateProvider.state('account', {
            url: "/account",
            abstract: true,
            templateUrl: "/audi-ui/apps/myaudi-login/ng-views/wrapper.html",
        })
        .state('account.login', {
            url: "/login",
            templateUrl: "/audi-ui/apps/myaudi-login/ng-components/accountLogin/login.html",
            controller: "loginCtrl"
        })
        .state('account.login.verified', {
            url: "/verified/:uniqueKey",
            views: {
                'messageView@account': {
                    templateUrl: "/audi-ui/apps/myaudi-login/ng-components/accountLogin/messageArea.html",
                    controller: "verifiedCtrl"
                }
            }
        })
        .state('account.forgotpassword', {
            url: "/forgotpassword",
            templateUrl: "/audi-ui/apps/myaudi-login/ng-components/accountForgot/forgot.html",
            controller: "forgotCtrl"
        })
        .state('account.signup', {
            url: "/signup",
            templateUrl: "/audi-ui/apps/myaudi-login/ng-components/accountSignup/signup.html",
            controller: "signupCtrl"
        })
        .state('account.resetPassword', {
            url: "/resetPassword/:uniqueKey",
            templateUrl: "/audi-ui/apps/myaudi-login/ng-components/resetPassword/resetPassword.html",
            controller: "resetPasswordCtrl"
        })


        /* default app route */
        $urlRouterProvider.otherwise("/account/login");


    })

    .config(function ($provide) {
        $provide.decorator('$httpBackend', angular.mock.e2e.$httpBackendDecorator);
    })

    // Delay the mock ajax request to simulate the real thing..
    /* DEV ONLY */
    .config(function ($provide) {
        $provide.decorator('$httpBackend', function ($delegate) {
            var proxy = function (method, url, data, callback, headers) {
                var interceptor = function () {
                    var _this = this,
                        _arguments = arguments;
                    setTimeout(function () {
                        callback.apply(_this, _arguments);
                    }, 700);
                };
                return $delegate.call(this, method, url, data, interceptor, headers);
            };
            for (var key in $delegate) {
                proxy[key] = $delegate[key];
            }
            return proxy;
        });
    })

    .run(function ($httpBackend) {
        // MOCK DATA SERVICES
        // respond with mockdata on all request for /api/frontend/*

        // pass trough all GET requests to api/backend and so on, that havent been catched // in another $httpBackend.whenGET rule
        $httpBackend.whenGET().passThrough();
    })

    .config(function ($httpProvider) {
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    })

    .config(['$locationProvider',
        function ($locationProvider) {
            $locationProvider.hashPrefix("!");
        }
    ]);
