"use strict";

angular.module('myAudiLogin').controller('loginCtrl', ['$scope','$state','loginService', function($scope,$state,loginService){
      //..

	$scope.email="";
	$scope.password= "";
	$scope.userDetails = loginService.userDetails;
	$scope.loginInProgress = false;
	$scope.loginButton = "Log in to account";

	$scope.login=function(){

		$scope.loginInProgress= true;
		$scope.loginButton = "Logging in...";
		
		loginService.login($scope.email,$scope.password).then(function(){
			$scope.loginInProgress = false;
			$state.go('account.forgotpassword');
		},function(){
			console.log("OOPS LOGIN FAILED");
			$scope.loginButton = "Log in to account";
		});
	

		return false;

	}





}]);