angular.module('financeCalculator', ['ui.router', 'ngResource', 'ngAnimate','ngSanitize'])

.config(function ($stateProvider, $urlRouterProvider) {
 
//https://github.com/angular-ui/ui-router/wiki/Nested-States-%26-Nested-Views
//https://github.com/angular-ui/ui-router/wiki/Multiple-Named-Views
    $stateProvider.state('ranges', {
        url: "/selectvehicle",
        templateUrl: "/audi-ui/apps/finance-calculator/ng-views/selectVehicle/selectRange.html",
        controller: "selectModelCtrl"

    })
      .state('ranges.range', {
            url: "/:range",
             views:{
                '@' : {
                        templateUrl: "/audi-ui/apps/finance-calculator/ng-views/selectVehicle/selectModel.html",
                        controller: "selectModelCtrl"
                    }
                }
       })

      .state('caclulator', {
            url: "/caclulator?range&model&trim&engine&financetype&deposit&mileage&duration",
            templateUrl: "/audi-ui/apps/finance-calculator/ng-views/calculateTool/calculateTool.html",
            controller: "calculateToolCtrl"
            //,reloadOnSearch: false
       });
      

        
        /* default app route */
    $urlRouterProvider.otherwise("/selectvehicle");



})

.config(function($provide) {
  $provide.decorator('$httpBackend', angular.mock.e2e.$httpBackendDecorator);
})

// Delay the mock ajax request to simulate the real thing..
/* DEV ONLY */
.config(function($provide) {
    $provide.decorator('$httpBackend', function($delegate) {
        var proxy = function(method, url, data, callback, headers) {
            var interceptor = function() {
                var _this = this,
                    _arguments = arguments;
                setTimeout(function() {
                    callback.apply(_this, _arguments);
                }, 700);
            };
            return $delegate.call(this, method, url, data, interceptor, headers);
        };
        for(var key in $delegate) {
            proxy[key] = $delegate[key];
        }
        return proxy;
    });
})

.run(function($httpBackend) {



// MOCK DATA SERVICES




// respond with mockdata on all request for /api/frontend/* 


  // pass trough all GET requests to api/backend and so on, that havent been catched // in another $httpBackend.whenGET rule 
  $httpBackend.whenGET().passThrough(); 
})

.config(function ($httpProvider) {
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
})

.config(['$locationProvider',
    function($locationProvider) {
        $locationProvider.hashPrefix("!");
    }
]);



