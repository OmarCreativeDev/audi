'use strict';
function log(txt){
    console.log(txt);
}
// Declare app level module which depends on filters, and services
angular.module('audiApps', ['ngResource','ngCookies','ngAnimate']);

  angular.element(document).ready(function() {
      angular.bootstrap(document, ['audiApps']);
    });

angular.module('audiApps').filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});


angular.module('audiApps').controller('shortListCtrl', ['$scope','shortlistService', function($scope,shortlistService){
    $scope.init = function(){
       _stickyShortlist();
       $scope.spaces = [1,2,3,4];
     
     shortlistService.getModelsForShortlist().then(function(){
         
         $('.render-shortlistTarget').addClass('render-shortlist');

     
     
         $scope.shortlist = shortlistService.shortlist;
      });
    }

    $scope.modelSample = shortlistService.models; // temp
    $scope.init();
}]);



angular.module('audiApps').factory('shortlistService', ['$resource','$q','$cookieStore','$timeout', function($resource,$q,$cookieStore,$timeout){

    var shortlist= [] // the shortliust data array [{range:'A4',configCode:'a4avant'},{range:'A1',configCode:'a1sb'},{range:'A1',configCode:'a1'}]; 
    var models =[]; // model details
    var resourceShortlistAjax = $resource('json/shortlist/:userId/:isDelete/:range/:model',{range:'@range',model:'@model',isDelete:'@isDelete', userId: $cookieStore.get('JSESSIONID')})

    // get the models for displaying the info on the shortlist
    var getModelsForShortlist = function(){
      getCookie();

        var deferred = $q.defer();



        if(models.length>1){
             deferred.resolve();
        }else{
                 $.ajax({
                    url:'https://webservices.audi.co.uk.uat.nativ-systems.com/pdb-override/rest/v2/models',
                      // url:'json/models/index.json',
                      success: function(response) {
                          
                          angular.copy(response,models);
                          console.log('MODESL LOADED',models)
                          deferred.resolve();
                      },
                      error: function (respose) {
                          console.log("Model data not available from PDB. Try again");
                    
                      }
                  });

            
        }
        return deferred.promise;
    }
    
    var getCookie = function(){
      var re =[{range:"A4",configCode:"a4avant"}];
      // $cookieStore.put('JSESSIONID', 'rANDOM');
      if(typeof $cookieStore.get('audi_car_shortlist') ==="undefined"){
        $cookieStore.put('audi_car_shortlist', re);
      }else{
            re =  $cookieStore.get('audi_car_shortlist');
      }  
     
      angular.copy(re,shortlist);
      return re;
    }

    var saveCookie = function(){
      $cookieStore.put('audi_car_shortlist', shortlist);

    }

    var addModel = function(range,configCode){
        event.preventDefault();
        event.stopPropagation();
        if(inShortlist(configCode)){
            removeModel(configCode);
        }
      shortlist.push({range:range,configCode: configCode});
      saveCookie();
         resourceShortlistAjax.get({range:range, model: configCode});
         
         $('#vehicleShortlist').microfiche({
            refresh: true
        });
         
         

        if(!$('.carShortlist .vehicleCarouselHolder').is(':visible')){
            $('.shotlistBtn').trigger('click');
        }  
    }

    var removeModel = function(range,configCode){
        event.preventDefault();
        event.stopPropagation();
        var index = -1;
            angular.forEach(shortlist,function(modelItem,ind){
                   if(modelItem.configCode == configCode){
                    index = ind;
                   }
            });
            if (index > -1) {
                shortlist.splice(index, 1);
            }
              resourceShortlistAjax.get({range:range, model: configCode,isDelete:'delete'});

        saveCookie();
        
    }

    var getModel = function(configCode){
        
      var returnModel = false;
       angular.forEach(models,function(model){
                  if(model.configCode == configCode){
                          returnModel=  model;
                  }
              });
       return returnModel;
    }

    var inShortlist= function(configCode){
        var returnModel = false;
        angular.forEach(shortlist,function(model){
          if(model.configCode == configCode){
                  returnModel=  true;;
          }
        });
       return returnModel;
     }

            
    return {
            getModelsForShortlist:getModelsForShortlist,
            getModel:getModel,
            addModel:addModel,
            removeModel: removeModel,
            models:models,// temp
            inShortlist:inShortlist,
            shortlist:shortlist
    }
  

}]);



angular.module('audiApps').directive('renderShortlist', function(shortlistService) {
    return {
      restrict: 'C',
      scope:{
        configcode : '@configcode',
        range:'@range'
      },
      controller:function($scope){
         $scope.model = shortlistService.getModel($scope.configcode);
      },
      template:'<li >'+
       '<a href="#" class="infoPanel" >'+
        '<div class="itemTitle">{{model.name}}</div>'+
      '<div class="fromPrice">Starting from &pound;{{model.p11dPriceMin}}</div>'+
       '<img ng-src="{{model.cdnImageSidePath}}" data-thumbnailhover="{{model.cdnImageFrontPath}}" width="124" height="50">'+
        '</a>'            +
       '<render-shortlist-button range="{{range}}" configcode="{{configcode}}" />' +
      '</li>'
       ,replace: true
    };
  });




angular.module('audiApps').directive('renderShortlistButton', function(shortlistService) {
    return {
      restrict: 'E',
      scope:{
        configcode : '@configcode',
        range:'@range'
      },
      controller:function($scope){

         $scope.removeModel = shortlistService.removeModel;
         $scope.addModel = shortlistService.addModel;
         $scope.inShortlist = shortlistService.inShortlist;
 
      },
      template:'<div class="shortListCnt" ng-class="{remove: inShortlist(configcode)}">'+ 
         '<a ng-if="inShortlist(configcode)" href="#"  ng-click="removeModel(range,configcode)">' +
           '<span class="btn"><i></i></span>'+
          '<span>Remove from my shortlist</span>'+
        '</a>'+
       '<a ng-if="!inShortlist(configcode)" href="#"  ng-click="addModel(range,configcode)">' +
          '<span class="btn"><i></i></span>'+
           '<span>Add to my shortlist</span>'+
        '</a>'+
       '</div>'
       ,replace: true
   

    };
  });


































 var _stickyShortlist = function () {

        var $shortlistWrapper = $(".shortListWrapper"),
            $shortListButton = $(".shortListWrapper a.shotlistBtn"),
            $shortListCarousel = $(".shortListWrapper .vehicleCarouselHolder");
        var lastScrollPos = 0;

        $shortlistWrapper.addClass('fixFoot');                                                            
        $shortListCarousel.hide();                                                
                
        // events     
        $shortListButton.click(function(){
            $shortListCarousel.slideToggle();
            $(this).toggleClass('up');
        });

        $(window).scroll(function(){

            var docHeight =     $(document).outerHeight(),
            winHeight =     $(window).outerHeight(),
            btnHeight =     $shortListButton.outerHeight(true), 
            footerHeight =  $shortlistWrapper.outerHeight(true),
            footerTop =     docHeight - footerHeight,
            scrollPos =     $(window).scrollTop();
            
          //  console.log((scrollPos + winHeight), footerTop);

            if ( scrollPos + winHeight < footerTop && lastScrollPos > scrollPos) {
                // Window hasn't reached footer
                if ($shortListCarousel.is(':visible')) {                            // Hide content if scrolling
                    if ($shortlistWrapper.hasClass('fixFoot')) {   
                 //           log('slideup');
                            $shortListCarousel.slideUp('fast');
                        }
                }                                       
                if (!$shortlistWrapper.hasClass('fixFoot')) {                     // fix shortlist to window
                    $shortlistWrapper.addClass('fixFoot');
                    $shortListCarousel.hide();
                }
            } else if ( scrollPos + winHeight >= footerTop && lastScrollPos < scrollPos){
                $shortlistWrapper.removeClass('fixFoot');
                $shortListCarousel.show();
            } 

            lastScrollPos = scrollPos;
        });


        /* This code is a bit temperamental. Needs updaing to achive the desired functionality */
        var mousewheelevt=(/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x
 
        if (document.attachEvent) //if IE (and Opera depending on user setting)
            document.attachEvent("on"+mousewheelevt, mouseScroll);
        else if (document.addEventListener) //WC3 browsers
            document.addEventListener(mousewheelevt, mouseScroll, false)    

        var mouseScroll = function (e) {
            var evt=window.event || e ;
            var delta=evt.detail? evt.detail*(-120) : evt.wheelDelta;

            if(lastScrollPos == $(window).scrollTop() && delta == -360) {
                if( $(document).outerHeight() > $(window).outerHeight() + $(window).scrollTop() )
                    $("html, body").animate({scrollTop: $(window).scrollTop()+1});
            }
        }
    
    };
        

