"use strict";

angular.module('financeCalculator').controller('selectModelCtrl', ['$scope','$state','$resource', function($scope,$state,$resource){
      //..


 var Ranges = $resource('json/ranges/');

 $scope.rangeData = Ranges.query(function() {});


}]);




angular.module('financeCalculator').filter('partition', function($cacheFactory) {
  var arrayCache = $cacheFactory('partition')
  return function(arr, size) {
   try{

    var parts = [], cachedParts,
      jsonArr = JSON.stringify(arr);
    for (var i=0; i < arr.length; i += size) {
        parts.push(arr.slice(i, i + size));
    }
    cachedParts = arrayCache.get(jsonArr);
    if (JSON.stringify(cachedParts) === JSON.stringify(parts)) {
      return cachedParts;
    }
    arrayCache.put(jsonArr, parts);

}catch(e){
  parts = arr;
}
    return parts;

  };
});