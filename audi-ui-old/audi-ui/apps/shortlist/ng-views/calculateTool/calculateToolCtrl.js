"use strict";

angular.module('financeCalculator').controller('calculateToolCtrl', ['$scope','$rootScope','$state','calculatorService','$timeout', function($scope,$rootScope,$state,calculatorService,$timeout){
      //..

	$scope.params=$state.params;
	

$scope.settings = {
	mileage : [5000,10000,15000,25000,30000],
	duration: [18,24,30,36,42,48]
};


    $scope.trimLines = [
    				{
					id:12, 
    				name:"Black edition",
    				engines:[
    					{id:1, name:"1.2 Tdi"},
						{id:6, name:"3.0 TDI Quatro"}
						]
					},
					{
					id:4,
					name:"S-Line",
					active:true,
					engines:[
							{id:1, name:"1.2 Tdi"},
							{id:6, name:"3.0 TDI Quatro"}
							]
						}];

	$scope.calcDisplayLoading = true;
	
	$scope.financeData = calculatorService.financeData;

	$scope.trim = {};
	$scope.engine = {};
	$scope.deposit = $state.params.deposit;



 $scope.init = function() {
	console.log('init')

	// set some defaults
	var sp = $state.params;
	var t = {};
	if(sp.financetype==null){t['financetype'] = 'finance';}
	if(sp.deposit==null){t['deposit'] = '4000';}

	if(!isEmpty(t)){
		$state.go('caclulator',t,{location:'replace',notify:false});
	}



	// auto select dropdowns
	angular.forEach($scope.trimLines ,function(trimLine){

		if(trimLine.id == $state.params.trim){
			console.log(trimLine)
			$scope.trim = trimLine;
		}
			angular.forEach(trimLine.engines ,function(engine){
					if(engine.id == $state.params.engine){
						console.log('engine',engine)
							$scope.engine = engine;
						}
			});
	});

		$scope.getData();

}

	$scope.getData = function(){
	
		$scope.calcDisplayLoading = true;
			calculatorService.getData($scope.params).then(function(){
			$scope.calcDisplayLoading = false;
		});
	}

	$scope.formChange = function(field,value){
		var t = {};
		t[field]= value;

		$state.go('caclulator',t,{location:'replace',notify:false});
		$scope.params[field]= value;
		console.log('formChange',t)

			$scope.getData();
	
	}



   $scope.getClass = function(what,value) {
        //if ($location.path().substr(0, path.length) == path) {
          if($state.params[what]==value){
          return "active"
        } else {
          return ""
        }
    }



$scope.init();


}]);


function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}




angular.module('financeCalculator').directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});


String.prototype.splice = function(idx, rem, s) {
    return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
};

angular.module('financeCalculator').directive('currencyInput', function() {
    return {
        restrict: 'A',
        scope: {
            field: '='
        },
        replace: true,
        template: '<span><input type="text" ng-model="field"></input>{{field}}</span>',
        link: function(scope, element, attrs) {

            $(element).bind('keyup', function(e) {
                var input = element.find('input');
                var inputVal = input.val();

                //clearing left side zeros
                while (scope.field.charAt(0) == '0') {
                    scope.field = scope.field.substr(1);
                }

                scope.field = scope.field.replace(/[^\d.\',']/g, '');

                var point = scope.field.indexOf(".");
                if (point >= 0) {
                    scope.field = scope.field.slice(0, point + 3);
                }

                var decimalSplit = scope.field.split(".");
                var intPart = decimalSplit[0];
                var decPart = decimalSplit[1];

                intPart = intPart.replace(/[^\d]/g, '');
                if (intPart.length > 3) {
                    var intDiv = Math.floor(intPart.length / 3);
                    while (intDiv > 0) {
                        var lastComma = intPart.indexOf(",");
                        if (lastComma < 0) {
                            lastComma = intPart.length;
                        }

                        if (lastComma - 3 > 0) {
                            intPart = intPart.splice(lastComma - 3, 0, ",");
                        }
                        intDiv--;
                    }
                }

                if (decPart === undefined) {
                    decPart = "";
                }
                else {
                    decPart = "." + decPart;
                }
                var res = intPart + decPart;

                scope.$apply(function() {scope.field = res});

            });

        }
    };
});