"use strict";

angular.module('financeCalculator').factory('calculatorService', ['$resource', '$http', '$q','$httpBackend', function($resource, $http, $q,$httpBackend){
     	



   var financeData = {};

     	var getData = function(data){
     			console.log('getData')
                        var deferred = $q.defer();
                            $http({
                                method: 'POST',
                                url: "api/calcData",
                                data:data
                            })
                            .success(function (result, status) {
                               console.log('LOGIN DONE', result);
                             angular.copy(result.stats,financeData);
                                
                                deferred.resolve();
                            }).
                        error(function (data, status) {
                                console.log('error with login', result);
                            deferred.reject();
                        });

                        return deferred.promise;

     	}



 var setupMocks = function(){

    $httpBackend.whenPOST("api/calcData").respond(function(method, url, data) {
       data =  angular.fromJson(data);
        console.log('mock',data)
        return [200, {
        "status": "OK",
        "stats": {
            deposit: data.deposit,
            mileage: data.mileage,
            duration: data.duration
            }
        }, {}];
    });

 }
 setupMocks();

     	return {
            financeData: financeData,
     		getData : getData


     	}

 }]);


