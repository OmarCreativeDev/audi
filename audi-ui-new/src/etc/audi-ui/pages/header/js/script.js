$(function(){

	//Stop event propagating to children
	$(document).on('click', '.yamm .dropdown-menu', function(e) {
		e.stopPropagation();
	});

	$(document).on('click', '.search-submit', function(e) {
		e.stopPropagation();
	});

	$('.header-bottom-wrapper').on('show.bs.dropdown', function () {
	 	var headerHeight = $(".header-top-wrapper").outerHeight();
		var dimDiv = $(".menuDim");
		var viewportWidth = $(document).width();
		var viewportHeight = $(document).height();

	 	dimDiv.toggleClass("dim-on");
		dimDiv.css({ top: headerHeight, height: viewportHeight, width: viewportWidth });
	});

	$('.header-bottom-wrapper').on('hide.bs.dropdown', function () {
		var dimDiv = $(".menuDim");
		dimDiv.toggleClass("dim-on");
	});

	//Search transitions
	$(document).on('click', '.search-field', function() {
		var headerTop = $(".header-top");
		var headerTopLinks = $(".header-top li:not(.search)");
		headerTop.addClass("search-transition");
		setTimeout(function(){
			headerTopLinks.css({ position: 'absolute', left: '-99999px' });
		}, 100)
	});

	$(document).on('focusout', '.search-field', function() {
		var headerTop = $(".header-top");
		var headerTopLinks = $(".header-top li:not(.search)");
		setTimeout(function(){
			headerTopLinks.css({ position: 'relative', left: '0px' });
		}, 100)
		if(headerTop.hasClass("search-transition")) {
			headerTop.removeClass("search-transition");
		}
	});

});

	
