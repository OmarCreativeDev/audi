$(function() {
	// Page load tagging
	salmon.globalTagging.getTrackingProperties('load', null, 'o', 'Page load', {
		props: {
			prop1: '/new-cars/tt/audi-tt-the-third.html' // Dynamic - window.location.href?
		},
		eVars: {

		},
		events: {

		},
		other: {
			pageName: 'Audi TT the third' // Check if analytics team are OK with this being static - document.title?
		}
	}); // Closes salmon.globalTagging.getTrackingProperties

	$(".top-car-catchphrase img").click(function() {
		salmon.globalTagging.getTrackingProperties('click', 'action', 'o', 'Click on "Scroll for more"', {
			props: {
				prop1: "/new-cars/tt/audi-tt-the-third.html/icon/design"
			},
			eVars: {

			},
			events: {

			},
			other: {
				
			}
		}); // Closes salmon.globalTagging.getTrackingProperties
	}); // Closes $(".top-car-catchphrase img").click(function() {})

	$(".sticky-nav-ul-tt a").click(function() {
		salmon.globalTagging.getTrackingProperties('click', 'action', 'o', 'Click on stickynav\'s TT', {
			props: {
				prop1: "/new-cars/tt/audi-tt-the-third.html/tt"
			},
			eVars: {

			},
			events: {

			},
			other: {
				
			}
		}); // Closes salmon.globalTagging.getTrackingProperties
	}); // Closes $(".sticky-nav-ul-tt").click(function() {})

	$(".sticky-nav-ul-design a").click(function() {
		salmon.globalTagging.getTrackingProperties('click', 'action', 'o', 'Click on stickynav\'s Design', {
			props: {
				prop1: "/new-cars/tt/audi-tt-the-third.html/design"
			},
			eVars: {

			},
			events: {

			},
			other: {
				
			}
		}); // Closes salmon.globalTagging.getTrackingProperties
	}); // Closes $(".sticky-nav-ul-design").click(function() {})

	$(".sticky-nav-ul-technology a").click(function() {
		salmon.globalTagging.getTrackingProperties('click', 'action', 'o', 'Click on stickynav\'s Technology', {
			props: {
				prop1: "/new-cars/tt/audi-tt-the-third.html/technology"
			},
			eVars: {

			},
			events: {

			},
			other: {
				
			}
		}); // Closes salmon.globalTagging.getTrackingProperties
	}); // Closes $(".sticky-nav-ul-technology").click(function() {})

	$(".sticky-nav-ul-drive a").click(function() {
		salmon.globalTagging.getTrackingProperties('click', 'action', 'o', 'Click on stickynav\'s Drive', {
			props: {
				prop1: "/new-cars/tt/audi-tt-the-third.html/drive"
			},
			eVars: {

			},
			events: {

			},
			other: {
				
			}
		}); // Closes salmon.globalTagging.getTrackingProperties
	}); // Closes $(".sticky-nav-ul-drive").click(function() {})

	$(".section4-brochure").click(function() {
		var position = $(".section4-brochure").attr("title").search(": Opens");
		var eVar37Value = $(".section4-brochure").attr("title").substring(0,position);
		salmon.globalTagging.getTrackingProperties('click', 'action', 'o', 'Click on section4\'s "Download the brochure"', {
			props: {

			},
			eVars: {
				eVar37: eVar37Value
			},
			events: {
				event4: null
			},
			other: {
				
			}
		}); // Closes salmon.globalTagging.getTrackingProperties
	}); // Closes $(".section4-brochure a").click(function() {})

	$(".mio-big-video").click(function() { // Doesn't work because it's an iframe. What to do?
		salmon.globalTagging.getTrackingProperties('click', 'action', 'o', 'Click on section3\'s big video\'s play button', {
			props: {
				prop2: "Play was pressed"
			},
			eVars: {

			},
			events: {

			},
			other: {
				
			}
		}); // Closes salmon.globalTagging.getTrackingProperties
	}); // Closes $(".mio-big-video").click(function() {})
}); // Closes $(function() {})