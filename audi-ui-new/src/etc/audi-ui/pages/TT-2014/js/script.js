var audiTT = (function () {

	var prevScrollY =  window.pageYOffset;
	var scrollYdirection = "none";
	var isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));

	var init = function(){
		setupJqueryBrowserObj();
		audiLoader();
		switchSectionClassesToIDs();
		pageResizeSetup();
		setUpLogo();
		setupStickyNav();
		setupSmoothScrolling();
	    startPageLoadTransitions();
		handleScroll();

		if(isTouch) {
			$('body').on({
			    'touchmove': function(e) {
			        handleScroll();
			    }
			});
		}
		else {
			$(window).scroll(function () {
				handleScroll();
			});
		}
	
		window.onresize = function() {
			pageResizeSetup();
		}

		// href injection for links which include illegal characters in the "eyes" of CQ5
		$(".section4-dream-tt").attr("href", "http://configurator.audi.co.uk/entry?mandant=accx-uk&pr=FV30TC\\2\\MRADCJ3\\MSIBN7K\\GPX2PX2\\GWQRWQR\\MWSS4GH&next=model-page");
		$(".test-drive-a-button").attr("href", "https://www.audi.co.uk/about-audi/contact-us/arrange-a-test-drive/test-drive-form.html?range=TT&model=TT-Coupe");
	}

	var setupJqueryBrowserObj =function() {
		jQuery.uaMatch = function( ua ) {
		    ua = ua.toLowerCase();
		    var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
		        /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
		        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
		        /(msie) ([\w.]+)/.exec( ua ) ||
		        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) || [];
		    return {
		        browser: match[ 1 ] || "",
		        version: match[ 2 ] || "0"
		    };
		};
		if ( !jQuery.browser ) {
		    var 
		    matched = jQuery.uaMatch( navigator.userAgent ),
		    browser = {};
		    if ( matched.browser ) {
		        browser[ matched.browser ] = true;
		        browser.version = matched.version;
		    }
		    // Chrome is Webkit, but Webkit is also Safari.
		    if ( browser.chrome ) {
		        browser.webkit = true;
		    } else if ( browser.webkit ) {
		        browser.safari = true;
		    }
		    jQuery.browser = browser;
		}
	}

	var switchSectionClassesToIDs = function() {
		$(".section1").attr("id", "section1");
		$(".section1").removeClass("section1");

		$(".design").attr("id", "design");
		$(".design").removeClass("design");

		$(".technology").attr("id", "technology");
		$(".technology").removeClass("technology");

		$(".drive").attr("id", "drive");
		$(".drive").removeClass("drive");
	}

	// Page setup which occurs on window.resize; Deals with positioning various elements and setting measurements.
	var pageResizeSetup = function() {
		var section1DesignatedPaddingTop = window.getHeight() - $(".sticky-nav-wrapper").outerHeight() - $("#navigation").outerHeight(true);
		$("#section1").css("paddingTop", section1DesignatedPaddingTop);
		resizeSection1Images();
	}

	var resizeSection1Images = function() {
		// Minimum resolution to display top-car and top-car-catchphrase images at full size.
		var minResForFullSizeImgs = [1235,860];

		// If viewport width is smaller than the minRes width described above,
		// or the viewport hight is smaller than the misRes height:
		// - Work out and apply the lowest ratio (viewport width / min width OR viewport height / min height)
		// to both images.
		// - If ratio*105 is smaller than 100 , apply it as a percentage to the height of top-car.
		// - Otherwise, apply 100% to it.
		if(window.getWidth() < minResForFullSizeImgs[0] || window.getHeight() < minResForFullSizeImgs[1]) {
			// console.log("Setting rules...");
			var lowestRatio = 1; // init, just in case.
			if(window.getWidth() / minResForFullSizeImgs[0] < window.getHeight() / minResForFullSizeImgs[1]) {
				lowestRatio = window.getWidth() / minResForFullSizeImgs[0];
			}
			else {
				lowestRatio = window.getHeight() / minResForFullSizeImgs[1];
			}

			if(lowestRatio < 0.5) {
				lowestRatio = 0.5;
			}			

			if(!$("#section1").hasClass("faded-in")) {
				lowestRatio -= 0.2;
			}
			$(".top-car").css("height", (lowestRatio*105 < 100 ? lowestRatio*105 : 100) + "%");
			$(".top-car img").css("transform", "scale(" + lowestRatio + ")");
			$(".top-car-catchphrase img").css("transform", "scale(" + lowestRatio + ")");
		}
		else {
			// Otherwise, remove any CSS rules that were potentially set.
			// console.log("Removing rules...");
			$(".top-car").css("height", "");
			$(".top-car img").css("transform", "");
			$(".top-car-catchphrase img").css("transform", "");
		}
	}

	var audiLoader = function() {
		//var spriteElement = document.getElementsByClassName("audi-loader")[0];
		var spriteElement = $(".audi-loader").eq(0);
		var frameWidth = 80;
		var frameHeight = 28;
		var spriteWidth = 3360;
		var spriteHeight = 28;
		var backgroundOffset = 0;
		var timing = setInterval(function() {
			if(backgroundOffset >= 3360) {	
				backgroundOffset = 0; 
				spriteElement.css('background-position', '0px 0px'); 
			}
			else {
			// console.log("backgroundOffset", backgroundOffset);
			// console.log("background-position", spriteElement.css('background-position'));
				backgroundOffset = backgroundOffset + frameWidth;
				spriteElement.css('background-position', backgroundOffset+'px 0px');
			}
		}, 60);
	}

	var setUpLogo = function() {
		if(isHighDensity()) {
			$("#logo-audi img").attr("src", "/etc/audi-ui/pages/TT-2014/images/header/logo-audi-darkbg-2x.png");
		}
		else {
			$("#logo-audi img").attr("src", "/etc/audi-ui/pages/TT-2014/images/header/logo-audi-darkbg.png");
		}
		$("#logo-audi img").attr({
			width: 267,
			height: 33
		});
	}

	// Choose the desired function out of the following two, and delete the other one.
	var isHighDensity= function() {
	    return ((window.matchMedia && (window.matchMedia('only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)').matches)) || (window.devicePixelRatio && window.devicePixelRatio > 1.3));
	}


	var isRetina = function() {
	    return ((window.matchMedia && (window.matchMedia('only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx), only screen and (min-resolution: 75.6dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min--moz-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)').matches)) || (window.devicePixelRatio && window.devicePixelRatio > 2)) && /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
	}

	// Sets the height for .sticky-nav-wrapper and initialises affix.js and scrollspy.js.
	var setupStickyNav = function() {
		$(".sticky-nav-wrapper").height($(".sticky-nav").outerHeight());
		$(".sticky-nav").affix({
		    offset: {
		        top: $(".sticky-nav").offset().top
		    }
	    });

	    $('body').scrollspy({ target: '.sticky-nav' });
	}

	// Sets up the smooth (animated) scrolling when clicking on anchor links.
	var setupSmoothScrolling = function() {
		$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        //return false; // This has been removed to allow tagging on click on sticknav li.
		      }
		    }
		  });
		});
	}

	// Starts the one-time transitions which occur at page-load.
	var startPageLoadTransitions = function() {
		resizeSection1Images();
		// Start page-load transition
		var timeoutVal = 3000;
		if( ($.browser.msie && $.browser.version < 10) || !isInView($("#section1"))) {
			timeoutVal = 0;
		}
		// console.log("timeoutVal: " + timeoutVal);
		setTimeout(function() {
			$(".audi-loader").eq(0).remove();
			$(".page-load-trans").each(function() {
				$(this).addClass("faded-in");
			});
			$("#navigation").addClass("faded-in");
			resizeSection1Images();
		}, timeoutVal);
	}

	// This function gets called on every scroll.
	var handleScroll = function() {
		determineScrollYdirection();
		configSection1ImgOpacity();
		if( (!$.browser.msie && !isTouch) || $.browser.version > 9) {
			startTransitions();
			initTransitions();
		}
	}

	// Determines scroll direction by comparing previous and current window.pageYOffset.
	var determineScrollYdirection = function() {
		if(window.pageYOffset > prevScrollY) {
			scrollYdirection = "down";
		}
		else {
			if(window.pageYOffset < prevScrollY) {
				scrollYdirection = "up";
			}
			else {
				scrollYdirection = "none";
			}
		}
		// console.log(scrollYdirection + " | window.pageYOffset: " + window.pageYOffset + " | prevScrollY: " + prevScrollY);
		prevScrollY = window.pageYOffset;
	}

	var configSection1ImgOpacity = function() {
		if(isInView($("#section1"))) {
			var docViewTop = $(window).scrollTop();
		    var docViewBottom = docViewTop + $(window).height();

		    var section1 = $("#section1");
		    var section1Top = section1.offset().top;
		    var section1Bottom = section1Top + section1.outerHeight();

			var diff = section1Bottom - docViewTop;
			var opac = diff / section1Bottom;
			// console.log("diff: " + diff + " | opac: " + opac);

			$(".top-car").css("opacity", opac >= 0.05 ? opac : 0.05);
			$(".top-car-catchphrase").css("opacity", opac >= 0.05 ? opac : 0.05);
		}
	}

	// Initialises transitions for re-use.
	var initTransitions = function() {
		//console.log("transitions initiating...");

		$(".img-fade-and-text-left-trans").each(function() {
			// If element doesn't have the transitioned-out class AND its section isn't in view,
			// add class "transitioned-out" to it and class "transitioned-out" to the images within it.
			if(!$(this).hasClass("transitioned-out") && ( !isInView($(this).parents(".audi-section")) ) || ( $(this).parents("#drive").length && isInView($("#footer1")) ) ) {
				//console.log("left trans from " + $(this).parents(".audi-section").attr("id") + ". key: " + key + ". No transitioned-out class: " + !$(this).hasClass("transitioned-out") + ". section not in view: " + !isInView($(this).parents(".audi-section")));
				$(this).addClass("transitioned-out");
				$(this).find('img').addClass("transitioned-out");
			}
		});

		$(".img-fade-and-text-up-trans").each(function() {
			if(!$(this).hasClass("transitioned-out") && !isInView($(this).parents(".audi-section"))) {
				//console.log("up trans from " + $(this).parents(".audi-section").attr("id") + ". key: " + key + ". No transitioned-out class: " + !$(this).hasClass("transitioned-out") + ". section not in view: " + !isInView($(this).parents(".audi-section")));
				$(this).addClass("transitioned-out");
			}
		});

		$(".h1-span-and-small-up-trans").each(function() {
			//if(!$(this).hasClass("transitioned-out") && !isInView($(this).parents(".audi-section"))) {
			if(!$(this).hasClass("transitioned-out") && ( !isInView($(this).parents(".audi-section")) ) || ( $(this).parents("#drive").length && isInView($("#footer1")) ) ) {
				//console.log("up trans from " + $(this).parents(".audi-section").attr("id") + ". key: " + key + ". No transitioned-out class: " + !$(this).hasClass("transitioned-out") + ". section not in view: " + !isInView($(this).parents(".audi-section")));
				$(this).addClass("transitioned-out");
			}
		});

		$(".h2-h3-and-p-up-trans").each(function() {
			if(!$(this).hasClass("transitioned-out") && !isInView($(this).parents(".audi-section"))) {
				//console.log("up trans from " + $(this).parents(".audi-section").attr("id") + ". key: " + key + ". No transitioned-out class: " + !$(this).hasClass("transitioned-out") + ". section not in view: " + !isInView($(this).parents(".audi-section")));
				$(this).find('h2, h3, p').addClass("transitioned-out");
			}
		});

		$(".img-fade-and-right-trans").each(function() {
			if(!$(this).hasClass("transitioned-out") && ( !isInView($(this)) ) ) {
				//console.log("up trans from " + $(this).parents(".audi-section").attr("id") + ". key: " + key + ". No transitioned-out class: " + !$(this).hasClass("transitioned-out") + ". section not in view: " + !isInView($(this).parents(".audi-section")));
				$(this).addClass("transitioned-out");
				//$("img.tt-reveal-outline").removeClass("tt-outline-hidden");
			}
		});
	}

	// Starts the transitions if it is their time to start.
	var startTransitions = function() {
		var extraOffest = 0;
		var part;
		switch(scrollYdirection) {
			case "down": part = "bottom";
				break;
			case "up": part = "top";
				break;
			default: part = "undefined";
		}
		extraOffest = -20;
		$(".img-fade-and-text-left-trans").each(function() {
			if($(this).hasClass("transitioned-out") && isInView($(this), extraOffest, part)) {
				//console.log("transition started by *" + part + "*");
				$(this).removeClass("transitioned-out");
				$(this).find('img').removeClass("transitioned-out");
			}
		});

		extraOffest = 10;
		$(".img-fade-and-text-up-trans").each(function() {
			if($(this).hasClass("transitioned-out") && isInView($(this), extraOffest, part)) {
				//console.log("transitioned-out fired");
				// console.log("transition started by *" + part + "*");
				$(this).removeClass("transitioned-out");
				//console.log("extraOffest = " + extraOffest);
			}
		});

		extraOffest = 80;
		$(".h1-span-and-small-up-trans").each(function() {
			if($(this).hasClass("transitioned-out") && isInView($(this), extraOffest, part)) {
				//console.log("transitioned-out fired");
				//console.log("transition started by *" + part + "*");
				$(this).removeClass("transitioned-out");
				//console.log("extraOffest = " + extraOffest);
			}
		});

		if(scrollYdirection == "up") {
			extraOffest = 60;
		}
		else {
			extraOffest = 0;
		}
		
		$(".h2-h3-and-p-up-trans").each(function() {
			$surroundingTransClass = $(this);
			$(this).find('h2, h3, p').each(function(index) {
				if($(this).hasClass("transitioned-out") && isInView($surroundingTransClass, extraOffest, part)) {
					var $that = $(this);
					setTimeout(function() {
						$that.removeClass("transitioned-out");
					}, 80*index);
				}
			});
		});

		extraOffest = -60;
		$(".img-fade-and-right-trans").each(function() {
			if($(this).hasClass("transitioned-out") && isInView($(this), extraOffest, part)) {
				// console.log("transitioned-out fired");
				// console.log("transition started by *" + part + "*");
				$(this).removeClass("transitioned-out");
				// console.log("extraOffest = " + extraOffest);
			}
		});
	}

	// Checks if element+extraOffset is either in view or the view has scrolled past it. Returns true if either
	// conditions are met.
	var isInOrAboveView = function(elem, extraOffest) {
		extraOffest = (typeof extraOffest == "undefined" ? 0 : extraOffest);
	    var docViewTop = $(window).scrollTop();
	    var docViewBottom = docViewTop + $(window).height();

	    var elemTop = elem.offset().top + extraOffest;

	    return elemTop <= docViewBottom;
	}

	// This function checks if the elements passed as an argument is in the view.
	// i've redefined this function to:
	// - work for every possible scenario of any part of the element showing in the viewport (even if element is
	// vertically bigger than the viewport).
	// - support extraOffset, in case it is required to check if a certain offset from the element's position is
	// in view.
	// - support checking if a specific part of the element is in view: top/bottom, by supplying the relevant
	// argument. If the "extraOffset" and "part" arguments were supplied and "part" is "top", the extraOffset is
	// made negative. This is done in order to automate the direction of the offset according to the scroll
	// direction.
	var isInView = function(elem, extraOffest, part) {
		extraOffest = (typeof extraOffest == "undefined" ? 0 : extraOffest);
		part = (typeof part == "undefined" ? "undefined" : part);
		if(extraOffest != 0 && part == "top") {
			extraOffest = extraOffest * -1;
		}
	    var docViewTop = $(window).scrollTop();
	    var docViewBottom = docViewTop + $(window).height();

	    var elemTop = elem.offset().top + extraOffest;
	    var elemBottom = elemTop + $(elem).outerHeight();

	    //console.log("elemTop = " + Math.round(elemTop) + " | docViewTop = " + Math.round(docViewTop) + " | elemBottom = " + Math.round(elemBottom) + " | docViewBottom = " + Math.round(docViewBottom));
	    switch(part) {
	    	case "top": return (elemTop >= docViewTop) && (elemTop <= docViewBottom);
	    		break;
	    	case "bottom": return (elemBottom >= docViewTop) && (elemBottom <= docViewBottom);
	    		break;
	    	default: return ((elemTop >= docViewTop) && (elemTop <= docViewBottom)) || ((elemBottom >= docViewTop) && (elemBottom <= docViewBottom)) || ((elemTop <= docViewTop) && (elemBottom >= docViewBottom));
	    }
	}

	return {
	    init: init
	};
})();


$(document).ready(function() {
	audiTT.init();
});