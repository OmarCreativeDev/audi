$(function(){
    $('.homepageVideo').iLightBox({
        controls: {
            fullscreen: false
        },
        overlay: {
          opacity: 1
        },
        thumbnails: {
          normalOpacity: 0.6,
          activeOpacity: 1
        },
        path: 'horizontal',
        skin: 'mac'
    });
});