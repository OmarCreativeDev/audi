$(document).ready(function() {
	salmon.globalTagging.getTrackingProperties('load', 'action', 'o', 'null', {
		'props': {
			'prop1': '/home.html'
		},
		'other': {
			'pageName' : 'Home'
		}		
	});
});

$('.innerHeroContainer > a').click(function(event){
	var link = $(event.currentTarget);
	var destinationURL = $(this).attr('href');
	event.preventDefault();

	salmon.globalTagging.getTrackingProperties('click', 'action', 'o', 'null', {
		'props': {
			'prop31': 'HPH: ' + destinationURL
		},
		'eVars': {
			'eVar13' : 'HPH: ' + destinationURL
		}		
	});
	window.setTimeout(function() {
		window.location.href = link.attr('href');
	}, 250);
})

$('.containerGrid .promoBlock').click(function(){

	var destinationURL = $(this).attr('href');
	var pageURL = window.location.href;

	salmon.globalTagging.getTrackingProperties('click', 'action', 'o', 'null', {
		'props': {
			'prop29': pageURL + '_grid_' + destinationURL
		}
	});
});

$('a.imageTextWidget').click(function(event){
	var link = $(event.currentTarget);
	var linkName = $(this).find('p').text();
	event.preventDefault();
	
	salmon.globalTagging.getTrackingProperties('click', 'action', 'o', 'null', {
		'props': {
			'prop29': linkName
		}
	});
	window.setTimeout(function() {
		window.location.href = link.attr('href');
	}, 250);

});