module.exports = {
	options: {
		separator: "\n\n",
		banner: "(function(){\n\n'use strict';\n\n",
		footer: "\n\n})();",
		sourceMap: false
	},
	src: [  
 

		'src/etc/audi-ui/apps/myaudi-header/myaudiheader.js',
		'src/etc/audi-ui/apps/myaudi-header/ng-views/headerCtrl.js',
		'src/etc/audi-ui/apps/myaudi-header/ng-services/headerService.js',
		'src/etc/audi-ui/apps/myaudi-header/ng-services/apiService.js',		
		'src/etc/audi-ui/apps/myaudi-header/ng-services/apiServiceMocks.js'
 

			 
	],
	dest: 'src/etc/audi-ui/apps/myaudi-header/myaudiheader.min.js'
};
