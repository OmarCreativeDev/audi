

	myAudiHeader.directive('myAudiHeader', ['headerService', '$timeout',
		function(headerService) {
				
			return {
				restrict: 'EA',
				replace: true,
				templateUrl: '/etc/audi-ui/apps/myaudi-header/ng-views/header.html',
				controller: function ($scope, $timeout) {

					// add headerBar app scope as a variable
					var myAudiAppScope = angular.element('#myAudi').scope();

					$scope.headerService = headerService;
					$scope.profileData = headerService.profileData;
					$scope.unreadMessagesInfo = headerService.unreadMessagesInfo;

					// navigation urls
					$scope.loginURL = myAudiHeader.appConfig.urls.login;
					$scope.accountURL = myAudiHeader.appConfig.urls.account;
					$scope.messagesURL = myAudiHeader.appConfig.urls.messages;
					$scope.accountSettingsURL = myAudiHeader.appConfig.urls.accountSettings;

					var getCookies = document.cookie.split('; ');
					var cookiesArray = [];

					for (var i=0; i < getCookies.length; i++) {
						var splitCharacter = getCookies[i].indexOf('=');
						var calcCookieName = getCookies[i].substring(0, splitCharacter);
						cookiesArray.push(calcCookieName);
					}
 
					console.log('%c cookies= ', 'background:green; color:#fff', cookiesArray);
					
					if (cookiesArray.indexOf('nscma') > -1){
						console.log('%c authenticatedUser | nscma cookie exists ', 'background:green; color:#fff');
						$scope.authenticatedUser = true;
					} else {
						$scope.authenticatedUser = false;
					}

					if ($scope.authenticatedUser == true){
						$timeout(function(){
							headerService.getUnreadMsgCount();
							headerService.loadProfileData();
						}, 500);
					}

					$scope.logout = function(){
						console.log('headerService.logout() called');

						headerService.logout().then(function(){
							window.location.href = myAudiHeader.appConfig.urls.login;
						});
					};

				}
			};
		}
	]);
