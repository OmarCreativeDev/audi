

	myAudiHeader.factory('headerService', ['$resource', '$http', '$q', '$httpBackend', 'apiService', 
		function ($resource, $http, $q, $httpBackend, apiService, $state) {

			var profileData = {};
			var unreadMessagesInfo = {};

			var getUnreadMsgCount = function() {

				return new apiService.withErrorHandling({
					title: 'getUnreadMsgCount',
					url: 'messages',
					method: 'GET',
					params: {
						filter: 'UNREAD'
					}
				}) 
				.then(function(data) {
					angular.copy(data.listInfo, unreadMessagesInfo);
					console.log('%c getUnreadMsgCount ', 'background: red; color: #fff', unreadMessagesInfo);
				});

			};

			var logout = function() {

				return new apiService.withErrorHandling({
					title: 'logout',
					url: 'logout',
					method: 'POST'
				})
				.then(function(data) {
					console.log('%c logout ', 'background:#9900FF; color: #fff', data);
				});

			};

			var loadProfileData = function() {

				console.log('profileService.loadProfileData() called');

				return new apiService.withErrorHandling({
						title: 'loadProfileData',
						url: 'profile',
						method: 'GET',
						params: {}
					})
					.then(function (data) {
						data = data.data;
						angular.copy(data, profileData);
						console.log('%c profileData ', 'background: blue; color: #fff', data);
					});
			};

			return {
				getUnreadMsgCount: getUnreadMsgCount,
				logout: logout,
				loadProfileData: loadProfileData,
				profileData: profileData,
				unreadMessagesInfo: unreadMessagesInfo
			};
		}
	]);