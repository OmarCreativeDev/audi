
console.log('myAudiHeader module loaded -- -- -  -- ');


var myAudiHeader = angular.module('myAudiHeader', [
		  // 'ngResource',
		'ngSanitize',
			'ui.bootstrap',
			'sa.utils',
			'angulartics',
			'angulartics.adobe.analytics'
]);

 

    myAudiHeader.appConfig = {
        useMocks: true,
        urls: {}
    }


    myAudiHeader.config(function () {
		
    	var init = function () {

	        var getRegexMatch = function( regex, string ) {
	            var match = regex.exec( string );
	            return match[1];
	        };

	        // Example:
	        //   if
	        //     window.location.href == "http://127.0.0.1:9090/etc/audi-ui/myaudi-login.html#!/account/login",
	        //   then:
	        //     baseUrl = http://127.0.0.1:9090/
	        //     appsUrl = http://127.0.0.1:9090/etc/audi-ui/
	        //     appUrl  = http://127.0.0.1:9090/etc/audi-ui/myaudi-login.html

	        var baseUrl = getRegexMatch( /(^http[s]?:\/\/[^\/]*\/)/gi, window.location.href ),
	            appsUrl = getRegexMatch( /(^http[s]?:\/\/[^#]*\/)/gi,  window.location.href ),
	            appUrl  = getRegexMatch( /(^http[s]?:\/\/[^#]*)/gi,    window.location.href );

	        myAudiHeader.appConfig.urls.base = baseUrl;
	        myAudiHeader.appConfig.urls.apps = appsUrl;
	        myAudiHeader.appConfig.urls.app  = appUrl;

	        myAudiHeader.appConfig.urls.api  = baseUrl + 'myaudi-public-api/services/v1/';

	        myAudiHeader.appConfig.urls.account = appsUrl + 'myaudi.html#!/yourAudi/dashboard';
	        myAudiHeader.appConfig.urls.login   = appsUrl + 'myaudi-login.html';
	        myAudiHeader.appConfig.urls.messages = appsUrl + 'myaudi.html#!/yourAudi/messages';
	        myAudiHeader.appConfig.urls.accountSettings = appsUrl + 'myaudi.html#!/yourAudi/myDetails';

	        if(window.console) console.log("appConfig :", myAudiHeader.appConfig);

	    };

	    init();
	});

