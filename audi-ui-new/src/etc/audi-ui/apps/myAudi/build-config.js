module.exports = {
	options: {
		separator: "\n\n",
		banner: "(function(){\n\n'use strict';\n\n",
		footer: "\n\n})();",
		sourceMap: false
	},
	src: [

		// myAudi Module
		'src/etc/audi-ui/apps/myAudi/src/myAudi.js',

		// Conrollers
		'src/etc/audi-ui/apps/myAudi/src/components/dashboardCtrl.js',
		'src/etc/audi-ui/apps/myAudi/src/components/addCarCtrl.js',
		'src/etc/audi-ui/apps/myAudi/src/components/addOrderCtrl.js',
		'src/etc/audi-ui/apps/myAudi/src/components/myAccountCtrl.js',
		'src/etc/audi-ui/apps/myAudi/src/components/messagesCtrl.js',
		'src/etc/audi-ui/apps/myAudi/src/components/communicationPreferencesCtrl.js',
		'src/etc/audi-ui/apps/myAudi/src/components/manageCarCtrl.js',
		'src/etc/audi-ui/apps/myAudi/src/components/manageOrderCtrl.js',
		'src/etc/audi-ui/apps/myAudi/src/components/myGarage/myGarageCtrl.js',
		'src/etc/audi-ui/apps/myAudi/src/components/myCentres/myCentresCtrl.js',

		// Services
		'src/etc/audi-ui/apps/myAudi/src/services/centresService.js',
		'src/etc/audi-ui/apps/myAudi/src/services/apiService.js',
		'src/etc/audi-ui/apps/myAudi/src/services/profileService.js',
		'src/etc/audi-ui/apps/myAudi/src/services/messageService.js',
		'src/etc/audi-ui/apps/myAudi/src/services/carService.js',
		'src/etc/audi-ui/apps/myAudi/src/services/communicationPreferencesService.js',
		'src/etc/audi-ui/apps/myAudi/src/services/apiServiceMocks.js',
		'src/etc/audi-ui/apps/myAudi/src/services/mocks/vehiclesMocks.js',
		'src/etc/audi-ui/apps/myAudi/src/services/mocks/ordersMocks.js',

		//Directives
		'src/etc/audi-ui/apps/myAudi/src/directives/addCar.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/addOrder.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/carSummary.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/centreLocator.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/centreStatus.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/locationMap.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/orderSummary.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/previousCar.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/removeCar.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/salFormFeedbackIcons.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/salPopoverHtmlUnsafe.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/updateMileage.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/salEmailUnique.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/salPasswordRepeat.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/salPasswordValidate.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/salPasswordPopover.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/formattedInput.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/contentLoader.js',
		'src/etc/audi-ui/apps/myAudi/src/directives/imageLoader.js',

		// Filters
		'src/etc/audi-ui/apps/myAudi/src/filters/addressFilter.js',
		'src/etc/audi-ui/apps/myAudi/src/filters/isEmptyFilter.js',
		'src/etc/audi-ui/apps/myAudi/src/filters/sliceFilter.js',
	],
	dest: 'src/etc/audi-ui/apps/myAudi/dist/myAudi.js'
};
