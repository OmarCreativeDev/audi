myAudi.filter('isEmpty', function() {
	var val;
	return function (obj) {
		for (val in obj) {
			if (obj.hasOwnProperty(val)) {
				return false;
			}
		}
		return true;
	};
});
