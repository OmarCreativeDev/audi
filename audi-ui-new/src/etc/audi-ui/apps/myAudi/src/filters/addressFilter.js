myAudi.filter('address', function() {
	return function(address) {
		var address = String(address).replace(/[,]+/g, ",").split(",");
		var html = "";
		for (i = 0; i < address.length; i++) {
			if(i < address.length-1)
				html += address[i] + ",<br/>\n";
			else html += address[i] + ".";
		}
		return html; 
	};
});