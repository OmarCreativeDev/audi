myAudi.filter('monthName', [function() {
	return function(monthNumber) {
		// 1 = January
		var monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
			'July', 'August', 'September', 'October', 'November', 'December'
		];
		return monthNames[monthNumber - 1];
	};
}]);

myAudi.controller('myAccountCtrl', ['$scope', '$filter', '$q', '$modal', '$timeout', '$state', 'templateCache', 'profileService', 'communicationPreferencesService',
	function($scope, $filter, $q, $modal, $timeout, $state, templateCache, profileService, commPrefService) {

		// Utility functions
		// -----------------

		$scope.fieldHasError = function(formName, fieldName) {
			var form = $scope[formName],
				field = form[fieldName];
			return field.$invalid && (form.submitted || field.$hadFocus) ? true : false;
		};

		$scope.fieldHasSuccess = function(formName, fieldName) {
			var form = $scope[formName],
				field = form[fieldName];
			return field.$valid && (form.submitted || field.$hadFocus) ? true : false;
		};

		$scope.fieldHasFeedback = function(formName, fieldName) {
			var form = $scope[formName],
				field = form[fieldName];
			return form.submitted || field.$hadFocus ? true : false;
		};

		$scope.isAddressEmpty = function() {
			return profileService.isAddressEmpty();
		};

		var resetFormStatus = function (formName) {
			var form = $scope[formName];

			if (form.hasOwnProperty('submitted')) {
				form.submitted = false;
			}
		};

		$scope.modalDismiss = function() {
			console.log('dismiss modal');
			$scope.$dismiss();
			$state.transitionTo('myAudi.mydetails');
		};

		// Date of birth utilities

		var range = function (start, end) {
			return end >= start ? range(start,end-1).concat(end) : [];
		};

		var pad = function (num, len) {
			if (num instanceof Array) {
				for (var i = num.length - 1; i >= 0; i--) {
					num[i] = pad(num[i], len);
				}
			} else {
				var s = '0000' + num;
				return s.substr(s.length - len, len);
			}
			return num;
		};

		var yearsAgo = function (years) {
			var now = new Date(),
				m = now.getMonth(),
				d = now.getDate(),
				y = now.getFullYear() - years;
			return new Date(y, m, d);
		};


		// Variables
		// -----------------

		var minAge = 16,
			maxAge = 100,
			maxDate = yearsAgo(minAge),
			endYear = maxDate.getFullYear(),
			startYear = endYear + minAge - maxAge;

		$scope.appLoading = true; // is the app loading - primary used to show the spinner
		$scope.editing = true; // is the page in an edit mode
		$scope.savingInProgress = false;
		$scope.saveError = false;
		$scope.accountDeleting = false;
		$scope.deleteMsgError = false;

		$scope.profileData = profileService.profileData;
		$scope.profileDataTemp = profileService.profileDataTemp; // holds the unchanged data - used for 'Cancel'

		$scope.commPrefData = commPrefService.communicationPreferences;
		$scope.commPrefDataTemp = commPrefService.communicationPreferencesTemp; // holds the unchanged data - used for 'Cancel'

		$scope.sections = {
			personalInfo: {
				formName: 'personalInfoForm',
				edit: false,
				data: {},
				dob: {
					options: {
						days: pad(range(1, 31), 2),
						months: pad(range(1, 12), 2),
						years: pad(range(startYear, endYear)
							.reverse(), 4)
					}
				},
				titles: ['Dame', 'Dr', 'Lady', 'Lord', 'Miss', 'Mr', 'Mrs', 'Ms', 'Prof', 'Sir']
			},
			address: {
				formName: 'addrEditForm',
				edit: false,
				editForm: {
					visible: false
				},
				lookupForm: {
					step1: {
						visible: false
					},
					step2: {
						visible: false
					},
					data: {},
					foundAddresses: [],
					selectedAddr: {}
				}
			},
			addressLookup: {},
			contact: {
				formName: 'contactForm',
				edit: false
			},
			password: {
				formName: 'passwordForm',
				edit: false
			},
			permissions: {
				edit: false
			},
			communicationPref: {
				edit: false
			},
			marketingPermissions: {
				formName: 'marketingPermissionsForm',
				edit: false,
				data: {},
				editForm: {
					visible: false
				}
			},
			systemNotifications: {
				formName: 'systemNotificationsForm',
				edit: false,
				data: {},
				editForm: {
					visible: false
				}
			}
		};

		// -------------------------------------
		// Sections
		// -------------------------------------

		$scope.editSectionStart = function(section) {

			resetFormStatus($scope.sections[section].formName);

			$scope.sections[section].edit = true;
			$scope.editing = true;

			if ( section === 'address' ) {
				if (profileService.isAddressEmpty()) {
					showAddrLookupStep1();
				} else {
					showAddrEdit();
				}
			}
		};

		$scope.editSectionCancel = function(section) {
			$scope.saveError = false;
			profileService.cancelEdit();
			commPrefService.cancelEdit();

			$scope.sections[section].edit = false;
			$scope.editing = false;

			if (section === 'address') {
				hideAddrEdit();
				hideAddrLookup();
			}
		};

		$scope.editSectionSave = function(section) {

			var saveOK = function() {
				console.log('saveOK');
				$scope.editing = false;
				$scope.savingInProgress = false;
				$scope.saveError = false;
			};

			var saveError = function(section) {
				console.log('saveError');
				$scope.sections[section].edit = true;
				$scope.editing = true;
				$scope.savingInProgress = false;
				$scope.saveError = true;

				if ( section === 'address' ) {
					if (profileService.isAddressEmpty()) {
						showAddrLookupStep1();
					} else {
						showAddrEdit();
					}
				}
			};

			console.log('editSectionSave:', section);

			var formName = $scope.sections[section].formName;

			console.log('formName:', formName);

			if (section === 'addressLookup' || $scope[formName].$valid) {

				console.log('Form valid');

				$scope.saveError = false;
				$scope.savingInProgress = true;

				if ( section === 'addressLookup' ) {
					$scope.sections.address.edit = false;
				} else {
					$scope.sections[section].edit = false;
				}

				if (section === 'marketingPermissions' || section === 'systemNotifications') {

					commPrefService.saveCommunicationPreferences().then(function() {
						saveOK();
					}, function() {
						saveError(section);
					});

				} else {

					profileService.save('PUT').then(function() {
						saveOK();
					},function() {
						saveError(section);
					});
				}

			} else {
				$scope[formName].submitted = true;
			}

		};


		// Personal info section
		// -------------------------------------

		// Change date of birth
		$scope.changeDOB = function() {

			var d = $scope.profileData.dobFormated.day,
				m = $scope.profileData.dobFormated.month,
				y = $scope.profileData.dobFormated.year;

			var isEmpty = function (val) {
				return (val === null || val === '') ? true : false;
			};

			var isValidDate = function(y, m, d) {
				y = parseInt(y, 10);
				m = parseInt(m, 10);
				d = parseInt(d, 10);
				var date = new Date(y, m - 1, d);
				if ( (date.getFullYear() === y) && (date.getMonth()+1 === m) && (date.getDate() === d) ) {
					return true;
				} else {
					return false;
				}
			};

			if (isEmpty(d) && isEmpty(m) && isEmpty(y)) {

				// all date fields empty
				$scope.profileData.dateOfBirth = '';
				$scope.personalInfoForm.dateOfBirth.$setValidity('invalidDate', true);
				$scope.personalInfoForm.dateOfBirth.$setValidity('invalidAge', true);

			} else if (isEmpty(d) || isEmpty(m) || isEmpty(y)) {

				// at least 1 field empty
				$scope.personalInfoForm.dateOfBirth.$setValidity('invalidDate', false);
				$scope.personalInfoForm.dateOfBirth.$setValidity('invalidAge', true);

			} else {

				if (isValidDate(y, m, d)) {

					$scope.personalInfoForm.dateOfBirth.$setValidity('invalidDate', true);

					// format entered dob and max dob for easier comparison
					var calcDateOfBirth = new Date(y, m - 1, d);
					calcDateOfBirth = $filter('date')(calcDateOfBirth, 'dd MMM yyyy');
					maxDate = $filter('date')(maxDate, 'dd MMM yyyy');

					$scope.profileData.dateOfBirth = calcDateOfBirth;

					console.log('DOB:', $scope.profileData.dateOfBirth);
					console.log('maxDate:', maxDate);

					if ($scope.profileData.dateOfBirth > maxDate) {
						$scope.personalInfoForm.dateOfBirth.$setValidity('invalidAge', false);
					} else {
						$scope.personalInfoForm.dateOfBirth.$setValidity('invalidAge', true);
					}

				} else {
					$scope.personalInfoForm.dateOfBirth.$setValidity('invalidDate', false);
				}
			}
		};


		// Change address section
		// -------------------------------------

		$scope.editAddrManualClick = function() {
			hideAddrLookup();
			showAddrEdit();
		};

		$scope.goToLookupClick = function() {
			hideAddrEdit();
			showAddrLookupStep1();
		};

		$scope.editAddrManualFromLookupClick = function() {
			console.log('editAddrManualFromLookupClick()');
			var selectedAddr = $scope.sections.address.lookupForm.selectedAddr;
			$scope.sections.address.data = profileService.getAddressFromLookupData(selectedAddr);
			hideAddrLookup();
			showAddrEdit();
		};

		var showAddrLookupStep1 = function() {
			resetFormStatus('addrLookup1');
			$scope.sections.address.lookupForm.data = {};
			$scope.sections.address.lookupForm.step1.visible = true;
		};

		var showAddrLookupStep2 = function(addresses) {

			var model = $scope.sections.address.lookupForm;

			model.foundAddresses = addresses;
			model.selectedAddr = getAddrByHouse(model.foundAddresses) || model.foundAddresses[0];

			$scope.sections.address.lookupForm.step2.visible = true;
			$scope.focusAddrSelect = true;
		};

		var showAddrEdit = function() {
			$scope.sections.address.editForm.visible = true;
		};
		var hideAddrEdit = function() {
			$scope.sections.address.editForm.visible = false;
		};

		var hideAddrLookupStep1 = function() {
			$scope.sections.address.lookupForm.step1.visible = false;
		};
		var hideAddrLookupStep2 = function() {
			$scope.sections.address.lookupForm.step2.visible = false;
		};
		var hideAddrLookup = function() {
			hideAddrLookupStep1();
			hideAddrLookupStep2();
		};

		var getAddrByHouse = function(addresses) {

			$scope.sections.address.lookupForm.data = $scope.profileData;
			var model = $scope.sections.address.lookupForm.data;

			if (typeof model.address1 === 'undefined') {
				return false;
			}

			var houseNum = model.address1.replace(/[^\d.]/g, '').trim(),
				houseName = model.address1.replace(/[0-9]/g, '').trim();

			if (houseNum.length) {
				for (var i = 0; i < addresses.length; i++) {
					if (addresses[i].houseFlatNumber.indexOf(houseNum) > -1) {
						return addresses[i];
					}
				}
			} else if (houseName.length) {
				houseName = houseName.toLowerCase();
				for (var i = 0; i < addresses.length; i++) {
					if (addresses[i].premise.toLowerCase().indexOf(houseName) > -1) {
						return addresses[i];
					}
				}
			}

			return false;
		};

		$scope.doLookupAddressStep1 = function() {

			if ($scope.addrLookup1.$valid) {

				hideAddrLookupStep2();

				profileService.lookupAddress($scope.profileData.postcode)
					.success(function(data) {

						console.log('address lookup sucess', data);

						angular.forEach(data, function(val, key) {
							val.ID = key + 1;
							val.full = val.simpleAddress + ', ' + val.postCode.toUpperCase();
						});

						showAddrLookupStep2(data);
					})
					.error(function() {
						console.log('address lookup failed');
					});

			} else {
				$scope.addrLookup1.submitted = true;
			}
		};

		$scope.doLookupAddressStep2 = function() {
			console.log('lookupAddressStep2');
			profileService.saveAddrLookup($scope.sections.address.lookupForm.selectedAddr);
			hideAddrLookup();
			hideAddrEdit();
			$scope.editSectionSave('addressLookup');
		};


		// Delete account
		// -------------------------------------

		$scope.deleteAccount = function() {

			if (window.console) { console.log('deleteAccount'); }

			$scope.accountDeleting = true;
			$scope.deleteMsgError = false;

			profileService.deleteAccount()
			.then(
				function() {
					$timeout(function() {
						window.location.href = '/etc/audi-ui/myaudi-login.html#!/learnMore/landing/accountDeleted';
					}, 2000);
				},
				function() {
					$scope.accountDeleting = false;
					$scope.deleteMsgError = true;
				}
			);
		};


		// Initialize
		// -------------------------------------

		var init = function() {

			console.log('init myAccountCtrl');

			profileService.loadProfileData()
				.then(function() {
					$scope.editing = false;
					$scope.appLoading = false;
				});

			commPrefService.getCommunicationPreferences();

			templateCache.get('/etc/audi-ui/apps/myAudi/tmpl/components/deleteAccount.html');
		};

		init();
	}
]);
