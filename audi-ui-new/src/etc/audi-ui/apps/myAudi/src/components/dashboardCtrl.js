myAudi.controller('dashboardCtrl', ['$scope', '$state', 'carService', function ($scope, $state, carService) {

	$scope.cars = carService.cars;
	$scope.orders = carService.orders;
	$scope.loading = true;

	$scope.init = function () {

		carService.getCars().then(function(){
			$scope.loading = false;
		});

		carService.getOrders().then(function(){
			$scope.loading = false;
		});
	};

	$scope.hasOrders = function() {
		return ( $scope.orders.length > 0 ) ? true : false;
	};

	$scope.hasCars = function() {
		return ( $scope.cars.length > 0 ) ? true : false;
	};

	$scope.init();
}]);
