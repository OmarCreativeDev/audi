myAudi.controller('messagesCtrl', ['$scope', 'messageService', '$filter',
	function($scope, messageService, $filter) {

		$scope.appLoading = true;
		$scope.messagesLoading = true;
		$scope.checkAllValue = false;

		var currentYear = new Date().getFullYear();
		var calcToday = new Date();
		var calcYesterday = new Date();
		calcYesterday = calcYesterday.setDate(calcYesterday.getDate()-1);

		$scope.today = $filter('date')(calcToday, 'dd MMM');
		$scope.yesterday = $filter('date')(calcYesterday, 'dd MMM');

		$scope.messages = messageService.messages; //message
		$scope.messagesListInfo = messageService.messagesListInfo; // paging info
		$scope.messagesListInfo.pageNumber = 1; // set default page number
		$scope.unreadMessagesInfo = messageService.unreadMessagesInfo;

		var getCookies = document.cookie.split('; ');
		var cookiesArray = [];

		for (var i=0; i < getCookies.length; i++) {
			var splitCharacter = getCookies[i].indexOf('=');
			var calcCookieName = getCookies[i].substring(0, splitCharacter);
			cookiesArray.push(calcCookieName);
		}

		console.log('%c cookies= ', 'background:yellow; color:#000', cookiesArray);

		if (cookiesArray.indexOf('nscma') > -1){
			console.log('%c authenticatedUser | nscma cookie exists ', 'background:blue; color:#fff');
			$scope.authenticatedUser = true;
		} else {
			$scope.authenticatedUser = false;
		}

		$scope.init = function() {
			$scope.getMessages($scope.messagesListInfo.pageNumber);
		};

		$scope.checkUnreadMessages = function() {
			messageService.checkUnreadMessages();

			// update unread message count in headerBar app
			myAudiHeaderAppScope.headerService.getUnreadMsgCount();
		};

		$scope.getMessages = function(pageNumber) {

			$scope.messagesLoading = true;

			messageService.getMessages(pageNumber).then(function() {

				angular.forEach($scope.messages, function(message) {

					var checkYear = $filter('date')(message.messageDateTime, 'y');

					if (checkYear == currentYear){
						message.messageDateTime = $filter('date')(message.messageDateTime, 'dd MMM');
					} else {
						message.messageDateTime = $filter('date')(message.messageDateTime, 'dd MMM y');
					}
				});

				$scope.messages.checked = false;
				$scope.appLoading = false;
				$scope.messagesLoading = false;
				$scope.checkUnreadMessages();
			});
		};

		$scope.getMessage = function(message) {
			if (this.messageIsopen = !this.messageIsopen) {
				messageService.getMessage(message).then(function(){
					message.messageDateTime = $filter('date')(message.messageDateTime, 'dd MMM');
					$scope.checkUnreadMessages();
				});
			}
		};

		$scope.toggleFlag = function(message) {
			console.log('%c message.flagged', 'background:#000; color:#fff', message.flagged);
			message.flagged = !message.flagged;
			messageService.toggleFlag(message);
		};

		$scope.deleteMessages = function() {

			var messageIDArray = [];

			angular.forEach($scope.messages, function(message) {
				if (message.checked === true) {
					message.deleted = true;
					$scope.messagesLoading = true;
					messageIDArray.push(message.messageId);
				}
			});

			messageService.deleteMessage(messageIDArray).then(function(){
				var currentPage = $scope.messagesListInfo.pageNumber;
				var totalPages = $scope.messagesListInfo.totalPages;
				var pageNumber = currentPage === totalPages ? totalPages : currentPage;

				$scope.messagesLoading = false;
				$scope.getMessages(pageNumber);
				$scope.checkAllValue = null;
			});

		};

		$scope.toggleMessage = function(message){

			console.log('toggle Message fired');
			if (typeof message.checked === 'undefined'){
				message.checked = false;
			}

			message.checked = !message.checked;
			console.log(message.checked);
			$scope.toggleAllMessagesStatus();
		};

		$scope.toggleAllMessages = function() {

			console.log('toggle ALL Messages fired');

			angular.forEach($scope.messages, function(message) {
				if (typeof message.checked === 'undefined'){
					message.checked = false;
				}
				message.checked = !$scope.checkAllValue;
			});

		};

		$scope.toggleAllMessagesStatus = function() {

			var checkedMessages = [];
			var messagesOnScreen = $scope.messages.length;

			angular.forEach($scope.messages, function(message, key){
				if(message.checked == true) {
					checkedMessages.push(message.checked);
				}
			});

			console.log(checkedMessages);
			console.log(messagesOnScreen);

			if (checkedMessages.length === messagesOnScreen) {
				$scope.checkAllValue = true;
			} else {
				$scope.checkAllValue = false;
			}
		};

		$scope.init();
	}
]);
