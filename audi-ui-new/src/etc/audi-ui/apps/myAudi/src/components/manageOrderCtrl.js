myAudi.controller('manageOrderCtrl', ['$scope', 'carService', 'templateCache',
	function ($scope, carService, templateCache) {

		// for testing...
		$scope.order = {};
		$scope.modelInfo = {
			'model' : 'A1 TEST car',
			'description' : 'almost ready!!! just two more weeks...'
		};

		var partials = {
			'orderReceived' : '/content/audi/ui-testapps/manage-order/order-received.tab-ajax.html',
			'bodyShop' : '/content/audi/ui-testapps/manage-order/body-shop.tab-ajax.html',
			'paintShop' : '/content/audi/ui-testapps/manage-order/paint-shop.tab-ajax.html',
			'finalAssembly' : '/content/audi/ui-testapps/manage-order/final-assembly.tab-ajax.html',
			'qualityControl' : '/content/audi/ui-testapps/manage-order/quality-control.tab-ajax.html',
			'inTransit' : '/content/audi/ui-testapps/manage-order/in-transit.tab-ajax.html',
			'readyToCollect' : '/content/audi/ui-testapps/manage-order/ready-to-collect.tab-ajax.html'
		};

		$scope.currentPartial = 'orderReceived';

		$scope.init = function() {

			if ( carService.currentOrderNumber !== undefined ) {
				$scope.orderNumber = carService.currentOrderNumber;
				delete carService.currentOrderNumber;
			} else {
				$scope.orderNumber = '22222222';
			}

			carService.getOrder($scope.orderNumber).then( function() {
				$scope.order = carService.order;
			});

			$scope.loadPartial($scope.currentPartial);

			templateCache.get('/etc/audi-ui/apps/myAudi/tmpl/components/manageOrder.html');
		};

		$scope.loadPartial = function(partial) {
			$scope.currentPartial = partial;
			$scope.modalMode = partials[$scope.currentPartial];
		};

		$scope.getPosition = function(){

			if (window.console) { console.log('partials ', partials); }

			var index = 0;
			for (var key in partials) {
				if (window.console) { console.log(key , $scope.currentPartial); }
				if (key === $scope.currentPartial) {
					return index;
				}
				index++;
			}
		};

		$scope.init();
	}
]);

myAudi.directive('indicator', [ 'carService',
	function( carService ) {
		return {
			restrict: 'A',
			require: 'ngModel',
			scope: {
				partial : '=ngModel'
			},
			template: '<img ng-src="{{imgSrc}}" />',
			link: function (scope, element, attrs, ngModel) {
				if (!ngModel) { return; } // do nothing if no ng-model

				element.addClass('indicator');

				// DEMO FIX
				carService.getImage('carReg', scope.$parent.orderNumber).
				then( function(data){
					scope.imgSrc =data.data.url;
				});

				ngModel.$formatters.unshift(function () {
					var tabWidth = 131;
					var position = scope.$parent.getPosition() * tabWidth;
					element.css({
						'left' : position,
						'width' : tabWidth
					});
				});

			}
		};
	}
]);
