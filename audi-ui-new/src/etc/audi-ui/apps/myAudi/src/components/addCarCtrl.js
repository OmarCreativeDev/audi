myAudi.controller('addCarCtrl', ['$scope','$timeout','carService','templateCache',
	function ($scope, $timeout, carService, templateCache) {

		var currentView = '';

		$scope.loading = false;
		$scope.loadingCarSearch1 = false;
		$scope.loadingCarSearch2 = false;

		$scope.carLookupResponse = {};
		$scope.addCarToAccountResponse = {};

		$scope.carLookupFormData = {
			'carReg': '',
			'postcode': ''
		};

		$scope.postcodeFormData = {
			'postcode': ''
		};

		var init = function () {

			$scope.loading = true;

			if ( carService.dashCarLookupFormData !== undefined ) {
				$scope.carLookupFormData = carService.dashCarLookupFormData;
				delete carService.dashCarLookupFormData;
				carSearch( $scope.carLookupFormData.carReg, $scope.carLookupFormData.postcode, false );
			} else {
				$scope.showView('carLookup');
			}

			templateCache.get('/etc/audi-ui/apps/myAudi/tmpl/components/addCar.html');

		};

		$scope.getCurentView = function () {
			return currentView;
		};

		$scope.isCurentView = function ( viewName ) {
			if ( $scope.loading ) { return false; }
			return ( currentView === viewName ) ? true : false;
		};

		$scope.showView = function ( viewName ) {
			$scope.loading = false;
			$scope.loadingCarSearch1 = false;
			$scope.loadingCarSearch2 = false;
			currentView = viewName;
		};

		$scope.goToCarLookup = function () {
			$scope.carLookupFormData.carReg = '';
			$scope.carLookupFormData.postcode = '';
			$scope.showView('carLookup');
		};

		$scope.goToCarDetails = function () {
			window.alert('Close the modal and go to the car details page');
			$scope.modalDismiss();
		};

		var carSearch = function( carReg, postcode, completeSearch ) {

			$scope.loading = true;

			if ( completeSearch ) {
				$scope.loadingCarSearch2 = true;
			} else {
				$scope.loadingCarSearch1 = true;
			}

			carService.carSearch( carReg, postcode, completeSearch ).then(
				function (response) {

					console.log('Car lookup success. Response =', response);

					var carDetailsMatch = function( newResponse ) {
						if (
							$scope.carLookupResponse.make === undefined ||
							$scope.carLookupResponse.model === undefined ||
							$scope.carLookupResponse.variant === undefined ||
							$scope.carLookupResponse.exteriorColour === undefined ||
							$scope.carLookupResponse.make !== newResponse.make ||
							$scope.carLookupResponse.model !== newResponse.model ||
							$scope.carLookupResponse.variant !== newResponse.variant ||
							$scope.carLookupResponse.exteriorColour !== newResponse.exteriorColour
						) {
							return false;
						} else {
							return true;
						}
					};

					if ( completeSearch && carDetailsMatch(response.data) ) {

						console.log('Car details from the 2nd lookup are the same');
						$scope.showView('carNotFound');

					} else {

						$scope.carLookupResponse = response.data;

						if ( response.data.onAccount ) {
							$scope.showView('carOnAccount');
						} else {
							$scope.showView('carFound');
						}

						// load car image
						carService.getImage('carReg', carReg).then(function( data ){
							$scope.carImageUrl =data.data.url;
						});

					}
				},
				function (response) {

					console.log('Car lookup failed. Response =', response);

					if ( response.responseInfo.code === 998 || response.responseInfo.code === 999 ) {
						serverError();
					}

					if ( response.responseInfo.code === 404 ) {
						$scope.showView('carNotFound');
					}

				}
			);
		};

		$scope.doCarLookup = function (scope) {

			scope.carLookupForm.submitted = true;

			if ( scope.carLookupForm.$valid ) {
				$scope.carLookupFormData.carReg = $scope.carLookupFormData.carReg.toUpperCase();
				$scope.carLookupFormData.postcode = $scope.carLookupFormData.postcode.toUpperCase();
				carSearch( $scope.carLookupFormData.carReg, $scope.carLookupFormData.postcode, false );
			}
		};

		$scope.doNotMyCar = function () {

			console.log('in doNotMyCar()');
			console.log('$scope.carLookupFormData =', $scope.carLookupFormData);
			console.log('$scope.carLookupResponse =', $scope.carLookupResponse);

			// Do 2nd ("complete") search if not done before
			if ( $scope.carLookupResponse.completeSearch === false ) {
				$scope.loading = true;
				carSearch( $scope.carLookupFormData.carReg, $scope.carLookupFormData.postcode, true );
			} else {
				$scope.showView('carNotFound');
			}

		};

		$scope.doAddCar = function () {

			console.log('doing doAddCar(), $scope.carLookupFormData =', $scope.carLookupFormData);

			$scope.loading = true;

			carService.addCarToAccount( $scope.carLookupResponse.vehicleKey, $scope.carLookupFormData.postcode, 'O' )
				.then( function (response) {

					console.log('Add car success. Response =', response);

					carService.getCars();

					$scope.addCarToAccountResponse = response.data;
					$scope.showView('carAdded');

				}, function (response) {

					console.log('Add car failed. Response =', response);

					if ( response.responseInfo.code === 998 || response.responseInfo.code === 999 ) {
						serverError();
					}

					// User has already added vehicle in MyAudi
					if ( response.responseInfo.code === 305 ) {
						$scope.showView('carOnAccount');
					}

					// Vehicle not found
					if ( response.responseInfo.code === 406 ) {
						$scope.showView('carNotFound');
					}

					$scope.addCarToAccountResponse = response.data;

					// Vehicle already has an owner
					if ( response.responseInfo.code === 304 ) {

						// Is owner a company?
						if ( response.data.ownerClass === 'C' ) {
							$scope.showView('companyCar');
						} else {
							$scope.showView('confirmOwnership');
						}
					}

				});
		};

		$scope.doAddCarNewPostcode = function () {

			console.log('doing doAddCarNewPostcode(), $scope.carLookupFormData =', $scope.carLookupFormData, ' new postcode =', $scope.postcodeFormData.postcode);

			$scope.loading = true;

			carService.addCarToAccount( $scope.carLookupResponse.vehicleKey, $scope.postcodeFormData.postcode, 'K' )
				.then( function (response) {

					console.log('Add car success. Response =', response);

					carService.getCars();

					$scope.addCarToAccountResponse = response.data;
					$scope.showView('carAdded');

				}, function (response) {

					console.log('Add car failed. Response =', response);
					$scope.showView('unableToVerify');

				});
		};

		$scope.doOwnedByAnotherPerson = function () {

			console.log('doOwnedByAnotherPerson(), addCarToAccountResponse =', $scope.addCarToAccountResponse );

			if ( $scope.addCarToAccountResponse.postcodeMatch ) {

				$scope.loading = true;

				// If postcodes match then add to account as Keeper (K)
				carService.addCarToAccount( $scope.carLookupResponse.vehicleKey, $scope.carLookupFormData.postcode, 'K' )
					.then( function (response) {

						console.log('Add car success. Response =', response);

						carService.getCars();

						$scope.addCarToAccountResponse = response.data;
						$scope.showView('carAdded');

					}, function (response) {

						console.log('Add car failed. Response =', response);

					});

			} else {
				$scope.showView('confirmPostcode');
			}
		};

		$scope.doAddCompanyCar = function () {

			console.log('in doAddCompanyCar()');

			if ( $scope.addCarToAccountResponse.hasDrivers ) {

				$scope.showView('unableToVerify');

			} else {

				$scope.loading = true;

				carService.addCarToAccount( $scope.carLookupResponse.vehicleKey, $scope.carLookupFormData.postcode, 'D' )
					.then( function (response) {

						console.log('Add car success. Response =', response);

						carService.getCars();

						$scope.addCarToAccountResponse = response.data;
						$scope.showView('carAdded');

					}, function (response) {

						console.log('Add car failed. Response =', response);
						$scope.showView('unableToVerify');

					});
			}
		};

		var serverError = function() {
			window.alert( 'Service temporarily unavailable. Please try again later.' );
		};

		init();


		// Utility functions
		// -----------------

		$scope.fieldHasError = function( formName, fieldName, scope ) {

			if ( scope === undefined ) { return false; }

			var form = scope[formName],
				field = form[fieldName];

			return field.$invalid && (form.submitted || field.$hadFocus) ? true : false;
		};

		$scope.fieldHasSuccess = function( formName, fieldName, scope ) {

			if ( scope === undefined ) { return false; }

			var form = scope[formName],
				field = form[fieldName];

			return field.$valid && (form.submitted || field.$hadFocus) ? true : false;
		};

		$scope.fieldHasFeedback = function( formName, fieldName, scope ) {

			if ( scope === undefined ) { return false; }

			var form = scope[formName],
				field = form[fieldName];

			return form.submitted || field.$hadFocus ? true : false;
		};

		$scope.modalDismiss = function() {
			console.log('dismiss modal');
			$scope.$dismiss();
		};

		$scope.modalClose = function() {
			console.log('close modal');
			$scope.$close();
		};

	}
]);
