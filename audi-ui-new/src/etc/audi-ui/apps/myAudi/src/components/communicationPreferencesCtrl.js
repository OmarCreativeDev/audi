myAudi.controller('communicationPreferencesCtrl', ['$scope', 'communicationPreferencesService', '$state',
	function ($scope, communicationPreferencesService, $state) {

		$scope.preferences = communicationPreferencesService.preferences;

		$scope.init = function(){
			communicationPreferencesService.getCommunicationPreferences();
		};

		$scope.init();

		$scope.checkAll = function(select) {
			angular.forEach($scope.preferences, function(value, key) {
				$scope.preferences[key] = select;
			});
			$scope.updatePreferences();
		};

		$scope.updatePreferences = function() {
			communicationPreferencesService.setCommunicationPreferences();
		};

		$scope.modalDismiss = function() {
			if (window.console) { console.log('modal dismissed'); }
			$scope.$dismiss();
			$state.go('myAudi.communicationPreferences');
		};

		$scope.deleteAccount = function() {
			if (window.console) { console.log('deleteAccount'); }
			$scope.$dismiss();
			window.location.href = 'http://www.audi.co.uk.5.uat.nativ-systems.com/content/audi/ui-testapps/myaudi-login.html#!/learnMore/landing/accountDeleted';
			// window.location.href = '/etc/audi-ui/myaudi-login.html#!/learnMore/landing/accountDeleted';
		};
	}
]);
