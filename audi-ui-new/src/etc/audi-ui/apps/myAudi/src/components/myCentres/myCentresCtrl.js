myAudi.controller('myCentresCtrl', ['$scope', 'centresService','$modal', function ($scope, centresService,$modal) {

	$scope.map = {
		center: {
			latitude: 51.6251,
			longitude: -0.1764
		},
		zoom: 15
	};

	$scope.marker = {
		coords: {
			latitude: 52.240021,
			longitude: -0.93018
		},
		show: true
	};

	$scope.centrelocator = {
		name : 'North Hampton',
		dealercode : '43'
	};

}]);
