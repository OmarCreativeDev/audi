myAudi.controller('manageCarCtrl', ['$scope', 'carService', 'centresService', '$modal', '$location', '$anchorScroll', '$timeout', 'templateCache',
	function ($scope, carService, centresService, $modal, $location, $anchorScroll, $timeout, templateCache) {
		$scope.carInfo = {};
		$scope.showSpec = false;
		$scope.loading = true;
		$scope.loadingMessage = 'Loading...';

		$scope.init = function() {

			var vehicleKey;
			if ( carService.currentCarVehicleKey !== undefined ) {
				vehicleKey = carService.currentCarVehicleKey;
				delete carService.currentCarVehicleKey;
			} else {
				vehicleKey = 'VALID';
			}

			carService.getCar(vehicleKey).then(function(){
				$scope.carInfo = carService.car;
				if ( $location.hash() === 'fullSpec' ) {
					$scope.displaySpec();
				}
				$scope.carInfo.age = carAge($scope.carInfo.ownershipStartDate);
				$scope.loading = false;
			}, function() {
				console.log('Error loading car info');
				$scope.loadingMessage = 'Unable to get the data. <br/>Please try refreshing the page again.';
			});

			templateCache.get('/etc/audi-ui/apps/myAudi/tmpl/components/manageCar.html');
		};

		$scope.displaySpec = function() {
			$scope.showSpec = true;
			$timeout(function()
			{
				$scope.scrollTo();
			}, 100);
		};

		$scope.hideSpec = function() {
			$scope.showSpec = false;
			$location.hash('');
		};

		$scope.scrollTo = function() {
			$location.hash('fullSpec');
			$anchorScroll();
		};

		var carAge = function(_date) {
			if (_date !== 'null' && typeof _date !== 'undefined' && _date !== null) {
				var date1 = new Date(_date);
				var date2 = new Date();

				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
				return diffDays;
			} else {
				// just an impossible number to not show the warranty and breakdown cover.
				return 50000;
			}
		};

		$scope.init();
	}
]);
