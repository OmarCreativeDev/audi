myAudi.controller('addOrderCtrl', ['$scope','$timeout','carService', 'profileService', 'templateCache', '$state',
	function ($scope, $timeout, carService, profileService, templateCache, $state) {

		var currentView = '';

		$scope.loading = false;
		$scope.loadingSearch = false;
		$scope.showOrderNotFound = false;
		$scope.dontKnowOrderNumVisible = false;

		$scope.orderDetails = {};

		$scope.orderLookupFormData = {
			'orderNumber': '',
			'postcode': ''
		};

		var init = function () {

			$scope.loading = true;

			if ( carService.dashOrderLookupFormData !== undefined ) {
				$scope.orderLookupFormData = carService.dashOrderLookupFormData;
				delete carService.dashOrderLookupFormData;
				orderSearch( $scope.orderLookupFormData.orderNumber, $scope.orderLookupFormData.postcode );
			} else {

				// Commented out until the profile service is optimised so that it will
				// cache the data and not make an AJAX call every time the user data is needed
				//
				// profileService.loadProfileData().then(function () {
				// 	var userProfile = profileService.profileData;
				// 	if ( typeof userProfile.postcode === 'string' && userProfile.postcode.length > 0 ) {
				// 		$scope.orderLookupFormData.postcode = userProfile.postcode;
				// 	}
				// 	$scope.showView('orderLookup');
				// }, function () {
				// 	console.log('profileService.loadProfileData() FAILED');
				// });
				//

				$scope.showView('orderLookup');
			}

			templateCache.get('/etc/audi-ui/apps/myAudi/tmpl/components/addOrder.html');

		};

		$scope.getCurentView = function () {
			return currentView;
		};

		$scope.isCurentView = function ( viewName ) {
			if ( $scope.loading ) { return false; }
			return ( currentView === viewName ) ? true : false;
		};

		$scope.showView = function ( viewName ) {
			$scope.loading = false;
			$scope.loadingSearch = false;
			currentView = viewName;
		};

		$scope.goToOrderLookup = function () {
			$scope.orderDetails = {};
			$scope.orderLookupFormData.orderNumber = '';
			$scope.orderLookupFormData.postcode = '';
			$scope.showView('orderLookup');
		};

		$scope.goToOrderDetails = function () {
			window.alert('Close the modal and go to the order details page');
			$scope.modalDismiss();
		};

		var orderSearch = function( orderNumber, postcode ) {

			$scope.loading = true;
			$scope.loadingSearch = true;

			carService.orderSearch( orderNumber, postcode ).then(
				function (response) {

					console.log('Order lookup success. Response =', response);
					$scope.orderDetails = response.data;
					$scope.showView('orderFound');

					// load car image
					carService.getImage('orderNumber', orderNumber).
						then( function(data){
							$scope.carImageUrl =data.data.url;
						});
				},
				function (response) {

					console.log('Order lookup failed. Response =', response);

					if ( response.responseInfo.code === 998 || response.responseInfo.code === 999 ) {
						serverError();
					} else if ( response.responseInfo.code === 306 ) {
						console.log('Order already on account');

						// load car image
						carService.getImage('orderNumber', orderNumber).
							then( function(data){
								$scope.carImageUrl =data.data.url;
							});

						$scope.orderDetails = response.data;
						$scope.showView('orderOnAccount');
					} else {
						$scope.showOrderNotFound = true;
						$scope.goToOrderLookup();
					}
				}
			);
		};

		$scope.doOrderLookup = function (scope) {

			scope.orderLookupForm.submitted = true;

			if ( scope.orderLookupForm.$valid ) {
				orderSearch( $scope.orderLookupFormData.orderNumber, $scope.orderLookupFormData.postcode );
			}
		};

		$scope.doAddToAccount = function () {

			$scope.loading = true;

			carService.addOrderToAccount( $scope.orderDetails.orderNumber ).then(
				function (response) {

					console.log('Add order success. Response =', response);
					carService.getOrders();
					$scope.showView('orderAdded');
				},
				function (response) {
					console.log('Add order failed. Response =', response);
					serverError();
				}
			);
		};

		$scope.toggleDontKnowOrderNum = function() {
			$scope.dontKnowOrderNumVisible = !$scope.dontKnowOrderNumVisible;
		};

		$scope.goToManageOrder = function() {
			carService.currentOrderNumber = $scope.orderLookupFormData.orderNumber;
			$scope.modalDismiss('redirect');
			$state.go('myAudi.manageorder');
		};

		var serverError = function() {
			window.alert( 'Service temporarily unavailable. Please try again later.' );
		};

		init();


		// Utility functions
		// -----------------

		$scope.fieldHasError = function( formName, fieldName, scope ) {

			if ( scope === undefined ) { return false; }

			var form = scope[formName],
				field = form[fieldName];

			return field.$invalid && (form.submitted || field.$hadFocus) ? true : false;
		};

		$scope.fieldHasSuccess = function( formName, fieldName, scope ) {

			if ( scope === undefined ) { return false; }

			var form = scope[formName],
				field = form[fieldName];

			return field.$valid && (form.submitted || field.$hadFocus) ? true : false;
		};

		$scope.fieldHasFeedback = function( formName, fieldName, scope ) {

			if ( scope === undefined ) { return false; }

			var form = scope[formName],
				field = form[fieldName];

			return form.submitted || field.$hadFocus ? true : false;
		};

		$scope.modalDismiss = function( reason ) {
			console.log('dismiss modal');
			$scope.$dismiss( reason );
		};

		$scope.modalClose = function() {
			console.log('close modal');
			$scope.$close();
		};
	}
]);
