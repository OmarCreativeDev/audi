myAudi.directive("carSummary", ['$timeout', '$filter', '$location', '$state', 'carService',
	function($timeout, $filter, $location, $state, carService) {
		return {
			restrict: "A",
			scope: {
				carInfo : "=carSummary"
			},
			templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/directives/carSummary.html',
			link : function(scope, element, attrs, ngModel) {
				console.log("carSummary link");
				var checkCarInfo = function() {
					$timeout( function() {
						($filter('isEmpty')(scope.carInfo)) ? checkCarInfo() : scope.init();
					}, 1000);
				}
				checkCarInfo();
			},
			controller: function ($scope, $timeout, $filter, $state) {

				console.log("carSummary controller");
				$scope.mileage = {};
				$scope.centrelocator = {};
				$scope.loading = true;

				$scope.init = function () {
					$scope.mileage = {
								date : $scope.carInfo.lastMileageDate, // yyyy-mm-dd
								miles : ($scope.carInfo.lastMileage != "null") ?  $scope.carInfo.lastMileage : 0,
								vehicleKey : $scope.carInfo.vehicleKey
							};

					console.log($scope.mileage, "mileage", $scope.carInfo.lastMileage);

					$scope.centrelocator = {
								name: $scope.carInfo.preferredDealerName,
								dealerCode: $scope.carInfo.preferredDealerCode,
								vehicleKey : $scope.carInfo.vehicleKey
							};

					carService.getImage('vehicleKey', $scope.carInfo.vehicleKey).
					then( function(data){
						$scope.src =data.data.url;
					});

					$scope.loading = false;

				};

				$scope.displaySpec = function() {
					if(document.getElementById('fullSpec')) {
						$scope.$parent.displaySpec();
					} else {
						carService.currentCarVehicleKey = $scope.carInfo.vehicleKey
						$state.transitionTo('myAudi.managecar');
					}
				}
			}
		}
	}
]);
