myAudi.directive('addCar', ['$state', 'carService', function($state, carService) {
	return {
		restrict: 'A',
		require: 'ngModel',
		transclude : true,
		scope: {
			ngModel : '='
		},
		templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/directives/addCar.html',
		link : function(scope, element, attrs) {
			scope.layout = (typeof attrs.narrow === 'undefined') ? 'wide' : 'narrow';
		},
		controller: function ($scope) {

			$scope.dashCarLookupFormData = {
				'carReg': '',
				'postcode': ''
			};

			$scope.doCarLookup = function() {

				$scope.dashCarLookupForm.submitted = true;

				if ( $scope.dashCarLookupForm.$valid ) {
					carService.dashCarLookupFormData = {
						carReg: $scope.dashCarLookupFormData.carReg.toUpperCase(),
						postcode: $scope.dashCarLookupFormData.postcode.toUpperCase()
					};
					$state.go('myAudi.dashboard.addCar');
				}
			};

			// Utility functions
			// -----------------

			$scope.fieldHasError = function( formName, fieldName ) {
				var form = $scope[formName],
					field = form[fieldName];
				return field.$invalid && (form.submitted || field.$hadFocus) ? true : false;
			};

			$scope.fieldHasSuccess = function( formName, fieldName ) {
				var form = $scope[formName],
					field = form[fieldName];
				return field.$valid && (form.submitted || field.$hadFocus) ? true : false;
			};

			$scope.fieldHasFeedback = function( formName, fieldName ) {
				var form = $scope[formName],
					field = form[fieldName];
				return form.submitted || field.$hadFocus ? true : false;
			};
		}
	};
}]);
