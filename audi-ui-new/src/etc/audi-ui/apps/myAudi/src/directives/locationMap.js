//
// USAGE : displays Google map
// <location-map map='map'> </location-map>
//
myAudi.directive('locationMap', [ '$modal', function($modal) {
	return {
		restrict: 'E',
		scope: {
			map : '=',
			height : '='
		},
		templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/directives/locationMap.html',
		link : function(scope, element, attrs) {

			window.initializeGmaps = function() {
				if (window.console) { console.log('google maps script loaded'); }
				scope.initializeGmaps();
			};

			function loadGmaps() {
				if (window.console) { console.log('LOAD GMAPS'); }
				var script = document.createElement('script');
				script.type = 'text/javascript';
				script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' +
				'callback=initializeGmaps';
				document.body.appendChild(script);
			}

			if (typeof google === 'object' && typeof google.maps === 'object'){
				scope.initializeGmaps();
			} else {
				loadGmaps();
			}
		},
		controller: function ($scope) {

			$scope.initializeGmaps = function (){
				if(typeof $scope.map.latitude !== 'undefined' && typeof $scope.map.longitude !== 'undefined') {
					// only now do render the map.

					var latLng = new google.maps.LatLng($scope.map.latitude, $scope.map.longitude);
					var mapOptions = {
						zoom: $scope.map.zoom,
						center: latLng
					};

					var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
					var marker = new google.maps.Marker({
						position: latLng,
						map: map,
						title: $scope.map.title
					});

					var contentString = '<div id="content">'+
										'<h2>'+$scope.map.title+'</h2>'+
										'</div>';

					var infowindow = new google.maps.InfoWindow({
						content: contentString,
						maxWidth: 350
					});

					// var directionsService = new google.maps.DirectionsService();
					// var directionsDisplay = new google.maps.DirectionsRenderer();
					// 	directionsDisplay.setMap(map);

					google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(map,marker);
					});
				}

				$scope.calcRoute = function(origin) {

					if(!origin) {
						$scope.directionInput(' ');
						return;
					}

					var destination = $scope.map.postcode;
					var request = {
						origin: origin,
						destination: destination,
						travelMode: google.maps.TravelMode.DRIVING
					};

					directionsService.route(request, function(response, status) {

						if (status == google.maps.DirectionsStatus.OK && response.status == 'OK')  {
							directionsDisplay.setDirections(response);
						} else if(response.status == 'NOT_FOUND'){
							$scope.directionInput(origin);
						} else {

						}
					});
				};

				$scope.directionInput = function (origin) {
					var url = 'https://maps.google.com?daddr='+$scope.map.title+'+'+$scope.map.postcode;
					window.open(url,'_blank');
				};
			}

			var directionInputModelCtrl = function ($scope, $modalInstance, origin) {

				$scope.origin = origin;

				// angular bootstrap modal instance flaw - not able to bind scope variables
				// bidirectional unless its an object... don't know why ?
				$scope.obj = {
					origin : null
				};

				$scope.close = function(){
					$modalInstance.close();
				};

				$scope.submit = function () {
					$modalInstance.close($scope.obj.origin);
				};
			}
		}
	}
}]);
