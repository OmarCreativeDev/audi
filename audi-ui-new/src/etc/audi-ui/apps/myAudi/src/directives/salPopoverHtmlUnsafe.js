angular.module( 'ui.bootstrap.popover' )

	.directive( 'popoverHtmlUnsafePopup', ['$sce',
		function ($sce) {
			var trusted = {};
			return {
				restrict: 'EA',
				replace: true,
				scope: { title: '@', content: '@', placement: '@', animation: '&', isOpen: '&' },
				templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/directives/salPopoverHtmlUnsafe.html',
				link: function ($scope) {
					$scope.toTrusted = function(html) {
						return trusted[html] || (trusted[html] = $sce.trustAsHtml(html));
					};
				}
			};
		}
	])

	.directive( 'popoverHtmlUnsafe', [ '$tooltip',
		function ( $tooltip ) {
			return $tooltip( 'popoverHtmlUnsafe', 'popover', 'click' );
		}
	]);