// USAGE : <input  ng-model='mileage.miles'
//                 formatted
//                 type='formatted numbers'/>

// type can be set to  :
// number or numbers : only displays numbers
// formatted number or formatted numbers : display numbers with commas
// currency : not yet updated...  should display pund and decimals...
// formatted text : does't allow special charaters...

myAudi.directive('formatted', [ function() {
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			type: '@type'
		},
		link: function(scope, element, attrs, ngModel) {

			if (window.console) { console.log('INPUT type : ', scope.type); }

			if (!ngModel) { return; } // do nothing if no ng-model

			var numberWithCommas = function(value) {
				console.log('value is ' + value);

				value = value.toString();
				value = value.replace(/[^0-9\.]/g, '');

				var parts = value.split('.');
				parts[0] = parts[0].replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, '$&,');
				if (parts[1] && parts[1].length > 2) {
					parts[1] = parts[1].substring(0, 2);
				}
				return parts.join('');
			};

			ngModel.$parsers.unshift(function(inputValue) {

				var inputClassList = element.context.className,
					hasIEPlaceholderClass = (inputClassList.indexOf('empty') > -1) ? true : false,
					hasIEPlaceholderValue = (element.context.value === attrs.placeholder) ? true : false;

				if (!hasIEPlaceholderClass && !hasIEPlaceholderValue && inputValue.length > 0) {

					console.log('input value=' + inputValue);
					var result = inputValue;

					switch (scope.type) {

						case 'number':
						case 'numbers':
							result = inputValue.replace(/[^\d]+/g, ''); //inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
							break;

						case 'formatted number':
						case 'formatted numbers':
							result = numberWithCommas(inputValue.replace(/[^\d]+/g, ''));
							break;

						case 'currency':
							break;

						case 'formatted text':
							console.log('input value=' + result);
							result = inputValue.replace(/[^\w\s]/gi, '');
							break;

						default:
							return result;
					}

					ngModel.$viewValue = result;
					ngModel.$render();
					return result;
				}
			});
		}
	};
}]);
