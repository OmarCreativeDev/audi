//
//	USAGE : display car summary - required orderInfo
//	<div order-summary order-info="order"></div>
//
myAudi.directive('orderSummary', ['$timeout', '$filter', '$state', 'carService', function($timeout, $filter, $state, carService) {
	return {
		restrict: 'A',
		scope: {
			orderInfo : '=orderSummary'
		},
		templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/directives/orderSummary.html',
		link : function(scope, element, attrs, ngModel) {

			scope.mini = (typeof attrs.mini !== "undefined") ? true : false;

			var checkOrderInfo = function() {
				$timeout(function() {
					if ( $filter('isEmpty')(scope.orderInfo) ) {
						checkOrderInfo();
					} else {
						scope.init();
					}
				}, 1000);
			};
			checkOrderInfo();
		},
		controller: function ($scope) {
			$scope.loading = true;

			$scope.init = function () {
				$scope.loading = false;
				carService.getImage('orderNumber', $scope.orderInfo.orderNumber).
				then( function(data){
					$scope.src =data.data.url;
				});

			};

			$scope.manageOrder = function () {
				carService.currentOrderNumber = $scope.orderInfo.orderNumber;
				$state.transitionTo('myAudi.manageorder');
			};
		}
	};
}]);
