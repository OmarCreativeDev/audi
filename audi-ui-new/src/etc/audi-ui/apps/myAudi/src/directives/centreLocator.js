//
//  USAGE : popup to show center details or allow search and selection of centre.
//	<a centre-locator search > Change Location </a>
//
myAudi.directive('centreLocator', [ '$modal', 'centresService', 'carService', '$timeout', function($modal, centresService, carService, $timeout) {
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			search : '@search',
			ngModel : '='
		},

		link : function(scope, element, attrs, ngModel) {
			element.bind('click', scope.centreLocator);
		},
		controller: function ($scope) {

			$scope.centreLocator = function () {

				$scope.size = (typeof $scope.search !== 'undefined') ? 'sm' : 'lg';

				var modalInstance = $modal.open({
					windowClass: 'locateCentrePopup',
					templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/directives/centreLocator/centreLocator.html',
					controller: ModalInstanceCtrl,
					size: $scope.size,
					resolve: {
						search: function() {
							return angular.copy($scope.search);
						},
						locationCenter: function() {
							if (typeof $scope.ngModel !== 'undefined' ) {
								return angular.copy($scope.ngModel);
							} else {
								return;
							}
						}
					}
				});

				modalInstance.setSize = function (mode) {

					switch(mode) {
						case 'search' :
							$scope.size = 'modal-sm';
							break;

						case 'details' :
							$scope.size = 'modal-lg';
							break;

						case 'error' :
						case 'results' :
						case 'loading' :
							$scope.size = 'modal-md';
							break;
					}

					// var elem = angular.element(document.querySelectorAll('[modal-window]'));

					var elem = angular.element($('[modal-window]'));
					elem.find('.modal-dialog').removeClass('modal-lg modal-md modal-sm').addClass($scope.size);

				};

				modalInstance.result.then(
					function (locationCenter) {
						if (typeof locationCenter !== 'undefined') {
							$scope.ngModel = locationCenter;
						}
					},
					function () {
					  if (window.console) { console.log('Modal dismissed at: ' + new Date()); }
					}
				);
			};

			var ModalInstanceCtrl = function ($scope, $modalInstance, search, locationCenter) {

				var currentMode;
				$scope.centre = centresService.centre;
				$scope.warning = false;
				$scope.loadingMessage = 'loading';

				if ( typeof locationCenter !== 'undefined' ) {
					$scope.location = locationCenter.name;
					$scope.dealerCode = locationCenter.dealerCode;
					$scope.vehicleKey = locationCenter.vehicleKey;
				}

				$scope.init = function(){
					if ( typeof search !== 'undefined' ) {
						$scope.location = '';
						$scope.showBackToResults = true;
						$scope.setModelMode('search');
					} else {
						$scope.viewCentre({dealerCode : $scope.dealerCode});
					}
				};

				$scope.setModelMode =  function(panel) {
					console.log('setModelMode:',panel);
					if (panel !== 'loading') {
						currentMode = panel;
						$scope.loadingData = false;
					} else {
						$scope.warning = false;
						$scope.loadingData = true;
					}

					$modalInstance.setSize(panel);
					$scope.modalMode='/etc/audi-ui/apps/myAudi/tmpl/directives/centreLocator/'+panel+'.html';
				};

				// $scope.reset = function() {
				//     $scope.location = '
				//     centresService.resetCentres();
				//     $scope.setModelMode('search')
				// }

				$scope.close = function(){
					$modalInstance.close();
				};

				$scope.viewCentreList = function() {
					$scope.setModelMode('results');
				};

				$scope.centreSearch = function(location) {
					if (location.length > 0) {

						$scope.loadingMessage = 'Please wait whilst we search for your Audi centre';
						$scope.setModelMode('loading');

						centresService.centreSearch(location, 5).then(function() {
							// unbind fix
							console.log('Results : ', $scope.centres);
							if (centresService.centres.length > 0) {
								$scope.centres = angular.copy(centresService.centres);
								$scope.location = location;
								$scope.viewCentreList();
							} else {
								setWarning('Location not found.');
							}
						}, function() {
							setWarning('The service is currently unavailable. Please try again later.');
						});
					}
				};

				var setWarning = function(message) {
					$scope.warning = true;
					$scope.warningMessage = message;
					$scope.setModelMode(currentMode);
				};

				$scope.viewCentre = function(centre) {

					centre.loadingData = true;
					$scope.loadingData = true;
					$scope.setModelMode('loading');

					centresService.getCentre(centre.dealerCode).then(
						function(){
							$scope.map = {
								zoom : 14,
								latitude: $scope.centre.location.latitude,
								longitude: $scope.centre.location.longitude,
								title : $scope.centre.name,
								postcode : $scope.centre.postcode
							};
							// $scope.centre.address = $scope.formatDealerAddress($scope.centre.address);
							$scope.setModelMode('details');
							centre.loadingData = false;
							$scope.centre.salesHours = groupServiceTimes(angular.copy($scope.centre.salesHours));
							$scope.centre.serviceHours = groupServiceTimes(angular.copy($scope.centre.serviceHours));
						},
						function() {
							$scope.setModelMode('error');
						}
					);
				};

				var groupServiceTimes = function(serviceHours) {

					var group = false;
					var weekHours = serviceHours[0].hours;

					for (var i in serviceHours) {
						var sh = serviceHours[i];
						console.log(sh.days, '>>>>', i);
						if( sh.days !== 'Sat' && sh.days !== 'Sun' && sh.days !== 'undefined') {
							console.log(weekHours,  sh.days, sh.hours);
							if(weekHours !== sh.hours) {
								return serviceHours;
							}
						} else {
							group = true;
							break;
						}
					}

					if (group && serviceHours.length >= 7) {
						serviceHours = serviceHours.slice(5, serviceHours.length);
						serviceHours.unshift({
							'days': 'Mon - Fri',
							'hours': weekHours
						});
					}

					return serviceHours;
				};

				$scope.selectCentre = function(centre){

					if (window.console) { console.log('selectCentre', centre); }
					$scope.loadingMessage = 'Please wait whilst we set <u>'+centre.name+'</u> as your preffered centre';
					$scope.setModelMode('loading');

					var dealer = {
						preferredDealerCode : centre.dealerCode,
						//	preferredDealerName : centre.name,
						vehicleKey : $scope.vehicleKey
					};

					carService.updateCar(dealer).then(function() {
						$modalInstance.close(centre);
					},function() {
						$scope.errorMessage = 'Unable to update the dealer information. Please try again';
						$scope.setModelMode('error');
					});
				};

				$scope.init();
			};
		}
	};
}]);
