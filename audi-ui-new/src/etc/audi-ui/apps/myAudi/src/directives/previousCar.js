myAudi.directive('previousCar', [ '$modal', 'carService', '$state', function($modal, carService, $state) {
	return {
		restrict: 'A',
		scope : {
			carInfo : '='
		},
		link : function(scope, element, attrs) {
			element.bind('click', scope.previousCar);
			if (window.console) { console.log('REMOVE CAR'); }
		},
		controller: function ($scope) {
			$scope.previousCar = function () {
				$modal.open({
					templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/directives/previousCar.html',
					controller: ModalInstanceCtrl,
					size: 'md',
					resolve: {
						carInfo: function() {
							return angular.copy($scope.carInfo);
						}
					}
				});
			};

			var ModalInstanceCtrl = function ($scope, $modalInstance, carInfo) {

				$scope.carInfo = carInfo;

				$scope.ok = function () {

					var data = {
						vehicleKey : $scope.carInfo.vehicleKey,
						setToPrevious : true
					};

					carService.setToPreviousTMP(data).then(function(){
						if (window.console) { console.log('Set as previous car'); }
						$modalInstance.close();
						$state.go('myAudi.dashboard');
					}, function() {
						if (window.console) { console.log('ERROR posting as previous car'); }
					});

					// carService.updateCar(data).then(function(){
					// 	if (window.console) { console.log('Set as previous car'); }
					// 	$modalInstance.close();
					// 	$state.transitionTo('myAudi.dashboard');
					// }, function() {
					// 	if (window.console) { console.log('ERROR posting as previous car'); }
					// });

				};

				$scope.open = function (event) {
					event.stopPropagation();
				};

				$scope.cancel = function () {
					$modalInstance.dismiss('cancel');
				};

				$scope.init = function() {};

				$scope.init();
			};
		}
	};
}]);
