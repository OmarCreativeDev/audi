// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this,
			args = arguments;
		clearTimeout(timeout);
		timeout = setTimeout(function() {
			timeout = null;
			if (!immediate) { func.apply(context, args); }
		}, wait);
		if (immediate && !timeout) { func.apply(context, args); }
	};
}

myAudi.directive('salEmailUnique', ['profileService', function( profileService ) {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function (scope, element, attrs, ctrl) {
			scope.$watch(attrs.ngModel, debounce(function() {

				console.log('salEmailUnique: checking email');

				ctrl.$setValidity( 'emailUsed', true );

				if ( ctrl.$valid === true ) {

					profileService.lookupEmail( ctrl.$viewValue ).then(function() {
						console.log('Email already registered');
						ctrl.$setValidity( 'emailUsed', false );
					}, function() {
						console.log('Email not registered');
					});
				}
			}, 250));
		}
	};
}]);

