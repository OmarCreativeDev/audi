myAudi.directive('salPasswordPopover', [ function() {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function (scope, element, attrs, ctrl) {

			var propName = attrs.salPasswordPopover;

			scope.$watch(attrs.ngModel, function() {

				var popoverTxt = '';

				if ( ctrl.$error.chars ) {
					popoverTxt += '<p><span class="glyphicon glyphicon-remove"></span> 10 or more characters</p>';
				} else {
					popoverTxt += '<p><span class="glyphicon glyphicon-ok"></span> 10 or more characters</p>';
				}
				if ( ctrl.$error.numbers ) {
					popoverTxt += '<p><span class="glyphicon glyphicon-remove"></span> at least 1 number</p>';
				} else {
					popoverTxt += '<p><span class="glyphicon glyphicon-ok"></span> at least 1 number</p>';
				}
				if ( ctrl.$error.ucase ) {
					popoverTxt += '<p><span class="glyphicon glyphicon-remove"></span> at least 1 uppercase letter</p>';
				} else {
					popoverTxt += '<p><span class="glyphicon glyphicon-ok"></span> at least 1 uppercase letter</p>';
				}
				if ( ctrl.$error.lcase ) {
					popoverTxt += '<p><span class="glyphicon glyphicon-remove"></span> at least 1 lowercase letter</p>';
				} else {
					popoverTxt += '<p><span class="glyphicon glyphicon-ok"></span> at least 1 lowercase letter</p>';
				}

				popoverTxt = '<div class="password-popover-content"><h3>Password validation</h3>' + popoverTxt + '</div>';

				scope.$parent[propName] = popoverTxt;
			});
		}
	};
}]);
