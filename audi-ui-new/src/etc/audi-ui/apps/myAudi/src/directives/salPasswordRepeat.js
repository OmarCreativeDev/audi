myAudi.directive('salPasswordRepeat', [ '$interpolate', function( $interpolate ) {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function (scope, element, attrs, ctrl) {

			var model1 = attrs.salPasswordRepeat,
				model2 = attrs.ngModel;

			scope.$watch( model1, function() {
				comparePasswords();
			});

			scope.$watch( model2, function() {
				comparePasswords();
			});

			function passwordsMatch( p1, p2 ) {
				ctrl.$setValidity( 'different', ( p1 === p2 ) ? true : false );
			}

			function comparePasswords() {
				var p1 = scope.$eval($interpolate( '{{'+ model1 +'}}' )),
					p2 = ctrl.$modelValue;
				passwordsMatch( p1, p2 );
			}
		}
	};
}]);
