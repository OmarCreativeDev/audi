//
//	USAGE : <div content-loader="path"/>
//
myAudi.directive('contentLoader', [ function() {
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			path : '=ngModel',
			info : '='
		},
		template: '<div class="text-center"><span ng-if="loading" audi-loader /></div><div ng-class="{\'in\' : !loading}" class="fade" ng-include="path" onload="loaded()"> </div>',
		link: function (scope, element, attrs, ngModel) {

			if (!ngModel) { return; } // do nothing if no ng-model

			scope.loading = true;

			ngModel.$formatters.unshift(function () {
				scope.loading = true;
			});
		},
		controller: function($scope) {
			$scope.loaded = function(){
				$scope.loading = false;
			};
		}
	};
}]);
