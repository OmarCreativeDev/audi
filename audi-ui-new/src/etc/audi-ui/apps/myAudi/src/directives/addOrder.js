myAudi.directive('addOrder', ['$state', 'carService', 'profileService',
	function($state, carService, profileService) {
		return {
			restrict: 'A',
			require: 'ngModel',
			scope: {
				ngModel : '='
			},
			templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/directives/addOrder.html',
			link : function(scope, element, attrs) {
				scope.layout = (typeof attrs.narrow === 'undefined') ? 'wide' : 'narrow';
			},
			controller: function ($scope) {

				$scope.dontKnowOrderNumVisible = false;

				$scope.dashOrderLookupFormData = {
					'orderNumber': '',
					'postcode': ''
				};

				profileService.loadProfileData().then(function () {
					var userProfile = profileService.profileData;
					if ( typeof userProfile.postcode === 'string' && userProfile.postcode.length > 0 ) {
						$scope.dashOrderLookupFormData.postcode = userProfile.postcode;
					}
				}, function () {
					console.log('profileService.loadProfileData() FAILED');
				});

				$scope.doOrderLookup = function() {

					$scope.dashOrderLookupForm.submitted = true;

					if ( $scope.dashOrderLookupForm.$valid ) {
						carService.dashOrderLookupFormData = {
							orderNumber: $scope.dashOrderLookupFormData.orderNumber,
							postcode: $scope.dashOrderLookupFormData.postcode.toUpperCase()
						};
						$state.go('myAudi.dashboard.addOrder');
					}
				};

				$scope.toggleDontKnowOrderNum = function() {
					$scope.dontKnowOrderNumVisible = !$scope.dontKnowOrderNumVisible;
				};


				// Utility functions
				// -----------------

				$scope.fieldHasError = function( formName, fieldName ) {
					var form = $scope[formName],
						field = form[fieldName];
					return field.$invalid && (form.submitted || field.$hadFocus) ? true : false;
				};

				$scope.fieldHasSuccess = function( formName, fieldName ) {
					var form = $scope[formName],
						field = form[fieldName];
					return field.$valid && (form.submitted || field.$hadFocus) ? true : false;
				};

				$scope.fieldHasFeedback = function( formName, fieldName ) {
					var form = $scope[formName],
						field = form[fieldName];
					return form.submitted || field.$hadFocus ? true : false;
				};
			}
		};
	}
]);
