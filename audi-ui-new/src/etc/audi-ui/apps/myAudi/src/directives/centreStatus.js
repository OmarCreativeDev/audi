//
// USAGE : Shows the Location Centre - open status
// <a centre-status times="times"> Change Location </a>
//
myAudi.directive('centreStatus', [ '$filter', function($filter) {
	return {
		restrict: 'E',
		scope: {
			times : '='
		},
		template: '<span class="text-center storeOpenOrClosed" ng-class="status"><strong>{{status}}</strong></span>',
		link : function(scope, element, attrs, ngModel) {
			scope.init();
		},
		controller: function ($scope) {

			$scope.init = function(){
				$scope.today = new Date();
				for (var i=0; i < $scope.times.length; i++) {

					var day = $filter('date')($scope.today, 'EEE'),
						week = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'];

					if  (day === $scope.times[i].days || ($scope.times[i].days === 'Mon - Fri' && week.indexOf(day) > -1) ) {
						var hours = String($scope.times[i].hours).toLowerCase();
						if (hours === 'closed' || hours === 'undefined') {
							$scope.status = 'Closed';
						} else {
							checkTimes(hours);
						}
						break;
					}
				}
			};

			var checkTimes = function(hours){

				var hours = String(hours).split('-'),
					startTime = hours[0].split(':'),
					endTime = hours[1].split(':'),
					currentTime = Number($filter('date')($scope.today, 'H'))*60+Number($filter('date')($scope.today, 'm'));

				startTime = Number(startTime[0])*60+Number(startTime[1]);
				endTime = Number(endTime[0])*60+Number(endTime[1]);

				$scope.status = (startTime < currentTime && currentTime < endTime) ? 'Open now' : 'Closed';
			};
		}
	};
}]);
