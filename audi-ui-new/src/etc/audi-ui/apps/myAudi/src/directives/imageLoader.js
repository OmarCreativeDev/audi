//
//	USAGE : <img ng-src="{{src}}" img-loader width="248" height="100">
//
myAudi.directive('imgLoader', [	function() {
	return {
		restrict: 'A',
		replace: true,
		template: '<div><span ng-if="!hasImageUrl" ng-style="loaderStyle" audi-loader></span><img ng-if="hasImageUrl" src="{{imageSrc}}" width="{{width}}" height="{{height}}"></div>',
		link: function (scope, element, attrs) {

			var loaderWidth = 100,
				loaderHeight = 38;

			scope.width = attrs.width;
			scope.height = attrs.height;
			scope.hasImageUrl = false;

			element.css({
				position: 'relative',
				width: scope.width + 'px',
				height: scope.height + 'px'
			});

			scope.loaderStyle = {
				position: 'absolute',
				top: Math.floor((scope.height - loaderHeight)/2) + 'px',
				left: Math.floor((scope.width - loaderWidth)/2) + 'px'
			};

			attrs.$observe('src', function(value) {
				if (typeof value === 'undefined' || value === '') {
				} else {
					scope.hasImageUrl = true;
					scope.imageSrc = value;
				}
			});
		}
	};
}]);
