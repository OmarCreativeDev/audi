myAudi.directive('removeCar', [ '$modal', 'carService', '$state', function($modal, carService, $state) {
	return {
		restrict: 'A',
		require : 'ngModel',
		scope : {
			carInfo : '=',
			ngModel : '='
		},
		link : function(scope, element, attrs) {
			element.bind('click', scope.removeCar);
		},
		controller: function ($scope) {
			$scope.removeCar = function () {
				var modalInstance = $modal.open({
					templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/directives/removeCar.html',
					controller: ModalInstanceCtrl,
					size: 'md',
					resolve: {
						carInfo: function() {
							return angular.copy($scope.carInfo);
						}
					}
				});

				modalInstance.result.then(function () {
					// post back set car as previous car. TO DO...
				 	/*
				 	carService.setToPrevious($scope.carInfo.vehicleKey).then(function(){
						if (window.console) console.log('car removed');
						//hide remove car link.
					}, function() {
						if (window.console) console.log('ERROR deleting the car');
					});
					*/
				});
			};

			var ModalInstanceCtrl = function ($scope, $modalInstance, carInfo) {

				$scope.carInfo = carInfo;

				$scope.ok = function () {
					$modalInstance.close();
				};

				$scope.open = function (event) {
					event.stopPropagation();
				};

				$scope.cancel = function () {
					$modalInstance.dismiss('cancel');
				};

				$scope.init = function() {};

				$scope.init();
			};
		}
	};
}]);
