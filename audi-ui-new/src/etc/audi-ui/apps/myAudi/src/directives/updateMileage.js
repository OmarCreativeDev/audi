//
//	USAGE : popup to update mileage and date
//	<a update-mileage mileage='mileage'> Update Mileage </a>
//
myAudi.directive('updateMileage', [ '$modal', 'carService', function($modal, carService) {
	return {
		restrict: 'A',
		scope: {
			mileage : '=updateMileage'
		},
		link : function(scope, element, attrs) {
			element.bind('click', scope.updateMileage);
		},
		controller: function ($scope) {
			$scope.updateMileage = function () {
				var modalInstance = $modal.open({
					templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/directives/updateMileage.html',
					controller: ModalInstanceCtrl,
					size: 'sm',
					resolve: {
						mileage: function () {
							return angular.copy($scope.mileage);
						}
					}
				});

				modalInstance.result.then(function (mileage) {
						// fix to reset the miles to a number with out formating...
						mileage.miles =  mileage.miles.replace(/[^\d]+/g, '');
						$scope.mileage = mileage;
					}, function () {
					  console.log('Modal dismissed at: ' + new Date());
				});
			};

			var ModalInstanceCtrl = function ($scope, $modalInstance, mileage) {

				$scope.isDisabled = false;
				$scope.mileage =  mileage;
				$scope.format = 'dd-MMMM-yyyy';

				$scope.ok = function () {
					if(mileage.miles !== '' & !$scope.isDisabled) {
						$scope.isDisabled = true;
						carService.updateCar($scope.mileage).then(function(data){
							$modalInstance.close($scope.mileage);
						}, function(){
							$scope.isDisabled = false;
							$scope.warning = 'Unsuccess!!! Please try again.';
						});
					}
				};

				$scope.cancel = function () {
					$modalInstance.dismiss('cancel');
				};

				$scope.init = function() {
					mileage.date = new Date();
					mileage.miles = '';
				};

				$scope.init();
			};
		}
	}
}]);