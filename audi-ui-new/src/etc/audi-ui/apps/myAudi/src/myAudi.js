'use strict';

console.log('myAudi module loaded');

var myAudi = angular.module('myAudi', [
	'ui.router',
	'ngResource',
	'ngAnimate',
	'ngSanitize',
	'ui.bootstrap',
	'sa.utils',
	'placeholder',
	'angulartics',
	'angulartics.adobe.analytics'
]);

myAudi.appConfig = {
	useMocks: true,
	urls: {}
};

var init = function () {

	var getRegexMatch = function( regex, string ) {
		var match = regex.exec( string );
		return match[1];
	};

	// Example:
	//   if
	//     window.location.href == 'http://127.0.0.1:9090/etc/audi-ui/myaudi-login.html#!/account/login',
	//   then:
	//     baseUrl = http://127.0.0.1:9090/
	//     appsUrl = http://127.0.0.1:9090/etc/audi-ui/
	//     appUrl  = http://127.0.0.1:9090/etc/audi-ui/myaudi-login.html

	var baseUrl = getRegexMatch( /(^http[s]?:\/\/[^\/]*\/)/gi, window.location.href ),
		appsUrl = getRegexMatch( /(^http[s]?:\/\/[^#]*\/)/gi,  window.location.href ),
		appUrl  = getRegexMatch( /(^http[s]?:\/\/[^#]*)/gi,    window.location.href );

	myAudi.appConfig.urls.base = baseUrl;
	myAudi.appConfig.urls.apps = appsUrl;
	myAudi.appConfig.urls.app  = appUrl;

	myAudi.appConfig.urls.api  = baseUrl + 'myaudi-public-api/services/v1/';

	myAudi.appConfig.urls.account = appsUrl + 'myaudi.html';
	myAudi.appConfig.urls.login   = appsUrl + 'myaudi-login.html';

	if ( typeof window.webServicesHost === 'string' ) {
		myAudi.appConfig.urls.webServices = window.webServicesHost;
	} else {
		myAudi.appConfig.urls.webServices = 'https://soa.audi.co.uk';
	}

	if (window.console) { console.log('appConfig :', myAudi.appConfig); }
};

init();

myAudi.config(['$stateProvider', '$urlRouterProvider',
	function ($stateProvider, $urlRouterProvider) {

		$stateProvider

			.state('myAudi', {
				url: '/yourAudi',
				abstract: true,
				templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/main-wrapper.html',
			})

			.state('myAudi.dashboard', {
				url: '/dashboard',
				templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/components/dashboard.html',
				controller: 'dashboardCtrl'
			})

			.state('myAudi.dashboard.addCar', {
				url: '/addCar',
				onEnter: ['$stateParams', '$state', '$modal', '$resource',
					function($stateParams, $state, $modal) {
						$modal.open({
							templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/components/addCar.html',
							size: 'md',
							backdrop: 'static'
						})
						.result.then(function() {
							console.log('modal closed, go to dashboard');
							$state.transitionTo( 'myAudi.dashboard', {}, { reload: false, notify: true } );
						},function() {
							console.log('modal dismissed, go to dashboard');
							$state.transitionTo( 'myAudi.dashboard', {}, { reload: false, notify: true } );
						});
					}
				]
			})

			.state('myAudi.dashboard.addOrder', {
				url: '/addOrder',
				onEnter: ['$stateParams', '$state', '$modal', '$resource',
					function($stateParams, $state, $modal) {
						$modal.open({
							templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/components/addOrder.html',
							size: 'md',
							backdrop: 'static'
						})
						.result.then(function() {
							console.log('modal closed, go to dashboard');
							$state.transitionTo( 'myAudi.dashboard', {}, { reload: false, notify: true } );
						},function( reason ) {
							if ( reason === 'redirect' ) {
								console.log('modal dismissed, redirect');
							} else {
								console.log('modal dismissed, go to dashboard');
								$state.transitionTo( 'myAudi.dashboard', {}, { reload: false, notify: true } );
							}
						});
					}
				]
			})

			.state('myAudi.messages', {
				url: '/messages',
				templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/components/messages.html',
				controller: 'messagesCtrl'
			})
			.state('myAudi.mydetails', {
				url: "/myDetails",
				templateUrl: "/etc/audi-ui/apps/myAudi/tmpl/components/myDetails.html",
				controller: "myAccountCtrl"
			})

			.state('myAudi.mydetails.deleteAccount', {
				url: '/deleteAccount',
				onEnter: ['$stateParams', '$state', '$modal', '$resource',
					function($stateParams, $state, $modal, $resource) {
						$modal.open({
							templateUrl: '/etc/audi-ui/apps/myAudi/tmpl/components/deleteAccount.html',
							controller: 'myAccountCtrl',
							size: 'lg',
							backdrop: 'static',
							resolve: {}
						})
					}
				]
			})

			.state('myAudi.mycentres', {
				url: "/myCentres",
				templateUrl: "/etc/audi-ui/apps/myAudi/tmpl/components/myCentres/myCentres.html",
				controller: "myCentresCtrl"
			})

			.state('myAudi.centressearch', {
				url: "/centressearch",
				templateUrl: "/etc/audi-ui/apps/myAudi/tmpl/components/myCentres/centreSearch.html",
				controller: "centreSearchCtrl"
			})

			.state('myAudi.managecar', {
				url: "/managecar",
				templateUrl: "/etc/audi-ui/apps/myAudi/tmpl/components/manageCar.html",
				controller: "manageCarCtrl"
			})

			.state('myAudi.manageorder', {
				url: "/manageorder",
				templateUrl: "/etc/audi-ui/apps/myAudi/tmpl/components/manageOrder.html",
				controller: "manageOrderCtrl"
			})


		/* default app route */
		//$urlRouterProvider.otherwise('/yourAudi/dashboard');
	}
]);

//
// Initate the UI Router state
//
myAudi.run(['$state', function ($state) {
	$state.go('myAudi.dashboard');
}]);
