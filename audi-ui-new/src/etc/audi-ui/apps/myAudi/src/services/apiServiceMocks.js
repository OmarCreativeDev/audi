myAudi.factory('apiServiceMocks', ['$httpBackend', 'vehiclesMocks', 'ordersMocks', function ($httpBackend, vehiclesMocks, ordersMocks) {

	var getQueryVar = function (query, varName) {
		var vars = query.split("&");
		for ( var i = 0; i < vars.length; i++ ) {
			   var pair = vars[i].split("=");
			   if(pair[0] == varName){ return pair[1]; }
		}
		return(false);
	};

	var apiUrl = myAudi.appConfig.urls.api;

	var userVehicles = [];
	var userOrders = [];

	var errorCodes = {
		/* Authentication */
		100: "Incorrect password",
		101: "Account locked - cannot login",
		102: /*"Account email not verified - cannot login"*/ "This email is associated with an unverified account, you need to follow the instructions in your verification email.",
		103: /* "User token expired", */ "Unfortunately your account verification has expired",
		104: /* "User token invalid", */ "Unfortunately your account verification is invalid",
		105: "User does not have authority to access resource",
		106: "Password change required",

		/* Validation */
		300: "Required param was null/empty",
		301: "Invalid param value",

		/** Resource Not Found */
		400: "Invalid Url",
		404: "Resource not found",
		414: "User not found in database",

		/* Unknown error */
		999: "Unknown error occurred"
	};

	var RI_SUCCESS = {
			code: 0,
			desc: "Success"
		};
	var RI_ERROR = {
			code: 500,
			desc: "Error"
		};

	var setupMocks = function () {

		userProfileMocks();
		communicationPrefMocks();
		orderMocks();
		setupVehicleMocks();
		manageCarMocks();
		messagesMocks();

	};

	var profileData = {
					"myAudiId": "124",
					"dbgId": "12346",
					"cbpId": "34678",
					"primaryPreferredDealerCode": "402",
					"SecondaryPreferredDealerCode": "03576",
					"title": "Mr",
					"initials": "J",
					"forename": "Alex",
					"surname": "Stewart",
					"formalSalutation": "Dear",
					"informalSalutation": "Hi",
					"organisationName": "Salmon Ltd",
					"address1": "",
					"address2": "",
					"city": "",
					"postcode": "",
					"county": "",
					"country": "",
					"pafValidated": "true",
					"genderCode": "M",
					"dateOfBirth": "",
					"maritalStatus": "M",
					"email": "mbrook@Salmon.com",
					"workPhoneNumber": "",
					"homePhoneNumber": "",
					"mobilePhoneNumber": "",
					"addressValidFrom": "020714",
					"prefCommType": "Phone",
					"commPrefTime": "10am",
					"chelseaSupporter": "0",
					"nearestDealerKey": "00234",
					"profileVehicleVIN":"WAUZZZ4B63N065761"
				};

	var userProfileMocks = function() {

		$httpBackend.whenGET(apiUrl + "profile")
			.respond(function (method, url, data) {
				return [200, {
					responseInfo: RI_SUCCESS,
					data: profileData
				}, {}];
			});

		$httpBackend.whenPOST(apiUrl + "profile")
			.respond(function (method, url, data) {
				data = angular.fromJson(data);
				angular.extend(profileData, data)
				return [200, {
					responseInfo: RI_SUCCESS,
					data: profileData
				}, {}];
			});


		$httpBackend.whenDELETE(apiUrl + "users")
			.respond(function (method, url, data) {
				return [200, {
					responseInfo: RI_SUCCESS
				}, {}];
		});

		$httpBackend.whenGET(/users\/lookup/)
			.respond(function (method, url, params) {

				var match = /\?(.*)/.exec(url),
					query = match[1],
					email = getQueryVar(query,'email');

				if ( email.toLowerCase() == 'registered@salmon.com' ) {

					var responseInfo = {
						code: 0,
						desc: "User exists"
					};
				} else {

					var responseInfo = {
						code: 414,
						desc: "User not found"
					};
				}

				return [200, {
					responseInfo: responseInfo
				}, {}];
			});

	};

	var orderMocks = function() {

		var addOrderToUserOrders = function( orderNumber ) {
			var order = ordersMocks.getOrder( orderNumber );
			userOrders.push( order );
		}

		// Get user's cars
		$httpBackend.whenGET(apiUrl + 'orders')
			.respond(function(method, url, data) {

				var httpStatus = 200;
				var response = {
					responseInfo: {
					  code: 0,
					  desc: 'Success'
					},
					listInfo: {
					  pageSize: 5,
					  pageNumber: 2,
					  totalCount: 24,
					  totalPages: 5
					},
					data: userOrders
				};
				return [httpStatus, response, {}];
			});

		$httpBackend.whenPOST(apiUrl + 'orders')
			.respond(function(method, url, data) {

				data = angular.fromJson(data);

				var response = {},
					httpStatus = 200,
					orderNumber = data.orderNumber;

				response = {
					responseInfo: RI_SUCCESS,
					data: { orderNumber: orderNumber }
				};

				addOrderToUserOrders( orderNumber );

				return [httpStatus, response, {}];
			});

		$httpBackend.whenGET(/orders\/search/)
			.respond(function(method, url) {

				var response = {},
					httpStatus = 200,
					match = /\?(.*)/.exec(url),
					query = match[1],
					orderNumber = getQueryVar(query,'orderNumber');

				if ( orderNumber.match(/^2222222[1-9]$/) || orderNumber === '22223333' ) {

					var order = ordersMocks.getOrder( orderNumber );

					// Valid order number
					response = {
						responseInfo: RI_SUCCESS,
						data: {
							orderNumber: orderNumber,
							make: order.vehicleData.make,
							model: order.vehicleData.model, // 'A6 Coupe',
							modelVariant: order.vehicleData.modelVariant, // '3.0 TDI quattro S Tronic',
							exteriorColour: order.vehicleData.exteriorColour //'Ice Blue'
						}
					};

					// Order already added to account
					if ( orderNumber === '22223333' ) {
						response.responseInfo = {
							code: 306,
							desc: 'Order already added to account'
						}
					};

				} else if ( orderNumber === '22229999' ) {
					// Error
					httpStatus = 500;
					response = {
						responseInfo: {
							code: 999,
							desc: 'Unknown error'
						},
					};
				} else {
					// Any other order number
					httpStatus = 404;
					response = {
						responseInfo: {
							code: 407,
							desc: 'Order not found'
						},
					};
				}

				return [httpStatus, response, {}];
		});


		// GET ORDER
		var orderNumberRegx = new RegExp(apiUrl + 'orders/\[A-Za-z0-9]');
		$httpBackend.whenGET(orderNumberRegx)
		.respond(function (method, url, data) {

			var orderNumber = url.replace(apiUrl+"orders/", "");
			var httpStatus = 200;
			var response = {
				responseInfo: RI_SUCCESS,
				data: ordersMocks.getOrder( orderNumber )
			};
			return [httpStatus, response, {}];
		});


	};

	var setupVehicleMocks = function() {

		var addCarToUserVehicles = function( carReg ) {
			var vehicle = vehiclesMocks.getVehicle( carReg );
			userVehicles.unshift( vehicle );
		}

		var removeCarFromUserVehicles = function( carReg ) {
			for (var i = userVehicles.length - 1; i >= 0; i--) {
				if ( userVehicles[i].carReg === carReg ) {
					userVehicles.splice(i);
				}
			};
		}

		// Get user's cars
		$httpBackend.whenGET(apiUrl + 'vehicles')
			.respond(function(method, url, data) {

				var httpStatus = 200;
				var response = {
					responseInfo: {
					  code: 0,
					  desc: 'Success'
					},
					listInfo: {
					  pageSize: 5,
					  pageNumber: 2,
					  totalCount: 24,
					  totalPages: 5
					},
					data: userVehicles
				};
				return [httpStatus, response, {}];
			});

		// Add car
		$httpBackend.whenPOST(apiUrl + 'vehicles')
			.respond(function(method, url, data) {

				var response = {},
					httpStatus = 200;

				data = angular.fromJson(data);

				var key = data.vehicleKey.toUpperCase();
				data.postcode = data.postcode.toUpperCase();

				if (
					vehiclesMocks.isDemoCar( key ) ||
					key === 'VALID' ||
					( data.ownerType === 'K' && key === 'VOPP1') ||
					( data.ownerType === 'K' && key === 'VOPP0' && data.postcode === 'WD17 1DA' ) ||
					( data.ownerType === 'D' && key === 'VOCD0')
				) {

					addCarToUserVehicles( key );

					httpStatus = 200;
					response = {
						responseInfo: {
							code: 0,
							desc: 'success'
						}
					};

				} else if ( key === 'VOPP0' || key === 'VOPP1' || key === 'VOCD0' || key === 'VOCD1' ) {

					httpStatus = 400;

					response = {
						responseInfo: {
							code: 304,
							desc: 'Vehicle already has an owner'
						},
						data: {
							vehicleKey: key,
							ownerType: 'O'
						}
					};

					// VOPP0 - vehicle is owned by another person, with a different postcode
					if ( key === 'VOPP0' ) {
						response.data.ownerClass = 'P';
						response.data.postcodeMatch = false;
					}

					// VOPP1 - vehicle is owned by another person, with the same postcode
					if ( key === 'VOPP1' ) {
						response.data.ownerClass = 'P';
						response.data.postcodeMatch = true;
					}

					// VOCD0 - vehicle is owned by a company, and there are no drivers registered to it
					if ( key === 'VOCD0' ) {
						response.data.ownerClass = 'C';
						response.data.hasDrivers = false;
					}

					// VOCD1 - vehicle is owned by a company, and there is a driver(s) registered to it already
					if ( key === 'VOCD1' ) {
						response.data.ownerClass = 'C';
						response.data.hasDrivers = true;
					}

				} else {
					httpStatus = 500;
					response = {
						responseInfo: {
							code: 999,
							desc: 'Unknown error'
						}
					};
					}

				return [httpStatus, response, {}];

			});

		// Car lookup
		$httpBackend.whenGET(/vehicles\/search/)
			.respond(function(method, url) {

				var response = {},
					httpStatus = 200,
					match = /\?(.*)/.exec(url),
					query = match[1],
					carReg = getQueryVar(query,'carReg'),
					completeSearch = getQueryVar(query,'completeSearch') === 'true' ? true : false;

				carReg = carReg.toUpperCase().replace('+', ' ');

				if ( carReg === 'V1SEARCH' ) {
					completeSearch = true;
				}

				if ( carReg === 'ERROR' ) {

					httpStatus = 500;
					response = {
						responseInfo: {
							code: 999,
							desc: 'Unknown error'
						}
					};

				} else if ( vehiclesMocks.isDemoCar( carReg ) ) {

					httpStatus = 200;
					response = {
						responseInfo: {
							code: 0,
							desc: 'success'
						},
						data: vehiclesMocks.getDataForVehicleSearch( carReg )
					};

				} else if ( carReg.match(/^V/) !== null || carReg === 'ONACC' ) {

					httpStatus = 200;
					response = {
						responseInfo: {
							code: 0,
							desc: 'success'
						},
						data: {
							completeSearch: false,
							onAccount: false,
							carReg: 'AUD1',
							vehicleKey: '12345678',
							vin: 'FYDHGF756DF',
							make: 'Audi',
							model: 'A6',
							modelVariant: '3.0 TDI quattro',
							modelYear: 2010,
							exteriorColour: 'Metallic Blue',
							engineSize: 3.0
						}
					};

					response.data.completeSearch = completeSearch;

					if ( carReg == 'VALID' ) {
						response.data.vehicleKey = carReg;
					} else if ( carReg === 'ONACC') {
						response.data.onAccount = true;
					} else if ( carReg === 'V2NDDIFF' ) {
						response.data.vehicleKey = 'VALID';
						if ( completeSearch ) {
							response.data.exteriorColour = 'Metallic Red';
						}
					} else if ( carReg === 'VOPP0') {
						// VOPP0 - vehicle is owned by another person, with a different postcode
						response.data.vehicleKey = 'VOPP0';
					} else if ( carReg === 'VOPP1') {
						// VOPP1 - vehicle is owned by another person, with the same postcode
						response.data.vehicleKey = 'VOPP1';
					} else if ( carReg === 'VOCD0') {
						// VOCD0 - vehicle is owned by a company, and there are no drivers registered to it
						response.data.vehicleKey = 'VOCD0';
					} else if ( carReg === 'VOCD1') {
						// VOCD1 - vehicle is owned by a company, and there is a driver(s) registered to it already
						response.data.vehicleKey = 'VOCD1';
					} else {

						httpStatus = 404;
						response = {
							responseInfo: {
								code: 404,
								desc: 'Vehicle not found'
							}
						};
					}

				} else {

					httpStatus = 404;
					response = {
						responseInfo: {
							code: 404,
							desc: 'Vehicle not found'
						}
					};
				}

				return [httpStatus, response, {}];
			});

		// POST Update - Mileage, Preferred Dealer Centre, Pervious Car
		$httpBackend.whenPOST(/\/vehicles\/[A-Za-z0-9 ]*\/update$/)
			.respond(function (method, url, data) {

				data = angular.fromJson(data);

				if ( data.setToPrevious === true ) {
					removeCarFromUserVehicles(data.vehicleKey);
				}

				return [200, {
					responseInfo: RI_SUCCESS
				}, {}];
			});

	};

	var messagesMocks = function(){

		// get unread messages count
		$httpBackend.whenGET(apiUrl + "messages?filter=UNREAD")
			.respond(function (method, url, data) {
				data = angular.fromJson(data);

				var data = {
							"pageSize": 10,
							"pageNumber": 1,
							"totalCount": 24,
							"totalPages": 3
						  };

				return [200, {
					responseInfo: RI_SUCCESS,
					listInfo: data
				}, {}];
			});

		// get messages page 1
		$httpBackend.whenGET(apiUrl + "messages?pageNumber=1")
			.respond(function (method, url, data) {

			data = angular.fromJson(data);

			if(typeof data.totalCount==="undefined"){
						data = {
								"pageSize": 10,
								"totalCount": 24,
								"pageNumber": 1,
								"totalPages": 3
							  };
				}
			var messages = [];
			var i = 0;
			var responseLimit = data.pageSize;

			if(data.pageNumber==3) {
				responseLimit = 5
			}

			for (i = 0; i < responseLimit; i++) { 
				messages.push({
					"messageId": 1,
					"messageDateTime": "2014-08-11T11:04:13+0100",
					"title": "TEST MESSAGE " + ((data.pageNumber - 1) * 10 + i + 1),
					"message": "Account Created - Your account has been created",
					"eventCode": "AC",
					"viewed": Math.random() < 0.5 ? true : false,
					"flagged": Math.random() < 0.5 ? true : false,
					"deleted": false,
					"messageTypeCode": "SYS"
				  });
				}

				return [200, {
					responseInfo: RI_SUCCESS,
					listInfo:data,
					data: messages
				}, {}];
			});

		// get messages page 2
		$httpBackend.whenGET(apiUrl + "messages?pageNumber=2")
			.respond(function (method, url, data) {

			data = angular.fromJson(data);

			if(typeof data.totalCount==="undefined"){
						data = {
								"pageSize": 10,
								"totalCount": 24,
								"pageNumber": 2,
								"totalPages": 3									
							  };
				}
			var messages = [];
			var i = 0;
			var responseLimit = data.pageSize;

			if(data.pageNumber==3) {
				responseLimit = 5
			}

			for (i = 0; i < responseLimit; i++) { 
				messages.push({
							"messageId": 1,
							"messageDateTime": "2014-08-11T11:04:13+0100",
							"title": "TEST MESSAGE " + ((data.pageNumber - 1) * 10 + i + 1),
							"message": "Account Created - Your account has been created",								
							"eventCode": "AC",
							"viewed": Math.random() < 0.5 ? true : false,
							"flagged": Math.random() < 0.5 ? true : false,
							"deleted": false,
							"messageTypeCode": "SYS"
						  });
				}

				return [200, {
					responseInfo: RI_SUCCESS,
					listInfo:data,
					data: messages
				}, {}];
			});

		// get messages page 3
		$httpBackend.whenGET(apiUrl + "messages?pageNumber=3")
			.respond(function (method, url, data) {

			data = angular.fromJson(data);

			if(typeof data.totalCount==="undefined"){
						data = {
								"pageSize": 10,
								"totalCount": 24,
								"pageNumber": 2,
								"totalPages": 3									
							  };
				}
			var messages = [];
			var i = 0;
			var responseLimit = data.pageSize;

			if(data.pageNumber==3) {
				responseLimit = 5
			}

			for (i = 0; i < responseLimit; i++) { 
				messages.push({
							"messageId": 1,
							"messageDateTime": "2014-08-11T11:04:13+0100",
							"title": "TEST MESSAGE " + ((data.pageNumber - 1) * 10 + i + 1),
							"message": "Account Created - Your account has been created",								
							"eventCode": "AC",
							"viewed": Math.random() < 0.5 ? true : false,
							"flagged": Math.random() < 0.5 ? true : false,
							"deleted": false,
							"messageTypeCode": "SYS"
						  });
				}

				return [200, {
					responseInfo: RI_SUCCESS,
					listInfo:data,
					data: messages
				}, {}];
			});

		// get message	
		$httpBackend.whenGET(apiUrl + "messages/1")
			.respond(function (method, url, data) {
				data = angular.fromJson(data);

				var data = {
							"messageId": 1,
							"messageDateTime": "2014-08-11T11:04:13+0100",
							"title": "TEST MESSAGE - Title is back from service",
							"message": "Account Created - Your account has been created - Message is back from service",
							"eventCode": "AC",
							"viewed": true,
							"flagged": false,
							"deleted": false,
							"messageTypeCode": "SYS"
						  };

				return [200, {
					responseInfo: RI_SUCCESS,
					data: data
				}, {}];
			});

		// flagged post
		$httpBackend.whenPOST(apiUrl + "messages/1/flag")
			.respond(function (method, url, data) {
				return [200, {
					responseInfo: RI_SUCCESS
				}, {}];
			});

		// flagged delete
		$httpBackend.whenDELETE(apiUrl + "messages/1/flag")
			.respond(function (method, url, data) {
				return [200, {
					responseInfo: RI_SUCCESS
				}, {}];
			});

		// delete message
		$httpBackend.whenDELETE(apiUrl + "messages/1")
			.respond(function (method, url, data) {
				return [200, {
					responseInfo: RI_SUCCESS
				}, {}];
			});

		/* delete multiple messages - comma seperated
		$httpBackend.whenDELETE(apiUrl + "messages/1,1,1,1,1,1,1,1,1,1")
			.respond(function (method, url, data) {
				return [200, {
					responseInfo: RI_SUCCESS
				}, {}];
			});

		// delete [last page] multiple messages - comma seperated
		$httpBackend.whenDELETE(apiUrl + "messages/1,1,1,1,1")
			.respond(function (method, url, data) {
				return [200, {
					responseInfo: RI_SUCCESS,
				}, {}];
			});
		*/

	}

	var communicationPrefObject =	 {
					"sysMsgEmail": true,
					"sysMsgSMS": true,
					"mailOptOut": true,
					"phoneOptOut": false,
					"emailOptOut": false,
					"smsOptOut": true,
					"globalOptOut": false
				};

	var communicationPrefMocks = function() {

		// Communication Preferences

		// GET
		$httpBackend.whenGET(apiUrl + "profile/communications")
			.respond(function (method, url, data) {
				
				return [200, {
					responseInfo: RI_SUCCESS,
					data: communicationPrefObject
				}, {}];
			});

		// put
		$httpBackend.whenPOST(apiUrl + "profile/communications")
			.respond(function (method, url, data) {
				data = angular.fromJson(data);
				angular.extend(communicationPrefObject, data)
				return [200, {
					responseInfo: RI_SUCCESS,
					data: communicationPrefObject
				}, {}];
			});
	}

	var manageCarMocks = function (){

		// GET CAR
		var vehicleKeyRegx = new RegExp(apiUrl + 'vehicles/\[A-Za-z0-9 ]');
		$httpBackend.whenGET(vehicleKeyRegx)
		.respond(function (method, url, data) {

			var vehicleKey = url.replace(apiUrl+"vehicles/", "");
			var httpStatus = 200;
			var response = {
				responseInfo: RI_SUCCESS,
				data: vehiclesMocks.getVehicle( vehicleKey )
			};
			return [httpStatus, response, {}];
		});

		// GET CAR IMAGE
		var carImageRegx = new RegExp(apiUrl + 'images/\[A-Za-z0-9 ]');
		$httpBackend.whenGET(carImageRegx)
		.respond(function (method, url, data) {

			var str = url.split('/');
			var key = str[str.length-1];
			var img = (str[str.length-2] == 'carReg') ? 1 : 0 ;

			var mockImages = {

				'FL64 SNN': [
					'http://cdn.audi.co.uk/visualiser/a1/a1-sportback/6y/pqs/006.png',
					'http://cdn.audi.co.uk/dam/7_days/ModelsThumbs/A1-SB_side.png'
				],
				'VO64 PZF': [
					'http://cdn.audi.co.uk/visualiser/a3/s3-cabriolet/n9/c6h/008.png',
					'http://www.audi.co.uk/content/dam/audi/cdn/7_days/ModelsThumbs/s3-cab-side.png'
				],
				'RK64 TZX': [
					'http://cdn.audi.co.uk/visualiser/a3/a3-saloon/p5/c0j/007.png',
					'http://cdn.audi.co.uk/dam/7_days/ModelsThumbs/A3-Saloon_side.png'
				],
				'S8WCV': [
					'http://cdn.audi.co.uk/visualiser/q7/q7/a1/pq6/006.png',
					'http://cdn.audi.co.uk/dam/7_days/ModelsThumbs/Q7_side.png'
				],

				// Test cars for "add car"
				'VALID': [
					'http://cdn.audi.co.uk/visualiser/a1/a1/n9/c5i/p5/008.png',
					'http://cdn.audi.co.uk/dam/7_days/ModelsThumbs/A1_side.png'
				],
				'VOPP0': [
					'http://cdn.audi.co.uk/visualiser/a8/s8/6y/pqw/007.png',
					'http://cdn.audi.co.uk/dam/7_days/ModelsThumbs/S8_side.png'
				],
				'VOPP1': [
					'http://cdn.audi.co.uk/visualiser/a8/s8/6y/pqw/007.png',
					'http://cdn.audi.co.uk/dam/7_days/ModelsThumbs/S8_side.png'
				],
				'VOCD0': [
					'http://cdn.audi.co.uk/visualiser/a8/s8/6y/pqw/007.png',
					'http://cdn.audi.co.uk/dam/7_days/ModelsThumbs/S8_side.png'
				],
				'VOCD1': [
					'http://cdn.audi.co.uk/visualiser/a8/s8/6y/pqw/007.png',
					'http://cdn.audi.co.uk/dam/7_days/ModelsThumbs/S8_side.png'
				],
				'V1SEARCH': [
					'http://cdn.audi.co.uk/visualiser/a8/s8/6y/pqw/007.png',
					'http://cdn.audi.co.uk/dam/7_days/ModelsThumbs/S8_side.png'
				],
				'V2NDDIFF': [
					'http://cdn.audi.co.uk/visualiser/a8/s8/6y/pqw/007.png',
					'http://cdn.audi.co.uk/dam/7_days/ModelsThumbs/S8_side.png'
				],

				// Order Numbers
				'22222221': [
					'http://cdn.audi.co.uk/visualiser/tt/tts-coupe/e9/cv9/008.png',
					'http://www.audi.co.uk/content/dam/audi/cdn/7_days/ModelsThumbs/tts-side.png'
				],
				'22222222': [
					'http://cdn.audi.co.uk/visualiser/a3/s3-cabriolet/n9/c6h/008.png',
					'http://www.audi.co.uk/content/dam/audi/cdn/7_days/ModelsThumbs/s3-cab-side.png'
				],
				'22222223': [
					'http://cdn.audi.co.uk/visualiser/a5/a5-sportback/n9/pq6/008.png',
					'http://cdn.audi.co.uk/dam/7_days/ModelsThumbs/A5-SB_side.png'
				],
				'22223333': [
					'http://cdn.audi.co.uk/visualiser/a5/a5-sportback/n9/pq6/008.png',
					'http://cdn.audi.co.uk/dam/7_days/ModelsThumbs/A5-SB_side.png'
				],
			};

			var httpStatus = 200;
			var response = {
				responseInfo: RI_SUCCESS,
				data:  {
					url : mockImages[key][img]
				}
			};

			return [httpStatus, response, {}];
		});

	}

	return {
		setupMocks: setupMocks
	};

}]);
