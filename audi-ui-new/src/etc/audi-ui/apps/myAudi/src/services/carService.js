myAudi.factory('carService', ['$resource', '$http', '$q', '$httpBackend', 'apiService',
	function ($resource, $http, $q, $httpBackend, apiService) {

		/* ************************************
			CARS
		************************************** */

		var car = {};
		var cars = [];

		var getCar = function (vehicleKey) {
			return new apiService.withErrorHandling({
				title: 'getCar',
				url: 'vehicles/'+vehicleKey,
				method: 'GET'
			}).then( function(data) {
				angular.copy(data.data,car);
			});
		};

		var getCars = function () {

			return new apiService.withErrorHandling({
				title: 'Get user cars',
				url: 'vehicles',
				method: 'GET'
			}).then( function(response) {
				angular.copy(response.data,cars);
			});
		};

		var updateCar = function(data) {

			var query;

			if(data.miles !== "undefined") {
				// update miles...
				query = "lastMileage="+data.miles;
			} else if(data.preferredDealerCode !== "undefined") {
				// update preffred location.
				query = "preferredDealerCode="+data.preferredDealerCode;
			} else if(data.setToPrevious !== "undefined") {
				// update as previous car.
				query = "setToPrevious="+data.setToPrevious;
			} else if(query === undefined) {
				return;
			}

			return new apiService.withErrorHandling({
				title: 'updateMileage',
				url: "vehicles/"+data.vehicleKey +"/update",
				method: 'POST',
				data : data
			});
		};

		// Temporary function to handle setting a car to previous
		var setToPreviousTMP = function( data ) {
			console.log('>>>>>> called setToPreviousTMP with data =',data);

			return new apiService.withErrorHandling({
				title: 'updateMileage',
				url: "vehicles/"+data.vehicleKey +"/update",
				method: 'POST',
				data : data
			});
		};

		var carSearch = function( carReg, postcode, completeSearch ) {

			return new apiService.withErrorHandling({
				title: 'Car Search',
				url: 'vehicles/search',
				method: 'GET',
				params: {
					carReg: carReg,
					postcode: postcode,
					completeSearch: completeSearch
				}
			});
		};

		var addCarToAccount = function( vehicleKey, postcode, ownerType ) {

			return new apiService.withErrorHandling({
				title: 'Add car to account',
				url: 'vehicles',
				method: 'POST',
				data: {
					vehicleKey: vehicleKey,
					ownerType: ownerType,
					postcode: postcode
				}
			});
		};


		/* ************************************
			ORDER
		************************************** */

		var order = {};
		var orders = [];

		var getOrder = function (orderNumber) {

			return new apiService.withErrorHandling({
				title: 'Get user orders',
				url: 'orders/'+orderNumber,
				method: 'GET'
			}).then( function(response) {
				angular.copy(response.data,order);
			});
		};

		var getOrders = function () {

			return new apiService.withErrorHandling({
				title: 'Get user orders',
				url: 'orders',
				method: 'GET'
			}).then( function(response) {
				angular.copy(response.data,orders);
			});
		};

		var orderSearch = function( orderNumber, postcode ) {

			return new apiService.withErrorHandling({
				title: 'Order Search',
				url: 'orders/search',
				method: 'GET',
				params: {
					orderNumber: orderNumber,
					postcode: postcode
				}
			});
		};

		var addOrderToAccount = function( orderNumber ) {

			return new apiService.withErrorHandling({
				title: 'Add order to account',
				url: 'orders',
				method: 'POST',
				data: { orderNumber: orderNumber }
			});
		};

		var getImage = function (key, value) {
			return new apiService.withErrorHandling({
				title: 'Get Image',
				url: 'images/'+key+'/'+value,
				method: 'GET'
			});
		}


		var getImageWithCarReg = function (key, value) {

			return new apiService.withErrorHandling({
				title: 'Get Image',
				url: '/audi-mediaservice/services/media/v1/image/external/view/04/type/PNG/regno/'+value,
				method: 'GET'
			});
		}


		return {
			//car
			car: car,
			cars: cars,
			getCar: getCar,
			getCars: getCars,
			updateCar : updateCar,
			carSearch: carSearch,
			addCarToAccount: addCarToAccount,
			setToPreviousTMP: setToPreviousTMP,
			// order
			order: order,
			orders: orders,
			getOrder: getOrder,
			getOrders: getOrders,
			orderSearch: orderSearch,
			addOrderToAccount: addOrderToAccount,
			// media services
			getImage: getImage
		}
	}
]);
