myAudi.factory('vehiclesMocks', [function() {

	var equipmentList = [{
		'name': 'Standard Equipment',
		'salesFamilies': [{
			'name': 'Technology',
			'options': [{
				'prCode': '7AL',
				'pnrCode': 'MEDW7AL',
				'pnrDescription': 'Anti-theft alarm',
				'pnrFamilyCode': 'EDW',
				'pnrFamilyDescription': 'Anti-theft protection',
				'price': 154.99,
				'imageURl': 'www.images.com/fkfgfgdfkg'
			}, {
				'prCode': '',
				'pnrCode': '',
				'pnrDescription': '',
				'pnrFamilyCode': '',
				'pnrFamilyDescription': '',
				'price': 154.99,
				'imageURl': 'www.images.com/fkfgfgdfkg'
			}],
			'packages': [{
				'packageCode': 'BBB',
				'packageKey': '',
				'packageDescription': 'Entertainment Package',
				'imageUrl': '',
				'price': 159.99,
				'options': [{
					'prCode': '7AL',
					'pnrCode': 'BFAW7AL',
					'pnrDescription': 'Anti-theft alarm',
					'pnrFamilyCode': 'EDW',
					'pnrFamilyDescription': 'Anti-theft protection',
					'price': 154.99,
					'imageURl': 'www.images.com/fkfgfgdfkg'
				}, {
					'prCode': '',
					'pnrCode': '',
					'pnrDescription': '',
					'pnrFamilyCode': '',
					'pnrFamilyDescription': '',
					'price': 154.99,
					'imageURl': 'www.images.com/fkfgfgdfkg'
				}]
			}]
		}, {
			'name': 'Telephone and Communications',
			'options': [{
				'prCode': '',
				'pnrCode': '',
				'pnrDescription': '',
				'pnrFamilyCode': '',
				'pnrFamilyDescription': '',
				'price': 154.99,
				'imageURl': 'www.images.com/fkfgfgdfkg'
			}],
			'packages': [{}]
		}]
	}, {
		'name': 'Optional extras',
		'salesFamilies': [{
			'name': 'Technology',
			'options': [{
				'prCode': '7AL',
				'pnrCode': 'MEDW7AL',
				'pnrDescription': 'Anti-theft alarm',
				'pnrFamilyCode': 'EDW',
				'pnrFamilyDescription': 'Anti-theft protection',
				'price': 154.99,
				'imageURl': 'www.images.com/fkfgfgdfkg'
			}, {
				'prCode': '',
				'pnrCode': '',
				'pnrDescription': '',
				'pnrFamilyCode': '',
				'pnrFamilyDescription': '',
				'price': 154.99,
				'imageURl': 'www.images.com/fkfgfgdfkg'
			}],
			'packages': [{
				'packageCode': 'BBB',
				'packageKey': '',
				'packageDescription': 'Entertainment Package',
				'imageUrl': '',
				'price': 159.99,
				'options': [{
					'prCode': '7AL',
					'pnrCode': 'BFAW7AL',
					'pnrDescription': 'Anti-theft alarm',
					'pnrFamilyCode': 'EDW',
					'pnrFamilyDescription': 'Anti-theft protection',
					'price': 154.99,
					'imageURl': 'www.images.com/fkfgfgdfkg'
				}, {
					'prCode': '',
					'pnrCode': '',
					'pnrDescription': '',
					'pnrFamilyCode': '',
					'pnrFamilyDescription': '',
					'price': 154.99,
					'imageURl': 'www.images.com/fkfgfgdfkg'
				}]
			}]
		}, {
			'name': 'Telephone and Communications',
			'options': [{
				'prCode': '',
				'pnrCode': '',
				'pnrDescription': '',
				'pnrFamilyCode': '',
				'pnrFamilyDescription': ''
			}],
			'packages': [{}]
		}]
	}];


	// Demo data
	// Dummy reg numbers: FL64 SNN, VO64 PZF, RK64 TZX, S8WCV
	// ---------------------------------------------------------

	var demoCars = {

		'FL64 SNN': {
			'vehicleKey': 'FL64 SNN',
			'vin': 'WAUZZZ8X1EB148878',
			'carReg': 'FL64 SNN',
			"make": "Audi",
	        "model": "A1 Sportback",
	        "modelVariant": "SE 1.6 TDI",
	        "modelVersion": "2",
	        "modelYear": "2014",
	        "vehicleLongDescription": "A1 Sportback 1.6 TDI 5 speed",
	        "exteriorColour": "Scuba Blue, metallic",
	        "interiorDescription": "black/black-black/black/titanium grey",
	        "warrantyStartDate": "2014-10-03T00:00:00+0100",
	        "warrantyEndDate": "2017-10-01T00:00:00+0100",
	        "lastMileage": "8848",
	        "engineNumber": "CAY U86019",
	        "fuelType": "Diesel",
	        "engineSize": "1598",
	        "registrationDate": "2014-10-03T00:00:00+0100",
	        "engineName": "1.6 TDI",
	        "acceleration": "11.9",
	        "power": "105",
	        "maxPower": "63 (86)/4800",
	        "topSpeed": "112",
	        "maxTorque": "160/1500-3500",
	        "drive": "Front-wheel drive",
	        "fuelTankCapacity": "45",
	        "grossVehicleWeight": "1555",
	        "unladenWeight": "1065",
	        "gear": "5 speed",
	        "trimLine": "SE",
	        "bikRate": "19",
	        "fuelConsumptionUrban": "45.6",
	        "fuelConsumptionRural": "64.2",
	        "fuelConsumptionAverage": "74.3",
	        "co2EmissionsAverage": "118",
	        "audi": true,
	        "preferredDealerCode" : "022",
    		"preferredDealerName" : "Finchley Road Audi",
	        'ownershipStartDate': '2014-09-20',
			'equipmentList': angular.copy(equipmentList)
		},

		'VO64 PZF': {
			'vehicleKey': 'VO64 PZF',
			'vin': 'WAUZZZ8V7F1052128',
			'carReg': 'VO64 PZF',
	        "make": "Audi",
	        "model": "S3 Cabriolet",
	        "modelVariant": "2.0 TFSI quattro",
	        "modelVersion": "0",
	        "modelYear": "2015",
	        "vehicleLongDescription": "S3 Cabriolet 2.0 TFSI quattro S tronic",
	        "exteriorColour": "Misano Red, pearl effect Black",
	        "interiorDescription": "black-lunar silver/black-black/black/ black",
	        "warrantyStartDate": "2014-09-30T00:00:00+0100",
	        "warrantyEndDate": "2017-09-28T00:00:00+0100",
	        "lastMileage": "2848",
	        "engineNumber": "CJX 034515",
	        "fuelType": "Petrol",
	        "engineSize": "1984",
	        "registrationDate": "2014-09-30T00:00:00+0100",
	        "engineName": "2.0 TFSI quattro",
	        "acceleration": "5.4",
	        "power": "300",
	        "maxPower": "221 (300)/5500-6200",
	        "topSpeed": "155",
	        "maxTorque": "380/1800-5500",
	        "drive": "quattro",
	        "fuelTankCapacity": "55",
	        "grossVehicleWeight": "2060",
	        "unladenWeight": "1620",
	        "gear": "S tronic",
	        "trimLine": "SE",
	        "bikRate": "26",
	        "fuelConsumptionUrban": "31.4",
	        "fuelConsumptionRural": "47.1",
	        "fuelConsumptionAverage": "39.8",
	        "co2EmissionsAverage": "165",
	        "audi": true,
	        "preferredDealerCode" : "022",
    		"preferredDealerName" : "Finchley Road Audi",
	        'ownershipStartDate': '2014-10-25',
			'equipmentList': angular.copy(equipmentList)
		},

		'RK64 TZX': {
			'vehicleKey': 'RK64 TZX',
			'vin': 'WAUZZZ8V3F1050053',
			'carReg': 'RK64 TZX',
			"make": "Audi",
	        "model": "A3 Saloon",
	        "modelVariant": "Sport 1.6 TDI",
	        "modelVersion": "0",
	        "modelYear": "2015",
	        "vehicleLongDescription": "A3 Saloon 1.6 TDI S tronic",
	        "exteriorColour": "Glacier White, metallic",
	        "interiorDescription": "silver-black/blakc/black/silver",
	        "warrantyStartDate": "2014-09-22T00:00:00+0100",
	        "warrantyEndDate": "2017-09-20T00:00:00+0100",
	        "lastMileage": "null",
	        "engineNumber": "CRK 222958",
	        "fuelType": "Diesel",
	        "engineSize": "1598",
	        "registrationDate": "2014-09-22T00:00:00+0100",
	        "engineName": "1.6 TDI",
	        "acceleration": "10.7",
	        "power": "110",
	        "maxPower": "81 (110)/3200-4000",
	        "topSpeed": "126",
	        "maxTorque": "250/1500-3000",
	        "drive": "Front-wheel drive",
	        "fuelTankCapacity": "50",
	        "grossVehicleWeight": "1840",
	        "unladenWeight": "1290",
	        "gear": "S tronic",
	        "trimLine": "Sport",
	        "bikRate": "16",
	        "fuelConsumptionUrban": "64.2",
	        "fuelConsumptionRural": "78.5",
	        "fuelConsumptionAverage": "72.4",
	        "co2EmissionsAverage": "102",
	        "audi": true,
	        "preferredDealerCode" : "022",
    		"preferredDealerName" : "Finchley Road Audi",
	        'ownershipStartDate': '2014-11-01',
			'equipmentList': angular.copy(equipmentList)
		},

		'S8WCV': {
			'vehicleKey': 'S8WCV',
			'vin': 'JMZGG14F601657489',
			'carReg': 'S8WCV',
			"make": "Audi",
	        "model": "S8",
	        "modelVariant": "4.0 TFSI quattro",
	        "modelVersion": "0",
	        "modelYear": "2015",
	        "vehicleLongDescription": "S8 4.0 TFSI quattro tiptronic",
	        "exteriorColour": "Phantom Black, pearl effect",
	        "interiorDescription": "blk/blk/blk-lnr slv",
	        "warrantyStartDate": "2014-06-24T00:00:00+0100",
	        "warrantyEndDate": "2017-06-22T00:00:00+0100",
	        "extendedWarrantyStartDate": "2017-06-23T00:00:00+0100",
	        "extendedWarrantyEndDate": "2019-06-22T00:00:00+0100",
	        "lastMileage": "2288",
	        "engineNumber": "CTF 001937",
	        "fuelType": "Petrol",
	        "engineSize": "3993",
	        "registrationDate": "2014-06-24T00:00:00+0100",
	        "engineName": "4.0 TFSI quattro",
	        "acceleration": "4.1",
	        "power": "520",
	        "maxPower": "382 (520)/5800",
	        "topSpeed": "155",
	        "maxTorque": "650/1700-5500",
	        "drive": "quattro",
	        "fuelTankCapacity": "82",
	        "grossVehicleWeight": "2590",
	        "unladenWeight": "1990",
	        "gear": "tiptronic",
	        "trimLine": "SE",
	        "bikRate": "35",
	        "fuelConsumptionUrban": "20.8",
	        "fuelConsumptionRural": "38.7",
	        "fuelConsumptionAverage": "29.4",
	        "co2EmissionsAverage": "225",
	        "audi": true,
	        "preferredDealerCode" : "022",
    		"preferredDealerName" : "Finchley Road Audi",
	        'ownershipStartDate': '2008-01-12',
			'equipmentList': angular.copy(equipmentList)
		},

	};


	// Test car template
	// ------------------

	var testCar = {
		'vehicleKey': 'VALID',
		'vin': 'JMZGG14F601657489',
		'carReg': 'VALID',
		'isAudi': true,
		'make': 'Audi',
		'model': 'Audi TT',
		'modelVariant': '',
		'modelYear': '2012',
		'exteriorColour': 'Blue',
		'interiorDescription': '',
		'commissionNumber': '',
		'commissionYear': '',
		'vehicleLongDescription': '3.0 TDI quattro',
		'ownershipStartDate': null,
		'ownershipEndDate': null,
		'intendedReplacementDate': '2015-01-01',
		'warrantyStartDate': '2012-03-25',
		'warrantyEndDate': '2015-03-25',
		'extendedWarrantyStartDate': '',
		'extendedWarrantyEndDate': '',
		'lastMileage': null,
		'lastMileageDate': null,
		'engineNumber': 'LF872584',
		'chassisNumber': '',
		'fuelType': 'Petrol',
		'engineSize': 2100,
		'registrationDate': '2012-02-01',
		'frontAxleWeight': '250',
		'rearAxleWeight': '650',
		'engineName': '',
		'acceleration': '',
		'power': '',
		'maxPower': '',
		'topSpeed': '',
		'maxTorque': '',
		'drive': '',
		'fuelTankCapacity': '',
		'grossVehicleWeight': '',
		'unladenWeight': '',
		'gear': '',
		'trimLine': '',
		'bikRate': '',
		'feulConsumptionUrban': '28.3',
		'feulConsumptionRural': '32.1',
		'feulConsumptionAverage': '29.7',
		'co2EmissionsUrban': 160,
		'co2EmissionsRural': 145,
		'co2EmissionsAverage': 153,
		'preferredDealerCode': null,
		'preferredDealerName': null,
		'equipmentList': angular.copy(equipmentList)
	};


	// Test cars for the "add car" user journeys
	// -----------------------------------------

	var testCars = {
		VALID: angular.extend(angular.copy(testCar), { vehicleKey: 'VALID', carReg: 'VALID' }),
		VOPP0: angular.extend(angular.copy(testCar), { vehicleKey: 'VOPP0', carReg: 'VOPP0' }),
		VOPP1: angular.extend(angular.copy(testCar), { vehicleKey: 'VOPP1', carReg: 'VOPP1' }),
		VOCD0: angular.extend(angular.copy(testCar), { vehicleKey: 'VOCD0', carReg: 'VOCD0' }),
		VOCD1: angular.extend(angular.copy(testCar), { vehicleKey: 'VOCD1', carReg: 'VOCD1' }),
		ONACC: angular.extend(angular.copy(testCar), { vehicleKey: 'ONACC', carReg: 'ONACC' }),
		V1SEARCH: angular.extend(angular.copy(testCar), { vehicleKey: 'V1SEARCH', carReg: 'V1SEARCH' }),
		V2NDDIFF: angular.extend(angular.copy(testCar), { vehicleKey: 'V2NDDIFF', carReg: 'V2NDDIFF' })
	};


	var getVehicles = function() {
		return demoCars;
	};

	var getVehicle = function(carReg) {
		if (typeof demoCars[carReg] !== 'undefined') {
			return demoCars[carReg];
		} else if (typeof testCars[carReg] !== 'undefined') {
			return testCars[carReg];
		} else {
			return false;
		}
	};

	var isDemoCar = function(carReg) {
		return (typeof demoCars[carReg] !== 'undefined') ? true : false;
	};

	var getDataForVehicleSearch = function(carReg) {

		var car = getVehicle(carReg);

		return {
			completeSearch: false,
			onAccount: false,
			carReg: carReg,
			vehicleKey: car.vehicleKey,
			vin: car.vin,
			make: car.make,
			model: car.model,
			modelVariant: car.vehicleLongDescription,
			modelYear: car.modelYear,
			exteriorColour: car.exteriorColour,
			engineSize: car.engineSize
		};
	};

	return {
		getVehicles: getVehicles,
		getVehicle: getVehicle,
		isDemoCar: isDemoCar,
		getDataForVehicleSearch: getDataForVehicleSearch
	};

}]);
