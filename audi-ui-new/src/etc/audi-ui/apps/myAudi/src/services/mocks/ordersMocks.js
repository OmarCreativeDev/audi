myAudi.factory('ordersMocks', [ function () {

	var orders = {

		'22222222': {
			'orderNumber': '22222222',
			'orderStatus': '50',
			'handoverDate': '',
			'deliveryDealerName': '',
			'deliveryDealerCode': '',
			'orderRetailerName': '',
			'orderRetailerCode': '',
			'createDate': '',
			'modelCode': '6XGT73',
			'modelYear': '2014',
			'modelVersion': '1',
			'exteriorCode': 'y2y2',
			'interiorCode': 'be',
			'translatedOptions': [
				'OFB',
				'FFD',
				'RET',
				'QAS'
			],
			'orderHistory': [{
				'orderStatus': '20',
				'statusDate': '2014-09-20'
			}, {
				'orderStatus': '30',
				'statusDate': '2014-09-22'
			}, {
				'orderStatus': '40',
				'statusDate': '2014-09-24'
			}, {
				'orderStatus': '50',
				'statusDate': '2014-09-28'
			}],
			'vehicleData': {
				'vehicleKey': "VO64 PZF",
				"vin": "WAUZZZ8V7F1052128",
		        "carReg": "VO64 PZF",
		        "make": "Audi",
		        "model": "S3 Cabriolet",
		        "modelVariant": "2.0 TFSI quattro",
		        "modelVersion": "0",
		        "modelYear": "2015",
		        "vehicleLongDescription": "S3 Cabriolet 2.0 TFSI quattro S tronic",
		        "exteriorColour": "Misano Red, pearl effect Black",
		        "interiorDescription": "black-lunar silver/black-black/black/ black",
		        "warrantyStartDate": "2014-09-30T00:00:00+0100",
		        "warrantyEndDate": "2017-09-28T00:00:00+0100",
		        "lastMileage": "null",
		        "engineNumber": "CJX 034515",
		        "fuelType": "Petrol",
		        "engineSize": "1984",
		        "registrationDate": "2014-09-30T00:00:00+0100",
		        "engineName": "2.0 TFSI quattro",
		        "acceleration": "5.4",
		        "power": "300",
		        "maxPower": "221 (300)/5500-6200",
		        "topSpeed": "155",
		        "maxTorque": "380/1800-5500",
		        "drive": "quattro",
		        "fuelTankCapacity": "55",
		        "grossVehicleWeight": "2060",
		        "unladenWeight": "1620",
		        "gear": "S tronic",
		        "trimLine": "SE",
		        "bikRate": "26",
		        "fuelConsumptionUrban": "31.4",
		        "fuelConsumptionRural": "47.1",
		        "fuelConsumptionAverage": "39.8",
		        "co2EmissionsAverage": "165",
		        "audi": true,
				'equipmentList': [{
					'name': 'Standard Equipment',
					'salesFamilies': [{
						'name': 'Technology',
						'options': [{
							'prCode': '7AL',
							'pnrCode': 'MEDW7AL',
							'pnrDescription': 'Anti-theft alarm',
							'pnrFamilyCode': 'EDW',
							'pnrFamilyDescription': 'Anti-theft protection',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}, {
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': '',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}],
						'packages': [{
							'packageCode': 'BBB',
							'packageKey': '',
							'packageDescription': 'Entertainment Package',
							'imageUrl': '',
							'price': 159.99,
							'options': [{
								'prCode': '7AL',
								'pnrCode': 'BFAW7AL',
								'pnrDescription': 'Anti-theft alarm',
								'pnrFamilyCode': 'EDW',
								'pnrFamilyDescription': 'Anti-theft protection',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}, {
								'prCode': '',
								'pnrCode': '',
								'pnrDescription': '',
								'pnrFamilyCode': '',
								'pnrFamilyDescription': '',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}]
						}]
					}, {
						'name': 'Telephone and Communications',
						'options': [{
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': '',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}],
						'packages': [{}]
					}]
				}, {
					'name': 'Optional extras',
					'salesFamilies': [{
						'name': 'Technology',
						'options': [{
							'prCode': '7AL',
							'pnrCode': 'MEDW7AL',
							'pnrDescription': 'Anti-theft alarm',
							'pnrFamilyCode': 'EDW',
							'pnrFamilyDescription': 'Anti-theft protection',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}, {
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': '',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}],
						'packages': [{
							'packageCode': 'BBB',
							'packageKey': '',
							'packageDescription': 'Entertainment Package',
							'imageUrl': '',
							'price': 159.99,
							'options': [{
								'prCode': '7AL',
								'pnrCode': 'BFAW7AL',
								'pnrDescription': 'Anti-theft alarm',
								'pnrFamilyCode': 'EDW',
								'pnrFamilyDescription': 'Anti-theft protection',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}, {
								'prCode': '',
								'pnrCode': '',
								'pnrDescription': '',
								'pnrFamilyCode': '',
								'pnrFamilyDescription': '',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}]
						}]
					}, {
						'name': 'Telephone and Communications',
						'options': [{
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': ''
						}],
						'packages': [{}]
					}]
				}]
			}
		},

		'22222221': {
			'orderNumber': '22222221',
			'orderStatus': '50',
			'handoverDate': '',
			'deliveryDealerName': '',
			'deliveryDealerCode': '',
			'orderRetailerName': '',
			'orderRetailerCode': '',
			'createDate': '',
			'modelCode': '6XGT73',
			'modelYear': '2014',
			'modelVersion': '1',
			'exteriorCode': 'y2y2',
			'interiorCode': 'be',
			'translatedOptions': [
				'OFB',
				'FFD',
				'RET',
				'QAS'
			],
			'orderHistory': [{
				'orderStatus': '20',
				'statusDate': '2014-09-20'
			}, {
				'orderStatus': '30',
				'statusDate': '2014-09-22'
			}, {
				'orderStatus': '40',
				'statusDate': '2014-09-24'
			}, {
				'orderStatus': '50',
				'statusDate': '2014-09-28'
			}],
			'vehicleData': {
				'vehicleKey': 1234567,
				'vin': 'JMZGG14F601657489',
				'carReg': 'VALID1',
				'isAudi': true,
				'make': 'Audi',
				'model': 'Audi TT',
				'modelVariant': '',
				'modelYear': '2012',
				'exteriorColour': 'Blue',
				'interiorDescription': '',
				'commissionNumber': '',
				'commissionYear': '',
				'vehicleLongDescription': 'Audi TT Roadster S line 2.0 TFSI quattro 211',
				'ownershipStartDate': '2012-03-25',
				'ownershipEndDate': null,
				'intendedReplacementDate': '2015-01-01',
				'warrantyStartDate': '2012-03-25',
				'warrantyEndDate': '2015-03-25',
				'extendedWarrantyStartDate': '',
				'extendedWarrantyEndDate': '',
				'lastMileage': 1500,
				'lastMileageDate': '2012-03-25',
				'engineNumber': 'LF872584',
				'chassisNumber': '',
				'fuelType': 'Petrol',
				'engineSize': 2100,
				'registrationDate': '2012-02-01',
				'frontAxleWeight': '250',
				'rearAxleWeight': '650',
				'engineName': '',
				'acceleration': '',
				'power': '',
				'maxPower': '',
				'topSpeed': '',
				'maxTorque': '',
				'drive': '',
				'fuelTankCapacity': '',
				'grossVehicleWeight': '',
				'unladenWeight': '',
				'gear': '',
				'trimLine': '',
				'bikRate': '',
				'feulConsumptionUrban': '28.3',
				'feulConsumptionRural': '32.1',
				'feulConsumptionAverage': '29.7',
				'co2EmissionsUrban': 160,
				'co2EmissionsRural': 145,
				'co2EmissionsAverage': 153,
				'equipmentList': [{
					'name': 'Standard Equipment',
					'salesFamilies': [{
						'name': 'Technology',
						'options': [{
							'prCode': '7AL',
							'pnrCode': 'MEDW7AL',
							'pnrDescription': 'Anti-theft alarm',
							'pnrFamilyCode': 'EDW',
							'pnrFamilyDescription': 'Anti-theft protection',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}, {
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': '',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}],
						'packages': [{
							'packageCode': 'BBB',
							'packageKey': '',
							'packageDescription': 'Entertainment Package',
							'imageUrl': '',
							'price': 159.99,
							'options': [{
								'prCode': '7AL',
								'pnrCode': 'BFAW7AL',
								'pnrDescription': 'Anti-theft alarm',
								'pnrFamilyCode': 'EDW',
								'pnrFamilyDescription': 'Anti-theft protection',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}, {
								'prCode': '',
								'pnrCode': '',
								'pnrDescription': '',
								'pnrFamilyCode': '',
								'pnrFamilyDescription': '',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}]
						}]
					}, {
						'name': 'Telephone and Communications',
						'options': [{
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': '',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}],
						'packages': [{}]
					}]
				}, {
					'name': 'Optional extras',
					'salesFamilies': [{
						'name': 'Technology',
						'options': [{
							'prCode': '7AL',
							'pnrCode': 'MEDW7AL',
							'pnrDescription': 'Anti-theft alarm',
							'pnrFamilyCode': 'EDW',
							'pnrFamilyDescription': 'Anti-theft protection',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}, {
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': '',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}],
						'packages': [{
							'packageCode': 'BBB',
							'packageKey': '',
							'packageDescription': 'Entertainment Package',
							'imageUrl': '',
							'price': 159.99,
							'options': [{
								'prCode': '7AL',
								'pnrCode': 'BFAW7AL',
								'pnrDescription': 'Anti-theft alarm',
								'pnrFamilyCode': 'EDW',
								'pnrFamilyDescription': 'Anti-theft protection',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}, {
								'prCode': '',
								'pnrCode': '',
								'pnrDescription': '',
								'pnrFamilyCode': '',
								'pnrFamilyDescription': '',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}]
						}]
					}, {
						'name': 'Telephone and Communications',
						'options': [{
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': ''
						}],
						'packages': [{}]
					}]
				}]
			}
		},

		'22222223': {
			'orderNumber': '22222223',
			'orderStatus': '50',
			'handoverDate': '',
			'deliveryDealerName': '',
			'deliveryDealerCode': '',
			'orderRetailerName': '',
			'orderRetailerCode': '',
			'createDate': '',
			'modelCode': '6XGT73',
			'modelYear': '2014',
			'modelVersion': '1',
			'exteriorCode': 'y2y2',
			'interiorCode': 'be',
			'translatedOptions': [
				'OFB',
				'FFD',
				'RET',
				'QAS'
			],
			'orderHistory': [{
				'orderStatus': '20',
				'statusDate': '2014-09-20'
			}, {
				'orderStatus': '30',
				'statusDate': '2014-09-22'
			}, {
				'orderStatus': '40',
				'statusDate': '2014-09-24'
			}, {
				'orderStatus': '50',
				'statusDate': '2014-09-28'
			}],
			'vehicleData': {
				'vehicleKey': 1234567,
				'vin': 'JMZGG14F601657489',
				'carReg': 'LL02LXS',
				'isAudi': true,
				'make': 'Audi',
				'model': 'A5 Sportback',
				'modelVariant': '',
				'modelYear': '2012',
				'exteriorColour': 'Red',
				'interiorDescription': '',
				'commissionNumber': '',
				'commissionYear': '',
				'vehicleLongDescription': '',
				'ownershipStartDate': '2012-03-25',
				'ownershipEndDate': null,
				'intendedReplacementDate': '2015-01-01',
				'warrantyStartDate': '2012-03-25',
				'warrantyEndDate': '2015-03-25',
				'extendedWarrantyStartDate': '',
				'extendedWarrantyEndDate': '',
				'lastMileage': 1500,
				'lastMileageDate': '2012-03-25',
				'engineNumber': 'LF872584',
				'chassisNumber': '',
				'fuelType': 'Petrol',
				'engineSize': 2100,
				'registrationDate': '2012-02-01',
				'frontAxleWeight': '250',
				'rearAxleWeight': '650',
				'engineName': '',
				'acceleration': '',
				'power': '',
				'maxPower': '',
				'topSpeed': '',
				'maxTorque': '',
				'drive': '',
				'fuelTankCapacity': '',
				'grossVehicleWeight': '',
				'unladenWeight': '',
				'gear': '',
				'trimLine': '',
				'bikRate': '',
				'feulConsumptionUrban': '28.3',
				'feulConsumptionRural': '32.1',
				'feulConsumptionAverage': '29.7',
				'co2EmissionsUrban': 160,
				'co2EmissionsRural': 145,
				'co2EmissionsAverage': 153,
				'equipmentList': [{
					'name': 'Standard Equipment',
					'salesFamilies': [{
						'name': 'Technology',
						'options': [{
							'prCode': '7AL',
							'pnrCode': 'MEDW7AL',
							'pnrDescription': 'Anti-theft alarm',
							'pnrFamilyCode': 'EDW',
							'pnrFamilyDescription': 'Anti-theft protection',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}, {
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': '',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}],
						'packages': [{
							'packageCode': 'BBB',
							'packageKey': '',
							'packageDescription': 'Entertainment Package',
							'imageUrl': '',
							'price': 159.99,
							'options': [{
								'prCode': '7AL',
								'pnrCode': 'BFAW7AL',
								'pnrDescription': 'Anti-theft alarm',
								'pnrFamilyCode': 'EDW',
								'pnrFamilyDescription': 'Anti-theft protection',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}, {
								'prCode': '',
								'pnrCode': '',
								'pnrDescription': '',
								'pnrFamilyCode': '',
								'pnrFamilyDescription': '',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}]
						}]
					}, {
						'name': 'Telephone and Communications',
						'options': [{
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': '',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}],
						'packages': [{}]
					}]
				}, {
					'name': 'Optional extras',
					'salesFamilies': [{
						'name': 'Technology',
						'options': [{
							'prCode': '7AL',
							'pnrCode': 'MEDW7AL',
							'pnrDescription': 'Anti-theft alarm',
							'pnrFamilyCode': 'EDW',
							'pnrFamilyDescription': 'Anti-theft protection',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}, {
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': '',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}],
						'packages': [{
							'packageCode': 'BBB',
							'packageKey': '',
							'packageDescription': 'Entertainment Package',
							'imageUrl': '',
							'price': 159.99,
							'options': [{
								'prCode': '7AL',
								'pnrCode': 'BFAW7AL',
								'pnrDescription': 'Anti-theft alarm',
								'pnrFamilyCode': 'EDW',
								'pnrFamilyDescription': 'Anti-theft protection',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}, {
								'prCode': '',
								'pnrCode': '',
								'pnrDescription': '',
								'pnrFamilyCode': '',
								'pnrFamilyDescription': '',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}]
						}]
					}, {
						'name': 'Telephone and Communications',
						'options': [{
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': ''
						}],
						'packages': [{}]
					}]
				}]
			}
		},

		'22223333': {
			'orderNumber': '22223333',
			'orderStatus': '50',
			'handoverDate': '',
			'deliveryDealerName': '',
			'deliveryDealerCode': '',
			'orderRetailerName': '',
			'orderRetailerCode': '',
			'createDate': '',
			'modelCode': '6XGT73',
			'modelYear': '2014',
			'modelVersion': '1',
			'exteriorCode': 'y2y2',
			'interiorCode': 'be',
			'translatedOptions': [
				'OFB',
				'FFD',
				'RET',
				'QAS'
			],
			'orderHistory': [{
				'orderStatus': '20',
				'statusDate': '2014-09-20'
			}, {
				'orderStatus': '30',
				'statusDate': '2014-09-22'
			}, {
				'orderStatus': '40',
				'statusDate': '2014-09-24'
			}, {
				'orderStatus': '50',
				'statusDate': '2014-09-28'
			}],
			'vehicleData': {
				'vehicleKey': 1234567,
				'vin': 'JMZGG14F601657489',
				'carReg': 'LL02LXS',
				'isAudi': true,
				'make': 'Audi',
				'model': 'A5 Sportback',
				'modelVariant': '',
				'modelYear': '2012',
				'exteriorColour': 'Red',
				'interiorDescription': '',
				'commissionNumber': '',
				'commissionYear': '',
				'vehicleLongDescription': '',
				'ownershipStartDate': '2012-03-25',
				'ownershipEndDate': null,
				'intendedReplacementDate': '2015-01-01',
				'warrantyStartDate': '2012-03-25',
				'warrantyEndDate': '2015-03-25',
				'extendedWarrantyStartDate': '',
				'extendedWarrantyEndDate': '',
				'lastMileage': 1500,
				'lastMileageDate': '2012-03-25',
				'engineNumber': 'LF872584',
				'chassisNumber': '',
				'fuelType': 'Petrol',
				'engineSize': 2100,
				'registrationDate': '2012-02-01',
				'frontAxleWeight': '250',
				'rearAxleWeight': '650',
				'engineName': '',
				'acceleration': '',
				'power': '',
				'maxPower': '',
				'topSpeed': '',
				'maxTorque': '',
				'drive': '',
				'fuelTankCapacity': '',
				'grossVehicleWeight': '',
				'unladenWeight': '',
				'gear': '',
				'trimLine': '',
				'bikRate': '',
				'feulConsumptionUrban': '28.3',
				'feulConsumptionRural': '32.1',
				'feulConsumptionAverage': '29.7',
				'co2EmissionsUrban': 160,
				'co2EmissionsRural': 145,
				'co2EmissionsAverage': 153,
				'equipmentList': [{
					'name': 'Standard Equipment',
					'salesFamilies': [{
						'name': 'Technology',
						'options': [{
							'prCode': '7AL',
							'pnrCode': 'MEDW7AL',
							'pnrDescription': 'Anti-theft alarm',
							'pnrFamilyCode': 'EDW',
							'pnrFamilyDescription': 'Anti-theft protection',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}, {
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': '',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}],
						'packages': [{
							'packageCode': 'BBB',
							'packageKey': '',
							'packageDescription': 'Entertainment Package',
							'imageUrl': '',
							'price': 159.99,
							'options': [{
								'prCode': '7AL',
								'pnrCode': 'BFAW7AL',
								'pnrDescription': 'Anti-theft alarm',
								'pnrFamilyCode': 'EDW',
								'pnrFamilyDescription': 'Anti-theft protection',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}, {
								'prCode': '',
								'pnrCode': '',
								'pnrDescription': '',
								'pnrFamilyCode': '',
								'pnrFamilyDescription': '',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}]
						}]
					}, {
						'name': 'Telephone and Communications',
						'options': [{
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': '',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}],
						'packages': [{}]
					}]
				}, {
					'name': 'Optional extras',
					'salesFamilies': [{
						'name': 'Technology',
						'options': [{
							'prCode': '7AL',
							'pnrCode': 'MEDW7AL',
							'pnrDescription': 'Anti-theft alarm',
							'pnrFamilyCode': 'EDW',
							'pnrFamilyDescription': 'Anti-theft protection',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}, {
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': '',
							'price': 154.99,
							'imageURl': 'www.images.com/fkfgfgdfkg'
						}],
						'packages': [{
							'packageCode': 'BBB',
							'packageKey': '',
							'packageDescription': 'Entertainment Package',
							'imageUrl': '',
							'price': 159.99,
							'options': [{
								'prCode': '7AL',
								'pnrCode': 'BFAW7AL',
								'pnrDescription': 'Anti-theft alarm',
								'pnrFamilyCode': 'EDW',
								'pnrFamilyDescription': 'Anti-theft protection',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}, {
								'prCode': '',
								'pnrCode': '',
								'pnrDescription': '',
								'pnrFamilyCode': '',
								'pnrFamilyDescription': '',
								'price': 154.99,
								'imageURl': 'www.images.com/fkfgfgdfkg'
							}]
						}]
					}, {
						'name': 'Telephone and Communications',
						'options': [{
							'prCode': '',
							'pnrCode': '',
							'pnrDescription': '',
							'pnrFamilyCode': '',
							'pnrFamilyDescription': ''
						}],
						'packages': [{}]
					}]
				}]
			}
		}
	};

	var getOrders = function() {
		return orders;
	};

	var getOrder = function( orderNumber ) {
		return orders[orderNumber];
	};

	return {
		getOrders: getOrders,
		getOrder:  getOrder
	};

}]);
