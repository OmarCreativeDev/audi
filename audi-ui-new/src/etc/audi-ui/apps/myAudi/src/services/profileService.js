myAudi.factory('profileService', ['$resource', '$http', '$q', '$httpBackend', 'apiService', '$state','$filter',
	function ($resource, $http, $q, $httpBackend, apiService, $state,$filter) {

		var profileData = {};
		var profileDataTemp  = {};

		var cancelEdit = function(){
			angular.copy(profileDataTemp, profileData);
		};

		var updateProfileData =  function(data) {

			// Tweak the object ready for the app
			var tempData = data.data;
			angular.extend(tempData,{
				passwordOld: '',
				password: '',
				passwordRepeat: ''
			});

			// make the date nice and easy for the dropdowns to work with
			angular.extend(tempData,{
				dobFormated:{
					day: $filter('date')(tempData.dateOfBirth, 'dd'),
					month: $filter('date')(tempData.dateOfBirth, 'MM'),
					year: $filter('date')(tempData.dateOfBirth, 'yyyy'),
				}
			});

			angular.copy(tempData, profileData);
			angular.copy(tempData, profileDataTemp);
		};

		var save = function( action ) {

			var methodAction = 'POST';
			var dataToSave = profileData;

			if (action === 'PUT') {
				var tempChanges = findChanges();
				if (tempChanges) {
					dataToSave = tempChanges;
				}
			}

			if (window.console) { console.log('dataToSave',dataToSave); }

			return new apiService.withErrorHandling({
				title: 'saveProfileData',
				url: 'profile',
				method: methodAction,
				data: dataToSave
			})
			.then(function (data) {
				updateProfileData(data);

			});
		};

		var loadProfileData = function() {

			if (window.console) { console.log('profileService.loadProfileData() called'); }

			return new apiService.withErrorHandling({
				title: 'loadProfileData',
				url: 'profile',
				method: 'GET',
				data: {}
			})
			.then(function (data) {
				updateProfileData(data);
			});
		};

		var lookupAddress = function( postcode ) {
			return $http({
				method: 'GET',
				url: myAudi.appConfig.urls.webServices + '/commonws/services/address/postcodeLookup/' + postcode
			});
		};

		var dobStrToDate = function( str ) {
			var d = parseInt(str.substr(0,2)),
				m = parseInt(str.substr(2,2))-1,
				y = parseInt(str.substr(4,2));
			return new Date(y, m, d);
		};

		var findChanges = function(){

			var newOb  = profileData;
			var oldOb  = profileDataTemp;
			var returnObj = {};
			var itemChanged = false;

			angular.forEach(oldOb,function(oldValue,oldKey){
				angular.forEach(newOb,function(newValue,newKey){
					//dobFormated ignored as it is created by the FE
					if (oldKey === newKey && oldKey !== 'dobFormated') {
						if (oldValue !== newValue) {
							if (window.console) { console.log(oldKey, ' Has changed'); }
							returnObj[oldKey] = newValue;
							itemChanged = true;
						}
					}
				});
			});

			if(itemChanged){
				if (window.console) { console.log('here are the changes: ',returnObj); }
				return returnObj;
			} else {
				return false;
			}
		};

		var saveAddrLookup = function( data ) {
			if (window.console) console.log('called saveAddrLookup() with data:',data);
			var newData = getAddressFromLookupData( data );
			angular.extend( profileData, newData );

		};

		var getAddressFromLookupData = function( data ) {
			var addr1 = data.houseFlatNumber,
				addr2 = data.street;

			if ( data.premise.length ) {
				addr1 = data.premise;
				addr2 = data.houseFlatNumber + ' ' + data.street;
			}

			var newData = {
				'address1': addr1,
				'address2': addr2,
				'city':     data.town,
				'postcode': data.postCode,
				'county':   data.county
			}
			angular.extend(profileData,newData);

			return newData;
		};

		var lookupEmail = function (email) {

			if (window.console) console.log('called lookupEmail() with:', { email:email });

			return new apiService.withErrorHandling({
				title: 'Lookup email',
				url: 'users/lookup',
				method: 'GET',
				params: { email:email }
			});
		};

		var isAddressEmpty = function() {
			if ( profileData.address1 || profileData.address2 || profileData.city || profileData.county || profileData.postcode ) {
				return false;
			} else {
				return true;
			}
		};

		var getDealerCode = function() {
			return {
				'dealerCode': profileData.primaryPreferredDealerCode
			}
		};

		var deleteAccount = function(){
			return new apiService.withErrorHandling({
				title: 'Lookup email',
				url: 'users',
				method: 'DELETE'
			});
		};

		return {
			profileData: profileData,
			profileDataTemp: profileDataTemp,
			loadProfileData: loadProfileData,
			lookupAddress: lookupAddress,
			save: save,
			saveAddrLookup: saveAddrLookup,
			getAddressFromLookupData: getAddressFromLookupData,
			lookupEmail: lookupEmail,
			isAddressEmpty: isAddressEmpty,
			getDealerCode: getDealerCode,
			cancelEdit:cancelEdit,
			deleteAccount:deleteAccount
		}
	}
]);
