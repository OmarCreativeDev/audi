myAudi.factory('communicationPreferencesService', ['$resource', '$http', '$q', '$httpBackend', 'apiService',
	function ($resource, $http, $q, $httpBackend, apiService) {

		var communicationPreferences = {};
		var communicationPreferencesTemp = {};

		var updatePrefObject = function(data){
			var tempData = data.data;

			// fix to get around opt out and interface being different
			angular.extend(tempData,{
				'mailOptIn': !tempData.mailOptOut,
				'phoneOptIn': !tempData.phoneOptOut,
				'emailOptIn': !tempData.emailOptOut,
				'smsOptOutIn': !tempData.smsOptOut,
				'globalOptIn': !tempData.globalOptOut
			});

			angular.copy(tempData, communicationPreferences);
			angular.copy(tempData, communicationPreferencesTemp);
		};

		// methods accepted GET / PUT
		var getCommunicationPreferences = function () {
			if (window.console) { console.log('communicationPreferencesService.communicationPreferences() called'); }
			return new apiService.withErrorHandling({
				title: 'getCommunicationPreferences',
				url: 'profile/communications',
				method: 'GET'
			})
			.then(function (data) {
				updatePrefObject(data);
			});
		};


		var saveCommunicationPreferences = function( action ) {
			// fix to get around opt out and interface being different
			angular.extend(communicationPreferences,{
				'mailOptOut': !communicationPreferences.mailOptIn,
				'phoneOptOut': !communicationPreferences.phoneOptIn,
				'emailOptOut': !communicationPreferences.emailOptIn,
				'smsOptOutOut': !communicationPreferences.smsOptIn,
				'globalOptOut': !communicationPreferences.globalOptIn
			});

			return new apiService.withErrorHandling({
				title: 'saveProfileCommunicationsData',
				url: 'profile/communications',
				method: 'POST',
				data: communicationPreferences
			})
			.then(function (data) {

				updatePrefObject(data);

			});
		};

		var cancelEdit = function(){
			angular.copy(communicationPreferencesTemp, communicationPreferences);
		};

		return {
			communicationPreferences:communicationPreferences,
			communicationPreferencesTemp:communicationPreferencesTemp,
			getCommunicationPreferences:getCommunicationPreferences,
			saveCommunicationPreferences:saveCommunicationPreferences,
			cancelEdit:cancelEdit
		};
	}
]);
