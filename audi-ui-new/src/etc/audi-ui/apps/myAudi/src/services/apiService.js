
var jsonToUrlencoded = function ( data ) {
    var key, result = [];
    for (key in data) {
        if (data.hasOwnProperty(key)) {
            result.push(encodeURIComponent(key) + "=" + encodeURIComponent(data[key]));
        }
    }
    return result.join("&");
}

myAudi.factory('templateCache', ['$http', '$templateCache',function($http,$templateCache){
    var get = function(url){
        $http.get(url, {cache:$templateCache});
    }
    return {
        get: get
    }
}]);

myAudi.factory('apiService', ['$resource', '$http', '$q', 'apiServiceMocks',
    function($resource, $http, $q, apiServiceMocks) {

        var apiUrl = myAudi.appConfig.urls.api;

        if ( myAudi.appConfig.useMocks ) {
            apiServiceMocks.setupMocks();
        }

        var handleRequestSuccess = function(deferred, data) {
            if (data.responseInfo.code == 0) {
                deferred.resolve( data );
            } else {
                deferred.reject( data );
            }
        }

        var handleRequestFailure = function(deferred) {
            var response = {
                "responseInfo": {
                    "code": 998,
                    "desc": "Service unavailable"
                }
            };
            deferred.reject( response );
        }

        var withErrorHandling = function(obj) {

            console.log('withErrorHandling :', obj.title, obj.url, obj);

            var deferred = $q.defer();

            var request = {
                method: obj.method,
                url: (String(obj.url).indexOf('http') > -1) ? obj.url : apiUrl + obj.url
            };

            obj.method = obj.method.toUpperCase();

            if ( (obj.method == 'POST' && typeof obj.data == 'object') || obj.method == 'PUT' && typeof obj.data == 'object' ) {

                if ( obj.contentType == 'form' ) {
                    request.headers = {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                    obj.data = jsonToUrlencoded(obj.data);
                }

                request.data = obj.data;

            } else if ( obj.method == 'GET' && typeof obj.params == 'object' ) {
                request.params = obj.params;
            }

            console.log("apiService : doing $http request with", request)

            $http( request )
                .success(function( data, status ) {
                    console.log(obj.title + ' DONE', data, status);
                    if ( data && typeof data.responseInfo == 'object' ) {
                        handleRequestSuccess( deferred, data );
                    } else {
                        handleRequestFailure( deferred );
                    }
                })
                .error(function( data, status ) {
                    console.log(obj.title + ' FAILED', data, status);
                    if ( data && typeof data.responseInfo == 'object' ) {
                        handleRequestSuccess( deferred, data );
                    } else {
                        handleRequestFailure( deferred );
                    }
                });

            return deferred.promise;
        }

        return {
            withErrorHandling: withErrorHandling
        }
    }
]);
