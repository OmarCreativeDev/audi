myAudi.factory('messageService', ['$resource', '$http', '$q', '$httpBackend', 'apiService', '$state',
	function($resource, $http, $q, $httpBackend, apiService, $state) {

		var messages = [];
		var messagesListInfo = {};
		var unreadMessagesInfo = {};

		var checkUnreadMessages = function() {
			if (window.console) console.log('%c messageService.checkUnreadMessages() called', 'background:#8700ff; color:#fff');

			return new apiService.withErrorHandling({
					title: 'checkUnreadMessages',
					url: 'messages?filter=UNREAD',
					method: 'GET',
					data: unreadMessagesInfo
				})
				.then(function(data) {
					angular.copy(data.listInfo, unreadMessagesInfo);
					if (window.console) console.log('%c totalCount ', 'background:#e9ff00; color:#000', unreadMessagesInfo);
				});
		};

		var getMessages = function(pageNumber) {
			if (window.console) console.log('messageService.getMessages() called with pageNumber ' + pageNumber);

			return new apiService.withErrorHandling({
					title: 'getMessages',
					url: 'messages?pageNumber=' + pageNumber,
					method: 'GET',
					data: messagesListInfo
				})
				.then(function(data) {
					angular.copy(data.data, messages);
					angular.copy(data.listInfo, messagesListInfo);
					if (window.console) console.log('datadatadata:', data.listInfo, messagesListInfo);
				});
		};

		var getMessage = function(message) {
			if (window.console) console.log("messageService.getMessage(" + message.messageId + ") called");

			return new apiService.withErrorHandling({
				title: 'getMessage',
				url: "messages/" + message.messageId,
				method: 'GET'
			}).then(function(data) {
				// copy the data from the response back onto 
				// the passed in message object#
				data.data.viewed = true;
				angular.copy(data.data, message);
			});
		};

		var toggleFlag = function(message) {
			if (window.console) console.log("messageService.toggleFlag(" + message.messageId + ") called");

			var _method = message.flagged ? "POST" : "DELETE"

			return new apiService.withErrorHandling({
				title: 'toggleFlag',
				url: "messages/" + message.messageId + "/flag",
				method: _method
			});
		};

		var deleteMessage = function(messageIDs) {
			if (window.console) console.log("messageService.deleteMessage(" + messageIDs + ") called");

			return new apiService.withErrorHandling({
				title: 'deleteMessage',
				url: 'messages/' + messageIDs,
				method: 'DELETE'
			});
		};

		/* use this for mocks
		var deleteMessage = function(messageID) {
			if (window.console) console.log("messageService.deleteMessage() called");

			return new apiService.withErrorHandling({
				title: 'deleteMessage',
				url: 'messages/1',
				method: 'DELETE'
			});
		};
		*/

		return {
			checkUnreadMessages: checkUnreadMessages,
			messages: messages,
			getMessages: getMessages,
			getMessage: getMessage,
			toggleFlag: toggleFlag,
			deleteMessage: deleteMessage,
			messagesListInfo: messagesListInfo,
			unreadMessagesInfo: unreadMessagesInfo
		};
	}
]);
