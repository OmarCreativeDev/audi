myAudi.factory('centresService', ['$resource', '$http', '$q', function ($resource, $http, $q) {

	var centres = [];
	var centre={};

	var resetCentres = function(){
		angular.copy([],centres);
	};

	var centreSearch = function(location,limit) {

		var deferred = $q.defer();

		$http({
			url: myAudi.appConfig.urls.webServices + '/audi-dealercentre-service/dealerservice/places/place',
			method: 'GET',
			params: {
				query: location,
				limit: limit
			}
		}).then(
			function(response){

				angular.forEach(response.data.dealers,function(centre,index){
					centre.id = index;
					centre.options = {
						draggable: false,
						labelAnchor: '10 39',
						labelContent: index,
						labelClass: 'labelMarker'
					};
					centre.latitude = centre.location.latitude;
					centre.longitude = centre.location.longitude;
					centre.showWindow = false;
					centre.label = index;
					centre.title = 'm:'+index;
				});
				angular.copy(response.data.dealers,centres);
				if (window.console) { console.log('centres',centres); }
				deferred.resolve(response);
			},
			function(response){
				deferred.reject(response);
			}
		);

		return deferred.promise;
	};

	var getCentre = function(dealerCode){

		var deferred = $q.defer();

		$http({
			url: myAudi.appConfig.urls.webServices + '/audi-dealercentre-service/dealerservice/getdealerdetails',
			method: 'GET',
			params: { dealerCode: dealerCode }
		}).then(
			function(response){
				if (window.console) { console.log('response.data',response.data); }
				angular.copy(response.data,centre);
				deferred.resolve(response.data);
			},
			function(response){
				if (window.console) { console.log('data fetch error'); }
				deferred.reject(response);
			}
		);

		return deferred.promise;
	};

	return {
		centres:centres,
		centre:centre,
		centreSearch:centreSearch,
		getCentre:getCentre,
		resetCentres:resetCentres
	};
}]);
