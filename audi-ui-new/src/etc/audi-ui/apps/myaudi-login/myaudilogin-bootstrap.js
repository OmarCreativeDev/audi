require([
	'ng',

	'sa-utils',
	'omniture-scode',
	'omniture-global-tagging',
	
	'myAudiLogin',
	'myAudiLogin-config',
	'myAudiLogin-routes',

	'myAudiLogin-ngViews-mainCtrl',
	'myAudiLogin-accountLogin-loginCtrl',
	'myAudiLogin-accountLogin-logoutCtrl',
	'myAudiLogin-accountVerifyCtrl',

	'myAudiLogin-accountSignup-signupCtrl',

	'myAudiLogin-resetPassword-forgotPasswordCtrl',
	'myAudiLogin-resetPassword-resetPasswordCtrl',
	
	'myAudiLogin-apiService',
	'myAudiLogin-apiServiceMocks',
	'myAudiLogin-userService',
	
	'myAudiLogin-directive-salFocus',
	'myAudiLogin-directive-salEmailUnique',
	'myAudiLogin-directive-salFormFeedbackIcons',
	'myAudiLogin-directive-salPopoverHtmlUnsafe',
	'myAudiLogin-directive-salPasswordRepeat',
	'myAudiLogin-directive-salPasswordValidate',
	'myAudiLogin-directive-salPasswordPopover',
	'myAudiLogin-directive-salPasswordFocus'	
	

], function (angular, app) {

	'use strict';

	var el = document.getElementById('myaudi-login');

	angular.element(el).ready(function() {
		angular.bootstrap(el, ['myAudiLogin']);
	});

});
