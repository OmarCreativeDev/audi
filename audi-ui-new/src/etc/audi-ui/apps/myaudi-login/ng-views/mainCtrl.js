

	myAudiLogin.controller('mainCtrl', ['$scope', '$location', '$timeout',
		function ($scope, $location, $timeout) {

			var CTRL = {};

			CTRL.alertVisible = false;

			CTRL.showAlert = function ( message ) {
				$scope.alertMessage = message;
				CTRL.alertVisible = true;
				$timeout(function(){
					CTRL.alertVisible = false;
				}, 3000);
			}

			CTRL.isAlertVisible = function ( message ) {
				return CTRL.alertVisible;
			}

			CTRL.isLandingPage = function() {
				if ( $location.path().match(/\/learnmore\/landing/) ) {
					return true;
				} else {
					return false;
				}
			}

			$scope.showAlert      = CTRL.showAlert;
			$scope.isAlertVisible = CTRL.isAlertVisible;
			$scope.isLandingPage  = CTRL.isLandingPage;

			$scope.alertMessage = "";
		}
	]);
