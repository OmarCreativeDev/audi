
define( ['ng'], function (angular) {
	
	'use strict';

	try {
		angular.module('myAudiLogin');
	} catch(err) {
		angular.module('myAudiLogin', [
			'ui.router',
			'ngResource',
			'ngAnimate',
			'ngSanitize',
			'ui.bootstrap',
			'sa.utils',
			'placeholder',
			'angulartics',
			'angulartics.adobe.analytics'
		]);
	}

	return angular.module('myAudiLogin');
});
