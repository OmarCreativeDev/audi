
	myAudiLogin.directive("salPasswordRepeat", [
		function() {
			return {
				restrict: "A",
				require: "ngModel",
				scope: {
					passwordRepeat: '=ngModel',
					password:       '=salPasswordRepeat'
				},
				link: function (scope, element, attrs, ctrl) {

					function passwordsMatch ( p1, p2 ) {

						var p1 = p1;
						var p2 = p2;

						if (p1.length > 0 & p1 === p2) {
							ctrl.$hadFocus = true;
						}

						ctrl.$setValidity( "different", ( p1 === p2 ) ? true : false );
					}

					scope.$watch("password", function() {
						passwordsMatch( ctrl.$viewValue, scope.password );
					});

					scope.$watch("passwordRepeat", function() {
						passwordsMatch( ctrl.$viewValue, scope.password );
					});
				}
			}
		}
	]);
