
	myAudiLogin.directive("salFormFeedbackIcons", [
		function() {
			return {
				restrict: "A",
				replace: true,
				scope: {
					"fieldName": "@",
					"formName": "@"
				},
				templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-directives/salFormFeedbackIcons/template.html",
				controller: function ($scope) {

					var form  = $scope.$parent[$scope.formName],
						field = form[$scope.fieldName];

					$scope.isValid = function(){
						if ( (field.$hadFocus || form.submitted) && field.$valid ) {
							return true;
						}
					};

					$scope.isInvalid = function(){
						if ( (field.$hadFocus || form.submitted) && field.$invalid ) {
							return true;
						}
					};
				}
			}
		}
	]);

