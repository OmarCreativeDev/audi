
	
	myAudiLogin.directive("salPasswordValidate", [
		function() {
			return {
				restrict: "A",
				require: "ngModel",
				link: function (scope, element, attrs, ctrl) {

					scope.$watch(attrs.ngModel, function() {

						var pass = ctrl.$viewValue,
							callback = attrs.salPasswordValidate;

						if ( typeof pass === 'undefined' ) {

							ctrl.$setValidity( "chars", false );
							ctrl.$setValidity( "numbers", false );
							ctrl.$setValidity( "ucase", false );
							ctrl.$setValidity( "lcase", false );

						} else {

							// min 10 char
							ctrl.$setValidity( "chars", ( pass.length >= 10 ) ? true : false );

							// min of 2 numbers
							ctrl.$setValidity( "numbers", pass.match(/([^0-9]*[0-9]){1}/) ? true : false );
							
							// min of 1 uppercase
							ctrl.$setValidity( "ucase", pass.match(/[A-Z]+/) ? true : false );

							// min of 1 lowercase
							ctrl.$setValidity( "lcase", pass.match(/[a-z]+/) ? true : false );
						}

						if ( typeof scope.$parent[callback] === 'function' ) {
							var fn = scope.$parent[callback];
							fn();
						}

					});

				}
			}
		}
	]);
