
	myAudiLogin.directive('salPasswordFocus', [
		function () {
			var FOCUS_CLASS = "sal-password-focused";
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function (scope, element, attrs, ctrl) {
					ctrl.$hadFocus = false;
					element.bind('blur', function (evt) {
						element.addClass(FOCUS_CLASS);
						scope.$apply(function () {
							ctrl.$hadFocus = true;
						});
					});
				}
			}
		}
	]);
 
