
console.log('myAudiLogin module loaded -- -- -  -- ');


var myAudiLogin = angular.module('myAudiLogin', [
			'ui.router',
			'ngResource',
			'ngAnimate',
			'ngSanitize',
			'ui.bootstrap',
			'sa.utils',
			'placeholder',
			'angulartics',
			'angulartics.adobe.analytics'
]);

 
/* fred */ 
myAudiLogin.config(['$stateProvider', '$urlRouterProvider',
	function ($stateProvider, $urlRouterProvider) {


	$stateProvider
				// "learn more" parent state
				.state('learnMore', {
					url: '/learnmore',
					abstract: true,
					templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-views/wrapper.html',
					controller: "mainCtrl"
				})
				.state('learnMore.landing', {
					url: '/landing',
					views: {
						'': {
							templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/loginForm.html',
							controller: 'loginCtrl'
						},
						'learnMoreView@learnMore': {
							templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/learnMore.html'
						}
					}
				})
				.state('learnMore.landing.accountDeleted', {
					url: '/:accountDeleted',
					views: {
						'': {
							templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/loginForm.html',
							controller: 'loginCtrl'
						},
						'learnMoreView@learnMore': {
							templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/learnMore.html'
						},
						'messageView@learnMore': {
							templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/accountDeleted.html',
							controller: 'logoutCtrl'
						}
					}
				})

				// "account" parent state
				.state('account', {
					url: "/account",
					abstract: true,
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-views/wrapper.html",
					controller: "mainCtrl"
				})
				.state('account.login', {
					url: "/login",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/loginForm.html",
					controller: "loginCtrl"
				})
				.state('account.login.verify', {
					url: "/verify",
					views: {
						'messageView@account': {
							templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/accountVerify.html",
							controller: "accountVerifyCtrl"
						}
					}
				})
				.state('account.verificationExpired', {
					url: '/verificationEmailExpired',
					templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/verificationExpired.html',
					controller: ['$scope', 'userService', function($scope, userService){
						$scope.userService = userService;
					}]
				})
				.state('account.signup', {
					url: "/signup",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/accountSignup/signup.html",
					controller: "signupCtrl"
				})
				.state('account.signupConfirm', {
					url: "/signupconfirm",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/accountSignup/signupConfirm.html",
					controller: "signupCtrl"
				})
				.state('account.signupResentVerification', {
					url: "/resentverification",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/accountSignup/signupResentVerification.html",
					controller: "signupCtrl"
				})
				.state('account.forgotpassword', {
					url: "/forgotpassword",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/resetPassword/forgotPassword.html",
					controller: "forgotPasswordCtrl"
				})
				.state('account.resetPassword', {
					url: "/resetpassword/:urlParam",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/resetPassword/resetPassword.html",
					controller: "resetPasswordCtrl"
				})
				.state('account.logout', {
					url: "/logout",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/logout.html",
					controller: "logoutCtrl"
				})




}])


  myAudiLogin.appConfig = {
        useMocks: true,
        urls: {}
    }

myAudiLogin.run(['$state', function ($state) {



    var init = function () {

        var getRegexMatch = function( regex, string ) {
            var match = regex.exec( string );
            return match[1];
        }

        // Example:
        //   if
        //     window.location.href == "http://127.0.0.1:9090/etc/audi-ui/myaudi-login.html#!/account/login",
        //   then:
        //     baseUrl = http://127.0.0.1:9090/
        //     appsUrl = http://127.0.0.1:9090/etc/audi-ui/
        //     appUrl  = http://127.0.0.1:9090/etc/audi-ui/myaudi-login.html

        var baseUrl = getRegexMatch( /(^http[s]?:\/\/[^\/]*\/)/gi, window.location.href ),
            appsUrl = getRegexMatch( /(^http[s]?:\/\/[^#]*\/)/gi,  window.location.href ),
            appUrl  = getRegexMatch( /(^http[s]?:\/\/[^#]*)/gi,    window.location.href );
        
        myAudiLogin.appConfig.urls.base = baseUrl;
        myAudiLogin.appConfig.urls.apps = appsUrl;
        myAudiLogin.appConfig.urls.app  = appUrl;

        myAudiLogin.appConfig.urls.api  = baseUrl + 'myaudi-public-api/services/v1/';

        myAudiLogin.appConfig.urls.account = appsUrl + 'myaudi.html';
        myAudiLogin.appConfig.urls.login   = appsUrl + 'myaudi-login.html';

        if (window.console) console.log("appConfig :", myAudiLogin.appConfig);
    };

    init();
	$state.go('account.login');

}]);

