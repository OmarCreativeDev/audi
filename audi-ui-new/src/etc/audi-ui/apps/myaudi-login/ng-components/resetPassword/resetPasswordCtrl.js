myAudiLogin.controller('resetPasswordCtrl', ['$scope', 'userService', '$stateParams', '$state', '$location',
	function($scope, userService, $stateParams, $state, $location) {

		var CTRL = {};

		CTRL.model = {
			verifyKey: '',
			email: '',
			title: '',
			forename: '',
			surname: '',
			password: '',
			passwordConfirm: ''
		};

		CTRL.currentError = null;
		CTRL.resetPasswordFormVisible = false;
		CTRL.loadingMessageVisible = false;
		CTRL.passwordExpiredMode = false;

		var getQueryVar = function (url, varName) {

			var match = /\?([^#]*)/.exec(url);

			if ( match === null ) { return false; }

			var query = match[1],
			    vars = query.split("&");

			for ( var i = 0; i < vars.length; i++ ) {
				var pair = vars[i].split("=");
				if(pair[0] == varName){ return pair[1]; }
			}

			return false;
		}

		CTRL.resolveRequest = function() {

			var queryVar = getQueryVar( $location.absUrl(),'data');

			if ( queryVar === false ) {
				if ( $stateParams.urlParam == 'passwordexpired' ) {
					CTRL.passwordExpiredInit();
				} else {
					console.log("MISSING URL PARAMS");
				}
			} else {
				CTRL.passwordResetInit( queryVar );
			}

		};

		CTRL.passwordExpiredInit = function() {

			CTRL.setPasswordExpiredMode();

			if ( userService.userModel.verifyKey == undefined || userService.userModel.email == undefined ) {
				console.log("MISSING verifyKey or email");
			} else {
				CTRL.model.email = userService.userModel.email;
				CTRL.model.forename = userService.userModel.forename;
				CTRL.model.verifyKey = userService.userModel.verifyKey;
				CTRL.showResetPasswordForm();
			}
		}

		CTRL.passwordResetInit = function( urlParams ) {

			CTRL.showLoadingMessage();

			userService.urlparamsDecrypt( urlParams ).then(function( response ) {

				console.log("urlparamsDecrypt: success, response:", response);

				CTRL.model.verifyKey = response.data.verifyKey;
				CTRL.model.email     = response.data.email;
				CTRL.model.title     = response.data.title;
				CTRL.model.forename  = response.data.forename;
				CTRL.model.surname   = response.data.surname;

				// FOR TESTING
				//CTRL.model.verifyKey = 'invalid';

				CTRL.hideLoadingMessage();
				CTRL.showResetPasswordForm();

			}, function(response) {

				console.log("urlparamsDecrypt: error, response:", response);

				CTRL.hideLoadingMessage();

				if ( response.responseInfo.code == 998 || response.responseInfo.code == 999 ) {
					CTRL.serverError();
				} else {
					if ( response.responseInfo.code == 103 ) {
						CTRL.model.email = response.data.email;
					}
					CTRL.setError( response.responseInfo.code );
				}
			});
		}

		CTRL.showResetPasswordForm = function() {
			CTRL.resetPasswordFormVisible = true;
		}
		CTRL.hideResetPasswordForm = function() {
			CTRL.resetPasswordFormVisible = false;
		}
		CTRL.isResetPasswordFormVisible = function() {
			return CTRL.resetPasswordFormVisible;
		}

		CTRL.showLoadingMessage = function() {
			CTRL.loadingMessageVisible = true;
		}
		CTRL.hideLoadingMessage = function() {
			CTRL.loadingMessageVisible = false;
		}
		CTRL.isLoadingMessageVisible = function() {
			return CTRL.loadingMessageVisible;
		}

		CTRL.setPasswordExpiredMode = function() {
			CTRL.passwordExpiredMode = true;
		}
		CTRL.isPasswordExpiredMode = function() {
			return CTRL.passwordExpiredMode;
		}

		CTRL.resetPasswordFormSubmit = function() {
			console.log("resetPasswordFormSubmit");

			$scope.resetPasswordForm.submitted = false;

			if ( $scope.resetPasswordForm.$valid ) {

				var data = {
					verifyKey:       CTRL.model.verifyKey,
					email:           CTRL.model.email,
					password:        CTRL.model.password,
					passwordConfirm: CTRL.model.passwordConfirm
				};

				userService.resetPassword( data ).then(function( response ) {
					$state.go('account.login');
				}, function(response) {
					if ( response.responseInfo.code == 998 || response.responseInfo.code == 999 ) {
						CTRL.serverError();
					} else {
						if ( response.responseInfo.code == 313 ) {
							CTRL.resetFormStatus();
						}
						CTRL.setError( response.responseInfo.code );
					}
				});

			} else {
				$scope.resetPasswordForm.submitted = true;
			}
		}

		CTRL.resendPasswordResetEmail = function() {
			userService.resetPasswordRequest( CTRL.model.email ).then(function() {
				$scope.resetPasswordEmailResent = true;
				CTRL.setError(null);
			}, function() {
				CTRL.serverError();
			});
		};

		CTRL.resetFormStatus = function() {
			$scope.resetPasswordForm.submitted = false;
			$scope.resetPasswordForm.password.$hadFocus = false;
			$scope.resetPasswordForm.passwordConfirm.$hadFocus = false;
			$scope.resetPasswordForm.$setPristine();

			CTRL.model.password = '';
			CTRL.model.passwordConfirm = '';
		}

		CTRL.setError = function(errorCode) {
			CTRL.currentError = errorCode;
		}
		CTRL.getErrorCode = function() {
			return CTRL.currentError;
		}

		CTRL.serverError = function() {
			$scope.$parent.showAlert( "Service temporarily unavailable. Please try again later." );
		}

		CTRL.goToLogin = function(){
			$state.go('account.login');
		}

		CTRL.resolveRequest();

		$scope.model                      = CTRL.model;
		$scope.isResetPasswordFormVisible = CTRL.isResetPasswordFormVisible;
		$scope.isLoadingMessageVisible    = CTRL.isLoadingMessageVisible;
		$scope.isPasswordExpiredMode      = CTRL.isPasswordExpiredMode;
		$scope.resetPasswordFormSubmit    = CTRL.resetPasswordFormSubmit;
		$scope.resendPasswordResetEmail   = CTRL.resendPasswordResetEmail;
		$scope.getErrorCode               = CTRL.getErrorCode;
		$scope.goToLogin                  = CTRL.goToLogin;


		// Utility functions
		// -----------------

		$scope.fieldHasError = function( formName, fieldName ) {
		    var form = $scope[formName],
		        field = form[fieldName];
		    return field.$invalid && (form.submitted || field.$hadFocus) ? true : false;
		}

		$scope.fieldHasSuccess = function( formName, fieldName ) {
		    var form = $scope[formName],
		        field = form[fieldName];
		    return field.$valid && (form.submitted || field.$hadFocus) ? true : false;
		}

		$scope.fieldHasFeedback = function( formName, fieldName ) {
		    var form = $scope[formName],
		        field = form[fieldName];
		    return form.submitted || field.$hadFocus ? true : false;
		}
	}
]);
