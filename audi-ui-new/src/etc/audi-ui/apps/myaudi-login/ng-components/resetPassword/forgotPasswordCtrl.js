	myAudiLogin.controller('forgotPasswordCtrl', ['$scope', '$state', 'userService',
		function ($scope, $state, userService) {

			var CTRL = {};

			CTRL.model = {
				email: ''
			};

			CTRL.currentError = null;
			CTRL.forgotPasswordFormVisible = true;
			CTRL.successMessageVisible = false;

			CTRL.forgotPasswordFormSubmit = function () {

				$scope.forgotPasswordForm.submitted = false;

				if ( $scope.forgotPasswordForm.$valid ) {

					$scope.forgotPasswordForm.submitted = true;

					userService.resetPasswordRequest( CTRL.model.email ).then(function( response ) {

						CTRL.hideForgotPasswordForm();
						CTRL.showSuccessMessage();

					}, function( response ) {

						if ( response.responseInfo.code == 998 || response.responseInfo.code == 999 ) {
							CTRL.serverError();
						} else {
							CTRL.setError( response.responseInfo.code );
							if ( response.responseInfo.code == 414 ) {
								$scope.forgotPasswordForm.email.$setValidity( "userExists", false );
							} else {
								$scope.forgotPasswordForm.email.$setValidity( "other", false );
							}
						}

					});

				} else {
					$scope.forgotPasswordForm.submitted = true;
				}
			};

			CTRL.showForgotPasswordForm = function() {
				CTRL.forgotPasswordFormVisible = true;
			}
			CTRL.hideForgotPasswordForm = function() {
				CTRL.forgotPasswordFormVisible = false;
			}
			CTRL.isForgotPasswordFormVisible = function() {
				return CTRL.forgotPasswordFormVisible;
			}

			CTRL.showSuccessMessage = function() {
				CTRL.successMessageVisible = true;
			}
			CTRL.hideSuccessMessage = function() {
				CTRL.successMessageVisible = false;
			}
			CTRL.isSuccessMessageVisible = function() {
				return CTRL.successMessageVisible;
			}

			CTRL.setError = function(errorCode) {
				CTRL.currentError = errorCode;
			}
			CTRL.getErrorCode = function() {
				return CTRL.currentError;
			}
			CTRL.serverError = function() {
				$scope.$parent.showAlert( "Service temporarily unavailable. Please try again later." );
			}
			CTRL.resetFieldErrors = function( formName, fieldName ) {
				var form = $scope[formName],
			        field = form[fieldName];
				field.$setValidity( "userExists", true );
				field.$setValidity( "other", true );
			}

			CTRL.goToLogin = function(){
				$state.go('account.login');
			}

			$scope.model                       = CTRL.model;
			$scope.getErrorCode                = CTRL.getErrorCode;
			$scope.resetFieldErrors            = CTRL.resetFieldErrors;
			$scope.forgotPasswordFormSubmit    = CTRL.forgotPasswordFormSubmit;
			$scope.isForgotPasswordFormVisible = CTRL.isForgotPasswordFormVisible;
			$scope.isSuccessMessageVisible     = CTRL.isSuccessMessageVisible;
			$scope.goToLogin                   = CTRL.goToLogin;


			// Utility functions
			// -----------------

			$scope.fieldHasError = function( formName, fieldName ) {
			    var form = $scope[formName],
			        field = form[fieldName];
			    return field.$invalid && (form.submitted || field.$hadFocus) ? true : false;
			}

			$scope.fieldHasSuccess = function( formName, fieldName ) {
			    var form = $scope[formName],
			        field = form[fieldName];
			    return field.$valid && (form.submitted || field.$hadFocus) ? true : false;
			}

			$scope.fieldHasFeedback = function( formName, fieldName ) {
			    var form = $scope[formName],
			        field = form[fieldName];
			    return form.submitted || field.$hadFocus ? true : false;
			}
		}
	]);
