	myAudiLogin.controller( 'loginCtrl', ['$scope','$state','userService', '$location', '$window',
		function ($scope, $state, userService, $location, $window) {

			var CTRL = {};

			CTRL.model = {
				loginForm: {
					email: '',
					password: ''
				}
			};

			CTRL.loginFormSubmit = function () {

				$scope.loginForm.submitted = false;
				CTRL.errorCode = null;

				if ( $scope.loginForm.$valid ) {

					$scope.loginLoading = true;

					userService.login( CTRL.model.loginForm.email, CTRL.model.loginForm.password ).then(
						function () {

							$scope.loginLoading = false;
							// TAKE THE USER TO THE DASHBOARD
							$window.location.href = myAudiLogin.appConfig.urls.account;
						},
						function (response) {

							console.log('Login failed. Response =', response);

							// on login failure clear email & password field
							CTRL.model.loginForm.password = '';
							if ( typeof userService.accountVerifiedEmail === 'undefined' ) {
								CTRL.model.loginForm.email = '';
							}

							if ( response.responseInfo.code === 998 || response.responseInfo.code === 999 ) {
								CTRL.serverError();
							}

							if ( response.responseInfo.code === 106 ) {

								console.log('go to reset password. data =', response);

								userService.userModel.email = response.data.email;
								userService.userModel.forename = response.data.forename;
								userService.userModel.verifyKey = response.data.verifyKey;

								$state.go('account.resetPassword', {urlParam: 'passwordexpired'} );
							}

							CTRL.errorCode = response.responseInfo.code;

							if ( CTRL.errorCode === 100 ) {
								$scope.attemptsRemaining = response.data.attemptsRemaining;
							}

							$scope.loginLoading = false;
						}
					);

				} else {
					$scope.loginForm.submitted = true;
				}
			};

			CTRL.serverError = function() {
				$scope.$parent.showAlert( 'Service temporarily unavailable. Please try again later.' );
			};

			CTRL.getErrorCode = function() {
				return CTRL.errorCode;
			};

			CTRL.isLandingPage = function() {
				return ($location.path() === '/learnmore/landing') ? true : false;
			};

			// pre populate email field from userService object
			$scope.$watch('userService.userModel.email', function(){
				CTRL.model.loginForm.email = userService.userModel.email;
			});

			$scope.model           = CTRL.model;
			$scope.getErrorCode    = CTRL.getErrorCode;
			$scope.loginFormSubmit = CTRL.loginFormSubmit;
			$scope.isLandingPage   = CTRL.isLandingPage;
			$scope.userService     = userService;
			$scope.animateSprite   = CTRL.animateSprite;
			$scope.startAnimation  = CTRL.startAnimation;

			// Utility functions
			// -----------------

			$scope.fieldHasError = function( formName, fieldName ) {
				var form = $scope[formName],
					field = form[fieldName];
				return field.$invalid && form.submitted ? true : false;
			};

		}
	]);
