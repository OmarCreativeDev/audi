	myAudiLogin.controller('accountVerifyCtrl', ['$scope', '$state', '$stateParams', 'userService', '$location',
		function ($scope, $state, $stateParams, userService, $location) {

			$scope.showLoading = false;
			$scope.showSuccess = false;
			$scope.showError = false;
			$scope.errorCode = null;

			$scope.debug = null;

			var getQueryVar = function (url, varName) {

				var match = /\?([^#]*)/.exec(url);

				if ( match === null ) { return false; }

				var query = match[1],
				    vars = query.split("&");

				for ( var i = 0; i < vars.length; i++ ) {
					var pair = vars[i].split("=");
					if(pair[0] == varName){ return pair[1]; }
				}

				return false;
			}

			var accountVerify = function( data ) {

				$scope.showLoading = true;

				userService.urlparamsDecrypt( data ).then(function( response ) {

					console.log("urlparamsDecrypt: success, response:", response);

					$scope.showLoading = false;
					$scope.showSuccess = true;

					userService.accountVerified = true;
					userService.userModel.email = response.data.email;

				}, function(response) {

					console.log("urlparamsDecrypt: error, response:", response);

					$scope.showLoading = false;

					// For testing
					//response.responseInfo.code = 103;
					//response.data = { email: "something@something.com" };
					//response.responseInfo.code = 104;
					//response.responseInfo.code = 108;

					if ( response.responseInfo.code == 998 || response.responseInfo.code == 999 ) {
						$scope.$parent.showAlert( "Service temporarily unavailable. Please try again later." );
					} else if ( response.responseInfo.code == 108 ) {
						userService.userModel.email = response.data.email;
						$scope.showError = true;
						$scope.errorCode = response.responseInfo.code;
					} else if ( response.responseInfo.code == 103 ) {
						userService.verificationResentEmail = response.data.email;
						$state.go("account.verificationExpired");
					} else {
						$scope.showError = true;
						$scope.errorCode = response.responseInfo.code;
					}
				});
			}

			accountVerify( getQueryVar( $location.absUrl(),'data') );

		}
	]);
