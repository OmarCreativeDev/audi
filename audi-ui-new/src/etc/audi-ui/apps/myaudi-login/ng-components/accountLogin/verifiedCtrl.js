
	myAudiLogin.controller('verifiedCtrl', ['$scope', '$state', 'userService',
		function ($scope, $state, userService) {

			$scope.loading = false;
			console.log('#######',$state.params.verifyKey,$state.params.email);
			$scope.email = $state.params.email;
			$scope.userService = userService;
			
			$scope.verifyNow = function () {
			
				$scope.loading = true;
				console.log('########################## verifyNow function');
				userService.userModel.disableEmailInput=false;
				userService.verifyEmailVerified($state.params.verifyKey,$state.params.email).then(function(response){
					// OK
					console.log(response);
					$scope.isVerified = true;
					$scope.serverMessages = response;
					$scope.loading = false;
					userService.userModel.disableEmailInput=true;
					userService.userModel.email = $state.params.email;
					userService.userModel.isVerified = $scope.isVerified;					
					
				},
				function(response){
					// error
					$scope.loading = false;
					$scope.serverMessages = response;

					if (response.responseInfo.code === 103) {
						console.log(response);
						userService.userModel.email = response.data.email;
						$state.go('account.verificationExpired',{},{reload :true});
					}
				});

			};
			
			$scope.verifyNow();
		}
	]);
