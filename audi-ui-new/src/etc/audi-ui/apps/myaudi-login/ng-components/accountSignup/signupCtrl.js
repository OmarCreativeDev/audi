	myAudiLogin.controller('signupCtrl', ['$scope', '$state', 'userService', '$modal',
		function ($scope, $state, userService, $modal) {
		
			$scope.userService = userService;
			$scope.user = userService.userModel;

			$scope.regForm = {
				title: "",
				forename: "",
				surname: "",
				email: "",
				password: "",
				passwordConfirm: ""
			};
			$scope.regForm.email = userService.userModel.email;

			$scope.serverErrors = {};

			// Ucomment to pre-fill the form during development
			// $scope.regForm.title = "Mr";
			// $scope.regForm.forename = "John";
			// $scope.regForm.surname = "Smith";
			// $scope.regForm.email = "john@smith.com";
			// $scope.regForm.password = "asdasdasd789Z";
			// $scope.regForm.passwordConfirm = "asdasdasd789Z";
	
			$scope.signupFormSubmit = function() {

				$scope.signupForm.submitted = false;

				if ( $scope.signupForm.$valid ) {

					userService.register( $scope.regForm ).then(function ( response ) {

						if (userService.userModel.marketingPermissionKey) {
							console.log('%c marketingPermissionKey exists ', 'background:green; color:#fff');
							$scope.openEmailMarketingModal();
						} else {
							console.log('%c marketingPermissionKey does NOT exist ', 'background:red; color:#fff');						
							$state.go('account.signupConfirm');
						}

					}, function ( response ) {

						console.log("Registration FAILED");

						$scope.signupForm.submitted = true;
						$scope.serverErrors = {};

						if ( response.responseInfo.code == 998 || response.responseInfo.code == 999 ) {
							serverError();
						}

						if ( response.responseInfo.code === 302 ) {
							$scope.signupForm.email.$setValidity( "emailUsed", false );
						}

						if ( response.responseInfo.code === 303 ) {
							$scope.signupForm.passwordConfirm.$setValidity( "different", false );
						}

						if ( response.responseInfo.code === 301 ) {
							if ( response.validationErrors.length > 0 ) {
								angular.forEach(response.validationErrors, function(field, index) {
									$scope.signupForm[field.fieldName].$setValidity( "serverError", false );
									$scope.serverErrors[field.fieldName] = field.message;
								});
							}
						}
					});

				} else {
					$scope.signupForm.submitted = true;
				}
			}

			$scope.openEmailMarketingModal = function() {
				$modal.open({
					templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-components/accountSignup/emailMarketingModal.html',
					controller: 'signupCtrl',
					backdrop: 'static'
				});
			};

			$scope.signupConfirm = function(OptInValue){
				console.log('signupConfirm() called');

				userService.usersPermission(OptInValue).then(function( response ){
					$scope.modalClose();
					$state.go('account.signupConfirm');
				});
			};

			$scope.modalClose = function() {
				console.log('close modal');
				$scope.$close();
			};

			$scope.goToLogin = function(){
				$state.go('account.login');
			}				

			// Utility functions
			// -----------------

			var serverError = function() {
				$scope.$parent.showAlert( "Service temporarily unavailable. Please try again later." );
			}

			$scope.fieldChanged = function( formName, fieldName ) {
				var form = $scope[formName],
			        field = form[fieldName];
				field.$setValidity( "serverError", true );
			}

			$scope.fieldHasError = function( formName, fieldName ) {
			    var form = $scope[formName],
			        field = form[fieldName];
			    return field.$invalid && (form.submitted || field.$hadFocus) ? true : false;
			}

			$scope.fieldHasSuccess = function( formName, fieldName ) {
			    var form = $scope[formName],
			        field = form[fieldName];
			    return field.$valid && (form.submitted || field.$hadFocus) ? true : false;
			}

			$scope.fieldHasFeedback = function( formName, fieldName ) {
			    var form = $scope[formName],
			        field = form[fieldName];
			    return form.submitted || field.$hadFocus ? true : false;
			}

			$scope.getServerError = function( fieldName ) {
				if ( $scope.serverErrors[fieldName] !== undefined ) {
					return $scope.serverErrors[fieldName];
				} else {
					return false;
				}
			}

		}
	]);
