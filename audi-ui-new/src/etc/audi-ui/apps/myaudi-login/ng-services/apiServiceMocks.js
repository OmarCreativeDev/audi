
    myAudiLogin.factory('apiServiceMocks', ['$httpBackend', function($httpBackend) {

        var getQueryVar = function (query, varName) {
           var vars = query.split("&");
           for ( var i = 0; i < vars.length; i++ ) {
                   var pair = vars[i].split("=");
                   if(pair[0] == varName){ return pair[1]; }
           }
           return(false);
        }

        var apiUrl = myAudiLogin.appConfig.urls.api;

        /* global error codes - will be set set in CQ */
        var errorCodes = {
            0: "Success",
            /* Authentication */
            100: "Incorrect password",
            101: "Account locked - cannot login",

            /* Account email not verified - cannot login */
            102: "This email is associated with an unverified account, you need to follow the instructions in your verification email.",

            /* User token expired */
            103: "Unfortunately your account verification has expired",

            /* User token invalid */
            104: "Unfortunately your account verification is invalid",
            105: "User does not have authority to access resource",
            106: "Password change required",

            /* Validation */
            300: "Required param was null/empty",
            301: "Invalid param value",

            /* Resource Not Found */
            400: "Invalid Url",
            404: "Resource not found",
            414: "User not found in database",

            /* Unknown error */
            999: "Unknown error occurred"
        };

        var emails = [
            {
                // Success
                email: "success@salmon.com",
                code: 0
            }, {
                // Incorrect password
                email: "incorrect@salmon.com",
                code: 100
            }, {
                // Account locked
                email: "locked@salmon.com",
                code: 101
            }, {
                // Account email not verified
                email: "unverified@salmon.com",
                code: 102
            }, {
                // Account verification token expired
                email: "expired@salmon.com",
                code: 103
            }, {
                // Account verification token invalid
                email: "invalid@salmon.com",
                code: 104
            }, {
                // Password change required
                email: "changepassword@salmon.com",
                code: 106
            }, {
                // Unknown email address
                email: "unknown@salmon.com",
                code: 301
            }, {
                // User not found in database
                email: "new@salmon.com",
                code: 414
            }, {
                // Unknown error
                email: "unknown-error@salmon.com",
                code: 999
            }
        ];

        var RI_SUCCESS = {
            code: 0,
            desc: "Success"
        };

        var STANDARD_RESPONSE = [200, {
            responseInfo: RI_SUCCESS
        }, {}];


        var buildResponseInfo = function(email) {

            var responseInfo = {};
            angular.forEach(emails, function(item) {
                if (email == item.email) {
                    responseInfo = {
                        code: item.code,
                        desc: errorCodes[item.code]
                    };
                }
            });
            if (typeof responseInfo.code == "undefined") {
                responseInfo = {
                    code: 414,
                    desc: errorCodes[414]
                };
            }
            return responseInfo;
        }

        var responseCodesBasedOnEmail = function(data) {

            console.log('responseCodesBasedOnEmail(), data =', data)
            
            var responseInfo = buildResponseInfo(data.email);

            var responseObject = {
                responseInfo: responseInfo
            };

            if (responseInfo.code == 106) {
                responseObject.data = {
                    verifyKey: "verifyKEYNEW",
                    email: data.email
                };
            }

            if (responseInfo.code == 103) {
                responseObject.data = {
                    email: data.email
                };
            }

            return [200, responseObject, {}];

        }

        var responseUserRegister = function(data) {
            if (data.email === "registered@salmon.com") {
                return [200, {
                    responseInfo: {
                        code: 1,
                        desc: "Email already being used for a MyAudi account"
                    }
                }, {}];
            } else {
                return [200, {
                    responseInfo: RI_SUCCESS,
                    data: {
                        "marketingPermissionKey": "hgfhg78tfhbdfe87df6",
                        "dbgId": "1234",
                        "myAudiId": "123",
                        "email": "test@test.com",
                        "ssid": "7fd59eff-fd09-428f-af1d-8814a685be85"
                    }
                }, {}];
            }
        }

        var responseUserPermission = function(data) {
            return [200, {
                responseInfo: RI_SUCCESS,
                data: {
                    "email": "test@test.com",
                }
            }, {}];
        }        

        var setupMocks = function() {

            $httpBackend.whenPOST(apiUrl + "user/verify/request")
                .respond(function(method, url, data) {
                    data = angular.fromJson(data);
                    return STANDARD_RESPONSE;
                });
            
            $httpBackend.whenPOST(apiUrl + "login")
                .respond(function(method, url, data) {
                    data = decodeURIComponent(data);
                    data = {
                        email:    getQueryVar( data, 'email' ),
                        password: getQueryVar( data, 'password' )
                    }
                    return responseCodesBasedOnEmail(data);
                });

            $httpBackend.whenPOST(apiUrl + "login/resetpassword/reset")
                .respond(function(method, url, data) {
                    data = angular.fromJson(data);
                    return STANDARD_RESPONSE;
                });


            $httpBackend.whenPOST(apiUrl + "user/verify/verified")
                .respond(function(method, url, data) {
                    data = angular.fromJson(data);
                    return responseCodesBasedOnEmail(data);
                });
            $httpBackend.whenPOST(apiUrl + "users")
                .respond(function(method, url, data) {
                    data = angular.fromJson(data);
                    return responseUserRegister(data);
                });

            $httpBackend.whenPOST(apiUrl + "users/permission")
                .respond(function(method, url, data) {
                    data = angular.fromJson(data);
                    return responseUserPermission(data);
                });

            $httpBackend.whenPOST(apiUrl + "forgotPassword")
                .respond(function(method, url, data) {
                    data = angular.fromJson(data);
                    return responseCodesBasedOnEmail(data);
                });
            $httpBackend.whenPOST(apiUrl + "login/resetpassword")
                .respond(function(method, url, data) {
                    data = angular.fromJson(data);
                    return responseCodesBasedOnEmail(data);
                });


            $httpBackend.whenGET(/urlparams\/decrypt/)
                .respond(function(method, url) {

                    var resp = {},
                        match = /\?(.*)/.exec(url),
                        query = match[1],
                        encryptedParams = getQueryVar(query,'encryptedParams');

                    if (encryptedParams == "valid") {
                        // yes we found you, all ok to go ahead and reset your password
                        console.log('%cvalid', 'color:Red');
                        resp = {
                            "responseInfo": {
                                "code": 0,
                                "desc": "success"
                            },
                            "data": {
                                "myAudiId": "3456345",
                                "email": "success@salmon.com",
                                "forename": "John",
                                "surname": "Stewart",
                                "title": "Mr",
                                "verifyKey": "fuihf89y883f79h"
                            }
                        }
                    }
                    if (encryptedParams == "expired") {
                        // we know who you are but you clicked the link to late. We have auto resent the email. 
                        resp = {
                            "responseInfo": {
                                "code": 103,
                                "desc": "User token expired"
                            }
                        }
                    }
                    if (encryptedParams == "invalid") {
                        // we don't know who you are or what you want!
                        resp = {
                            "responseInfo": {
                                "code": 104,
                                "desc": "User token invalid"
                            }
                        }
                    }
                    console.log('resp', resp)

                    return [200, resp, {}];
                });

            $httpBackend.whenGET(/users\/lookup\//)
                .respond(function(method, url) {
                    var match = /\?(.*)/.exec(url),
                        query = match[1],
                        data = { email: getQueryVar(query,'email') };
                    return responseCodesBasedOnEmail(data);
                });
        }

        return {
            setupMocks: setupMocks
        }
    }]);
