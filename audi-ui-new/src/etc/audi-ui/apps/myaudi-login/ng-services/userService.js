

	if(window.console) console.log("userService loaded");

	myAudiLogin.factory('userService', ['$resource', '$http', '$q', '$httpBackend', 'apiService', '$state',
		function ($resource, $http, $q, $httpBackend, apiService, $state) {

			var userModel = {}, //stores main user response
				verifyEmailLoading = false,
				disableEmailInput = false;

			var login = function (email, password) {

				return new apiService.withErrorHandling({
					title: 'Login',
					url: 'login',
					method: 'POST',
					data: {
						email: email,
						password: password,
					},
					contentType: 'form'
				});
			};

			var urlparamsDecrypt = function (urlparams) {
				return new apiService.withErrorHandling({
					title: 'urlparamsDecrypt',
					url: 'urlparams/decrypt?data=' + encodeURIComponent(urlparams),
					method: 'GET'
				});
			}

			var resetPasswordRequest = function (email) {
				return new apiService.withErrorHandling({
					title: 'resetPasswordRequest',
					url: 'login/resetpassword/request',
					method: 'POST',
					data: { email: email }
				});
			};

			var resetPassword = function (data) {
				return new apiService.withErrorHandling({
					title: 'resetPassword',
					url: 'login/resetpassword/reset',
					method: 'POST',
					data: data
				}).then(function (data) {
					angular.copy(data.data, userModel);
					console.log('%c userModel ', 'background:red; color:#fff' , userModel)
				});
			};

			var register = function (data) {

				data.channelCode = "WEB";

				return new apiService.withErrorHandling({
					title: 'Register',
					url: "users",
					method: 'POST',
					data: data
				})
				.then(function (data) {
					angular.copy(data.data, userModel);
					console.log('%c userModel ', 'background:red; color:#fff' , userModel)
				});
			};

			var usersPermission = function (OptInValue) {

				console.log('usersPermission() called with OptInValue = ' + OptInValue);
				
				return new apiService.withErrorHandling({
					title: 'Users Permission',
					url: "users/permission",
					method: 'POST',
					data: {
						email: userModel.email,
						marketingPermissionKey: userModel.marketingPermissionKey,
						emailOptOut: OptInValue
					}
				});
			};

			var verifyEmailRequest = function (email) {

				console.log("userService.verifyEmail() called with:", email);
				userModel.email = email;

				verifyEmailLoading = true;
				return new apiService.withErrorHandling({
					title: 'Verify email',
					url: "user/verify/request",
					method: 'POST',
					data: {
						email: email
					}
				})
				.then(function () {
					verifyEmailLoading = false;
					$state.go('account.signupResentVerification');
				}, function () {
					verifyEmailLoading = false;
				});
			};

			var verifyEmailVerified = function (verifyKey, email) {

				console.log("verifyEmailVerified called with:", verifyKey, email);

				return new apiService.withErrorHandling({
					title: 'Verify email - verified',
					url: "user/verify/verified",
					method: 'POST',
					data: {
						verifyKey: verifyKey,
						email: email
					}
				});
			};

			var lookupEmail = function (email) {

				console.log("userService.lookupEmail() called with:", email);

				return new apiService.withErrorHandling({
					title: 'Lookup email',
					url: 'users/lookup/',
					method: 'GET',
					params: { email:email }
				});
			};

			return {
				login: login,
				urlparamsDecrypt: urlparamsDecrypt,
				resetPasswordRequest: resetPasswordRequest,
				resetPassword: resetPassword,

				register: register,
				usersPermission: usersPermission,
				verifyEmailRequest: verifyEmailRequest,
				lookupEmail: lookupEmail,
				userModel: userModel,
				disableEmailInput:disableEmailInput,
				verifyEmailVerified: verifyEmailVerified,

				verifyEmailLoading: verifyEmailLoading
			}
		}
	]);

