module.exports = {
	options: {
		separator: "\n\n",
		banner: "(function(){\n\n'use strict';\n\n",
		footer: "\n\n})();",
		sourceMap: false
	},
	src: [  
 

		'src/etc/audi-ui/apps/myaudi-login/myaudilogin.js',

		'src/etc/audi-ui/apps/myaudi-login/ng-views/mainCtrl.js',
		'src/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/loginCtrl.js',
		'src/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/logoutCtrl.js',
		'src/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/accountVerifyCtrl.js',

		'src/etc/audi-ui/apps/myaudi-login/ng-components/accountSignup/signupCtrl.js',

		'src/etc/audi-ui/apps/myaudi-login/ng-components/resetPassword/forgotPasswordCtrl.js',
		'src/etc/audi-ui/apps/myaudi-login/ng-components/resetPassword/resetPasswordCtrl.js',

		'src/etc/audi-ui/apps/myaudi-login/ng-services/apiService.js',
		'src/etc/audi-ui/apps/myaudi-login/ng-services/apiServiceMocks.js',
		'src/etc/audi-ui/apps/myaudi-login/ng-services/userService.js',

		'src/etc/audi-ui/apps/myaudi-login/ng-directives/salFocus.js',
		'src/etc/audi-ui/apps/myaudi-login/ng-directives/salEmailUnique.js',
		'src/etc/audi-ui/apps/myaudi-login/ng-directives/salFormFeedbackIcons/directive.js',
		'src/etc/audi-ui/apps/myaudi-login/ng-directives/salPopoverHtmlUnsafe/directive.js',
		'src/etc/audi-ui/apps/myaudi-login/ng-directives/salPasswordRepeat.js',
		'src/etc/audi-ui/apps/myaudi-login/ng-directives/salPasswordValidate.js',
		'src/etc/audi-ui/apps/myaudi-login/ng-directives/salPasswordPopover.js',
		'src/etc/audi-ui/apps/myaudi-login/ng-directives/salPasswordFocus.js',

	
	 
	],
	dest: 'src/etc/audi-ui/apps/myaudi-login/myaudilogin.min.js'
};
