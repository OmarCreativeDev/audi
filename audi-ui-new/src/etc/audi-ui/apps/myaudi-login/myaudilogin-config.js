define(['myAudiLogin', 'ng-mocks'], function (myAudiLogin, ngMock) {

    'use strict';

    myAudiLogin.appConfig = {
        useMocks: true,
        urls: {}
    }

    var host = window.location.hostname;

    var init = function () {

        var getRegexMatch = function( regex, string ) {
            var match = regex.exec( string );
            return match[1];
        }

        // Example:
        //   if
        //     window.location.href == "http://127.0.0.1:9090/etc/audi-ui/myaudi-login.html#!/account/login",
        //   then:
        //     baseUrl = http://127.0.0.1:9090/
        //     appsUrl = http://127.0.0.1:9090/etc/audi-ui/
        //     appUrl  = http://127.0.0.1:9090/etc/audi-ui/myaudi-login.html

        var baseUrl = getRegexMatch( /(^http[s]?:\/\/[^\/]*\/)/gi, window.location.href ),
            appsUrl = getRegexMatch( /(^http[s]?:\/\/[^#]*\/)/gi,  window.location.href ),
            appUrl  = getRegexMatch( /(^http[s]?:\/\/[^#]*)/gi,    window.location.href );
        
        myAudiLogin.appConfig.urls.base = baseUrl;
        myAudiLogin.appConfig.urls.apps = appsUrl;
        myAudiLogin.appConfig.urls.app  = appUrl;

        myAudiLogin.appConfig.urls.api  = baseUrl + 'myaudi-public-api/services/v1/';

        myAudiLogin.appConfig.urls.account = appsUrl + 'myaudi.html';
        myAudiLogin.appConfig.urls.login   = appsUrl + 'myaudi-login.html';

        if (window.console) console.log("appConfig :", myAudiLogin.appConfig);
    };

    init();

    myAudiLogin.config(['$httpProvider', function ($httpProvider) {
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }])

    myAudiLogin.config(['$locationProvider', function ($locationProvider) {
       $locationProvider.hashPrefix("!");
    }]);

    if ( myAudiLogin.appConfig.useMocks ) {

        myAudiLogin.config(['$provide', function ($provide) {

            $provide.decorator('$httpBackend', ngMock.e2e.$httpBackendDecorator);

            // Delay the mock ajax request to simulate the real thing
            $provide.decorator('$httpBackend', function ($delegate, $timeout) {
                var proxy = function (method, url, data, callback, headers) {
                    var interceptor = function () {
                        var _this = this,
                            _arguments = arguments;
                        $timeout(function () {
                            callback.apply(_this, _arguments);
                        }, 1000);
                    };
                    return $delegate.call(this, method, url, data, interceptor, headers);
                };
                for (var key in $delegate) {
                    proxy[key] = $delegate[key];
                }
                return proxy;
            });
        }])

        myAudiLogin.run(['$httpBackend', function ($httpBackend) {

            // Add here regular expression for all URLs which should be passed through in the mocks
            // dont allow /v1/users/permission as it needs to be mocked - back end not ready

            var passThrough = [
                { method: "GET",  regex: /ng-views\/.*/ },
                { method: "GET",  regex: /ng-components\/.*/ },
                { method: "GET",  regex: /ng-directives\/.*/ },
                { method: "GET",  regex: /webservices.audi.co.uk.uat.nativ-systems.com\/.*/ },
                { method: "POST", regex: /v1\/login\/*/ },
                { method: "GET",  regex: /v1\/urlparams\/*/ },
                { method: "POST", regex: /v1\/user\/verify\/request/ },
                { method: "POST", regex: /v1\/user\/verify\/verified/ },
                { method: "GET",  regex: /v1\/users\/lookup\// },
                { method: "POST", regex: /v1\/users$/ }
            ];

            for (var i = 0; i < passThrough.length; i++) {
                var method = passThrough[i].method,
                    regex  = passThrough[i].regex;
                $httpBackend.when(method, regex).passThrough();
            }

        }]);        

    }
});