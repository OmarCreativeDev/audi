
define( ['myAudiLogin'], function (myAudiLogin) {

	'use strict';

	myAudiLogin.config(['$stateProvider', '$urlRouterProvider',
		function ($stateProvider, $urlRouterProvider, $analyticsProvider) {

			// https://github.com/angular-ui/ui-router/wiki/Nested-States-%26-Nested-Views
			// https://github.com/angular-ui/ui-router/wiki/Multiple-Named-Views
			$stateProvider

				// "learn more" parent state
				.state('learnMore', {
					url: '/learnmore',
					abstract: true,
					templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-views/wrapper.html',
					controller: "mainCtrl"
				})
				.state('learnMore.landing', {
					url: '/landing',
					views: {
						'': {
							templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/loginForm.html',
							controller: 'loginCtrl'
						},
						'learnMoreView@learnMore': {
							templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/learnMore.html'
						}
					}
				})
				.state('learnMore.landing.accountDeleted', {
					url: '/:accountDeleted',
					views: {
						'': {
							templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/loginForm.html',
							controller: 'loginCtrl'
						},
						'learnMoreView@learnMore': {
							templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/learnMore.html'
						},
						'messageView@learnMore': {
							templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/accountDeleted.html',
							controller: 'logoutCtrl'
						}
					}
				})

				// "account" parent state
				.state('account', {
					url: "/account",
					abstract: true,
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-views/wrapper.html",
					controller: "mainCtrl"
				})
				.state('account.login', {
					url: "/login",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/loginForm.html",
					controller: "loginCtrl"
				})
				.state('account.login.verify', {
					url: "/verify",
					views: {
						'messageView@account': {
							templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/accountVerify.html",
							controller: "accountVerifyCtrl"
						}
					}
				})
				.state('account.verificationExpired', {
					url: '/verificationEmailExpired',
					templateUrl: '/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/verificationExpired.html',
					controller: ['$scope', 'userService', function($scope, userService){
						$scope.userService = userService;
					}]
				})
				.state('account.signup', {
					url: "/signup",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/accountSignup/signup.html",
					controller: "signupCtrl"
				})
				.state('account.signupConfirm', {
					url: "/signupconfirm",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/accountSignup/signupConfirm.html",
					controller: "signupCtrl"
				})
				.state('account.signupResentVerification', {
					url: "/resentverification",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/accountSignup/signupResentVerification.html",
					controller: "signupCtrl"
				})
				.state('account.forgotpassword', {
					url: "/forgotpassword",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/resetPassword/forgotPassword.html",
					controller: "forgotPasswordCtrl"
				})
				.state('account.resetPassword', {
					url: "/resetpassword/:urlParam",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/resetPassword/resetPassword.html",
					controller: "resetPasswordCtrl"
				})
				.state('account.logout', {
					url: "/logout",
					templateUrl: "/etc/audi-ui/apps/myaudi-login/ng-components/accountLogin/logout.html",
					controller: "logoutCtrl"
				})

			/* default app route */
			$urlRouterProvider.otherwise("/account/login");
		}
	]);
});
