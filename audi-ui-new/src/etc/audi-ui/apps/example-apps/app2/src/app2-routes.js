app2.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

	console.log('app2 : configuring states');

	$stateProvider
		.state('app2_stateOne', {
			url: '/app2-state-one',
			views: {
				'mainAppView': {
					controller: 'app2.stateOneController',
					templateUrl: '/etc/audi-ui/apps/example-apps/app2/tmpl/state-one-template.html'
				},
			}
		})
		.state('app2_stateTwo', {
			url: '/app2-state-two',
			views: {
				'mainAppView': {
					controller: 'app2.stateTwoController',
					templateUrl: '/etc/audi-ui/apps/example-apps/app2/tmpl/state-two-template.html'
				},
			}
		});

}]);