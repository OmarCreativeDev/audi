module.exports = {
	options: {
		separator: "\n\n",
		//banner: "define(['ng'], function (angular) {\n\n'use strict';\n\n",
		//footer: "\n\n});",
		banner: "(function(){\n\n'use strict';\n\n",
		footer: "\n\n})();",
		sourceMap: true,
		sourceMapStyle: 'inline'
	},
	src: [
		'src/etc/audi-ui/apps/example-apps/app2/src/app2.js',
		'src/etc/audi-ui/apps/example-apps/app2/src/app2-config.js',
		'src/etc/audi-ui/apps/example-apps/app2/src/app2-routes.js',
		'src/etc/audi-ui/apps/example-apps/app2/src/app2-controller1.js',
		'src/etc/audi-ui/apps/example-apps/app2/src/app2-controller2.js'
	],
	dest: 'src/etc/audi-ui/apps/example-apps/app2/dist/app2.js',
};
