(function(){

//define(['ng'], function (angular) {

	'use strict';

	console.log('app3 loaded');

	var app3 = angular.module('app3', []);

	app3.directive('testDirectiveOne', function() {
		return {
			restrict: 'EA',
			replace: true,
			templateUrl: '/etc/audi-ui/apps/example-apps/app3/directive-one-template.html',
			controller: function ($scope) {
				console.log('testDirectiveOne controller executed');
				$scope.hello = 'Hi, I\'m testDirectiveOne controller';
			}
		};
	});

//});

})();
