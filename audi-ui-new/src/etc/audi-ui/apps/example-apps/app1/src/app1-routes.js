app1.config(['$stateProvider', '$urlRouterProvider',
	function ($stateProvider, $urlRouterProvider) {

		console.log('app1 : configuring states');

		$stateProvider
			.state('app1_stateOne', {
				url: '/app1-state-one',
				views: {
					'mainAppView': {
						controller: 'app1.stateOneController',
						templateUrl: '/etc/audi-ui/apps/example-apps/app1/tmpl/state-one-template.html'
					},
				}
			})
			.state('app1_stateTwo', {
				url: '/app1-state-two',
				views: {
					'mainAppView': {
						controller: 'app1.stateTwoController',
						templateUrl: '/etc/audi-ui/apps/example-apps/app1/tmpl/state-two-template.html'
					},
				}
			});

	}
]);