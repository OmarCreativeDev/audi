module.exports = {
	options: {
		separator: "\n\n",
		//banner: "define(['ng'], function (angular) {\n\n'use strict';\n\n",
		//footer: "\n\n});",
		banner: "(function(){\n\n'use strict';\n\n",
		footer: "\n\n})();",
		sourceMap: true,
		sourceMapStyle: 'inline'
	},
	src: [
		'src/etc/audi-ui/apps/example-apps/app1/src/app1.js',
		'src/etc/audi-ui/apps/example-apps/app1/src/app1-routes.js',
		'src/etc/audi-ui/apps/example-apps/app1/src/app1-controller1.js',
		'src/etc/audi-ui/apps/example-apps/app1/src/app1-controller2.js'
	],
	dest: 'src/etc/audi-ui/apps/example-apps/app1/dist/app1.js',
};
