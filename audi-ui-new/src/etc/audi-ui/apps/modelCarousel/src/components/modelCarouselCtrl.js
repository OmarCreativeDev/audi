

modelCarousel.filter('partition', function($cacheFactory) {
    var arrayCache = $cacheFactory('partition')
    return function(arr, size) {
        try {

            var parts = [],
                cachedParts,
                jsonArr = JSON.stringify(arr);
            for (var i = 0; i < arr.length; i += size) {
                parts.push(arr.slice(i, i + size));
            }
            cachedParts = arrayCache.get(jsonArr);
            if (JSON.stringify(cachedParts) === JSON.stringify(parts)) {
                return cachedParts;
            }
            arrayCache.put(jsonArr, parts);

        } catch (e) {
            parts = arr;
        }
        return parts;
    };
});


    modelCarousel.directive('modelCarousel', ['modelCarouselService', '$timeout','$analytics',
        function(modelCarouselService, $timeout, $analytics) {
            return {
                restrict: 'EA',
                replace: true, 
                 scope: {
                    params: '@'
                }, 
                templateUrl: '/etc/audi-ui/apps/modelCarousel/tmpl/modelCarousel.html',
                controller: function($scope, $timeout) {
                    
                    $scope.params = $scope.$eval($scope.params);
                    $scope.carouselID = "carousel_"+makeid(10) //make unique for each instance
                    

                    var ModelCarouselService = new modelCarouselService($scope.params);

                    $scope.classificationNodes = ModelCarouselService.classificationNodes; // holds the Classification Nodes
                    $scope.currentItems = [];
                    $scope.ctaButton = $scope.params.ctaButton;
                    $scope.navOverrideText = "";
                    if(typeof $scope.params.navOverrideText ==="string"){  
                        $scope.navOverrideText = $scope.params.navOverrideText
                    }   
                    // name for analytics
                    if(typeof $scope.params.carouselname ==="string"){  
                        $scope.carouselname = "carousel_"+ $scope.params.carouselname
                    } else{
                        $scope.carouselname = "carousel_" + window.location.pathname
                    }

                    $scope.carouselMode= true

                   if(typeof $scope.params.carouselMode ==="boolean"){  
                         $scope.carouselMode =$scope.params.carouselMode;
                    }   
        
        
 

                

                    var init = function() {
                    
                       
                        console.log('attr:::',$scope.params)
                  
                        ModelCarouselService.loadModels().then(function(response) {

                            $scope.setCars($scope.classificationNodes[0])

                        }, function(response) {
                            // error

                        });

                    }
                    var currentNodeCode ="";
                   
                    $scope.setCars = function(node,e) {
                        if(typeof e !="undefined"){
                            e.preventDefault();
                        }

                        // if same item then dont do anything
                        if(node.code==currentNodeCode){
                           return false;
                        }
                        currentNodeCode = node.code;

                        // mark the nav as active
                        angular.forEach($scope.classificationNodes, function(item) {
                            item.active = false;
                        })
                        node.active = true;

                        if($scope.carouselMode){

                                                // setup the carousel and animate in
                                        if (typeof $('#'+$scope.carouselID).data('microfiche') === 'undefined') {
                                           
                                            $('#'+$scope.carouselID).fadeTo(0, 0) // fade out the cars (or more to the point where the cars will be rendered)
                                            angular.copy(node.classifiedItems, $scope.currentItems)

                                                // kick off microfiche
                                                $('#'+$scope.carouselID).microfiche({
                                                    noScrollAlign: 'center',
                                                    bullets: false,
                                                    refreshOnResize: true,
                                                    prevButtonLabel: '',
                                                    nextButtonLabel: ''
                                                });

                                                fadeItemsIn();

                                        } else {        
                                        // if a carousel is already setup then we just need to fade the current 
                                        // view out then copy the data in it and fade it back in

                                                  $('#'+$scope.carouselID).fadeTo(300, 0)
                                            $timeout(function() {
                                                // change the dom
                                                angular.copy(node.classifiedItems, $scope.currentItems)
                                                //fade new items back in
                                                fadeItemsIn();
                                            }, 305)

                                       
                                        }


                        }else{// no carousel mode
                                $('#'+$scope.carouselID).fadeTo(150, 0) 
                                            $timeout(function() {
                                                // change the dom
                                                angular.copy(node.classifiedItems, $scope.currentItems)
                                                //fade new items back in
                                                $('#'+$scope.carouselID).fadeTo(150, 1) 
                                            }, 155)
                        }

                      
                     


                    }




                    var  fadeItemsIn = function(){

                                // update current stuff
                                $timeout(function() {
                                    // get microfiche to update
                                    $('#'+$scope.carouselID).microfiche({
                                        refresh: true
                                    })

                                    $('#'+$scope.carouselID).fadeTo(0, 0);

                                    var carousel = $('#'+$scope.carouselID).data('microfiche');
                                    if (carousel.totalPageCount() > 1) {
                                        $timeout(function() {

                                            //jump to page two then slide back to the first page. Giving the effect of the cars driving on then off the screen
                                            $('#'+$scope.carouselID).microfiche({
                                                jumpToPage: 2
                                            });
                                            // now 
                                            $('#'+$scope.carouselID).microfiche({
                                                slideByPages: -1
                                            })
                                            animateDriveIn();
                                        }, 200);


                                    } else {
                                        animateDriveIn()
                                    }

                                }, 20); //wait for microfich to update
                        }


                    var animateDriveIn = function() {
                        // TODO
                        $('#'+$scope.carouselID).fadeTo(0, 0).fadeTo(800, 1)

                        var totalItems = $('#'+$scope.carouselID +' li').length;
                        $('#'+$scope.carouselID +' li').each(function(inde) {
                            $(this).css({
                                left: '-700px',
                                opacity: 0,
                                position: 'relative'
                            })
                            $(this).delay(80 * totalItems).animate({
                                left: 0,
                                opacity: 1
                            }, 800, function(x, t, b, c, d) {
                                return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
                            });


                            var label = $(this).find('.modelLabel')
                            label.css({
                                top: '-50px',
                                position: 'relative',
                                opacity: 0
                            })
                            label.delay((20 * totalItems) + 600).animate({
                                top: 0,
                                opacity: 1
                            }, 300, function(x, t, b, c, d) {
                                return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
                            });




                            totalItems--;
                        })

                    }

                function makeid(len)
                {
                    var text = "";
                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                    for( var i=0; i < len; i++ )
                        text += possible.charAt(Math.floor(Math.random() * possible.length));

                    return text;
                }


                    init();
                }
            };
        }
    ]);


    modelCarousel.directive('fallbackSrc', function() {
        var fallbackSrc = {
            link: function postLink(scope, iElement, iAttrs) {
                iElement.bind('error', function() {
                    angular.element(this).attr("src", iAttrs.fallbackSrc);
                });
            }
        }
        return fallbackSrc;
    });

    modelCarousel.directive('imgPreload', function() {
        return {
            restrict: 'A',
            scope: {
                ngSrc: '@'
            },
            link: function(scope, element, attrs) {
                element.on('load', function() {
                    console.log('loading.....', element)
                    element.addClass('in');
                }).on('error', function() {
                    //
                });

                scope.$watch('ngSrc', function(newVal) {
                    element.removeClass('in');
                });
            }
        };
    });



modelCarousel.filter('partition', function($cacheFactory) {
    var arrayCache = $cacheFactory('partition')
    return function(arr, size) {
        try {

            var parts = [],
                cachedParts,
                jsonArr = JSON.stringify(arr);
            for (var i = 0; i < arr.length; i += size) {
                parts.push(arr.slice(i, i + size));
            }
            cachedParts = arrayCache.get(jsonArr);
            if (JSON.stringify(cachedParts) === JSON.stringify(parts)) {
                return cachedParts;
            }
            arrayCache.put(jsonArr, parts);

        } catch (e) {
            parts = arr;
        }
        return parts;
    };
});
