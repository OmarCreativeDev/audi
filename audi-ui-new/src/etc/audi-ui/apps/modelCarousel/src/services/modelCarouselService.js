
	if(window.console) console.log("modelCarouselService loaded");

	modelCarousel.factory('modelCarouselService', ['$http', '$q', function ($http, $q) {

		  return function(params){

			var classificationNodes = []; 
	

			var loadModels = function() {
				var deferred = $q.defer();

					var url = ""

					var collapseRange = false;
					var nodeCode = "nodes";
					if(typeof params.collapseRange ==="boolean"){
						collapseRange = params.collapseRange;
					}
					if(typeof params.nodeCode ==="string"){
						nodeCode = params.nodeCode;
					}

					if(typeof params.schemeCode ==="string"){
						url = "http://audi-dailybuild-web.salmon.com:30067/pdb-override/rest/schemes/"+params.schemeCode+"/"+nodeCode+"/models?collapseRange="+collapseRange
					}else{
							deferred.reject('schemeCode required');
					}
	
					console.log('url:',url)
				
                $http({
                    method: 'GET',
                    url:'/etc/audi-ui/apps/modelCarousel/mockData/models.js?scheme='+params.scheme
                    // url:'http://audi-dailybuild-web.salmon.com:30067/pdb-override/rest/schemes/TypeOfCar/nodes/models&colapseRange=true'
                })
                    .success(function(data, status) {
						               
						angular.copy(data.data.classificationNodes, classificationNodes); 


						deferred.resolve();
                    })
                    .error (function(data, status) {
						deferred.reject(data);
                    });

                return deferred.promise;
            }

			return {
				classificationNodes: classificationNodes, 
				loadModels: loadModels
			};
		}
		}
	]);
