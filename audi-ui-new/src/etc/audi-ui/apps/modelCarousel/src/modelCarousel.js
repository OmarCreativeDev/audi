
console.log('modelCarousel module loaded');


var modelCarousel = angular.module('modelCarousel', [
	'angulartics',
	'angulartics.adobe.analytics'
]);

modelCarousel.config(['$analyticsProvider',
	function ($analyticsProvider) {
		// turn off automatic tracking
		$analyticsProvider.virtualPageviews(false);
	}
]);