module.exports = {
	options: {
		separator: "\n\n",
		banner: "(function(){\n\n'use strict';\n\n",
		footer: "\n\n})();",
		sourceMap: false
	},
	src: [ 
 
		// osb Module
		'src/etc/audi-ui/apps/modelCarousel/src/modelCarousel.js',

		// Services
		'src/etc/audi-ui/apps/modelCarousel/src/services/modelCarouselService.js',

		// Conrollers
		'src/etc/audi-ui/apps/modelCarousel/src/components/modelCarouselCtrl.js',

		'src/etc/audi-ui/global/third-party-plugins/microfiche/microfiche.js'


	],
	dest: 'src/etc/audi-ui/apps/modelCarousel/dist/modelCarousel.js'
};
