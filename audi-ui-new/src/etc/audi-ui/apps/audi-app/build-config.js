module.exports = {
	options: {
		separator: "\n\n",
		banner: "(function(){\n\n'use strict';\n\n",
		footer: "\n\n})();",
		sourceMap: true,
		sourceMapStyle: 'inline'
	},
	src: [
		// The Audi App
		'src/etc/audi-ui/apps/audi-app/src/audiApp.js'
	],
	dest: 'src/etc/audi-ui/apps/audi-app/dist/audiApp.js',
};
