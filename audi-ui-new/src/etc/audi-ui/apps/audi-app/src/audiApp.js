
var app = angular.module('audiApp', [
	'oc.lazyLoad',
	'ui.router',
	'sa.utils',
	'angulartics',
	'angulartics.adobe.analytics'
]);

app.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
	console.log('audiApp - config');
	$ocLazyLoadProvider.config({
		loadedModules: ['audiApp'],
		debug: false
	});
}]);

app.config(['$analyticsProvider',
	function ($analyticsProvider) {
		// turn off automatic tracking
		$analyticsProvider.virtualPageviews(false);
	}
]);

app.config(['$httpProvider', function ($httpProvider) {
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);

/*
app.config(['$locationProvider', function ($locationProvider) {
	$locationProvider.hashPrefix('!');
}]);
*/

///////
// Moved this from myAudi config
///////

app.config(function ($animateProvider) {
	// fix strange issue with the spinner having to compleate a full 360 before being hidden
	$animateProvider.classNameFilter(/^((?!(rotate360)).)*$/);
});

app.config(['$provide', function ($provide) {

	$provide.decorator('$httpBackend', angular.mock.e2e.$httpBackendDecorator);

	// Delay the mock ajax request to simulate the real thing
	$provide.decorator('$httpBackend', function ($delegate, $timeout) {
		var proxy = function (method, url, data, callback, headers) {
			var interceptor = function () {
				var _this = this,
					_arguments = arguments;
				$timeout(function () {
					callback.apply(_this, _arguments);
				}, 300);
			};
			return $delegate.call(this, method, url, data, interceptor, headers);
		};
		for (var key in $delegate) {
			proxy[key] = $delegate[key];
		}
		return proxy;
	});
}]);

app.run(['$httpBackend', function ($httpBackend) {

	// Add here regular expression for all URLs which should be passed trhough in the mocks
	var passThrough = [
		{ method: 'GET', regex: /\.html/ },
		{ method: 'GET', regex: /views\/.*/ },
		{ method: 'GET', regex: /mockData\/.*/ },
		{ method: 'GET', regex: /components\/.*/ },
		{ method: 'GET', regex: /directives\/.*/ },
		{ method: 'GET', regex: /webservices.audi.co.uk.uat.nativ-systems.com\/.*/ },
		{ method: 'GET', regex: /postcodeLookup\/.*/ },
		{ method: 'GET', regex: /v1\/messages/ },
		{ method: 'GET', regex: /audi-mediaservice\/.*/ },
		{ method: 'DELETE', regex: /v1\/messages/ },
		{ method: 'POST', regex: /v1\/messages/ },
		{ method: 'GET', regex: /content\/.*/ },
		{ method: 'DELETE', regex: /v1\/users$/ }
	];

	for (var i = 0; i < passThrough.length; i++) {
		var method = passThrough[i].method,
			regex  = passThrough[i].regex;
		$httpBackend.when(method, regex).passThrough();
	}

}]);

///////
// end of code moved from myAudi config
///////

app.controller('audiAppController', ['$scope', '$ocLazyLoad', '$state','$compile', function($scope, $ocLazyLoad, $state,$compile) {

	console.log('called audiAppController');

	$scope.ocLazyUrls = {
		modelCarousel : ['/etc/audi-ui/apps/modelCarousel/dist/modelCarousel.js'],
		financeCalculator: ['/etc/audi-ui/apps/finance-calculator/financeCalculator.min.js'],
		osb: ['/etc/audi-ui/apps/osb/osb.min.js'],
		myAudiLogin: ['/etc/audi-ui/apps/myaudi-login/myaudilogin.min.js'],
		myAudiHeader: ['/etc/audi-ui/apps/myaudi-header/myaudiheader.min.js'],
		myAudi: ['/etc/audi-ui/apps/myaudi/dist/myaudi.js']
	}




$scope.injectContent = function(target,htmlcontents){

// angular.element('body').scope().injectContent('.injectMe','<div oc-lazy-load="{name: \'modelCarousel\', files: ocLazyUrls.modelCarousel}"><div class="osb" model-carousel data-params="{schemeCode:\'fred\'}"></div></div>');
	 $(target).html(
	          $compile(
	            htmlcontents
	          )($scope));
	        $scope.$apply();
}


	var init = function() {

		console.log('called audiAppController:init()');

		// $ocLazyLoad.load({
		// 	name: 'app1',
		// 	files: ['/etc/audi-ui/apps/example-apps/app1/dist/app1.js']
		// }).then(function(){
		// 	$state.go('app1_stateOne');
		// }, function(e){
		// 	console.log('Error loading app1');
		// });
	};

	init();
}]);






		(function(){
			$('body').attr( 'ng-controller', 'audiAppController' );
			var el = document.body;
			angular.element(el).ready(function() {
				angular.bootstrap(el, ['audiApp']);
			});
		})();
