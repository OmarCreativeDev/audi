
      osb.factory('osbService', ['$resource', '$http', '$q', '$httpBackend', '$timeout', '$filter',
          function($resource, $http, $q, $httpBackend, $timeout, $filter) {


              var urls_mock = {
                  findDealerships: "/etc/audi-ui/apps/osb/mockData/findDealerships/index.html?location=",
                  getDealerDetails: "/etc/audi-ui/apps/osb/mockData/getDealerDetails/index.html?code=",
                  getServicesByDealerCodeCarReg: "/etc/audi-ui/apps/osb/mockData/getServicesByDealerCodeCarReg/index.html?dealerCode=",
                  getFacilities: "/etc/audi-ui/apps/osb/mockData/getFacilities/index.html?dealerCode=",
                  getCarInfoByCarReg: "/etc/audi-ui/apps/osb/mockData/getCarInfoByCarReg/index.html?carReg=",
                  createBooking: "/etc/audi-ui/apps/osb/mockData/createBooking?dealerCode="
              }
              var urls = {

                  findDealerships: "https://soa.audi.co.uk/audi-online-service-booking/osbservices/v1/findDealerships?location=",
                  getDealerDetails: "https://soa.audi.co.uk/audi-dealercentre-service/dealerservice/getdealerdetails?dealerCode=",
                  getServicesByDealerCodeCarReg: "https://soa.audi.co.uk/audi-online-service-booking/osbservices/v1/getServicesByDealerCodeCarReg?dealerCode=",
                  getFacilities: "https://soa.audi.co.uk/audi-online-service-booking/osbservices/v1/getFacilities?dealerCode=",
                  getCarInfoByCarReg: "https://soa.audi.co.uk/audi-online-service-booking/osbservices/v1/getCarInfoByCarReg?carReg=",
                  createBooking: "/etc/audi-ui/apps/osb/mockData/createBooking?dealerCode="
              }


              var user = {
                  facilities: {
                      option: 'dropOff',
                      showDatePicker: 'loading'
                  },
                  selectedDateTime: {
                      date: "", //"2014-11-04T00:00:00.000Z",
                      timeSlot: ""
                  },
                  profileData: {
                      //name address etc
                      marketingByPost: "1",
                      marketingByTelephone: "1",
                      marketingByEmail: "1",
                      marketingBySMS: "1"
                  }
              };

              var dealers = [];
              var dealer = {
                  details: {},
                  services: [],
                  facilities: {},
                  availabilityFetched: [],
                  selectedDay: {}
              };
              var car = {};
              var calendarDates = [];


              var getDealers = function(usersLocation) {

                  console.log('++++++getDealers', usersLocation)
                  var deferred = $q.defer();
                  $http({
                      method: 'GET',
                      url: urls.findDealerships + usersLocation
                  })
                      .success(function(result, status) {
                          console.log('getDealers DONE', result, dealers);
                          angular.forEach(result.dealers, function(dealer) {
                              dealer.osbAvailable = true
                          })
                          angular.copy(result.dealers, dealers);
                          deferred.resolve();
                      }).
                  error(function(data, status) {
                      console.log('error with dealerships', result);
                      deferred.reject('error getDealers');
                  });

                  return deferred.promise;

              }








/* dealers 
=========================================================================== */
              var getDealerDetails = function(dealerCode) {
               clearDateCache()
                  console.log('getDealerDetails')
                  var deferred = $q.defer();
                  $http({
                      method: 'GET',
                      url: urls.getDealerDetails + dealerCode
                  })
                      .success(function(result, status) {
                          console.log('dealer', result);
                          result.services = [];
                          angular.copy(result, dealer.details);

                          deferred.resolve();
                      }).
                  error(function(data, status) {
                      console.log('error with dealer', result);
                      deferred.reject();
                  });

                  return deferred.promise;


              }




              var getServicesByDealerCodeCarReg = function(dealerCode, vrm) {

                  console.log('getCarInfoByCarReg', dealerCode, vrm)
                  var deferred = $q.defer();
                  $http({
                      method: 'GET',
                      url: urls.getServicesByDealerCodeCarReg + dealerCode + "&carReg=" + vrm
                  })
                      .success(function(result, status) {
                          if (typeof dealer.services === "undefined") {
                              dealer.services = [];
                          }



                          angular.copy(result.servicePrices, dealer.services);
                          deferred.resolve();
                      }).
                  error(function(data, status) {
                      console.log('error with car', result);
                      deferred.reject();
                  });

                  return deferred.promise;


              }



              var getFacilities = function(dealerCode) {

                  console.log('getFacilities')
                  var deferred = $q.defer();
                  $http({
                      method: 'GET',
                      url: urls.getFacilities + dealerCode
                  })
                      .success(function(result, status) {
                          console.log('services', result);
                          angular.copy(result.dealerFacilities, dealer.facilities);

                          deferred.resolve();
                      }).
                  error(function(data, status) {
                      console.log('error with car', result);
                      deferred.reject();
                  });

                  return deferred.promise;


              }




/* car info
=========================================================================== */

              var getCarInfoByCarReg = function(carReg) {

                  console.log('getCarInfoByCarReg')
                  var deferred = $q.defer();
                  $http({
                      method: 'GET',
                      url: urls.getCarInfoByCarReg + carReg
                  })
                      .success(function(result, status) {
                          if (result.success) {
                              console.log('=====car found', result);
                              angular.copy(result.items[0], car);
                              deferred.resolve();
                          } else {
                              console.log('=====car NOT FOUND', result);
                              deferred.reject('error getCarInfoByCarReg, not found');
                          }
                      }).
                  error(function(data, status) {
                      console.log('======error with car', result);
                      deferred.reject('error getCarInfoByCarReg');
                  });

                  return deferred.promise;


              }





/* avalability
=========================================================================== */

              /* Select a date time page */

              var inDateCache = function(startDate) {
                  return (dealer.availabilityFetched.indexOf(startDate) == -1) ? false : true
              }

              var clearDateCache = function() {
                  console.log(' - - - clearDateCache')
                  dealer.availabilityFetched = [];
              }

              var formatDateForUrl = function(date) {
                  return $filter('date')(date.getTime(), 'dd-MM-yyyy');
              }

              var getMonthAhead = function(monthAhead) {

                  var now = new Date();
                  if (now.getMonth() == 11) {
                      var current = new Date(now.getFullYear() + monthAhead, 0, 1);
                  } else {
                      var current = new Date(now.getFullYear(), now.getMonth() + monthAhead, 1);
                  }
                  console.log('getMonthAhead:', monthAhead, current)
                  return formatDateForUrl(current);

              }

              var sortCalendarAvailabilityByDate = function(){

                      dealer.availability= $filter('orderBy')(dealer.availability, 'availableDate')
               

              }
              var getCalendarAvailability = function() {
                  var deferred = $q.defer();


                  // fetch this month and the following 3 months

            
/*

                  getAvailability(formatDateForUrl(new Date())).then(function(){

                      sortCalendarAvailabilityByDate();
                      filterAvailability(user.facilities.option).then(function(){
                            deferred.resolve();
                      })

                  })


                    getAvailability(getMonthAhead(1)).then(function(){
                    
                      sortCalendarAvailabilityByDate();
                      filterAvailability(user.facilities.option).then(function(){
                            deferred.resolve();
                      })

                  })



                 getAvailability(getMonthAhead(2)).then(function(){
                    
                      sortCalendarAvailabilityByDate();
                      filterAvailability(user.facilities.option).then(function(){
                            deferred.resolve();
                      })

                  })

                   getAvailability(getMonthAhead(3)).then(function(){
                    
                      sortCalendarAvailabilityByDate();
                      filterAvailability(user.facilities.option).then(function(){
                            deferred.resolve();
                      })

                  })
                 
*/


                  $q.all([

                      getAvailability(formatDateForUrl(new Date())),
                      getAvailability(getMonthAhead(1)),
                      getAvailability(getMonthAhead(2)),
                      getAvailability(getMonthAhead(3)),
                  ]).then(function() {
                    console.log('1 dealer.availability[0]',dealer.availability[0])
                      

                      sortCalendarAvailabilityByDate();

                      filterAvailability(user.facilities.option);


                      deferred.resolve();

                      // TODO: add in check to make sure there is a aleast one valid date...
                  },function(){
                     deferred.reject('error_loading_date_info');
                  })

                  return deferred.promise;

              }


              // two below do the same thing for two differnig things 
              // lookup from server format
              var getSelectedServiceCodesForLookup = function() {
                      var serviceCodesArray = [];
                      angular.forEach(serviceObject, function(group) {
                          angular.forEach(group.items, function(item) {
                              if (item.checked) {
                                  serviceCodesArray.push("serviceCodes=" + item.serviceCode);
                              }
                          })
                      })
                      var serviceCodesStr = serviceCodesArray.join("&")
                      console.log('serviceCodesStr', serviceCodesStr)
                      return serviceCodesStr
                  }
                  // store in query string in users browser
              var getSelectedServiceCodes = function() {
                  var serviceCodesArray = [];
                  angular.forEach(serviceObject, function(group) {
                      angular.forEach(group.items, function(item) {
                          if (item.checked) {
                              serviceCodesArray.push(item.serviceCode);
                          }
                      })
                  })
                  var serviceCodesStr = serviceCodesArray.join(",")
                  console.log('serviceCodesStr', serviceCodesStr)
                  return serviceCodesStr
              }

              // set from query string to object
              var setSelectedServiceCodes = function(serviceCodes) {
                  console.log('setSelectedServiceCodes', serviceCodes)
                  angular.forEach(serviceObject, function(group) {
                      angular.forEach(group.items, function(item) {
                          if (serviceCodes.indexOf(item.serviceCode) > -1) {
                              item.checked = true;

                          }
                      })
                  })
                  console.log('serviceObject', serviceObject)
              }



              /* server fetch of date info */
              var getAvailability = function(startDate) {

                  var deferred = $q.defer();

                  // see if we have already fetched this data yet- if we have no need to go and get it again.
                  if (!inDateCache(startDate)) {


                      //   serviceCodesStr = "serviceCodes=MOT"
                      var serviceCodesStr = getSelectedServiceCodesForLookup();
                      $http({
                          method: 'GET',
                          //url: "/etc/audi-ui/apps/osb/mockData/getAvailability/" + startDate + ".html?" + serviceCodesStr + "&dealerCode=" + dealer.details.dealerCode
                              url: "https://soa.audi.co.uk/audi-online-service-booking/osbservices/v1/getAvailability?dealerCode="+dealer.details.dealerCode+"&startDate="+startDate+"-00.00&"+ serviceCodesStr

                      })
                          .success(function(result, status) {
                            // create the arrays if first item
                              if (typeof dealer.availability === "undefined") {
                                  dealer.availability = [];
                                  dealer.filteredAvailability = [];
                              }

                              //double check to make sure
                              if (!inDateCache(startDate)) {
                                  angular.forEach(result.availability, function(item) {
                                      dealer.availability.push(item);



                                      //  dealer.filteredAvailability.push(item);
                                  });


                                  dealer.availabilityFetched.push(startDate);
                                  console.log('pushed date into availabilityFetched')
                              }

                              console.log('dealer.availability', dealer.availability)



                              deferred.resolve();
                          }).
                      error(function(data, status) {
                          console.log('error with availability', data);
                          deferred.reject();
                      });
                  } else {
                      deferred.resolve();
                  }
                  return deferred.promise;
              }


              /* not all options need time */
              var isTimeRequired = function() {
                  //see if time is required
                  var requiredTime = ['dropOff', 'whileYouWait', 'courtesyVehicle']
                  return (requiredTime.indexOf(user.facilities.option) == -1) ? false : true
              }


              /* used for next button on wizard on the date time screen */
              var isDateTimeValid = function() {
                  // time required and is compleat
                  if (isTimeRequired()) {
                      if (user.selectedDateTime.date && user.selectedDateTime.timeSlot) {
                          return true
                      } else {
                          return false
                      }

                  } else {
                      user.selectedDateTime.timeSlot = "";
                      if (user.selectedDateTime.date) {
                          return true
                      } else {
                          return false
                      }
                  }

              }


              /* filter the calendar to show only the days that have the selected option */
              var filterAvailability = function(whatFor) {

  var deferred = $q.defer();

                  user.facilities.showDatePicker = 'loading';
                  console.log('filterAvailability', whatFor)
                  var filteredDates = [];

                  switch (whatFor) {
                      case 'dropOff': // WORKS
                          filteredDates = dealer.availability;
                          break;
                      case 'whileYouWait': //works

                          angular.forEach(dealer.availability, function(dateItem) {
                              var timeArray = []; // only times that are valid to wait for will be included
                              angular.forEach(dateItem.availableTimes, function(timeItem, index) {
                                  if (timeItem.whileYouWait == true) {
                                      timeArray.push(timeItem)
                                  }
                              })
                              if (timeArray.length > 0) {
                                  dateItem.availableTimes = timeArray;
                                  filteredDates.push(dateItem);
                              }
                          })

                          break;
                      case 'courtesyVehicle': // WORKS
                          angular.forEach(dealer.availability, function(dateItem) {
                              if (dateItem.courtesyVehicle) {
                                  filteredDates.push(dateItem);
                              }
                          })


                          break;
                      case 'collectionDelivery':
                          angular.forEach(dealer.availability, function(dateItem) {
                             // if (dateItem.colDelPostcodeAreas.indexOf(user.colDelPostcodeArea) > -1) {
                               if (dateItem.colDelPostcodeAreas.length > 0) {
                                  filteredDates.push(dateItem);
                              }
                          })
                          break;
                        case 'collectionDeliveryAnyPostcode':
                          angular.forEach(dealer.availability, function(dateItem) {
                              if (dateItem.colDelPostcodeAreas.length > 0) {
                                  filteredDates.push(dateItem);
                              }
                          })
                          break;
                          
                      case 'courtesyLift': // WORKS
                          angular.forEach(dealer.availability, function(dateItem) {
                              if (dateItem.courtesyLift) {
                                  filteredDates.push(dateItem);
                              }
                          })
                          break;

                  }

                  console.log('calendarDates', filteredDates.length, filteredDates)

                  calendarDates = angular.copy(filteredDates, dealer.filteredAvailability);
                 /* if (whatFor == 'collectionDelivery' && filteredDates.length == 0) {
                      angular.copy([], dealer.filteredAvailability);
                      selectFirstAvailableDateAndTime()
                      user.facilities.showDatePicker = 'loaded';
                  } else ~*/

                  if (filteredDates.length == 0) {
                     deferred.reject();
                      console.error('NO FILTERED DATES');
                    // angular.copy([], dealer.filteredAvailability);

                      user.facilities.showDatePicker = 'loading';
                      // TODO : look ahead 
                      user.facilities.showDatePicker = 'noDates';

                      user.selectedDateTime = {
                          date: "",
                          timeSlot: ""
                      }

                  } else {
                         deferred.resolve();
//calendarDates = angular.copy(filteredDates, dealer.filteredAvailability);

                      selectFirstAvailableDateAndTime()

                      $timeout(function() {
                          user.facilities.showDatePicker = 'loaded';
                      }, 200)



                  }


          
                  return deferred.promise;
              }




              /* select the date from the calendar and show the times */
              var selectDate = function(timestamp) {
                  dealer.selectedDay = {};
                  angular.forEach(dealer.filteredAvailability, function(day) {
                      if (day.availableDate == timestamp) {
                          dealer.selectedDay = day;
                      }
                  });
                  console.log('selectedDateTimeSlots', dealer.selectedDay)
                  // check to see if the user can still have the same time slot as they had before
                  isSelectedTimeSlotAvailable();
              }


              /* check to see if the user can have the same date and time as they had on there other selected date */
              var isSelectedTimeSlotAvailable = function() {

                  if (isTimeRequired()) {

                      console.log('isSelectedTimeSlotAvailable')
                      var isAv = false;
                      // check to see if the time slot is stil valid on another day
                      if (user.selectedDateTime.timeSlot != "") {
                          angular.forEach(dealer.selectedDay.availableTimes, function(timeSlot) {


                              console.log(user.selectedDateTime.timeSlot, timeSlot.time)
                              if (user.selectedDateTime.timeSlot == timeSlot.time) {
                                  isAv = true;
                              }

                          });


                          if (!isAv) {
                              selectFirstTimeSlotForDate();
                          }

                      } else {
                          selectFirstTimeSlotForDate();
                      }
                  } else {
                      // no time required so lets clear the value
                      user.selectedDateTime.timeSlot = ""
                  }


              }


              /* select the first available date from the calendar */
              var selectFirstAvailableDateAndTime = function() {
                  console.warn('============selectFirstAvailableDateAndTime')
                  if (typeof dealer.filteredAvailability[0] === 'object') {
                      console.log('selectFirstAvailableDateAndTime', user.selectedDateTime.date, dealer.filteredAvailability[0].availableDate)
                      if (user.selectedDateTime.date != dealer.filteredAvailability[0].availableDate) {
                          user.selectedDateTime.date = dealer.filteredAvailability[0].availableDate;
                      }

                      selectDate(user.selectedDateTime.date);
                  }
              }
              /* select the first time slot for a date */
              var selectFirstTimeSlotForDate = function() {
                  if (typeof dealer.selectedDay.availableTimes[0] === 'object') {
                      user.selectedDateTime.timeSlot = dealer.selectedDay.availableTimes[0].time
                  }
              }



              /* Calculate the estimated total of all selected services */
              var estimatedTotal = function() {
                  var estimate = 0;
                  angular.forEach(serviceObject, function(group) {
                      angular.forEach(group.items, function(item) {
                          if (item.checked && typeof item.price == "number") {
                              estimate += item.price;
                          }
                          if (item.checked && typeof item.price === "undefined") {
                              estimate = 0
                              console.log('item with no price')
                              return true;
                          }
                      })
                  });
                  return estimate;
              }


              /* cacluate the total time required for all selcted services */
              var totalTimeRequired = function() {
                  var totalTime = 0;
                  angular.forEach(serviceObject, function(group) {
                      angular.forEach(group.items, function(item) {
                          if (item.checked) {
                              totalTime += item.allowedTime;
                          }
                      });
                  });
                  return totalTime;
              }







/* Make booking
=========================================================================== */


              var saveBooking = function() {
                  var extendProfileData = {};

                  function isMOT() {
                      return false
                  }

                  function hasAudiCam() {
                      return true
                  }

                  function getAppointmentTime() {

                      return ""
                  }

                  function collectionDeliveryOptions() {
                      // TODO
                      if (user.colDelPostcode == "other") {
                          extendProfileData.collectionDeliveryOther = true;

                          // fix ...
                          extendProfileData.collectionDelivery = false;
                      } else {
                          extendProfileData.colDelPostcode = user.colDelPostcode;
                      }

                  }

                  extendProfileData = {
                      onlineServiceBookingPersistenceRequest: {
                          dealerCentreId: dealer.dealerCode,
                          currentMileage: "",

                          currentCar: (car.length > 0 ? car[0].trim() : ""),
                          carReg: user.vrm,
                          channelId: 2,
                          repairOperations: [],
                          generalInvestigationInfo: "" // if GEN
                      }, //

                      mot: isMOT(), /// if MOT
                      audiCam: hasAudiCam(),
                      appointmentTime: getAppointmentTime(),
                      colour: "" //??
                  }

                  collectionDeliveryOptions();


                  angular.extend(user.profileData, extendProfileData.onlineServiceBookingPersistenceRequest)



                  console.log('getCarInfoByCarReg')
                  var deferred = $q.defer();
                  $http({
                      method: 'POST',
                      url: urls.createBooking + dealer.dealerCode,
                      data: extendProfileData
                  })
                      .success(function(result, status) {

                          if (result.success) {
                              console.log('BOOKING SUCCESSFUL')
                              deferred.resolve();
                          } else {

                              deferred.reject('error creating booking');
                          }
                      }).
                  error(function(data, status) {
                      console.log('======error with BOOKING', result);
                      deferred.reject('error with booking');
                  });



                  return deferred.promise;

              }







/* Address services
=========================================================================== */




              // ADDRESS
              var lookupAddress = function(postcode) {
                  return $http({
                      method: 'GET',
                      url: 'https://soa.audi.co.uk' + '/commonws/services/address/postcodeLookup/' + postcode
                  });
              }

              var saveAddrLookup = function(data) {
                  if (window.console) console.log("called saveAddrLookup() with data:", data);
                  var addr1 = data.houseFlatNumber,
                      addr2 = data.street;

                  if (data.premise.length) {
                      addr1 = data.premise;
                      addr2 = data.houseFlatNumber + " " + data.street;
                  }

                  var newData = {
                      "address1": addr1,
                      "address2": addr2,
                      "city": data.town,
                      "postcode": data.postCode,
                      "county": data.county
                  }
                  angular.extend(user.profileData, newData);


              }



              var checkIfPostcode = function(str) {

                  var re = /^(GIR[ ]?0AA)$|^([A-PR-UWYZ][0-9][ ]?[0-9][ABD-HJLNPQ-UW-Z]{2})$|^([A-PR-UWYZ][0-9][0-9][ ]?[0-9][ABD-HJLNPQ-UW-Z]{2})$|^([A-PR-UWYZ][A-HK-Y0-9][0-9][ ]?[0-9][ABD-HJLNPQ-UW-Z]{2})$|^([A-PR-UWYZ][A-HK-Y0-9][0-9][0-9][ ]?[0-9][ABD-HJLNPQ-UW-Z]{2})$|^([A-PR-UWYZ][0-9][A-HJKS-UW0-9][ ]?[0-9][ABD-HJLNPQ-UW-Z]{2})$|^([A-PR-UWYZ][A-HK-Y0-9][0-9][ABEHMNPRVWXY0-9][ ]?[0-9][ABD-HJLNPQ-UW-Z]{2})/;
                  str = str.toUpperCase()
                  var m = re.exec(str)
                  if (m != null) {
                      console.log('postcode:', m[0])
                      user.postcode = m[0];
                  } else {
                      console.log('NOT A POSTCODE')
                  }
              }









/* Services Object
=========================================================================== */





              var serviceObject = [{
                      title: "Book a Service",
                      actAsRadio: true,
                      code: "bookService",
                      items: [{
                          serviceCode: 'SERMAJ',
                          title: 'Major service',
                          titleShort: 'Major service',
                          text: 'Replacement oil, oil filter, air filter, spark plugs (petrol engines), fuel filter (diesel engines), plus road test and condition report.',
                          moreDetails: true,
                          allowedTime: 2
                      }, {
                          serviceCode: 'SERMIN',
                          title: 'Interim Service',
                          titleShort: 'Interim Service',
                          text: 'Replacement of engine oil and filter, plus written condition report.',
                          moreDetails: true,
                          allowedTime: 0.5
                      }, {
                          serviceCode: 'SERUNK',
                          title: 'I\'m not sure what service I need',
                          titleShort: "Service (unknown)",
                          text: 'If you\’re are unsure which service your Audi needs continue with your booking and your Audi Centre will content you within 48 hours to discuss',
                          allowedTime: 2
                      }]
                  }, {
                      title: "Regular Maintenance",
                      items: [{
                          serviceCode: 'MOT',
                          title: 'MOT (including MOT Protection)',
                          titleShort: 'MOT',
                          text: 'Cover of up to £750 (inc VAT) should your Audi fail its next MOT.',
                          moreDetails: true,
                          allowedTime: 1
                      }, {
                          serviceCode: 'TYR',
                          title: 'Tyre Replacement',
                          titleShort: 'Tyre Replacement',
                          text: '',
                          moreDetails: true,
                          allowedTime: 0.4
                      }]
                  }, {
                      title: "Unscheduled Maintenance",
                      items: [{
                          serviceCode: 'GEN',
                          title: 'Investigate an issue with my car',
                          titleShort: 'General investigation',
                          text: "If you have a concern with your Audi our technicians will complete an 'Initial diagnostic investigation' for a fixed fee of £60.",
                          allowedTime: 1
                      }]
                  }, {
                      title: "Complimentary as part of The Audi <span class='red-text'>Difference</span>",
                      items: [{
                          serviceCode: 'HC',
                          title: 'Complimentary Healthcheck',
                          titleShort: 'Healthcheck',
                          text: "We offer a complimentary check of your vehicle's main safety-related components, summarised in a written report.",
                          moreDetails: true,
                          price: "FREE",
                          checked: true,
                          allowedTime: 0.3
                      }, {
                          serviceCode: 'CAM',
                          title: 'Audi Cam',
                          titleShort: 'Audi Cam',
                          text: "We won't just tell you what work may be required on your vehicle, we'll show you.",
                          moreDetails: true,
                          price: "FREE",
                          checked: true,
                          allowedTime: 0
                      }]
                  }

              ];






              return {
                  dealers: dealers,
                  dealer: dealer,
                  car: car,
                  user: user,
                  calendarDates: calendarDates,
                  filterAvailability: filterAvailability,
                  getDealers: getDealers,
                  getDealerDetails: getDealerDetails,
                  getCarInfoByCarReg: getCarInfoByCarReg,
                  getServicesByDealerCodeCarReg: getServicesByDealerCodeCarReg,
                  getFacilities: getFacilities,
                  getAvailability: getAvailability,
                  selectDate: selectDate,
                  inDateCache: inDateCache,
                  clearDateCache: clearDateCache,
                  getCalendarAvailability: getCalendarAvailability,
                  getSelectedServiceCodes: getSelectedServiceCodes,
                  setSelectedServiceCodes: setSelectedServiceCodes,
                  serviceObject: serviceObject,
                  totalTimeRequired: totalTimeRequired,
                  estimatedTotal: estimatedTotal,
                  saveBooking: saveBooking,
                  isTimeRequired: isTimeRequired,
                  isDateTimeValid: isDateTimeValid,

                  lookupAddress: lookupAddress,
                  saveAddrLookup: saveAddrLookup,
                  checkIfPostcode: checkIfPostcode


              }

          }
      ]);

