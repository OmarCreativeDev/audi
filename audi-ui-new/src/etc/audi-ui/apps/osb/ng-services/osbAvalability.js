

      osb.factory('osbAvalability', ['$resource', '$http', '$q', '$timeout', '$filter',
          function($resource, $http, $q, $timeout, $filter) {




/* avalability
=========================================================================== */

              /* Select a date time page */

              var inDateCache = function(startDate) {
                  return (dealer.availabilityFetched.indexOf(startDate) == -1) ? false : true
              }

              var clearDateCache = function() {
                  console.log(' - - - clearDateCache')
                  dealer.availabilityFetched = [];
              }

              var formatDateForUrl = function(date) {
                  return $filter('date')(date.getTime(), 'dd-MM-yyyy');
              }

              var getMonthAhead = function(monthAhead) {

                  var now = new Date();
                  if (now.getMonth() == 11) {
                      var current = new Date(now.getFullYear() + monthAhead, 0, 1);
                  } else {
                      var current = new Date(now.getFullYear(), now.getMonth() + monthAhead, 1);
                  }
                  console.log('getMonthAhead:', monthAhead, current)
                  return formatDateForUrl(current);

              }

              var getCalendarAvailability = function() {
                  var deferred = $q.defer();


                  // fetch this month and the following 3 months
                  $q.all([

                      getAvailability(formatDateForUrl(new Date())),
                      getAvailability(getMonthAhead(1)),
                      getAvailability(getMonthAhead(2)),
                      getAvailability(getMonthAhead(3)),
                  ]).then(function() {

                      filterAvailability(user.facilities.option);


                      deferred.resolve();

                      // TODO: add in check to make sure there is a aleast one valid date...
                  },function(){
                     deferred.reject('error_loading_date_info');
                  })

                  return deferred.promise;

              }


              // two below do the same thing for two differnig things 
              // lookup from server format
              var getSelectedServiceCodesForLookup = function() {
                      var serviceCodesArray = [];
                      angular.forEach(serviceObject, function(group) {
                          angular.forEach(group.items, function(item) {
                              if (item.checked) {
                                  serviceCodesArray.push("serviceCodes=" + item.serviceCode);
                              }
                          })
                      })
                      var serviceCodesStr = serviceCodesArray.join("&")
                      console.log('serviceCodesStr', serviceCodesStr)
                      return serviceCodesStr
                  }
                  // store in query string in users browser
              var getSelectedServiceCodes = function() {
                  var serviceCodesArray = [];
                  angular.forEach(serviceObject, function(group) {
                      angular.forEach(group.items, function(item) {
                          if (item.checked) {
                              serviceCodesArray.push(item.serviceCode);
                          }
                      })
                  })
                  var serviceCodesStr = serviceCodesArray.join(",")
                  console.log('serviceCodesStr', serviceCodesStr)
                  return serviceCodesStr
              }

              // set from query string to object
              var setSelectedServiceCodes = function(serviceCodes) {
                  console.log('setSelectedServiceCodes', serviceCodes)
                  angular.forEach(serviceObject, function(group) {
                      angular.forEach(group.items, function(item) {
                          if (serviceCodes.indexOf(item.serviceCode) > -1) {
                              item.checked = true;

                          }
                      })
                  })
                  console.log('serviceObject', serviceObject)
              }



              /* server fetch of date info */
              var getAvailability = function(startDate) {

                  var deferred = $q.defer();

                  // see if we have already fetched this data yet- if we have no need to go and get it again.
                  if (!inDateCache(startDate)) {


                      //   serviceCodesStr = "serviceCodes=MOT"
                      var serviceCodesStr = getSelectedServiceCodesForLookup();
                      $http({
                          method: 'GET',
                          url: "/etc/audi-ui/apps/osb/mockData/getAvailability/" + startDate + ".html?" + serviceCodesStr + "&dealerCode=" + dealer.details.dealerCode
                          //     url: "https://soa.audi.co.uk/audi-online-service-booking/osbservices/v1/getAvailability?dealerCode="+dealer.details.dealerCode+"&startDate="+startDate+"-00.00&"+ serviceCodesStr

                      })
                          .success(function(result, status) {
                            // create the arrays if first item
                              if (typeof dealer.availability === "undefined") {
                                  dealer.availability = [];
                                  dealer.filteredAvailability = [];
                              }

                              //double check to make sure
                              if (!inDateCache(startDate)) {
                                  angular.forEach(result.availability, function(item) {
                                      dealer.availability.push(item);
                                      //  dealer.filteredAvailability.push(item);
                                  });
                                  dealer.availabilityFetched.push(startDate);
                                  console.log('pushed date into availabilityFetched')
                              }

                              console.log('dealer.availability', dealer.availability)



                              deferred.resolve();
                          }).
                      error(function(data, status) {
                          console.log('error with availability', data);
                          deferred.reject();
                      });
                  } else {
                      deferred.resolve();
                  }
                  return deferred.promise;
              }


              /* not all options need time */
              var isTimeRequired = function() {
                  //see if time is required
                  var requiredTime = ['dropOff', 'whileYouWait', 'courtesyVehicle']
                  return (requiredTime.indexOf(user.facilities.option) == -1) ? false : true
              }


              /* used for next button on wizard on the date time screen */
              var isDateTimeValid = function() {
                  // time required and is compleat
                  if (isTimeRequired()) {
                      if (user.selectedDateTime.date && user.selectedDateTime.timeSlot) {
                          return true
                      } else {
                          return false
                      }

                  } else {
                      user.selectedDateTime.timeSlot = "";
                      if (user.selectedDateTime.date) {
                          return true
                      } else {
                          return false
                      }
                  }

              }


              /* filter the calendar to show only the days that have the selected option */
              var filterAvailability = function(whatFor) {

                  user.facilities.showDatePicker = 'loading';
                  console.log('filterAvailability', whatFor)
                  var filteredDates = [];

                  switch (whatFor) {
                      case 'dropOff': // WORKS
                          filteredDates = dealer.availability;
                          break;
                      case 'whileYouWait': //works

                          angular.forEach(dealer.availability, function(dateItem) {
                              var timeArray = []; // only times that are valid to wait for will be included
                              angular.forEach(dateItem.availableTimes, function(timeItem, index) {
                                  if (timeItem.whileYouWait == true) {
                                      timeArray.push(timeItem)
                                  }
                              })
                              if (timeArray.length > 0) {
                                  dateItem.availableTimes = timeArray;
                                  filteredDates.push(dateItem);
                              }
                          })

                          break;
                      case 'courtesyVehicle': // WORKS
                          angular.forEach(dealer.availability, function(dateItem) {
                              if (dateItem.courtesyVehicle) {
                                  filteredDates.push(dateItem);
                              }
                          })


                          break;
                      case 'collectionDelivery':
                          angular.forEach(dealer.availability, function(dateItem) {
                              // if (dateItem.colDelPostcodeAreas.length > 0) {
                              console.log('dateItem.colDelPostcodeAreas', dateItem.colDelPostcodeAreas)
                              console.log('user.colDelPostcodeArea', user.colDelPostcodeArea)
                              console.log('inde', dateItem.colDelPostcodeAreas.indexOf(user.colDelPostcodeArea))
                              console.log('===========')
                              if (dateItem.colDelPostcodeAreas.indexOf(user.colDelPostcodeArea) > -1) {
                                  filteredDates.push(dateItem);
                              }
                          })
                          break;
                      case 'courtesyLift': // WORKS
                          angular.forEach(dealer.availability, function(dateItem) {
                              if (dateItem.courtesyLift) {
                                  filteredDates.push(dateItem);
                              }
                          })
                          break;

                  }

                  console.log('calendarDates', filteredDates.length, filteredDates)
                  if (whatFor == 'collectionDelivery' && filteredDates.length == 0) {
                      angular.copy([], dealer.filteredAvailability);
                      user.facilities.showDatePicker = 'loaded';
                  } else if (filteredDates.length == 0) {

                      console.error('NO FILTERED DATES');
                      angular.copy([], dealer.filteredAvailability);

                      user.facilities.showDatePicker = 'loading';
                      // TODO : look ahead 
                      user.facilities.showDatePicker = 'noDates';

                      user.selectedDateTime = {
                          date: "",
                          timeSlot: ""
                      }

                  } else {
                      calendarDates = angular.copy(filteredDates, dealer.filteredAvailability);


                      selectFirstAvailableDateAndTime()


                      $timeout(function() {
                          user.facilities.showDatePicker = 'loaded';
                      }, 200)



                  }



              }




              /* select the date from the calendar and show the times */
              var selectDate = function(timestamp) {
                  dealer.selectedDay = {};
                  angular.forEach(dealer.filteredAvailability, function(day) {
                      if (day.availableDate == timestamp) {
                          dealer.selectedDay = day;
                      }
                  });
                  console.log('selectedDateTimeSlots', dealer.selectedDay)
                  // check to see if the user can still have the same time slot as they had before
                  isSelectedTimeSlotAvailable();
              }


              /* check to see if the user can have the same date and time as they had on there other selected date */
              var isSelectedTimeSlotAvailable = function() {

                  if (isTimeRequired()) {

                      console.log('isSelectedTimeSlotAvailable')
                      var isAv = false;
                      // check to see if the time slot is stil valid on another day
                      if (user.selectedDateTime.timeSlot != "") {
                          angular.forEach(dealer.selectedDay.availableTimes, function(timeSlot) {


                              console.log(user.selectedDateTime.timeSlot, timeSlot.time)
                              if (user.selectedDateTime.timeSlot == timeSlot.time) {
                                  isAv = true;
                              }

                          });
                          if (!isAv) {
                              selectFirstTimeSlotForDate();
                          }
                      } else {
                          selectFirstTimeSlotForDate();
                      }
                  } else {
                      // no time required so lets clear the value
                      user.selectedDateTime.timeSlot = ""
                  }


              }


              /* select the first available date from the calendar */
              var selectFirstAvailableDateAndTime = function() {
                  if (typeof dealer.filteredAvailability[0] === 'object') {
                      console.warn('selectFirstAvailableDateAndTime', user.selectedDateTime.date, dealer.filteredAvailability[0].availableDate)
                      if (user.selectedDateTime.date != dealer.filteredAvailability[0].availableDate) {
                          user.selectedDateTime.date = dealer.filteredAvailability[0].availableDate;
                      }

                      selectDate(user.selectedDateTime.date);
                  }
              }
              /* select the first time slot for a date */
              var selectFirstTimeSlotForDate = function() {
                  if (typeof dealer.selectedDay.availableTimes[0] === 'object') {
                      user.selectedDateTime.timeSlot = dealer.selectedDay.availableTimes[0].time
                  }
              }



              /* Calculate the estimated total of all selected services */
              var estimatedTotal = function() {
                  var estimate = 0;
                  angular.forEach(serviceObject, function(group) {
                      angular.forEach(group.items, function(item) {
                          if (item.checked && typeof item.price == "number") {
                              estimate += item.price;
                          }
                          if (item.checked && typeof item.price === "undefined") {
                              estimate = 0
                              console.log('item with no price')
                              return true;
                          }
                      })
                  });
                  return estimate;
              }


              /* cacluate the total time required for all selcted services */
              var totalTimeRequired = function() {
                  var totalTime = 0;
                  angular.forEach(serviceObject, function(group) {
                      angular.forEach(group.items, function(item) {
                          if (item.checked) {
                              totalTime += item.allowedTime;
                          }
                      });
                  });
                  return totalTime;
              }








              return {
                 
                  getAvailability: getAvailability,
                  selectDate: selectDate,
                  inDateCache: inDateCache,
                  clearDateCache: clearDateCache,
                  getCalendarAvailability: getCalendarAvailability,
                  getSelectedServiceCodes: getSelectedServiceCodes,
                  setSelectedServiceCodes: setSelectedServiceCodes,
                  
                  totalTimeRequired: totalTimeRequired,
                  estimatedTotal: estimatedTotal,
                 
                  isTimeRequired: isTimeRequired,
                  isDateTimeValid: isDateTimeValid

                

              }

          }
      ]);


