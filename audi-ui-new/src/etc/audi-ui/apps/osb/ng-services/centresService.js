

	if (window.console) console.log("centresService loaded");

	osb.factory('centresService', ['$resource', '$http', '$q', '$state','$timeout',
		function ($resource, $http, $q, $state,$timeout) {
			var centres = [];
			var centre={};
			var resetCentres = function(){
				angular.copy([],centres);
			}
			var centreSearch = function(location,limit){								
								var query = "?query="+location + "&limit="+limit;
								var deferred = $q.defer();
									$http({
										//url: "http://webservices.audi.co.uk.uat.nativ-systems.com/audi-dealercentre-service/dealerservice/places/place"+query,
										//url: (location == "test") ? "test" : "apps/osb/sampleJson/dealerservice_places_place.js"+query,
										 url: "/audi-ui/apps/osb/mockData/findDealerships/index.html?location="+location,
										method: 'GET'
									}).then(function(response){

									/*angular.forEach(response.data.dealers,function(centre,index){

											centre.id = index;
											centre.options = {
												draggable: false,
												labelAnchor: '10 39',
												labelContent: index,
												labelClass: 'labelMarker'
											};
											centre.latitude = centre.location.latitude;
               								centre.longitude = centre.location.longitude;
               								centre.showWindow = false;
               							
									        centre.label = index;
               								centre.title = "m:"+index;
										
									}); */
										angular.copy(response.data.dealers,centres);
										if (window.console) console.log('centres',centres)
										deferred.resolve(response);
			                    },
			                    function(response){
			                    		deferred.reject(response);
			                    });

									return deferred.promise;

			}
			var getCentre = function(dealerCode){

					var deferred = $q.defer();
					
							if (window.console) console.log('fetching data')

								var query = "?dealerCode="+dealerCode
		      					  $http({
		      					  	//	url: "http://webservices.audi.co.uk.uat.nativ-systems.com/audi-dealercentre-service/dealerservice/getdealerdetails"+query,
										// url: "apps/osb/sampleJson/dealerservice_getdealerdetails.js"+query,
										 url: "/audi-ui/apps/osb/mockData/getDealerDetails/index.html?code="+dealerCode,
                                                               
										method: 'GET'
									}).then(function(response){
										if (window.console) console.log('response.data',response.data)
										angular.copy(response.data,centre);
										deferred.resolve(response.data)
									},
				                    function(response){
				                    		if (window.console) console.log('data fetch error')
				                    		deferred.reject(response);
				                    });
				                 

					return deferred.promise;

			}

			return {
					centres:centres,
					centre:centre,
					centreSearch:centreSearch,
					getCentre:getCentre,
					resetCentres:resetCentres
			}
		}
	]);

