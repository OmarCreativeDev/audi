
console.log('osb module loaded');

var osb = angular.module('osb', [
	'ui.router',
	'ngResource',
	//'ngAnimate',
	//'ngSanitize',
	'ui.bootstrap',
	'sa.utils',
	'sa.utils.modules.locationMap',
	'sa.utils.modules.addressLookup',
	'ui.bootstrap.affix',
	'angulartics',
	'angulartics.adobe.analytics'
]);

osb.appConfig = {
	useMocks: true,
	urls: {}
};

osb.config(['$stateProvider', '$urlRouterProvider',
	function ($stateProvider, $urlRouterProvider) {
		console.log('OSB ROUTES CONFIG');

		$stateProvider

			.state('landing', {
				url: "/landing?postcode&vrm&error",
				templateUrl: '/etc/audi-ui/apps/osb/ng-components/landing/landing.html',
				controller: 'landingCtrl'

			})

			.state('wizard', {
				url: "/wizard?step&postcode&vrm&dealerCode&services",
				templateUrl: '/etc/audi-ui/apps/osb/ng-components/wizard/wizard.html',
				controller: 'wizardCtrl'
			});
	}
]);

//
// Initate the UI Router state
//
osb.run(['$state', function ($state) {
	console.log('OSB LOADING. GO TO STATE')
	$state.go('landing');
	console.log('OSB LOADING. GO TO STATE DONE')
}]);
