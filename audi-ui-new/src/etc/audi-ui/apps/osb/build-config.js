module.exports = {
	options: {
		separator: "\n\n",
		banner: "(function(){\n\n'use strict';\n\n",
		footer: "\n\n})();",
		sourceMap: false
	},
	src: [ 

		// osb Module
		'src/etc/audi-ui/apps/osb/osb.js',

		'src/etc/audi-ui/apps/osb/ng-services/osbService.js',
		'src/etc/audi-ui/apps/osb/ng-components/wizard/wizardCtrl.js',

		'src/etc/audi-ui/apps/osb/ng-components/landing/landingCtrl.js',

		'src/etc/audi-ui/apps/osb/ng-components/wizard/steps/yourDetailsCtrl.js',
		'src/etc/audi-ui/apps/osb/ng-components/wizard/steps/servicesCtrl.js',
		'src/etc/audi-ui/apps/osb/ng-components/wizard/steps/selectCentreCtrl.js',
		'src/etc/audi-ui/apps/osb/ng-components/wizard/steps/dateTimeCtrl.js',


	// Salmon Utils for Angular
		'src/etc/audi-ui/global/scripts/salmon.utils.modules/addressLookup/addressLookupDirective.js',


'src/etc/audi-ui/global/scripts/salmon.utils.modules/locationMap/directive.js'

	],
	dest: 'src/etc/audi-ui/apps/osb/osb.min.js'
};
