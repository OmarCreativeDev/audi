


      osb.controller('yourDetailsCtrl', ['$scope', 'osbService', '$q','$timeout','$analytics',
          function($scope, osbService, $q,$timeout,$analytics) {
              $scope.user = osbService.user;


              $scope.sections = {
                  address: {
                      formName: 'addrEditForm',
                      edit: false,
                      editForm: {
                          visible: false
                      },
                      lookupForm: {
                          step1: {
                              visible: true
                          },
                          step2: {
                              visible: false
                          },
                          data: {},
                          foundAddresses: [],
                          selectedAddr: {}
                      }
                  },
                  addressLookup: {}
              }


              ///// addresss



              // Change address section
              // -------------------------------------

              $scope.editAddrManualClick = function() {
                  hideAddrLookup();
                  showAddrEdit();
              };

              $scope.goToLookupClick = function() {
                  hideAddrEdit();
                  showAddrLookupStep1();
              };

              $scope.editAddrManualFromLookupClick = function() {
                  console.log('editAddrManualFromLookupClick()');
                  var selectedAddr = $scope.sections.address.lookupForm.selectedAddr;
                  $scope.sections.address.data = profileService.getAddressFromLookupData(selectedAddr);
                  hideAddrLookup();
                  showAddrEdit();
              };

              var showAddrLookupStep1 = function() {
                  resetFormStatus('addrLookup1');
                  $scope.sections.address.lookupForm.data = {};
                  $scope.sections.address.lookupForm.step1.visible = true;
              };




              var showAddrLookupStep2 = function(addresses) {

                  var model = $scope.sections.address.lookupForm;

                  model.foundAddresses = addresses;
                  model.selectedAddr = getAddrByHouse(model.foundAddresses) || model.foundAddresses[0];

                  $scope.sections.address.lookupForm.step2.visible = true;
                  $scope.focusAddrSelect = true;
              };

              var showAddrEdit = function() {
                  $scope.sections.address.editForm.visible = true;
              };
              var hideAddrEdit = function() {
                  $scope.sections.address.editForm.visible = false;
              };

              var hideAddrLookupStep1 = function() {
                  $scope.sections.address.lookupForm.step1.visible = false;
              };
              var hideAddrLookupStep2 = function() {
                  $scope.sections.address.lookupForm.step2.visible = false;
              };
              var hideAddrLookup = function() {
                  hideAddrLookupStep1();
                  hideAddrLookupStep2();
              };

              var getAddrByHouse = function(addresses) {

                  var model = $scope.sections.address.lookupForm.data;

                  if (typeof model.address1 === 'undefined') {
                      return false;
                  }

                  var houseNum = model.address1.replace(/[^\d.]/g, '').trim(),
                      houseName = model.address1.replace(/[0-9]/g, '').trim();

                  if (houseNum.length) {
                      for (var i = 0; i < addresses.length; i++) {
                          if (addresses[i].houseFlatNumber.indexOf(houseNum) > -1) {
                              return addresses[i];
                          }
                      }
                  } else if (houseName.length) {
                      houseName = houseName.toLowerCase();
                      for (var i = 0; i < addresses.length; i++) {
                          if (addresses[i].premise.toLowerCase().indexOf(houseName) > -1) {
                              return addresses[i];
                          }
                      }
                  }

                  return false;
              };

              $scope.doLookupAddressStep1 = function() {
                  console.log('doLookupAddressStep1')
                  if ($scope.addrLookup1.$valid) {

                      hideAddrLookupStep2();

                      osbService.lookupAddress($scope.osb.user.postcode)
                          .success(function(data) {

                              console.log('address lookup sucess', data);
                              var addresses = [{
                                ID:"",
                                full : "Please select"
                              }];

                              angular.forEach(data, function(val, key) {
                                  val.ID = key + 1;
                                  val.full = val.simpleAddress + ', ' + val.postCode.toUpperCase();
                                  addresses.push(val);
                              });

                              showAddrLookupStep2(addresses);
                          })
                          .error(function() {
                              console.log('address lookup failed');
                          });

                  } else {
                      $scope.addrLookup1.submitted = true;
                  }
              };

              $scope.doLookupAddressStep2 = function() {
                  console.log('lookupAddressStep2');
                  osbService.saveAddrLookup($scope.sections.address.lookupForm.selectedAddr);
                  hideAddrLookup();
                  showAddrEdit();
              };









              ///////



              $scope.validateDetailsForm = function() {



                  if ($scope.personalInfoForm.$valid) {

                      //

                      console.warning('NEXT');
                      //    $scope.osb.next();     

                  } else {
                      $scope.personalInfoForm.submitted = true;
                  }

              }


Array.prototype.uniqueArray = function()
{
  var n = {},r=[];
  for(var i = 0; i < this.length; i++)
  {
    if (!n[this[i]])
    {
      n[this[i]] = true;
      r.push(this[i]);
    }
  }
  return r;
}


              $scope.osb.saveBooking = function() {
                  console.log('========= saveBooking')
                  console.log('personalInfoForm', $scope.personalInfoForm)
                  var deferred = $q.defer();




                  $scope.personalInfoForm.submitted = true;
                  $scope.addrLookup1.submitted = true;
                  $scope.sections.address.editForm.visible = true
                  $scope.addrEditForm.submitted = true;
                  //fix
                  $scope.sections.address.lookupForm.step1.visible = false
                  $scope.sections.address.lookupForm.step2.visible = false




                  if ($scope.personalInfoForm.$valid){ //&& ($scope.sections.address.lookupForm.step2.visible && $scope.addrEditForm.$valid) && !$scope.sections.address.lookupForm.step2.visible) {


                     
                      $timeout(function(){
                         deferred.resolve();
                    } ,2000);

                     // osbService.saveBooking().then(function() {
                       //   deferred.resolve();
                      //});
                      return deferred.promise;
                  } else {

                          $timeout(function(){
                                    var unique = $scope.fieldThatHaveErrors.uniqueArray()

                                    console.log('$scope.fieldThatHaveErrors unique , ', unique)

                                        $analytics.eventTrack('osbFormFieldErrors', null, {
                                                                      fieldError: unique.join(','),
                                                                      description: 'osbFormField error'
                                                                    });
                                    $scope.fieldThatHaveErrors = [];
                          },1000)
               
                      deferred.reject();
                      console.log('invalid form')

                  }

                  return deferred.promise;
              }






              $scope.fieldChanged = function(formName, fieldName) {
                  var form = $scope[formName],
                      field = form[fieldName];
                  field.$setValidity("serverError", true);
              }
              $scope.fieldThatHaveErrors = [];

              $scope.fieldHasError = function(formName, fieldName) {
                  var form = $scope[formName],
                      field = form[fieldName];
                      var re = false
                      if(field.$invalid && (form.submitted || field.$hadFocus)){
                          re = true
                          $scope.fieldThatHaveErrors.push(fieldName);
                      }
                  return re;
              }

              $scope.fieldHasSuccess = function(formName, fieldName) {
                  var form = $scope[formName],
                      field = form[fieldName];
                  return field.$valid && (form.submitted || field.$hadFocus) ? true : false;
              }

              $scope.fieldHasFeedback = function(formName, fieldName) {
                  var form = $scope[formName],
                      field = form[fieldName];
                  return form.submitted || field.$hadFocus ? true : false;
              }

              $scope.getServerError = function(fieldName) {
                  if ($scope.serverErrors[fieldName] !== undefined) {
                      return $scope.serverErrors[fieldName];
                  } else {
                      return false;
                  }
              }



              var resetFormStatus = function(formName) {
                  var form = $scope[formName];

                  if (form.hasOwnProperty('submitted')) {
                      form.submitted = false;
                  }
              };




          }
      ])
