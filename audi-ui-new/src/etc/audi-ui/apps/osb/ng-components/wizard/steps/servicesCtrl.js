

      osb.controller('servicesCtrl', ['$scope', 'osbService', '$modal', '$timeout', 'templateCache',
          function($scope, osbService, $modal, $timeout, templateCache) {


              $scope.serviceObject = osbService.serviceObject;
              $scope.genInvestigationText = "";

              var init = function() {

                  templateCache.get("/etc/audi-ui/apps/osb/ng-components/wizard/modals/serviceModal.html");
                  templateCache.get("/etc/audi-ui/apps/osb/ng-components/wizard/modals/genInvestigation.html");
              }

              $scope.isServiceOptionAvailable = function(item) {
                  var re = false;
                  if(item.serviceCode == "CAM"){
                    re = true
                  }
                  angular.forEach(osbService.dealer.services, function(service) {
                      if (service.serviceCode == item.serviceCode) {
                          if (service.price) {
                              item.price = service.price
                          }
                          re = service;
                      }
                  });
                  return re;
              }

              $scope.compareServices = function() {

                  var compareServicesModal = $modal.open({
                      templateUrl: "/etc/audi-ui/apps/osb/ng-components/wizard/modals/compareServices.html",
                      size: 'lg',
                      controller: function($scope, $modalInstance) {

                          $scope.close = function() {
                              $modalInstance.close();
                          }

                      }

                  });

              }


              $scope.editGenInvestigationText = function() {
                  var osb = $scope.osb;
                  var GenModal = $modal.open({
                      templateUrl: "/etc/audi-ui/apps/osb/ng-components/wizard/modals/genInvestigation.html",
                      size: 'md',
                      controller: function($scope, $modalInstance) {
                          $scope.osb = osb;
                          $scope.genInvestigationText = $scope.osb.user.genInvestigationText;
                          $scope.close = function() {
                              $scope.osb.user.genInvestigationText = $scope.genInvestigationText;
                              $modalInstance.close();
                          }

                      }

                  });
              }




              // make item act as radio
              $scope.serviceItemCheckbox = function(group, item) {


                  osbService.clearDateCache();


                  if (item.serviceCode == 'GEN') {
                      if (item.checked) {
                          $scope.editGenInvestigationText();

                          return;
                      } else {
                          $scope.osb.user.genInvestigationText = "";
                          $scope.genInvestigationText = "";
                          return;
                      }

                  }
                  var curentValue = item.checked;

                  if (group.actAsRadio && item.checked) {
                      angular.forEach(group.items, function(gItem) {
                          gItem.checked = false
                      })
                      item.checked = curentValue;
                  }

              }

              $scope.loadDetailsModal = function(group, item) {
                  var serviceItemCheckbox = $scope.serviceItemCheckbox;
                  var modalInstance = $modal.open({
                      templateUrl: "/etc/audi-ui/apps/osb/ng-components/wizard/modals/serviceModal.html",
                      size: 'lg',
                      controller: function($scope, $modalInstance) {

                          $scope.item = item;
                          var init = function() {


                          }


                          $scope.close = function() {

                              $modalInstance.close();
                          }
                          $scope.selectOrUnSelectService = function() {
                              $timeout(function() { //so the user sees the item being checked on the page
                                  $scope.item.checked = !$scope.item.checked
                                  serviceItemCheckbox(group, item);
                              }, 500)
                              $scope.close();

                          }

                          init();


                      }

                  });


              }





          }
      ])
