

      osb.filter("timeFormated", function() {
          return function(timeItem) {
              return timeItem.toFixed(2) // fix 9.1 to 9.10
          }
      })

      osb.filter("timeFormatedAmPM", function() {
          return function(timeItem) {
            timeItem = timeItem *1
            var amPM = "am"
            if(timeItem> 12){
              amPM = "pm"
            }
        var time12h =  ((timeItem + 11) % 12 + 1);
           timeItem = time12h.toFixed(2)
          
              return timeItem  + amPM // fix 9.1 to 9.10
          }
      })


      osb.filter("timeAsHoursMinutes", function() {
          return function(timeItem) {
              timeItem = "" + timeItem + ""
              var splitTime = timeItem.split('.');
              console.log(splitTime)
              var hours = splitTime[0] * 1;
              var minutes = splitTime[1] * 1;

              var returnString = "";
              if (hours) {
                  returnString += hours + " hrs ";
              }

              if (minutes) {
                  returnString += ((60 / 100) * (minutes * 10)) + " mins";
              }

              return returnString
          }
      })





      osb.controller('wizardCtrl', ['$scope', '$rootScope', '$state', 'osbService', '$timeout', '$q', '$filter', '$modal', 'templateCache','$analytics',
          function($scope, $rootScope, $state, osbService, $timeout, $q, $filter, $modal, templateCache,$analytics) {


              /*
  STEPS
  ==================================
     0    -     Landing     INPUT: Search location and REG

     1    -     Centre finder - Select DealerCode
     2    -     Select a service - Service Codes
     3    -     Date & Time - 
     4    -     Your details
     5    -     Confirmation
  ==================================
*/





              // namespaced onto 'osb' so ng-include can access and change varibles and functions
              $scope.osb = {
                  params: angular.copy($state.params),
                  dealers: osbService.dealers, // list of dealers matching the search results
                  dealer: osbService.dealer, // selected dealer information -  used within Modal
                  serviceOptions: osbService.serviceObject,
                  car: osbService.car, // car information
                  user: osbService.user, // holds the users selection and data (from search location to car reg and details to their selections)
                  calendarDates: osbService.calendarDates, // filtered date object
                  dateOptions: {
                      formatYear: 'yy',
                      startingDay: 1,
                      format: 'yyyy-mm-dd',
                      minDate: '',
                      maxMode: "day",
                      showWeeks: false
                  },
                  dealerBrowseInfo: {},
                  personalInfoForm: {}, // details form
                  wizard: {
                      currentStepObj: {},
                      currentStep: 0,
                      furthestStepNumber: -1,
                      steps: [
                          /*{
                            stepNumber:0,
                            title:"Welcome",
                           // NOT PART OF 'WIZARD'
                          },*/
                          {
                              stepNumber: 1,
                              title: "Select a centre",
                              templateUrl: "/etc/audi-ui/apps/osb/ng-components/wizard/steps/selectCentre.html",
                              showSteps: true,
                              showNext: false,
                              nextScreenLoadMessage: "<h2>Checking available service options</h2>",
                              preLoad: function() {


                          
                                  console.log('============= step 1 preload')

                                  if (typeof $scope.osb.user.vrm === "undefined") {

                                      //  $state.go('landing');
                                      return false;
                                  } else {
                                      var deferred = $q.defer();

                                      osbService.getCarInfoByCarReg($scope.osb.user.vrm).then(function() {




                                      }, function() {
                                          $state.go('landing', {
                                              error: 'getCarInfoByCarRegFailed',
                                              postcode: $scope.osb.user.postcode,
                                              vrm: $scope.osb.user.vrm
                                          })
                                      })





                                      osbService.getDealers($scope.osb.user.postcode)
                                          .then(function(response) {


                                          $analytics.pageTrack("/osb/wizard/select-centre", {
                                            pageTitle: "Online Service Booking Select a Centre",
                                            url: "/osb//wizard/select-centre",
                                            description: 'Select a centre page loaded',
                                            osbSeachLocation:$scope.osb.user.postcode,
                                            osbVrm:$scope.osb.user.vrm
                                          });

                        


                                              deferred.resolve();
                                          }, function(response) {

                                              deferred.resolve();


                                          });
                                      return deferred.promise;
                                  }

                              },
                              panels: {
                                  your_car: {
                                      show: true,
                                      open: true
                                  }
                              }
                          }, {
                              stepNumber: 2,
                              title: "Select a service",
                              templateUrl: "/etc/audi-ui/apps/osb/ng-components/wizard/steps/services.html",
                              showSteps: true,
                              showNext: true,
                              nextScreenLoadMessage: "<h2>Searching for available dates</h2><span>This may take a couple of moments...</span>",
                              showNextActive: function() {

                                  var ret = false
                                  angular.forEach(osbService.serviceObject, function(group) {
                                      angular.forEach(group.items, function(item) {
                                          if (item.checked) {
                                              ret = true;
                                          }
                                      })
                                  })

                                  return ret
                              },
                              preLoad: function() {
                                  var deferred = $q.defer();




                                  $q.all([
                                      osbService.getDealerDetails($scope.osb.dealer.dealerCode),
                                      osbService.getServicesByDealerCodeCarReg($scope.osb.dealer.dealerCode, $scope.osb.user.vrm),
                                      osbService.getFacilities($scope.osb.dealer.dealerCode) // if audi cam etc
                                  ]).then(function() {

                                      $analytics.pageTrack("/osb/wizard/select-service", {
                                          pageTitle: "Online Service Booking Select a Service",
                                          url: "/osb/wizard/select-service",
                                          description: 'Select a service page loaded',
                                          osbSelectDealer:$scope.osb.dealer.dealerCode
                                        });


                                      formChange('dealerCode', $scope.osb.dealer.dealerCode);

                                      deferred.resolve();
                                  });
                                  return deferred.promise;
                              },
                              panels: {
                                  services: {
                                      show: true,
                                      open: true
                                  },
                                  centre: {
                                      show: true,
                                      open: true
                                  },

                                  your_car: {
                                      show: true,
                                      open: false
                                  }
                              }
                          }, {
                              stepNumber: 3,
                              title: "Date & time",
                              templateUrl: "/etc/audi-ui/apps/osb/ng-components/wizard/steps/dateTime.html",
                              showSteps: true,
                              showNext: true,
                              showNextActive: function() {
                                  return osbService.isDateTimeValid()
                              },

                              preLoad: function() {


                                  formChange('services', osbService.getSelectedServiceCodes())
                                  var deferred = $q.defer();
                                  osbService.getCalendarAvailability().then(function() {
      

                                     $analytics.pageTrack("/osb/wizard/select-date", {
                                          pageTitle: "Online Service Booking Date and Time",
                                          url: "/osb/wizard/select-date",
                                          description: 'Select a time loaded',
                                          osbSelectedServices: osbService.getSelectedServiceCodes()
                                        });


                                      deferred.resolve();
                                  },function(error){
                                      deferred.reject(error);
                                  });
                                  return deferred.promise;
                              },
                              panels: {
                                  appointment: {
                                      show: true,
                                      open: true
                                  },
                                  services: {
                                      show: true,
                                      open: true
                                  },
                                  centre: {
                                      show: true,
                                      open: false
                                  },

                                  your_car: {
                                      show: true,
                                      open: false
                                  }
                              }
                          }, {
                              stepNumber: 4,
                              title: "Your details",
                              templateUrl: "/etc/audi-ui/apps/osb/ng-components/wizard/steps/yourDetails.html",
                              showSteps: true,
                              showNext: true,
                              nextButton: "Confirm booking",
                              preLoad:function(){


                    

                               var deferred = $q.defer();
                              $analytics.pageTrack("/osb/wizard/your-details", {
                                  pageTitle: "Online Service Booking Your Details",
                                  url: "/osb/wizard/your-details",
                                  description: 'Your details loaded'
                                });

                                   deferred.resolve();
                                 return deferred.promise;

                              },
                              showNextActive: function() {
                                  if (osb.user.profileData.title) {
                                      return true;
                                  } else {
                                      return true;
                                  }

                              },
                              panels: {
                                  appointment: {
                                      show: true,
                                      open: true
                                  },
                                  services: {
                                      show: true,
                                      open: true
                                  },
                                  centre: {
                                      show: true,
                                      open: true
                                  },

                                  your_car: {
                                      show: true,
                                      open: true
                                  },
                                  legal: {
                                      show: true,
                                      open: true
                                  }
                              }
                          }, {
                              stepNumber: 5,
                              title: "Confirmation",
                              templateUrl: "/etc/audi-ui/apps/osb/ng-components/wizard/steps/confirmation.html",
                              showSteps: false,
                              preLoad: function() {

                                  var deferred = $q.defer();
                                  // in yourDetailsCtrl
                                 
                                  $scope.osb.saveBooking().then(function() {


                                      $analytics.pageTrack("/osb/wizard/confirmation", {
                                          pageTitle: "Online Service Booking Confirmation",
                                          url: "/osb/wizard/confirmation",
                                          description: 'Confirmation loaded'
                                        });


                                      deferred.resolve();
                                  }, function() {
                                      deferred.reject();
                                  })
                                  return deferred.promise;



                              }
                          },

                      ]
                  }
              }




              var jumpToStep = 0; //$scope.osb.params.step;


              var init = function() {
                  console.log('wizardCtrl init')

                  if (typeof $scope.osb.params.postcode === "string" && typeof $scope.osb.params.vrm === "string") {
                      jumpToStep = 1;
                      $scope.osb.user.postcode = $scope.osb.params.postcode
                      $scope.osb.user.vrm = $scope.osb.params.vrm
                  } else {
                      $state.go('landing', {
                          postcode: $scope.osb.params.postcode,
                          vrm: $scope.osb.params.vrm
                      })
                  }

                  if (typeof $scope.osb.params.dealerCode === "string" && typeof $scope.osb.params.vrm === "string") {
                      jumpToStep = 2;
                      $scope.osb.dealer.dealerCode = $scope.osb.params.dealerCode
                      $scope.osb.user.vrm = $scope.osb.params.vrm
                      osbService.getCarInfoByCarReg($scope.osb.user.vrm);
                      osbService.getDealerDetails($scope.osb.dealer.dealerCode)
                  }

                  if (jumpToStep == 2 && typeof $scope.osb.params.services === "string") {

                      jumpToStep = 3; // set to 3  TODO
                      //  jumpToStep=4;
                      osbService.dealer.details.dealerCode = $scope.osb.params.dealerCode

                      osbService.setSelectedServiceCodes($scope.osb.params.services);
                  }



                  if (typeof $scope.osb.params.step === "string") {
                      jumpToStep = $scope.osb.params.step * 1;

                  }


                  if (jumpToStep || jumpToStep == 1) {
                      $scope.osb.wizard.goTo(jumpToStep);
                  }

                  //cache templates
                  angular.forEach($scope.osb.wizard.steps, function(step) {
                      templateCache.get(step.templateUrl);
                  })

                  /*$scope.$watch('osb.user.postcode',function(newValue,oldValue){
      //console.log('postcode changed',o,n)
      //var re = new RegExp("^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? ?[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/i");

     // if()


      })*/
              }









              $scope.osb.wizard.goTo = function(step) {
                  console.log('GO TO STEP:', step)
                  //if number go and fetch the item
                  if (typeof step === "number") {
                      console.log('step is number')
                      step = $scope.osb.wizard.steps[step - 1];
                  }


                  console.log('STEP', step)
                  if (typeof step.preLoad === "function") {
                      $scope.osb.loading = true;
                      step.preLoad().then(function() {
                          setStep(step);

                      }, function(error) {
                          $scope.osb.loading = false;

                      })
                  } else {
                      setStep(step);
                  }


              }

              var setStep = function(step) {
                  // called from goTo once everying is loaded
                  $scope.osb.wizard.furthestStepNumber = step.stepNumber
                  $scope.osb.wizard.currentStep = step.stepNumber
                  angular.copy(step, $scope.osb.wizard.currentStepObj);
                  // formChange('step',step.stepNumber);

                  $scope.osb.loading = false;


              }

              $scope.osb.next = function() {
                  $scope.osb.wizard.goTo($scope.osb.wizard.currentStep + 1);
              }









              /* Centre / dealerships */




              var formChangeTimeout;

              var formChange = function(field, value) {


                  if (typeof $scope.osb.params[field] == "undefined" || $scope.osb.params[field] != value) {

                      $scope.osb.params[field] = value;

                      $timeout.cancel(formChangeTimeout);

                      formChangeTimeout = $timeout(function() {
                          console.log('PARAMETER CHANGE - ' + field + ' changed to "' + value + '"', $scope.osb.params);


                          $state.go('wizard', $scope.osb.params, {
                              location: true,
                              notify: false,
                              reload: false
                          });
                      }, 500);
                  }
              }








              /*
                var stringOfServices  = function(services){

                  var servicesList = [];
                  angular.forEach(services,function(service){
                    servicesList.push(service.serviceDescription);

                  });
                  console.log('servicesList',servicesList,servicesList.join(','))

            return servicesList.join(',');

                }*/




              $scope.osb.estimatedTotal = function() {
                  return osbService.estimatedTotal();
              }




              var osb = $scope.osb; // put into normal var for modal to access
              var dealerDetailsModalInstanceCtrl = function($scope, $modalInstance, osbService) {
                  var currentMode;
                  $scope.osb = osb;
                  $scope.centre = osbService.dealer.details;
                  $scope.showBackToResults = true;

                  $scope.init = function() {
                      console.log('modal instance dealer', osbService.dealer.browserDetails)
                      if (!osbService.dealer.browserDetails.osbAvailable) {
                          $scope.showBackToResults = false
                      }
                      $scope.viewCentre(osbService.dealer.browserDetails);



                                   $analytics.pageTrack("/osb/wizard/view-centre-modal", {
                                            pageTitle: "Online Service Booking View Centre",
                                            url: "/osb/wizard/view-centre-modal",
                                            description: 'View centre modal loaded'
                                          });
                  }

                  $scope.setModelMode = function(panel) {
                      console.log('setModelMode:', panel);
                      if (panel != "loading") {
                          currentMode = panel;
                          $scope.loadingData = false;
                      } else {
                          $scope.warning = false;
                          $scope.loadingData = true;
                      }

                      //  $modalInstance.setSize(panel);
                      $scope.modalMode = "/etc/audi-ui/global/scripts/salmon.utils.modules/centreLocator/" + panel + ".html"
                  }

                  $scope.viewCentre = function(centre) {

                      console.log('viewCentre', centre)
                      $scope.loadingData = true;
                      $scope.setModelMode('loading');

                      osbService.getDealerDetails(centre.dealerCode).then(function() {
                          console.log('dealer loaded:', osbService.dealer)
                          $scope.map = {
                              zoom: 14,
                              latitude: osbService.dealer.details.location.latitude,
                              longitude: osbService.dealer.details.location.longitude,
                              title: osbService.dealer.details.name,
                              postcode: osbService.dealer.details.postcode
                          };
                          //    $scope.centre.address = $scope.formatDealerAddress($scope.centre.address);
                          $scope.setModelMode('details');
                          centre.loadingData = false;


                          $scope.centre.salesHours = groupServiceTimes(osbService.dealer.details.salesHours);
                          $scope.centre.serviceHours = groupServiceTimes(osbService.dealer.details.serviceHours);

                      }, function() {
                          $scope.setModelMode('error');
                      });
                  }



                  $scope.selectCentre = function(centre) {

                      if (window.console) console.log('selectCentre', centre, $scope);
                      //   $scope.loadingMessage = "Please wait whilst we set <u>"+centre.name+"</u> as your preffered centre";
                      $scope.setModelMode('loading');
                      $scope.osb.dealer.dealerCode = centre.dealerCode;

                      // $scope.selectCentreNext();
                      $scope.osb.next();

                      $modalInstance.close();


                  }

                  $scope.close = function() {

                      //$scope.loadingData =false
                      $modalInstance.close();
                  }

                  $scope.viewCentreList = function() {

                      $scope.close();
                  }





                  /* centre utils */
                  var groupServiceTimes = function(serviceHours) {

                      var group = false;
                      var weekHours = serviceHours[0].hours;

                      for (var i in serviceHours) {
                          var sh = serviceHours[i];
                          console.log(sh.days, ">>>>", i);
                          if (sh.days != 'Sat' && sh.days != 'Sun' && sh.days != "undefined") {
                              console.log(weekHours, sh.days, sh.hours);
                              if (weekHours != sh.hours) {
                                  return serviceHours;
                              }
                          } else {
                              group = true;
                              break;
                          }
                      }

                      if (group && serviceHours.length >= 7) {
                          serviceHours = serviceHours.slice(5, serviceHours.length);
                          serviceHours.unshift({
                              "days": "Mon - Fri",
                              "hours": weekHours
                          });
                      }
                      console.log('serviceHours', serviceHours)
                      return serviceHours;

                  };


                  $scope.init();
              }
              $scope.osb.viewDealer = function(dealer) {
                  console.log('viewDealer', dealer);
                  // dealer.loadingData = true;  
                  //    $scope.loadingData = true;                            
                  //$scope.setModelMode('loading');
                  osbService.dealer.browserDetails = dealer;
                  var modalInstance = $modal.open({
                      templateUrl: "/etc/audi-ui/global/scripts/salmon.utils.modules/centreLocator/template.html",
                      controller: dealerDetailsModalInstanceCtrl,
                      size: 'lg'
                  });
                  modalInstance.pScope = $scope;

              }









              init();



          }
      ]);


