


      osb.controller('dateTimeCtrl', ['$scope', 'osbService', '$modal', '$timeout', '$filter',
          function($scope, osbService, $modal, $timeout, $filter) {

              $scope.courtesyOptions = [{
                  title: "I'll <span class='red-text'>drop my car off</span>",
                  code: 'dropOff'
              }, {
                  title: "I want to <span class='red-text'>wait at the Centre</span>",
                  code: 'whileYouWait'
              }, {
                  title: "I want a <span class='red-text'>courtesy car</span>",
                  code: 'courtesyVehicle'
              }, {
                  title: "I want my car <span class='red-text'>collected</span>",
                  code: 'collectionDelivery'
              }, {
                  title: "I need a <span class='red-text'>lift</span>",
                  code: 'courtesyLift'
              }];


              $scope.user = osbService.user;
              $scope.dealer = osbService.dealer;

              $scope.minDate = new Date();



              $scope.totalTimeRequired = function() {
                  return osbService.totalTimeRequired();
              }

              $scope.filterDatesOnPostcodeArea = function() {
                  if (osbService.user.colDelPostcodeArea == 'Not on list') {

                      osbService.filterAvailability('collectionDeliveryAnyPostcode');
                  } else {
                      osbService.filterAvailability('collectionDelivery');
                  }
              }


              $scope.selectCourtesyOption = function(option) {
                  console.log('selectCourtesyOption:', option)
                  if (osbService.user.facilities.option != option) {
                      osbService.user.facilities.option = option;

                      osbService.filterAvailability(option);

                  }
              }

              $scope.isSelectedCourtesyOption = function(option) {
                  if (osbService.user.facilities.option == option) {
                      return true
                  }
              }

              // Disable weekend selection
              $scope.disabledDate = function(date, mode) {

                  if (date.getDate() == '01') {
                      osbService.getAvailability($filter('date')(date.getTime(), 'dd-MM-yyyy'));
                  }
                  return (mode === 'day' && (dateAvailable(date.getTime())));
              };

              var dateAvailable = function(calendarDate) {
                  var returnWhat = true;
                  angular.forEach($scope.osb.dealer.filteredAvailability, function(day) {

                      if (day.availableDate == (calendarDate - 43200000)) { //fix for 12pm and 12 am
                          returnWhat = false;
                      }
                  });
                  return returnWhat;
              }




              $scope.dateSelected = function(date) {
                  osbService.selectDate(date.getTime());
              }




              $scope.init();




          }
      ])

