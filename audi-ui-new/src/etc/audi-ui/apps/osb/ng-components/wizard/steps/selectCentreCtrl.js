


      osb.controller('selectCentreCtrl', ['$scope', 'osbService', '$modal', '$timeout', 'templateCache',
          function($scope, osbService, $modal, $timeout, templateCache) {

              $scope.centreSearch = function(newLocation) {
                  $scope.osb.loading = true;

                  osbService.getDealers(newLocation).then(function() {
                      osbService.user.searchLocation = newLocation
                      $scope.osb.loading = false;

                      osbService.checkIfPostcode(newLocation)

                  })

              }


              var init = function() {

                  if (!osbService.user.searchLocation) {
                      osbService.user.searchLocation = $scope.osb.user.postcode;

                  }
                  // preload templates
                  templateCache.get('/etc/audi-ui/global/scripts/salmon.utils.modules/centreLocator/template.html');
                  templateCache.get('/etc/audi-ui/global/scripts/salmon.utils.modules/locationMap/template.html');
              }

              init();



              $scope.selectDealer = function(dealer) {
                  $scope.osb.dealer.dealerCode = dealer.dealerCode;
                  $scope.osb.next();
              }





          }
      ])
