

     osb.controller('landingCtrl', ['$scope','$state','osbService',function($scope,$state,osbService){


$scope.params = angular.copy($state.params)

		$scope.vrm= $scope.params.vrm
		$scope.postcode=$scope.params.postcode;

		$scope.serverErrors ={}
		$scope.searching = "";

var init = function(){


		if($scope.vrm && $scope.postcode && typeof $scope.params.error=="undefined"){
			$scope.formGo();

		}
		if(typeof $scope.params.error=="string"){

			if($scope.params.error=="getCarInfoByCarRegFailed"){
				$scope.serverErrors.registration = "Unable to find vehicle. Please check the registration and try again"
			}
		}
}


     	$scope.formGo = function(){
			$scope.searching = "Loading...";
				$scope.serverErrors ={}
					console.log('VALIDATE FORM');


     		   osbService.getCarInfoByCarReg($scope.vrm).then(function(){
     		   	$state.go('wizard',{vrm:$scope.vrm,postcode:$scope.postcode})
     		   	console.log('CAR FOUND')

               },function(){
				$scope.serverErrors.registration = "Unable to find vehicle. Please check the registration and try again"
               	console.log('CAR NOT FOUND')
					$scope.searching = "";
               });



     	}







     	 $scope.fieldHasError = function(formName,fieldName ) {
     	 	  var form = $scope[formName],
                field = $scope.osbLandingForm[fieldName];
            return field.$invalid && (form.submitted || field.$hadFocus) ? true : false;
        }
        $scope.fieldHasFeedback = function(formName, fieldName ) {
            var form = $scope[formName],
                field = $scope.osbLandingForm[fieldName];
            return form.submitted || field.$hadFocus ? true : false;
        }

	var resetFormStatus = function (formName) {
        var form = $scope[formName];

        if (form.hasOwnProperty('submitted')) {
          form.submitted = false;
        }
      };
      $scope.getServerError = function( fieldName ) {
          if ( $scope.serverErrors[fieldName] !== undefined ) {
            return $scope.serverErrors[fieldName];
          } else {
            return false;
          }
        }




        init();



  		}
      ]);



