
console.log('financeCalculator module loaded');


var financeCalculator = angular.module('financeCalculator', [
			'ui.router',
			'ngResource',
			'ngAnimate',
			'ngSanitize',
			'ui.bootstrap',
			'sa.utils',
			'dcbImgFallback',
            'angulartics',
            'angulartics.adobe.analytics',
]);

 

 
/* fred */
financeCalculator.config(['$stateProvider', '$urlRouterProvider',
	function ($stateProvider, $urlRouterProvider) {


			$stateProvider
			.state('ranges', {
				url: "/ranges?range",
				templateUrl: "/etc/audi-ui/apps/finance-calculator/ng-components/selectVehicle/selectRange.html",
				controller: "selectModelCtrl",
				reloadOnSearch: false
			})
			.state('calculator', {
				url: "/calculator?range&model&trim&modelCode&productType&deposit&mileage&period",
				templateUrl: "/etc/audi-ui/apps/finance-calculator/ng-components/calculateTool/calculateTool.html",
				controller: "calculateToolCtrl",
				reloadOnSearch: false
			})
			.state('error', {
				url: "/error",
				templateUrl: "/etc/audi-ui/apps/finance-calculator/ng-components/error/error.html",
				controller: "errorCtrl",
				reloadOnSearch: false
			})




}])




	financeCalculator.run(['$state', '$timeout', '$urlRouter','$rootScope', function($state, $timeout, $urlRouter, $rootScope) {
		var init = true;
	$state.go('ranges');
console.log('HTTP RUN')
	
		// On init...
		$rootScope.$on('$stateChangeStart', function(evt) {
			console.log('rootchange')
      		// If this isnt the first (init) run, bail.
      		if(!init) return;
      		init = false;

			// Halt state change from even starting
      		evt.preventDefault();

			// Sort out where we are going to get our data from
			if(typeof webServicesHost === 'undefined' || !webServicesHost.trim().length) {
				// Not configured? Kick to error screen.
				console.log('CONFIG ERROR - No webservice configuration found!');
				$state.go('error');
			}
			else if(webServicesHost.toLowerCase() === 'mockdata') {
				// use local mock data - this should only ever happen on a dev box
				console.log('CONFIG FOUND - Using mock data');
				financeCalculator.dataSources = {
					CQ5Properties: '/etc/audi-ui/apps/finance-calculator/mockData/cq5-properties.html',
					rangeData: '/etc/audi-ui/apps/finance-calculator/mockData/RangeModelTrimEngine.js',
					quoteData: '/etc/audi-ui/apps/finance-calculator/mockData/quote.js',
					financeUnavailable: '/etc/audi-ui/apps/finance-calculator/mockData/error-finance-unavailable.js',
					downloadPDF: 'http://webservices.audi.co.uk.uat.nativ-systems.com/pdb-webservices/services/rest/finance/v2/quote/pdf',
					sendQuoteEmail: '/etc/audi-ui/apps/finance-calculator/mockData/emailquote.js'
				};
				$urlRouter.sync();
			}
			else {
				console.log('CONFIG FOUND - Using data from ' + webServicesHost);
				financeCalculator.dataSources = {
					CQ5Properties: '/content/audi/ui-testapps/finance_calculatordictionary.html',
					rangeData: webServicesHost + '/pdb-webservices/services/rest/pdb/getCarModels',
					quoteData: webServicesHost + '/pdb-webservices/services/rest/finance/v2/quote/json',
					downloadPDF: webServicesHost + '/pdb-webservices/services/rest/finance/v2/quote/pdf',
					sendQuoteEmail: webServicesHost + '/pdb-webservices/services/rest/finance/v2/quote/email'
				};
			$urlRouter.sync();
			}
		}); 
	}]);


financeCalculator.run(['$state', function ($state) {
	console.log('ranges LOADING. GO TO STATE')
	$state.go('ranges');
	console.log('OSB LOADING. GO TO STATE DONE')
}]);


	// fix strange issue with the spinner having to compleate a full 360 before being hidden
	financeCalculator.config(function ($animateProvider) {
		$animateProvider.classNameFilter(/^((?!(rotate360)).)*$/);
	});

	// // Delay the mock ajax request to simulate the real thing..
	// /* DEV ONLY */
	// financeCalculator.config(function ($provide) {
	// 	$provide.decorator('$httpBackend', ngMock.e2e.$httpBackendDecorator);
	// })
	// financeCalculator.config(function ($provide) {
	// 	$provide.decorator('$httpBackend', function ($delegate) {
	// 		var proxy = function (method, url, data, callback, headers) {
	// 			var interceptor = function () {
	// 				var _this = this,
	// 					_arguments = arguments;
	// 				setTimeout(function () {
	// 					callback.apply(_this, _arguments);
	// 				}, 2000);
	// 			};
	// 			return $delegate.call(this, method, url, data, interceptor, headers);
	// 		};
	// 		for (var key in $delegate) {
	// 			proxy[key] = $delegate[key];
	// 		}
	// 		return proxy;
	// 	});
	// });

	financeCalculator.config(function ($httpProvider) {
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
	});

	financeCalculator.config(['$locationProvider',
		function ($locationProvider) {
			$locationProvider.hashPrefix('!');
		}
	]);

	financeCalculator.config(['$analyticsProvider',
		function ($analyticsProvider) {
			// turn off automatic tracking
			$analyticsProvider.virtualPageviews(false);
		}
	]);


