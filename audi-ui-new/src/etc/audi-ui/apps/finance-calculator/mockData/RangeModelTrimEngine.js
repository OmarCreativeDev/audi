{
	"resultType": "SUCCESS",
	"data": [{
		"rangeName": "A1",
		"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A1/A1_front.png",
		"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A1/A1.png",
		"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Range/small_A1.png",
		"models": [{
			"name": "S1",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A1/S1/low_res/S1_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A1/S1/low_res/S1.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A1/small_S1.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "24905.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A1/S1/low_res/S1_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A1/S1/low_res/S1_SE.png",
				"engines": [{
					"modelCode": "8XKSF9\\1",
					"capcode": "AUA120S1 3HPTM4",
					"engine": "2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "24905.00"
				}]
			}]
		}, {
			"name": "S1 Sportback",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A1/S1_Sportback/low_res/S1_Sportback_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A1/S1_Sportback/low_res/S1_Sportback.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A1/small_S1_Sportback.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "25635.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A1/S1_Sportback/low_res/S1_Sportback_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A1/S1_Sportback/low_res/S1_Sportback_SE.png",
				"engines": [{
					"modelCode": "8XFSF9\\1",
					"capcode": "AUA120S1 5HPTM4",
					"engine": "2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "25635.00"
				}]
			}]
		}]
	}, {
		"rangeName": "A3",
		"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_front.png",
		"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3.png",
		"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Range/small_A3.png",
		"models": [{
			"name": "A3",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3/low_res/A3_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3/low_res/A3.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A3/small_A3.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "18575.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3/low_res/A3_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3/low_res/A3_SE.png",
				"engines": [{
					"modelCode": "8V1AKC\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZQ\\MMFA9S5",
					"capcode": "AUA31210S3HPTM  3",
					"engine": "SE 1.2 TFSI",
					"displayEngineName": "1.2 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "18575.00"
				}, {
					"modelCode": "8V1AAC\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZQ\\MMFA9S5",
					"capcode": "AUA31412E3HPTM  3",
					"engine": "SE 1.4 TFSI",
					"displayEngineName": "1.4 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "19875.00"
				}, {
					"modelCode": "8V1AKG\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZS\\MMFA9S5",
					"capcode": "AUA31210S3HPTA  3",
					"engine": "SE 1.2 TFSI",
					"displayEngineName": "1.2 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "20055.00"
				}, {
					"modelCode": "8V1ACC\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZQ\\MMFA9S5",
					"capcode": "AUA314S503HPTM  3",
					"engine": "SE 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "Manual",
					"otrPriceMin": "20725.00"
				}, {
					"modelCode": "8V1AFC\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZQ\\MMFA9S5",
					"capcode": "AUA31610S3HDTM  3",
					"engine": "SE 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "Manual",
					"otrPriceMin": "20825.00"
				}, {
					"modelCode": "8V1AAG\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZS\\MMFA9S5",
					"capcode": "AUA31412E3HPTA  3",
					"engine": "SE 1.4 TFSI",
					"displayEngineName": "1.4 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "21355.00"
				}, {
					"modelCode": "8V1ARC\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZQ\\MMFA9S5",
					"capcode": "AUA320SE 3HDTM  3",
					"engine": "SE 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "22175.00"
				}, {
					"modelCode": "8V1ACG\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZS\\MMFA9S5",
					"capcode": "AUA314S503HPTA  3",
					"engine": "SE 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "S tronic",
					"otrPriceMin": "22205.00"
				}, {
					"modelCode": "8V1AFG\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZS\\MMFA9S5",
					"capcode": "AUA31610S3HDTA  3",
					"engine": "SE 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "22305.00"
				}, {
					"modelCode": "8V1ARX\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZS\\MMFA9S5",
					"capcode": "AUA320SE 3HDTA  3",
					"engine": "SE 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "23655.00"
				}]
			}, {
				"name": "Sport",
				"otrPriceMin": "19975.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3/low_res/A3_Sport_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3/low_res/A3_Sport.png",
				"engines": [{
					"modelCode": "8V1BKC\\0",
					"capcode": "AUA31211S3HPTM  3",
					"engine": "Sport 1.2 TFSI",
					"displayEngineName": "1.2 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "19975.00"
				}, {
					"modelCode": "8V1BAC\\0",
					"capcode": "AUA31412S3HPTM  3",
					"engine": "Sport 1.4 TFSI",
					"displayEngineName": "1.4 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "21275.00"
				}, {
					"modelCode": "8V1BKG\\0",
					"capcode": "AUA31211S3HPTA  3",
					"engine": "Sport 1.2 TFSI",
					"displayEngineName": "1.2 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "21455.00"
				}, {
					"modelCode": "8V1BCC\\0",
					"capcode": "AUA3145SP3HPTM  3",
					"engine": "Sport 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "Manual",
					"otrPriceMin": "22125.00"
				}, {
					"modelCode": "8V1BFC\\0",
					"capcode": "AUA31611S3HDTM  3",
					"engine": "Sport 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "Manual",
					"otrPriceMin": "22225.00"
				}, {
					"modelCode": "8V1BAG\\0",
					"capcode": "AUA31412S3HPTA  3",
					"engine": "Sport 1.4 TFSI",
					"displayEngineName": "1.4 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "22755.00"
				}, {
					"modelCode": "8V1BRC\\0",
					"capcode": "AUA320SPO3HDTM  3",
					"engine": "Sport 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "23575.00"
				}, {
					"modelCode": "8V1BCG\\0",
					"capcode": "AUA3145SP3HPTA  3",
					"engine": "Sport 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "S tronic",
					"otrPriceMin": "23605.00"
				}, {
					"modelCode": "8V1BFG\\0",
					"capcode": "AUA31611S3HDTA  3",
					"engine": "Sport 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "23705.00"
				}, {
					"modelCode": "8V1B6C\\0",
					"capcode": "AUA318SPO3HPTM  3",
					"engine": "Sport 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "23905.00"
				}, {
					"modelCode": "8V1BRX\\0",
					"capcode": "AUA320SPO3HDTA  3",
					"engine": "Sport 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "25055.00"
				}, {
					"modelCode": "8V1B6G\\0",
					"capcode": "AUA318SPO3HPTA  3",
					"engine": "Sport 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "25255.00"
				}, {
					"modelCode": "8V1B6L\\0",
					"capcode": "AUA318SPO3HPTA4 3",
					"engine": "Sport 1.8 TFSI quattro",
					"displayEngineName": "1.8 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "26830.00"
				}, {
					"modelCode": "8V1BTL\\0",
					"capcode": "AUA320SPO3HDTA4 3",
					"engine": "Sport 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "27755.00"
				}]
			}, {
				"name": "SE Technik",
				"otrPriceMin": "21575.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3/low_res/A3_SE_Technik_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3/low_res/A3_SE_Technik.png",
				"engines": [{
					"modelCode": "8V1AFC\\0\\GPNUPNU\\GWF5WF5\\MLRA2ZQ\\MGRA8T2\\MMFA9S6",
					"capcode": "AUA316STK3HDTM  3",
					"engine": "SE Technik 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "Manual",
					"otrPriceMin": "21575.00"
				}, {
					"modelCode": "8V1ARC\\0\\GPNUPNU\\GWF5WF5\\MLRA2ZQ\\MGRA8T2\\MMFA9S6",
					"capcode": "AUA320STK3HDTM  3",
					"engine": "SE Technik 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "22925.00"
				}, {
					"modelCode": "8V1AFG\\0\\GPNUPNU\\GWF5WF5\\MLRA2ZS\\MGRA8T2\\MMFA9S6",
					"capcode": "AUA316STK3HDTA  3",
					"engine": "SE Technik 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "23055.00"
				}, {
					"modelCode": "8V1ARX\\0\\GPNUPNU\\GWF5WF5\\MLRA2ZS\\MGRA8T2\\MMFA9S6",
					"capcode": "AUA320STK3HDTA  3",
					"engine": "SE Technik 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "24405.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "22125.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3/low_res/A3_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3/low_res/A3_S_line.png",
				"engines": [{
					"modelCode": "8V1BKC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA31211L3HPTM  3",
					"engine": "S line 1.2 TFSI",
					"displayEngineName": "1.2 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "22125.00"
				}, {
					"modelCode": "8V1BAC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA31412L3HPTM  3",
					"engine": "S line 1.4 TFSI",
					"displayEngineName": "1.4 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "23425.00"
				}, {
					"modelCode": "8V1BKG\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA31211L3HPTA  3",
					"engine": "S line 1.2 TFSI",
					"displayEngineName": "1.2 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "23605.00"
				}, {
					"modelCode": "8V1BCC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA314L503HPTM  3",
					"engine": "S line 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "Manual",
					"otrPriceMin": "24275.00"
				}, {
					"modelCode": "8V1BFC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA31611L3HDTM  3",
					"engine": "S line 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "Manual",
					"otrPriceMin": "24375.00"
				}, {
					"modelCode": "8V1BAG\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA31412L3HPTA  3",
					"engine": "S line 1.4 TFSI",
					"displayEngineName": "1.4 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "24905.00"
				}, {
					"modelCode": "8V1BRC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA320SL 3HDTM  3",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "25725.00"
				}, {
					"modelCode": "8V1BCG\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA314L503HPTA  3",
					"engine": "S line 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "S tronic",
					"otrPriceMin": "25755.00"
				}, {
					"modelCode": "8V1BFG\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA31611L3HDTA  3",
					"engine": "S line 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "25855.00"
				}, {
					"modelCode": "8V1B6C\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA318SL 3HPTM  3",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "26055.00"
				}, {
					"modelCode": "8V1BRX\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA320SL 3HDTA  3",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "27205.00"
				}, {
					"modelCode": "8V1B6G\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA318SL 3HPTA  3",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "27405.00"
				}, {
					"modelCode": "8V1B6L\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA318SL 3HPTA4 3",
					"engine": "S line 1.8 TFSI quattro",
					"displayEngineName": "1.8 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "28980.00"
				}, {
					"modelCode": "8V1BTL\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA320SL 3HDTA4 3",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "29905.00"
				}]
			}]
		}, {
			"name": "A3 Sportback",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Sportback/low_res/A3_Sportback_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Sportback/low_res/A3_Sportback.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A3/small_A3_Sportback.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "19195.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Sportback/low_res/A3_Sportback_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Sportback/low_res/A3_Sportback_SE.png",
				"engines": [{
					"modelCode": "8VAAKC\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZQ\\MMFA9S5",
					"capcode": "AUA31210S5HPTM  3",
					"engine": "SE 1.2 TFSI",
					"displayEngineName": "1.2 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "19195.00"
				}, {
					"modelCode": "8VAAAC\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZQ\\MMFA9S5",
					"capcode": "AUA31412E5HPTM  3",
					"engine": "SE 1.4 TFSI",
					"displayEngineName": "1.4 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "20495.00"
				}, {
					"modelCode": "8VAAKG\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZS\\MMFA9S5",
					"capcode": "AUA31210S5HPTA  3",
					"engine": "SE 1.2 TFSI",
					"displayEngineName": "1.2 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "20675.00"
				}, {
					"modelCode": "8VAACC\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZQ\\MMFA9S5",
					"capcode": "AUA314S505HPTM  3",
					"engine": "SE 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "Manual",
					"otrPriceMin": "21345.00"
				}, {
					"modelCode": "8VAAFC\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZQ\\MMFA9S5",
					"capcode": "AUA31610S5HDTM  3",
					"engine": "SE 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "Manual",
					"otrPriceMin": "21445.00"
				}, {
					"modelCode": "8VAAAG\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZS\\MMFA9S5",
					"capcode": "AUA31412E5HPTA  3",
					"engine": "SE 1.4 TFSI",
					"displayEngineName": "1.4 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "21975.00"
				}, {
					"modelCode": "8VAARC\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZQ\\MMFA9S5",
					"capcode": "AUA320SE 5HDTM  3",
					"engine": "SE 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "22795.00"
				}, {
					"modelCode": "8VAACG\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZS\\MMFA9S5",
					"capcode": "AUA314S505HPTA  3",
					"engine": "SE 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "S tronic",
					"otrPriceMin": "22825.00"
				}, {
					"modelCode": "8VAAFG\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZS\\MMFA9S5",
					"capcode": "AUA31610S5HDTA  3",
					"engine": "SE 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "22925.00"
				}, {
					"modelCode": "8VAARX\\0\\MRADC2L\\GPNVPNV\\GWB1WB1\\MLRA2ZS\\MMFA9S5",
					"capcode": "AUA320SE 5HDTA  3",
					"engine": "SE 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "24275.00"
				}]
			}, {
				"name": "Sport",
				"otrPriceMin": "20595.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Sportback/low_res/A3_Sportback_Sport_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Sportback/low_res/A3_Sportback_Sport.png",
				"engines": [{
					"modelCode": "8VABKC\\0",
					"capcode": "AUA31211S5HPTM  3",
					"engine": "Sport 1.2 TFSI",
					"displayEngineName": "1.2 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "20595.00"
				}, {
					"modelCode": "8VABAC\\0",
					"capcode": "AUA31412S5HPTM  3",
					"engine": "Sport 1.4 TFSI",
					"displayEngineName": "1.4 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "21895.00"
				}, {
					"modelCode": "8VABKG\\0",
					"capcode": "AUA31211S5HPTA  3",
					"engine": "Sport 1.2 TFSI",
					"displayEngineName": "1.2 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "22075.00"
				}, {
					"modelCode": "8VABCC\\0",
					"capcode": "AUA3145SP5HPTM  3",
					"engine": "Sport 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "Manual",
					"otrPriceMin": "22745.00"
				}, {
					"modelCode": "8VABFC\\0",
					"capcode": "AUA31611S5HDTM  3",
					"engine": "Sport 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "Manual",
					"otrPriceMin": "22845.00"
				}, {
					"modelCode": "8VABAG\\0",
					"capcode": "AUA31412S5HPTA  3",
					"engine": "Sport 1.4 TFSI",
					"displayEngineName": "1.4 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "23375.00"
				}, {
					"modelCode": "8VABRC\\0",
					"capcode": "AUA320SP 5HDTM  3",
					"engine": "Sport 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "24195.00"
				}, {
					"modelCode": "8VABCG\\0",
					"capcode": "AUA3145SP5HPTA  3",
					"engine": "Sport 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "S tronic",
					"otrPriceMin": "24225.00"
				}, {
					"modelCode": "8VABFG\\0",
					"capcode": "AUA31611S5HDTA  3",
					"engine": "Sport 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "24325.00"
				}, {
					"modelCode": "8VAB6C\\0",
					"capcode": "AUA318SPO5HPTM  3",
					"engine": "Sport 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "24525.00"
				}, {
					"modelCode": "8VABRX\\0",
					"capcode": "AUA320SP 5HDTA  3",
					"engine": "Sport 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "25675.00"
				}, {
					"modelCode": "8VAB6G\\0",
					"capcode": "AUA318SP 5HPTA  3",
					"engine": "Sport 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "25875.00"
				}, {
					"modelCode": "8VAB6L\\0",
					"capcode": "AUA318SPO5HPTA4 3",
					"engine": "Sport 1.8 TFSI quattro",
					"displayEngineName": "1.8 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "27450.00"
				}, {
					"modelCode": "8VABTL\\0",
					"capcode": "AUA320SPO5HDTA4 3",
					"engine": "Sport 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "28375.00"
				}]
			}, {
				"name": "SE Technik",
				"otrPriceMin": "22195.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Sportback/low_res/A3_Sportback_SE_Technik_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Sportback/low_res/A3_Sportback_SE_Technik.png",
				"engines": [{
					"modelCode": "8VAAFC\\0\\GPNUPNU\\GWF5WF5\\MLRA2ZQ\\MGRA8T2\\MMFA9S6",
					"capcode": "AUA316STK5HDTM  3",
					"engine": "SE Technik 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "Manual",
					"otrPriceMin": "22195.00"
				}, {
					"modelCode": "8VAARC\\0\\GPNUPNU\\GWF5WF5\\MLRA2ZQ\\MGRA8T2\\MMFA9S6",
					"capcode": "AUA320STK5HDTM  3",
					"engine": "SE Technik 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "23545.00"
				}, {
					"modelCode": "8VAAFG\\0\\GPNUPNU\\GWF5WF5\\MLRA2ZS\\MGRA8T2\\MMFA9S6",
					"capcode": "AUA316STK5HDTA  3",
					"engine": "SE Technik 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "23675.00"
				}, {
					"modelCode": "8VAARX\\0\\GPNUPNU\\GWF5WF5\\MLRA2ZS\\MGRA8T2\\MMFA9S6",
					"capcode": "AUA320STK5HDTA  3",
					"engine": "SE Technik 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "25025.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "22745.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Sportback/low_res/A3_Sportback_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Sportback/low_res/A3_Sportback_S_line.png",
				"engines": [{
					"modelCode": "8VABKC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA31211L5HPTM  3",
					"engine": "S line 1.2 TFSI",
					"displayEngineName": "1.2 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "22745.00"
				}, {
					"modelCode": "8VABAC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA31412L5HPTM  3",
					"engine": "S line 1.4 TFSI",
					"displayEngineName": "1.4 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "24045.00"
				}, {
					"modelCode": "8VABKG\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA31211L5HPTA  3",
					"engine": "S line 1.2 TFSI",
					"displayEngineName": "1.2 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "24225.00"
				}, {
					"modelCode": "8VABCC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA314L505HPTM  3",
					"engine": "S line 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "Manual",
					"otrPriceMin": "24895.00"
				}, {
					"modelCode": "8VABFC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA31611L5HDTM  3",
					"engine": "S line 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "Manual",
					"otrPriceMin": "24995.00"
				}, {
					"modelCode": "8VABAG\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA31412L5HPTA  3",
					"engine": "S line 1.4 TFSI",
					"displayEngineName": "1.4 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "25525.00"
				}, {
					"modelCode": "8VABRC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA320SL 5HDTM  3",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "26345.00"
				}, {
					"modelCode": "8VABCG\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA314L505HPTA  3",
					"engine": "S line 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "S tronic",
					"otrPriceMin": "26375.00"
				}, {
					"modelCode": "8VABFG\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA31611L5HDTA  3",
					"engine": "S line 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "26475.00"
				}, {
					"modelCode": "8VAB6C\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA318SL 5HPTM  3",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "26675.00"
				}, {
					"modelCode": "8VABRX\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA320SL 5HDTA  3",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "27825.00"
				}, {
					"modelCode": "8VAB6G\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA318SL 5HPTA  3",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "28025.00"
				}, {
					"modelCode": "8VAB6L\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA318SL 5HPTA4 3",
					"engine": "S line 1.8 TFSI quattro",
					"displayEngineName": "1.8 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "29600.00"
				}, {
					"modelCode": "8VABTL\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA320SL 5HDTA4 3",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "30525.00"
				}]
			}, {
				"name": "e-tron",
				"otrPriceMin": "34950.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Sportback/low_res/A3_Sportback_e-tron_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Sportback/low_res/A3_Sportback_e-tron.png",
				"engines": [{
					"modelCode": "8VABPX\\1\\MONLEL5\\GPNQPNQ\\GPX2PX2\\MEPH7X1\\MNES8WM",
					"capcode": "AUA314ET 5HXTA  3",
					"engine": "  e-tron",
					"displayEngineName": " ",
					"gearType": "S tronic",
					"otrPriceMin": "34950.00"
				}]
			}]
		}, {
			"name": "A3 Saloon",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Saloon/low_res/A3_Saloon_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Saloon/low_res/A3_Saloon.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A3/small_A3_Saloon.png",
			"trimlines": [{
				"name": "Sport",
				"otrPriceMin": "23295.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Saloon/low_res/A3_Saloon_Sport_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Saloon/low_res/A3_Saloon_Sport.png",
				"engines": [{
					"modelCode": "8VSBCC\\0",
					"capcode": "AUA3145SP4SPTM",
					"engine": "Sport 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "Manual",
					"otrPriceMin": "23295.00"
				}, {
					"modelCode": "8VSBFC\\0",
					"capcode": "AUA31611S4SDTM",
					"engine": "Sport 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "Manual",
					"otrPriceMin": "23395.00"
				}, {
					"modelCode": "8VSBRC\\0",
					"capcode": "AUA320SPO4SDTM",
					"engine": "Sport 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "24745.00"
				}, {
					"modelCode": "8VSBCG\\0",
					"capcode": "AUA3145SP4SPTA",
					"engine": "Sport 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "S tronic",
					"otrPriceMin": "24775.00"
				}, {
					"modelCode": "8VSBFG\\0",
					"capcode": "AUA31611S4SDTA",
					"engine": "Sport 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "24875.00"
				}, {
					"modelCode": "8VSB6C\\0",
					"capcode": "AUA318SPO4SPTM",
					"engine": "Sport 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "25075.00"
				}, {
					"modelCode": "8VSBRX\\0",
					"capcode": "AUA320SPO4SDTA",
					"engine": "Sport 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "26225.00"
				}, {
					"modelCode": "8VSB6G\\0",
					"capcode": "AUA318SPO4SPTA",
					"engine": "Sport 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "26425.00"
				}, {
					"modelCode": "8VSB6L\\0",
					"capcode": "AUA318SPO4SPTA4",
					"engine": "Sport 1.8 TFSI quattro",
					"displayEngineName": "1.8 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "28000.00"
				}, {
					"modelCode": "8VSBTL\\0",
					"capcode": "AUA320SP84SDTA4",
					"engine": "Sport 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "28925.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "25445.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Saloon/low_res/A3_Saloon_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Saloon/low_res/A3_Saloon_S_line.png",
				"engines": [{
					"modelCode": "8VSBCC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA314L504SPTM",
					"engine": "S line 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "Manual",
					"otrPriceMin": "25445.00"
				}, {
					"modelCode": "8VSBFC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA31611L4SDTM",
					"engine": "S line 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "Manual",
					"otrPriceMin": "25545.00"
				}, {
					"modelCode": "8VSBRC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA320SL 4SDTM",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "26895.00"
				}, {
					"modelCode": "8VSBCG\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA314L504SPTA",
					"engine": "S line 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "S tronic",
					"otrPriceMin": "26925.00"
				}, {
					"modelCode": "8VSBFG\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA31611L4SDTA",
					"engine": "S line 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "27025.00"
				}, {
					"modelCode": "8VSB6C\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA318SL 4SPTM",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "27225.00"
				}, {
					"modelCode": "8VSBRX\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA320SL 4SDTA",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "28375.00"
				}, {
					"modelCode": "8VSB6G\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA318SL 4SPTA",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "28575.00"
				}, {
					"modelCode": "8VSB6L\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA318SL 4SPTA4",
					"engine": "S line 1.8 TFSI quattro",
					"displayEngineName": "1.8 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "30150.00"
				}, {
					"modelCode": "8VSBTL\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA320L844SDTA4",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "31075.00"
				}]
			}]
		}, {
			"name": "A3 Cabriolet",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Cabriolet/low_res/A3_Cabriolet_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Cabriolet/low_res/A3_Cabriolet.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A3/small_A3_Cabriolet.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "26085.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Cabriolet/low_res/A3_Cabriolet_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Cabriolet/low_res/A3_Cabriolet_SE.png",
				"engines": [{
					"modelCode": "8V7ACC\\0\\GPNVPNV\\GWB1WB1\\MLRA2ZQ\\MMFA9S5",
					"capcode": "AUA314S502BPTM  1",
					"engine": "SE 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "Manual",
					"otrPriceMin": "26085.00"
				}, {
					"modelCode": "8V7AFC\\1\\GPNVPNV\\GWB1WB1\\MLRA2ZQ\\MMFA9S5",
					"capcode": "AUA316SE 2BDTM  1",
					"engine": "SE 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "Manual",
					"otrPriceMin": "26185.00"
				}, {
					"modelCode": "8V7ARC\\2\\GPNVPNV\\GWB1WB1\\MLRA2ZQ\\MMFA9S5",
					"capcode": "AUA320SE 2BDTM  1",
					"engine": "SE 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "27535.00"
				}, {
					"modelCode": "8V7ACG\\0\\GPNVPNV\\GWB1WB1\\MLRA2ZS\\MMFA9S5",
					"capcode": "AUA314S502BPTA  1",
					"engine": "SE 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "S tronic",
					"otrPriceMin": "27565.00"
				}, {
					"modelCode": "8V7ARX\\1\\GPNVPNV\\GWB1WB1\\MLRA2ZS\\MMFA9S5",
					"capcode": "AUA320SE 2BDTA  1",
					"engine": "SE 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "29015.00"
				}]
			}, {
				"name": "Sport",
				"otrPriceMin": "27485.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Cabriolet/low_res/A3_Cabriolet_Sport_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Cabriolet/low_res/A3_Cabriolet_Sport.png",
				"engines": [{
					"modelCode": "8V7BCC\\0",
					"capcode": "AUA3145SP2BPTM  1",
					"engine": "Sport 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "Manual",
					"otrPriceMin": "27485.00"
				}, {
					"modelCode": "8V7BFC\\1",
					"capcode": "AUA316SPO2BDTM  1",
					"engine": "Sport 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "Manual",
					"otrPriceMin": "27585.00"
				}, {
					"modelCode": "8V7BRC\\2",
					"capcode": "AUA320SPO2BDTM  1",
					"engine": "Sport 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "28935.00"
				}, {
					"modelCode": "8V7BCG\\0",
					"capcode": "AUA3145SP2BPTA  1",
					"engine": "Sport 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "S tronic",
					"otrPriceMin": "28965.00"
				}, {
					"modelCode": "8V7B6C\\2",
					"capcode": "AUA318SPO2BPTM  1",
					"engine": "Sport 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "29265.00"
				}, {
					"modelCode": "8V7BRX\\1",
					"capcode": "AUA320SPO2BDTA  1",
					"engine": "Sport 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "30415.00"
				}, {
					"modelCode": "8V7B6G\\2",
					"capcode": "AUA318SPO2BPTA  1",
					"engine": "Sport 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "30745.00"
				}, {
					"modelCode": "8V7B6L\\1",
					"capcode": "AUA318SPO2BPTA4 1",
					"engine": "Sport 1.8 TFSI quattro",
					"displayEngineName": "1.8 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "32225.00"
				}, {
					"modelCode": "8V7BTL\\0",
					"capcode": "AUA320SP82BDTA4 1",
					"engine": "Sport 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "33115.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "29635.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Cabriolet/low_res/A3_Cabriolet_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/A3_Cabriolet/low_res/A3_Cabriolet_S_line.png",
				"engines": [{
					"modelCode": "8V7BCC\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA314L502BPTM  1",
					"engine": "S line 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "Manual",
					"otrPriceMin": "29635.00"
				}, {
					"modelCode": "8V7BFC\\1\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA316SL 2BDTM  1",
					"engine": "S line 1.6 TDI",
					"displayEngineName": "1.6 TDI",
					"gearType": "Manual",
					"otrPriceMin": "29735.00"
				}, {
					"modelCode": "8V7BRC\\2\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA320SL 2BDTM  1",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "31085.00"
				}, {
					"modelCode": "8V7BCG\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA314L502BPTA  1",
					"engine": "S line 1.4 TFSI cylinder on demand",
					"displayEngineName": "1.4 TFSI cylinder on demand",
					"gearType": "S tronic",
					"otrPriceMin": "31115.00"
				}, {
					"modelCode": "8V7B6C\\2\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PF",
					"capcode": "AUA318SL 2BPTM  1",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "31415.00"
				}, {
					"modelCode": "8V7BRX\\1\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA320SL 2BDTA  1",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "32565.00"
				}, {
					"modelCode": "8V7B6G\\2\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA318SL 2BPTA  1",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "S tronic",
					"otrPriceMin": "32895.00"
				}, {
					"modelCode": "8V7B6L\\1\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA318SL 2BPTA4 1",
					"engine": "S line 1.8 TFSI quattro",
					"displayEngineName": "1.8 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "34375.00"
				}, {
					"modelCode": "8V7BTL\\0\\MSIBN7V\\GPQDPQD\\GPX4PX4\\GWQVWQV\\MLRA2PK",
					"capcode": "AUA320L842BDTA4 1",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "35265.00"
				}]
			}]
		}, {
			"name": "S3",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3/low_res/S3_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3/low_res/S3.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A3/small_S3.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "30940.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3/low_res/S3_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3/low_res/S3_SE.png",
				"engines": [{
					"modelCode": "8V1S39\\0",
					"capcode": "AUA320S3Q3HPTM4 3",
					"engine": "2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "30940.00"
				}, {
					"modelCode": "8V1S3L\\0",
					"capcode": "AUA320S3Q3HPTA4 3",
					"engine": "2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "32420.00"
				}]
			}]
		}, {
			"name": "S3 Sportback",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3_Sportback/low_res/S3_Sportback_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3_Sportback/low_res/S3_Sportback.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A3/small_S3_Sportback.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "31560.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3_Sportback/low_res/S3_Sportback_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3_Sportback/low_res/S3_Sportback_SE.png",
				"engines": [{
					"modelCode": "8VAS39\\0",
					"capcode": "AUA320S3Q5HPTM4 3",
					"engine": "2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "31560.00"
				}, {
					"modelCode": "8VAS3L\\0",
					"capcode": "AUA320S3Q5HPTA4 3",
					"engine": "2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "33040.00"
				}]
			}]
		}, {
			"name": "S3 Saloon",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3_Saloon/low_res/S3_Saloon_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3_Saloon/low_res/S3_Saloon.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A3/small_S3_Saloon.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "33540.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3_Saloon/low_res/S3_Saloon_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3_Saloon/low_res/S3_Saloon_SE.png",
				"engines": [{
					"modelCode": "8VSS39\\0",
					"capcode": "AUA320S3Q4SPTM4",
					"engine": "2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "33540.00"
				}, {
					"modelCode": "8VSS3L\\0",
					"capcode": "AUA320S3Q4SPTA4",
					"engine": "2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "35020.00"
				}]
			}]
		}, {
			"name": "S3 Cabriolet",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3_Cabriolet/low_res/S3_Cabriolet_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3_Cabriolet/low_res/S3_Cabriolet.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A3/small_S3_Cabriolet.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "39205.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3_Cabriolet/low_res/S3_Cabriolet_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A3/S3_Cabriolet/low_res/S3_Cabriolet_SE.png",
				"engines": [{
					"modelCode": "8V7S3L\\0",
					"capcode": "AUA320S3Q2BPTA4 1",
					"engine": "2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "39205.00"
				}]
			}]
		}]
	}, {
		"rangeName": "A4",
		"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_front.png",
		"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4.png",
		"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Range/small_A4.png",
		"models": [{
			"name": "A4 Saloon",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Saloon/low_res/A4_Saloon_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Saloon/low_res/A4_Saloon.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A4/small_A4_Saloon.png",
			"trimlines": [{
				"name": "SE Technik",
				"otrPriceMin": "25685.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Saloon/low_res/A4_Saloon_SE_Technik_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Saloon/low_res/A4_Saloon_SE_Technik.png",
				"engines": [{
					"modelCode": "8K201C\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA418STK4SPTM  4",
					"engine": "SE Technik 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "25685.00"
				}, {
					"modelCode": "8K2ETC\\0\\GPRGPRG\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420STK4SDTM  4",
					"engine": "SE Technik 2.0 TDIe",
					"displayEngineName": "2.0 TDIe",
					"gearType": "Manual",
					"otrPriceMin": "28600.00"
				}, {
					"modelCode": "8K203H\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA418ST04SPTA  4",
					"engine": "SE Technik 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "28780.00"
				}, {
					"modelCode": "8K20WC\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420ST74SDTM  4",
					"engine": "SE Technik 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "29620.00"
				}, {
					"modelCode": "8K2EZC\\0\\GPRGPRG\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420UST4SDTM  4",
					"engine": "SE Technik 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "29620.00"
				}, {
					"modelCode": "8K20SH\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA4200ST4SDTA  4",
					"engine": "SE Technik 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "30155.00"
				}, {
					"modelCode": "8K20W9\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420ST74SDTM4 4",
					"engine": "SE Technik 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "31180.00"
				}, {
					"modelCode": "8K20WY\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420ST74SDTA4 4",
					"engine": "SE Technik 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "32660.00"
				}, {
					"modelCode": "8K20AY\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA4205ST4SPTA4 4",
					"engine": "SE Technik 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "32945.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "27240.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Saloon/low_res/A4_Saloon_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Saloon/low_res/A4_Saloon_S_line.png",
				"engines": [{
					"modelCode": "8K201C\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA418SL 4SPTM  4",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "27240.00"
				}, {
					"modelCode": "8K20SC\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA4201SL4SDTM  4",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "30230.00"
				}, {
					"modelCode": "8K203H\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA418SL74SPTA  4",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "30335.00"
				}, {
					"modelCode": "8K20SH\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA4201SL4SDTA  4",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "31710.00"
				}, {
					"modelCode": "8K20W9\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420SN74SDTM4 4",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "32735.00"
				}, {
					"modelCode": "8K20WY\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420SN74SDTA4 4",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "34215.00"
				}, {
					"modelCode": "8K20AY\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420SL24SPTA4 4",
					"engine": "S line 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "34500.00"
				}, {
					"modelCode": "8K20JY\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA430SL 4SDTA4 4",
					"engine": "S line 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "38215.00"
				}]
			}, {
				"name": "SE",
				"otrPriceMin": "27600.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Saloon/low_res/A4_Saloon_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Saloon/low_res/A4_Saloon_SE.png",
				"engines": [{
					"modelCode": "8K2ETC\\0\\GPRGPRG\\GWBIWBI\\MLRA2ZQ\\MEPH7X1\\MGRA8T1\\MMFA9Q8\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420SE 4SDTM  4",
					"engine": "SE 2.0 TDIe",
					"displayEngineName": "2.0 TDIe",
					"gearType": "Manual",
					"otrPriceMin": "27600.00"
				}]
			}, {
				"name": "Black Edition Plus",
				"otrPriceMin": "28740.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Saloon/low_res/A4_Saloon_Black_Edition_Plus_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Saloon/low_res/A4_Saloon_Black_Edition_Plus.png",
				"engines": [{
					"modelCode": "8K201C\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "28740.00"
				}, {
					"modelCode": "8K20SC\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "31730.00"
				}, {
					"modelCode": "8K203H\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "31835.00"
				}, {
					"modelCode": "8K20SH\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "33210.00"
				}, {
					"modelCode": "8K20W9\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "34235.00"
				}, {
					"modelCode": "8K20WY\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "35715.00"
				}, {
					"modelCode": "8K20AY\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "36000.00"
				}, {
					"modelCode": "8K20JY\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "39715.00"
				}]
			}]
		}, {
			"name": "A4 Avant",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Avant/low_res/A4_Avant_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Avant/low_res/A4_Avant.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A4/small_A4_Avant.png",
			"trimlines": [{
				"name": "SE Technik",
				"otrPriceMin": "26985.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Avant/low_res/A4_Avant_SE_Technik_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Avant/low_res/A4_Avant_SE_Technik.png",
				"engines": [{
					"modelCode": "8K501C\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA418STK5EPTM  4",
					"engine": "SE Technik 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "26985.00"
				}, {
					"modelCode": "8K5ETC\\0\\GPRGPRG\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420STK5EDTM  4",
					"engine": "SE Technik 2.0 TDIe",
					"displayEngineName": "2.0 TDIe",
					"gearType": "Manual",
					"otrPriceMin": "29900.00"
				}, {
					"modelCode": "8K503H\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA418ST05EPTA  4",
					"engine": "SE Technik 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "30080.00"
				}, {
					"modelCode": "8K50WC\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420ST75EDTM  4",
					"engine": "SE Technik 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "30920.00"
				}, {
					"modelCode": "8K5EZC\\0\\GPRGPRG\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420UST5EDTM  4",
					"engine": "SE Technik 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "30920.00"
				}, {
					"modelCode": "8K50SH\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA4200ST5EDTA  4",
					"engine": "SE Technik 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "31455.00"
				}, {
					"modelCode": "8K50W9\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420ST75EDTM4 4",
					"engine": "SE Technik 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "32480.00"
				}, {
					"modelCode": "8K50WY\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420ST75EDTA4 4",
					"engine": "SE Technik 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "33975.00"
				}, {
					"modelCode": "8K50AY\\0\\GPQ5PQ5\\GWBIWBI\\GWB7WB7\\GWF5WF5\\MLRA2ZQ\\MEPH7X4\\MGRA8T1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA4205ST5EPTA4 4",
					"engine": "SE Technik 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "34245.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "28540.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Avant/low_res/A4_Avant_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Avant/low_res/A4_Avant_S_line.png",
				"engines": [{
					"modelCode": "8K501C\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA418SL 5EPTM  4",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "28540.00"
				}, {
					"modelCode": "8K50SC\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA4201SL5EDTM  4",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "31530.00"
				}, {
					"modelCode": "8K503H\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA418SL75EPTA  4",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "31635.00"
				}, {
					"modelCode": "8K50SH\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA4201SL5EDTA  4",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "33010.00"
				}, {
					"modelCode": "8K50W9\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420SN75EDTM4 4",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "34035.00"
				}, {
					"modelCode": "8K50WY\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420SN75EDTA4 4",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "35530.00"
				}, {
					"modelCode": "8K50AY\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420SL25EPTA4 4",
					"engine": "S line 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "35800.00"
				}, {
					"modelCode": "8K50JY\\0\\MSIBN7V\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MZIE4ZB\\MEPH7X1\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA430SL 5EDTA4 4",
					"engine": "S line 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "39550.00"
				}]
			}, {
				"name": "SE",
				"otrPriceMin": "28900.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Avant/low_res/A4_Avant_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Avant/low_res/A4_Avant_SE.png",
				"engines": [{
					"modelCode": "8K5ETC\\0\\GPRGPRG\\GWBIWBI\\MLRA2ZQ\\MEPH7X1\\MGRA8T1\\MMFA9Q8\\MLSE9VD\\MVTV9ZX",
					"capcode": "AUA420SE 5EDTM  4",
					"engine": "SE 2.0 TDIe",
					"displayEngineName": "2.0 TDIe",
					"gearType": "Manual",
					"otrPriceMin": "28900.00"
				}]
			}, {
				"name": "Black Edition Plus",
				"otrPriceMin": "30040.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Avant/low_res/A4_Avant_Black_Edition_Plus_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_Avant/low_res/A4_Avant_Black_Edition_Plus.png",
				"engines": [{
					"modelCode": "8K501C\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "30040.00"
				}, {
					"modelCode": "8K50SC\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "33030.00"
				}, {
					"modelCode": "8K503H\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "33135.00"
				}, {
					"modelCode": "8K50SH\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "34510.00"
				}, {
					"modelCode": "8K50W9\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "35535.00"
				}, {
					"modelCode": "8K50WY\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "37030.00"
				}, {
					"modelCode": "8K50AY\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "37300.00"
				}, {
					"modelCode": "8K50JY\\0\\MSIBN7V\\GPB3PB3\\GPQ4PQ4\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MEIH5TL\\MBBO6FJ\\MEPH7X4\\MVTV9ZX",
					"engine": "Black Edition Plus 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "41050.00"
				}]
			}]
		}, {
			"name": "A4 allroad quattro",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_allroad_quattro/low_res/A4_allroad_quattro_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_allroad_quattro/low_res/A4_allroad_quattro.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A4/small_A4_allroad_quattro.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "32680.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_allroad_quattro/low_res/A4_allroad_quattro_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_allroad_quattro/low_res/A4_allroad_quattro_SE.png",
				"engines": [{
					"modelCode": "8KH0W9\\0",
					"capcode": "AU4A20   5EDTM4 1",
					"engine": "2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "32680.00"
				}, {
					"modelCode": "8KH0WY\\0",
					"capcode": "AU4A20   5EDTA4 1",
					"engine": "2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "34160.00"
				}, {
					"modelCode": "8KH0AY\\0",
					"capcode": "AU4A202255EPTA4 1",
					"engine": "2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "34395.00"
				}, {
					"modelCode": "8KH0JY\\0",
					"capcode": "AU4A30   5EDTA4 1",
					"engine": "3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "38145.00"
				}]
			}, {
				"name": "Sport",
				"otrPriceMin": "35130.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_allroad_quattro/low_res/A4_allroad_quattro_Sport_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/A4_allroad_quattro/low_res/A4_allroad_quattro_Sport.png",
				"engines": [{
					"modelCode": "8KH0W9\\0\\MSIBN7U\\GPQMPQM\\GWB2WB2\\MEIH5MU",
					"engine": "Sport 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "35130.00"
				}, {
					"modelCode": "8KH0WY\\0\\MSIBN7U\\GPQMPQM\\GWB2WB2\\MEIH5MU",
					"engine": "Sport 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "36610.00"
				}, {
					"modelCode": "8KH0AY\\0\\MSIBN7U\\GPQMPQM\\GWB2WB2\\MEIH5MU",
					"engine": "Sport 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "36845.00"
				}, {
					"modelCode": "8KH0JY\\0\\MSIBN7U\\GPQMPQM\\GWB2WB2\\MEIH5MU",
					"engine": "Sport 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "40595.00"
				}]
			}]
		}, {
			"name": "S4 Saloon",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/S4_Saloon/low_res/S4_Saloon_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/S4_Saloon/low_res/S4_Saloon.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A4/small_S4_Saloon.png",
			"trimlines": [{
				"name": "S4",
				"otrPriceMin": "39610.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/S4_Saloon/low_res/S4_Saloon_S4_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/S4_Saloon/low_res/S4_Saloon_S4.png",
				"engines": [{
					"modelCode": "8K2SCY\\0",
					"capcode": "AUA430S4 4SPSA4 4",
					"engine": "3.0 TFSI quattro",
					"displayEngineName": "3.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "39610.00"
				}]
			}, {
				"name": "S4 Black Edition",
				"otrPriceMin": "40685.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/S4_Saloon/low_res/S4_Saloon_S4_Black_Edition_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/S4_Saloon/low_res/S4_Saloon_S4_Black_Edition.png",
				"engines": [{
					"modelCode": "8K2SCY\\0\\GPB2PB2\\GPQUPQU\\MLRA2PK\\MEIH5TL\\MEPH7X4",
					"capcode": "AUA430S4B4SPSA4 4",
					"engine": "Black Edition 3.0 TFSI quattro",
					"displayEngineName": "Black Edition 3.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "40685.00"
				}]
			}]
		}, {
			"name": "S4 Avant",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/S4_Avant/low_res/S4_Avant_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/S4_Avant/low_res/S4_Avant.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A4/small_S4_Avant.png",
			"trimlines": [{
				"name": "S4",
				"otrPriceMin": "40910.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/S4_Avant/low_res/S4_Avant_S4_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/S4_Avant/low_res/S4_Avant_S4.png",
				"engines": [{
					"modelCode": "8K5SCY\\0",
					"capcode": "AUA430S4 5EPSA4 4",
					"engine": "3.0 TFSI quattro",
					"displayEngineName": "3.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "40910.00"
				}]
			}, {
				"name": "S4 Black Edition",
				"otrPriceMin": "41985.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/S4_Avant/low_res/S4_Avant_S4_Black_Edition_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/S4_Avant/low_res/S4_Avant_S4_Black_Edition.png",
				"engines": [{
					"modelCode": "8K5SCY\\0\\GPB2PB2\\GPQUPQU\\MSSHVW1\\MLRA2PK\\MDAR3S2\\MEIH5TL",
					"capcode": "AUA430S4B5EPSA4 4",
					"engine": "Black Edition 3.0 TFSI quattro",
					"displayEngineName": "Black Edition 3.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "41985.00"
				}]
			}]
		}, {
			"name": "RS 4 Avant",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/RS_4_Avant/low_res/RS_4_Avant_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/RS_4_Avant/low_res/RS_4_Avant.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A4/small_RS_4_Avant.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "56545.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/RS_4_Avant/low_res/RS_4_Avant_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A4/RS_4_Avant/low_res/RS_4_Avant_SE.png",
				"engines": [{
					"modelCode": "8K5RXY\\0",
					"capcode": "AURS42RS45EPIA4 3",
					"engine": "4.2 FSI quattro",
					"displayEngineName": "4.2 FSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "56545.00"
				}, {
					"modelCode": "8K5RXY\\0\\GPC2PC2\\GPQVPQV\\GPYIPYI\\MSNRS3G\\GWB0WB0\\GYRBYRB\\GYTUYTU",
					"engine": "Limited Edition 4.2 FSI quattro",
					"displayEngineName": "Limited Edition 4.2 FSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "65775.00"
				}]
			}]
		}]
	}, {
		"rangeName": "A5",
		"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_front.png",
		"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5.png",
		"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Range/small_A5.png",
		"models": [{
			"name": "A5 Coupé",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Coupe/low_res/A5_Coupe_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Coupe/low_res/A5_Coupe.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A5/small_A5_Coupe.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "29200.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Coupe/low_res/A5_Coupe_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Coupe/low_res/A5_Coupe_SE.png",
				"engines": [{
					"modelCode": "8T303C\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA518SE 2CPTM  1",
					"engine": "SE 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "29200.00"
				}, {
					"modelCode": "8T303H\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XX\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA518SE 2CPTA  1",
					"engine": "SE 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "30680.00"
				}, {
					"modelCode": "8T30WC\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S772CDTM  1",
					"engine": "SE 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "31470.00"
				}, {
					"modelCode": "8T3EZC\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520USE2CDTM  1",
					"engine": "SE 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "31470.00"
				}, {
					"modelCode": "8T30WH\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XX\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S772CDTA  1",
					"engine": "SE 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "32950.00"
				}, {
					"modelCode": "8T30W9\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S772CDTM4 1",
					"engine": "SE 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "33125.00"
				}, {
					"modelCode": "8T30A9\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S252CPTM4 1",
					"engine": "SE 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "33910.00"
				}, {
					"modelCode": "8T30WY\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XX\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S772CDTA4 1",
					"engine": "SE 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "34605.00"
				}, {
					"modelCode": "8T30AY\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XX\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S252CPTA4 1",
					"engine": "SE 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "35390.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "31790.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Coupe/low_res/A5_Coupe_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Coupe/low_res/A5_Coupe_S_line.png",
				"engines": [{
					"modelCode": "8T303C\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA518SLV2CPTM  1",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "31790.00"
				}, {
					"modelCode": "8T303H\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA518SLV2CPTA  1",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "33270.00"
				}, {
					"modelCode": "8T30WC\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA520S7N2CDTM  1",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "34060.00"
				}, {
					"modelCode": "8T30WH\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA520S7N2CDTA  1",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "35540.00"
				}, {
					"modelCode": "8T30W9\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA520S7N2CDTM4 1",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "35715.00"
				}, {
					"modelCode": "8T30A9\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA520SN22CPTM4 1",
					"engine": "S line 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "36500.00"
				}, {
					"modelCode": "8T30WY\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA520S7N2CDTA4 1",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "37195.00"
				}, {
					"modelCode": "8T30IH\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA530SLV2CDTA  1",
					"engine": "S line 3.0 TDI",
					"displayEngineName": "3.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "37660.00"
				}, {
					"modelCode": "8T30AY\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA520SN22CPTA4 1",
					"engine": "S line 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "37980.00"
				}, {
					"modelCode": "8T30JY\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA530SLV2CDTA4 1",
					"engine": "S line 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "41185.00"
				}]
			}, {
				"name": "Black Edition Plus",
				"otrPriceMin": "33290.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Coupe/low_res/A5_Coupe_Black_Edition_Plus_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Coupe/low_res/A5_Coupe_Black_Edition_Plus.png",
				"engines": [{
					"modelCode": "8T303C\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "33290.00"
				}, {
					"modelCode": "8T303H\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "34770.00"
				}, {
					"modelCode": "8T30WC\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "35560.00"
				}, {
					"modelCode": "8T30WH\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "37040.00"
				}, {
					"modelCode": "8T30W9\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "37215.00"
				}, {
					"modelCode": "8T30A9\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "38000.00"
				}, {
					"modelCode": "8T30WY\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "38695.00"
				}, {
					"modelCode": "8T30IH\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 3.0 TDI",
					"displayEngineName": "3.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "39160.00"
				}, {
					"modelCode": "8T30AY\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "39480.00"
				}, {
					"modelCode": "8T30JY\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "42685.00"
				}]
			}]
		}, {
			"name": "A5 Sportback",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Sportback/low_res/A5_Sportback_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Sportback/low_res/A5_Sportback.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A5/small_A5_Sportback.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "26780.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Sportback/low_res/A5_Sportback_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Sportback/low_res/A5_Sportback_SE.png",
				"engines": [{
					"modelCode": "8TA03C\\0",
					"capcode": "AUA5185  5HPTM  1",
					"engine": "Standard 1.8 TFSI",
					"displayEngineName": "Standard 1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "26780.00"
				}, {
					"modelCode": "8TA03H\\0",
					"capcode": "AUA5185  5HPTA  1",
					"engine": "Standard 1.8 TFSI",
					"displayEngineName": "Standard 1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "28260.00"
				}, {
					"modelCode": "8TAEYC\\0",
					"capcode": "AUA520ULT5HDTM  1",
					"engine": "Standard 2.0 TDI ultra",
					"displayEngineName": "Standard 2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "28545.00"
				}, {
					"modelCode": "8TA03C\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA518S5E5HPTM  1",
					"engine": "SE 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "28550.00"
				}, {
					"modelCode": "8TA0WC\\0",
					"capcode": "AUA52015S5HDTM  1",
					"engine": "Standard 2.0 TDI",
					"displayEngineName": "Standard 2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "29050.00"
				}, {
					"modelCode": "8TA03H\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XX\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA518S5E5HPTA  1",
					"engine": "SE 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "30030.00"
				}, {
					"modelCode": "8TA0SH\\0",
					"capcode": "AUA5201505HDTA  1",
					"engine": "Standard 2.0 TDI",
					"displayEngineName": "Standard 2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "30100.00"
				}, {
					"modelCode": "8TAEYC\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520USE5HDTM  1",
					"engine": "SE 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "30315.00"
				}, {
					"modelCode": "8TA0WC\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S755HDTM  1",
					"engine": "SE 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "30820.00"
				}, {
					"modelCode": "8TA0SH\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XX\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA5205SE5HDTA  1",
					"engine": "SE 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "31870.00"
				}, {
					"modelCode": "8TA0W9\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S5E5HDTM4 1",
					"engine": "SE 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "32475.00"
				}, {
					"modelCode": "8TA0A9\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S255HPTM4 1",
					"engine": "SE 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "33260.00"
				}, {
					"modelCode": "8TA0WY\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XX\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S5E5HDTA4 1",
					"engine": "SE 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "33955.00"
				}, {
					"modelCode": "8TA0AY\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XX\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S255HPTA4 1",
					"engine": "SE 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "34740.00"
				}]
			}, {
				"name": "SE Technik",
				"otrPriceMin": "29900.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Sportback/low_res/A5_Sportback_SE_Technik_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Sportback/low_res/A5_Sportback_SE_Technik.png",
				"engines": [{
					"modelCode": "8TA03C\\0\\MSIBN5W\\GPNQPNQ\\GPQ5PQ5\\MESSUF7\\GWB1WB1\\GYEEYEE\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA5185TK5HPTM  1",
					"engine": "SE Technik 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "29900.00"
				}, {
					"modelCode": "8TA03H\\0\\MSIBN5W\\GPNQPNQ\\GPQ5PQ5\\MESSUF7\\GWB1WB1\\GYEEYEE\\MLRA1XX\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA5185TK5HPTA  1",
					"engine": "SE Technik 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "31380.00"
				}, {
					"modelCode": "8TAEYC\\0\\MSIBN5W\\GPNQPNQ\\GPQ5PQ5\\MESSUF7\\GWB1WB1\\GYEEYEE\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520UST5HDTM  1",
					"engine": "SE Technik 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "31665.00"
				}, {
					"modelCode": "8TA0WC\\0\\MSIBN5W\\GPNQPNQ\\GPQ5PQ5\\MESSUF7\\GWB1WB1\\GYEEYEE\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA5207T55HDTM  1",
					"engine": "SE Technik 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "32170.00"
				}, {
					"modelCode": "8TA0SH\\0\\MSIBN5W\\GPNQPNQ\\GPQ5PQ5\\MESSUF7\\GWB1WB1\\GYEEYEE\\MLRA1XX\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA5200ST5HDTA  1",
					"engine": "SE Technik 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "33220.00"
				}, {
					"modelCode": "8TA0W9\\0\\MSIBN5W\\GPNQPNQ\\GPQ5PQ5\\MESSUF7\\GWB1WB1\\GYEEYEE\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA5207T55HDTM4 1",
					"engine": "SE Technik 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "33825.00"
				}, {
					"modelCode": "8TA0A9\\0\\MSIBN5W\\GPNQPNQ\\GPQ5PQ5\\MESSUF7\\GWB1WB1\\GYEEYEE\\MLRA1XW\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA5205ST5HPTM4 1",
					"engine": "SE Technik 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "34610.00"
				}, {
					"modelCode": "8TA0WY\\0\\MSIBN5W\\GPNQPNQ\\GPQ5PQ5\\MESSUF7\\GWB1WB1\\GYEEYEE\\MLRA1XX\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA5207T55HDTA4 1",
					"engine": "SE Technik 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "35305.00"
				}, {
					"modelCode": "8TA0AY\\0\\MSIBN5W\\GPNQPNQ\\GPQ5PQ5\\MESSUF7\\GWB1WB1\\GYEEYEE\\MLRA1XX\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA5205ST5HPTA4 1",
					"engine": "SE Technik 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "36090.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "31540.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Sportback/low_res/A5_Sportback_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Sportback/low_res/A5_Sportback_S_line.png",
				"engines": [{
					"modelCode": "8TA03C\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA518S5N5HPTM  1",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "31540.00"
				}, {
					"modelCode": "8TA03H\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA518S5N5HPTA  1",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "33020.00"
				}, {
					"modelCode": "8TAEYC\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA520USL5HDTM  1",
					"engine": "S line 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "33305.00"
				}, {
					"modelCode": "8TA0WC\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA5207S55HDTM  1",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "33810.00"
				}, {
					"modelCode": "8TA0SH\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA5201SN5HDTA  1",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "34860.00"
				}, {
					"modelCode": "8TA0W9\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA5207S55HDTM4 1",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "35465.00"
				}, {
					"modelCode": "8TA0A9\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA520SN25HPTM4 1",
					"engine": "S line 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "36250.00"
				}, {
					"modelCode": "8TA0WY\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA5207S55HDTA4 1",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "36945.00"
				}, {
					"modelCode": "8TA0IH\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA530S5N5HDTA  1",
					"engine": "S line 3.0 TDI",
					"displayEngineName": "3.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "37410.00"
				}, {
					"modelCode": "8TA0AY\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA520SN25HPTA4 1",
					"engine": "S line 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "37730.00"
				}, {
					"modelCode": "8TA0JY\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA530S5N5HDTA4 1",
					"engine": "S line 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "40935.00"
				}]
			}, {
				"name": "Black Edition Plus",
				"otrPriceMin": "33040.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Sportback/low_res/A5_Sportback_Black_Edition_Plus_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Sportback/low_res/A5_Sportback_Black_Edition_Plus.png",
				"engines": [{
					"modelCode": "8TA03C\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "33040.00"
				}, {
					"modelCode": "8TA03H\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "34520.00"
				}, {
					"modelCode": "8TA0WC\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "35310.00"
				}, {
					"modelCode": "8TA0SH\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "36360.00"
				}, {
					"modelCode": "8TA0W9\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "36965.00"
				}, {
					"modelCode": "8TA0A9\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PF\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "37750.00"
				}, {
					"modelCode": "8TA0WY\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "38445.00"
				}, {
					"modelCode": "8TA0IH\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 3.0 TDI",
					"displayEngineName": "3.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "38910.00"
				}, {
					"modelCode": "8TA0AY\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "39230.00"
				}, {
					"modelCode": "8TA0JY\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA2PK\\MSIH4A3\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X4",
					"engine": "Black Edition Plus 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "42435.00"
				}]
			}]
		}, {
			"name": "A5 Cabriolet",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Cabriolet/low_res/A5_Cabriolet_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Cabriolet/low_res/A5_Cabriolet.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A5/small_A5_Cabriolet.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "32320.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Cabriolet/low_res/A5_Cabriolet_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Cabriolet/low_res/A5_Cabriolet_SE.png",
				"engines": [{
					"modelCode": "8F703C\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XW\\MHIS3KA\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA518SE 2VPTM  1",
					"engine": "SE 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "32320.00"
				}, {
					"modelCode": "8F703H\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XX\\MHIS3KA\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA518SE 2VPTA  1",
					"engine": "SE 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "33800.00"
				}, {
					"modelCode": "8F70SC\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XW\\MHIS3KA\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA5205SE2VDTM  1",
					"engine": "SE 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "34145.00"
				}, {
					"modelCode": "8F70AC\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XW\\MHIS3KA\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S252VPTM  1",
					"engine": "SE 2.0 TFSI",
					"displayEngineName": "2.0 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "35455.00"
				}, {
					"modelCode": "8F70WH\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XX\\MHIS3KA\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S772VDTA  1",
					"engine": "SE 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "36185.00"
				}, {
					"modelCode": "8F70AH\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XX\\MHIS3KA\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S252VPTA  1",
					"engine": "SE 2.0 TFSI",
					"displayEngineName": "2.0 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "36935.00"
				}, {
					"modelCode": "8F70AY\\0\\MRADCD7\\MSIBN5W\\GWB1WB1\\MLRA1XX\\MHIS3KA\\MSIH4A3\\MEPH7X4\\MLSE9VD",
					"capcode": "AUA520S252VPTA4 1",
					"engine": "SE 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "38495.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "35570.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Cabriolet/low_res/A5_Cabriolet_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Cabriolet/low_res/A5_Cabriolet_S_line.png",
				"engines": [{
					"modelCode": "8F703C\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA518SLV2VPTM  1",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "35570.00"
				}, {
					"modelCode": "8F703H\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA518SLV2VPTA  1",
					"engine": "S line 1.8 TFSI",
					"displayEngineName": "1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "37050.00"
				}, {
					"modelCode": "8F70SC\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA5201SN2VDTM  1",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "37395.00"
				}, {
					"modelCode": "8F70AC\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA520SN22VPTM  1",
					"engine": "S line 2.0 TFSI",
					"displayEngineName": "2.0 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "38705.00"
				}, {
					"modelCode": "8F70WH\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA520S7N2VDTA  1",
					"engine": "S line 2.0 TDI",
					"displayEngineName": "2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "39435.00"
				}, {
					"modelCode": "8F70AH\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA520SN22VPTA  1",
					"engine": "S line 2.0 TFSI",
					"displayEngineName": "2.0 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "40185.00"
				}, {
					"modelCode": "8F70IH\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA530SLV2VDTA  1",
					"engine": "S line 3.0 TDI",
					"displayEngineName": "3.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "41555.00"
				}, {
					"modelCode": "8F70AY\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA520SN22VPTA4 1",
					"engine": "S line 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "41745.00"
				}, {
					"modelCode": "8F70JY\\0\\MSIBN2R\\GPNQPNQ\\GPQGPQG\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MSIH4A3\\MLUM7HB\\MEPH7X4\\MMFA9Q8\\MLSE9VD",
					"capcode": "AUA530SLV2VDTA4 1",
					"engine": "S line 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "44985.00"
				}]
			}, {
				"name": "S line Special Edition Plus",
				"otrPriceMin": "37070.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Cabriolet/low_res/A5_Cabriolet_S_line_Special_Edition_Plus_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/A5_Cabriolet/low_res/A5_Cabriolet_S_line_Special_Edition_Plus.png",
				"engines": [{
					"modelCode": "8F703C\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X8\\MMFA9Q8",
					"engine": "Special Edition Plus 1.8 TFSI",
					"displayEngineName": "Special Edition Plus 1.8 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "37070.00"
				}, {
					"modelCode": "8F703H\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X8\\MMFA9Q8",
					"engine": "Special Edition Plus 1.8 TFSI",
					"displayEngineName": "Special Edition Plus 1.8 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "38550.00"
				}, {
					"modelCode": "8F70SC\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X8\\MMFA9Q8",
					"engine": "Special Edition Plus 2.0 TDI",
					"displayEngineName": "Special Edition Plus 2.0 TDI",
					"gearType": "Manual",
					"otrPriceMin": "38895.00"
				}, {
					"modelCode": "8F70AC\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X8\\MMFA9Q8",
					"engine": "Special Edition Plus 2.0 TFSI",
					"displayEngineName": "Special Edition Plus 2.0 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "40060.00"
				}, {
					"modelCode": "8F70WH\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X8\\MMFA9Q8",
					"engine": "Special Edition Plus 2.0 TDI",
					"displayEngineName": "Special Edition Plus 2.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "40935.00"
				}, {
					"modelCode": "8F70AH\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X8\\MMFA9Q8",
					"engine": "Special Edition Plus 2.0 TFSI",
					"displayEngineName": "Special Edition Plus 2.0 TFSI",
					"gearType": "multitronic",
					"otrPriceMin": "41685.00"
				}, {
					"modelCode": "8F70IH\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X8\\MMFA9Q8",
					"engine": "Special Edition Plus 3.0 TDI",
					"displayEngineName": "Special Edition Plus 3.0 TDI",
					"gearType": "multitronic",
					"otrPriceMin": "43055.00"
				}, {
					"modelCode": "8F70AY\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X8\\MMFA9Q8",
					"engine": "Special Edition Plus 2.0 TFSI quattro",
					"displayEngineName": "Special Edition Plus 2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "43245.00"
				}, {
					"modelCode": "8F70JY\\0\\MSIBN2R\\GPB3PB3\\GPNQPNQ\\GPQ9PQ9\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIH5TL\\MBBO6FJ\\MLUM7HB\\MEPH7X8\\MMFA9Q8",
					"engine": "Special Edition Plus 3.0 TDI quattro",
					"displayEngineName": "Special Edition Plus 3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "46485.00"
				}]
			}]
		}, {
			"name": "S5 Coupé",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Coupe/low_res/S5_Coupe_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Coupe/low_res/S5_Coupe.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A5/small_S5_Coupe.png",
			"trimlines": [{
				"name": "S5",
				"otrPriceMin": "43665.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Coupe/low_res/S5_Coupe_S5_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Coupe/low_res/S5_Coupe_S5.png",
				"engines": [{
					"modelCode": "8T3SCY\\1",
					"capcode": "AUA530S5 2CPSA4 1",
					"engine": "3.0 TFSI quattro",
					"displayEngineName": "3.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "43665.00"
				}]
			}, {
				"name": "S5 Black Edition",
				"otrPriceMin": "44740.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Coupe/low_res/S5_Coupe_S5_Black_Edition_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Coupe/low_res/S5_Coupe_S5_Black_Edition.png",
				"engines": [{
					"modelCode": "8T3SCY\\1\\GPB2PB2\\GPQTPQT\\MLRA2PK",
					"capcode": "AUA530S5B2CPSA4 1",
					"engine": "Black Edition 3.0 TFSI quattro",
					"displayEngineName": "Black Edition 3.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "44740.00"
				}]
			}]
		}, {
			"name": "S5 Sportback",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Sportback/low_res/S5_Sportback_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Sportback/low_res/S5_Sportback.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A5/small_S5_Sportback.png",
			"trimlines": [{
				"name": "S5",
				"otrPriceMin": "42865.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Sportback/low_res/S5_Sportback_S5_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Sportback/low_res/S5_Sportback_S5.png",
				"engines": [{
					"modelCode": "8TASCY\\1",
					"capcode": "AUA530S5 5HPSA4 1",
					"engine": "3.0 TFSI quattro",
					"displayEngineName": "3.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "42865.00"
				}]
			}, {
				"name": "S5 Black Edition",
				"otrPriceMin": "43940.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Sportback/low_res/S5_Sportback_S5_Black_Edition_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Sportback/low_res/S5_Sportback_S5_Black_Edition.png",
				"engines": [{
					"modelCode": "8TASCY\\1\\GPB2PB2\\GPQTPQT\\MLRA2PK",
					"capcode": "AUA530S5B5HPSA4 1",
					"engine": "Black Edition 3.0 TFSI quattro",
					"displayEngineName": "Black Edition 3.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "43940.00"
				}]
			}]
		}, {
			"name": "S5 Cabriolet",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Cabriolet/low_res/S5_Cabriolet_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Cabriolet/low_res/S5_Cabriolet.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A5/small_S5_Cabriolet.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "46770.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Cabriolet/low_res/S5_Cabriolet_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/S5_Cabriolet/low_res/S5_Cabriolet_SE.png",
				"engines": [{
					"modelCode": "8F7SCY\\2",
					"capcode": "AUA530S5 2VPSA4 1",
					"engine": "3.0 TFSI quattro",
					"displayEngineName": "3.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "46770.00"
				}]
			}]
		}, {
			"name": "RS 5 Coupé",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/RS_5_Coupe/low_res/RS_5_Coupe_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/RS_5_Coupe/low_res/RS_5_Coupe.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A5/small_RS_5_Coupe.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "59870.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/RS_5_Coupe/low_res/RS_5_Coupe_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/RS_5_Coupe/low_res/RS_5_Coupe_SE.png",
				"engines": [{
					"modelCode": "8T3RXY\\1",
					"capcode": "AURS42RS52CPIA4 1",
					"engine": "4.2 FSI quattro",
					"displayEngineName": "4.2 FSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "59870.00"
				}, {
					"modelCode": "8T3RXY\\1\\GPC2PC2\\GPQVPQV\\GPYIPYI\\MSNRS3G\\GWB0WB0\\GYRBYRB\\GYTUYTU",
					"engine": "Limited Edition 4.2 FSI quattro",
					"displayEngineName": "Limited Edition 4.2 FSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "69100.00"
				}]
			}]
		}, {
			"name": "RS 5 Cabriolet",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/RS_5_Cabriolet/low_res/RS_5_Cabriolet_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/RS_5_Cabriolet/low_res/RS_5_Cabriolet.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A5/small_RS_5_Cabriolet.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "69505.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/RS_5_Cabriolet/low_res/RS_5_Cabriolet_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A5/RS_5_Cabriolet/low_res/RS_5_Cabriolet_SE.png",
				"engines": [{
					"modelCode": "8F7RXY\\1",
					"capcode": "AURS42RS52BPIA4",
					"engine": "4.2 FSI quattro",
					"displayEngineName": "4.2 FSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "69505.00"
				}, {
					"modelCode": "8F7RXY\\1\\MSIBN1T\\GPAPPAP\\GPC2PC2\\GPQVPQV\\GPS5PS5\\GPYIPYI\\MSNRS3G\\GWB0WB0\\GYRBYRB\\GYTUYTU",
					"engine": "Limited Edition 4.2 FSI quattro",
					"displayEngineName": "Limited Edition 4.2 FSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "78735.00"
				}]
			}]
		}]
	}, {
		"rangeName": "A6",
		"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_front.png",
		"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6.png",
		"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Range/small_A6.png",
		"models": [{
			"name": "A6 Saloon",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Saloon/low_res/A6_Saloon_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Saloon/low_res/A6_Saloon.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A6/small_A6_Saloon.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "31955.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Saloon/low_res/A6_Saloon_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Saloon/low_res/A6_Saloon_SE.png",
				"engines": [{
					"modelCode": "4GC0IC\\1",
					"capcode": "AUA620USE4SDTM  5",
					"engine": "SE 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "31955.00"
				}, {
					"modelCode": "4GC0IG\\1",
					"capcode": "AUA620USE4SDTA  5",
					"engine": "SE 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "S tronic",
					"otrPriceMin": "33485.00"
				}, {
					"modelCode": "4GC0LG\\0",
					"capcode": "AUA630SE 4SDTA  5",
					"engine": "SE 3.0 TDI",
					"displayEngineName": "3.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "38095.00"
				}, {
					"modelCode": "4GC0TY\\0",
					"capcode": "AUA630SE 4SDTA4 5",
					"engine": "SE 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "39855.00"
				}, {
					"modelCode": "4GC0VA\\0",
					"capcode": "AUA630E324SDTA4 5",
					"engine": "SE 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "46125.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "34405.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Saloon/low_res/A6_Saloon_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Saloon/low_res/A6_Saloon_S_line.png",
				"engines": [{
					"modelCode": "4GC0IC\\1\\MSIBN1V\\GPQDPQD\\GPQ1PQ1\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MEIL7M8",
					"capcode": "AUA620USL4SDTM  5",
					"engine": "S line 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "34405.00"
				}, {
					"modelCode": "4GC0IG\\1\\MSIBN1V\\GPQDPQD\\GPQ1PQ1\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA620USL4SDTA  5",
					"engine": "S line 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "S tronic",
					"otrPriceMin": "35935.00"
				}, {
					"modelCode": "4GC0LG\\0\\MSIBN1V\\GPQDPQD\\GPQ1PQ1\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA630SL 4SDTA  5",
					"engine": "S line 3.0 TDI",
					"displayEngineName": "3.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "40545.00"
				}, {
					"modelCode": "4GC0TY\\0\\MSIBN1V\\GPQDPQD\\GPQ1PQ1\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA630SL 4SDTA4 5",
					"engine": "S line 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "42305.00"
				}, {
					"modelCode": "4GC0VA\\0\\MSIBN1V\\GPQDPQD\\GPQ1PQ1\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA630S324SDTA4 5",
					"engine": "S line 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "48575.00"
				}]
			}, {
				"name": "Black Edition",
				"otrPriceMin": "36580.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Saloon/low_res/A6_Saloon_Black_Edition_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Saloon/low_res/A6_Saloon_Black_Edition.png",
				"engines": [{
					"modelCode": "4GC0IC\\1\\MSIBN1V\\GPB2PB2\\GPQDPQD\\GPQUPQU\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MEIL7M8",
					"capcode": "AUA620UBE4SDTM  5",
					"engine": "Black Edition 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "36580.00"
				}, {
					"modelCode": "4GC0IG\\1\\MSIBN1V\\GPB2PB2\\GPQDPQD\\GPQUPQU\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA620UBE4SDTA  5",
					"engine": "Black Edition 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "S tronic",
					"otrPriceMin": "38110.00"
				}, {
					"modelCode": "4GC0LG\\0\\MSIBN1V\\GPB2PB2\\GPQDPQD\\GPQUPQU\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA630BLK4SDTA  5",
					"engine": "Black Edition 3.0 TDI",
					"displayEngineName": "3.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "42720.00"
				}, {
					"modelCode": "4GC0TY\\0\\MSIBN1V\\GPB2PB2\\GPQDPQD\\GPQUPQU\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA630BLK4SDTA4 5",
					"engine": "Black Edition 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "44480.00"
				}, {
					"modelCode": "4GC0VA\\0\\MSIBN1V\\GPB2PB2\\GPQDPQD\\GPQUPQU\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA630B324SDTA4 5",
					"engine": "Black Edition 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "50750.00"
				}]
			}]
		}, {
			"name": "A6 Avant",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Avant/low_res/A6_Avant_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Avant/low_res/A6_Avant.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A6/small_A6_Avant.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "33955.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Avant/low_res/A6_Avant_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Avant/low_res/A6_Avant_SE.png",
				"engines": [{
					"modelCode": "4GD0IC\\1",
					"capcode": "AUA620USE5EDTM  5",
					"engine": "SE 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "33955.00"
				}, {
					"modelCode": "4GD0IG\\1",
					"capcode": "AUA620USE5EDTA  5",
					"engine": "SE 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "S tronic",
					"otrPriceMin": "35485.00"
				}, {
					"modelCode": "4GD0LG\\0",
					"capcode": "AUA630SE 5EDTA  5",
					"engine": "SE 3.0 TDI",
					"displayEngineName": "3.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "40095.00"
				}, {
					"modelCode": "4GD0TY\\0",
					"capcode": "AUA630SE 5EDTA4 5",
					"engine": "SE 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "41855.00"
				}, {
					"modelCode": "4GD0VA\\0",
					"capcode": "AUA630E325EDTA4 5",
					"engine": "SE 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "48125.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "36405.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Avant/low_res/A6_Avant_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Avant/low_res/A6_Avant_S_line.png",
				"engines": [{
					"modelCode": "4GD0IC\\1\\MSIBN1V\\GPQDPQD\\GPQ1PQ1\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MEIL7M8",
					"capcode": "AUA620USL5EDTM  5",
					"engine": "S line 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "36405.00"
				}, {
					"modelCode": "4GD0IG\\1\\MSIBN1V\\GPQDPQD\\GPQ1PQ1\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA620USL5EDTA  5",
					"engine": "S line 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "S tronic",
					"otrPriceMin": "37935.00"
				}, {
					"modelCode": "4GD0LG\\0\\MSIBN1V\\GPQDPQD\\GPQ1PQ1\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA630SL 5EDTA  5",
					"engine": "S line 3.0 TDI",
					"displayEngineName": "3.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "42545.00"
				}, {
					"modelCode": "4GD0TY\\0\\MSIBN1V\\GPQDPQD\\GPQ1PQ1\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA630SL 5EDTA4 5",
					"engine": "S line 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "44305.00"
				}, {
					"modelCode": "4GD0VA\\0\\MSIBN1V\\GPQDPQD\\GPQ1PQ1\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA630S325EDTA4 5",
					"engine": "S line 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "50575.00"
				}]
			}, {
				"name": "Black Edition",
				"otrPriceMin": "38580.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Avant/low_res/A6_Avant_Black_Edition_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_Avant/low_res/A6_Avant_Black_Edition.png",
				"engines": [{
					"modelCode": "4GD0IC\\1\\MSIBN1V\\GPB2PB2\\GPQDPQD\\GPQUPQU\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XW\\MEIL7M8",
					"capcode": "AUA620UBE5EDTM  5",
					"engine": "Black Edition 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "38580.00"
				}, {
					"modelCode": "4GD0IG\\1\\MSIBN1V\\GPB2PB2\\GPQDPQD\\GPQUPQU\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA620UBE5EDTA  5",
					"engine": "Black Edition 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "S tronic",
					"otrPriceMin": "40110.00"
				}, {
					"modelCode": "4GD0LG\\0\\MSIBN1V\\GPB2PB2\\GPQDPQD\\GPQUPQU\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA630BLK5EDTA  5",
					"engine": "Black Edition 3.0 TDI",
					"displayEngineName": "3.0 TDI",
					"gearType": "S tronic",
					"otrPriceMin": "44720.00"
				}, {
					"modelCode": "4GD0TY\\0\\MSIBN1V\\GPB2PB2\\GPQDPQD\\GPQUPQU\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA630BLK5EDTA4 5",
					"engine": "Black Edition 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "46495.00"
				}, {
					"modelCode": "4GD0VA\\0\\MSIBN1V\\GPB2PB2\\GPQDPQD\\GPQUPQU\\GPX2PX2\\GWQRWQR\\MDAE1BE\\MLRA1XX\\MEIL7M8",
					"capcode": "AUA630B325EDTA4 5",
					"engine": "Black Edition 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "52860.00"
				}]
			}]
		}, {
			"name": "A6 allroad quattro",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_allroad_quattro/low_res/A6_allroad_quattro_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_allroad_quattro/low_res/A6_allroad_quattro.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A6/small_A6_allroad_quattro.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "45255.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_allroad_quattro/low_res/A6_allroad_quattro_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/A6_allroad_quattro/low_res/A6_allroad_quattro_SE.png",
				"engines": [{
					"modelCode": "4GJ0TY\\0\\GWBIWBI",
					"capcode": "AUAL302185EDTA4 4",
					"engine": "3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "45255.00"
				}, {
					"modelCode": "4GJ0VA\\0\\GWBIWBI",
					"capcode": "AUAL30   5EDTA4 4",
					"engine": "3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "51620.00"
				}]
			}]
		}, {
			"name": "RS 6 Avant",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/RS_6_Avant/low_res/RS_6_Avant_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/RS_6_Avant/low_res/RS_6_Avant.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A6/small_RS_6_Avant.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "77995.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/RS_6_Avant/low_res/RS_6_Avant_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A6/RS_6_Avant/low_res/RS_6_Avant_SE.png",
				"engines": [{
					"modelCode": "4GDRAA\\0",
					"engine": "4.0 TFSI quattro",
					"displayEngineName": "4.0 TFSI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "77995.00"
				}]
			}]
		}]
	}, {
		"rangeName": "A7",
		"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/A7_front.png",
		"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/A7.png",
		"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Range/small_A7.png",
		"models": [{
			"name": "A7 Sportback",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/A7_Sportback/low_res/A7_Sportback_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/A7_Sportback/low_res/A7_Sportback.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A7/small_A7_Sportback.png",
			"trimlines": [{
				"name": "SE Executive",
				"otrPriceMin": "45875.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/A7_Sportback/low_res/A7_Sportback_SE_Executive_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/A7_Sportback/low_res/A7_Sportback_SE_Executive.png",
				"engines": [{
					"modelCode": "4GF0LG\\1",
					"capcode": "AUA730SEX5HDTA  1",
					"engine": "SE Executive 3.0 TDI ultra",
					"displayEngineName": "3.0 TDI ultra",
					"gearType": "S tronic",
					"otrPriceMin": "45875.00"
				}, {
					"modelCode": "4GF0TY\\1",
					"capcode": "AUA730SEX5HDTA4 1",
					"engine": "SE Executive 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "47630.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "48665.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/A7_Sportback/low_res/A7_Sportback_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/A7_Sportback/low_res/A7_Sportback_S_line.png",
				"engines": [{
					"modelCode": "4GF0LG\\1\\MSIBN1V\\GWQRWQR\\MDAE1BE\\MEIL7M8",
					"capcode": "AUA730SL 5HDTA  1",
					"engine": "S line 3.0 TDI ultra",
					"displayEngineName": "3.0 TDI ultra",
					"gearType": "S tronic",
					"otrPriceMin": "48665.00"
				}, {
					"modelCode": "4GF0TY\\1\\MSIBN1V\\GWQRWQR\\MDAE1BE\\MEIL7M8",
					"capcode": "AUA730SL 5HDTA4 1",
					"engine": "S line 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "50425.00"
				}, {
					"modelCode": "4GF02Y\\1\\MSIBN1V\\GWQRWQR\\MDAE1BE\\MEIL7M8",
					"capcode": "AUA730SL 5HPSA4 1",
					"engine": "S line 3.0 TFSI quattro",
					"displayEngineName": "3.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "53000.00"
				}, {
					"modelCode": "4GF0VA\\1\\MSIBN1V\\GWQRWQR\\MDAE1BE\\MEIL7M8",
					"capcode": "AUA730S325HDTA4 1",
					"engine": "S line 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "56575.00"
				}]
			}, {
				"name": "Black Edition",
				"otrPriceMin": "52775.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/A7_Sportback/low_res/A7_Sportback_Black_Edition_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/A7_Sportback/low_res/A7_Sportback_Black_Edition.png",
				"engines": [{
					"modelCode": "4GF0TY\\1\\MSIBN1V\\GPQ3PQ3\\GWC3WC3\\GWQRWQR\\MDAE1BV\\MEIH5TL\\MEIL7M8",
					"capcode": "AUA730BLK5HDTA4 1",
					"engine": "Black Edition 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "52775.00"
				}, {
					"modelCode": "4GF02Y\\1\\MSIBN1V\\GPQ3PQ3\\GWC3WC3\\GWQRWQR\\MDAE1BV\\MEIH5TL\\MEIL7M8",
					"capcode": "AUA730BLK5HPSA4 1",
					"engine": "Black Edition 3.0 TFSI quattro",
					"displayEngineName": "3.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "55350.00"
				}, {
					"modelCode": "4GF0VA\\1\\MSIBN1V\\GPQ3PQ3\\GWC3WC3\\GWQRWQR\\MDAE1BV\\MEIH5TL\\MEIL7M8",
					"capcode": "AUA730B325HDTA4 1",
					"engine": "Black Edition 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "58925.00"
				}]
			}]
		}, {
			"name": "S7 Sportback",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/S7_Sportback/low_res/S7_Sportback_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/S7_Sportback/low_res/S7_Sportback.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A7/small_S7_Sportback.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "63830.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/S7_Sportback/low_res/S7_Sportback_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/S7_Sportback/low_res/S7_Sportback_SE.png",
				"engines": [{
					"modelCode": "4GFSFY\\1",
					"capcode": "AUA740S7 5HPTA4 1",
					"engine": "4.0 TFSI quattro",
					"displayEngineName": "4.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "63830.00"
				}]
			}]
		}, {
			"name": "RS 7 Sportback",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/RS_7_Sportback/low_res/RS_7_Sportback_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/RS_7_Sportback/low_res/RS_7_Sportback.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A7/small_RS_7_Sportback.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "84480.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/RS_7_Sportback/low_res/RS_7_Sportback_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A7/RS_7_Sportback/low_res/RS_7_Sportback_SE.png",
				"engines": [{
					"modelCode": "4GFRAA\\1",
					"capcode": "AURS40RS75HPTA4 1",
					"engine": "4.0 TFSI quattro",
					"displayEngineName": "4.0 TFSI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "84480.00"
				}]
			}]
		}]
	}, {
		"rangeName": "A8",
		"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8_front.png",
		"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8.png",
		"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Range/small_A8.png",
		"models": [{
			"name": "A8",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8/low_res/A8_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8/low_res/A8.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A8/small_A8.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "59580.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8/low_res/A8_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8/low_res/A8_SE.png",
				"engines": [{
					"modelCode": "4HC0UA\\1",
					"engine": "SE 3.0 TDI clean diesel quattro",
					"displayEngineName": "3.0 TDI clean diesel quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "59580.00"
				}]
			}, {
				"name": "SE Executive",
				"otrPriceMin": "62185.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8/low_res/A8_SE_Executive_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8/low_res/A8_SE_Executive.png",
				"engines": [{
					"modelCode": "4HC0UA\\1\\GPRDPRD\\GWB1WB1",
					"engine": "SE Executive 3.0 TDI clean diesel quattro",
					"displayEngineName": "3.0 TDI clean diesel quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "62185.00"
				}, {
					"modelCode": "4HC01A\\1\\GPRDPRD\\GWB1WB1",
					"engine": "SE Executive 3.0 TFSI quattro",
					"displayEngineName": "3.0 TFSI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "64290.00"
				}, {
					"modelCode": "4HC0TA\\1\\GPRDPRD\\GWB1WB1",
					"engine": "SE Executive 4.2 TDI clean diesel quattro",
					"displayEngineName": "4.2 TDI clean diesel quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "72790.00"
				}]
			}, {
				"name": "Hybrid",
				"otrPriceMin": "64280.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8/low_res/A8_Hybrid_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8/low_res/A8_Hybrid.png",
				"engines": [{
					"modelCode": "4HC0FW\\0",
					"capcode": "AUA820HYB4SHTA  4",
					"engine": "A8 hybrid 2.0 TFSI",
					"displayEngineName": "A8 hybrid 2.0 TFSI",
					"gearType": "tiptronic",
					"otrPriceMin": "64280.00"
				}]
			}, {
				"name": "Sport Executive",
				"otrPriceMin": "65785.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8/low_res/A8_Sport_Executive_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8/low_res/A8_Sport_Executive.png",
				"engines": [{
					"modelCode": "4HC0UA\\1\\GPQ8PQ8\\GPS1PS1\\GWB2WB2\\MLRA1XX\\MSSH4KS\\MLSE8RY",
					"engine": "Sport Executive 3.0 TDI clean diesel quattro",
					"displayEngineName": "3.0 TDI clean diesel quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "65785.00"
				}, {
					"modelCode": "4HC01A\\1\\GPQ8PQ8\\GPS1PS1\\GWB2WB2\\MLRA1XX\\MSSH4KS\\MLSE8RY",
					"engine": "Sport Executive 3.0 TFSI quattro",
					"displayEngineName": "3.0 TFSI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "67890.00"
				}, {
					"modelCode": "4HC0TA\\1\\GPQ8PQ8\\GPS1PS1\\GWB2WB2\\MLRA1XX\\MSSH4KS\\MLSE8RY",
					"engine": "Sport Executive 4.2 TDI clean diesel quattro",
					"displayEngineName": "4.2 TDI clean diesel quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "76390.00"
				}]
			}]
		}, {
			"name": "A8L",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8L/low_res/A8L_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8L/low_res/A8L.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A8/small_A8L.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "63545.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8L/low_res/A8L_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8L/low_res/A8L_SE.png",
				"engines": [{
					"modelCode": "4HL0UA\\1",
					"engine": "SE 3.0 TDI clean diesel quattro LWB",
					"displayEngineName": "3.0 TDI clean diesel quattro LWB",
					"gearType": "tiptronic",
					"otrPriceMin": "63545.00"
				}]
			}, {
				"name": "SE Executive",
				"otrPriceMin": "66150.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8L/low_res/A8L_SE_Executive_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8L/low_res/A8L_SE_Executive.png",
				"engines": [{
					"modelCode": "4HL0UA\\1\\GPRDPRD\\GWB1WB1",
					"engine": "SE Executive 3.0 TDI clean diesel quattro LWB",
					"displayEngineName": "3.0 TDI clean diesel quattro LWB",
					"gearType": "tiptronic",
					"otrPriceMin": "66150.00"
				}, {
					"modelCode": "4HL0CA\\1\\GPRDPRD\\GWB1WB1",
					"engine": "SE Executive 4.0 TFSI quattro LWB",
					"displayEngineName": "4.0 TFSI quattro LWB",
					"gearType": "tiptronic",
					"otrPriceMin": "76160.00"
				}, {
					"modelCode": "4HL0TA\\1\\GPRDPRD\\GWB1WB1",
					"engine": "SE Executive 4.2 TDI clean diesel quattro LWB",
					"displayEngineName": "4.2 TDI clean diesel quattro LWB",
					"gearType": "tiptronic",
					"otrPriceMin": "76755.00"
				}]
			}, {
				"name": "Hybrid",
				"otrPriceMin": "68245.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8L/low_res/A8L_Hybrid_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8L/low_res/A8L_Hybrid.png",
				"engines": [{
					"modelCode": "4HL0FW\\0",
					"capcode": "AUA820HYL4SHTA  4",
					"engine": "A8 hybrid 2.0 TFSI LWB",
					"displayEngineName": "A8 hybrid 2.0 TFSI LWB",
					"gearType": "tiptronic",
					"otrPriceMin": "68245.00"
				}]
			}, {
				"name": "Sport Executive",
				"otrPriceMin": "69750.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8L/low_res/A8L_Sport_Executive_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8L/low_res/A8L_Sport_Executive.png",
				"engines": [{
					"modelCode": "4HL0UA\\1\\GPQ8PQ8\\GPS1PS1\\GWB2WB2\\MLRA1XX\\MSSH4KS\\MLSE8RY",
					"engine": "Sport Executive 3.0 TDI clean diesel quattro LWB",
					"displayEngineName": "3.0 TDI clean diesel quattro LWB",
					"gearType": "tiptronic",
					"otrPriceMin": "69750.00"
				}, {
					"modelCode": "4HL0CA\\1\\GPQ8PQ8\\GPS1PS1\\GWB2WB2\\MLRA1XX\\MSSH4KS\\MLSE8RY",
					"engine": "Sport Executive 4.0 TFSI quattro LWB",
					"displayEngineName": "4.0 TFSI quattro LWB",
					"gearType": "tiptronic",
					"otrPriceMin": "79760.00"
				}, {
					"modelCode": "4HL0TA\\1\\GPQ8PQ8\\GPS1PS1\\GWB2WB2\\MLRA1XX\\MSSH4KS\\MLSE8RY",
					"engine": "Sport Executive 4.2 TDI clean diesel quattro LWB",
					"displayEngineName": "4.2 TDI clean diesel quattro LWB",
					"gearType": "tiptronic",
					"otrPriceMin": "80355.00"
				}]
			}]
		}, {
			"name": "S8",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/S8/low_res/S8_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/S8/low_res/S8.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A8/small_S8.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "80690.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/S8/low_res/S8_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/S8/low_res/S8_SE.png",
				"engines": [{
					"modelCode": "4HCSSA\\0",
					"capcode": "AUA840S8 4SPTA4 4",
					"engine": "4.0 TFSI quattro",
					"displayEngineName": "4.0 TFSI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "80690.00"
				}]
			}]
		}, {
			"name": "A8 W12",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8_W12/low_res/A8_W12_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8_W12/low_res/A8_W12.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/A8/small_A8_W12.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "98100.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8_W12/low_res/A8_W12_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/A8/A8_W12/low_res/A8_W12_SE.png",
				"engines": [{
					"modelCode": "4HL0GA\\0",
					"capcode": "AUA863LWB4SPIA4 4",
					"engine": "W12 6.3 FSI",
					"displayEngineName": "W12 6.3 FSI",
					"gearType": "tiptronic",
					"otrPriceMin": "98100.00"
				}]
			}]
		}]
	}, {
		"rangeName": "Q5",
		"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/Q5_front.png",
		"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/Q5.png",
		"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Range/small_Q5.png",
		"models": [{
			"name": "Q5",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/Q5/low_res/Q5_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/Q5/low_res/Q5.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Q5/small_Q5.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "31370.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/Q5/low_res/Q5_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/Q5/low_res/Q5_SE.png",
				"engines": [{
					"modelCode": "8RB0N9\\1\\GPEBPEB\\GPRDPRD\\MSFSVJ1\\GWB1WB1\\MLRA2ZQ\\MEIH5MF\\MEPH7X1\\MLSE9VD",
					"capcode": "AUQ520SE85EPTM4 1",
					"engine": "SE 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "31370.00"
				}, {
					"modelCode": "8RB0S9\\1\\GPEBPEB\\GPRDPRD\\MSFSVJ1\\GWB1WB1\\MLRA2ZQ\\MEIH5MF\\MEPH7X1\\MLSE9VD",
					"capcode": "AUQ5205SE5EDTM4 1",
					"engine": "SE 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "31635.00"
				}, {
					"modelCode": "8RB0MY\\1\\GPEBPEB\\GPRDPRD\\MSFSVJ1\\GWB1WB1\\MLRA2ZS\\MEIH5MF\\MEPH7X1\\MLSE9VD",
					"capcode": "AUQ520SE 5EDTA4 1",
					"engine": "SE 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "34160.00"
				}, {
					"modelCode": "8RB0AA\\1\\GPEBPEB\\GPRDPRD\\MSFSVJ1\\GWB1WB1\\MLRA2ZS\\MEIH5MF\\MEPH7X1\\MLSE9VD",
					"capcode": "AUQ520SE 5EPTA4 1",
					"engine": "SE 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "34325.00"
				}, {
					"modelCode": "8RB0JY\\1\\GPEBPEB\\GPRDPRD\\MSFSVJ1\\GWB1WB1\\MLRA2ZS\\MEIH5MF\\MEPH7X1\\MLSE9VD",
					"capcode": "AUQ530SE 5EDTA4 1",
					"engine": "SE 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "38370.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "33770.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/Q5/low_res/Q5_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/Q5/low_res/Q5_S_line.png",
				"engines": [{
					"modelCode": "8RB0N9\\1\\MSIBN2R\\GPQRPQR\\GWQRWQR\\MLRA1XW\\MZIE4ZB\\MLUM7HB\\MEPH7X1\\MLSE9VD",
					"capcode": "AUQ52018L5EPTM4 1",
					"engine": "S line 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "33770.00"
				}, {
					"modelCode": "8RB0S9\\1\\MSIBN2R\\GPQRPQR\\GWQRWQR\\MLRA1XW\\MZIE4ZB\\MLUM7HB\\MEPH7X1\\MLSE9VD",
					"capcode": "AUQ5201SL5EDTM4 1",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "34035.00"
				}, {
					"modelCode": "8RB0MY\\1\\MSIBN2R\\GPQRPQR\\GWQRWQR\\MLRA1XX\\MZIE4ZB\\MLUM7HB\\MEPH7X1\\MLSE9VD",
					"capcode": "AUQ520SL 5EDTA4 1",
					"engine": "S line 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "36560.00"
				}, {
					"modelCode": "8RB0AA\\1\\MSIBN2R\\GPQRPQR\\GWQRWQR\\MLRA1XX\\MZIE4ZB\\MLUM7HB\\MEPH7X1\\MLSE9VD",
					"capcode": "AUQ520SL 5EPTA4 1",
					"engine": "S line 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "36725.00"
				}, {
					"modelCode": "8RB0JY\\1\\MSIBN2R\\GPQRPQR\\GWQRWQR\\MLRA1XX\\MZIE4ZB\\MLUM7HB\\MEPH7X1\\MLSE9VD",
					"capcode": "AUQ530SL 5EDTA4 1",
					"engine": "S line 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "40770.00"
				}]
			}, {
				"name": "S line Plus",
				"otrPriceMin": "36270.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/Q5/low_res/Q5_S_line_Plus_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/Q5/low_res/Q5_S_line_Plus.png",
				"engines": [{
					"modelCode": "8RB0N9\\1\\MSIBN2R\\GWB7WB7\\GWC9WC9\\GWQRWQR\\MLRA1XW\\MZIE4ZB\\MLUM7HB\\MLSE9VD",
					"capcode": "AUQ5208SP5EPTM4 1",
					"engine": "S line Plus 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "36270.00"
				}, {
					"modelCode": "8RB0S9\\1\\MSIBN2R\\GWB7WB7\\GWC9WC9\\GWQRWQR\\MLRA1XW\\MZIE4ZB\\MLUM7HB\\MLSE9VD",
					"capcode": "AUQ5205SN5EDTM4 1",
					"engine": "S line Plus 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "Manual",
					"otrPriceMin": "36535.00"
				}, {
					"modelCode": "8RB0MY\\1\\MSIBN2R\\GWB7WB7\\GWC9WC9\\GWQRWQR\\MLRA1XX\\MZIE4ZB\\MLUM7HB\\MLSE9VD",
					"capcode": "AUQ520SLP5EDTA4 1",
					"engine": "S line Plus 2.0 TDI quattro",
					"displayEngineName": "2.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "39060.00"
				}, {
					"modelCode": "8RB0AA\\1\\MSIBN2R\\GWB7WB7\\GWC9WC9\\GWQRWQR\\MLRA1XX\\MZIE4ZB\\MLUM7HB\\MLSE9VD",
					"capcode": "AUQ520SLP5EPTA4 1",
					"engine": "S line Plus 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "39225.00"
				}, {
					"modelCode": "8RB0JY\\1\\MSIBN2R\\GWB7WB7\\GWC9WC9\\GWQRWQR\\MLRA1XX\\MZIE4ZB\\MLUM7HB\\MLSE9VD",
					"capcode": "AUQ530SLP5EDTA4 1",
					"engine": "S line Plus 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "43270.00"
				}]
			}]
		}, {
			"name": "SQ5",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/SQ5/low_res/SQ5_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/SQ5/low_res/SQ5.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Q5/small_SQ5.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "44715.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/SQ5/low_res/SQ5_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/Q5/SQ5/low_res/SQ5_SE.png",
				"engines": [{
					"modelCode": "8RBSUA\\1",
					"capcode": "AUQ530SQ55EDTA4 1",
					"engine": "3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "44715.00"
				}]
			}]
		}]
	}, {
		"rangeName": "Q7",
		"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/Q7/Q7_front.png",
		"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/Q7/Q7.png",
		"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Range/small_Q7.png",
		"models": [{
			"name": "Q7",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/Q7/Q7/low_res/Q7_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/Q7/Q7/low_res/Q7.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Q7/small_Q7.png",
			"trimlines": [{
				"name": "S line",
				"otrPriceMin": "46655.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/Q7/Q7/low_res/Q7_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/Q7/Q7/low_res/Q7_S_line.png",
				"engines": [{
					"modelCode": "4LB0WA\\0\\MSIBN7K\\GPDZPDZ\\GPQRPQR\\GWQRWQR\\MLRA1XX\\MSIH4A3\\MVTV9ZX",
					"capcode": "AUQ730S4L5EDTA4 1",
					"engine": "S line 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "46655.00"
				}, {
					"modelCode": "4LB0VA\\1\\MSIBN7K\\GPDZPDZ\\GPQRPQR\\GWQRWQR\\MLRA1XX\\MSIH4A3\\MVTV9ZX",
					"capcode": "AUQ741S8L5EDTA4 1",
					"engine": "S line 4.2 TDI quattro",
					"displayEngineName": "4.2 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "57720.00"
				}]
			}, {
				"name": "S line Plus",
				"otrPriceMin": "51155.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/Q7/Q7/low_res/Q7_S_line_Plus_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/Q7/Q7/low_res/Q7_S_line_Plus.png",
				"engines": [{
					"modelCode": "4LB0WA\\0\\MSIBN1W\\GPDZPDZ\\GPQ6PQ6\\MSNRSD8\\GWC3WC3\\GWQRWQR\\GYNBYNB\\MLRA1XX\\MVTV9ZW",
					"capcode": "AUQ730S4P5EDTA4 1",
					"engine": "S line Plus 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "51155.00"
				}, {
					"modelCode": "4LB0VA\\1\\MSIBN1W\\GPDZPDZ\\GPQ6PQ6\\MSNRSD8\\GWC3WC3\\GWQRWQR\\GYNBYNB\\MLRA1XX\\MVTV9ZW",
					"capcode": "AUQ741SLP5EDTA4 1",
					"engine": "S line Plus 4.2 TDI quattro",
					"displayEngineName": "4.2 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "62220.00"
				}]
			}, {
				"name": "S line Style Edition",
				"otrPriceMin": "54085.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/Q7/Q7/low_res/Q7_S_line_Style_Edition_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/Q7/Q7/low_res/Q7_S_line_Style_Edition.png",
				"engines": [{
					"modelCode": "4LB0GA\\2\\MSIBN1W\\GPDZPDZ\\GPQWPQW\\MSNRSD8\\MSNRS8Z\\GWQYWQY\\GYNBYNB\\GYNVYNV\\MLRA1XX\\MVTV9ZW",
					"capcode": "AUQ730STY5EDTA4 1",
					"engine": "S line Style Edition 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "54085.00"
				}, {
					"modelCode": "4LB0VA\\1\\MSIBN1W\\GPDZPDZ\\GPQWPQW\\MSNRSD8\\MSNRS8Z\\GWQYWQY\\GYNBYNB\\GYNVYNV\\MLRA1XX\\MVTV9ZW",
					"capcode": "AUQ741STY5EDTA4 1",
					"engine": "S line Style Edition 4.2 TDI quattro",
					"displayEngineName": "4.2 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "63720.00"
				}]
			}, {
				"name": "S line Sport Edition",
				"otrPriceMin": "55585.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/Q7/Q7/low_res/Q7_S_line_Sport_Edition_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/Q7/Q7/low_res/Q7_S_line_Sport_Edition.png",
				"engines": [{
					"modelCode": "4LB0GA\\2\\MSIBN1W\\GPQ8PQ8\\MSNRSD8\\GWQFWQF\\GWQPWQP\\GYNBYNB\\MLRA1XX\\MVTV9ZW",
					"capcode": "AUQ730STP5EDTA4 1",
					"engine": "S line Sport Edition 3.0 TDI quattro",
					"displayEngineName": "3.0 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "55585.00"
				}, {
					"modelCode": "4LB0VA\\1\\MSIBN1W\\GPQ8PQ8\\MSNRSD8\\GWQFWQF\\GWQPWQP\\GYNBYNB\\MLRA1XX\\MVTV9ZW",
					"capcode": "AUQ741STP5EDTA4 1",
					"engine": "S line Sport Edition 4.2 TDI quattro",
					"displayEngineName": "4.2 TDI quattro",
					"gearType": "tiptronic",
					"otrPriceMin": "65220.00"
				}]
			}]
		}]
	}, {
		"rangeName": "R8",
		"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/R8/R8_front.png",
		"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/R8/R8.png",
		"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Range/small_R8.png",
		"models": [{
			"name": "R8 Coupé",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/R8/R8_Coupe/low_res/R8_Coupe_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/R8/R8_Coupe/low_res/R8_Coupe.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/R8/small_R8_Coupe.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "93735.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/R8/R8_Coupe/low_res/R8_Coupe_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/R8/R8_Coupe/low_res/R8_Coupe_SE.png",
				"engines": [{
					"modelCode": "4230NT\\0",
					"capcode": "AUR842   2CPIM4 1",
					"engine": "4.2 FSI quattro",
					"displayEngineName": "4.2 FSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "93735.00"
				}, {
					"modelCode": "4230NE\\0",
					"capcode": "AUR842   2CPIA4 1",
					"engine": "4.2 FSI quattro",
					"displayEngineName": "4.2 FSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "96635.00"
				}, {
					"modelCode": "42302T\\0",
					"capcode": "AUR852   2CPIM4 1",
					"engine": "5.2 FSI quattro",
					"displayEngineName": "5.2 FSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "114835.00"
				}, {
					"modelCode": "42302E\\0",
					"capcode": "AUR852   2CPIA4 1",
					"engine": "5.2 FSI quattro",
					"displayEngineName": "5.2 FSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "117735.00"
				}, {
					"modelCode": "42304T\\0",
					"capcode": "AUR852PL 2CPIM4 1",
					"engine": "5.2 FSI quattro V10 plus",
					"displayEngineName": "5.2 FSI quattro V10 plus",
					"gearType": "Manual",
					"otrPriceMin": "126835.00"
				}, {
					"modelCode": "42304E\\0",
					"capcode": "AUR852PL 2CPIA4 1",
					"engine": "5.2 FSI quattro V10 plus",
					"displayEngineName": "5.2 FSI quattro V10 plus",
					"gearType": "S tronic",
					"otrPriceMin": "129735.00"
				}]
			}]
		}, {
			"name": "R8 Spyder",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/R8/R8_Spyder/low_res/R8_Spyder_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/R8/R8_Spyder/low_res/R8_Spyder.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/R8/small_R8_Spyder.png",
			"trimlines": [{
				"name": "SE",
				"otrPriceMin": "102385.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/R8/R8_Spyder/low_res/R8_Spyder_SE_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/R8/R8_Spyder/low_res/R8_Spyder_SE.png",
				"engines": [{
					"modelCode": "4290NT\\0",
					"capcode": "AUR842   2VPIM4 1",
					"engine": "4.2 FSI quattro",
					"displayEngineName": "4.2 FSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "102385.00"
				}, {
					"modelCode": "4290NE\\0",
					"capcode": "AUR842   2VPIA4 1",
					"engine": "4.2 FSI quattro",
					"displayEngineName": "4.2 FSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "105285.00"
				}, {
					"modelCode": "42902T\\0",
					"capcode": "AUR852   2VPIM4 1",
					"engine": "5.2 FSI quattro",
					"displayEngineName": "5.2 FSI quattro",
					"gearType": "Manual",
					"otrPriceMin": "123485.00"
				}, {
					"modelCode": "42902E\\0",
					"capcode": "AUR852   2VPIA4 1",
					"engine": "5.2 FSI quattro",
					"displayEngineName": "5.2 FSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "126385.00"
				}]
			}]
		}]
	}, {
		"rangeName": "TT",
		"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/TT/TT_front.png",
		"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/TT/TT.png",
		"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/Range/small_TT.png",
		"models": [{
			"name": "TT Coupé",
			"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/TT/TT_Coupe/low_res/TT_Coupe_front.png",
			"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/TT/TT_Coupe/low_res/TT_Coupe.png",
			"imageSpritePath": "http://www.audi.co.uk/etc/designs/audi/img/model-explorer/thumbnails/Small/TT/small_TT_Coupe.png",
			"trimlines": [{
				"name": "Sport",
				"otrPriceMin": "29770.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/TT/TT_Coupe/low_res/TT_Coupe_Sport_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/TT/TT_Coupe/low_res/TT_Coupe_Sport.png",
				"engines": [{
					"modelCode": "FV30TC\\2",
					"capcode": "AUTT20SPO2CDTM  2",
					"engine": "Sport 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "29770.00"
				}, {
					"modelCode": "FV307C\\2",
					"capcode": "AUTT20SPO2CPTM  2",
					"engine": "Sport 2.0 TFSI",
					"displayEngineName": "2.0 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "29860.00"
				}, {
					"modelCode": "FV307L\\2",
					"capcode": "AUTT20SPO2CPTA4 2",
					"engine": "Sport 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "32785.00"
				}]
			}, {
				"name": "S line",
				"otrPriceMin": "32320.00",
				"imageFrontPath": "http://cdn.audi.co.uk/dam/7_days/Models/TT/TT_Coupe/low_res/TT_Coupe_S_line_front.png",
				"imageSidePath": "http://cdn.audi.co.uk/dam/7_days/Models/TT/TT_Coupe/low_res/TT_Coupe_S_line.png",
				"engines": [{
					"modelCode": "FV30TC\\2\\MRADCJ3\\MSIBN7K\\GPX2PX2\\GWQRWQR\\MWSS4GH",
					"capcode": "AUTT20SL 2CDTM  2",
					"engine": "S line 2.0 TDI ultra",
					"displayEngineName": "2.0 TDI ultra",
					"gearType": "Manual",
					"otrPriceMin": "32320.00"
				}, {
					"modelCode": "FV307C\\2\\MRADCJ3\\MSIBN7K\\GPX2PX2\\GWQRWQR",
					"capcode": "AUTT20SL 2CPTM  2",
					"engine": "S line 2.0 TFSI",
					"displayEngineName": "2.0 TFSI",
					"gearType": "Manual",
					"otrPriceMin": "32410.00"
				}, {
					"modelCode": "FV307L\\2\\MRADCJ3\\MSIBN7K\\GPX2PX2\\GWQRWQR",
					"capcode": "AUTT20SL 2CPTA4 2",
					"engine": "S line 2.0 TFSI quattro",
					"displayEngineName": "2.0 TFSI quattro",
					"gearType": "S tronic",
					"otrPriceMin": "35335.00"
				}]
			}]
		}]
	}]
}