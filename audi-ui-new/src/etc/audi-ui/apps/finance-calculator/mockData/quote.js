{
  "responseInfo": {
    "desc": "SUCCESS"
  },
  "data": {
    "parameters": {
      "periodValue": 36,
      "periodMinimum": 18,
      "periodMaximum": 49,
      "periodIncrement": 1,
      "annualMileage": 20,
      "annualMileageMinimum": 0,
      "annualMileageMaximum": 100,
      "annualMileageIncrement": 1,
      "fixedCostMaintenance": 0,
      "rentalsInAdvance": 0,
      "depositAmount": "1980.50",
      "depositPercentage": "0.00",
      "amountFinanced": "37629.50",
      "finalPayment": "15739.60",
      "excessMileage": "9.00"
    },
    "quote": {
      "depositContribution": "0.00",
      "retailerCashPrice": "39610.00",
      "initialPayment": "0.00",
      "initialPaymentText": "1 payment of",
      "apr": "7.20",
      "customerRate": "3.59",
      "acceptanceFee": "125.00",
      "isBalancedFee": false,
      "purchaseOptionFee": "60.00",
      "documentationFee": "752.59",
      "monthlyRental": "785.84",
      "finalPayment": "15739.60",
      "financeCommission": "752.59",
      "manufacturerSubsidy": "0.00",
      "brandSubsidy": "0.00",
      "amountFinanced": "37629.50",
      "excessMileage": "9.00",
      "totalAmountPayable": "45284.50",
      "vehicleFinancePayment": "785.84",
      "fixedCostMaintenanceCost": 0,
      "fixedCostMaintenanceInitialPayment": "0.00",
      "fixedPriceServicing": "0.00",
      "aerPayment": "6.98",
      "monthlyPayment": "785.84",
      "monthlyPaymentText": "35 payments of",
      "lastPayment": "15799.60",
      "lastPaymentText": "0.00",
      "isOffer": false,
      "legalText": "At the end of the Solutions Personal Contract Plan (PCP) agreement there are three options: i) retain the vehicle: pay the optional final payment to own the vehicle; ii) return the vehicle; or iii) replace: part exchange the vehicle, subject to status. \nRetail Sales only *Payable as first payment **Payable with optional final payment.  ***Depending on agreed annual mileage. Available to 18's and over. Offer available for vehicles ordered by 31st December 2014 and delivered by 31st March 2015 from participating Centres. ^Deposit contribution is available when purchased on Solutions PCP. Offers are not available in conjunction with any other offer and may be varied or withdrawn at any time. Subject to availability. Terms and conditions apply. Finance subject to status.  Accurate at time of publication October 2014. Audi Finance, Freepost Audi Finance",
      "balancedFee": false,
      "offer": false
    }
  }
}