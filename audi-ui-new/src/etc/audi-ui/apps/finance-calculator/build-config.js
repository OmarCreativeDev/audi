module.exports = {
	options: {
		separator: "\n\n",
		banner: "(function(){\n\n'use strict';\n\n",
		footer: "\n\n})();",
		sourceMap: false
	},
	src: [ 
		'src/etc/audi-ui/apps/finance-calculator/financeCalculator.js',
		'src/etc/audi-ui/apps/finance-calculator/ng-services/calculatorService.js',
		'src/etc/audi-ui/apps/finance-calculator/ng-services/taggingService.js',
		'src/etc/audi-ui/apps/finance-calculator/ng-components/calculateTool/calculateToolCtrl.js',
		'src/etc/audi-ui/apps/finance-calculator/ng-components/calculateTool/termsCtrl.js',
		'src/etc/audi-ui/apps/finance-calculator/ng-components/selectVehicle/selectModelCtrl.js',
		'src/etc/audi-ui/apps/finance-calculator/ng-components/calculateTool/emailQuoteCtrl.js',
		'src/etc/audi-ui/apps/finance-calculator/ng-components/error/errorCtrl.js',
		'src/etc/audi-ui/global/bower-components/angular-img-fallback/angular.dcb-img-fallback.min.js'
		
 
	],
	dest: 'src/etc/audi-ui/apps/finance-calculator/financecalculator.min.js'
};
