
	angular.module('financeCalculator').controller('selectModelCtrl', ['$scope', '$state', '$resource','calculatorService', 'taggingService', '$timeout', '$location',
		function($scope, $state, $resource, calculatorService, taggingService, $timeout, $location) {
			$scope.ranges = calculatorService.ranges;

			// Used for loading icons
			$scope.loadingRangeData = true;
			$scope.selectedRange = null;
			$scope.selectedModel = null;
			$scope.carRangeRows = [];
			$scope.selectedRangeModelRows = [];

			$scope.init = function(argument) {
				calculatorService.getRanges()
				.then(function() {
					// Success handler
					var i;

					// If we've come in with a range specified, try to find it
					if($state.params.range){
						angular.forEach($scope.ranges, function(range) {
							if(range.rangeName === $state.params.range) {
								$scope.selectRange(range);
							}
						});

						// If we didnt find the range, nuke the param in URL
						if($scope.range === null) {
							$state.go('ranges', {range: null}, {
								notify: false
							});
						}
					}

					// Split data up into chunks of 4 (for display purposes)
					for (i=0; i<$scope.ranges.length; i+=4) {
						$scope.carRangeRows.push($scope.ranges.slice(i, i+4));
					}

					// do tagging, remove loading flag
					taggingService.modelSelectorLoaded();
					$scope.loadingRangeData = false;
				},

				// Error handler
				function(data, status) {
					$timeout(function() {
						$state.go('error');
					}, 250);
					$scope.loadingRangeData = false;
				});
			}

			$scope.selectRange = function(range) {
				var i;

				$scope.selectedRange = range;
				/*$state.go('ranges', {range: range.rangeName}, {
					notify: false,
					replace: true
				});*/

				$location.search({range: range.rangeName});
				$location.replace();

				// Split data up into chunks of 4 (for display purposes)
				$scope.selectedRangeModelRows.length = 0;
				for (i=0; i<$scope.selectedRange.models.length; i+=4) {
					$scope.selectedRangeModelRows.push($scope.selectedRange.models.slice(i, i+4));
				}
			}

			$scope.selectModel = function(model) {
				if($scope.selectedModel === null) {
					$scope.selectedModel = model;
					$state.go('calculator', {
						range: $scope.selectedRange.rangeName,
						model: model.name
					});
				}
			}

			$scope.deselectRange = function($event) {
				$event.preventDefault();
				$event.stopPropagation();

				$scope.selectedRange = null;
				$scope.selectedRangeModelRows.length = 0;

				$location.search({range: null});
				$location.replace();
				//$state.go('ranges', {range: null});
			}

			$scope.init();
		}
	]);


	angular.module('financeCalculator').filter('partition', function($cacheFactory) {
		var arrayCache = $cacheFactory('partition')
		return function(arr, size) {
			try {

				var parts = [],
					cachedParts,
					jsonArr = JSON.stringify(arr);
				for (var i = 0; i < arr.length; i += size) {
					parts.push(arr.slice(i, i + size));
				}
				cachedParts = arrayCache.get(jsonArr);
				if (JSON.stringify(cachedParts) === JSON.stringify(parts)) {
					return cachedParts;
				}
				arrayCache.put(jsonArr, parts);

			} catch (e) {
				parts = arr;
			}
			return parts;

		};
	});


	/*angular.module('financeCalculator').directive('rangeoption', function() {
		return {
			restrict: 'C',
			replace: false,
			scope: {
				range: "="
			},

			link: function (scope, element, attrs) {
				element.bind("click", function(){
					var rangeRow = jQuery(element.parent()),
						modelsRow = jQuery('.modelsContainer');

					modelsRow.insertAfter(rangeRow);
					console.log(scope, element, attrs);
				});
			}
		}
	})*/
