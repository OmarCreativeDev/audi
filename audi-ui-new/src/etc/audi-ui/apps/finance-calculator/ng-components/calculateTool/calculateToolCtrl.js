        angular.module('financeCalculator').controller('calculateToolCtrl', ['$scope', '$rootScope', '$state', 'calculatorService', '$timeout', '$modal', '$q', '$window', 'taggingService', '$location',
            function($scope, $rootScope, $state, calculatorService, $timeout, $modal, $q, $window, taggingService, $location) {
                $scope.params ={}; // holds all the params that are needed in the query string to store the state

                var formChangeTimeout = null; // timeout to make sure if multi actions to the $scope.params are done quickly we only update the $state.params after 1s

                /* application view settings */
                $scope.settings = {
                    mileage: [5, 10, 15, 20, 25, 30],
                    mileageIncr: 5,
                    period: [18, 24, 30, 36, 42, 48],
                    periodIncr: 6

                    /*periodMin: 18,
                    periodMax: 60,
                    periodStep: 6,

                    mileageMin: 5,
                    mileageMax: 100,
                    mileageStep: 5,*/
                };

                // CQ5 params
                $scope.CQ5Properties = {};

                // Data Models
                $scope.ranges = calculatorService.ranges; // All available ranges and their models
                $scope.range = null;
                $scope.model = null;
                $scope.trim = null;
                $scope.engine = null;
                $scope.financeData = calculatorService.financeData;

                $scope.$location = $location;
                $scope.$state = $state;

                // Loading indicators
                $scope.loadingRangeData = true;
                $scope.calcDisplayLoading = true;
                var currentRequest = null;

                // Used for 'change' section
                $scope.showChangeSection = false;
                $scope.changeSectionRange = null;
                $scope.changeSectionModel = null;

                // Used for trimline / engine selection section
                $scope.trim = {};
                $scope.engine = {};

                // Deposit vars - depositPrevious used to toggle the UPDATE button
                $scope.deposit = $scope.depositPrevious = $state.params.deposit;

                // Error / status indicators
                $scope.financeAvailable = false;
                $scope.invalidDeposit = false;
                $scope.currentError = {};
                //$scope.tempPeriodSliderValue = null;
                //$scope.tempMileageSliderValue = null;

                $scope.init = function() {
                    console.log('***** FINANCE CALC QUOTE PAGE INIT *****');
                    angular.copy($state.params, $scope.params); // copy the $state to  $scope
                    console.log('init');

                    // set some defaults
                    var t = {};
                    var quoteQueued = false;

                    if ($scope.params.range == null) {
                        $state.go('ranges');
                        return;
                    }
                    if ($scope.params.model == null) {
                        $state.go('ranges.range', {range: $scope.params.range});
                        return;
                    }

                    // Get CQ5 properties
                    calculatorService.getCQ5Properties().then(function(props){
                        angular.extend($scope.CQ5Properties, props);

                        // Get range / model data
                        calculatorService.getRanges().then(function(){
                            // Set some defaults if not specified
                            if ($scope.params.productType == null) {
                                $scope.formChange('productType', 'SOLUTIONS', true);
                            }

                            // Find selected range (if not coming from ranges page - update service and scope)
                            if($scope.range === null || $scope.range.rangeName !== $scope.params.range) {
                                $scope.range = getURLRange();
                                if($scope.range === null) {
                                    // Couldnt find range
                                    $state.go('ranges');
                                    return;
                                }
                            }

                            // Find selected model (if not coming from ranges page - update service and scope)
                            if($scope.model === null || $scope.model.name !== $scope.params.model) {
                                $scope.model = getURLModel();
                                if($scope.model === null) {
                                    // Couldnt find model
                                    $state.go('ranges.range', {range: $scope.params.range});
                                    return;
                                }
                            }

                            // Sort out trimline
                            $scope.trim = getURLTrim();
                            if($scope.trim === null) {
                                $scope.trim = getCheapestTrim();
                                $scope.formChange('trim', $scope.trim.name, true);
                            }

                            // Sort out engine engine
                            $scope.engine = getURLEngine();
                            if($scope.engine === null) {
                                $scope.engine = getCheapestEngine();
                                $scope.formChange('modelCode', $scope.engine.modelCode, true);
                            }

                            // Set up 'change' section
                            $scope.changeSectionRange = $scope.range;
                            $scope.changeSectionModel = $scope.model;

                            $scope.getQuoteExample();
                            taggingService.financeCalculatorLoaded();
                            $scope.loadingRangeData = false;
                        }, function(){
                            // Cant get ranges - error handler - show main error screen
                            $timeout(function() {
                                $state.go('error');
                            }, 500);
                        });
                    },
                    function() {
                        // Cant get CQ5 data - error handler - show main error screen
                        $timeout(function() {
                            $state.go('error');
                        }, 500);
                    });
                }

                $scope.getQuoteExample = function(reset) {
                    $scope.calcDisplayLoading = true;
                    $scope.currentError = {};

                    // cancel last request if required
                    if(currentRequest !== null) {
                        currentRequest.abort();
                    }

                    (currentRequest = calculatorService.getQuote($scope.params, false, reset)).then(function(resp) {
                        // Success
                        var data = resp.result.data;

                        $scope.financeAvailable = true;
                        $scope.calcDisplayLoading = false;

                        // Update repayment period and mileage ranges
                        //calculatePeriodRange();
                        //calculateMileageRange();

                        // Set default deposit, mileage and period to match quote
                        $scope.setDeposit(parseFloat(data.parameters.depositAmount), true);
                        $scope.formChange('mileage', data.parameters.annualMileage, true);
                        $scope.formChange('period', data.parameters.periodValue, true);

                        // Tag calculator refresh
                        var filterString = [
                            'pcp:' + $scope.model.name,
                            $scope.trim.name,
                            $scope.engine.displayEngineName,
                            $scope.deposit,
                            $scope.params.mileage*1000,
                            $scope.params.period
                        ].join('|');

                        taggingService.quoteRefresh(filterString);
                    },
                    function(resp) {
                        // Aborted
                        if(typeof resp !== 'undefined' && typeof resp.result !== 'undefined' && resp.result === 'ABORTED') {
                            return;
                        }

                        // Handled error
                        if(resp.result.responseInfo && resp.result.responseInfo.desc.toUpperCase() === 'FAILED') {
                            $scope.financeAvailable = true;
                            $scope.invalidDeposit = false;
                            switch(resp.result.responseInfo.code) {
                                case '101': // Invalid deposit
                                case '102':
                                case '103':
                                case '104':
                                case '106':
                                case '107':
                                case '108':
                                case '109':
                                case '111':
                                	$scope.invalidDeposit = true;
                                    taggingService.invalidDeposit($scope.deposit);
                                    break;
                                case '110': // Invalid mileage
                                case '112':
                                	break;
                                case '70': // No finance available
                                case '120':
                                    $scope.financeAvailable = false;
                                    break;
                                default: // Anything else - curtains.
									$timeout(function() {$state.go('error');}, 500);
									return;
                            }

                            $scope.currentError = resp.result.responseInfo;
                            $scope.calcDisplayLoading = false;
                        }

                        // server error
                        else {
                            $timeout(function() {
                                $state.go('error');
                            }, 500);
                        }
                    });
                }

                $scope.getPDF = function() {
                    calculatorService.getQuote($scope.params, true);
                }

                $scope.printQuote = function() {
                    $window.print();
                }

                $scope.arrangeTestDrive = function() {
                    var params = {
                        range: $scope.params.range,
                        model: $scope.params.model
                    };
                    $window.open("/content/audi/about-audi/contact-us/arrange-a-test-drive/test-drive-form.html?" + serialize(params));
                }

                $scope.locateACentre = function() {
                    $window.open("/content/audi/locate-a-centre.html");
                }

                $scope.trimLineChange = function(trim) {
                	if(typeof trim !== 'undefined' && trim !== null) {
	                    $scope.calcDisplayLoading = true;
                        $scope.financeAvailable = false;

                        $scope.setDeposit(null, true);
                        $scope.formChange('mileage', null, true);
                        $scope.formChange('period', null, true);

                        $scope.engine = getCheapestEngine();
	                    $scope.formChange('trim', trim, true);
	                    $scope.formChange('modelCode', $scope.engine.modelCode, false, true);
	                }
                }

                $scope.engineChange = function(modelCode) {
                	if(typeof modelCode !== 'undefined' && modelCode !== null) {
                        $scope.calcDisplayLoading = true;
                        $scope.financeAvailable = false;

	                    $scope.setDeposit(null, true);
                        $scope.formChange('mileage', null, true);
                        $scope.formChange('period', null, true);
	                    $scope.formChange('modelCode', modelCode, false, true);
	                }
                }

                $scope.modeChange = function(field, value) {
                    $scope.formChange(field, value);
                    $timeout(taggingService.financeCalculatorLoaded, 250);
                }

                $scope.mileageChange = function(mileage) {
                    // If the selected period is now unavailable due to mileage/period restriction, select highest available
                    if(!$scope.checkPeriodAvailable($scope.params.period)) {
                        $scope.formChange('period', newPeriod, true);
                    }
                    $scope.formChange('mileage', mileage);
                }

                $scope.periodChange = function(period) {
                    // If the selected mileage is now unavailable due to mileage/period restriction, select highest available
                    if(!$scope.checkMileageAvailable($scope.params.period)) {
                        setHighestAvailableMileage(period);
                    }
                    $scope.formChange('mileage', mileage);
                }

                $scope.formChange = function(field, value, doNotGetQuote, reset) {
                    $scope.params[field] = value;
                    console.log('PARAMETER CHANGE - ' + field + ' changed to "' +  value + '"');

                    // This allows code to use this function multiple times without cuasing multiple reloads
                    $timeout.cancel(formChangeTimeout);
                    formChangeTimeout = $timeout(function(){
                        /*$state.go('calculator', $scope.params, {
                            notify: false
                        });*/
                        $location.search($scope.params);
                        $location.replace();

                        if(!doNotGetQuote) {
                            $scope.getQuoteExample(reset);
                        }
                    }, 30);
                }

                $scope.updateModel = function() {
                    if($scope.changeSectionModel !== null) {
                        $scope.showChangeSection = false;
                        $scope.calcDisplayLoading = true;
                        $scope.financeAvailable = false;

                        // Update scope with new range, model, trimline and engine
                        $scope.range  = $scope.changeSectionRange;
                        $scope.model  = $scope.changeSectionModel;
                        $scope.trim   = getCheapestTrim();
                        $scope.engine = getCheapestEngine();

                        // Unset deposit, mileage and period
                        $scope.setDeposit(null, true);
                        $scope.formChange('mileage', null, true);
                        $scope.formChange('period', null, true);

                        // Set other parameters and update quote
                        $scope.calcDisplayLoading = true;
                        $scope.formChange('range', $scope.range.rangeName, true);
                        $scope.formChange('model', $scope.model.name, true);
                        $scope.formChange('trim', $scope.trim.name, true);
                        $scope.formChange('modelCode', $scope.engine.modelCode, false, true);
                    }
                }

                $scope.cancelUpdateModel = function() {
                    $scope.showChangeSection = false;
                    $scope.changeSectionRange = $scope.range;
                    $scope.changeSectionModel = $scope.model;
                }

                $scope.getClass = function(what, value) {
                    var str = "";
                    if($scope.params[what] == value) str = str + "active";
                    //if(what !== 'productType' && !$scope.financeAvailable) str = str + " disabled";
                    return str;
                }

                $scope.openEmailQuoteModal = function() {
                    $modal.open({
                        templateUrl: '/etc/audi-ui/apps/finance-calculator/ng-components/calculateTool/emailQuote.html',
                        controller: 'emailQuoteCtrl',
                        resolve: {
                            params: function () {
                                return $scope.params;
                            },
                            arrangeTestDrive: function() {
                                return $scope.arrangeTestDrive;
                            },
                            locateACentre: function() {
                                return $scope.locateACentre;
                            },
                            parentScopeData: function() {
                                return {
                                    model: $scope.model,
                                    trim: $scope.trim,
                                    engine: $scope.engine,
                                    deposit: $scope.deposit
                                }
                            }
                        }
                    });
                }

                $scope.openTermsModal = function() {
                    $modal.open({
                        templateUrl: '/etc/audi-ui/apps/finance-calculator/ng-components/calculateTool/terms.html',
                        controller: 'termsCtrl',
                        size: 'lg',
                        resolve: {
                            terms: function () {
                                if($scope.params.productType === 'SOLUTIONS') {
                                    return $scope.CQ5Properties.PCPFullTerms;
                                } else {
                                    return $scope.CQ5Properties.HPFullTerms;
                                }
                            }
                        }
                    });
                }

                $scope.setDeposit = function(override, doNotGetQuote) {
                    var deposit = override ? override : null;
                    if(typeof doNotGetQuote === 'undefined') doNotGetQuote = false;
                    $scope.deposit = $scope.depositPrevious = deposit;
                    $scope.formChange('deposit', $scope.deposit, doNotGetQuote);
                    $scope.invalidDeposit = false;
                    return deposit;
                }

                $scope.checkMileageAvailable = function(mileage) {
                    if($scope.financeData && typeof $scope.financeData.parameters !== 'undefined') {
                        return (mileage*($scope.params.period/12)) <= $scope.financeData.parameters.annualMileageMaximum
                    } else {
                        return true;
                    }
                }

                $scope.checkPeriodAvailable = function(period) {
                    if($scope.financeData && typeof $scope.financeData.parameters !== 'undefined') {
                        return ($scope.params.mileage*(period/12)) <= $scope.financeData.parameters.annualMileageMaximum
                    } else {
                        return true;
                    }
                }



                // =================================================
                // ===             PRIVATE FUNCTIONS             ===
                // =================================================
                var getURLRange = function() {
                    var range = null;

                    angular.forEach($scope.ranges, function(r) {
                        if(r.rangeName === $scope.params.range) {
                            console.log('Found range from URL: ', r);
                            range = r;
                        }
                    });
                    return range;
                }

                var getURLModel = function() {
                    var model = null;

                    angular.forEach($scope.range.models, function(m) {
                        if(m.name === $scope.params.model) {
                            console.log('Found model from URL: ', m);
                            model = m;
                        }
                    });
                    return model;
                }

                var getURLTrim = function() {
                    var trim = null;

                    angular.forEach($scope.model.trimlines, function(trimline) {
                        if (trimline.name === $state.params.trim) {
                            console.log('Found trim from URL: ', trimline);
                            trim = trimline;
                        }
                    });
                    return trim;
                }

                var getURLEngine = function() {
                    var URLEngine = null;

                    angular.forEach($scope.trim.engines, function(engine) {
                        if (engine.modelCode === $state.params.modelCode) {
                            console.log('Found engine/modelCode from URL: ', engine);
                            URLEngine = engine;
                        }
                    });
                    return URLEngine;
                }

                var getCheapestTrim = function() {
                    var cheapest = null,
                    i;

                    if($scope.model.trimlines && $scope.model.trimlines.length) {
                        cheapest = $scope.model.trimlines[0];
                        angular.forEach($scope.model.trimlines, function(trim) {
                            if(trim.otrPriceMin < cheapest.otrPriceMin) {
                                cheapest = trim;
                            }
                        });
                    }
                    return cheapest;
                }

                var getCheapestEngine = function() {
                    var cheapest = null,
                    i;

                    if($scope.trim.engines && $scope.trim.engines.length) {
                        cheapest = $scope.trim.engines[0];
                        angular.forEach($scope.trim.engines, function(engine) {
                            if(parseFloat(engine.otrPriceMin) < parseFloat(cheapest.otrPriceMin)) {
                                cheapest = engine;
                            }
                        });
                    }
                    return cheapest;
                }

                // This is no longer used - server gives us default deposit
                /*var getDefaultDeposit = function() {
                    return Math.ceil(($scope.engine.otrPriceMin / 100) * 10);
                }*/

                var calculatePeriodRange = function() {
                	var data = $scope.financeData,
                        currentVal = parseInt(data.parameters.periodMinimum) || 18,
                        maxVal = parseInt(data.parameters.periodMaximum) || 48,
                        incr = parseInt(data.parameters.periodIncrement) || 6;

                    // Set new repayment period range (buttons)
                    $scope.settings.period = [];
                    while(currentVal <= maxVal) {
                    	$scope.settings.period.push(currentVal);
                    	currentVal+=incr;
                    }

                    // Set new repayment period range (slider)
                    /*$scope.settings.periodMin = parseInt(data.parameters.periodMinimum) || 18;
                    $scope.settings.periodMax = parseInt(data.parameters.periodMaximum) || 48;
                    $scope.settings.periodStep = parseInt(data.parameters.periodIncrement) || 6;*/
            	}

            	var calculateMileageRange = function() {
                	var data = $scope.financeData,
                        currentVal = parseInt(data.parameters.annualMileageMinimum) || 5,
                        maxVal = parseInt(data.parameters.annualMileageMaximum) || 30,
                        incr = parseInt(data.parameters.annualMileageIncrement) || 5;

                    // Set new mileage range (buttons)
                    $scope.settings.mileage = [];
                    while(currentVal <= maxVal) {
                    	$scope.settings.mileage.push(currentVal);
                    	currentVal+=incr;
                    }

                    // Set new mileage range (slider)
                    /*$scope.settings.mileageMin = parseInt(data.parameters.annualMileageMinimum) || 5;
                    $scope.settings.mileageMax = parseInt(data.parameters.annualMileageMaximum) || 30;
                    $scope.settings.mileageStep = parseInt(data.parameters.annualMileageIncrement) || 5;*/
            	}

                var getHighestAvailablePeriod = function(mileage) {
                    return parseInt((($scope.financeData.parameters.annualMileageMaximum / mileage) * 12) / $scope.settings.periodIncr) * $scope.settings.periodIncr;
                }

                var getHighestAvailableMileage = function(period) {
                    return parseInt(($scope.financeData.parameters.annualMileageMaximum / (period/12)) / $scope.settings.mileageIncr) * 1000;
                }

                var serialize = function(obj) {
                    var str = [];
                    for(var p in obj)
                        if (obj.hasOwnProperty(p)) {
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        }
                    return str.join("&");
                }

                $scope.init();
            }
        ]);


        function isEmpty(obj) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key))
                    return false;
            }
            return true;
        }



        // =================================================
        // ===                DIRECTIVES                 ===
        // =================================================
        angular.module('financeCalculator').directive('ngEnter', function()
        {
            return function(scope, element, attrs)
            {
                element.bind("keydown keypress", function(event)
                {
                    if (event.which === 13)
                    {
                        scope.$apply(function()
                        {
                            scope.$eval(attrs.ngEnter);
                        });

                        event.preventDefault();
                    }
                });
            };
        });


        angular.module('financeCalculator').directive('currencyMask', function () {
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function (scope, element, attrs, ngModelController) {
					// Run formatting on keyup
					var numberWithCommas = function(value, addExtraZero) {
						if (addExtraZero == undefined) addExtraZero = false;
						value = value.toString();
						value = value.replace(/[^0-9\.]/g, "");
						var parts = value.split('.');
						parts[0] = parts[0].replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, "$&,");

						if (parts[1] && parts[1].length > 2) {
							parts[1] = parts[1].substring(0, 2);
						}
						if (addExtraZero && parts[1] && (parts[1].length === 1)) {
							parts[1] = parts[1] + "0"
						}

						if(typeof parts[1] !== 'undefined') {
							return [parts[0], parts[1]].join(".");
						} else {
							return parts[0];
						}
					};
					var fixPennies = function(value) {
						value = value.toString();
						value = value.replace(/[^0-9\.]/g, "");
						var parts = value.split('.');
						parts[0] = parts[0].replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, "$&,");

						if (parts[1] && parts[1].length > 2) {
							parts[1] = parts[1].substring(0, 2);
						}
						else if (parts[1] && (parts[1].length === 1)) {
							parts[1] = parts[1] + "0"
						}
						else if(!parts[1] || !parts[1].length) {
							parts[1] = "00";
						}

						return parts.join(".");
					}
					var applyFormatting = function() {
						var value = element.val();
						var original = value;
						if (!value || value.length == 0) { return }
						value = numberWithCommas(value);
						if (value != original) {
							element.val(value);
							element.triggerHandler('input')
						}
					};
					var applyPennyFormatting = function() {
						var value = element.val();
						var original = value;
						if (!value || value.length == 0) { return }
						value = fixPennies(numberWithCommas(value));
						if (value != original) {
							element.val(value);
							element.triggerHandler('input')
						}
					};
					element.bind('keyup', function(e) {
						var keycode = e.keyCode;
						var isTextInputKey =
							(keycode > 47 && keycode < 58)   || // number keys
							keycode == 32 || keycode == 8    || // spacebar or backspace
							(keycode > 64 && keycode < 91)   || // letter keys
							(keycode > 95 && keycode < 112)  || // numpad keys
							(keycode > 185 && keycode < 193) || // ;=,-./` (in order)
							(keycode > 218 && keycode < 223);   // [\]' (in order)
						if (isTextInputKey) {
							applyFormatting();
						}
					});
					element.bind('blur', function(e) {
						applyPennyFormatting();
					});
					ngModelController.$parsers.push(function(value) {
						if (!value || value.length == 0) {
							return value;
						}
						value = value.toString();
						value = value.replace(/[^0-9\.]/g, "");
						return value;
					});
					ngModelController.$formatters.push(function(value) {
						if (!value || value.length == 0) {
							return value;
						}
						value = numberWithCommas(value, true);
						return value;
					});
				}
			};
		});


    	// Used to validate comma separated email list on email modal
		var MULTI_EMAIL_REGEXP = /^(([a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*)( )?(,)?( )?)+$/;
		angular.module('financeCalculator').directive('emaillist', function() {
			return {
				require: 'ngModel',
				link: function(scope, elm, attrs, ctrl) {
					ctrl.$parsers.unshift(function(viewValue) {
						if (MULTI_EMAIL_REGEXP.test(viewValue)) {
							// it is valid
							ctrl.$setValidity('emaillist', true);
							return viewValue;
						} else {
							// it is invalid, return undefined (no model update)
							ctrl.$setValidity('emaillist', false);
							return undefined;
						}
					});
				}
			};
		});


		angular.module('financeCalculator').directive('fixie8altrows', function() {
			return {
				restrict: 'A',
				replace: false,
				link: function (scope, element, attrs) {
					scope.$watchCollection(
						function () {
							return element[0].getElementsByTagName("tr");
						},
						function (newRows, oldRows) {
							var rows = element[0].getElementsByTagName("tr"),
							i=0;

							for(i=0;i<rows.length;i++) {
								angular.element(rows[i]).addClass((i%2===0 ? 'even' : 'odd'));
								angular.element(rows[i]).removeClass((i%2===0 ? 'odd' : 'even'));
							}
						}
					);
				}
			}
		})


        String.prototype.splice = function(idx, rem, s) {
            return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
        };

