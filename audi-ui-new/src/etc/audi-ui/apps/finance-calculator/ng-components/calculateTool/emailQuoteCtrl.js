
    angular.module('financeCalculator').controller('emailQuoteCtrl', ['$scope', 'calculatorService', '$modalInstance', '$window', 'params', 'arrangeTestDrive', 'locateACentre', 'parentScopeData',
        function($scope, calculatorService, $modalInstance, $window, params, arrangeTestDrive, locateACentre, parentScopeData) {
        	$scope.mode = 'input';
        	$scope.busy = false;
        	$scope.formData = {
        		recipientName: null,
        		recipientEmail: null
        	}
        	$scope.errorMessage = null;
        	$scope.params = params;
        	$scope.arrangeTestDrive = arrangeTestDrive;
        	$scope.locateACentre = locateACentre;
        	$scope.parentScopeData = parentScopeData;

        	$scope.init = function() {

        	};

			$scope.send = function(form) {
				form.submitted = true;
				if(form.$valid && !$scope.busy) {
					$scope.busy = true;
					calculatorService.sendQuoteEmail(angular.extend($scope.formData, $scope.params))
					.then(function(resp) {
						// Success handler
						$scope.mode = 'success';
						$scope.busy = false;
					}, function(resp){
						// Error handler
						$scope.mode = 'error';
						$scope.busy = false;
					});
				}
			};

			$scope.close = function () {
				$modalInstance.dismiss('cancel');
			};
        }
    ]);

