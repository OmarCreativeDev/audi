
    angular.module('financeCalculator').controller('termsCtrl', ['$scope', '$modalInstance', 'terms',
        function($scope, $modalInstance, terms) {
            $scope.terms = terms;
            $scope.currentSection = $scope.terms.length ? $scope.terms[0] : null;

            $scope.init = function() {

            };

            $scope.changeSection = function(section) {
                $scope.currentSection = section;
            }

            $scope.close = function () {
                $modalInstance.dismiss('cancel');
            };
        }
    ]);


