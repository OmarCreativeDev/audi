

        angular.module('financeCalculator').controller('errorCtrl', ['$scope', '$rootScope', '$state', '$window', 'taggingService',
            function($scope, $rootScope, $state, $window, taggingService) {
                $scope.arrangeTestDrive = function() {
                    $window.open("/content/audi/about-audi/contact-us/arrange-a-test-drive/test-drive-form.html");
                }

                $scope.backToRanges = function() {
                	$state.go('ranges');
                }

                $scope.findLocalDealer = function() {
                    $window.open("/content/audi/locate-a-centre.html");
                }

                $scope.learnMore = function() {
                    $window.open("/content/audi/explore-models/finance.html");
                }

                taggingService.financeCalculatorError();
            }
        ]);
