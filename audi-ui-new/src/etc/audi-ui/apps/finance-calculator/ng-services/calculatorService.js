
        angular.module('financeCalculator').factory('calculatorService', ['$resource', '$http', '$q', '$httpBackend', '$window',
          function($resource, $http, $q, $httpBackend, $window) {

            var financeData = {};
            var _ranges = [];

            var _getRanges = function() {
              var deferred = $q.defer();
              if (!_ranges.length) {
                return _fetchRanges(deferred);
              }
              else {
                deferred.resolve();
                return deferred.promise;
              }
            }


            var _fetchRanges = function(deferred) {
              $http({
                method: 'GET',
                url: financeCalculator.dataSources.rangeData,
              })
              .success(function(result, status) {
                _ranges = angular.copy(result.data, _ranges);
                deferred.resolve(result, status);
              })
              .error(function(result, status) {
                deferred.reject(result, status);
              });

              return deferred.promise;
            };


            var _getQuote = function(params, getPDF, reset) {
              console.log('Getting quote using params:', params);
              var deferred = $q.defer();
              var deferredAbort = $q.defer();
              var aborted = false;

              if(reset) {
              	angular.copy({}, financeData);
              }

              if(getPDF) {
                $window.open(financeCalculator.dataSources.downloadPDF + "?" + serialize(params));
              }
              else {
                var req = $http({
                  method: 'GET',
                  url: financeCalculator.dataSources.quoteData,
                  params: angular.extend({nocache: Math.random()}, params),
                  dataType: "json",
                  timeout: deferredAbort.promise
                })
                .success(function(result, status) {
                  if(result.responseInfo.desc.toUpperCase() === 'SUCCESS') {
                    console.log('Got quote data:', result.data);
                    angular.copy(result.data, financeData);
                    deferred.resolve({result: result, status: status});
                  }
                  else {
                    console.log('Received valid quote response, but server reported an error', result);
                    deferred.reject({result: result, status: status});
                  }
                })
                .error(function(result, status) {
                  if(aborted) {
                    console.log('Aborted last quote request');
                    deferred.reject({result: 'ABORTED', status: status});
                  }
                  else {
                    console.log('Serious error while getting quote data', result);
                    deferred.reject({result: result, status: status});
                  }
                });
              }

              // Add abort function to promise object
              deferred.promise.abort = function() {
                aborted = true;
                deferredAbort.resolve();
              }

              return deferred.promise;
            }


            var _sendQuoteEmail = function(params) {
              console.log('Sending email quote using params:', params);
              var deferred = $q.defer();

              $http({
                method: 'GET',
                url: financeCalculator.dataSources.sendQuoteEmail,
                params: params
              })
              .success(function(result, status) {
                if(result.responseInfo.desc.toUpperCase() === 'SUCCESS') {
                  console.log('Email quote sent successfully');
                  deferred.resolve(result, status);
                } else {
                  console.log('Handled error while sending quote', result);
                  deferred.reject(result, status);
                }
              })
              .error(function(result, status) {
                console.log('Serious error while sending quote', result, status);
                deferred.reject(result, status);
              });

              return deferred.promise;
            }


            var _getCQ5Properties = function() {
              console.log('Attempting to get CQ5 properties...');
              var deferred = $q.defer();

              $http({
                method: 'GET',
                url: financeCalculator.dataSources.CQ5Properties,
              })
              .success(function(result, status) {
                var props = decodeCQ5Properties(result);
                console.log('Got CQ5 properties successfully', props);
                deferred.resolve(props);
              })
              .error(function(result, status) {
                console.log('Error getting CQ5 properties');
                deferred.reject(result, status);
              });

              return deferred.promise;
            };


            var serialize = function(obj) {
              var str = [];
              for(var p in obj)
                if (obj.hasOwnProperty(p)) {
                  str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                }
              return str.join("&");
            }


            var decodeCQ5Properties = function(htmlString) {
                var dictionary = $(htmlString).filter('.dictionary'),
                dictionaryItems = dictionary.length ? $(dictionary[0]).find('.dictionary-item') : [],
                re = /^([a-zA-Z]+)(\d+)?_?(.+)?$/,
                props = {},
                arrayIndexMap = {},
                keyObj, key, value, parts, mappedIndex, i;

                for(i=0; i<dictionaryItems.length; i++) {
                  keyObj = $(dictionaryItems[i]).find('.key');
                  if(!keyObj.length) continue;
                  key = keyObj.text();

                  if(re.test(key)) {
                    parts = re.exec(key);
                    value = $(dictionaryItems[i]).find('.value');

                    // Check if this is an array entry
                    // Need the lengths checks here due to IE8 making umatched parts an empty string, where as other browsers make them undefined.
                    if((typeof parts[2] !== 'undefined' && parts[2].length) && (typeof parts[3] !== 'undefined' && parts[3].length)) {
                      // Check array exists, if not, create it.
                      if(!$.isArray(props[parts[1]])) {
                        props[parts[1]] = [];
                      }

                      // Update index map
                      if(typeof arrayIndexMap[parts[1]] === 'undefined') {
                        arrayIndexMap[parts[1]] = {};
                      }
                      if(typeof arrayIndexMap[parts[1]][parts[2]] === 'undefined') {
                        mappedIndex = arrayIndexMap[parts[1]][parts[2]] = props[parts[1]].length;
                      } else {
                        mappedIndex = arrayIndexMap[parts[1]][parts[2]];
                      }

                      // check array item exists - if not, create it. Then save value.
                      if(typeof props[parts[1]][mappedIndex] === 'undefined') {
                        props[parts[1]][mappedIndex] = {};
                        props[parts[1]][mappedIndex]['id'] = parts[2];
                      }
                      props[parts[1]][mappedIndex][parts[3]] = value.text();
                      continue;
                    }

                    // Check if object literal entry
                    if(typeof parts[2] !== 'undefined' && parts[2].length) {
                      // Check object exists, if not, create it. Then save value.
                      if(!$.isArray(props[parts[1]])) props[parts[1]] = {};
                      props[parts[1]][parts[2]] = value.text();
                      continue;
                    }

                    // If we get here, its just a regular property
                    props[parts[1]] = value.text();
                  }
                }
                return props;
            }


            var setupMocks = function() {

              /*$httpBackend.whenPOST("api/calcData").respond(function(method, url, data) {
                data = angular.fromJson(data);
                console.log('mock', data)
                return [200, {
                  "status": "OK",
                  "stats": {
                    deposit: data.deposit,
                    mileage: data.mileage,
                    duration: data.duration
                  }
                }, {}];
              });*/ //NO LONGER USED

              $httpBackend.whenPOST("api/sendQuoteEmail").respond(function(method, url, data) {
                console.log('Using mock data for sendQuoteEmail request');
                return [200, {"status": "OK"}, {}];
                // return [500, {"status": "FAIL", msg: "Run for the hills!"}, {}];
              });

            }
            //setupMocks();

            return {
              financeData  : financeData,
              ranges       : _ranges,

              getQuote          : _getQuote,
              getRanges         : _getRanges,
              getCQ5Properties  : _getCQ5Properties,

              sendQuoteEmail : _sendQuoteEmail
            }
          }
        ]);

