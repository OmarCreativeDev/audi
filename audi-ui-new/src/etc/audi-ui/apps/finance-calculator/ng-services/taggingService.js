  angular.module('financeCalculator').factory('taggingService', ['$window', '$injector', '$analytics',
    function($window, $injector, $analytics) {

      var modelSelectorLoaded = function() {
        var url = '/finance-calculator/range-selector';

        $analytics.pageTrack(url, {
          pageTitle: 'Finance Calculator - Model Selector',
          url: url,
          description: 'Finance calculator model selector page loaded'
        });
      }

      var financeCalculatorLoaded = function() {
        var $state = $injector.get('$state'),
            url = 'finance-calculator/' + (($state.params.productType === 'SOLUTIONS' || $state.params.productType === null) ? 'personal-contract-plan' : 'hire-purchase'),
            mode = ($state.params.productType && $state.params.productType === 'HIRE_PURCHASE') ? 'Hire Purchase' : 'Personal Contract Plan';

        $analytics.pageTrack(url, {
          pageTitle: 'Finance Calculator - ' + mode,
          url: url,
          description: 'Finance calculator quote page loaded'
        });
      }

      var financeCalculatorError = function() {
        var url = 'finance-calculator/error';

        $analytics.pageTrack(url, {
          pageTitle: 'Finance Calculator - Error',
          url: url,
          description: 'Finance calculator error page encountered'
        });
      }

      var invalidDeposit = function(deposit) {
        $analytics.eventTrack('invalidDeposit', null, {
          invalidDeposit: deposit ? deposit.toString() : 'null',
          description: 'Finance calculator - invalid deposit has been input'
        });
      }

      var quoteRefresh = function(filterString) {
        $analytics.eventTrack('quoteRefresh', null, {
          quoteRefreshedFinanceCalculatorFilters: filterString,
          eventQuoteRefreshed: null,
          description: 'Finance calculator - quote refreshed'
        });
      }


      return {
        modelSelectorLoaded     : modelSelectorLoaded,
        financeCalculatorLoaded : financeCalculatorLoaded,
        financeCalculatorError  : financeCalculatorError,
        invalidDeposit          : invalidDeposit,
        quoteRefresh            : quoteRefresh
      }
    }
  ]);

