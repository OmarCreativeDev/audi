$(document).ready(function() {
    
    console.log('parallax loaded')

    $('[data-type="parallax"]').each(function() {
    	   
    	//Assigning the object   
        var $parallaxContainer = $(this); 
    
        $(window).scroll(function() {

    		// console.log('running');
    		// console.log('parallaxPosition', parallaxPosition);
    		
    		//Setting coordinates based on HTML data
            var yPos = -($(window).scrollTop() / $parallaxContainer.data('speed'));
            
            //Put together our final background position
            var coords = yPos + 'px';           

            //Move the background
            $parallaxContainer.css({ bottom: coords });
        }); 
    });    
});


