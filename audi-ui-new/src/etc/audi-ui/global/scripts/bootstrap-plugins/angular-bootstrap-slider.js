/******************************************************************
 * Taken from https://github.com/seiyria/angular-bootstrap-slider *
 * I cant use the bower package as it breaks the build!           *
 ******************************************************************/

angular.module('ui.bootstrap-slider', [])
	.directive('slider', ['$parse', '$timeout', function ($parse, $timeout) {
		return {
			restrict: 'AE',
			replace: true,
			template: '<input type="text" />',
			require: 'ngModel',
			scope:{
				'sliderid': '=',
				'min':'=',
				'max':'=',
				'step':'=',
				'precision':'=',
				'orientation':'=',
				'value':'=',
				'range':'=',
				'selection':'=',
				'tooltips':'=',
				'tooltipseparator':'=',
				'tooltipsplit':'=',
				'handle':'=',
				'reversed':'=',
				'enabled':'=',
				'naturalarrowkeys':'=',
				'formater':'=',
				'ngModel':'=', // Contains what the slider is set currently at (when mouse is released)
				'tempslidervalue':'=', // Contains what the slider is currently at, but not set to yet (while mouse button held)
				'updatevent':'=',
				'commitEvent':'=',
				'unavailableStart':'=' // Used to mark where the forbidden region at the high end of the slider starts
			},

			/*controller: function($scope, $element, $attrs){
				console.log('attrs:+++++++',$attrs)
				$scope.$watch('settings.mileageMax', function(value) {
					console.log('==================slider updated');
					$($element[0]).slider('recalculateRange');
				});
			},*/
			link: function ($scope, element, attrs, ngModelCtrl) {

				$.fn.slider.Constructor.prototype.disable = function () {
					this.picker.off();
				};

				$.fn.slider.Constructor.prototype.enable = function () {
					this.picker.on();
				};

                if ($scope.ngChange) {
                    ngModelCtrl.$viewChangeListeners.push(function() {
                        $scope.$apply($scope.ngChange);
                    });
                }
                if (typeof $scope.tempslidervalue !== 'undefined') {
                    ngModelCtrl.$viewChangeListeners.push(function() {
                        $scope.$apply($scope.tempslidervalue);
                    });
                }

                var keys = [
                	'sliderid', 'min', 'max', 'step', 'precision', 'orientation',
					'value', 'range', 'selection', 'tooltips', 'tooltipseparator',
					'tooltipsplit', 'handle', 'reversed', 'enabled', 'naturalarrowkeys',
					'formater', 'unavailableStart'
				];
				var passedValues = {};
				var i = 0;
				for(i=0; i<keys.length; i++) {
					if($scope[keys[i]]) passedValues[keys[i]] = $scope[keys[i]];
					else if(attrs[keys[i]]) passedValues[keys[i]] = attrs[keys[i]];
				}

                var options = {};
				if(passedValues.sliderid) options.id = passedValues.sliderid;
				if(passedValues.min) options.min = parseFloat(passedValues.min);
				if(passedValues.max) options.max = parseFloat(passedValues.max);
				if(passedValues.unavailableStart) options.unavailableStart = parseFloat(passedValues.unavailableStart);
				if(passedValues.step) options.step = parseFloat(passedValues.step);
				if(passedValues.precision) options.precision = parseFloat(passedValues.precision);
				if(passedValues.orientation) options.orientation = passedValues.orientation;
				if(passedValues.value) {
					if (angular.isNumber(passedValues.value) || angular.isArray(passedValues.value)) {
						options.value = passedValues.value;
					} else if (angular.isString(passedValues.value)) {
						if (passedValues.value.indexOf("[") === 0) {
							options.value = angular.fromJson(passedValues.value);
						} else {
							options.value = parseFloat(passedValues.value);
						}
					}

				}
				if(passedValues.range) options.range = passedValues.range === 'true';
				if(passedValues.selection) options.selection = passedValues.selection;
				if(passedValues.tooltips) options.tooltip = passedValues.tooltips;
				if(passedValues.tooltipseparator) options.tooltip_separator = passedValues.tooltipseparator;
				if(passedValues.tooltipsplit) options.tooltip_split = passedValues.tooltipsplit === 'true';
				if(passedValues.handle) options.handle = passedValues.handle;
				if(passedValues.reversed) options.reversed = passedValues.reversed === 'true';
				if(passedValues.enabled) options.enabled = passedValues.enabled === 'true';
				if(passedValues.naturalarrowkeys) options.natural_arrow_keys = passedValues.naturalarrowkeys === 'true';
                if(passedValues.formater) options.formater = passedValues.$eval(passedValues.formater);

				if (options.range && !options.value) {
					options.value = [0,0]; // This is needed, because of value defined at $.fn.slider.defaults - default value 5 prevents creating range slider
				}

				var slider = $(element[0]).slider(options);
				var updateEvent = $scope.updateevent || 'slide';
				var commitEvent = $scope.commitevent || 'slideStop';

				// Sets the temp slider value on 'slide' (slider moved, mouse held down)
				slider.on(updateEvent, function(ev) {
					$scope.tempslidervalue = ev.value;
					$scope.$apply();
				});

				// Sets the slider value on 'slideStop' (mouseUp)
				slider.on(commitEvent, function(ev) {
					$scope.tempslidervalue = ev.value;
					ngModelCtrl.$setViewValue(ev.value);
					$timeout(function() {
						$scope.$apply();
					});
				});

				// Watches for changes to ngModel value and updates slider
				$scope.$watch('ngModel', function(value) {
					if(value || value === 0) {
						slider.slider('setValue', value, false);
						$scope.tempslidervalue = value;
					}
				});

				// Watches for changes to min, max and step params redraws slider
				$scope.$watch('min + max + step', function(value) {
					slider.slider('recalculateRange', {
						min: $scope.min || passedValues.min,
						max: $scope.max || passedValues.max,
						step: $scope.step || passedValues.step
					});
				});

				// Watches for changes to the unavailable region and redraws that
				$scope.$watch('unavailableStart', function(value) {
					slider.slider('recalculateUnavailableRegion', {
						unavailableStart: $scope.unavailableStart
					});
				});

				// Watches for changes to ngDisabled, and disables/enables slider
				if (angular.isDefined($scope.ngDisabled)) {
					$scope.$watch($scope.ngDisabled, function(value) {
						if (value) {
							slider.slider('disable');
						} else {
							slider.slider('enable');
						}
					});
				}
			}
		};
	}])
;
