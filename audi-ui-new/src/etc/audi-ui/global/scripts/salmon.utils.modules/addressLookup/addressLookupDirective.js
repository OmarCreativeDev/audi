

	angular.module('sa.utils.modules.addressLookup',[])


	.directive("addressLookup", [ '$modal',
		function($modal) {
			return {
				restrict: "EA",
				templateUrl: "/etc/audi-ui/global/scripts/salmon.utils.modules/addressLookup/addressLookupTemplate.html",
		
				controller: function ($scope) {
						  // -------------------------------------
            // Change address section
            // -------------------------------------

            $scope.editAddrManualClick = function() {
                hideAddrLookup();
                showAddrEdit();
            }

            $scope.goToLookupClick = function() {
                hideAddrEdit();
                showAddrLookupStep1();
            }

            $scope.editAddrManualFromLookupClick = function() {
                console.log("editAddrManualFromLookupClick()");
                var selectedAddr = $scope.sections.address.lookupForm.selectedAddr;
                $scope.sections.address.data = profileService.getAddressFromLookupData( selectedAddr );
                hideAddrLookup();
                showAddrEdit();
            }

            var changeAddrInit = function() {
                if ( profileService.isAddressEmpty() ) {
                    showAddrLookupStep1();
                } else {
                    showAddrEdit();
                }
                
            }

            var showAddrLookupStep1 = function() {
                resetFormStatus('addrLookup1');
                $scope.sections.address.lookupForm.data = {};
                $scope.sections.address.lookupForm.step1.visible = true;
            }

            var showAddrLookupStep2 = function( addresses ){

                var model = $scope.sections.address.lookupForm;

                model.foundAddresses = addresses;
                model.selectedAddr = getAddrByHouse( model.foundAddresses ) || model.foundAddresses[0];

                $scope.sections.address.lookupForm.step2.visible = true;
                $scope.focusAddrSelect = true;
            }

            var showAddrEdit = function() {
                $scope.addressLookupEditMode.visible = true;
            }
            var hideAddrEdit = function() {
                $scope.addressLookupEditMode.visible = false;
            }

            var hideAddrLookupStep1 = function() {
                $scope.sections.address.lookupForm.step1.visible = false;
            }
            var hideAddrLookupStep2 = function() {
                $scope.sections.address.lookupForm.step2.visible = false;
            }
            var hideAddrLookup = function() {
                hideAddrLookupStep1();
                hideAddrLookupStep2();
            }

            var getAddrByHouse = function( addresses ) {

                var model = $scope.sections.address.lookupForm.data;

                if ( typeof model.address1 == 'undefined' )
                    return false

                var houseNum  = model.address1.replace(/[^\d.]/g, '').trim(),
                    houseName = model.address1.replace(/[0-9]/g, '').trim();

                if ( houseNum.length ) {
                    for (var i = 0; i < addresses.length; i++) {
                        if ( addresses[i].houseFlatNumber.indexOf( houseNum ) > -1 )
                            return addresses[i];
                    }
                } else if ( houseName.length ) {
                    houseName = houseName.toLowerCase();
                    for (var i = 0; i < addresses.length; i++) {
                        if ( addresses[i].premise.toLowerCase().indexOf( houseName ) > -1 )
                            return addresses[i];
                    }
                }

                return false;
            }

            $scope.doLookupAddressStep1 = function(){


                if ( $scope.addrLookup1.$valid ) {

                    hideAddrLookupStep2();

                    profileService.lookupAddress( $scope.profileData.postcode )
                        .success(function(data, status) {

                            console.log("address lookup sucess", data);

                            angular.forEach(data, function(val, key) {
                                val.ID = key+1;
                                val.full = val.simpleAddress + ', ' + val.postCode.toUpperCase();
                            });

                            showAddrLookupStep2( data );
                        })
                        .error(function(data, status) {
                            console.log("address lookup failed");
                        });
                    
                } else {
                    $scope.addrLookup1.submitted = true;
                }
            }

                
      var getAddressFromLookupData = function( data ) {
        var addr1 = data.houseFlatNumber,
            addr2 = data.street;

        if ( data.premise.length ) {
          addr1 = data.premise;
          addr2 = data.houseFlatNumber + " " + data.street;
        }

        var newData = {
          "address1": addr1,
          "address2": addr2,
          "city":     data.town,
          "postcode": data.postCode,
          "county":   data.county
        }
        angular.extend($scope.personalInfoForm,newData);

    //    return newData;
      }


            $scope.doLookupAddressStep2 = function(){
                console.log("lookupAddressStep2");


                getAddressFromLookupData($scope.sections.address.lookupForm.selectedAddr)
                hideAddrLookup();
                hideAddrEdit();

            }


				}
			}
		

		}])


