/*! globalTagging.js */

/*	-------------------
*	Function to pass prop/eVars/events to Site Catalyst for reporting.
*
*
*	Usage
salmon.globalTagging.getTrackingProperties(
	'click', 				//	Tracking request type: load/click [s.t()/s.tl()]
	'action', 				//	Used with [load:'click'], link/action [link clicked to navigate away/JS click event without navigation]
	'o',					//	'o' - Omniture's Custom Link property is the option to go for (others are 'd' for download or 'e' for exit link)
	'Your link name',		//	Omniture's Custom Link Title stored in the element's properties Hash
	{
	'props':{
		'prop1':'Some value',
		'prop12':'Some other value'
	},
	'eVars':{
		'eVar13':'prop1',
		'eVar14':'Some other value again'
	},
	'events':{
		'event12':'',
		'event14':''
	},
	'other': {
		'pageName' : 'My example page title'
	}
})
	salmon.globalTagging.getTrackingProperties('click','action','o','Your link name',{'props':{'prop1':'Some value','prop12':'Some other value'},'eVars':{'eVar13':'prop1','eVar14':'Some other value again'},'events':{'event12':'','event14':''}});
*/

var salmon = salmon || {};
salmon.globalTagging = (new function(){
	this.checkLastChar = function(checkString,trimChar) {
		if (checkString.substring(checkString.length-1) == trimChar){
			checkString = checkString.substring(0,checkString.length-1);
		}
		return checkString;
	};

	this.evalPropVal = function(propVal){
		if ((propVal.indexOf('prop') > -1) || (propVal.indexOf('eVar') > -1)) {
			return s[propVal];
		} else {
			return propVal;
		}
	};

	this.getTrackingProperties = function(tlEvent,tlMethod,tlType,customLinkTitle,trackingObj){
		var linkTrackVars = '';
		var linkTrackEvents = '';
		s = s_gi(s_account); 	// Set Omniture account
		for (var key in trackingObj) {
			if (trackingObj.hasOwnProperty(key)) {
				var obj = trackingObj[key];
				for (var prop in obj) {
					if ((obj.hasOwnProperty(prop)) && (prop.indexOf('event') == -1)) {
						linkTrackVars += prop + ','; // Build s.linkTrackVars for props/eVars
						s[prop] = salmon.globalTagging.evalPropVal(obj[prop]);
					} else if ((obj.hasOwnProperty(prop)) && (prop.indexOf('event') > -1)) {
						linkTrackEvents += prop + ',';
					}
				}
				if (key == 'events') { // Add 'events' to s.linkTrackVars if events key present
					linkTrackVars += 'events' + ',';
				}
			}
		}
		s.linkTrackEvents = s.events = salmon.globalTagging.checkLastChar(linkTrackEvents,',');
		s.linkTrackVars = salmon.globalTagging.checkLastChar(linkTrackVars,',');

		if (tlEvent == 'load') {
			var s_code = s.t();
			if (s_code) document.write(s_code);
		} else if (tlEvent == 'click') {
			switch (tlMethod) {
				case "action":
					s.tl(this,tlType,customLinkTitle,null,null);
					break;
				case "link":
					s.tl(true,tlType,customLinkTitle,null,null);
					break;
			}
		}
	};
});