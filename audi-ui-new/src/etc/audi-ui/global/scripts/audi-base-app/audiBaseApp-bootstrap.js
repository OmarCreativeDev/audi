define([
	'ng',
	'sa-utils',
	'modelCarousel',
	'modelCarousel-config',
	
	'modelCarousel-Service',
	'modelCarousel-Ctrl',
	'microfiche_carousel',

	'omniture-scode',
	'omniture-global-tagging'



], function (angular, app) {

	'use strict';

	var el = document.getElementById('modelCarousel');

	angular.element(el).ready(function() {
		angular.bootstrap(el, ['modelCarousel']);
	});

});
