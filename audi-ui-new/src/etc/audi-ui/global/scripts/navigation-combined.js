/* Audi.co.uk
*  rf.namespace.js
*  Razorfish Namespace javascript
*  Author: Razorfish, London
*  Date: 11-11-2009 */

var RF = 			{}; // Our RF empty namespace
RF.Moo = 			{}; // Any additional Moo specific objects
RF.Browser = 		{}; // All browser specific objects
RF.Browser.ie, RF.Browser.ie6 = false;

/*@cc_on
	@if (@_jscript_version >= 5.0)
		RF.Browser.ie = true;
		
   	@end
@*/
// Test for SP3 IE6
RF.Browser.ie   = /msie/i.test(navigator.userAgent);
RF.Browser.ie6  = /msie 6/i.test(navigator.userAgent);
RF.Browser.ie7  = /msie 7/i.test(navigator.userAgent);
RF.Browser.ie8  = /msie 8/i.test(navigator.userAgent);
// Test for iPad
RF.Browser.iPad = navigator.userAgent.match(/iPad/i) != null;

RF.Global = 		{}; // Global sitewide objects
RF.SWF = 			{}; // All SWF related objects
RF.SWF.Showroom = 	{}; // The Showroom object
RF.Tracking = 		{}; // All tracking and analytics related objects
RF.Tracking.Showroom = {}; // Tracking and analytics for the Showroom & Visualizer
RF.Experience = 	{}; // All RF experience related objects, e.g. journeyType (Gold / Bronze)
RF.Site = 			{}; // All site specific objects, e.g. currentPage
RF.Class = 			{}; // All RF Classes
RF.Class.Showroom = {}; // All Showroom Classes
RF.Class.Audi = 	{};	//	All Audi visualiser classes
RF.Init = 			{}; // instantiate classes in here
RF.Init.API =		{}; // instantiate classes from flash/javascript interface
RF.Campaign = 		{};	//	All campaign code that does not need to be a class

var SI_CBW = {}; // STYLING FILE INPUTS 1.0 | Shaun Inman <http://www.shauninman.com/> | 2007-09-07 - updated by ChrisBWard;//MooTools, <http://mootools.net>, My Object Oriented (JavaScript) Tools. Copyright (c) 2006-2009 Valerio Proietti, <http://mad4milk.net>, MIT Style License.
var MooTools = {
    version: "1.2.4",
    build: "0d9113241a90b9cd5643b926795852a2026710d4"
};
var Native = function (k) {
    k = k || {};
    var a = k.name;
    var i = k.legacy;
    var b = k.protect;
    var c = k.implement;
    var h = k.generics;
    var f = k.initialize;
    var g = k.afterImplement || function () {};
    var d = f || i;
    h = h !== false;
    d.constructor = Native;
    d.$family = {
        name: "native"
    };
    if (i && f) {
        d.prototype = i.prototype;
    }
    d.prototype.constructor = d;
    if (a) {
        var e = a.toLowerCase();
        d.prototype.$family = {
            name: e
        };
        Native.typize(d, e);
    }
    var j = function (n, l, o, m) {
        if (!b || m || !n.prototype[l]) {
            n.prototype[l] = o;
        }
        if (h) {
            Native.genericize(n, l, b);
        }
        g.call(n, l, o);
        return n;
    };
    d.alias = function (n, l, p) {
        if (typeof n == "string") {
            var o = this.prototype[n];
            if ((n = o)) {
                return j(this, l, n, p);
            }
        }
        for (var m in n) {
            this.alias(m, n[m], l);
        }
        return this;
    };
    d.implement = function (m, l, o) {
        if (typeof m == "string") {
            return j(this, m, l, o);
        }
        for (var n in m) {
            j(this, n, m[n], l);
        }
        return this;
    };
    if (c) {
        d.implement(c);
    }
    return d;
};
Native.genericize = function (b, c, a) {
    if ((!a || !b[c]) && typeof b.prototype[c] == "function") {
        b[c] = function () {
            var d = Array.prototype.slice.call(arguments);
            return b.prototype[c].apply(d.shift(), d);
        };
    }
};
Native.implement = function (d, c) {
    for (var b = 0, a = d.length; b < a; b++) {
        d[b].implement(c);
    }
};
Native.typize = function (a, b) {
    if (!a.type) {
        a.type = function (c) {
            return ($type(c) === b);
        };
    }
};
(function () {
    var a = {
        Array: Array,
        Date: Date,
        Function: Function,
        Number: Number,
        RegExp: RegExp,
        String: String
    };
    for (var h in a) {
        new Native({
            name: h,
            initialize: a[h],
            protect: true
        });
    }
    var d = {
        "boolean": Boolean,
        "native": Native,
        object: Object
    };
    for (var c in d) {
        Native.typize(d[c], c);
    }
    var f = {
        Array: ["concat", "indexOf", "join", "lastIndexOf", "pop", "push", "reverse", "shift", "slice", "sort", "splice", "toString", "unshift", "valueOf"],
        String: ["charAt", "charCodeAt", "concat", "indexOf", "lastIndexOf", "match", "replace", "search", "slice", "split", "substr", "substring", "toLowerCase", "toUpperCase", "valueOf"]
    };
    for (var e in f) {
        for (var b = f[e].length; b--;) {
            Native.genericize(a[e], f[e][b], true);
        }
    }
})();
var Hash = new Native({
    name: "Hash",
    initialize: function (a) {
        if ($type(a) == "hash") {
            a = $unlink(a.getClean());
        }
        for (var b in a) {
            this[b] = a[b];
        }
        return this;
    }
});
Hash.implement({
    forEach: function (b, c) {
        for (var a in this) {
            if (this.hasOwnProperty(a)) {
                b.call(c, this[a], a, this);
            }
        }
    },
    getClean: function () {
        var b = {};
        for (var a in this) {
            if (this.hasOwnProperty(a)) {
                b[a] = this[a];
            }
        }
        return b;
    },
    getLength: function () {
        var b = 0;
        for (var a in this) {
            if (this.hasOwnProperty(a)) {
                b++;
            }
        }
        return b;
    }
});
Hash.alias("forEach", "each");
Array.implement({
    forEach: function (c, d) {
        for (var b = 0, a = this.length; b < a; b++) {
            c.call(d, this[b], b, this);
        }
    }
});
Array.alias("forEach", "each");

function $A(b) {
    if (b.item) {
        var a = b.length,
            c = new Array(a);
        while (a--) {
            c[a] = b[a];
        }
        return c;
    }
    return Array.prototype.slice.call(b);
}

function $arguments(a) {
    return function () {
        return arguments[a];
    };
}

function $chk(a) {
    return !!(a || a === 0);
}

function $clear(a) {
    clearTimeout(a);
    clearInterval(a);
    return null;
}

function $defined(a) {
    return (a != undefined);
}

function $each(c, b, d) {
    var a = $type(c);
    ((a == "arguments" || a == "collection" || a == "array") ? Array : Hash).each(c, b, d);
}

function $empty() {}

function $extend(c, a) {
    for (var b in (a || {})) {
        c[b] = a[b];
    }
    return c;
}

function $H(a) {
    return new Hash(a);
}

function $lambda(a) {
    return ($type(a) == "function") ? a : function () {
        return a;
    };
}

function $merge() {
    var a = Array.slice(arguments);
    a.unshift({});
    return $mixin.apply(null, a);
}

function $mixin(e) {
    for (var d = 1, a = arguments.length; d < a; d++) {
        var b = arguments[d];
        if ($type(b) != "object") {
            continue;
        }
        for (var c in b) {
            var g = b[c],
                f = e[c];
            e[c] = (f && $type(g) == "object" && $type(f) == "object") ? $mixin(f, g) : $unlink(g);
        }
    }
    return e;
}

function $pick() {
    for (var b = 0, a = arguments.length; b < a; b++) {
        if (arguments[b] != undefined) {
            return arguments[b];
        }
    }
    return null;
}

function $random(b, a) {
    return Math.floor(Math.random() * (a - b + 1) + b);
}

function $splat(b) {
    var a = $type(b);
    return (a) ? ((a != "array" && a != "arguments") ? [b] : b) : [];
}
var $time = Date.now || function () {
        return +new Date;
    };

function $try() {
    for (var b = 0, a = arguments.length; b < a; b++) {
        try {
            return arguments[b]();
        } catch (c) {}
    }
    return null;
}

function $type(a) {
    if (a == undefined) {
        return false;
    }
    if (a.$family) {
        return (a.$family.name == "number" && !isFinite(a)) ? false : a.$family.name;
    }
    if (a.nodeName) {
        switch (a.nodeType) {
        case 1:
            return "element";
        case 3:
            return (/\S/).test(a.nodeValue) ? "textnode" : "whitespace";
        }
    } else {
        if (typeof a.length == "number") {
            if (a.callee) {
                return "arguments";
            } else {
                if (a.item) {
                    return "collection";
                }
            }
        }
    }
    return typeof a;
}

function $unlink(c) {
    var b;
    switch ($type(c)) {
    case "object":
        b = {};
        for (var e in c) {
            b[e] = $unlink(c[e]);
        }
        break;
    case "hash":
        b = new Hash(c);
        break;
    case "array":
        b = [];
        for (var d = 0, a = c.length; d < a; d++) {
            b[d] = $unlink(c[d]);
        }
        break;
    default:
        return c;
    }
    return b;
}
var Browser = $merge({
    Engine: {
        name: "unknown",
        version: 0
    },
    Platform: {
        name: (window.orientation != undefined) ? "ipod" : (navigator.platform.match(/mac|win|linux/i) || ["other"])[0].toLowerCase()
    },
    Features: {
        xpath: !! (document.evaluate),
        air: !! (window.runtime),
        query: !! (document.querySelector)
    },
    Plugins: {},
    Engines: {
        presto: function () {
            return (!window.opera) ? false : ((arguments.callee.caller) ? 960 : ((document.getElementsByClassName) ? 950 : 925));
        },
        trident: function () {
            return (!window.ActiveXObject) ? false : ((window.XMLHttpRequest) ? ((document.querySelectorAll) ? 6 : 5) : 4);
        },
        webkit: function () {
            return (navigator.taintEnabled) ? false : ((Browser.Features.xpath) ? ((Browser.Features.query) ? 525 : 420) : 419);
        },
        gecko: function () {
            return (!document.getBoxObjectFor && window.mozInnerScreenX == null) ? false : ((document.getElementsByClassName) ? 19 : 18);
        }
    }
}, Browser || {});
Browser.Platform[Browser.Platform.name] = true;
Browser.detect = function () {
    for (var b in this.Engines) {
        var a = this.Engines[b]();
        if (a) {
            this.Engine = {
                name: b,
                version: a
            };
            this.Engine[b] = this.Engine[b + a] = true;
            break;
        }
    }
    return {
        name: b,
        version: a
    };
};
Browser.detect();
Browser.Request = function () {
    return $try(function () {
        return new XMLHttpRequest();
    }, function () {
        return new ActiveXObject("MSXML2.XMLHTTP");
    }, function () {
        return new ActiveXObject("Microsoft.XMLHTTP");
    });
};
Browser.Features.xhr = !! (Browser.Request());
Browser.Plugins.Flash = (function () {
    var a = ($try(function () {
        return navigator.plugins["Shockwave Flash"].description;
    }, function () {
        return new ActiveXObject("ShockwaveFlash.ShockwaveFlash").GetVariable("$version");
    }) || "0 r0").match(/\d+/g);
    return {
        version: parseInt(a[0] || 0 + "." + a[1], 10) || 0,
        build: parseInt(a[2], 10) || 0
    };
})();

function $exec(b) {
    if (!b) {
        return b;
    }
    if (window.execScript) {
        window.execScript(b);
    } else {
        var a = document.createElement("script");
        a.setAttribute("type", "text/javascript");
        a[(Browser.Engine.webkit && Browser.Engine.version < 420) ? "innerText" : "text"] = b;
        document.head.appendChild(a);
        document.head.removeChild(a);
    }
    return b;
}
Native.UID = 1;
var $uid = (Browser.Engine.trident) ? function (a) {
        return (a.uid || (a.uid = [Native.UID++]))[0];
    } : function (a) {
        return a.uid || (a.uid = Native.UID++);
    };
var Window = new Native({
    name: "Window",
    legacy: (Browser.Engine.trident) ? null : window.Window,
    initialize: function (a) {
        $uid(a);
        if (!a.Element) {
            a.Element = $empty;
            if (Browser.Engine.webkit) {
                a.document.createElement("iframe");
            }
            a.Element.prototype = (Browser.Engine.webkit) ? window["[[DOMElement.prototype]]"] : {};
        }
        a.document.window = a;
        return $extend(a, Window.Prototype);
    },
    afterImplement: function (b, a) {
        window[b] = Window.Prototype[b] = a;
    }
});
Window.Prototype = {
    $family: {
        name: "window"
    }
};
new Window(window);
var Document = new Native({
    name: "Document",
    legacy: (Browser.Engine.trident) ? null : window.Document,
    initialize: function (a) {
        $uid(a);
        a.head = a.getElementsByTagName("head")[0];
        a.html = a.getElementsByTagName("html")[0];
        if (Browser.Engine.trident && Browser.Engine.version <= 4) {
            $try(function () {
                a.execCommand("BackgroundImageCache", false, true);
            });
        }
        if (Browser.Engine.trident) {
            a.window.attachEvent("onunload", function () {
                a.window.detachEvent("onunload", arguments.callee);
                a.head = a.html = a.window = null;
            });
        }
        return $extend(a, Document.Prototype);
    },
    afterImplement: function (b, a) {
        document[b] = Document.Prototype[b] = a;
    }
});
Document.Prototype = {
    $family: {
        name: "document"
    }
};
new Document(document);
Array.implement({
    every: function (c, d) {
        for (var b = 0, a = this.length; b < a; b++) {
            if (!c.call(d, this[b], b, this)) {
                return false;
            }
        }
        return true;
    },
    filter: function (d, e) {
        var c = [];
        for (var b = 0, a = this.length; b < a; b++) {
            if (d.call(e, this[b], b, this)) {
                c.push(this[b]);
            }
        }
        return c;
    },
    clean: function () {
        return this.filter($defined);
    },
    indexOf: function (c, d) {
        var a = this.length;
        for (var b = (d < 0) ? Math.max(0, a + d) : d || 0; b < a; b++) {
            if (this[b] === c) {
                return b;
            }
        }
        return -1;
    },
    map: function (d, e) {
        var c = [];
        for (var b = 0, a = this.length; b < a; b++) {
            c[b] = d.call(e, this[b], b, this);
        }
        return c;
    },
    some: function (c, d) {
        for (var b = 0, a = this.length; b < a; b++) {
            if (c.call(d, this[b], b, this)) {
                return true;
            }
        }
        return false;
    },
    associate: function (c) {
        var d = {}, b = Math.min(this.length, c.length);
        for (var a = 0; a < b; a++) {
            d[c[a]] = this[a];
        }
        return d;
    },
    link: function (c) {
        var a = {};
        for (var e = 0, b = this.length; e < b; e++) {
            for (var d in c) {
                if (c[d](this[e])) {
                    a[d] = this[e];
                    delete c[d];
                    break;
                }
            }
        }
        return a;
    },
    contains: function (a, b) {
        return this.indexOf(a, b) != -1;
    },
    extend: function (c) {
        for (var b = 0, a = c.length; b < a; b++) {
            this.push(c[b]);
        }
        return this;
    },
    getLast: function () {
        return (this.length) ? this[this.length - 1] : null;
    },
    getRandom: function () {
        return (this.length) ? this[$random(0, this.length - 1)] : null;
    },
    include: function (a) {
        if (!this.contains(a)) {
            this.push(a);
        }
        return this;
    },
    combine: function (c) {
        for (var b = 0, a = c.length; b < a; b++) {
            this.include(c[b]);
        }
        return this;
    },
    erase: function (b) {
        for (var a = this.length; a--; a) {
            if (this[a] === b) {
                this.splice(a, 1);
            }
        }
        return this;
    },
    empty: function () {
        this.length = 0;
        return this;
    },
    flatten: function () {
        var d = [];
        for (var b = 0, a = this.length; b < a; b++) {
            var c = $type(this[b]);
            if (!c) {
                continue;
            }
            d = d.concat((c == "array" || c == "collection" || c == "arguments") ? Array.flatten(this[b]) : this[b]);
        }
        return d;
    },
    hexToRgb: function (b) {
        if (this.length != 3) {
            return null;
        }
        var a = this.map(function (c) {
            if (c.length == 1) {
                c += c;
            }
            return c.toInt(16);
        });
        return (b) ? a : "rgb(" + a + ")";
    },
    rgbToHex: function (d) {
        if (this.length < 3) {
            return null;
        }
        if (this.length == 4 && this[3] == 0 && !d) {
            return "transparent";
        }
        var b = [];
        for (var a = 0; a < 3; a++) {
            var c = (this[a] - 0).toString(16);
            b.push((c.length == 1) ? "0" + c : c);
        }
        return (d) ? b : "#" + b.join("");
    }
});
Function.implement({
    extend: function (a) {
        for (var b in a) {
            this[b] = a[b];
        }
        return this;
    },
    create: function (b) {
        var a = this;
        b = b || {};
        return function (d) {
            var c = b.arguments;
            c = (c != undefined) ? $splat(c) : Array.slice(arguments, (b.event) ? 1 : 0);
            if (b.event) {
                c = [d || window.event].extend(c);
            }
            var e = function () {
                return a.apply(b.bind || null, c);
            };
            if (b.delay) {
                return setTimeout(e, b.delay);
            }
            if (b.periodical) {
                return setInterval(e, b.periodical);
            }
            if (b.attempt) {
                return $try(e);
            }
            return e();
        };
    },
    run: function (a, b) {
        return this.apply(b, $splat(a));
    },
    pass: function (a, b) {
        return this.create({
            bind: b,
            arguments: a
        });
    },
    bind: function (b, a) {
        return this.create({
            bind: b,
            arguments: a
        });
    },
    bindWithEvent: function (b, a) {
        return this.create({
            bind: b,
            arguments: a,
            event: true
        });
    },
    attempt: function (a, b) {
        return this.create({
            bind: b,
            arguments: a,
            attempt: true
        })();
    },
    delay: function (b, c, a) {
        return this.create({
            bind: c,
            arguments: a,
            delay: b
        })();
    },
    periodical: function (c, b, a) {
        return this.create({
            bind: b,
            arguments: a,
            periodical: c
        })();
    }
});
Number.implement({
    limit: function (b, a) {
        return Math.min(a, Math.max(b, this));
    },
    round: function (a) {
        a = Math.pow(10, a || 0);
        return Math.round(this * a) / a;
    },
    times: function (b, c) {
        for (var a = 0; a < this; a++) {
            b.call(c, a, this);
        }
    },
    toFloat: function () {
        return parseFloat(this);
    },
    toInt: function (a) {
        return parseInt(this, a || 10);
    }
});
Number.alias("times", "each");
(function (b) {
    var a = {};
    b.each(function (c) {
        if (!Number[c]) {
            a[c] = function () {
                return Math[c].apply(null, [this].concat($A(arguments)));
            };
        }
    });
    Number.implement(a);
})(["abs", "acos", "asin", "atan", "atan2", "ceil", "cos", "exp", "floor", "log", "max", "min", "pow", "sin", "sqrt", "tan"]);
String.implement({
    test: function (a, b) {
        return ((typeof a == "string") ? new RegExp(a, b) : a).test(this);
    },
    contains: function (a, b) {
        return (b) ? (b + this + b).indexOf(b + a + b) > -1 : this.indexOf(a) > -1;
    },
    trim: function () {
        return this.replace(/^\s+|\s+$/g, "");
    },
    clean: function () {
        return this.replace(/\s+/g, " ").trim();
    },
    camelCase: function () {
        return this.replace(/-\D/g, function (a) {
            return a.charAt(1).toUpperCase();
        });
    },
    hyphenate: function () {
        return this.replace(/[A-Z]/g, function (a) {
            return ("-" + a.charAt(0).toLowerCase());
        });
    },
    capitalize: function () {
        return this.replace(/\b[a-z]/g, function (a) {
            return a.toUpperCase();
        });
    },
    escapeRegExp: function () {
        return this.replace(/([-.*+?^${}()|[\]\/\\])/g, "\\$1");
    },
    toInt: function (a) {
        return parseInt(this, a || 10);
    },
    toFloat: function () {
        return parseFloat(this);
    },
    hexToRgb: function (b) {
        var a = this.match(/^#?(\w{1,2})(\w{1,2})(\w{1,2})$/);
        return (a) ? a.slice(1).hexToRgb(b) : null;
    },
    rgbToHex: function (b) {
        var a = this.match(/\d{1,3}/g);
        return (a) ? a.rgbToHex(b) : null;
    },
    stripScripts: function (b) {
        var a = "";
        var c = this.replace(/<script[^>]*>([\s\S]*?)<\/script>/gi, function () {
            a += arguments[1] + "\n";
            return "";
        });
        if (b === true) {
            $exec(a);
        } else {
            if ($type(b) == "function") {
                b(a, c);
            }
        }
        return c;
    },
    substitute: function (a, b) {
        return this.replace(b || (/\\?\{([^{}]+)\}/g), function (d, c) {
            if (d.charAt(0) == "\\") {
                return d.slice(1);
            }
            return (a[c] != undefined) ? a[c] : "";
        });
    }
});
Hash.implement({
    has: Object.prototype.hasOwnProperty,
    keyOf: function (b) {
        for (var a in this) {
            if (this.hasOwnProperty(a) && this[a] === b) {
                return a;
            }
        }
        return null;
    },
    hasValue: function (a) {
        return (Hash.keyOf(this, a) !== null);
    },
    extend: function (a) {
        Hash.each(a || {}, function (c, b) {
            Hash.set(this, b, c);
        }, this);
        return this;
    },
    combine: function (a) {
        Hash.each(a || {}, function (c, b) {
            Hash.include(this, b, c);
        }, this);
        return this;
    },
    erase: function (a) {
        if (this.hasOwnProperty(a)) {
            delete this[a];
        }
        return this;
    },
    get: function (a) {
        return (this.hasOwnProperty(a)) ? this[a] : null;
    },
    set: function (a, b) {
        if (!this[a] || this.hasOwnProperty(a)) {
            this[a] = b;
        }
        return this;
    },
    empty: function () {
        Hash.each(this, function (b, a) {
            delete this[a];
        }, this);
        return this;
    },
    include: function (a, b) {
        if (this[a] == undefined) {
            this[a] = b;
        }
        return this;
    },
    map: function (b, c) {
        var a = new Hash;
        Hash.each(this, function (e, d) {
            a.set(d, b.call(c, e, d, this));
        }, this);
        return a;
    },
    filter: function (b, c) {
        var a = new Hash;
        Hash.each(this, function (e, d) {
            if (b.call(c, e, d, this)) {
                a.set(d, e);
            }
        }, this);
        return a;
    },
    every: function (b, c) {
        for (var a in this) {
            if (this.hasOwnProperty(a) && !b.call(c, this[a], a)) {
                return false;
            }
        }
        return true;
    },
    some: function (b, c) {
        for (var a in this) {
            if (this.hasOwnProperty(a) && b.call(c, this[a], a)) {
                return true;
            }
        }
        return false;
    },
    getKeys: function () {
        var a = [];
        Hash.each(this, function (c, b) {
            a.push(b);
        });
        return a;
    },
    getValues: function () {
        var a = [];
        Hash.each(this, function (b) {
            a.push(b);
        });
        return a;
    },
    toQueryString: function (a) {
        var b = [];
        Hash.each(this, function (f, e) {
            if (a) {
                e = a + "[" + e + "]";
            }
            var d;
            switch ($type(f)) {
            case "object":
                d = Hash.toQueryString(f, e);
                break;
            case "array":
                var c = {};
                f.each(function (h, g) {
                    c[g] = h;
                });
                d = Hash.toQueryString(c, e);
                break;
            default:
                d = e + "=" + encodeURIComponent(f);
            }
            if (f != undefined) {
                b.push(d);
            }
        });
        return b.join("&");
    }
});
Hash.alias({
    keyOf: "indexOf",
    hasValue: "contains"
});
var Event = new Native({
    name: "Event",
    initialize: function (a, f) {
        f = f || window;
        var k = f.document;
        a = a || f.event;
        if (a.$extended) {
            return a;
        }
        this.$extended = true;
        var j = a.type;
        var g = a.target || a.srcElement;
        while (g && g.nodeType == 3) {
            g = g.parentNode;
        }
        if (j.test(/key/)) {
            var b = a.which || a.keyCode;
            var m = Event.Keys.keyOf(b);
            if (j == "keydown") {
                var d = b - 111;
                if (d > 0 && d < 13) {
                    m = "f" + d;
                }
            }
            m = m || String.fromCharCode(b).toLowerCase();
        } else {
            if (j.match(/(click|mouse|menu)/i)) {
                k = (!k.compatMode || k.compatMode == "CSS1Compat") ? k.html : k.body;
                var i = {
                    x: a.pageX || a.clientX + k.scrollLeft,
                    y: a.pageY || a.clientY + k.scrollTop
                };
                var c = {
                    x: (a.pageX) ? a.pageX - f.pageXOffset : a.clientX,
                    y: (a.pageY) ? a.pageY - f.pageYOffset : a.clientY
                };
                if (j.match(/DOMMouseScroll|mousewheel/)) {
                    var h = (a.wheelDelta) ? a.wheelDelta / 120 : -(a.detail || 0) / 3;
                }
                var e = (a.which == 3) || (a.button == 2);
                var l = null;
                if (j.match(/over|out/)) {
                    switch (j) {
                    case "mouseover":
                        l = a.relatedTarget || a.fromElement;
                        break;
                    case "mouseout":
                        l = a.relatedTarget || a.toElement;
                    }
                    if (!(function () {
                        while (l && l.nodeType == 3) {
                            l = l.parentNode;
                        }
                        return true;
                    }).create({
                        attempt: Browser.Engine.gecko
                    })()) {
                        l = false;
                    }
                }
            }
        }
        return $extend(this, {
            event: a,
            type: j,
            page: i,
            client: c,
            rightClick: e,
            wheel: h,
            relatedTarget: l,
            target: g,
            code: b,
            key: m,
            shift: a.shiftKey,
            control: a.ctrlKey,
            alt: a.altKey,
            meta: a.metaKey
        });
    }
});
Event.Keys = new Hash({
    enter: 13,
    up: 38,
    down: 40,
    left: 37,
    right: 39,
    esc: 27,
    space: 32,
    backspace: 8,
    tab: 9,
    "delete": 46
});
Event.implement({
    stop: function () {
        return this.stopPropagation().preventDefault();
    },
    stopPropagation: function () {
        if (this.event.stopPropagation) {
            this.event.stopPropagation();
        } else {
            this.event.cancelBubble = true;
        }
        return this;
    },
    preventDefault: function () {
        if (this.event.preventDefault) {
            this.event.preventDefault();
        } else {
            this.event.returnValue = false;
        }
        return this;
    }
});

function Class(b) {
    if (b instanceof Function) {
        b = {
            initialize: b
        };
    }
    var a = function () {
        Object.reset(this);
        if (a._prototyping) {
            return this;
        }
        this._current = $empty;
        var c = (this.initialize) ? this.initialize.apply(this, arguments) : this;
        delete this._current;
        delete this.caller;
        return c;
    }.extend(this);
    a.implement(b);
    a.constructor = Class;
    a.prototype.constructor = a;
    return a;
}
Function.prototype.protect = function () {
    this._protected = true;
    return this;
};
Object.reset = function (a, c) {
    if (c == null) {
        for (var e in a) {
            Object.reset(a, e);
        }
        return a;
    }
    delete a[c];
    switch ($type(a[c])) {
    case "object":
        var d = function () {};
        d.prototype = a[c];
        var b = new d;
        a[c] = Object.reset(b);
        break;
    case "array":
        a[c] = $unlink(a[c]);
        break;
    }
    return a;
};
new Native({
    name: "Class",
    initialize: Class
}).extend({
    instantiate: function (b) {
        b._prototyping = true;
        var a = new b;
        delete b._prototyping;
        return a;
    },
    wrap: function (a, b, c) {
        if (c._origin) {
            c = c._origin;
        }
        return function () {
            if (c._protected && this._current == null) {
                throw new Error('The method "' + b + '" cannot be called.');
            }
            var e = this.caller,
                f = this._current;
            this.caller = f;
            this._current = arguments.callee;
            var d = c.apply(this, arguments);
            this._current = f;
            this.caller = e;
            return d;
        }.extend({
            _owner: a,
            _origin: c,
            _name: b
        });
    }
});
Class.implement({
    implement: function (a, d) {
        if ($type(a) == "object") {
            for (var e in a) {
                this.implement(e, a[e]);
            }
            return this;
        }
        var f = Class.Mutators[a];
        if (f) {
            d = f.call(this, d);
            if (d == null) {
                return this;
            }
        }
        var c = this.prototype;
        switch ($type(d)) {
        case "function":
            if (d._hidden) {
                return this;
            }
            c[a] = Class.wrap(this, a, d);
            break;
        case "object":
            var b = c[a];
            if ($type(b) == "object") {
                $mixin(b, d);
            } else {
                c[a] = $unlink(d);
            }
            break;
        case "array":
            c[a] = $unlink(d);
            break;
        default:
            c[a] = d;
        }
        return this;
    }
});
Class.Mutators = {
    Extends: function (a) {
        this.parent = a;
        this.prototype = Class.instantiate(a);
        this.implement("parent", function () {
            var b = this.caller._name,
                c = this.caller._owner.parent.prototype[b];
            if (!c) {
                throw new Error('The method "' + b + '" has no parent.');
            }
            return c.apply(this, arguments);
        }.protect());
    },
    Implements: function (a) {
        $splat(a).each(function (b) {
            if (b instanceof Function) {
                b = Class.instantiate(b);
            }
            this.implement(b);
        }, this);
    }
};
var Chain = new Class({
    $chain: [],
    chain: function () {
        this.$chain.extend(Array.flatten(arguments));
        return this;
    },
    callChain: function () {
        return (this.$chain.length) ? this.$chain.shift().apply(this, arguments) : false;
    },
    clearChain: function () {
        this.$chain.empty();
        return this;
    }
});
var Events = new Class({
    $events: {},
    addEvent: function (c, b, a) {
        c = Events.removeOn(c);
        if (b != $empty) {
            this.$events[c] = this.$events[c] || [];
            this.$events[c].include(b);
            if (a) {
                b.internal = true;
            }
        }
        return this;
    },
    addEvents: function (a) {
        for (var b in a) {
            this.addEvent(b, a[b]);
        }
        return this;
    },
    fireEvent: function (c, b, a) {
        c = Events.removeOn(c);
        if (!this.$events || !this.$events[c]) {
            return this;
        }
        this.$events[c].each(function (d) {
            d.create({
                bind: this,
                delay: a,
                "arguments": b
            })();
        }, this);
        return this;
    },
    removeEvent: function (b, a) {
        b = Events.removeOn(b);
        if (!this.$events[b]) {
            return this;
        }
        if (!a.internal) {
            this.$events[b].erase(a);
        }
        return this;
    },
    removeEvents: function (c) {
        var d;
        if ($type(c) == "object") {
            for (d in c) {
                this.removeEvent(d, c[d]);
            }
            return this;
        }
        if (c) {
            c = Events.removeOn(c);
        }
        for (d in this.$events) {
            if (c && c != d) {
                continue;
            }
            var b = this.$events[d];
            for (var a = b.length; a--; a) {
                this.removeEvent(d, b[a]);
            }
        }
        return this;
    }
});
Events.removeOn = function (a) {
    return a.replace(/^on([A-Z])/, function (b, c) {
        return c.toLowerCase();
    });
};
var Options = new Class({
    setOptions: function () {
        this.options = $merge.run([this.options].extend(arguments));
        if (!this.addEvent) {
            return this;
        }
        for (var a in this.options) {
            if ($type(this.options[a]) != "function" || !(/^on[A-Z]/).test(a)) {
                continue;
            }
            this.addEvent(a, this.options[a]);
            delete this.options[a];
        }
        return this;
    }
});
var Element = new Native({
    name: "Element",
    legacy: window.Element,
    initialize: function (a, b) {
        var c = Element.Constructors.get(a);
        if (c) {
            return c(b);
        }
        if (typeof a == "string") {
            return document.newElement(a, b);
        }
        return document.id(a).set(b);
    },
    afterImplement: function (a, b) {
        Element.Prototype[a] = b;
        if (Array[a]) {
            return;
        }
        Elements.implement(a, function () {
            var c = [],
                g = true;
            for (var e = 0, d = this.length; e < d; e++) {
                var f = this[e][a].apply(this[e], arguments);
                c.push(f);
                if (g) {
                    g = ($type(f) == "element");
                }
            }
            return (g) ? new Elements(c) : c;
        });
    }
});
Element.Prototype = {
    $family: {
        name: "element"
    }
};
Element.Constructors = new Hash;
var IFrame = new Native({
    name: "IFrame",
    generics: false,
    initialize: function () {
        var f = Array.link(arguments, {
            properties: Object.type,
            iframe: $defined
        });
        var d = f.properties || {};
        var c = document.id(f.iframe);
        var e = d.onload || $empty;
        delete d.onload;
        d.id = d.name = $pick(d.id, d.name, c ? (c.id || c.name) : "IFrame_" + $time());
        c = new Element(c || "iframe", d);
        var b = function () {
            var g = $try(function () {
                return c.contentWindow.location.host;
            });
            if (!g || g == window.location.host) {
                var h = new Window(c.contentWindow);
                new Document(c.contentWindow.document);
                $extend(h.Element.prototype, Element.Prototype);
            }
            e.call(c.contentWindow, c.contentWindow.document);
        };
        var a = $try(function () {
            return c.contentWindow;
        });
        ((a && a.document.body) || window.frames[d.id]) ? b() : c.addListener("load", b);
        return c;
    }
});
var Elements = new Native({
    initialize: function (f, b) {
        b = $extend({
            ddup: true,
            cash: true
        }, b);
        f = f || [];
        if (b.ddup || b.cash) {
            var g = {}, e = [];
            for (var c = 0, a = f.length; c < a; c++) {
                var d = document.id(f[c], !b.cash);
                if (b.ddup) {
                    if (g[d.uid]) {
                        continue;
                    }
                    g[d.uid] = true;
                }
                if (d) {
                    e.push(d);
                }
            }
            f = e;
        }
        return (b.cash) ? $extend(f, this) : f;
    }
});
Elements.implement({
    filter: function (a, b) {
        if (!a) {
            return this;
        }
        return new Elements(Array.filter(this, (typeof a == "string") ? function (c) {
            return c.match(a);
        } : a, b));
    }
});
Document.implement({
    newElement: function (a, b) {
/*
		if (Browser.Engine.trident && b) {

            ["name", "type", "checked"].each(function (c) {
                if (!b[c]) {
                    return;
                }
                a += " " + c + '="' + b[c] + '"';
                if (c != "checked") {
                    delete b[c];
                }
            });
            a = "<" + a + ">";
        }
*/   
        return document.id(this.createElement(a)).set(b);
        
    },
    newTextNode: function (a) {
        return this.createTextNode(a);
    },
    getDocument: function () {
        return this;
    },
    getWindow: function () {
        return this.window;
    },
    id: (function () {
        var a = {
            string: function (d, c, b) {
                d = b.getElementById(d);
                return (d) ? a.element(d, c) : null;
            },
            element: function (b, e) {
                $uid(b);
                if (!e && !b.$family && !(/^object|embed$/i).test(b.tagName)) {
                    var c = Element.Prototype;
                    for (var d in c) {
                        b[d] = c[d];
                    }
                }
                return b;
            },
            object: function (c, d, b) {
                if (c.toElement) {
                    return a.element(c.toElement(b), d);
                }
                return null;
            }
        };
        a.textnode = a.whitespace = a.window = a.document = $arguments(0);
        return function (c, e, d) {
            if (c && c.$family && c.uid) {
                return c;
            }
            var b = $type(c);
            return (a[b]) ? a[b](c, e, d || document) : null;
        };
    })()
});
if (window.$ == null) {
    Window.implement({
        $: function (a, b) {
            return document.id(a, b, this.document);
        }
    });
}
Window.implement({
    $$: function (a) {
        if (arguments.length == 1 && typeof a == "string") {
            return this.document.getElements(a);
        }
        var f = [];
        var c = Array.flatten(arguments);
        for (var d = 0, b = c.length; d < b; d++) {
            var e = c[d];
            switch ($type(e)) {
            case "element":
                f.push(e);
                break;
            case "string":
                f.extend(this.document.getElements(e, true));
            }
        }
        return new Elements(f);
    },
    getDocument: function () {
        return this.document;
    },
    getWindow: function () {
        return this;
    }
});
Native.implement([Element, Document], {
    getElement: function (a, b) {
        return document.id(this.getElements(a, true)[0] || null, b);
    },
    getElements: function (a, d) {
        a = a.split(",");
        var c = [];
        var b = (a.length > 1);
        a.each(function (e) {
            var f = this.getElementsByTagName(e.trim());
            (b) ? c.extend(f) : c = f;
        }, this);
        return new Elements(c, {
            ddup: b,
            cash: !d
        });
    }
});
(function () {
    var h = {}, f = {};
    var i = {
        input: "checked",
        option: "selected",
        textarea: (Browser.Engine.webkit && Browser.Engine.version < 420) ? "innerHTML" : "value"
    };
    var c = function (l) {
        return (f[l] || (f[l] = {}));
    };
    var g = function (n, l) {
        if (!n) {
            return;
        }
        var m = n.uid;
        if (Browser.Engine.trident) {
            if (n.clearAttributes) {
                var q = l && n.cloneNode(false);
                n.clearAttributes();
                if (q) {
                    n.mergeAttributes(q);
                }
            } else {
                if (n.removeEvents) {
                    n.removeEvents();
                }
            } if ((/object/i).test(n.tagName)) {
                for (var o in n) {
                    if (typeof n[o] == "function") {
                        n[o] = $empty;
                    }
                }
                Element.dispose(n);
            }
        }
        if (!m) {
            return;
        }
        h[m] = f[m] = null;
    };
    var d = function () {
        Hash.each(h, g);
        if (Browser.Engine.trident) {
            $A(document.getElementsByTagName("object")).each(g);
        }
        if (window.CollectGarbage) {
            CollectGarbage();
        }
        h = f = null;
    };
    var j = function (n, l, s, m, p, r) {
        var o = n[s || l];
        var q = [];
        while (o) {
            if (o.nodeType == 1 && (!m || Element.match(o, m))) {
                if (!p) {
                    return document.id(o, r);
                }
                q.push(o);
            }
            o = o[l];
        }
        return (p) ? new Elements(q, {
            ddup: false,
            cash: !r
        }) : null;
    };
    var e = {
        html: "innerHTML",
        "class": "className",
        "for": "htmlFor",
        defaultValue: "defaultValue",
        text: (Browser.Engine.trident || (Browser.Engine.webkit && Browser.Engine.version < 420)) ? "innerText" : "textContent"
    };
    var b = ["compact", "nowrap", "ismap", "declare", "noshade", "checked", "disabled", "readonly", "multiple", "selected", "noresize", "defer"];
    var k = ["value", "type", "defaultValue", "accessKey", "cellPadding", "cellSpacing", "colSpan", "frameBorder", "maxLength", "readOnly", "rowSpan", "tabIndex", "useMap"];
    b = b.associate(b);
    Hash.extend(e, b);
    Hash.extend(e, k.associate(k.map(String.toLowerCase)));
    var a = {
        before: function (m, l) {
            if (l.parentNode) {
                l.parentNode.insertBefore(m, l);
            }
        },
        after: function (m, l) {
            if (!l.parentNode) {
                return;
            }
            var n = l.nextSibling;
            (n) ? l.parentNode.insertBefore(m, n) : l.parentNode.appendChild(m);
        },
        bottom: function (m, l) {
            l.appendChild(m);
        },
        top: function (m, l) {
            var n = l.firstChild;
            (n) ? l.insertBefore(m, n) : l.appendChild(m);
        }
    };
    a.inside = a.bottom;
    Hash.each(a, function (l, m) {
        m = m.capitalize();
        Element.implement("inject" + m, function (n) {
            l(this, document.id(n, true));
            return this;
        });
        Element.implement("grab" + m, function (n) {
            l(document.id(n, true), this);
            return this;
        });
    });
    Element.implement({
        set: function (o, m) {
            switch ($type(o)) {
            case "object":
                for (var n in o) {
                    this.set(n, o[n]);
                }
                break;
            case "string":
                var l = Element.Properties.get(o);
                (l && l.set) ? l.set.apply(this, Array.slice(arguments, 1)) : this.setProperty(o, m);
            }
            return this;
        },
        get: function (m) {
            var l = Element.Properties.get(m);
            return (l && l.get) ? l.get.apply(this, Array.slice(arguments, 1)) : this.getProperty(m);
        },
        erase: function (m) {
            var l = Element.Properties.get(m);
            (l && l.erase) ? l.erase.apply(this) : this.removeProperty(m);
            return this;
        },
        setProperty: function (m, n) {
            var l = e[m];
            if (n == undefined) {
                return this.removeProperty(m);
            }
            if (l && b[m]) {
                n = !! n;
            }(l) ? this[l] = n : this.setAttribute(m, "" + n);
            return this;
        },
        setProperties: function (l) {
            for (var m in l) {
                this.setProperty(m, l[m]);
            }
            return this;
        },
        getProperty: function (m) {
            var l = e[m];
            var n = (l) ? this[l] : this.getAttribute(m, 2);
            return (b[m]) ? !! n : (l) ? n : n || null;
        },
        getProperties: function () {
            var l = $A(arguments);
            return l.map(this.getProperty, this).associate(l);
        },
        removeProperty: function (m) {
            var l = e[m];
            (l) ? this[l] = (l && b[m]) ? false : "" : this.removeAttribute(m);
            return this;
        },
        removeProperties: function () {
            Array.each(arguments, this.removeProperty, this);
            return this;
        },
        hasClass: function (l) {
            return this.className.contains(l, " ");
        },
        addClass: function (l) {
            if (!this.hasClass(l)) {
                this.className = (this.className + " " + l).clean();
            }
            return this;
        },
        removeClass: function (l) {
            this.className = this.className.replace(new RegExp("(^|\\s)" + l + "(?:\\s|$)"), "$1");
            return this;
        },
        toggleClass: function (l) {
            return this.hasClass(l) ? this.removeClass(l) : this.addClass(l);
        },
        adopt: function () {
            Array.flatten(arguments).each(function (l) {
                l = document.id(l, true);
                if (l) {
                    this.appendChild(l);
                }
            }, this);
            return this;
        },
        appendText: function (m, l) {
            return this.grab(this.getDocument().newTextNode(m), l);
        },
        grab: function (m, l) {
            a[l || "bottom"](document.id(m, true), this);
            return this;
        },
        inject: function (m, l) {
            a[l || "bottom"](this, document.id(m, true));
            return this;
        },
        replaces: function (l) {
            l = document.id(l, true);
            l.parentNode.replaceChild(this, l);
            return this;
        },
        wraps: function (m, l) {
            m = document.id(m, true);
            return this.replaces(m).grab(m, l);
        },
        getPrevious: function (l, m) {
            return j(this, "previousSibling", null, l, false, m);
        },
        getAllPrevious: function (l, m) {
            return j(this, "previousSibling", null, l, true, m);
        },
        getNext: function (l, m) {
            return j(this, "nextSibling", null, l, false, m);
        },
        getAllNext: function (l, m) {
            return j(this, "nextSibling", null, l, true, m);
        },
        getFirst: function (l, m) {
            return j(this, "nextSibling", "firstChild", l, false, m);
        },
        getLast: function (l, m) {
            return j(this, "previousSibling", "lastChild", l, false, m);
        },
        getParent: function (l, m) {
            return j(this, "parentNode", null, l, false, m);
        },
        getParents: function (l, m) {
            return j(this, "parentNode", null, l, true, m);
        },
        getSiblings: function (l, m) {
            return this.getParent().getChildren(l, m).erase(this);
        },
        getChildren: function (l, m) {
            return j(this, "nextSibling", "firstChild", l, true, m);
        },
        getWindow: function () {
            return this.ownerDocument.window;
        },
        getDocument: function () {
            return this.ownerDocument;
        },
        getElementById: function (o, n) {
            var m = this.ownerDocument.getElementById(o);
            if (!m) {
                return null;
            }
            for (var l = m.parentNode; l != this; l = l.parentNode) {
                if (!l) {
                    return null;
                }
            }
            return document.id(m, n);
        },
        getSelected: function () {
            return new Elements($A(this.options).filter(function (l) {
                return l.selected;
            }));
        },
        getComputedStyle: function (m) {
            if (this.currentStyle) {
                return this.currentStyle[m.camelCase()];
            }
            var l = this.getDocument().defaultView.getComputedStyle(this, null);
            return (l) ? l.getPropertyValue([m.hyphenate()]) : null;
        },
        toQueryString: function () {
            var l = [];
            this.getElements("input, select, textarea", true).each(function (m) {
                if (!m.name || m.disabled || m.type == "submit" || m.type == "reset" || m.type == "file") {
                    return;
                }
                var n = (m.tagName.toLowerCase() == "select") ? Element.getSelected(m).map(function (o) {
                    return o.value;
                }) : ((m.type == "radio" || m.type == "checkbox") && !m.checked) ? null : m.value;
                $splat(n).each(function (o) {
                    if (typeof o != "undefined") {
                        l.push(m.name + "=" + encodeURIComponent(o));
                    }
                });
            });
            return l.join("&");
        },
        clone: function (o, l) {
            o = o !== false;
            var r = this.cloneNode(o);
            var n = function (v, u) {
                if (!l) {
                    v.removeAttribute("id");
                }
                if (Browser.Engine.trident) {
                    v.clearAttributes();
                    v.mergeAttributes(u);
                    v.removeAttribute("uid");
                    if (v.options) {
                        var w = v.options,
                            s = u.options;
                        for (var t = w.length; t--;) {
                            w[t].selected = s[t].selected;
                        }
                    }
                }
                var x = i[u.tagName.toLowerCase()];
                if (x && u[x]) {
                    v[x] = u[x];
                }
            };
            if (o) {
                var p = r.getElementsByTagName("*"),
                    q = this.getElementsByTagName("*");
                for (var m = p.length; m--;) {
                    n(p[m], q[m]);
                }
            }
            n(r, this);
            return document.id(r);
        },
        destroy: function () {
            Element.empty(this);
            Element.dispose(this);
            g(this, true);
            return null;
        },
        empty: function () {
            $A(this.childNodes).each(function (l) {
                Element.destroy(l);
            });
            return this;
        },
        dispose: function () {
            return (this.parentNode) ? this.parentNode.removeChild(this) : this;
        },
        hasChild: function (l) {
            l = document.id(l, true);
            if (!l) {
                return false;
            }
            if (Browser.Engine.webkit && Browser.Engine.version < 420) {
                return $A(this.getElementsByTagName(l.tagName)).contains(l);
            }
            return (this.contains) ? (this != l && this.contains(l)) : !! (this.compareDocumentPosition(l) & 16);
        },
        match: function (l) {
            return (!l || (l == this) || (Element.get(this, "tag") == l));
        }
    });
    Native.implement([Element, Window, Document], {
        addListener: function (o, n) {
            if (o == "unload") {
                var l = n,
                    m = this;
                n = function () {
                    m.removeListener("unload", n);
                    l();
                };
            } else {
                h[this.uid] = this;
            } if (this.addEventListener) {
                this.addEventListener(o, n, false);
            } else {
                this.attachEvent("on" + o, n);
            }
            return this;
        },
        removeListener: function (m, l) {
            if (this.removeEventListener) {
                this.removeEventListener(m, l, false);
            } else {
                this.detachEvent("on" + m, l);
            }
            return this;
        },
        retrieve: function (m, l) {
            var o = c(this.uid),
                n = o[m];
            if (l != undefined && n == undefined) {
                n = o[m] = l;
            }
            return $pick(n);
        },
        store: function (m, l) {
            var n = c(this.uid);
            n[m] = l;
            return this;
        },
        eliminate: function (l) {
            var m = c(this.uid);
            delete m[l];
            return this;
        }
    });
    window.addListener("unload", d);
})();
Element.Properties = new Hash;
Element.Properties.style = {
    set: function (a) {
        this.style.cssText = a;
    },
    get: function () {
        return this.style.cssText;
    },
    erase: function () {
        this.style.cssText = "";
    }
};
Element.Properties.tag = {
    get: function () {
        return this.tagName.toLowerCase();
    }
};
Element.Properties.html = (function () {
    var c = document.createElement("div");
    var a = {
        table: [1, "<table>", "</table>"],
        select: [1, "<select>", "</select>"],
        tbody: [2, "<table><tbody>", "</tbody></table>"],
        tr: [3, "<table><tbody><tr>", "</tr></tbody></table>"]
    };
    a.thead = a.tfoot = a.tbody;
    var b = {
        set: function () {
            var e = Array.flatten(arguments).join("");
            var f = Browser.Engine.trident && a[this.get("tag")];
            if (f) {
                var g = c;
                g.innerHTML = f[1] + e + f[2];
                for (var d = f[0]; d--;) {
                    g = g.firstChild;
                }
                this.empty().adopt(g.childNodes);
            } else {
                this.innerHTML = e;
            }
        }
    };
    b.erase = b.set;
    return b;
})();
if (Browser.Engine.webkit && Browser.Engine.version < 420) {
    Element.Properties.text = {
        get: function () {
            if (this.innerText) {
                return this.innerText;
            }
            var a = this.ownerDocument.newElement("div", {
                html: this.innerHTML
            }).inject(this.ownerDocument.body);
            var b = a.innerText;
            a.destroy();
            return b;
        }
    };
}
Element.Properties.events = {
    set: function (a) {
        this.addEvents(a);
    }
};
Native.implement([Element, Window, Document], {
    addEvent: function (e, g) {
        var h = this.retrieve("events", {});
        h[e] = h[e] || {
            keys: [],
            values: []
        };
        if (h[e].keys.contains(g)) {
            return this;
        }
        h[e].keys.push(g);
        var f = e,
            a = Element.Events.get(e),
            c = g,
            i = this;
        if (a) {
            if (a.onAdd) {
                a.onAdd.call(this, g);
            }
            if (a.condition) {
                c = function (j) {
                    if (a.condition.call(this, j)) {
                        return g.call(this, j);
                    }
                    return true;
                };
            }
            f = a.base || f;
        }
        var d = function () {
            return g.call(i);
        };
        var b = Element.NativeEvents[f];
        if (b) {
            if (b == 2) {
                d = function (j) {
                    j = new Event(j, i.getWindow());
                    if (c.call(i, j) === false) {
                        j.stop();
                    }
                };
            }
            this.addListener(f, d);
        }
        h[e].values.push(d);
        return this;
    },
    removeEvent: function (c, b) {
        var a = this.retrieve("events");
        if (!a || !a[c]) {
            return this;
        }
        var f = a[c].keys.indexOf(b);
        if (f == -1) {
            return this;
        }
        a[c].keys.splice(f, 1);
        var e = a[c].values.splice(f, 1)[0];
        var d = Element.Events.get(c);
        if (d) {
            if (d.onRemove) {
                d.onRemove.call(this, b);
            }
            c = d.base || c;
        }
        return (Element.NativeEvents[c]) ? this.removeListener(c, e) : this;
    },
    addEvents: function (a) {
        for (var b in a) {
            this.addEvent(b, a[b]);
        }
        return this;
    },
    removeEvents: function (a) {
        var c;
        if ($type(a) == "object") {
            for (c in a) {
                this.removeEvent(c, a[c]);
            }
            return this;
        }
        var b = this.retrieve("events");
        if (!b) {
            return this;
        }
        if (!a) {
            for (c in b) {
                this.removeEvents(c);
            }
            this.eliminate("events");
        } else {
            if (b[a]) {
                while (b[a].keys[0]) {
                    this.removeEvent(a, b[a].keys[0]);
                }
                b[a] = null;
            }
        }
        return this;
    },
    fireEvent: function (d, b, a) {
        var c = this.retrieve("events");
        if (!c || !c[d]) {
            return this;
        }
        c[d].keys.each(function (e) {
            e.create({
                bind: this,
                delay: a,
                "arguments": b
            })();
        }, this);
        return this;
    },
    cloneEvents: function (d, a) {
        d = document.id(d);
        var c = d.retrieve("events");
        if (!c) {
            return this;
        }
        if (!a) {
            for (var b in c) {
                this.cloneEvents(d, b);
            }
        } else {
            if (c[a]) {
                c[a].keys.each(function (e) {
                    this.addEvent(a, e);
                }, this);
            }
        }
        return this;
    }
});
Element.NativeEvents = {
    click: 2,
    dblclick: 2,
    mouseup: 2,
    mousedown: 2,
    contextmenu: 2,
    mousewheel: 2,
    DOMMouseScroll: 2,
    mouseover: 2,
    mouseout: 2,
    mousemove: 2,
    selectstart: 2,
    selectend: 2,
    keydown: 2,
    keypress: 2,
    keyup: 2,
    focus: 2,
    blur: 2,
    change: 2,
    reset: 2,
    select: 2,
    submit: 2,
    load: 1,
    unload: 1,
    beforeunload: 2,
    resize: 1,
    move: 1,
    DOMContentLoaded: 1,
    readystatechange: 1,
    error: 1,
    abort: 1,
    scroll: 1
};
(function () {
    var a = function (b) {
        var c = b.relatedTarget;
        if (c == undefined) {
            return true;
        }
        if (c === false) {
            return false;
        }
        return ($type(this) != "document" && c != this && c.prefix != "xul" && !this.hasChild(c));
    };
    Element.Events = new Hash({
        mouseenter: {
            base: "mouseover",
            condition: a
        },
        mouseleave: {
            base: "mouseout",
            condition: a
        },
        mousewheel: {
            base: (Browser.Engine.gecko) ? "DOMMouseScroll" : "mousewheel"
        }
    });
})();
Element.Properties.styles = {
    set: function (a) {
        this.setStyles(a);
    }
};
Element.Properties.opacity = {
    set: function (a, b) {
        if (!b) {
            if (a == 0) {
                if (this.style.visibility != "hidden") {
                    this.style.visibility = "hidden";
                }
            } else {
                if (this.style.visibility != "visible") {
                    this.style.visibility = "visible";
                }
            }
        }
        if (!this.currentStyle || !this.currentStyle.hasLayout) {
            this.style.zoom = 1;
        }
        if (Browser.Engine.trident) {
            this.style.filter = (a == 1) ? "" : "alpha(opacity=" + a * 100 + ")";
        }
        this.style.opacity = a;
        this.store("opacity", a);
    },
    get: function () {
        return this.retrieve("opacity", 1);
    }
};
Element.implement({
    setOpacity: function (a) {
        return this.set("opacity", a, true);
    },
    getOpacity: function () {
        return this.get("opacity");
    },
    setStyle: function (b, a) {
        switch (b) {
        case "opacity":
            return this.set("opacity", parseFloat(a));
        case "float":
            b = (Browser.Engine.trident) ? "styleFloat" : "cssFloat";
        }
        b = b.camelCase();
        if ($type(a) != "string") {
            var c = (Element.Styles.get(b) || "@").split(" ");
            a = $splat(a).map(function (e, d) {
                if (!c[d]) {
                    return "";
                }
                return ($type(e) == "number") ? c[d].replace("@", Math.round(e)) : e;
            }).join(" ");
        } else {
            if (a == String(Number(a))) {
                a = Math.round(a);
            }
        }
        this.style[b] = a;
        return this;
    },
    getStyle: function (g) {
        switch (g) {
        case "opacity":
            return this.get("opacity");
        case "float":
            g = (Browser.Engine.trident) ? "styleFloat" : "cssFloat";
        }
        g = g.camelCase();
        var a = this.style[g];
        if (!$chk(a)) {
            a = [];
            for (var f in Element.ShortStyles) {
                if (g != f) {
                    continue;
                }
                for (var e in Element.ShortStyles[f]) {
                    a.push(this.getStyle(e));
                }
                return a.join(" ");
            }
            a = this.getComputedStyle(g);
        }
        if (a) {
            a = String(a);
            var c = a.match(/rgba?\([\d\s,]+\)/);
            if (c) {
                a = a.replace(c[0], c[0].rgbToHex());
            }
        }
        if (Browser.Engine.presto || (Browser.Engine.trident && !$chk(parseInt(a, 10)))) {
            if (g.test(/^(height|width)$/)) {
                var b = (g == "width") ? ["left", "right"] : ["top", "bottom"],
                    d = 0;
                b.each(function (h) {
                    d += this.getStyle("border-" + h + "-width").toInt() + this.getStyle("padding-" + h).toInt();
                }, this);
                return this["offset" + g.capitalize()] - d + "px";
            }
            if ((Browser.Engine.presto) && String(a).test("px")) {
                return a;
            }
            if (g.test(/(border(.+)Width|margin|padding)/)) {
                return "0px";
            }
        }
        return a;
    },
    setStyles: function (b) {
        for (var a in b) {
            this.setStyle(a, b[a]);
        }
        return this;
    },
    getStyles: function () {
        var a = {};
        Array.flatten(arguments).each(function (b) {
            a[b] = this.getStyle(b);
        }, this);
        return a;
    }
});
Element.Styles = new Hash({
    left: "@px",
    top: "@px",
    bottom: "@px",
    right: "@px",
    width: "@px",
    height: "@px",
    maxWidth: "@px",
    maxHeight: "@px",
    minWidth: "@px",
    minHeight: "@px",
    backgroundColor: "rgb(@, @, @)",
    backgroundPosition: "@px @px",
    color: "rgb(@, @, @)",
    fontSize: "@px",
    letterSpacing: "@px",
    lineHeight: "@px",
    clip: "rect(@px @px @px @px)",
    margin: "@px @px @px @px",
    padding: "@px @px @px @px",
    border: "@px @ rgb(@, @, @) @px @ rgb(@, @, @) @px @ rgb(@, @, @)",
    borderWidth: "@px @px @px @px",
    borderStyle: "@ @ @ @",
    borderColor: "rgb(@, @, @) rgb(@, @, @) rgb(@, @, @) rgb(@, @, @)",
    zIndex: "@",
    zoom: "@",
    fontWeight: "@",
    textIndent: "@px",
    opacity: "@"
});
Element.ShortStyles = {
    margin: {},
    padding: {},
    border: {},
    borderWidth: {},
    borderStyle: {},
    borderColor: {}
};
["Top", "Right", "Bottom", "Left"].each(function (g) {
    var f = Element.ShortStyles;
    var b = Element.Styles;
    ["margin", "padding"].each(function (h) {
        var i = h + g;
        f[h][i] = b[i] = "@px";
    });
    var e = "border" + g;
    f.border[e] = b[e] = "@px @ rgb(@, @, @)";
    var d = e + "Width",
        a = e + "Style",
        c = e + "Color";
    f[e] = {};
    f.borderWidth[d] = f[e][d] = b[d] = "@px";
    f.borderStyle[a] = f[e][a] = b[a] = "@";
    f.borderColor[c] = f[e][c] = b[c] = "rgb(@, @, @)";
});
(function () {
    Element.implement({
        scrollTo: function (h, i) {
            if (b(this)) {
                this.getWindow().scrollTo(h, i);
            } else {
                this.scrollLeft = h;
                this.scrollTop = i;
            }
            return this;
        },
        getSize: function () {
            if (b(this)) {
                return this.getWindow().getSize();
            }
            return {
                x: this.offsetWidth,
                y: this.offsetHeight
            };
        },
        getScrollSize: function () {
            if (b(this)) {
                return this.getWindow().getScrollSize();
            }
            return {
                x: this.scrollWidth,
                y: this.scrollHeight
            };
        },
        getScroll: function () {
            if (b(this)) {
                return this.getWindow().getScroll();
            }
            return {
                x: this.scrollLeft,
                y: this.scrollTop
            };
        },
        getScrolls: function () {
            var i = this,
                h = {
                    x: 0,
                    y: 0
                };
            while (i && !b(i)) {
                h.x += i.scrollLeft;
                h.y += i.scrollTop;
                i = i.parentNode;
            }
            return h;
        },
        getOffsetParent: function () {
            var h = this;
            if (b(h)) {
                return null;
            }
            if (!Browser.Engine.trident) {
                return h.offsetParent;
            }
            while ((h = h.parentNode) && !b(h)) {
                if (d(h, "position") != "static") {
                    return h;
                }
            }
            return null;
        },
        getOffsets: function () {
            if (this.getBoundingClientRect) {
                var j = this.getBoundingClientRect(),
                    m = document.id(this.getDocument().documentElement),
                    p = m.getScroll(),
                    k = this.getScrolls(),
                    i = this.getScroll(),
                    h = (d(this, "position") == "fixed");
                return {
                    x: j.left.toInt() + k.x - i.x + ((h) ? 0 : p.x) - m.clientLeft,
                    y: j.top.toInt() + k.y - i.y + ((h) ? 0 : p.y) - m.clientTop
                };
            }
            var l = this,
                n = {
                    x: 0,
                    y: 0
                };
            if (b(this)) {
                return n;
            }
            while (l && !b(l)) {
                n.x += l.offsetLeft;
                n.y += l.offsetTop;
                if (Browser.Engine.gecko) {
                    if (!f(l)) {
                        n.x += c(l);
                        n.y += g(l);
                    }
                    var o = l.parentNode;
                    if (o && d(o, "overflow") != "visible") {
                        n.x += c(o);
                        n.y += g(o);
                    }
                } else {
                    if (l != this && Browser.Engine.webkit) {
                        n.x += c(l);
                        n.y += g(l);
                    }
                }
                l = l.offsetParent;
            }
            if (Browser.Engine.gecko && !f(this)) {
                n.x -= c(this);
                n.y -= g(this);
            }
            return n;
        },
        getPosition: function (k) {
            if (b(this)) {
                return {
                    x: 0,
                    y: 0
                };
            }
            var l = this.getOffsets(),
                i = this.getScrolls();
            var h = {
                x: l.x - i.x,
                y: l.y - i.y
            };
            var j = (k && (k = document.id(k))) ? k.getPosition() : {
                x: 0,
                y: 0
            };
            return {
                x: h.x - j.x,
                y: h.y - j.y
            };
        },
        getCoordinates: function (j) {
            if (b(this)) {
                return this.getWindow().getCoordinates();
            }
            var h = this.getPosition(j),
                i = this.getSize();
            var k = {
                left: h.x,
                top: h.y,
                width: i.x,
                height: i.y
            };
            k.right = k.left + k.width;
            k.bottom = k.top + k.height;
            return k;
        },
        computePosition: function (h) {
            return {
                left: h.x - e(this, "margin-left"),
                top: h.y - e(this, "margin-top")
            };
        },
        setPosition: function (h) {
            return this.setStyles(this.computePosition(h));
        }
    });
    Native.implement([Document, Window], {
        getSize: function () {
            if (Browser.Engine.presto || Browser.Engine.webkit) {
                var i = this.getWindow();
                return {
                    x: i.innerWidth,
                    y: i.innerHeight
                };
            }
            var h = a(this);
            return {
                x: h.clientWidth,
                y: h.clientHeight
            };
        },
        getScroll: function () {
            var i = this.getWindow(),
                h = a(this);
            return {
                x: i.pageXOffset || h.scrollLeft,
                y: i.pageYOffset || h.scrollTop
            };
        },
        getScrollSize: function () {
            var i = a(this),
                h = this.getSize();
            return {
                x: Math.max(i.scrollWidth, h.x),
                y: Math.max(i.scrollHeight, h.y)
            };
        },
        getPosition: function () {
            return {
                x: 0,
                y: 0
            };
        },
        getCoordinates: function () {
            var h = this.getSize();
            return {
                top: 0,
                left: 0,
                bottom: h.y,
                right: h.x,
                height: h.y,
                width: h.x
            };
        }
    });
    var d = Element.getComputedStyle;

    function e(h, i) {
        return d(h, i).toInt() || 0;
    }

    function f(h) {
        return d(h, "-moz-box-sizing") == "border-box";
    }

    function g(h) {
        return e(h, "border-top-width");
    }

    function c(h) {
        return e(h, "border-left-width");
    }

    function b(h) {
        return (/^(?:body|html)$/i).test(h.tagName);
    }

    function a(h) {
        var i = h.getDocument();
        return (!i.compatMode || i.compatMode == "CSS1Compat") ? i.html : i.body;
    }
})();
Element.alias("setPosition", "position");
Native.implement([Window, Document, Element], {
    getHeight: function () {
        return this.getSize().y;
    },
    getWidth: function () {
        return this.getSize().x;
    },
    getScrollTop: function () {
        return this.getScroll().y;
    },
    getScrollLeft: function () {
        return this.getScroll().x;
    },
    getScrollHeight: function () {
        return this.getScrollSize().y;
    },
    getScrollWidth: function () {
        return this.getScrollSize().x;
    },
    getTop: function () {
        return this.getPosition().y;
    },
    getLeft: function () {
        return this.getPosition().x;
    }
});
Native.implement([Document, Element], {
    getElements: function (h, g) {
        h = h.split(",");
        var c, e = {};
        for (var d = 0, b = h.length; d < b; d++) {
            var a = h[d],
                f = Selectors.Utils.search(this, a, e);
            if (d != 0 && f.item) {
                f = $A(f);
            }
            c = (d == 0) ? f : (c.item) ? $A(c).concat(f) : c.concat(f);
        }
        return new Elements(c, {
            ddup: (h.length > 1),
            cash: !g
        });
    }
});
Element.implement({
    match: function (b) {
        if (!b || (b == this)) {
            return true;
        }
        var d = Selectors.Utils.parseTagAndID(b);
        var a = d[0],
            e = d[1];
        if (!Selectors.Filters.byID(this, e) || !Selectors.Filters.byTag(this, a)) {
            return false;
        }
        var c = Selectors.Utils.parseSelector(b);
        return (c) ? Selectors.Utils.filter(this, c, {}) : true;
    }
});
var Selectors = {
    Cache: {
        nth: {},
        parsed: {}
    }
};
Selectors.RegExps = {
    id: (/#([\w-]+)/),
    tag: (/^(\w+|\*)/),
    quick: (/^(\w+|\*)$/),
    splitter: (/\s*([+>~\s])\s*([a-zA-Z#.*:\[])/g),
    combined: (/\.([\w-]+)|\[(\w+)(?:([!*^$~|]?=)(["']?)([^\4]*?)\4)?\]|:([\w-]+)(?:\(["']?(.*?)?["']?\)|$)/g)
};
Selectors.Utils = {
    chk: function (b, c) {
        if (!c) {
            return true;
        }
        var a = $uid(b);
        if (!c[a]) {
            return c[a] = true;
        }
        return false;
    },
    parseNthArgument: function (h) {
        if (Selectors.Cache.nth[h]) {
            return Selectors.Cache.nth[h];
        }
        var e = h.match(/^([+-]?\d*)?([a-z]+)?([+-]?\d*)?$/);
        if (!e) {
            return false;
        }
        var g = parseInt(e[1], 10);
        var d = (g || g === 0) ? g : 1;
        var f = e[2] || false;
        var c = parseInt(e[3], 10) || 0;
        if (d != 0) {
            c--;
            while (c < 1) {
                c += d;
            }
            while (c >= d) {
                c -= d;
            }
        } else {
            d = c;
            f = "index";
        }
        switch (f) {
        case "n":
            e = {
                a: d,
                b: c,
                special: "n"
            };
            break;
        case "odd":
            e = {
                a: 2,
                b: 0,
                special: "n"
            };
            break;
        case "even":
            e = {
                a: 2,
                b: 1,
                special: "n"
            };
            break;
        case "first":
            e = {
                a: 0,
                special: "index"
            };
            break;
        case "last":
            e = {
                special: "last-child"
            };
            break;
        case "only":
            e = {
                special: "only-child"
            };
            break;
        default:
            e = {
                a: (d - 1),
                special: "index"
            };
        }
        return Selectors.Cache.nth[h] = e;
    },
    parseSelector: function (e) {
        if (Selectors.Cache.parsed[e]) {
            return Selectors.Cache.parsed[e];
        }
        var d, h = {
                classes: [],
                pseudos: [],
                attributes: []
            };
        while ((d = Selectors.RegExps.combined.exec(e))) {
            var i = d[1],
                g = d[2],
                f = d[3],
                b = d[5],
                c = d[6],
                j = d[7];
            if (i) {
                h.classes.push(i);
            } else {
                if (c) {
                    var a = Selectors.Pseudo.get(c);
                    if (a) {
                        h.pseudos.push({
                            parser: a,
                            argument: j
                        });
                    } else {
                        h.attributes.push({
                            name: c,
                            operator: "=",
                            value: j
                        });
                    }
                } else {
                    if (g) {
                        h.attributes.push({
                            name: g,
                            operator: f,
                            value: b
                        });
                    }
                }
            }
        }
        if (!h.classes.length) {
            delete h.classes;
        }
        if (!h.attributes.length) {
            delete h.attributes;
        }
        if (!h.pseudos.length) {
            delete h.pseudos;
        }
        if (!h.classes && !h.attributes && !h.pseudos) {
            h = null;
        }
        return Selectors.Cache.parsed[e] = h;
    },
    parseTagAndID: function (b) {
        var a = b.match(Selectors.RegExps.tag);
        var c = b.match(Selectors.RegExps.id);
        return [(a) ? a[1] : "*", (c) ? c[1] : false];
    },
    filter: function (f, c, e) {
        var d;
        if (c.classes) {
            for (d = c.classes.length; d--; d) {
                var g = c.classes[d];
                if (!Selectors.Filters.byClass(f, g)) {
                    return false;
                }
            }
        }
        if (c.attributes) {
            for (d = c.attributes.length; d--; d) {
                var b = c.attributes[d];
                if (!Selectors.Filters.byAttribute(f, b.name, b.operator, b.value)) {
                    return false;
                }
            }
        }
        if (c.pseudos) {
            for (d = c.pseudos.length; d--; d) {
                var a = c.pseudos[d];
                if (!Selectors.Filters.byPseudo(f, a.parser, a.argument, e)) {
                    return false;
                }
            }
        }
        return true;
    },
    getByTagAndID: function (b, a, d) {
        if (d) {
            var c = (b.getElementById) ? b.getElementById(d, true) : Element.getElementById(b, d, true);
            return (c && Selectors.Filters.byTag(c, a)) ? [c] : [];
        } else {
            return b.getElementsByTagName(a);
        }
    },
    search: function (o, h, t) {
        var b = [];
        var c = h.trim().replace(Selectors.RegExps.splitter, function (k, j, i) {
            b.push(j);
            return ":)" + i;
        }).split(":)");
        var p, e, A;
        for (var z = 0, v = c.length; z < v; z++) {
            var y = c[z];
            if (z == 0 && Selectors.RegExps.quick.test(y)) {
                p = o.getElementsByTagName(y);
                continue;
            }
            var a = b[z - 1];
            var q = Selectors.Utils.parseTagAndID(y);
            var B = q[0],
                r = q[1];
            if (z == 0) {
                p = Selectors.Utils.getByTagAndID(o, B, r);
            } else {
                var d = {}, g = [];
                for (var x = 0, w = p.length; x < w; x++) {
                    g = Selectors.Getters[a](g, p[x], B, r, d);
                }
                p = g;
            }
            var f = Selectors.Utils.parseSelector(y);
            if (f) {
                e = [];
                for (var u = 0, s = p.length; u < s; u++) {
                    A = p[u];
                    if (Selectors.Utils.filter(A, f, t)) {
                        e.push(A);
                    }
                }
                p = e;
            }
        }
        return p;
    }
};
Selectors.Getters = {
    " ": function (h, g, j, a, e) {
        var d = Selectors.Utils.getByTagAndID(g, j, a);
        for (var c = 0, b = d.length; c < b; c++) {
            var f = d[c];
            if (Selectors.Utils.chk(f, e)) {
                h.push(f);
            }
        }
        return h;
    },
    ">": function (h, g, j, a, f) {
        var c = Selectors.Utils.getByTagAndID(g, j, a);
        for (var e = 0, d = c.length; e < d; e++) {
            var b = c[e];
            if (b.parentNode == g && Selectors.Utils.chk(b, f)) {
                h.push(b);
            }
        }
        return h;
    },
    "+": function (c, b, a, e, d) {
        while ((b = b.nextSibling)) {
            if (b.nodeType == 1) {
                if (Selectors.Utils.chk(b, d) && Selectors.Filters.byTag(b, a) && Selectors.Filters.byID(b, e)) {
                    c.push(b);
                }
                break;
            }
        }
        return c;
    },
    "~": function (c, b, a, e, d) {
        while ((b = b.nextSibling)) {
            if (b.nodeType == 1) {
                if (!Selectors.Utils.chk(b, d)) {
                    break;
                }
                if (Selectors.Filters.byTag(b, a) && Selectors.Filters.byID(b, e)) {
                    c.push(b);
                }
            }
        }
        return c;
    }
};
Selectors.Filters = {
    byTag: function (b, a) {
        return (a == "*" || (b.tagName && b.tagName.toLowerCase() == a));
    },
    byID: function (a, b) {
        return (!b || (a.id && a.id == b));
    },
    byClass: function (b, a) {
        return (b.className && b.className.contains && b.className.contains(a, " "));
    },
    byPseudo: function (a, d, c, b) {
        return d.call(a, c, b);
    },
    byAttribute: function (c, d, b, e) {
        var a = Element.prototype.getProperty.call(c, d);
        if (!a) {
            return (b == "!=");
        }
        if (!b || e == undefined) {
            return true;
        }
        switch (b) {
        case "=":
            return (a == e);
        case "*=":
            return (a.contains(e));
        case "^=":
            return (a.substr(0, e.length) == e);
        case "$=":
            return (a.substr(a.length - e.length) == e);
        case "!=":
            return (a != e);
        case "~=":
            return a.contains(e, " ");
        case "|=":
            return a.contains(e, "-");
        }
        return false;
    }
};
Selectors.Pseudo = new Hash({
    checked: function () {
        return this.checked;
    },
    empty: function () {
        return !(this.innerText || this.textContent || "").length;
    },
    not: function (a) {
        return !Element.match(this, a);
    },
    contains: function (a) {
        return (this.innerText || this.textContent || "").contains(a);
    },
    "first-child": function () {
        return Selectors.Pseudo.index.call(this, 0);
    },
    "last-child": function () {
        var a = this;
        while ((a = a.nextSibling)) {
            if (a.nodeType == 1) {
                return false;
            }
        }
        return true;
    },
    "only-child": function () {
        var b = this;
        while ((b = b.previousSibling)) {
            if (b.nodeType == 1) {
                return false;
            }
        }
        var a = this;
        while ((a = a.nextSibling)) {
            if (a.nodeType == 1) {
                return false;
            }
        }
        return true;
    },
    "nth-child": function (g, e) {
        g = (g == undefined) ? "n" : g;
        var c = Selectors.Utils.parseNthArgument(g);
        if (c.special != "n") {
            return Selectors.Pseudo[c.special].call(this, c.a, e);
        }
        var f = 0;
        e.positions = e.positions || {};
        var d = $uid(this);
        if (!e.positions[d]) {
            var b = this;
            while ((b = b.previousSibling)) {
                if (b.nodeType != 1) {
                    continue;
                }
                f++;
                var a = e.positions[$uid(b)];
                if (a != undefined) {
                    f = a + f;
                    break;
                }
            }
            e.positions[d] = f;
        }
        return (e.positions[d] % c.a == c.b);
    },
    index: function (a) {
        var b = this,
            c = 0;
        while ((b = b.previousSibling)) {
            if (b.nodeType == 1 && ++c > a) {
                return false;
            }
        }
        return (c == a);
    },
    even: function (b, a) {
        return Selectors.Pseudo["nth-child"].call(this, "2n+1", a);
    },
    odd: function (b, a) {
        return Selectors.Pseudo["nth-child"].call(this, "2n", a);
    },
    selected: function () {
        return this.selected;
    },
    enabled: function () {
        return (this.disabled === false);
    }
});
Element.Events.domready = {
    onAdd: function (a) {
        if (Browser.loaded) {
            a.call(this);
        }
    }
};
(function () {
    var b = function () {
        if (Browser.loaded) {
            return;
        }
        Browser.loaded = true;
        window.fireEvent("domready");
        document.fireEvent("domready");
    };
    window.addEvent("load", b);
    if (Browser.Engine.trident) {
        var a = document.createElement("div");
        (function () {
            ($try(function () {
                a.doScroll();
                return document.id(a).inject(document.body).set("html", "temp").dispose();
            })) ? b() : arguments.callee.delay(50);
        })();
    } else {
        if (Browser.Engine.webkit && Browser.Engine.version < 525) {
            (function () {
                (["loaded", "complete"].contains(document.readyState)) ? b() : arguments.callee.delay(50);
            })();
        } else {
            document.addEvent("DOMContentLoaded", b);
        }
    }
})();
var JSON = new Hash(this.JSON && {
    stringify: JSON.stringify,
    parse: JSON.parse
}).extend({
    $specialChars: {
        "\b": "\\b",
        "\t": "\\t",
        "\n": "\\n",
        "\f": "\\f",
        "\r": "\\r",
        '"': '\\"',
        "\\": "\\\\"
    },
    $replaceChars: function (a) {
        return JSON.$specialChars[a] || "\\u00" + Math.floor(a.charCodeAt() / 16).toString(16) + (a.charCodeAt() % 16).toString(16);
    },
    encode: function (b) {
        switch ($type(b)) {
        case "string":
            return '"' + b.replace(/[\x00-\x1f\\"]/g, JSON.$replaceChars) + '"';
        case "array":
            return "[" + String(b.map(JSON.encode).clean()) + "]";
        case "object":
        case "hash":
            var a = [];
            Hash.each(b, function (e, d) {
                var c = JSON.encode(e);
                if (c) {
                    a.push(JSON.encode(d) + ":" + c);
                }
            });
            return "{" + a + "}";
        case "number":
        case "boolean":
            return String(b);
        case false:
            return "null";
        }
        return null;
    },
    decode: function (string, secure) {
        if ($type(string) != "string" || !string.length) {
            return null;
        }
        if (secure && !(/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(string.replace(/\\./g, "@").replace(/"[^"\\\n\r]*"/g, ""))) {
            return null;
        }
        return eval("(" + string + ")");
    }
});
Native.implement([Hash, Array, String, Number], {
    toJSON: function () {
        return JSON.encode(this);
    }
});
var Cookie = new Class({
    Implements: Options,
    options: {
        path: false,
        domain: false,
        duration: false,
        secure: false,
        document: document
    },
    initialize: function (b, a) {
        this.key = b;
        this.setOptions(a);
    },
    write: function (b) {
        b = encodeURIComponent(b);
        if (this.options.domain) {
            b += "; domain=" + this.options.domain;
        }
        if (this.options.path) {
            b += "; path=" + this.options.path;
        }
        if (this.options.duration) {
            var a = new Date();
            a.setTime(a.getTime() + this.options.duration * 24 * 60 * 60 * 1000);
            b += "; expires=" + a.toGMTString();
        }
        if (this.options.secure) {
            b += "; secure";
        }
        this.options.document.cookie = this.key + "=" + b;
        return this;
    },
    read: function () {
        var a = this.options.document.cookie.match("(?:^|;)\\s*" + this.key.escapeRegExp() + "=([^;]*)");
        return (a) ? decodeURIComponent(a[1]) : null;
    },
    dispose: function () {
        new Cookie(this.key, $merge(this.options, {
            duration: -1
        })).write("");
        return this;
    }
});
Cookie.write = function (b, c, a) {
    return new Cookie(b, a).write(c);
};
Cookie.read = function (a) {
    return new Cookie(a).read();
};
Cookie.dispose = function (b, a) {
    return new Cookie(b, a).dispose();
};
var Swiff = new Class({
    Implements: [Options],
    options: {
        id: null,
        height: 1,
        width: 1,
        container: null,
        properties: {},
        params: {
            quality: "high",
            allowScriptAccess: "always",
            wMode: "transparent",
            swLiveConnect: true
        },
        callBacks: {},
        vars: {}
    },
    toElement: function () {
        return this.object;
    },
    initialize: function (l, m) {
        this.instance = "Swiff_" + $time();
        this.setOptions(m);
        m = this.options;
        var b = this.id = m.id || this.instance;
        var a = document.id(m.container);
        Swiff.CallBacks[this.instance] = {};
        var e = m.params,
            g = m.vars,
            f = m.callBacks;
        var h = $extend({
            height: m.height,
            width: m.width
        }, m.properties);
        var k = this;
        for (var d in f) {
            Swiff.CallBacks[this.instance][d] = (function (n) {
                return function () {
                    return n.apply(k.object, arguments);
                };
            })(f[d]);
            g[d] = "Swiff.CallBacks." + this.instance + "." + d;
        }
        e.flashVars = Hash.toQueryString(g);
        if (Browser.Engine.trident) {
            h.classid = "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000";
            e.movie = l;
        } else {
            h.type = "application/x-shockwave-flash";
            h.data = l;
        }
        var j = '<object id="' + b + '"';
        for (var i in h) {
            j += " " + i + '="' + h[i] + '"';
        }
        j += ">";
        for (var c in e) {
            if (e[c]) {
                j += '<param name="' + c + '" value="' + e[c] + '" />';
            }
        }
        j += "</object>";
        this.object = ((a) ? a.empty() : new Element("div")).set("html", j).firstChild;
    },
    replaces: function (a) {
        a = document.id(a, true);
        a.parentNode.replaceChild(this.toElement(), a);
        return this;
    },
    inject: function (a) {
        document.id(a, true).appendChild(this.toElement());
        return this;
    },
    remote: function () {
        return Swiff.remote.apply(Swiff, [this.toElement()].extend(arguments));
    }
});
Swiff.CallBacks = {};
Swiff.remote = function (obj, fn) {
    var rs = obj.CallFunction('<invoke name="' + fn + '" returntype="javascript">' + __flash__argumentsToXML(arguments, 2) + "</invoke>");
    return eval(rs);
};
var Fx = new Class({
    Implements: [Chain, Events, Options],
    options: {
        fps: 50,
        unit: false,
        duration: 500,
        link: "ignore"
    },
    initialize: function (a) {
        this.subject = this.subject || this;
        this.setOptions(a);
        this.options.duration = Fx.Durations[this.options.duration] || this.options.duration.toInt();
        var b = this.options.wait;
        if (b === false) {
            this.options.link = "cancel";
        }
    },
    getTransition: function () {
        return function (a) {
            return -(Math.cos(Math.PI * a) - 1) / 2;
        };
    },
    step: function () {
        var a = $time();
        if (a < this.time + this.options.duration) {
            var b = this.transition((a - this.time) / this.options.duration);
            this.set(this.compute(this.from, this.to, b));
        } else {
            this.set(this.compute(this.from, this.to, 1));
            this.complete();
        }
    },
    set: function (a) {
        return a;
    },
    compute: function (c, b, a) {
        return Fx.compute(c, b, a);
    },
    check: function () {
        if (!this.timer) {
            return true;
        }
        switch (this.options.link) {
        case "cancel":
            this.cancel();
            return true;
        case "chain":
            this.chain(this.caller.bind(this, arguments));
            return false;
        }
        return false;
    },
    start: function (b, a) {
        if (!this.check(b, a)) {
            return this;
        }
        this.from = b;
        this.to = a;
        this.time = 0;
        this.transition = this.getTransition();
        this.startTimer();
        this.onStart();
        return this;
    },
    complete: function () {
        if (this.stopTimer()) {
            this.onComplete();
        }
        return this;
    },
    cancel: function () {
        if (this.stopTimer()) {
            this.onCancel();
        }
        return this;
    },
    onStart: function () {
        this.fireEvent("start", this.subject);
    },
    onComplete: function () {
        this.fireEvent("complete", this.subject);
        if (!this.callChain()) {
            this.fireEvent("chainComplete", this.subject);
        }
    },
    onCancel: function () {
        this.fireEvent("cancel", this.subject).clearChain();
    },
    pause: function () {
        this.stopTimer();
        return this;
    },
    resume: function () {
        this.startTimer();
        return this;
    },
    stopTimer: function () {
        if (!this.timer) {
            return false;
        }
        this.time = $time() - this.time;
        this.timer = $clear(this.timer);
        return true;
    },
    startTimer: function () {
        if (this.timer) {
            return false;
        }
        this.time = $time() - this.time;
        this.timer = this.step.periodical(Math.round(1000 / this.options.fps), this);
        return true;
    }
});
Fx.compute = function (c, b, a) {
    return (b - c) * a + c;
};
Fx.Durations = {
    "short": 250,
    normal: 500,
    "long": 1000
};
Fx.CSS = new Class({
    Extends: Fx,
    prepare: function (d, e, b) {
        b = $splat(b);
        var c = b[1];
        if (!$chk(c)) {
            b[1] = b[0];
            b[0] = d.getStyle(e);
        }
        var a = b.map(this.parse);
        return {
            from: a[0],
            to: a[1]
        };
    },
    parse: function (a) {
        a = $lambda(a)();
        a = (typeof a == "string") ? a.split(" ") : $splat(a);
        return a.map(function (c) {
            c = String(c);
            var b = false;
            Fx.CSS.Parsers.each(function (f, e) {
                if (b) {
                    return;
                }
                var d = f.parse(c);
                if ($chk(d)) {
                    b = {
                        value: d,
                        parser: f
                    };
                }
            });
            b = b || {
                value: c,
                parser: Fx.CSS.Parsers.String
            };
            return b;
        });
    },
    compute: function (d, c, b) {
        var a = [];
        (Math.min(d.length, c.length)).times(function (e) {
            a.push({
                value: d[e].parser.compute(d[e].value, c[e].value, b),
                parser: d[e].parser
            });
        });
        a.$family = {
            name: "fx:css:value"
        };
        return a;
    },
    serve: function (c, b) {
        if ($type(c) != "fx:css:value") {
            c = this.parse(c);
        }
        var a = [];
        c.each(function (d) {
            a = a.concat(d.parser.serve(d.value, b));
        });
        return a;
    },
    render: function (a, d, c, b) {
        a.setStyle(d, this.serve(c, b));
    },
    search: function (a) {
        if (Fx.CSS.Cache[a]) {
            return Fx.CSS.Cache[a];
        }
        var b = {};
        Array.each(document.styleSheets, function (e, d) {
            var c = e.href;
            if (c && c.contains("://") && !c.contains(document.domain)) {
                return;
            }
            var f = e.rules || e.cssRules;
            Array.each(f, function (j, g) {
                if (!j.style) {
                    return;
                }
                var h = (j.selectorText) ? j.selectorText.replace(/^\w+/, function (i) {
                    return i.toLowerCase();
                }) : null;
                if (!h || !h.test("^" + a + "$")) {
                    return;
                }
                Element.Styles.each(function (k, i) {
                    if (!j.style[i] || Element.ShortStyles[i]) {
                        return;
                    }
                    k = String(j.style[i]);
                    b[i] = (k.test(/^rgb/)) ? k.rgbToHex() : k;
                });
            });
        });
        return Fx.CSS.Cache[a] = b;
    }
});
Fx.CSS.Cache = {};
Fx.CSS.Parsers = new Hash({
    Color: {
        parse: function (a) {
            if (a.match(/^#[0-9a-f]{3,6}$/i)) {
                return a.hexToRgb(true);
            }
            return ((a = a.match(/(\d+),\s*(\d+),\s*(\d+)/))) ? [a[1], a[2], a[3]] : false;
        },
        compute: function (c, b, a) {
            return c.map(function (e, d) {
                return Math.round(Fx.compute(c[d], b[d], a));
            });
        },
        serve: function (a) {
            return a.map(Number);
        }
    },
    Number: {
        parse: parseFloat,
        compute: Fx.compute,
        serve: function (b, a) {
            return (a) ? b + a : b;
        }
    },
    String: {
        parse: $lambda(false),
        compute: $arguments(1),
        serve: $arguments(0)
    }
});
Fx.Tween = new Class({
    Extends: Fx.CSS,
    initialize: function (b, a) {
        this.element = this.subject = document.id(b);
        this.parent(a);
    },
    set: function (b, a) {
        if (arguments.length == 1) {
            a = b;
            b = this.property || this.options.property;
        }
        this.render(this.element, b, a, this.options.unit);
        return this;
    },
    start: function (c, e, d) {
        if (!this.check(c, e, d)) {
            return this;
        }
        var b = Array.flatten(arguments);
        this.property = this.options.property || b.shift();
        var a = this.prepare(this.element, this.property, b);
        return this.parent(a.from, a.to);
    }
});
Element.Properties.tween = {
    set: function (a) {
        var b = this.retrieve("tween");
        if (b) {
            b.cancel();
        }
        return this.eliminate("tween").store("tween:options", $extend({
            link: "cancel"
        }, a));
    },
    get: function (a) {
        if (a || !this.retrieve("tween")) {
            if (a || !this.retrieve("tween:options")) {
                this.set("tween", a);
            }
            this.store("tween", new Fx.Tween(this, this.retrieve("tween:options")));
        }
        return this.retrieve("tween");
    }
};
Element.implement({
    tween: function (a, c, b) {
        this.get("tween").start(arguments);
        return this;
    },
    fade: function (c) {
        var e = this.get("tween"),
            d = "opacity",
            a;
        c = $pick(c, "toggle");
        switch (c) {
        case "in":
            e.start(d, 1);
            break;
        case "out":
            e.start(d, 0);
            break;
        case "show":
            e.set(d, 1);
            break;
        case "hide":
            e.set(d, 0);
            break;
        case "toggle":
            var b = this.retrieve("fade:flag", this.get("opacity") == 1);
            e.start(d, (b) ? 0 : 1);
            this.store("fade:flag", !b);
            a = true;
            break;
        default:
            e.start(d, arguments);
        }
        if (!a) {
            this.eliminate("fade:flag");
        }
        return this;
    },
    highlight: function (c, a) {
        if (!a) {
            a = this.retrieve("highlight:original", this.getStyle("background-color"));
            a = (a == "transparent") ? "#fff" : a;
        }
        var b = this.get("tween");
        b.start("background-color", c || "#ffff88", a).chain(function () {
            this.setStyle("background-color", this.retrieve("highlight:original"));
            b.callChain();
        }.bind(this));
        return this;
    }
});
Fx.Morph = new Class({
    Extends: Fx.CSS,
    initialize: function (b, a) {
        this.element = this.subject = document.id(b);
        this.parent(a);
    },
    set: function (a) {
        if (typeof a == "string") {
            a = this.search(a);
        }
        for (var b in a) {
            this.render(this.element, b, a[b], this.options.unit);
        }
        return this;
    },
    compute: function (e, d, c) {
        var a = {};
        for (var b in e) {
            a[b] = this.parent(e[b], d[b], c);
        }
        return a;
    },
    start: function (b) {
        if (!this.check(b)) {
            return this;
        }
        if (typeof b == "string") {
            b = this.search(b);
        }
        var e = {}, d = {};
        for (var c in b) {
            var a = this.prepare(this.element, c, b[c]);
            e[c] = a.from;
            d[c] = a.to;
        }
        return this.parent(e, d);
    }
});
Element.Properties.morph = {
    set: function (a) {
        var b = this.retrieve("morph");
        if (b) {
            b.cancel();
        }
        return this.eliminate("morph").store("morph:options", $extend({
            link: "cancel"
        }, a));
    },
    get: function (a) {
        if (a || !this.retrieve("morph")) {
            if (a || !this.retrieve("morph:options")) {
                this.set("morph", a);
            }
            this.store("morph", new Fx.Morph(this, this.retrieve("morph:options")));
        }
        return this.retrieve("morph");
    }
};
Element.implement({
    morph: function (a) {
        this.get("morph").start(a);
        return this;
    }
});
Fx.implement({
    getTransition: function () {
        var a = this.options.transition || Fx.Transitions.Sine.easeInOut;
        if (typeof a == "string") {
            var b = a.split(":");
            a = Fx.Transitions;
            a = a[b[0]] || a[b[0].capitalize()];
            if (b[1]) {
                a = a["ease" + b[1].capitalize() + (b[2] ? b[2].capitalize() : "")];
            }
        }
        return a;
    }
});
Fx.Transition = function (b, a) {
    a = $splat(a);
    return $extend(b, {
        easeIn: function (c) {
            return b(c, a);
        },
        easeOut: function (c) {
            return 1 - b(1 - c, a);
        },
        easeInOut: function (c) {
            return (c <= 0.5) ? b(2 * c, a) / 2 : (2 - b(2 * (1 - c), a)) / 2;
        }
    });
};
Fx.Transitions = new Hash({
    linear: $arguments(0)
});
Fx.Transitions.extend = function (a) {
    for (var b in a) {
        Fx.Transitions[b] = new Fx.Transition(a[b]);
    }
};
Fx.Transitions.extend({
    Pow: function (b, a) {
        return Math.pow(b, a[0] || 6);
    },
    Expo: function (a) {
        return Math.pow(2, 8 * (a - 1));
    },
    Circ: function (a) {
        return 1 - Math.sin(Math.acos(a));
    },
    Sine: function (a) {
        return 1 - Math.sin((1 - a) * Math.PI / 2);
    },
    Back: function (b, a) {
        a = a[0] || 1.618;
        return Math.pow(b, 2) * ((a + 1) * b - a);
    },
    Bounce: function (f) {
        var e;
        for (var d = 0, c = 1; 1; d += c, c /= 2) {
            if (f >= (7 - 4 * d) / 11) {
                e = c * c - Math.pow((11 - 6 * d - 11 * f) / 4, 2);
                break;
            }
        }
        return e;
    },
    Elastic: function (b, a) {
        return Math.pow(2, 10 * --b) * Math.cos(20 * b * Math.PI * (a[0] || 1) / 3);
    }
});
["Quad", "Cubic", "Quart", "Quint"].each(function (b, a) {
    Fx.Transitions[b] = new Fx.Transition(function (c) {
        return Math.pow(c, [a + 2]);
    });
});
var Request = new Class({
    Implements: [Chain, Events, Options],
    options: {
        url: "",
        data: "",
        headers: {
            "X-Requested-With": "XMLHttpRequest",
            Accept: "text/javascript, text/html, application/xml, text/xml, */*"
        },
        async: true,
        format: false,
        method: "post",
        link: "ignore",
        isSuccess: null,
        emulation: true,
        urlEncoded: true,
        encoding: "utf-8",
        evalScripts: false,
        evalResponse: false,
        noCache: false
    },
    initialize: function (a) {
        this.xhr = new Browser.Request();
        this.setOptions(a);
        this.options.isSuccess = this.options.isSuccess || this.isSuccess;
        this.headers = new Hash(this.options.headers);
    },
    onStateChange: function () {
        if (this.xhr.readyState != 4 || !this.running) {
            return;
        }
        this.running = false;
        this.status = 0;
        $try(function () {
            this.status = this.xhr.status;
        }.bind(this));
        this.xhr.onreadystatechange = $empty;
        if (this.options.isSuccess.call(this, this.status)) {
            this.response = {
                text: this.xhr.responseText,
                xml: this.xhr.responseXML
            };
            this.success(this.response.text, this.response.xml);
        } else {
            this.response = {
                text: null,
                xml: null
            };
            this.failure();
        }
    },
    isSuccess: function () {
        return ((this.status >= 200) && (this.status < 300));
    },
    processScripts: function (a) {
        if (this.options.evalResponse || (/(ecma|java)script/).test(this.getHeader("Content-type"))) {
            return $exec(a);
        }
        return a.stripScripts(this.options.evalScripts);
    },
    success: function (b, a) {
        this.onSuccess(this.processScripts(b), a);
    },
    onSuccess: function () {
        this.fireEvent("complete", arguments).fireEvent("success", arguments).callChain();
    },
    failure: function () {
        this.onFailure();
    },
    onFailure: function () {
        this.fireEvent("complete").fireEvent("failure", this.xhr);
    },
    setHeader: function (a, b) {
        this.headers.set(a, b);
        return this;
    },
    getHeader: function (a) {
        return $try(function () {
            return this.xhr.getResponseHeader(a);
        }.bind(this));
    },
    check: function () {
        if (!this.running) {
            return true;
        }
        switch (this.options.link) {
        case "cancel":
            this.cancel();
            return true;
        case "chain":
            this.chain(this.caller.bind(this, arguments));
            return false;
        }
        return false;
    },
    send: function (k) {
        if (!this.check(k)) {
            return this;
        }
        this.running = true;
        var i = $type(k);
        if (i == "string" || i == "element") {
            k = {
                data: k
            };
        }
        var d = this.options;
        k = $extend({
            data: d.data,
            url: d.url,
            method: d.method
        }, k);
        var g = k.data,
            b = String(k.url),
            a = k.method.toLowerCase();
        switch ($type(g)) {
        case "element":
            g = document.id(g).toQueryString();
            break;
        case "object":
        case "hash":
            g = Hash.toQueryString(g);
        }
        if (this.options.format) {
            var j = "format=" + this.options.format;
            g = (g) ? j + "&" + g : j;
        }
        if (this.options.emulation && !["get", "post"].contains(a)) {
            var h = "_method=" + a;
            g = (g) ? h + "&" + g : h;
            a = "post";
        }
        if (this.options.urlEncoded && a == "post") {
            var c = (this.options.encoding) ? "; charset=" + this.options.encoding : "";
            this.headers.set("Content-type", "application/x-www-form-urlencoded" + c);
        }
        if (this.options.noCache) {
            var f = "noCache=" + new Date().getTime();
            g = (g) ? f + "&" + g : f;
        }
        var e = b.lastIndexOf("/");
        if (e > -1 && (e = b.indexOf("#")) > -1) {
            b = b.substr(0, e);
        }
        if (g && a == "get") {
            b = b + (b.contains("?") ? "&" : "?") + g;
            g = null;
        }
        this.xhr.open(a.toUpperCase(), b, this.options.async);
        this.xhr.onreadystatechange = this.onStateChange.bind(this);
        this.headers.each(function (m, l) {
            try {
                this.xhr.setRequestHeader(l, m);
            } catch (n) {
                this.fireEvent("exception", [l, m]);
            }
        }, this);
        this.fireEvent("request");
        this.xhr.send(g);
        if (!this.options.async) {
            this.onStateChange();
        }
        return this;
    },
    cancel: function () {
        if (!this.running) {
            return this;
        }
        this.running = false;
        this.xhr.abort();
        this.xhr.onreadystatechange = $empty;
        this.xhr = new Browser.Request();
        this.fireEvent("cancel");
        return this;
    }
});
(function () {
    var a = {};
    ["get", "post", "put", "delete", "GET", "POST", "PUT", "DELETE"].each(function (b) {
        a[b] = function () {
            var c = Array.link(arguments, {
                url: String.type,
                data: $defined
            });
            return this.send($extend(c, {
                method: b
            }));
        };
    });
    Request.implement(a);
})();
Element.Properties.send = {
    set: function (a) {
        var b = this.retrieve("send");
        if (b) {
            b.cancel();
        }
        return this.eliminate("send").store("send:options", $extend({
            data: this,
            link: "cancel",
            method: this.get("method") || "post",
            url: this.get("action")
        }, a));
    },
    get: function (a) {
        if (a || !this.retrieve("send")) {
            if (a || !this.retrieve("send:options")) {
                this.set("send", a);
            }
            this.store("send", new Request(this.retrieve("send:options")));
        }
        return this.retrieve("send");
    }
};
Element.implement({
    send: function (a) {
        var b = this.get("send");
        b.send({
            data: this,
            url: a || b.options.url
        });
        return this;
    }
});
Request.HTML = new Class({
    Extends: Request,
    options: {
        update: false,
        append: false,
        evalScripts: true,
        filter: false
    },
    processHTML: function (c) {
        var b = c.match(/<body[^>]*>([\s\S]*?)<\/body>/i);
        c = (b) ? b[1] : c;
        var a = new Element("div");
        return $try(function () {
            var d = "<root>" + c + "</root>",
                g;
            if (Browser.Engine.trident) {
                g = new ActiveXObject("Microsoft.XMLDOM");
                g.async = false;
                g.loadXML(d);
            } else {
                g = new DOMParser().parseFromString(d, "text/xml");
            }
            d = g.getElementsByTagName("root")[0];
            if (!d) {
                return null;
            }
            for (var f = 0, e = d.childNodes.length; f < e; f++) {
                var h = Element.clone(d.childNodes[f], true, true);
                if (h) {
                    a.grab(h);
                }
            }
            return a;
        }) || a.set("html", c);
    },
    success: function (d) {
        var c = this.options,
            b = this.response;
        b.html = d.stripScripts(function (e) {
            b.javascript = e;
        });
        var a = this.processHTML(b.html);
        b.tree = a.childNodes;
        b.elements = a.getElements("*");
        if (c.filter) {
            b.tree = b.elements.filter(c.filter);
        }
        if (c.update) {
            document.id(c.update).empty().set("html", b.html);
        } else {
            if (c.append) {
                document.id(c.append).adopt(a.getChildren());
            }
        } if (c.evalScripts) {
            $exec(b.javascript);
        }
        this.onSuccess(b.tree, b.elements, b.html, b.javascript);
    }
});
Element.Properties.load = {
    set: function (a) {
        var b = this.retrieve("load");
        if (b) {
            b.cancel();
        }
        return this.eliminate("load").store("load:options", $extend({
            data: this,
            link: "cancel",
            update: this,
            method: "get"
        }, a));
    },
    get: function (a) {
        if (a || !this.retrieve("load")) {
            if (a || !this.retrieve("load:options")) {
                this.set("load", a);
            }
            this.store("load", new Request.HTML(this.retrieve("load:options")));
        }
        return this.retrieve("load");
    }
};
Element.implement({
    load: function () {
        this.get("load").send(Array.link(arguments, {
            data: Object.type,
            url: String.type
        }));
        return this;
    }
});
Request.JSON = new Class({
    Extends: Request,
    options: {
        secure: true
    },
    initialize: function (a) {
        this.parent(a);
        this.headers.extend({
            Accept: "application/json",
            "X-Request": "JSON"
        });
    },
    success: function (a) {
        this.response.json = JSON.decode(a, this.options.secure);
        this.onSuccess(this.response.json, a);
    }
});;//MooTools More, <http://mootools.net/more>. Copyright (c) 2006-2009 Aaron Newton <http://clientcide.com/>, Valerio Proietti <http://mad4milk.net> & the MooTools team <http://mootools.net/developers>, MIT Style License.
MooTools.More = {
    version: "1.2.4.2",
    build: "bd5a93c0913cce25917c48cbdacde568e15e02ef"
};
(function () {
    var c = this;
    var b = function () {
        if (c.console && console.log) {
            try {
                console.log.apply(console, arguments);
            } catch (d) {
                console.log(Array.slice(arguments));
            }
        } else {
            Log.logged.push(arguments);
        }
        return this;
    };
    var a = function () {
        this.logged.push(arguments);
        return this;
    };
    this.Log = new Class({
        logged: [],
        log: a,
        resetLog: function () {
            this.logged.empty();
            return this;
        },
        enableLog: function () {
            this.log = b;
            this.logged.each(function (d) {
                this.log.apply(this, d);
            }, this);
            return this.resetLog();
        },
        disableLog: function () {
            this.log = a;
            return this;
        }
    });
    Log.extend(new Log).enableLog();
    Log.logger = function () {
        return this.log.apply(this, arguments);
    };
})();
Class.Mutators.Binds = function (a) {
    return a;
};
Class.Mutators.initialize = function (a) {
    return function () {
        $splat(this.Binds).each(function (b) {
            var c = this[b];
            if (c) {
                this[b] = c.bind(this);
            }
        }, this);
        return a.apply(this, arguments);
    };
};
Array.implement({
    min: function () {
        return Math.min.apply(null, this);
    },
    max: function () {
        return Math.max.apply(null, this);
    },
    average: function () {
        return this.length ? this.sum() / this.length : 0;
    },
    sum: function () {
        var a = 0,
            b = this.length;
        if (b) {
            do {
                a += this[--b];
            } while (b);
        }
        return a;
    },
    unique: function () {
        return [].combine(this);
    }
});
Hash.implement({
    getFromPath: function (a) {
        var b = this.getClean();
        a.replace(/\[([^\]]+)\]|\.([^.[]+)|[^[.]+/g, function (c) {
            if (!b) {
                return null;
            }
            var d = arguments[2] || arguments[1] || arguments[0];
            b = (d in b) ? b[d] : null;
            return c;
        });
        return b;
    },
    cleanValues: function (a) {
        a = a || $defined;
        this.each(function (c, b) {
            if (!a(c)) {
                this.erase(b);
            }
        }, this);
        return this;
    },
    run: function () {
        var a = arguments;
        this.each(function (c, b) {
            if ($type(c) == "function") {
                c.run(a);
            }
        });
    }
});
(function () {
    var b = ["Ã€", "Ã ", "Ã", "Ã¡", "Ã‚", "Ã¢", "Ãƒ", "Ã£", "Ã„", "Ã¤", "Ã…", "Ã¥", "Ä‚", "Äƒ", "Ä„", "Ä…", "Ä†", "Ä‡", "ÄŒ", "Ä", "Ã‡", "Ã§", "ÄŽ", "Ä", "Ä", "Ä‘", "Ãˆ", "Ã¨", "Ã‰", "Ã©", "ÃŠ", "Ãª", "Ã‹", "Ã«", "Äš", "Ä›", "Ä˜", "Ä™", "Äž", "ÄŸ", "ÃŒ", "Ã¬", "Ã", "Ã­", "ÃŽ", "Ã®", "Ã", "Ã¯", "Ä¹", "Äº", "Ä½", "Ä¾", "Å", "Å‚", "Ã‘", "Ã±", "Å‡", "Åˆ", "Åƒ", "Å„", "Ã’", "Ã²", "Ã“", "Ã³", "Ã”", "Ã´", "Ã•", "Ãµ", "Ã–", "Ã¶", "Ã˜", "Ã¸", "Å‘", "Å˜", "Å™", "Å”", "Å•", "Å ", "Å¡", "Åž", "ÅŸ", "Åš", "Å›", "Å¤", "Å¥", "Å¤", "Å¥", "Å¢", "Å£", "Ã™", "Ã¹", "Ãš", "Ãº", "Ã›", "Ã»", "Ãœ", "Ã¼", "Å®", "Å¯", "Å¸", "Ã¿", "Ã½", "Ã", "Å½", "Å¾", "Å¹", "Åº", "Å»", "Å¼", "Ãž", "Ã¾", "Ã", "Ã°", "ÃŸ", "Å’", "Å“", "Ã†", "Ã¦", "Âµ"];
    var a = ["A", "a", "A", "a", "A", "a", "A", "a", "Ae", "ae", "A", "a", "A", "a", "A", "a", "C", "c", "C", "c", "C", "c", "D", "d", "D", "d", "E", "e", "E", "e", "E", "e", "E", "e", "E", "e", "E", "e", "G", "g", "I", "i", "I", "i", "I", "i", "I", "i", "L", "l", "L", "l", "L", "l", "N", "n", "N", "n", "N", "n", "O", "o", "O", "o", "O", "o", "O", "o", "Oe", "oe", "O", "o", "o", "R", "r", "R", "r", "S", "s", "S", "s", "S", "s", "T", "t", "T", "t", "T", "t", "U", "u", "U", "u", "U", "u", "Ue", "ue", "U", "u", "Y", "y", "Y", "y", "Z", "z", "Z", "z", "Z", "z", "TH", "th", "DH", "dh", "ss", "OE", "oe", "AE", "ae", "u"];
    var d = {
        "[\xa0\u2002\u2003\u2009]": " ",
        "\xb7": "*",
        "[\u2018\u2019]": "'",
        "[\u201c\u201d]": '"',
        "\u2026": "...",
        "\u2013": "-",
        "\u2014": "--",
        "\uFFFD": "&raquo;"
    };
    var c = function (e, f) {
        e = e || "";
        var g = f ? "<" + e + "[^>]*>([\\s\\S]*?)</" + e + ">" : "</?" + e + "([^>]+)?>";
        reg = new RegExp(g, "gi");
        return reg;
    };
    String.implement({
        standardize: function () {
            var e = this;
            b.each(function (g, f) {
                e = e.replace(new RegExp(g, "g"), a[f]);
            });
            return e;
        },
        repeat: function (e) {
            return new Array(e + 1).join(this);
        },
        pad: function (f, h, e) {
            if (this.length >= f) {
                return this;
            }
            var g = (h == null ? " " : "" + h).repeat(f - this.length).substr(0, f - this.length);
            if (!e || e == "right") {
                return this + g;
            }
            if (e == "left") {
                return g + this;
            }
            return g.substr(0, (g.length / 2).floor()) + this + g.substr(0, (g.length / 2).ceil());
        },
        getTags: function (e, f) {
            return this.match(c(e, f)) || [];
        },
        stripTags: function (e, f) {
            return this.replace(c(e, f), "");
        },
        tidy: function () {
            var e = this.toString();
            $each(d, function (g, f) {
                e = e.replace(new RegExp(f, "g"), g);
            });
            return e;
        }
    });
})();
Element.implement({
    isDisplayed: function () {
        return this.getStyle("display") != "none";
    },
    isVisible: function () {
        var a = this.offsetWidth,
            b = this.offsetHeight;
        return (a == 0 && b == 0) ? false : (a > 0 && b > 0) ? true : this.isDisplayed();
    },
    toggle: function () {
        return this[this.isDisplayed() ? "hide" : "show"]();
    },
    hide: function () {
        var b;
        try {
            if ((b = this.getStyle("display")) == "none") {
                b = null;
            }
        } catch (a) {}
        return this.store("originalDisplay", b || "block").setStyle("display", "none");
    },
    show: function (a) {
        return this.setStyle("display", a || this.retrieve("originalDisplay") || "block");
    },
    swapClass: function (a, b) {
        return this.removeClass(a).addClass(b);
    }
});
Fx.Elements = new Class({
    Extends: Fx.CSS,
    initialize: function (b, a) {
        this.elements = this.subject = $$(b);
        this.parent(a);
    },
    compute: function (g, h, j) {
        var c = {};
        for (var d in g) {
            var a = g[d],
                e = h[d],
                f = c[d] = {};
            for (var b in a) {
                f[b] = this.parent(a[b], e[b], j);
            }
        }
        return c;
    },
    set: function (b) {
        for (var c in b) {
            var a = b[c];
            for (var d in a) {
                this.render(this.elements[c], d, a[d], this.options.unit);
            }
        }
        return this;
    },
    start: function (c) {
        if (!this.check(c)) {
            return this;
        }
        var h = {}, j = {};
        for (var d in c) {
            var f = c[d],
                a = h[d] = {}, g = j[d] = {};
            for (var b in f) {
                var e = this.prepare(this.elements[d], b, f[b]);
                a[b] = e.from;
                g[b] = e.to;
            }
        }
        return this.parent(h, j);
    }
});
var Accordion = Fx.Accordion = new Class({
    Extends: Fx.Elements,
    options: {
        display: 0,
        show: false,
        height: true,
        width: false,
        opacity: true,
        alwaysHide: false,
        trigger: "click",
        initialDisplayFx: true,
        returnHeightToAuto: true
    },
    initialize: function () {
        var c = Array.link(arguments, {
            container: Element.type,
            options: Object.type,
            togglers: $defined,
            elements: $defined
        });
        this.parent(c.elements, c.options);
        this.togglers = $$(c.togglers);
        this.container = document.id(c.container);
        this.previous = -1;
        this.internalChain = new Chain();
        if (this.options.alwaysHide) {
            this.options.wait = true;
        }
        if ($chk(this.options.show)) {
            this.options.display = false;
            this.previous = this.options.show;
        }
        if (this.options.start) {
            this.options.display = false;
            this.options.show = false;
        }
        this.effects = {};
        if (this.options.opacity) {
            this.effects.opacity = "fullOpacity";
        }
        if (this.options.width) {
            this.effects.width = this.options.fixedWidth ? "fullWidth" : "offsetWidth";
        }
        if (this.options.height) {
            this.effects.height = this.options.fixedHeight ? "fullHeight" : "scrollHeight";
        }
        for (var b = 0, a = this.togglers.length; b < a; b++) {
            this.addSection(this.togglers[b], this.elements[b]);
        }
        this.elements.each(function (e, d) {
            if (this.options.show === d) {
                this.fireEvent("active", [this.togglers[d], e]);
            } else {
                for (var f in this.effects) {
                    e.setStyle(f, 0);
                }
            }
        }, this);
        if ($chk(this.options.display)) {
            this.display(this.options.display, this.options.initialDisplayFx);
        }
        this.addEvent("complete", this.internalChain.callChain.bind(this.internalChain));
    },
    addSection: function (e, c) {
        e = document.id(e);
        c = document.id(c);
        var f = this.togglers.contains(e);
        this.togglers.include(e);
        this.elements.include(c);
        var a = this.togglers.indexOf(e);
        var b = this.display.bind(this, a);
        e.store("accordion:display", b);
        e.addEvent(this.options.trigger, b);
        if (this.options.height) {
            c.setStyles({
                "padding-top": 0,
                "border-top": "none",
                "padding-bottom": 0,
                "border-bottom": "none"
            });
        }
        if (this.options.width) {
            c.setStyles({
                "padding-left": 0,
                "border-left": "none",
                "padding-right": 0,
                "border-right": "none"
            });
        }
        c.fullOpacity = 1;
        if (this.options.fixedWidth) {
            c.fullWidth = this.options.fixedWidth;
        }
        if (this.options.fixedHeight) {
            c.fullHeight = this.options.fixedHeight;
        }
        c.setStyle("overflow", "hidden");
        if (!f) {
            for (var d in this.effects) {
                c.setStyle(d, 0);
            }
        }
        return this;
    },
    detach: function () {
        this.togglers.each(function (a) {
            a.removeEvent(this.options.trigger, a.retrieve("accordion:display"));
        }, this);
    },
    display: function (a, b) {
        if (!this.check(a, b)) {
            return this;
        }
        b = $pick(b, true);
        if (this.options.returnHeightToAuto) {
            var d = this.elements[this.previous];
            if (d && !this.selfHidden) {
                for (var c in this.effects) {
                    d.setStyle(c, d[this.effects[c]]);
                }
            }
        }
        a = ($type(a) == "element") ? this.elements.indexOf(a) : a;
        if ((this.timer && this.options.wait) || (a === this.previous && !this.options.alwaysHide)) {
            return this;
        }
        this.previous = a;
        var e = {};
        this.elements.each(function (h, g) {
            e[g] = {};
            var f;
            if (g != a) {
                f = true;
            } else {
                if (this.options.alwaysHide && ((h.offsetHeight > 0 && this.options.height) || h.offsetWidth > 0 && this.options.width)) {
                    f = true;
                    this.selfHidden = true;
                }
            }
            this.fireEvent(f ? "background" : "active", [this.togglers[g], h]);
            for (var j in this.effects) {
                e[g][j] = f ? 0 : h[this.effects[j]];
            }
        }, this);
        this.internalChain.chain(function () {
            if (this.options.returnHeightToAuto && !this.selfHidden) {
                var f = this.elements[a];
                if (f) {
                    f.setStyle("height", "auto");
                }
            }
        }.bind(this));
        return b ? this.start(e) : this.set(e);
    }
});
Fx.Scroll = new Class({
    Extends: Fx,
    options: {
        offset: {
            x: 0,
            y: 0
        },
        wheelStops: true
    },
    initialize: function (b, a) {
        this.element = this.subject = document.id(b);
        this.parent(a);
        var d = this.cancel.bind(this, false);
        if ($type(this.element) != "element") {
            this.element = document.id(this.element.getDocument().body);
        }
        var c = this.element;
        if (this.options.wheelStops) {
            this.addEvent("start", function () {
                c.addEvent("mousewheel", d);
            }, true);
            this.addEvent("complete", function () {
                c.removeEvent("mousewheel", d);
            }, true);
        }
    },
    set: function () {
        var a = Array.flatten(arguments);
        if (Browser.Engine.gecko) {
            a = [Math.round(a[0]), Math.round(a[1])];
        }
        this.element.scrollTo(a[0], a[1]);
    },
    compute: function (c, b, a) {
        return [0, 1].map(function (d) {
            return Fx.compute(c[d], b[d], a);
        });
    },
    start: function (c, g) {
        if (!this.check(c, g)) {
            return this;
        }
        var e = this.element.getScrollSize(),
            b = this.element.getScroll(),
            d = {
                x: c,
                y: g
            };
        for (var f in d) {
            var a = e[f];
            if ($chk(d[f])) {
                d[f] = ($type(d[f]) == "number") ? d[f] : a;
            } else {
                d[f] = b[f];
            }
            d[f] += this.options.offset[f];
        }
        return this.parent([b.x, b.y], [d.x, d.y]);
    },
    toTop: function () {
        return this.start(false, 0);
    },
    toLeft: function () {
        return this.start(0, false);
    },
    toRight: function () {
        return this.start("right", false);
    },
    toBottom: function () {
        return this.start(false, "bottom");
    },
    toElement: function (b) {
        var a = document.id(b).getPosition(this.element);
        return this.start(a.x, a.y);
    },
    scrollIntoView: function (c, e, d) {
        e = e ? $splat(e) : ["x", "y"];
        var h = {};
        c = document.id(c);
        var f = c.getPosition(this.element);
        var i = c.getSize();
        var g = this.element.getScroll();
        var a = this.element.getSize();
        var b = {
            x: f.x + i.x,
            y: f.y + i.y
        };
        ["x", "y"].each(function (j) {
            if (e.contains(j)) {
                if (b[j] > g[j] + a[j]) {
                    h[j] = b[j] - a[j];
                }
                if (f[j] < g[j]) {
                    h[j] = f[j];
                }
            }
            if (h[j] == null) {
                h[j] = g[j];
            }
            if (d && d[j]) {
                h[j] = h[j] + d[j];
            }
        }, this);
        if (h.x != g.x || h.y != g.y) {
            this.start(h.x, h.y);
        }
        return this;
    },
    scrollToCenter: function (c, e, d) {
        e = e ? $splat(e) : ["x", "y"];
        c = $(c);
        var h = {}, f = c.getPosition(this.element),
            i = c.getSize(),
            g = this.element.getScroll(),
            a = this.element.getSize(),
            b = {
                x: f.x + i.x,
                y: f.y + i.y
            };
        ["x", "y"].each(function (j) {
                if (e.contains(j)) {
                    h[j] = f[j] - (a[j] - i[j]) / 2;
                }
                if (h[j] == null) {
                    h[j] = g[j];
                }
                if (d && d[j]) {
                    h[j] = h[j] + d[j];
                }
            }, this);
        if (h.x != g.x || h.y != g.y) {
            this.start(h.x, h.y);
        }
        return this;
    }
});
Fx.Slide = new Class({
    Extends: Fx,
    options: {
        mode: "vertical",
        hideOverflow: true
    },
    initialize: function (b, a) {
        this.addEvent("complete", function () {
            this.open = (this.wrapper["offset" + this.layout.capitalize()] != 0);
            if (this.open && Browser.Engine.webkit419) {
                this.element.dispose().inject(this.wrapper);
            }
        }, true);
        this.element = this.subject = document.id(b);
        this.parent(a);
        var d = this.element.retrieve("wrapper");
        var c = this.element.getStyles("margin", "position", "overflow");
        if (this.options.hideOverflow) {
            c = $extend(c, {
                overflow: "hidden"
            });
        }
        this.wrapper = d || new Element("div", {
            styles: c
        }).wraps(this.element);
        this.element.store("wrapper", this.wrapper).setStyle("margin", 0);
        this.now = [];
        this.open = true;
    },
    vertical: function () {
        this.margin = "margin-top";
        this.layout = "height";
        this.offset = this.element.offsetHeight;
    },
    horizontal: function () {
        this.margin = "margin-left";
        this.layout = "width";
        this.offset = this.element.offsetWidth;
    },
    set: function (a) {
        this.element.setStyle(this.margin, a[0]);
        this.wrapper.setStyle(this.layout, a[1]);
        return this;
    },
    compute: function (c, b, a) {
        return [0, 1].map(function (d) {
            return Fx.compute(c[d], b[d], a);
        });
    },
    start: function (b, e) {
        if (!this.check(b, e)) {
            return this;
        }
        this[e || this.options.mode]();
        var d = this.element.getStyle(this.margin).toInt();
        var c = this.wrapper.getStyle(this.layout).toInt();
        var a = [
            [d, c],
            [0, this.offset]
        ];
        var g = [
            [d, c],
            [-this.offset, 0]
        ];
        var f;
        switch (b) {
        case "in":
            f = a;
            break;
        case "out":
            f = g;
            break;
        case "toggle":
            f = (c == 0) ? a : g;
        }
        return this.parent(f[0], f[1]);
    },
    slideIn: function (a) {
        return this.start("in", a);
    },
    slideOut: function (a) {
        return this.start("out", a);
    },
    hide: function (a) {
        this[a || this.options.mode]();
        this.open = false;
        return this.set([-this.offset, 0]);
    },
    show: function (a) {
        this[a || this.options.mode]();
        this.open = true;
        return this.set([0, this.offset]);
    },
    toggle: function (a) {
        return this.start("toggle", a);
    }
});
Element.Properties.slide = {
    set: function (b) {
        var a = this.retrieve("slide");
        if (a) {
            a.cancel();
        }
        return this.eliminate("slide").store("slide:options", $extend({
            link: "cancel"
        }, b));
    },
    get: function (a) {
        if (a || !this.retrieve("slide")) {
            if (a || !this.retrieve("slide:options")) {
                this.set("slide", a);
            }
            this.store("slide", new Fx.Slide(this, this.retrieve("slide:options")));
        }
        return this.retrieve("slide");
    }
};
Element.implement({
    slide: function (d, e) {
        d = d || "toggle";
        var b = this.get("slide"),
            a;
        switch (d) {
        case "hide":
            b.hide(e);
            break;
        case "show":
            b.show(e);
            break;
        case "toggle":
            var c = this.retrieve("slide:flag", b.open);
            b[c ? "slideOut" : "slideIn"](e);
            this.store("slide:flag", !c);
            a = true;
            break;
        default:
            b.start(d, e);
        }
        if (!a) {
            this.eliminate("slide:flag");
        }
        return this;
    }
});;(function($){

RF.Class.Drag = new Class({

	//	intialise properties and listeners
	initialize: function(id, data) {

		var touch;

		$extend(this, data);
		
		this.id = id;
		this.element = $(this.id);
		this.document = this.element.getDocument();
		
		this.mouse = {now: {}, pos: {}};
		//this.value = {now: {}};
		
		this.start = this.start.bind(this);
		this.drag = this.drag.bind(this);
		this.stop = this.stop.bind(this);
		
		touch = RF.isEventSupported("touchstart");
		
		this.events = {
			start: (touch ? "touchstart" : "mousedown"),
			drag: (touch ? "touchmove" : "mousemove"),
			stop: (touch ? "touchend" : "mouseup")
		};
				

		this.element.addEvent(this.events.start, this.start);

	},
	
	start: function(e) {

		if (e.rightClick) {
			return true;
		}
		e.preventDefault();

		if(e.targetTouches && e.targetTouches[0]) {
			this.mouse.start = {
				x: e.targetTouches[0].clientX,
				y: e.targetTouches[0].clientY
			};
		}
		else {
			this.mouse.start = e.page;
		}
		
		this.onStart();

		this.document.addEvent(this.events.drag, this.drag);
		this.document.addEvent(this.events.stop, this.stop);
		this.document.addEvent("mouseleave", this.stop);
		
	},
	
	drag: function(e) {
		e.preventDefault();
		if(e.targetTouches && e.targetTouches[0]) {
			this.mouse.now = {
				x:e.targetTouches[0].clientX,
				y:e.targetTouches[0].clientY
			};
		}
		else {
			this.mouse.now = e.page;
		}		

		this.onDrag(this.mouse.now.x - this.mouse.start.x, this.mouse.now.y - this.mouse.start.y);
	},
	
	stop: function(e) {
		
		this.onComplete();

		this.document.removeEvent(this.events.drag, this.drag);
		this.document.removeEvent(this.events.stop, this.stop);
		this.document.removeEvent("mouseleave", this.stop);		
	}
	
});


RF.fx = function () {

	var self,
		looping = false,
		frameRate = 60,
		tweens = [],
		eventLoop,
		getComputedStyle,
		setOptions,
	    bezier2,
		bezier3,
		defaultOptions = {
			duration: 1000,
		    delay: 0,
			easing: false,
		    prefix: {},
		    suffix: {},
		    onStart: undefined,
		    onStartParams: [],
		    onUpdate: undefined,
		    onUpdateParams: [],
		    onComplete: undefined,
		    onCompleteParams: []
		};


	setOptions = function(fx, options) {
		var key;
	    for (key in defaultOptions) {
	        if (typeof options[key] !== 'undefined') {
	            fx[key] = options[key];
				if(/Params$/.test(key)) {
					options[key].unshift(0);
				}
				delete options[key];
	        }
			else {
	            fx[key] = defaultOptions[key];
	        }
	    }

		fx.easing = fx.easing || self.linear;

	};

	getComputedStyle = function(el, property) {

		var css, value;

		if (document.defaultView && document.defaultView.getComputedStyle) {
			css = document.defaultView.getComputedStyle(el, null);
		}
		else {
		    css = el.currentStyle
		}

		value = /^(.+)(px)$/.exec(css[property]);

		return {
			value: parseFloat(value[1], 10),
			suffix: value[2]
		};

	};

	//	bezier
    bezier2 = function(t, p0, p1, p2) {
		return (1-t) * (1-t) * p0 + 2 * t * (1-t) * p1 + t * t * p2;
	};

    bezier3 = function(t, p0, p1, p2, p3) {
         return Math.pow(1-t, 3) * p0 + 3 * t * Math.pow(1-t, 2) * p1 + 3 * t * t * (1-t) * p2 + t * t * t * p3;
    };

	eventLoop = function() {

        var i, fx, t, now = (new Date() - 0);

        for (i = 0; i < tweens.length; i++) {

            fx = tweens[i];
            t = now - fx.startTime;

            fx.update(t, fx.duration);

            if (typeof fx.onUpdate === 'function') {
				fx.onUpdateParams[0] = t;
                fx.onUpdate.apply(fx, fx.onUpdateParams);
            }

            if (t >= fx.duration) {

                tweens.splice(i, 1);
    
				if (typeof fx.onComplete === 'function') {
                    fx.onComplete.apply(fx, fx.onCompleteParams);
                }
            }
        }

        if (tweens.length > 0) {
            setTimeout(eventLoop, 1000/frameRate);
        }
		else {
            looping = false;
        }

	};

	self = {
		//	start an fx off
		add: function(fx) {

		    setTimeout(function() {

		        fx.startTime = (new Date() - 0);

		        if (typeof fx.onStart === 'function') {
	                fx.onStart.apply(fx, fx.onStartParams);
		        }

		        tweens.push(fx);

		        if (!looping) { 
		            looping = true;
		            eventLoop();
		        }

		    }, fx.delay);
			return fx;

		},

		//	fx
		morph: function (el, options) {

			if(options.path) {

				return this.path(el, options, true);
			}

			var fx, key, start;

			fx = {
				target: el.style,
				properties: {},
				update: function(t) {
					var p, property, value, d = this.duration;
					t = (t >= d) ? d : t;
			        for (property in this.properties) {
			            p = this.properties[property];
						value = this.easing(t, p.start, p.change, d);
			            try {
			                this.target[property] = value + this.suffix[property];
							p.current = value
			            }
						catch(e) {}
			        }

				}
			};

			setOptions(fx, options);

		    for (key in options) {
				start = getComputedStyle(el, key);
				fx.properties[key] = {
		            start: start.value,
					current: start.value,
					change: options[key] - start.value
		        };
				fx.suffix[key] = start.suffix;
		    }

			this.add(fx);

			return fx;
		},

		path: function(el, options, defineP0) {

			var fx,	x, y;

			if(defineP0) {
				x = getComputedStyle(el, "left");
				y = getComputedStyle(el, "top");

				options.path.splice(0, 0, {
					x: x.value,
					y: y.value
				});
				options.suffix = {
					left: x.suffix,
					top: y.suffix
				};
			}
			else {
				options.suffix = {
					left: "px",
					top: "px"
				};	
			}
			

			fx = {
				target: el.style,
				path: options.path
			};

			if(fx.path.length === 3) {
				fx.update = function() {
					var t = Math.min(((new Date() - 0)-this.startTime)/(this.duration), 1);
		            try {
		            	this.target.left = bezier2(t, this.path[0].x, this.path[1].x, this.path[2].x) + this.suffix.left;
		            	this.target.top = bezier2(t, this.path[0].y, this.path[1].y, this.path[2].y) + this.suffix.top;
		            }
					catch(e){};
				};
			}
			else if(fx.path.length === 4) {
				fx.update = function() {
					var t = Math.min(((new Date() - 0)-this.startTime)/(this.duration), 1);
		            try {
		            	this.target.left = bezier3(t, this.path[0].x, this.path[1].x, this.path[2].x, this.path[3].x) + this.suffix.left;
		            	this.target.top = bezier3(t, this.path[0].y, this.path[1].y, this.path[2].y,  this.path[3].y) + this.suffix.top;
		            }
					catch(e){};
				};
			}
			else {
				throw "Bezier beware"
			}

			setOptions(fx, options);

			this.add(fx);

			return fx;
		},

		throb: function(el, options, defineP0) {

			var fx,	w, h;

			if(defineP0) {
				w = getComputedStyle(el, "width");
				h = getComputedStyle(el, "height");

				options.path.splice(0, 0, {
					w: w.value,
					h: h.value
				});
				options.suffix = {
					width: w.suffix,
					height: h.suffix
				};
			}
			else {
				options.suffix = {
					width: "px",
					height: "px"
				};	
			}
			

			fx = {
				target: el.style,
				path: options.path
			};

			if(fx.path.length === 3) {
				fx.update = function() {
					var t = Math.min(((new Date() - 0)-this.startTime)/(this.duration), 1);
		            try {
		            	this.target.width = bezier2(t, this.path[0].w, this.path[1].w, this.path[2].w) + this.suffix.width;
		            	this.target.height = bezier2(t, this.path[0].h, this.path[1].h, this.path[2].h) + this.suffix.height;
		            }
					catch(e){};
				};
			}
			else if(fx.path.length === 4) {
				fx.update = function() {
					var t = Math.min(((new Date() - 0)-this.startTime)/(this.duration), 1);
		            try {
		            	this.target.width = bezier3(t, this.path[0].w, this.path[1].w, this.path[2].w, this.path[3].w) + this.suffix.width;
		            	this.target.height = bezier3(t, this.path[0].h, this.path[1].h, this.path[2].h,  this.path[3].h) + this.suffix.height;
		            }
					catch(e){};
				};
			}
			else {
				throw "Bezier beware"
			}

			setOptions(fx, options);

			this.add(fx);

			return fx;
		},


		//	easing
		linear: function(t, b, c, d) {
		    return c*t/d + b;
		},    
		easeInQuad: function(t, b, c, d) {
		    return c*(t/=d)*t + b;
		},    
		easeOutQuad: function(t, b, c, d) {
		    return -c *(t/=d)*(t-2) + b;
		},    
		easeInOutQuad: function(t, b, c, d) {
		    if((t/=d/2) < 1) return c/2*t*t + b;
		    return -c/2 *((--t)*(t-2) - 1) + b;
		},    
		easeInCubic: function(t, b, c, d) {
		    return c*(t/=d)*t*t + b;
		},    
		easeOutCubic: function(t, b, c, d) {
		    return c*((t=t/d-1)*t*t + 1) + b;
		},    
		easeInOutCubic: function(t, b, c, d) {
		    if((t/=d/2) < 1) return c/2*t*t*t + b;
		    return c/2*((t-=2)*t*t + 2) + b;
		},    
		easeOutInCubic: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutCubic(t*2, b, c/2, d);
		    return self.easeInCubic((t*2)-d, b+c/2, c/2, d);
		},    
		easeInQuart: function(t, b, c, d) {
		    return c*(t/=d)*t*t*t + b;
		},    
		easeOutQuart: function(t, b, c, d) {
		    return -c *((t=t/d-1)*t*t*t - 1) + b;
		},    
		easeInOutQuart: function(t, b, c, d) {
		    if((t/=d/2) < 1) return c/2*t*t*t*t + b;
		    return -c/2 *((t-=2)*t*t*t - 2) + b;
		},    
		easeOutInQuart: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutQuart(t*2, b, c/2, d);
		    return self.easeInQuart((t*2)-d, b+c/2, c/2, d);
		},    
		easeInQuint: function(t, b, c, d) {
		    return c*(t/=d)*t*t*t*t + b;
		},    
		easeOutQuint: function(t, b, c, d) {
		    return c*((t=t/d-1)*t*t*t*t + 1) + b;
		},    
		easeInOutQuint: function(t, b, c, d) {
		    if((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
		    return c/2*((t-=2)*t*t*t*t + 2) + b;
		},    
		easeOutInQuint: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutQuint(t*2, b, c/2, d);
		    return self.easeInQuint((t*2)-d, b+c/2, c/2, d);
		},    
		easeInSine: function(t, b, c, d) {
		    return -c * Math.cos(t/d *(Math.PI/2)) + c + b;
		},    
		easeOutSine: function(t, b, c, d) {
		    return c * Math.sin(t/d *(Math.PI/2)) + b;
		},    
		easeInOutSine: function(t, b, c, d) {
		    return -c/2 *(Math.cos(Math.PI*t/d) - 1) + b;
		},    
		easeOutInSine: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutSine(t*2, b, c/2, d);
		    return self.easeInSine((t*2)-d, b+c/2, c/2, d);
		},    
		easeInExpo: function(t, b, c, d) {
		    return(t==0) ? b : c * Math.pow(2, 10 *(t/d - 1)) + b - c * 0.001;
		},    
		easeOutExpo: function(t, b, c, d) {
		    return(t==d) ? b+c : c * 1.001 *(-Math.pow(2, -10 * t/d) + 1) + b;
		},    
		easeInOutExpo: function(t, b, c, d) {
		    if(t==0) return b;
		    if(t==d) return b+c;
		    if((t/=d/2) < 1) return c/2 * Math.pow(2, 10 *(t - 1)) + b - c * 0.0005;
		    return c/2 * 1.0005 *(-Math.pow(2, -10 * --t) + 2) + b;
		},    
		easeOutInExpo: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutExpo(t*2, b, c/2, d);
		    return self.easeInExpo((t*2)-d, b+c/2, c/2, d);
		},    
		easeInCirc: function(t, b, c, d) {
		    return -c *(Math.sqrt(1 -(t/=d)*t) - 1) + b;
		},    
		easeOutCirc: function(t, b, c, d) {
		    return c * Math.sqrt(1 -(t=t/d-1)*t) + b;
		},    
		easeInOutCirc: function(t, b, c, d) {
		    if((t/=d/2) < 1) return -c/2 *(Math.sqrt(1 - t*t) - 1) + b;
		    return c/2 *(Math.sqrt(1 -(t-=2)*t) + 1) + b;
		},    
		easeOutInCirc: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutCirc(t*2, b, c/2, d);
		    return self.easeInCirc((t*2)-d, b+c/2, c/2, d);
		},    
		easeInElastic: function(t, b, c, d, a, p) {
		    var s;
		    if(t==0) return b;  if((t/=d)==1) return b+c;  if(!p) p=d*.3;
		    if(!a || a < Math.abs(c)) { a=c; s=p/4; } else s = p/(2*Math.PI) * Math.asin(c/a);
		    return -(a*Math.pow(2,10*(t-=1)) * Math.sin((t*d-s)*(2*Math.PI)/p )) + b;
		},    
		easeOutElastic: function(t, b, c, d, a, p) {
		    var s;
		    if(t==0) return b;  if((t/=d)==1) return b+c;  if(!p) p=d*.3;
		    if(!a || a < Math.abs(c)) { a=c; s=p/4; } else s = p/(2*Math.PI) * Math.asin(c/a);
		    return(a*Math.pow(2,-10*t) * Math.sin((t*d-s)*(2*Math.PI)/p ) + c + b);
		},    
		easeInOutElastic: function(t, b, c, d, a, p) {
		    var s;
		    if(t==0) return b;  if((t/=d/2)==2) return b+c;  if(!p) p=d*(.3*1.5);
		    if(!a || a < Math.abs(c)) { a=c; s=p/4; }       else s = p/(2*Math.PI) * Math.asin(c/a);
		    if(t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin((t*d-s)*(2*Math.PI)/p )) + b;
		    return a*Math.pow(2,-10*(t-=1)) * Math.sin((t*d-s)*(2*Math.PI)/p )*.5 + c + b;
		},    
		easeOutInElastic: function(t, b, c, d, a, p) {
		    if(t < d/2) return self.easeOutElastic(t*2, b, c/2, d, a, p);
		    return self.easeInElastic((t*2)-d, b+c/2, c/2, d, a, p);
		},    
		easeInBack: function(t, b, c, d, s) {
		    if(s == undefined) s = 1.70158;
		    return c*(t/=d)*t*((s+1)*t - s) + b;
		},    
		easeOutBack: function(t, b, c, d, s) {
		    if(s == undefined) s = 1.70158;
		    return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
		},    
		easeInOutBack: function(t, b, c, d, s) {
		    if(s == undefined) s = 1.70158;
		    if((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		    return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
		},    
		easeOutInBack: function(t, b, c, d, s) {
		    if(t < d/2) return self.easeOutBack(t*2, b, c/2, d, s);
		    return self.easeInBack((t*2)-d, b+c/2, c/2, d, s);
		},    
		easeInBounce: function(t, b, c, d) {
		    return c - self.easeOutBounce(d-t, 0, c, d) + b;
		},    
		easeOutBounce: function(t, b, c, d) {
		    if((t/=d) <(1/2.75)) {
		        return c*(7.5625*t*t) + b;
		    } else if(t <(2/2.75)) {
		        return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
		    } else if(t <(2.5/2.75)) {
		        return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
		    } else {
		        return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
		    }
		},    
		easeInOutBounce: function(t, b, c, d) {
		    if(t < d/2) return self.easeInBounce(t*2, 0, c, d) * .5 + b;
		    else return self.easeOutBounce(t*2-d, 0, c, d) * .5 + c*.5 + b;
		},    
		easeOutInBounce: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutBounce(t*2, b, c/2, d);
		    return self.easeInBounce((t*2)-d, b+c/2, c/2, d);
		}


	};

	return self;
	
}();



var Group = new Class({
	initialize: function(){
		this.instances = Array.flatten(arguments);
		this.events = {};
		this.checker = {};
	},
	addEvent: function(type, fn){
		this.checker[type] = this.checker[type] || {};
		this.events[type] = this.events[type] || [];
		if (this.events[type].contains(fn)) return false;
		else this.events[type].push(fn);
		this.instances.each(function(instance, i){
			instance.addEvent(type, this.check.bind(this, [type, instance, i]));
		}, this);
		return this;
	},
	check: function(type, instance, i){
		
		//	Google Chrome has it's own Function.prototype.bind which doesn't accept the sam earguments as Moo's.
		//	Hence we need to test the value of type and force it to be what we want if it's incorrect
		if($type(type) === "array") {
			i = type[2];
			type = type[0];
		}

		this.checker[type][i] = true;
		var every = this.instances.every(function(current, j){
			return this.checker[type][j] || false;
		}, this);
		if (!every) return;
		this.checker[type] = {};
		this.events[type].each(function(event){
			event.call(this, this.instances, instance);
		}, this);
	}
});


/*
	mootools more event delegation functions
	extended to be a bit more user friendly.

	to fire the click event on all href's with class linky that are decendents of element with id
	
	$("someId").on("click", "a.linky", function(){});
	
	this will return a listener object and NOT the mootools extended element.
	
	listener = $("someId").on("click", "a.linky", function(){});
	
	to stop/remove the listener
	
	listener.stop();
	
	to add/restart
	
	listener.add();
	
	
*/
(function(addEvent, removeEvent){

	var match = /(.*?):relay\(([^)]+)\)$/,
		combinators = /[+>~\s]/,
		splitType = function(type){
			var bits = type.match(match);
			return !bits ? {event: type} : {
				event: bits[1],
				selector: bits[2]
			};
		},
		check = function(e, selector){
			var t = e.target;
			if (combinators.test(selector = selector.trim())){
				var els = this.getElements(selector);
				for (var i = els.length; i--; ){
					var el = els[i];
					if (t == el || el.hasChild(t)) return el;
				}
			} else {
				for ( ; t && t != this; t = t.parentNode){
					if (Element.match(t, selector)) return document.id(t);
				}
			}
			return null;
		};

	Element.implement({

		addEvent: function(type, fn){

			var splitted = splitType(type);
			if (splitted.selector){
				var monitors = this.retrieve('$moo:delegateMonitors', {});
				if (!monitors[type]){
					var monitor = function(e){
						var el = check.call(this, e, splitted.selector);
						if (el) this.fireEvent(type, [e, el], 0, el);
					}.bind(this);
					monitors[type] = monitor;
					addEvent.call(this, splitted.event, monitor);
				}
			}
			return addEvent.apply(this, arguments);
		},

		removeEvent: function(type, fn){
			var splitted = splitType(type);
			if (splitted.selector){
				var events = this.retrieve('events');
				if (!events || !events[type] || (fn && !events[type].keys.contains(fn))) return this;

				if (fn) removeEvent.apply(this, [type, fn]);
				else removeEvent.apply(this, type);

				events = this.retrieve('events');
				if (events && events[type] && events[type].keys.length == 0){
					var monitors = this.retrieve('$moo:delegateMonitors', {});
					removeEvent.apply(this, [splitted.event, monitors[type]]);
					delete monitors[type];
				}
				return this;
			}
			return removeEvent.apply(this, arguments);
		},

		fireEvent: function(type, args, delay, bind){
			var events = this.retrieve('events');
			if (!events || !events[type]) return this;
			events[type].keys.each(function(fn){
				fn.create({bind: bind || this, delay: delay, arguments: args})();
			}, this);
			return this;
		},
		
		on: function(type, selector, fn) {

			var self = this, ev = type + ":relay(" + selector + ")";
			
			this.addEvent(ev, fn);

			return {
				start: function() {
					self.addEvent(ev, fn);
				},
				stop: function() {
					self.removeEvent(ev, fn);
				}
			}
		}

	});

})(Element.prototype.addEvent, Element.prototype.removeEvent);


/*
*	@name: 			RF.onThese
*	@description: 	Test to see if event is supported
*
*/
RF.isEventSupported = (function(){
	var tagNames = {
		"select":"input","change":"input",
		"submit":"form","reset":"form",
		"error":"img","load":"img","abort":"img"
	};
	return function (eventName) {
		var el = document.createElement(tagNames[eventName] || "div");
		eventName = "on" + eventName;
		var isSupported = (eventName in el);
		if (!isSupported) {
			el.setAttribute(eventName, "return;");
			isSupported = typeof el[eventName] === "function";
		}
		el = null;
		return isSupported;
	};
})();


/*
*	@name: 			RF.onThese
*	@description: 	Fires the callabck when all the deferred have fired
*
*/
RF.onThese = function() {
	var i = 0, l = arguments.length - 2, func, f, args = $splat(arguments);
	func = arguments[arguments.length - 1];

	f = function() {
		args[i++].addCallbacks(function(data) {
			if(i <= l) {
				f();
			}
			else {
				func(data);
			}
			return data;
		},
		function(e) {
			return e;
		});
	};
	f();

};


/*
*	@name: 			RF.Class.Deferred
*	@description: 	Callback API to handle asynchronous loading
*						
*
*/
RF.Class.Deferred = new Class({

	initialize: function() {
		this.status = 0;
		this.stack = [];
		this.result = false;
	},

	addCallbacks: function(callback, errback){
		this.stack.push([callback, errback]);
		if (this.status === 1) {
			this.fire();
		}
	},
	
	fire: function(res){

		if (this.status === -1) {
			return;
		}
		this.status = 1;

		res = res || this.result;

		var stack = this.stack, result = this.result;

		while (stack.length > 0) {
			(function(){
				var f = stack.shift()[res instanceof Error ? 1 : 0];
				if (!f) {
					return;
				}
				res = f(res) || result;
			})();
		}
		this.result = res;

	},
	cancel: function(){
		this.status = -1;
	}

});



/*
*	@name: 			Events
*	@description: 	Razorfish events Class that acts as a message passing bridge to allow different ojects to comunicate with each other.
*						
*
*/
RF.Class.Events = function() {
	
	var stack = {}, id = 0, debug;

	debug = (function() {

		var i, l,
			qs = window.location.search.substring(1),
			kv = qs.split("&");
		for (i = 0, l = kv.length; i < l; i++) {
			if (kv[i] === "debug=true") {
				return true;
			}
		}
		return false
	})();
	
	return new Class({
		
		addListener: function(event, listener) {

			var guid = "__" + (id++), self = this;

			if(!stack[event]) {
				stack[event] = {};
			}
			stack[event][guid] = function(data){
				listener.apply(self, [data]);
			}
			
			return {

				start: function() {
					stack[event][guid] = function(data){
						listener.apply(self, [data]);
					}
				},
				
				stop: function() {
					delete stack[event][guid];
				}
			}
		},
		
		fire: function(event, data) {

			if(debug) {
				console.log(event, data);
			}
			
			var i, listeners = stack[event];

			if(typeof listeners === "undefined") {
				return false;
			}

			for(i in listeners) (function() {
				listeners[i](data);
			})();
			
			
		}
		
	});
	
}();



/*
 *	Array: Convenience methods. 
 *	This is implemented in a 'safe' way for Element collections as well, where getPrevious and getNext are Element matches
 */
Array.implement({
	getIndex: function(currentItemIndex,direction) {
		/*
		*	currentItemIndex: zero-based index integer
		*	direction: positive/negative integer
		*/
		if($defined(this[currentItemIndex+direction])) {
			return currentItemIndex+direction;
		} else {
			return direction>0 ? 0 : this.indexOf(this.getLast());
		}
	},
	getNextIndex: function(currentItemIndex) {
		return $defined(this[currentItemIndex+1]) ? currentItemIndex+1 : 0;
	},
	getPreviousIndex: function(currentItemIndex) {
		return $defined(this[currentItemIndex-1]) ? currentItemIndex-1 : this.indexOf(this.getLast());
	},
	getNextItem: function(currentItem) {
		return $defined(this[currentItemIndex+1]) ? this[currentItemIndex+1] : this[0];
	},
	getPreviousItem: function(currentItem) {
		return $defined(this[currentItemIndex-1]) ? this[currentItemIndex-1] : this[this.indexOf(this.getLast())];
	}
});

RF.Class.Utilities = new Class({
	Implements:[Options,Log,Chain],
	options:{
		imageResetData : 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7', // image GIF binary data code [String]
		viewport : {
			width:null,
			height:null,
			iphone:{
				portrait:{
					width:980,
					height:1091					
				},
				landscape:{
					width:980,
					height:425				
				}
			},
			ipad:{
				portrait:{
					width:980,
					height:1208					
				},
				landscape:{
					width:980,
					height:660				
				}
			}
		}
	},
	ArrayBasics: {
		array: [],
		addItems:function(items){
			$$(items).each(function(item){this.array.include($(item));}, this);
		},
		addItem:function(item){
			this.addItems($splat($(item)));
		}
	},
	ClassBasics: {
		addClassToAll: function(items, css){
			items.each(function(i){
				i.addClass(css);
			});
		},
		clearClassFromAll: function(items, css){
			items.each(function(i){
				i.removeClass(css);
			});
		},
		swapClassFromAll:function(items, removecss, addcss){
			// requires moo more
			items.each(function(i){
				i.swapClass(removecss, addcss);
			});	
		}
	},
	EffectsHelper: {
		Overlay: function(overlayObject){
			this.overlay = new Element('div',{
				'id':'RF_Overlay_'+new Date().getTime(),
				'class': ($defined(overlayObject.cssClass) ? overlayObject.cssClass : ''),
				'styles':{
                    'opacity':0,
                    'visibility':'hidden'
                }
			}).inject(document.body,'top');
			this.overlay.style.cssText = this.FullScreen();
			return this.overlay;
		},
		FullScreen: function() {
	        var style = [];
			if(RF.Browser.ie6) {
                style.push('position:absolute');
                style.push('left   : expression((ignoreMe = document.documentElement.scrollLeft   ? document.documentElement.scrollLeft   : document.body.scrollLeft))');
                style.push('top    : expression((ignoreMe = document.documentElement.scrollTop    ? document.documentElement.scrollTop    : document.body.scrollTop))');
                style.push('width  : expression((ignoreMe = document.documentElement.clientWidth  ? document.documentElement.clientWidth  : window.innerWidth))');
                style.push('height : expression((ignoreMe = document.documentElement.clientHeight ? document.documentElement.clientHeight : window.innerHeight))');
            } else {
                style.push('position:fixed');
                style.push('left:0');
                style.push('top:0');
                style.push('width:100%');
                style.push('height:100%');
            }
            return style.join(';');
		}
	},
	ErrorHelper: function(errorObject) {
		/* Object Schema
		* errorObject : {
		* 	method:"MyClass : myFunction()", // string to help you identify which js file and function caused the error
		* 	error:e // the captured error object
		* }
		*/

		if (errorObject.error instanceof TypeError) {
			if(errorObject.method) {
				this.enableLog().log("JS ERROR : "+errorObject.method+" : TypeError; the type of a variable is not as expected : "+errorObject.error.message);
			}
		} else if (errorObject.error instanceof RangeError) {
			if(errorObject.method) {
				this.enableLog().log("JS ERROR : "+errorObject.method+" : RangeError; numeric variable has exceeded its allowed range : "+errorObject.error.message);
			}
		} else if (errorObject.error instanceof SyntaxError) {
			if(errorObject.method) {
				this.enableLog().log("JS ERROR : "+errorObject.method+" : SyntaxError; syntax error occurred while parsing : "+errorObject.error.message);
			}
		} else if (errorObject.error instanceof ReferenceError) {
			if(errorObject.method) {
				this.enableLog().log("JS ERROR : "+errorObject.method+" : ReferenceError; invalid reference used : "+errorObject.error.message);
			}
		} else {
			if(errorObject.method) {
				this.enableLog().log("JS ERROR : "+errorObject.method+" : Unidentified Error Type : "+errorObject.error.message);
			}
		}
	},
	RegexHelper: {
		replacer: function(expression,startValue,replacementValue) {
			return startValue.replace(new RegExp(expression,"i"), replacementValue);
		}
	},
	createSWFCloseButton: function(object) {
		this.closeButton = new Element('a',{
			'class': object.cssClass,
			'title': object.title,
			'tabIndex': object.tabIndex
		}).addEvents({
			'click': function(event) {if(event) object.eventFunction.pass(null,this)()},
			'keydown': function(event) {if(event && event.keyCode == 13) object.eventFunction.pass(null,this)()}
		});
		return this.closeButton;
	},
	scrollToTopAndRunFunction: function() {
		this.functionArg = arguments[0];
		if(window.getScroll && window.getScroll().y > 75) {
			new Fx.Scroll(window).toTop().chain(function(){
				this.functionArg();
				delete this.functionArg;
			}.bind(this));
		} else {
			this.functionArg();
			delete this.functionArg;
		}
	},
	existsContainerForInjection: function(elementType, id, parentId) {
		if($defined(document.id(id))) {
			return document.id(id);
		} else {
			return new Element(elementType,{'id':id}).inject(document.id(parentId));
		}
	},
	
	cleanUpMemory: function() {
		/*
		*	@description:
		*		iOS4 (iPod, iPhone & iPad) hog memory, typically with a 5Mb download limit in Safari Webkit, particularly noticeable
		*		if you have large filesize images
		*		Using this technique we can free up almost all available memory per Safari instance
		*		This is likely to leave a 43byte footprint for each 1x1 pixel GIF we create, which is minimal
		*	@arguments:
		*		arguments[0]: Element or array of Elements
		*/
		$each($splat(arguments[0]).flatten(), function(element) {
			this.destroyFromMemory(element);
		},this);	
	},
	destroyFromMemory: function() {
		/*
		*	@arguments:
		*		arguments[0]: Element
		*/
		/*
		*	If the nodeName of the element is an image, then set it's src to a 1x1 pixel GIF data binary value (see options of RF.Class.Utilities)
		*	first, before using Moo's destory() method to clean it from the DOM
		*/
		if(arguments[0].nodeName && arguments[0].nodeName=='IMG') arguments[0].src=this.options.imageResetData;
		arguments[0].destroy();
	},
	evaluateViewport: function() {
		/*
		*	On load, and before any orientation change, we need to detect the viewport dimensions
		*	This is so that we can provide device specific styling
		*/
		if (typeof window.innerWidth != 'undefined' && Browser.Platform.ipod) {
			this.options.viewport.width = window.innerWidth;
		    this.options.viewport.height = window.innerHeight;
			if(this.options.viewport.height == this.options.viewport.iphone.portrait.height || this.options.viewport.height == this.options.viewport.iphone.landscape.height) {
				document.getElement('html').addClass('iphone');
			}
			else if(this.options.viewport.height >= this.options.viewport.ipad.portrait.height || this.options.viewport.height >= this.options.viewport.ipad.landscape.height) {
				document.getElement('html').addClass('ipad');
			}
		}		
	},
	
    fixTransparentPngsInIE: function(){
		/* IE7 and 8 don't fade tranparent pngs content well: it loses the alpha channel and defaults to black until the opacity is 100% or 0% 
		This is a known IE bug. 
		The simplest fix is to apply a solid background colour to the image/parent div; i'm adding to the image
		If this isn't possible, then apply the AlphaLoader fix, this will reduce the black, but won't necessarily remove it entirely */
		if (window.XMLHttpRequest && document.all) {
			// test for IE7 & 8
			this.pngs = $$('img.png');
			// use the AlphaLoader fix suggested by http://www.snutt.net/testzon/ie_alpha/
			this.pngs.each(function(p){
                if (p.hasClass('png')) return;
                p.setStyles({
					'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\''+p.getProperty('src')+'\')'	
				});
			}, this);
		}	
	},
	getUrlVar : function (name, source){
	    source = source || window.location.href;
	    var query = new RegExp("[\\?&]" + name + "=([^&#]*)").exec(source);
	    if (query)
	        return query[1];
	    else
	        return null;
	}

});


/*
*	ACOUK-3409: Detect QuickTime
*	Browser.Plugins.QuickTime.isInstalled will return true if it's installed
*/
Browser.Plugins.QuickTime = {
	isInstalled:false,
	version:0
};

if (window.ActiveXObject) {
	var qtObject = null;
	/*
	*	Internet Explorer
	*/
	try {
		qtObject = new ActiveXObject('QuickTime.QuickTime');
		if(qtObject) {
			// In case QuickTimeCheckObject.QuickTimeCheck does not exist
			Browser.Plugins.QuickTime.isInstalled = true;
		}
	} catch (e) {
    	/*
 		* do not throw error
		*/
	}
	try {
		qtObject = new ActiveXObject('QuickTimeCheckObject.QuickTimeCheck');
	    // This generates a user prompt in Internet Explorer 7    
		if(qtObject) {
		   	// In case QuickTime.QuickTime does not exist
			Browser.Plugins.QuickTime.isInstalled = true;

		    // Get version
			
		    var qtVersion = qtObject.QuickTimeVersion.toString(16); // Convert to hex
			// Get the version major release
		    qtVersion = qtVersion.substring(0, 1);
		    Browser.Plugins.QuickTime.version = parseInt(qtVersion);
		}
	} catch (e) {
		/*
 		* do not throw error
		*/
	}	
} else if(navigator.mimeTypes && navigator.mimeTypes.length > 0) {
	/*
	*	Mozilla, Safari, Chrome
	*/

	/*
	* iOS Safari does not recognise 'enabledPlugin.version', only .name is available
	*/
	if(navigator.mimeTypes["video/quicktime"] && navigator.mimeTypes["video/quicktime"].enabledPlugin != null) {
		Browser.Plugins.QuickTime = {
			isInstalled:true,
			version:navigator.mimeTypes["video/quicktime"].enabledPlugin.name.match(/\d+/g) ? parseInt(navigator.mimeTypes["video/quicktime"].enabledPlugin.name.match(/\d+/g)[0]) : 0,
			revision:navigator.mimeTypes["video/quicktime"].enabledPlugin.name
		}
	}
}

RF.isEventSupported = (function(){
    var tagNames = {
        "select":"input","change":"input",
        "submit":"form","reset":"form",
        "error":"img","load":"img","abort":"img"
    };
    return function (eventName) {
        var el = document.createElement(tagNames[eventName] || "div");
        eventName = "on" + eventName;
        var isSupported = (eventName in el);
        if (!isSupported) {
            el.setAttribute(eventName, "return;");
            isSupported = typeof el[eventName] === "function";
        }
        el = null;
        return isSupported;
    };
})();

})(document.id);
;(function($){

/*

*	@ClassName: RF_TrackingHound
*	@Author: Stuart Thorne
*	@Company: Razorfish UK
* 	@Version: 1.1
*	@Date: 2009-10-22
*	@Description: Class checks to see whether any elements exist with the vendor CSS class names, and then processes them
*/
RF.Class.TrackingHound = new Class({
	Implements:[Options,RF.Class.Utilities],
	options:{
		elements:null,
		singleAction:null
	},
	initialize: function(options) {
		try {
			this.setOptions(options);
			if($defined(this.options.singleAction)) {
				if(this.options.singleAction.contains('EyeBlaster')) this.eyeBlasterNewImpression.pass(this.options.singleAction,this)();
				if(this.options.singleAction.contains('Atlas')) this.atlasNewImpression.pass(this.options.singleAction,this)();
			}
			if($defined(this.options.elements)) {
				$$(this.options.elements).each(function(element) {
					element.getProperty('class').split(' ').each(function(cssClassName) {
						if(cssClassName.contains('EyeBlaster')) this.addEvents.pass([element,this.eyeBlasterNewImpression,cssClassName],this)();
						if(cssClassName.contains('Atlas')) this.addEvents.pass([element,this.atlasNewImpression,cssClassName],this)();
						if(cssClassName.contains('s3_')) this.addEvents.pass([element,this.sophusNewImpression,cssClassName],this)();
					},this);
				},this);
			}
		} catch(e) {
			this.ErrorHelper({method:"RF.Class.TrackingHound : initialize()",error:e});
		}
	},
	addEvents: function() {
		var args=arguments; // leon.coto@razorfish.com
							// using this.args causes the this.args variable to be overwritten over and over again,
							// while setting var args constrains the variable to a closure.
		args[0].addEvents({
			'click':function(event){if(event) args[1].pass([args[2],args[0].get('href')])()}.bind(this),
			'keydown':function(event){if(event && event.keyCode == 13) args[1].pass([args[2],args[0].get('href')])()}.bind(this)
		});
	},
	eyeBlasterNewImpression: function() {
		// arguments[0] example = 'EyeBlaster_12345'
		var ebRand = Math.random()+'';
		ebRand = ebRand * 1000000;
		new Element('img', {
			src:window.location.protocol+'//bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&amp;ActivityID='+arguments[0].substring(arguments[0].indexOf('_')+1)+'&amp;ns=1'+ebRand,
			id:arguments[0],
			alt:'',
			width:1,
			height:1
		}).inject(document.body,'bottom');
	},
	atlasNewImpression: function() {
		// arguments[0] example = 'Atlas_9lmadb_PDFAudiTTRS2010_1'
		this.atlasTagReference = arguments[0].substring(arguments[0].indexOf('_')+1);
		/* If the tag reference already includes the '9l' prefix, do not include it in the Script src */
		if((/9l/.test(this.atlasTagReference)) == false) {
			this.atlasTagReference = '9lmadb_'+this.atlasTagReference;
		}
		new Element('script', {
			id: arguments[0],
			src: window.location.protocol+'//view.atdmt.com/jaction/'+this.atlasTagReference
		}).inject(document.body,'bottom');
	},
    sophusNewImpression: function() {
        if (arguments[0] == 's3_log') {
            s3_log(arguments[1]);
        }
    }
});

})(document.id);
;(function($){

RF.Class.MeetTheRangePromo = new Class({

	html: null,
	Implements:[RF.Class.Utilities],
	makePromoRequest:function(promourl){
		try {
			if(this.html != null){
				this.getContent();
			}else{
				if(this.options.promourl != null){
					this.req = new Request.HTML({
						method:'get',
						url: this.options.promourl,
						onSuccess: function(tree, elements, html, js){
							this.html = html;
							this.getContent();
						}.bind(this)	
					}).send();
				}				
			}
		} catch(e) {
			this.ErrorHelper({method:"MeetTheRangePromo Class : makePromoRequest()",error:e});
		}
	},
	getContent:function(){
		try {
			if(this.options.promourl != null) this.promoarea.set('html',this.html);
		} catch(e) {
			this.ErrorHelper({method:"MeetTheRangePromo Class : getContent()",error:e});
		}
	}
});

})(document.id);
;(function($){

RF.Class.NavigationContent = new Class({
	Implements: [RF.Class.Utilities, Events, Options],
	options: {
		tabLink: false,
		flyoutVar: false,
		contentArea: false
	},
	initialize:function(options){
		try {
			this.setOptions(options);
			this.tab = this.options.tabLink; if(!(this.tab)) return;
			this.flyout = this.options.flyoutVar; if(!(this.flyout)) return;
			this.content = document.id(this.options.contentArea); if(!(this.content)) return;
			this.submenulinks = '#' + this.options.contentArea + ' a';
		} catch(e) {
			this.ErrorHelper({method:"NavContent Class : initialize()",error:e});
		}
	}
});

})(document.id);;/* IE8  Fix -  getElementsByClassName */

if(!document.addEventListener){
   // IE8 -	
	 document.getElementsByClassName = Element.prototype.getElementsByClassName = function(class_names) {
        // Turn input in a string, prefix space for later space-dot substitution
        class_names = (' ' + class_names)
            .replace(/[~!@$%^&*()_+\-=,./';:"?><[\]{}|`#]/g, '\\$&')
            .replace(/\s*(\s|$)/g, '$1')
            .replace(/\s/g, '.');
        return this.querySelectorAll(class_names);
    };
}

/* '--  */



var tnStatus = {taborder:[], currtab:0, subnavorder:[], currsubnav:0};
RF.Class.TopNavigation = new Class({
	Extends: RF.Class.NavigationContent,
	Implements: [RF.Class.Utilities, Events, Options, RF.Class.MeetTheRangePromo],
	Binds:['clickEvent','touchEventAppleMobile'],
	options: {
		model: null,
		promourl: null
	},
	initialize:function(options){
		try {
			this.parent(options);
			this.setOptions(options);
			this.model = this.options.model;
			this.list = document.id('model-list');
			this.price = document.id('price');
			this.pricelabel = document.id('price-label');
			this.currency = document.id('currency');
			this.otr = document.id('otr');
			//this.finance = document.id('finance');
			//this.months = document.id('months');
			this.copy = document.id('overview-copy');
			this.cta = document.id('overview-cta');
			this.promoarea = document.id('meet-the-range');
			
			// add range button to fly-out
			if (document.id('range-list') == null) {
				var flyoutListWrap = new Element('div', {id: 'flyoutListWrap'});
				flyoutListWrap.wraps(document.id('model-list'));
				document.id('flyoutListWrap').setStyle('float','left');
				document.id('model-list').getParent().grab(new Element('ul', {id: 'range-list'}), 'top');
				document.id('range-list').grab(new Element('li', {id: 'range-link'}), 'top');	
				$$('.model-overview').grab(new Element('div', {id: 'explore-label'}), 'top');
				//if (RF.Browser.ie6) {DD_belatedPNG.fix('#range-link a');}
			}			
			this.explorelabel = document.id('explore-label');
			tnStatus.taborder.push(this.tab);		
			this.createModelList(this.options.model);
	
			this.callEvents();
		
		} catch(e) {
			this.ErrorHelper({method:"RF.Class.FlyOverNavigation : initialize()",error:e});
		}
	},
	modelsStr: '',
	createModelList:function(models){
		models.each(function(m, i){
			this.modelsStr += '<li><a class="prop35" href="'+m.url+'" title="Explore the '+m.model+' model">' + m.model + '</a></li>';
		},this);	
	},
	getInfo: function(prop, html){
		prop.empty();
		prop.set('html', html);	
	},
	clickEvent: function() {
		this.flyout.ClassBasics.clearClassFromAll(this.flyout.maintabs, 'on');
		this.tab.addClass('on');
	},
	callEvents:function(){
	
		this.tab.addEvents({
			'click':this.clickEvent,
			'mouseenter':function(e){
				if(e) {e.preventDefault()}
				// set-up range button
				this.getInfo(this.list, this.modelsStr);
				var allModels = $$('#model-list a'), rangeTag = "";
				if (allModels.length > 1) {
					document.id('model-list').setStyle('visibility','visible');
					rangeTag = " range";
				}
				else {
					document.id('model-list').setStyle('visibility','hidden');
					rangeTag = "";
				}
				if (this.tab.id.toUpperCase() == "A1" || allModels.length > 1) {					
					document.id('range-link').set('html', '<a class="prop35 on" href="'+this.tab.href+'" title="Explore the '+this.tab.id.toUpperCase()+rangeTag+'">'+this.tab.id.toUpperCase()+rangeTag+'</a>');
				} else {
					document.id('range-link').set('html', '<a class="prop35 on" href="'+this.model[0].url+'" title="Explore the '+this.tab.id.toUpperCase()+rangeTag+'">'+this.tab.id.toUpperCase()+rangeTag+'</a>');
				}
				document.id('tn-flyover').removeEvents('mouseenter').removeEvents('mouseleave').addEvents({
					'mouseenter':function(e){
						e.preventDefault();
						document.id(this.tab.id).addClass('fly');
					}.bind(this),
					'mouseleave':function(e){
						e.preventDefault();
						document.id(this.tab.id).removeClass('fly');
					}.bind(this)
				});						
				document.id('range-list').removeEvents('mouseenter').addEvents({
					'mouseenter':function(){
						if (allModels.length > 1) {$$('#range-link a').addClass('on'); this.flyout.ClassBasics.clearClassFromAll(allModels, 'on');}
						else {this.flyout.ClassBasics.clearClassFromAll(allModels, 'on'); document.id(allModels[0]).addClass('on');}
						
						document.id('range-landing').set('styles',{'background-position': '0 0'});
						document.id('overview-img').set({
							'styles' : {'background-position': '0 0'},
							'text'   : this.options.model[0].model
						});
						document.id('explore-label').setStyle('display','block');
						if (allModels.length > 1) {
							this.getInfo(this.explorelabel, (this.tab.id.toUpperCase()));
							document.id('explore-label').setStyle('display','block');
							$$('.finance-copy').setStyle('display','none');
							this.getInfo(this.copy, (this.model[0]['range-copy']));
							this.getInfo(this.cta, "<a class='prop35 btn-flyover explore-range' href='"+this.tab.href+"' title='"+this.tab.title+"'>"+this.tab.id.toUpperCase()+rangeTag+"</a>");
						} else {
							this.getInfo(this.explorelabel, (""));
							document.id('explore-label').setStyle('display','none');
							$$('.finance-copy').setStyle('display','inline');
							this.getInfo(this.price, (this.model[0].price));
							this.getInfo(this.pricelabel, (this.model[0].priceLabel+" "));
							if(this.model[0].priceLabel == ""){
								this.pricelabel.setStyles({'display':'inline'});
							} else {
								this.pricelabel.setStyles({'display':'block'});
							}
							this.getInfo(this.currency, (this.model[0].currency));
							this.getInfo(this.otr, (this.model[0].otr));
							this.getInfo(this.copy, (this.model[0].copy));
							// ACOUK-6031
//							if (this.tab.id.toUpperCase() == "A1") {
//								this.getInfo(this.cta, "<a class='prop35 btn-flyover explore-model' href='"+this.tab.href+"' title='Explore this model'>"+this.tab.id.toUpperCase()+rangeTag+"</a>");
//							} else {
//								this.getInfo(this.cta, (this.model[0].cta));
//							}
							this.getInfo(this.cta, (this.model[0].cta));
						}
					}.bind(this),
					'mouseleave':function(e){
						e.preventDefault();
					}.bind(this)
				});				
				
				document.id('range-landing').removeProperty('class');
				document.id('range-landing').addClass('overview-' + this.tab.get('id'));
				
				this.promoarea.empty();
				this.makePromoRequest();		
				
				var allSubLinks = $$(this.submenulinks);
				
				tnStatus.subnavorder = [];
				tnStatus.currsubnav = 0;
				
				allModels.each(function(a, i){
					tnStatus.subnavorder.push(a);
					a.addEvents({
						'mouseenter':function(){
							$$('#range-link a').removeClass('on');
							this.flyout.ClassBasics.clearClassFromAll(allModels, 'on');
							a.addClass('on');
							document.id('range-landing').set('styles',{'background-position': '0 ' + ((i+1)*212)*-1 + 'px'});
							document.id('overview-img').set({
								'styles' : {'background-position': '0 ' + ((i+1)*212)*-1 + 'px'},
								'text'   : this.options.model[i].model
							});
							document.id('explore-label').setStyle('display','none');
							$$('.finance-copy').setStyle('display','inline');
							this.getInfo(this.price, (this.model[i].price));
							this.getInfo(this.pricelabel, (this.model[i].priceLabel+" "));
							if(this.model[i].priceLabel == ""){
								this.pricelabel.setStyles({'display':'inline'});
							} else {
								this.pricelabel.setStyles({'display':'block'});
							}
							this.getInfo(this.currency, (this.model[i].currency));
							this.getInfo(this.otr, (this.model[i].otr));
							//this.getInfo(this.finance, (this.model[i].finance));
							//this.getInfo(this.months, (this.model[i].months));
							this.getInfo(this.copy, (this.model[i].copy));	
							this.getInfo(this.cta, (this.model[i].cta));

							RF.Moo.idE('#overview-cta a').addEvents({
								'keydown':function(m){
									if (m.key == 'tab'){
										m.stop();
										if(!(m.shift)){
											if(tnStatus.currsubnav < tnStatus.subnavorder.length -1){
												tnStatus.currsubnav += 1;
												tnStatus.subnavorder[tnStatus.currsubnav].focus();
											}else{
												if(tnStatus.currtab < tnStatus.taborder.length){
													tnStatus.taborder[tnStatus.currtab].focus();
												}else {
													document.id('closetn-flyover').focus();
												}
											}	
										}else{
											if(tnStatus.currsubnav > 0){
												tnStatus.currsubnav -= 1;
												tnStatus.subnavorder[tnStatus.currsubnav].focus();	
											}
										}	
									}		
								}
							});
										
						}.bind(this),
						'focus':function(m){
							m.stop();
							(m.target).fireEvent('mouseenter');	
						},
						'keydown':function(m){
							if (m.key == 'tab'){
								m.stop();
								if(!(m.shift)){
									(i < allModels.length) ? RF.Moo.idE('#overview-cta a').focus() : tnStatus.taborder[tnStatus.currtab].focus();
								}else{	
									if(tnStatus.currsubnav > 0){
										tnStatus.currsubnav -= 1;
										tnStatus.subnavorder[tnStatus.currsubnav].focus();	
									} else {
										if(tnStatus.currtab > 0){
											tnStatus.currtab -= 1;
											tnStatus.taborder[tnStatus.currtab].focus();
										}
									}	
								}
								
							}
						}
					});	
				},this);
				//allModels[0].fireEvent('mouseenter');
				document.id('range-list').fireEvent('mouseenter');
				
				// Add Omniture event tracking, just to the links in the top navigation flyover
				RF.Init.OmnitureTopNavEvents = new RF.Class.OmnitureTrackingManager({eventListeners:{elements:'#top-nav-content a'}});
				delete RF.Init.OmnitureTopNavEvents;
				// Trigger mouseenter tracking for the tab item
			}.bind(this),
			'focus':function(e){
				e.stop();
				(e.target).fireEvent('mouseenter');
			}.bind(this),
			'keydown':function(e){
				if(e.key == 'tab'){
					e.stop();
					if(!(e.shift)){
						if (tnStatus.currtab < tnStatus.taborder.length){
							RF.Moo.idE(this.submenulinks).focus();
							tnStatus.currtab += 1;
						}	
					}else{
						if(tnStatus.currtab > 0){
							tnStatus.currtab -= 1;
							tnStatus.taborder[tnStatus.currtab].focus();	
						} else {
							RF.Moo.idE('#logo-audi a').focus();
							this.flyout.slideClose();	
						}	
					}	
				}
			}.bind(this),
			'keyup':function(e){
				if (e.key == 'tab') {
					e.stop();
					this.flyout.slideOpen();
				}
			}.bind(this)
		});	
		
		/* Remove the click event when in Apple Mobile */
		if(Browser.Platform.ipod) {
			this.tab.addEvents({
				'click':this.touchEventAppleMobile
			});
		}
	},
	touchEventAppleMobile: function() {
		this.appleMobileEvent = arguments[0];
		this.appleMobileEvent.preventDefault();	
		this.tab.fireEvent('mouseenter');
		this.removeTouchEvents();
	},
	removeTouchEvents: function() {
		this.tab.removeEvents({
			'click':this.touchEventAppleMobile
		});
	}
});


window.addEvents({
	'domready': function() {
		if (document.id('top-nav')){			
			RF.Init.FlyOverNavigation = new RF.Class.FlyOverNavigation({id:'top-nav', maintabs:'#top-nav dd.tab a', flyover:'tn-flyover'});			
			(audiModelsObj) ? parseTopNavigationData(audiModelsObj) : new Request.JSON({
																			url:"/content/audi/audi-models.html",
																			method:'get',
																			onSuccess:function(jsonObj){
																				parseTopNavigationData (jsonObj);
																			}
																		});
		}
	}
});



/*

window.addEvents({
	'load': function() {
		console.log("PAGE LOADED");
		if (document.id('top-nav')){			
			console.log("PAGE LOADED");
			var reqTopNavHTML = new Request.HTML({
				method:'get',
				url: "/etc/designs/audi/inc/top-nav-flyover.html",
				onSuccess: function(tree, elements, html, js){
					document.id('top-nav-content').empty();
					document.id('top-nav-content').set('html',html);
					RF.Init.FlyOverNavigation = new RF.Class.FlyOverNavigation({id:'top-nav', maintabs:'#top-nav dd.tab a', flyover:'tn-flyover'});			
					RF.Init.RequestTopNavigation = (audiModelsObj) ? parseTopNavigationData(audiModelsObj) : new Request.JSON({
						url:"/content/audi/audi-models.html",
						method:'get',
						onSuccess:function(jsonObj){
							parseTopNavigationData (jsonObj);
						}
					}).send();
				}.bind(this)
			}).send();
		}
		
	}
});

*/

function parseTopNavigationData (jsonObj) {
	$each(jsonObj, function(value, key) {
		new RF.Class.TopNavigation({model:value, tabLink:RF.Moo.idE('#model-'+key+' a'), flyoutVar:RF.Init.FlyOverNavigation, contentArea:'tn-flyover'})
	});
	document.getElementsByClassName("flyover")[0].style.background="#d6d7d7 url(/etc/designs/audi/img/bgs/flyover-gradient.gif) repeat-x 0 0";
	document.getElementsByClassName("flyover")[0].style.borderLeft="1px solid #fff";
	document.getElementsByClassName("sub-menu")[0].style.background="url(/etc/designs/audi/img/bgs/flyover-btm.png) repeat-x center bottom";
};var mnStatus = {taborder:[], currtab:0};
RF.Class.MainNavigation = new Class({
	Extends: RF.Class.NavigationContent,
	options:{
		model: false
	},
	Implements: [RF.Class.Utilities, Events, Options],
	Binds:['clickEvent','touchEventAppleMobile'],
	initialize:function(options){
		try {
			this.parent(options);
			this.model = this.options.model; if(!(this.model)) return;
			this.callEvents();
			mnStatus.taborder.push(this.tab);
		} catch(e) {
			this.ErrorHelper({method:"MainNavigation Class : initialize()",error:e});
		}
	},
	html: function(){
		var getCols = $H(this.model);
		var colsStr = '';
		var newwin = ' title="Opens in a new window" target="_blank"';
		
		getCols.each(function(value, key){
			if(key != 'promo'){
				colsStr += '<div class="col"><ul>';
				value.each(function(e){
					
					if(e.newwindow == 'true'){
						(e.duplicateparent == 'true') ?  colsStr += '<li class="duplicate-parent"><a class="prop30" '+ newwin +' href="'+ e.href +'" title="'+e.listpoint+'">'+ e.listpoint +'</a>' : colsStr += '<li><a class="prop30" '+ newwin +' href="'+ e.href +'" title="'+e.listpoint+'">'+ e.listpoint +'</a>';
					}else{
						(e.duplicateparent == 'true') ?  colsStr += '<li class="duplicate-parent"><a class="prop30" href="'+ e.href +'" title="'+e.listpoint+'">'+ e.listpoint +'</a>' : colsStr += '<li><a class="prop30" href="'+ e.href +'" title="'+e.listpoint+'">'+ e.listpoint +'</a>';	
					}

					if(e.sublist){
						colsStr += '<ul>';
						e.sublist.each(function(s){
							(s.newwindow == 'true') ? 	colsStr += '<li><a class="prop30" '+ newwin +' href="'+ s.href +'" target="_blank" title="'+s.listpoint+'">'+ s.listpoint +'</a></li>' : 
								colsStr += '<li><a class="prop30" href="'+ s.href +'" title="'+s.listpoint+'">'+ s.listpoint +'</a></li>';
						});
						colsStr += '</ul>';
					}

					colsStr += '</li>';
				});
				colsStr += '</ul></div>';
			}	
		});

		// create promo div and put in place
		this.content.empty();
		
		var promoStr = '<h2><img src="'+ this.model.promo.h2img +'" alt="'+ this.model.promo.ctacopy +'"/><span>'+ this.model.promo.h2copy +'</span></h2><p><a class="prop30 btn-sml '+ this.model.promo.ctacss +'" href="'+ this.model.promo.ctahref +'" title="' + this.model.promo.ctatext + (this.model.promo.newwindow == 'true' ? ' [Opens in a new window]" target="_blank' : '') + '">'+ this.model.promo.ctacopy +'</a></p>';
		
		var createPromo = new Element('div', {
			'class': 'promo ' + this.model.promo.promocss,
			'html': promoStr						
		}).inject(this.content);
		
		var createCols = new Element('div',{'class': 'cols', 'html': colsStr}).inject(this.content);
		// Add Omniture event tracking, just to the links in the main navigation flyover
		RF.Init.OmnitureMainNavEvents = new RF.Class.OmnitureTrackingManager({eventListeners:{elements:'#main-nav-content a'}});
		delete RF.Init.OmnitureMainNavEvents;
	},
	clickEvent: function() {
		this.flyout.ClassBasics.clearClassFromAll(this.flyout.maintabs, 'on');
		this.tab.addClass('on');
	},
	callEvents:function(){
		this.tab.addEvents({
			'click':this.clickEvent,
			'mouseenter':function(){
				this.getContent();
				this.callSubMenuEvents();
			}.bind(this),
			'focus':function(e){
				e.stop();
				(e.target).fireEvent('mouseenter');
			}.bind(this),
			'keydown':function(e){
				if(e.key == 'tab'){
					e.stop();
					if(!(e.shift)){
						if (mnStatus.currtab < mnStatus.taborder.length){
							RF.Moo.idE(this.submenulinks).focus();
							mnStatus.currtab += 1;
						}
					}else{
						if(mnStatus.currtab > 0){
							mnStatus.currtab -= 1;
							mnStatus.taborder[mnStatus.currtab].focus();	
						} else {
							(document.id('top-nav')) ? RF.Moo.idE('#top-nav a').focus() : RF.Moo.idE('#logo-audi a').focus();
							this.flyout.slideClose();	
						}	
					}
				}	
			}.bind(this),
			'keyup':function(e){
				if (e.key == 'tab') {
					e.stop();
					this.flyout.slideOpen();
				}
			}.bind(this)
		});	
		
		/* Remove the click event when in Apple Mobile */
		if(Browser.Platform.ipod) {
			/*
			this.tab.addEvents({
				'click':this.touchEventAppleMobile
			});
			*/
		}
	},
	touchEventAppleMobile: function() {
		this.appleMobileEvent = arguments[0];
		this.appleMobileEvent.preventDefault();
		this.tab.fireEvent('mouseenter');
		this.removeTouchEvents();
	},
	removeTouchEvents: function() {
		this.tab.removeEvents({
			'click':this.touchEventAppleMobile
		});
	},
	getContent:function(){
		this.html();
	},
	callSubMenuEvents:function(){
		var allSubLinks = $$(this.submenulinks);
		var flyout = this.flyout;
		allSubLinks.getLast().addEvents({
			'keydown':function(e){
				if (e.key == 'tab' && !(e.shift)) {
					e.stop();
					if (mnStatus.currtab < mnStatus.taborder.length) {
						mnStatus.taborder[mnStatus.currtab].focus();
					} else {
						flyout.slideClose();
						if(RF.Moo.idE('#locate-dealer a')) RF.Moo.idE('#locate-dealer a').focus();
					}
				}						
			}
		});
		allSubLinks[0].addEvents({
			'keydown':function(e){
				if (e.key == 'tab' && (e.shift)) {
					e.stop();
					mnStatus.currtab -= 1;
					mnStatus.taborder[mnStatus.currtab].focus();
				}							
			}	
		});
	}	
});

window.addEvents({
	'domready': function() {
		if(document.id('main-nav')){

			RF.Init.MainNavSlide = new RF.Class.FlyOverNavigation({id:'main-nav', maintabs:'#main-nav dd.tab a', flyover:'mn-flyover'});
			RF.Init.MainNavContent = (mainNavObj) ? parseMainNavigationData(mainNavObj) : new Request.JSON({
				method:'get',
				url: "/content/audi/main_nav_flyover.html",
				onSuccess: function(jsonObj){
					parseMainNavigationData(jsonObj)
				}
			}).send();

			if(RF.Moo.idE('#locate-dealer a')) {
				RF.Moo.idE('#locate-dealer a').addEvents({
					'mouseenter': function(e){
						RF.Init.MainNavSlide.slideClose();
					},
					'focus': function(e){
						(e.target).fireEvent('mouseenter');
					},
					'keydown': function(e){
						if (e.key == 'tab' && !(e.shift)) {
							e.stop();
							document.id('closemn-flyover').focus();
						}
					}	
				});
			}
		}
	}
});


function parseMainNavigationData(jsonObj) {
	var i = 0;
	$each(jsonObj, function(value, key){
		new RF.Class.MainNavigation({model:value, tabLink: RF.Init.MainNavSlide.maintabs[i], flyoutVar:RF.Init.MainNavSlide, contentArea:'main-nav-content'});
		i+=1;
	});	
	document.getElementsByClassName("flyover")[1].style.background="#d6d7d7 url(/etc/designs/audi/img/bgs/flyover-gradient.gif) repeat-x 0 0";
	document.getElementsByClassName("flyover")[1].style.borderLeft="1px solid #fff";
	document.getElementsByClassName("sub-menu")[1].style.background="url(/etc/designs/audi/img/bgs/flyover-btm.png) repeat-x center bottom";
	document.getElementById("mn-flyover").getElementsByClassName("promo")[0].style.borderLeft="1px solid #ccc";
}
;(function($){

RF.Class.TargetBlankReplace = new Class({
	// call <a href="http://www.website-url.com" title="A title" rel="external">Link text</a>
	Implements: [Options,RF.Class.Utilities],
	options: {
		warning: 'Opens in a new window',
		warningseparator: ': '
	},
	initialize: function(options){
		try {
			this.setOptions(options);
			$$('a[rel="external"],a[href$=".pdf"]').each(function(a) {
                if (a.getProperty('target') === '_blank') return; //Already performed on this link
                var title = (a.getProperty('title') != null ? a.getProperty('title') + this.options.warningseparator + this.options.warning : this.options.warning);
				a.setProperties({
					'title'  : title,
					'target' : '_blank'
				});
                if (a.getElement('img')) a.getElement('img').setProperty('alt',title);
			}, this);
		} catch(e) {
			this.ErrorHelper({method:"TargetBlankReplacement Class : initialize()",error:e});
		}
	}
});

})(document.id);;(function($){

RF.Init.Utils = new RF.Class.Utilities();
RF.Tracking.ObjectOmnitureTracking = {
	eVar1: function() {
		/*
		*	Type: singleAction
		*	Description: SignUp for myAudi
		*/
		s.eVar1="Signup with Address/Signup no Address";
		RF.Init.Utils.log("s.eVar1: SignUp for myAudi: "+s.eVar1);
	},
	eVar2: function() {
		//
		//	Type: singleAction
		//	Description: Contact requests
		//
		s.eVar2=arguments[0];
		RF.Init.Utils.log("s.eVar2: Contact requests: "+s.eVar2);
	},
	eVar3: function() {
		/*
		*	Type: singleAction
		*	Description: Internal search term
		*/
		s.eVar3=(($defined(document.id('q')) && $defined(document.id('q').value) && document.id('q').value != "") ? document.id('q').value : "Search term unavailable");
		RF.Init.Utils.log("s.eVar3: Internal search term: "+s.eVar3);
	},
	eVar4: function() {
		/*
		*	Type: singleAction
		*	Description: Login to myAudi
		*/
		s.eVar4="Login myAudi";
		RF.Init.Utils.log("s.eVar4: Login to myAudi: "+s.eVar4);
	},
	eVar5: function() {
		/*
		*	Type: singleAction
		*	Description: Search result success (string value: "successful" : "not successful")
		*/
		s.eVar5=arguments[0];
		RF.Init.Utils.log("s.eVar5: Search result success: "+s.eVar5);
	},
	eVar6: function() {
		/*
		*	Type: singleAction
		*	Description: On successful ebrochure request
		*/
		s.eVar6="Ebrochure Request";
		RF.Init.Utils.log("s.eVar6: Ebrochure Request: "+s.eVar6);
	},
	eVar7: function() {
		s.eVar7="Newsletter Signup";
		RF.Init.Utils.log("s.eVar7: Newsletter Signup: "+s.eVar7);
		// On successful newsletter signup
		// include event6 newsletter signup
	},
	eVar8: function() {
		/*
		*	Type: singleAction
		*	Description: On successful request a quote submit
		*/
		s.eVar8="Quote Request";
		RF.Init.Utils.log("s.eVar8: Quote Request: "+s.eVar8);
	},
	eVar9: function() {
		/*
		*	Type: singleAction
		*	Description: When internal search is performed (on submit)
		*/
		s.eVar9="Internal Search Submit";
		RF.Init.Utils.log("s.eVar9: Internal Search Submit: "+s.eVar9);
	},
	eVar10: function() {
		s.eVar10="Trade in Appraisal Submission";
		RF.Init.Utils.log("s.eVar10: Trade in Appraisal Submission: "+s.eVar10);
		// On successful trade-in appraisal submission
		// inlude event13
	},
	eVar11: function() {
		s.eVar11="Send to a Friend";
		RF.Init.Utils.log("s.eVar11: Send to a Friend: "+s.eVar11);
		// On successful send to a friend
		// include event14
	},
	eVar12: function() {
		s.eVar12="Print";
		RF.Init.Utils.log("s.eVar12: Print: "+s.eVar12);
		// On print page open
		// include event15
	},
	eVar13: function() {
		s.eVar13="Monthly Payment Estimator";
		RF.Init.Utils.log("s.eVar13: Monthly Payment Estimator: "+s.eVar13);
		// On button click for monthly payment estimator
		// include event16
	},
	eVar14: function() {
		/*
		*	Type: singleAction
		*	Description: On dealer locator submission
		*/
		s.eVar14="Dealer Locator Submit";
		RF.Init.Utils.log("s.eVar13: Dealer Locator Submit: "+s.eVar13);
	},
	eVar15: function() {
		s.eVar15="Model Comparison Tool Click";
		RF.Init.Utils.log("s.eVar13: Model Comparison Tool Click: "+s.eVar13);
		// On button click for model comparison tool
		// include event18
	},
	eVar16: function() {
		s.eVar16=arguments[0];
		// ACC - Carline ID
	},
	eVar17: function() {
		s.eVar17=arguments[0];
		// ACC - Carline
	},
	eVar18: function() {
		s.eVar18=arguments[0];
		// ACC - Conflict
	},
	eVar19: function() {
		s.eVar19=arguments[0];
		// ACC - Engine Extension
	},
	eVar20: function() {
		s.eVar20=arguments[0];
		// ACC - Engine ID
	},
	eVar21: function() {
		s.eVar21=arguments[0];
		// ACC - Engine name
	},
	eVar22: function() {
		s.eVar22=arguments[0];
		// ACC - Exterior colour
	},
	eVar23: function() {
		s.eVar23=arguments[0];
		// ACC - Successful configuration
	},
	eVar24: function() {
		s.eVar24=arguments[0];
		// ACC - Instance
	},
	eVar25: function() {
		s.eVar25=arguments[0];
		// ACC - Interior colour
	},
	eVar26: function() {
		s.eVar26=arguments[0];
		// ACC - Model extension
	},
	eVar27: function() {
		s.eVar27=arguments[0];
		// ACC - Selected
	},
	eVar28: function() {
		s.eVar28=arguments[0];
		// A/B Testing
	},
	eVar29: function() {
		s.eVar29=arguments[0];
		// A/B Testing
	},
	eVar30: function() {
		/*
		*	Type: singleAction
		*	Description: User ID
		*/
		s.eVar30=arguments[0];
		RF.Init.Utils.log("s.eVar30: User ID: "+s.eVar30);
	},
	eVar31: function() {
		/*
		*	Type: singleAction
		*	Description: Video name, when video starts playing
		*/
		s.eVar31=arguments[0];
		RF.Init.Utils.log("s.eVar31: Video Name: "+s.eVar31);
	},
	eVar32: function() {
		/*
		*	Type: singleAction
		*	Description: Used with form plugin (unsure of purpose here for Phase 1 launch, so unlikely to be in use)
		*/
		s.eVar32=arguments[0];
	},
	eVar34: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Image Gallery: Image Gallery viewed, value RANGE MODEL e.g. "A1 Sportback Concept"
		*/
		s.eVar34=arguments[0];
		RF.Init.Utils.log("s.eVar34: Image Gallery viewed: "+s.eVar34);
	},
	eVar35: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Request a Test Drive (goes to Arrange a test drive webapp)
		*/
		s.eVar35=arguments[0];
		RF.Init.Utils.log("s.eVar35: Showroom - Request a Test Drive: "+s.eVar35);
	},
	eVar36: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Specifications selected
		*/
		s.eVar36=arguments[0];
		RF.Init.Utils.log("s.eVar36: Showroom - Specifications: "+s.eVar36);
	},
	eVar37: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Request a Brochure
		*/
		s.eVar37=arguments[0];
		RF.Init.Utils.log("s.eVar37: Showroom - Request a Brochure: "+s.eVar37);
	},
	eVar38: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Guided Tour : "RANGE MODEL"
		*/
		s.eVar38=arguments[0];
		RF.Init.Utils.log("s.eVar38: Showroom - Guided Tour: "+s.eVar38);
	},
	eVar39: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Switch Model
		*/
		s.eVar39=arguments[0];
		RF.Init.Utils.log("s.eVar39: Showroom - Switch Model: "+s.eVar39);
	},
	eVar40: function() {
		s.eVar40=arguments[0];
		RF.Init.Utils.log("s.eVar40: Showroom - View Full Range: "+s.eVar40);
		// eVar40: Flex SWF Showroom: View Full Range
	},
	eVar41: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Hotspot Started
		*/
		s.eVar41=arguments[0];
		RF.Init.Utils.log("s.eVar41: Showroom - Hotspot Started: "+s.eVar41);
	},
	eVar42: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Hotspot Completed
		*/
		s.eVar42=arguments[0];
		RF.Init.Utils.log("s.eVar42: Showroom - Hotspot Completed: "+s.eVar42);
	},
	eVar43: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF A5 Cabriolet Campaign - Call To Action
		*/
		s.eVar43=arguments[0];
		RF.Init.Utils.log("s.eVar43: Flex SWF A5 Cabriolet Campaign - Call To Action: "+s.eVar43);
	},
	eVar44:function(){
	    s.eVar44=arguments[0];
	    RF.Init.Utils.log("s.eVar44: Factory Tour - Select Area: "+s.eVar44) 
	},
	eVar45:function(){
	    s.eVar45=arguments[0];
	    RF.Init.Utils.log("s.eVar45: Form Section - Car Model Type: "+s.eVar45) 
	},
	eVar46: function() {
		s.eVar46=arguments[0];
		RF.Init.Utils.log("s.eVar46: Social Media Event: "+s.eVar46);
	},

	event1: function() {
		/*
		*	Type: singleAction
		*	Description: Login to myAudi
		*/
		s.events="event1";
		RF.Init.Utils.log("s.events: Login to myAudi (event1): "+s.events);
	},
	event2: function() {
		/*
		*	Type: singleAction
		*	Description: SignUp my Audi with full address
		*/
		s.events="event2";
		RF.Init.Utils.log("s.events: SignUp my Audi with full address (event2): "+s.events);
	},
	event3: function() {
		/*
		*	Type: singleAction
		*	Description: SignUp my Audi with no address
		*/
		s.events="event3";
		RF.Init.Utils.log("s.events: SignUp my Audi with no address (event3): "+s.events);
	},
	event4: function() {
		/*
		*	Type: singleAction
		*	Description: Download of document
		*/
		s.events="event4";
		RF.Init.Utils.log("s.events: Download of document (event4): "+s.events);
	},
	event5: function() {
		/*
		*	Type: singleAction
		*	Description: EBrochure Request
		*/
		s.events="event5";
		RF.Init.Utils.log("s.events: Ebrochure Request (event5): "+s.events);
	},
	event6: function() {
		/*
		*	Type: singleAction
		*	Description: Newsletter Signup
		*/
		s.events="event6";
		RF.Init.Utils.log("s.events: Newsletter Signup (event6): "+s.events);
	},
	event7: function() {
		//
		//	Type: singleAction
		//	Description: ACC - Configuration started
		//
		s.events="event7";
		RF.Init.Utils.log("s.events: ACC - Configuration started (event7): "+s.events);
	},
	event8: function() {
		//
		//	Type: singleAction
		//	Description: ACC - Successful configuration
		//
		s.events="event8";
		RF.Init.Utils.log("s.events: ACC - Successful configuration (event8): "+s.events);
	},
	event9: function() {
		//
		//	Type: singleAction
		//	Description: ACC - Extras chosen
		//
		s.events="event9";
		RF.Init.Utils.log("s.events: ACC - Extras chosen (event9): "+s.events);
	},
	event10: function() {
		//
		//	Type: singleAction
		//	Description: ACC - Accessory chosen
		//
		s.events="event10";
		RF.Init.Utils.log("s.events: ACC - Accessory chosen (event10): "+s.events);
	},
	event11: function() {
		//
		//	Type: singleAction
		//	Description: Quote request
		//
		s.events="event11";
		RF.Init.Utils.log("s.events: Quote request (event11): "+s.events);
	},
	event12: function() {
		/*
		*	Type: singleAction
		*	Description: Internal search submit
		*/
		s.events="event12";
		RF.Init.Utils.log("s.events: Internal search submit (event12): "+s.events);
	},
	event13: function() {
		//
		//	Type: singleAction
		//	Description : Trade in Appraisal Submission
		//
		s.events="event13";
		RF.Init.Utils.log("s.events: Trade in Appraisal Submission (event13): "+s.events);
	},
	event14: function() {
		//
		//	Type: singleAction
		//	Description: Send to a friend
		//
		s.events="event14";
		RF.Init.Utils.log("s.events: Send to a friend (event14): "+s.events);
	},
	event15: function() {
		//
		//	Type: singleAction
		//	Description: Print
		//
		s.events="event15";
		RF.Init.Utils.log("s.events: Print (event15): "+s.events);
	},
	event16: function() {
		//
		//	Type: singleAction
		//	Description: Monthly Payment Estimator Submit
		//
		s.events="event16";
		RF.Init.Utils.log("s.events: Monthly Payment Estimator Submit (event16): "+s.events);
	},
	event17: function() {
		/*
		*	Type: singleAction
		*	Description: Dealer Locator Submit
		*/
		s.events="event17";
		RF.Init.Utils.log("s.events: Dealer Locator Submit (event17): "+s.events);
	},
	event18: function() {
		//
		//	Type: singleAction
		//	Description: Model Comparison Tool Selected
		//
		s.events="event18";
		RF.Init.Utils.log("s.events: Model Comparison Tool Selected (event18): "+s.events);
	},
	event19: function() {
		/*
		*	Type: singleAction
		*	Description: Video started
		*/
		s.events="event19";
		RF.Init.Utils.log("s.events: Video started (event19): "+s.events);
	},
	event20: function() {
		/*
		*	Type: singleAction
		*	Description: Video completed
		*/
		s.events="event20";
		RF.Init.Utils.log("s.events: Video completed (event20): "+s.events);
	},
	event21: function() {
		//
		//	Type: singleAction
		//	Description: Video share selected
		//
		s.events="event21";
		RF.Init.Utils.log("s.events: Video share (event21): "+s.events);
	},
	event22: function() {
		/*
		*	Type: singleAction
		*	Description: Video full screen selected
		*/
		s.events="event22";
		RF.Init.Utils.log("s.events: Video full screen (event22): "+s.events);
	},
	event23: function() {
		/*
		*	Type: singleAction
		*	Description: Video replay selected
		*/
		s.events="event23";
		RF.Init.Utils.log("s.events: Video replay (event23): "+s.events);
	},
	event24: function() {
		/*
		*	Type: singleAction
		*	Description: Form Abandon
		*/
		s.events="event24";
		RF.Init.Utils.log("s.events: Form Abandon (event24): "+s.events);
	},
	event25: function() {
		/*
		*	Type: singleAction
		*	Description: Form Success
		*/
		s.events="event25";
		RF.Init.Utils.log("s.events: Form Success (event25): "+s.events);
	},
	event26: function() {
		/*
		*	Type: singleAction
		*	Description: Form Error
		*/
		s.events="event26";
		RF.Init.Utils.log("s.events: Form Error (event26): "+s.events);
	},
 	event27: function() {
		/*
		*	Type: singleAction
		*	Description: Form Complete
		*/
		s.events="event27";
		RF.Init.Utils.log("s.events: Form Start (event27): "+s.events);
	},
 	event41: function() {
		/*
		*	Type: singleAction
		*	Description: Social Media Events
		*/
		s.events="event41";
		RF.Init.Utils.log("s.events: Social Media Event (event27): "+s.events);
	},
	prop5: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Search results link URL (or term, if URL is not available)
		*/
		s.prop5=(($defined(arguments[0]) && $defined(arguments[0].getProperty('href'))) ? arguments[0].getProperty('href') : arguments[0].retrieve("OmnitureObject").title);
		RF.Init.Utils.log("s.prop5: Search results link URL: "+s.prop5);
	},
	prop29: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Quick Link (Value: "PAGE TITLE - LINK TITLE")
		*/
		s.prop29=location.pathname + '_ql_'+arguments[0].retrieve("OmnitureObject").linkDepth+'_' + arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-');
		RF.Init.Utils.log("s.prop29: Quick Link: "+s.prop29);
	},
	prop30: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Main menu link title. Includes flyover links
		*/
		s.prop30=location.pathname + '_mm_' + arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-');
		RF.Init.Utils.log("s.prop30: Main menu link title: "+s.prop30);
	},
	prop31: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Homepage Teaser (Link title + URL), from HTML components OR Flash Carousel on Homepage
		*/
		if(arguments[0].retrieve) {
			s.prop31=($defined(arguments[0].getProperty('href')) ? ("\""+arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-')+"\" : "+arguments[0].getProperty('href')) : "\""+arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-')+"\"");
		} else {
			s.prop31 = 'HPH: ' + retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-');
		}
		RF.Init.Utils.log("s.prop31: Homepage teaser: "+s.prop31);
	},
	prop32: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Bottom (Footer) menu 1 link title
		*/
		if (arguments[0].retrieve("OmnitureObject").title.indexOf('Find us on ') > -1) {
			var socialNetworkName = arguments[0].retrieve("OmnitureObject").title;
			socialNetworkName = socialNetworkName.replace('Find us on ','');
			socialNetworkName = socialNetworkName.replace(': Opens in a new window','');
			socialNetworkName = socialNetworkName.toLowerCase().replace(new RegExp(' ', 'g'), '');
			s.prop32=location.pathname + '_bm1_' + socialNetworkName;			
		} else {
			s.prop32=location.pathname + '_bm1_' + arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-');			
		}
		RF.Init.Utils.log("s.prop32: Bottom (Footer) menu 1: "+s.prop32);
	},
	prop33: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Bottom (Footer) menu 2 link title
		*/
		s.prop33=location.pathname + '_bm2_' + arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-');
		RF.Init.Utils.log("s.prop33: Bottom (Footer) menu 2: "+s.prop33);
	},
	prop35: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Top Menu (car model) link title. Includes flyover links
		*/
		s.prop35=location.pathname + '_tmc_' + arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-');
		RF.Init.Utils.log("s.prop35: Top Menu (car model) link title: "+s.prop35);
	},
	prop38: function() {
		/*
		*	Type: singleAction
		*	Description: Video Name, when video starts playing
		*/
		s.prop38=arguments[0];
		RF.Init.Utils.log("s.prop38: Video Name: "+s.prop38);
	},
	prop43: function() {
		/*
		*	Type: singleAction
		*	Description: Arrange A Test Drive - Login
		*/
		s.prop43=arguments[0];
		RF.Init.Utils.log("s.prop43: Arrange A Test Drive - Login: "+s.prop43);
	},
	prop44: function() {
		/*
		*	Type: singleAction
		*	Description: Arrange A Test Drive - Login
		*/
		s.prop44=arguments[0];
		RF.Init.Utils.log("s.prop44: Arrange A Test Drive - Confirmation: "+s.prop44);
	},
	prop45: function() {
		/*
		*	Type: singleAction
		*	Description: CO2 Calculator - Form
		*/
		s.prop45=arguments[0];
		RF.Init.Utils.log("s.prop45: CO2 Calculator - Results: "+s.prop45);
	},
	prop46: function() {
		/*
		*	Type: singleAction
		*	Description: CO2 Calculator - Form
		*/
		s.prop46=arguments[0];
		RF.Init.Utils.log("s.prop46: CO2 Calculator - Results: "+s.prop46);
	},
	prop49: function() {
		/*
		*	Type: singleAction
		*	Description: Navigation Panel mouseenter - The page & nav panel title (or number, e.g. 'Home - "nav_panel_1"'), where the user has performed a Navigation Panel mouseenter
		*/
		s.prop49=document.title.substring(0,document.title.indexOf('<')-1)+" - \""+arguments[0]+"\"";
		RF.Init.Utils.log("s.prop49: Navigation Panel mouseenter: "+s.prop49);
	},
	prop50: function() {
		/*
		*	Type: singleAction
		*	Description: A8 Campaign Hotspot and Highlight Feature Interaction
		*/
		s.prop50=arguments[0];
		RF.Init.Utils.log("s.prop50: A8 Campaign (Hotspots and Highlight Feature interaction): "+s.prop50);
	},
	prop52: function() {
		/*
		*	Type: singleAction
		*	Description: Social Media Event
		*/
		s.prop52=arguments[0];
		RF.Init.Utils.log("s.prop52: Social Media Event): "+s.prop52);
	}
}

})(document.id);;(function($){

/*	Project: Audi.co.uk
* 	Name: OmnitureTrackingManager Class
* 	Version: v1.4
* 	Author: Razorfish London
*/
RF.Class.OmnitureTrackingManager = new Class({
	Implements:[Options,RF.Class.Utilities],
	options:{
		eventListeners:{
			elements:null			
		},
		singleAction:{
			callToAction:null,
			eVars:null,
			events:null,
			props:null,
			linkDepth:null,
			linkType:'o'
		}
	},
	initialize: function(options){
		try {
			this.enableLog();
			this.setOptions(options);
			if($defined(this.options.eventListeners.elements)) this.processElements()
			if($defined(this.options.singleAction.eVars) || $defined(this.options.singleAction.events) || $defined(this.options.singleAction.props)) {
				this.options.singleAction.eVarsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(this.options.singleAction.eVars);
				this.options.singleAction.eventsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(this.options.singleAction.events);
				this.options.singleAction.propsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(this.options.singleAction.props);
				this.options.singleAction.propsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(this.options.singleAction.linkDepth);

				this.track.pass(this.options.singleAction,this)();
			}
		} catch(e) {
			this.ErrorHelper({method:"RF.Class.OmnitureTrackingManager : initialize()",error:e});
		}
	},
	processElements: function() {
		$$(this.options.eventListeners.elements).each(function(element,index) {
			this.elementOmnitureObject={};

			this.elementOmnitureObject.linkDepth = element.getProperty("data-link-position");
			this.elementOmnitureObject.title = (element.getProperty("title") || element.getProperty("value") || element.getProperty("alt") || "Value unavailable");
			/*
			*	Remove any potential link title clutter
			*/
			this.elementOmnitureObject.title = this.elementOmnitureObject.title.replace(": Opens in a new window", "");
			this.elementOmnitureObject.title = this.elementOmnitureObject.title.replace(" PDF document.", "");
			this.elementOmnitureObject.title = this.elementOmnitureObject.title.replace("Audi ", "");
			/*
			*	Store the linkType for processed HTML elements that are PDF documents
			*/
			if(element.getProperty('href') && element.getProperty('href') != "") {
				this.elementOmnitureObject.linkType = element.getProperty('href').test("/.pdf/","gi") ? 'd' : 'o';
			}
			
			element.getProperty('class').split(' ').each(function(className){
				if($H(RF.Tracking.ObjectOmnitureTracking).has(className)) {
					if(className.contains('eVar')) {
						this.elementOmnitureObject.eVars = className;
						this.elementOmnitureObject.eVarsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(className);
					} else if(className.contains('event')) {
						this.elementOmnitureObject.events = className;
						this.elementOmnitureObject.eventsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(className);
					} else if(className.contains('prop')) {
						this.elementOmnitureObject.props = className;
						this.elementOmnitureObject.propsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(className);
					}
				}
			},this);

			element.store("OmnitureObject",this.elementOmnitureObject);
			
			if($defined(element.retrieve("OmnitureObject"))) {
				element.addEvents({
					'click': function(event) {
						this.track.pass([element.retrieve("OmnitureObject"),element],this)();
					}.bind(this),
					'keydown': function(event) {
						if(event && event.type=='keydown' && event.keyCode == 13) {
							this.track.pass([element.retrieve("OmnitureObject"),element],this)();
						}
					}.bind(this)
				});
			}
		},this);
		delete this.elementOmnitureObject;
	},
	track: function() {
		/*
		*	singleAction (in-page code)
		*	arguments[0]: Object
		*		.eVars: String (the eVar name)
		*		.eVarsfunction: Function
		*		.events: String (the event name)
		*		.eventsFunction: Function
		*		.props: String (the prop name)
		*		.propsFunction: Function
		*		.callToAction: String OR arguments[1]: Element (with OmnitureObject in Hash store)
		*		.linkType: String - 'd' for custom download or 'o' for custom link
		*
		*	OR
		*	
		*	eventListeners (target)
		*	arguments[0]: target.retrieve("OmnitureObject")
		*		.eVars: String (the eVar name)
		*		.eVarsfunction: Function
		*		.events: String (the event name)
		*		.eventsFunction: Function
		*		.props: String (the prop name)
		*		.propsFunction: Function
		*		.title: The element title attribute
		*	arguments[1]: target
		*/
		var s=s_gi(s_account); // set Omniture account
		
		/* Call any props tracking function */
		if(arguments[0].propsFunction) {
			arguments[0].propsFunction.pass(arguments[0].callToAction||arguments[1])(); // Pass either the callToAction if there is one, or the element itself
			this.linkTrackVarsProps = arguments[0].props;
		} else {
			this.linkTrackVarsProps = "";
		}
		
		/* Check to see if we have ANY event, and if so add the 'events' suffix to the linkTrackVars */
		if(arguments[0].eventsFunction) {
			this.linkTrackVarsEvents=",events";
		} else {
			this.linkTrackVarsEvents = "";
		}
		
		/* Call any eVars tracking function */
		if(arguments[0].eVarsFunction) {
			/*
			*	Pass the function either:
			*		The callToAction, sent by a singleAction request
			*		Or the Title attribue, sent by a processed HTML element event
			*		Or the whole HTML Event Target, sent by a processed HTML element event
			*/
			arguments[0].eVarsFunction.pass(arguments[0].callToAction||arguments[0].title||arguments[1])();
			
			/* Additionally, if any events tracking function exists we have to declare 'events' in linkTrackVars */
			this.linkTrackVarsEVars=(this.linkTrackVarsProps!='' ? "," : "")+arguments[0].eVars;
		} else {
			this.linkTrackVarsEVars="";
		}
		
		/* Write out linkTrackVars statement: if eVars, or props, or events exist : else 'None' is the default */
		s.linkTrackVars=(this.linkTrackVarsProps!='' || this.linkTrackVarsEVars!='' || this.linkTrackVarsEvents!='') ? (this.linkTrackVarsProps+this.linkTrackVarsEVars+this.linkTrackVarsEvents) : 'None';
		//this.log("s.linkTrackVars("+s.linkTrackVars+")");
		
		/* Call any events tracking function */
		if(arguments[0].events) {
			arguments[0].eventsFunction.pass(arguments[0].callToAction||arguments[1])(); // Pass either the callToAction if there is one, or the element itself
			s.linkTrackEvents=arguments[0].events;
		} else {
			s.linkTrackEvents="None";
		}
		//this.log("s.linkTrackEvents(\'"+s.linkTrackEvents+"\')");
		/* Track:
		*	singleAction:
		*		arg1[Target Element]: null - there is no target element, as we've triggered this using 'in-page' code
		*		arg2[Link Type]: 'o' - Omniture's Custom Link property is the option to go for (others are 'd' for download or 'e' for exit link)
		*		arg3[Link Title]: arguments[0].callToAction
		*	eventListeners:
		*		arg1[Target Element]: arguments[1] - the target element (Anchor link)
		*		arg2[Link Type]: 'o' - Omniture's Custom Link property is the option to go for (others are 'd' for download or 'e' for exit link)
		*		arg3[Link Title]: arguments[1].retrieve("OmnitureObject").title - link title stored in the element's properties Hash
		*/
		this.trackTarget = $defined(arguments[1]) ? arguments[1] : true; // boolean true prevents 500ms delay for image request, and seems a sensible alternative to not having a 'this' object
		this.trackTitle = $defined(arguments[1]) ? arguments[1].retrieve("OmnitureObject").title : arguments[0].callToAction;
		
		/*
		*	Set the default linkType to 'o' for singleAction tracking where no linkType was specified.
		*	Processed HTML elements should have the linkType set already
		*/
		s.tl(this.trackTarget,(arguments[0].linkType||'o'),this.trackTitle);
		
		//this.log("s.tl("+this.trackTarget+","+(arguments[0].linkType||'o')+","+this.trackTitle+");");
	}
});

window.addEvents({
	'load': function() {
		RF.Tracking.OmnitureTrackingManager = new RF.Class.OmnitureTrackingManager({
			eventListeners:{
				elements:'a[class*=eVar],a[class*=event],a[class*=prop],input[type=submit]'
			}
		});
	}
});

})(document.id);;(function($){

RF.Class.FlyOverNavigation = new Class({
	Implements: [RF.Class.Utilities, Events, Options],
	options: {
		id: false,
		flyover: false,
		maintabs: false
	},
	initialize:function(options){
		try {
			this.setOptions(options);
			this.id = document.id(this.options.id); if(!(this.id)) return;
			this.flyover= document.id(this.options.flyover); if(!(this.flyover)) return;
			this.maintabs= $$(this.options.maintabs); if(!(this.maintabs)) return;
			var flyover = this.flyover;
			this.slideFx = new Fx.Slide(RF.Moo.idE('#'+ this.flyover.get('id') + ' .sub-menu'), {
				fps:100,
				duration:500,
				transition: Fx.Transitions.Quad.easeOut,
				link: 'cancel',
				onStart: function(){
					flyover.addClass('open')
				},
				onComplete: function(){
					if(this.open != true) flyover.removeClass('open');	
				}
			});

			this.addMainEvents();
			this.slideFx.hide();
			this.createClose();
			this.flyover.removeClass('hide');
		} catch(e) {
			this.ErrorHelper({method:"FlyOverNav Class : initialize()",error:e});
		}
	},
	slideClose: function(){
		this.slideFx.slideOut();
	},
	slideOpen: function(){
		this.slideFx.slideIn();
	},
	addMainEvents:function(){
		this.id.addEvents({
			'mouseover':function(e){
				this.flyoverEventTarget = e.target;
				var isTab = document.id(this.flyoverEventTarget).getParent().hasClass('tab');
				if (isTab != false || (this.flyoverEventTarget) != this.id) {
					if (isTab != false && (this.flyoverEventTarget) != this.flyover) this.slideOpen();
				} else this.ignoreAreas(e);
			}.bind(this),
			'mouseleave':function(e){
				this.ignoreAreas(e);
			}.bind(this)
		});
	},
	ignoreAreas: function(e){
		this.slideClose();
		this.flyoverEventTarget = e.target;
		if((this.flyoverEventTarget) == this.id || (this.flyoverEventTarget) == this.flyover) this.flyover.removeClass('open');
	},
	createClose:function(){
		this.closeButton = new Element('a',{
			id: 'close' + this.options.flyover,
			href: '#',
			html: (this.options.flyover.contains('mn-')) ? 'Close main site navigation' : 'Close car line site navigation'
		}).addClass('closeflyover').addEvents({
			'click':function(e){e.stop();},
			'focus':function(){this.slideClose();}.bind(this)
		}).inject(this.id, 'after');
	
	}
});

})(document.id);;/**
 * Kickup
 * 
 * Shows/hides a nav menu when a toggler element is clicked
 * @param {DOMElement} $el The outer container element of the kickup
 * @param {String} triggerEvent The event that triggers the opening/closing of the nav. click|hover
 * 
 * Expects structure
 * 
 * <div class="kickup" id="kickup-identifier">
 *     <a class="toggle">Toggle</a>
 *     <ul>
 *         <li><a href="#">Menu item 1</a></li>
 *         <li><a href="#">Menu item 2</a></li>
 *     </ul>
 * </div>
 */
RF.Class.Kickup = function ($el, triggerEvent) {

    // Default trigger event to click
    triggerEvent = triggerEvent || 'click';

    /**
     * Open/close animation
     * @type {Fx}
     */
    var tween = new Fx.Tween($el.getElement('.nav'), {
        property: 'opacity',
        duration: 250
    });

    // Start opacity at 0
    tween.set(0);

    // Set display:none on element if hidden to avoid blocking click events
    tween.addEvent('complete', function ($el) {
        if ($el.getStyle('opacity') === 0) {
            $el.hide();
        }
    });

    /**
     * Shows the nav menu
     */
    function open () {
        $el.addClass('open');
        $el.getElement('.nav').show();
        tween.cancel().start(1);
    }

    /**
     * Hides the nav menu
     */
    function close () {
        $el.removeClass('open');
        tween.cancel().start(0);
    }

    /**
     * Checks whether the nav is currently open
     * @return {Boolean} Whether the nav is open or not
     */
    function isOpen () {
        return $el.hasClass('open');
    }

    /**
     * Toggles the open state of the nav menu
     */
    function toggle () {
        (isOpen() ? close : open)();
    }

    // Bind to the .toggle element
    if (triggerEvent === 'click') {
        $el.getElement('.toggle').addEvent('click', toggle);
    } else if (triggerEvent === 'hover') {
        $el.getElement('.toggle').addEvent('mouseenter', open);
        $el.addEvent('mouseleave', close);
    }

    // Return interface
    return {
        open: open,
        close: close,
        isOpen: isOpen,
        $el: $el
    }

};


// Implementation
(function () {

    // Declare empty object to store kickups
    RF.Init.kickups = {};

    /**
     * Determines whether an element is a child of another element
     * @param  {DOMElement}  $potentialChild  The potential child element
     * @param  {DOMElement}  $potentialParent The potential parent element
     * @return {Boolean}    Whether $potentialParent is a descendant of $potentialChild
     */
    function isChildOf ($potentialChild, $potentialParent) {
        return $potentialParent.getElements('*').indexOf($potentialChild) > -1;
    }

    /**
     * Closes all open kickups
     */
    function closeAllKickups () {
        $each(RF.Init.kickups, function (kickup) {
            if (kickup.isOpen()) {
                kickup.close();
            }
        });
    }

    // Close kickup when there's a click outside of it
    document.addEvent('mousedown', function (e) {
        $each(RF.Init.kickups, function (kickup) {
            if (kickup.isOpen() && !isChildOf(this, kickup.$el)) {
                kickup.close();
            }
        }, e.target);
    });

    // Close kickups on escape key press
    document.addEvent('keyup', function (e) {
        if (e.code === 27) {
            closeAllKickups();
        }
    });
    
})();

window.addEvent('domready', function () {
    $$('.kickup').each(function ($el) {
        RF.Init.kickups[$el.id] = new RF.Class.Kickup($el, RF.Browser.ie ? 'hover' : 'click');
    });
});;RF.Moo.idE = function (a, b) {
    return (document.id(b) || document).getElement(a)
};
RF.Moo.idES = function (a, b) {
    return (document.id(b) || document).getElements(a)
}; 
RF.Global.CorrectSafariTabbing = function () {
    if (document.id("skip-nav")) {
        document.id("skip-nav").addEvents({
            focus: function () {
                document.id("skip-nav").setStyle("left", "0")
            },
            blur: function () {
                document.id("skip-nav").setStyle("left", "-10000px")
            },
            click: function () {
                if ((navigator.appVersion).match(/safari/i)) {
                    RF.Init.hrf = document.id("skip-nav").getProperty("href");
                    if (RF.Init.hrf.indexOf("#") == 0) {
                        RF.Init.hrf = RF.Init.hrf.substring(1, RF.Init.hrf.length)
                    }
                    if (!document.id("faux-target")) {
                        RF.Init.a = new Element("a").setStyles({
                            position: "absolute",
                            left: "-10000px"
                        }).set("html", "").setProperties({
                            id: "faux-target",
                            title: "Entered main content of page",
                            href: ""
                        }).injectTop(document.id(RF.Init.hrf))
                    }
                    document.id("faux-target").focus()
                }
            }
        })
    }
    if ((navigator.appVersion).match(/safari/i)) {
        $$(".target-top").each(function (a) {
            a.addEvent("click", function () {
                if (!document.id("faux-target-top")) {
                    RF.Init.b = new Element("a").setStyles({
                        position: "absolute",
                        left: "-10000px"
                    }).set("html", "").setProperties({
                        id: "faux-target-top",
                        title: "Top of the page",
                        href: ""
                    }).injectTop(document.id("top"))
                }
                document.id("faux-target-top").focus()
            })
        })
    }
};
RF.Global.PngFix = function () {
    RF.Init.allPNGs = $$("img[src$=.png]");
    RF.Init.filtered = RF.Init.allPNGs.filter(function (b, a) {
        if (b.getParents("#navigation-panels").length <= 0) {
            return b
        }
    });
    RF.Init.filtered.each(function (a) {
        if (a.hasClass("png") != true) {
            a.addClass("png")
        }
    })
};
RF.Global.ReinstateScriptsAfterAjaxCall = function () {
    RF.Global.PngFix();
    new RF.Class.TargetBlankReplace()
};
RF.Global.Facebook = function () {
    if (window.FB == undefined) {
        return
    }
    FB.Event.subscribe("edge.create", function (a) {
        RF.Init.OmnitureNavPanelSingleAction = new RF.Class.OmnitureTrackingManager({
            singleAction: {
                eVars: "eVar46",
                props: "prop52",
                events: "event41",
                callToAction: "Facebook:Add Like"
            }
        })
    });
    FB.Event.subscribe("edge.remove", function (a) {
        RF.Init.OmnitureNavPanelSingleAction = new RF.Class.OmnitureTrackingManager({
            singleAction: {
                eVars: "eVar46",
                props: "prop52",
                events: "event41",
                callToAction: "Facebook:Remove Like"
            }
        })
    });
    FB.Event.subscribe("comment.create", function (a) {
        RF.Init.OmnitureNavPanelSingleAction = new RF.Class.OmnitureTrackingManager({
            singleAction: {
                eVars: "eVar46",
                props: "prop52",
                events: "event41",
                callToAction: "Facebook:Add Comment"
            }
        })
    });
    FB.Event.subscribe("comment.remove", function (a) {
        RF.Init.OmnitureNavPanelSingleAction = new RF.Class.OmnitureTrackingManager({
            singleAction: {
                eVars: "eVar46",
                props: "prop52",
                events: "event41",
                callToAction: "Facebook:Remove Comment"
            }
        })
    })
};
window.addEvents({
    domready: function () {
        RF.Global.CorrectSafariTabbing();
        RF.Global.PngFix();
        RF.Init.TargetBlankReplace = new RF.Class.TargetBlankReplace();
        RF.Init.TrackingHound = new RF.Class.TrackingHound({
            elements: "*[class*=Atlas],a[class*=EyeBlaster],a[class*=s3_]"
        })
    },
    load: function () {
        RF.Global.Facebook()
    }
});
String.prototype.contains = function (a, b) {
    return (b) ? (b + this + b).indexOf(b + a + b) > -1 : this.indexOf(a) > -1
};
window.enableAlphaFilter = false;
/*
if (@_jscript_version == 5.5 || @_jscript_version==5.6 ||
   (@_jscript_version==5.7 &&
      navigator.userAgent.toLowerCase().indexOf("msie 6.") != -1)) {
  //ie6 or IE5.5 code
  window.enableAlphaFilter = true;
}
*/
Element.Properties.opacity.set = function (d, c) {
    if (!c) {
        if (d == 0) {
            if (this.style.visibility != "hidden") {
                this.style.visibility = "hidden"
            }
        } else {
            if (this.style.visibility != "visible") {
                this.style.visibility = "visible"
            }
        }
    }
    if (!this.currentStyle || !this.currentStyle.hasLayout) {
        this.style.zoom = 1
    }
    if (window.enableAlphaFilter) {
        this.style.filter = (d == 1) ? "" : "alpha(opacity=" + d * 100 + ")"
    }
    this.style.opacity = d;
    this.store("opacity", d)
};

(function (n, o, d) {
    var g = {
        messages: {
            required: "The %s field is required.",
            matches: "The %s field does not match the %s field.",
            valid_email: "The %s field must contain a valid email address.",
            valid_emails: "The %s field must contain all valid email addresses.",
            min_length: "The %s field must be at least %s characters in length.",
            max_length: "The %s field must not exceed %s characters in length.",
            exact_length: "The %s field must be exactly %s characters in length.",
            greater_than: "The %s field must contain a number greater than %s.",
            less_than: "The %s field must contain a number less than %s.",
            alpha: "The %s field must only contain alphabetical characters.",
            alpha_numeric: "The %s field must only contain alpha-numeric characters.",
            alpha_dash: "The %s field must only contain alpha-numeric characters, underscores, and dashes.",
            numeric: "The %s field must contain only numbers.",
            integer: "The %s field must contain an integer.",
            decimal: "The %s field must contain a decimal number.",
            is_natural: "The %s field must contain only positive numbers.",
            is_natural_no_zero: "The %s field must contain a number greater than zero.",
            valid_ip: "The %s field must contain a valid IP.",
            valid_base64: "The %s field must contain a base64 string."
        },
        callback: function (t) {}
    };
    var l = /^(.+)\[(.+)\]$/,
        k = /^[0-9]+$/,
        h = /^\-?[0-9]+$/,
        a = /^\-?[0-9]*\.?[0-9]+$/,
        q = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,6}$/i,
        c = /^[a-z]+$/i,
        i = /^[a-z0-9]+$/i,
        f = /^[a-z0-9_-]+$/i,
        b = /^[0-9]+$/i,
        r = /^[1-9][0-9]*$/i,
        p = /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})$/i,
        m = /[^a-zA-Z0-9\/\+=]/i;
    var j = function (v, t, y) {
        this.callback = y || g.callback;
        this.errors = [];
        this.fields = {};
        this.form = o.forms[v] || {};
        this.messages = {};
        this.handlers = {};
        for (var u = 0, x = t.length; u < x; u++) {
            var w = t[u];
            if (!w.name || !w.rules) {
                continue
            }
            this.fields[w.name] = {
                name: w.name,
                display: w.display || w.name,
                rules: w.rules,
                type: null,
                value: null,
                checked: null
            }
        }
        this.form.onsubmit = (function (z) {
            return function (A) {
                try {
                    return z._validateForm(A)
                } catch (B) {}
            }
        })(this)
    };
    j.prototype.setMessage = function (u, t) {
        this.messages[u] = t;
        return this
    };
    j.prototype.registerCallback = function (t, u) {
        if (t && typeof t === "string" && u && typeof u === "function") {
            this.handlers[t] = u
        }
        return this
    };
    j.prototype._validateForm = function (u) {
        this.errors = [];
        for (var t in this.fields) {
            if (this.fields.hasOwnProperty(t)) {
                var v = this.fields[t] || {};
                this._validateField(v)
            }
        }
        if (typeof this.callback === "function") {
            this.callback(this.errors, u)
        }
        if (this.errors.length > 0) {
            if (u && u.preventDefault) {
                u.preventDefault()
            } else {
                return false
            }
        }
        return true
    };
    j.prototype.validateField = function (t) {
        this.errros = [];
        this._validateField(t);
        return this
    };
    j.prototype._validateField = function (A) {
        var B = A.rules.split("|"),
            x = this.form[A.name];
        if (x && x != d) {
            A.type = x.type;
            A.value = x.value;
            A.checked = x.checked
        }
        if (A.rules.indexOf("required") === -1 && (!A.value || A.value === "" || A.value === d)) {
            return
        }
        for (var y = 0, v = B.length; y < v; y++) {
            var u = B[y],
                w = null,
                z = false;
            if (parts = l.exec(u)) {
                u = parts[1];
                w = parts[2]
            }
            if (typeof this._hooks[u] === "function") {
                if (!this._hooks[u].apply(this, [A, w])) {
                    z = true
                }
            } else {
                if (u.substring(0, 9) === "callback_") {
                    u = u.substring(9, u.length);
                    if (typeof this.handlers[u] === "function") {
                        if (this.handlers[u].apply(this, [A.value]) === false) {
                            z = true
                        }
                    }
                }
            } if (z) {
                var t = this.messages[u] || g.messages[u];
                if (t) {
                    var C = t.replace("%s", A.display);
                    if (w) {
                        C = C.replace("%s", (this.fields[w]) ? this.fields[w].display : w)
                    }
                    this.errors.push(C)
                } else {
                    this.errors.push("An error has occurred with the " + A.display + " field.")
                }
                break
            }
        }
    };
    j.prototype._hooks = {
        required: function (u) {
            var t = u.value;
            if (u.type === "checkbox") {
                return (u.checked === true)
            }
            return (t !== null && t !== "")
        },
        matches: function (u, t) {
            if (el = this.form[t]) {
                return u.value === el.value
            }
            return false
        },
        valid_email: function (t) {
            return q.test(t.value)
        },
        valid_emails: function (v) {
            var t = v.value.split(",");
            for (var u = 0; u < t.length; u++) {
                if (!q.test(t[u])) {
                    return false
                }
            }
            return true
        },
        min_length: function (u, t) {
            if (!k.test(t)) {
                return false
            }
            return (u.value.length >= t)
        },
        max_length: function (u, t) {
            if (!k.test(t)) {
                return false
            }
            return (u.value.length <= t)
        },
        exact_length: function (u, t) {
            if (!k.test(t)) {
                return false
            }
            return (u.value.length === t)
        },
        greater_than: function (t, u) {
            if (!a.test(t.value)) {
                return false
            }
            return (parseFloat(t.value) > parseFloat(u))
        },
        less_than: function (t, u) {
            if (!a.test(t.value)) {
                return false
            }
            return (parseFloat(t.value) < parseFloat(u))
        },
        alpha: function (t) {
            return (c.test(t.value))
        },
        alpha_numeric: function (t) {
            return (i.test(t.value))
        },
        alpha_dash: function (t) {
            return (f.test(t.value))
        },
        numeric: function (t) {
            return (a.test(t.value))
        },
        integer: function (t) {
            return (h.test(t.value))
        },
        decimal: function (t) {
            return (a.test(t.value))
        },
        is_natural: function (t) {
            return (b.test(t.value))
        },
        is_natural_no_zero: function (t) {
            return (r.test(t.value))
        },
        valid_ip: function (t) {
            return (p.test(t.value))
        },
        valid_base64: function (t) {
            return (m.test(t.value))
        }
    };
    n.FormValidator = j
})(window, document);


 SI_CBW.FileInputStyler = {
    htmlClass: "SI-FILES-STYLIZED",
    fileInputFieldClass: "fileInputField",
    fileInputWrapperClass: "fileInputFieldWrapper",
    initialised: false,
    fileInputCanBeStyled: false,
    fileInputField: null,
    fileInputFieldWrapper: null,
    fileInputFieldFakeButton: null,
    init: function () {
        this.initialised = true;
        var b = 0;
        if (window.opera || (b && b < 5.5) || !document.getElementsByTagName) {
            return
        }
        this.fileInputCanBeStyled = true;
        var a = document.getElementsByTagName("html")[0];
        a.className += (a.className != "" ? " " : "") + this.htmlClass
    },
    stylize: function (b) {
        this.fileInputField = b;
        this.fileInputFieldWrapper = this.fileInputField.parentNode;
        this.fileInputFieldFakeButton = this.fileInputFieldWrapper.parentNode;
        var a = this;
        this.fileInputFieldFakeButton.onmousemove = function (j) {
            if (typeof j == "undefined") {
                j = window.event
            }
            if (typeof j.pageY == "undefined" && typeof j.clientX == "number" && document.documentElement) {
                j.pageX = j.clientX + document.documentElement.scrollLeft;
                j.pageY = j.clientY + document.documentElement.scrollTop
            }
            var f = oy = 0;
            var i = this;
            if (i.offsetParent) {
                f = i.offsetLeft;
                oy = i.offsetTop;
                while (i = i.offsetParent) {
                    f += i.offsetLeft;
                    oy += i.offsetTop
                }
            }
            var c = j.pageX - f;
            var k = j.pageY - oy;
            var d = a.fileInputField.offsetWidth;
            var g = a.fileInputField.offsetHeight;
            if (!(0 < c && c < 392) || !(0 < k && k < 60)) {
                return
            }
            if (RF.Browser.ie7 || RF.Browser.ie8) {
                a.fileInputFieldWrapper.style.clip = "rect(10px,220px,15px,160px)";
                a.fileInputFieldWrapper.style.top = (k - (g / 2)) + 95 + "px";
                a.fileInputFieldWrapper.style.left = (c - (d - 30)) + 50 + "px"
            } else {
                a.fileInputFieldWrapper.style.top = k - (g / 2) + 100 + "px";
                a.fileInputFieldWrapper.style.left = c - (d - 30) + 20 + "px"
            }
        }
    },
    stylizeById: function (a) {
        if (!this.initialised) {
            this.init()
        }
        if (!this.fileInputCanBeStyled) {
            return
        }
        this.stylize(document.getElementById(a))
    }
};
