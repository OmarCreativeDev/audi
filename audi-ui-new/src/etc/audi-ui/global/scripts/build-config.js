module.exports = {
	options: {
		separator: "\n\n",
	},
	src: [

		// Omniture
		'src/etc/audi-ui/global/scripts/s_code.js',
		'src/etc/audi-ui/global/scripts/globalTagging.js',

		// Angular
		'src/etc/audi-ui/global/bower-components/angular/angular.js',

		// Angular Router
		'src/etc/audi-ui/global/bower-components/angular-route/angular-route.min.js',

		// Angular UI Router
		'src/etc/audi-ui/global/bower-components/angular-ui-router/release/angular-ui-router.min.js',

		// Angular UI Bootstrap
		'src/etc/audi-ui/global/bower-components/angular-bootstrap/ui-bootstrap-tpls.js',

		// Angular Resource
		'src/etc/audi-ui/global/bower-components/angular-resource/angular-resource.min.js',

		// Angular Animation
		'src/etc/audi-ui/global/bower-components/angular-animate/angular-animate.min.js',

		// Angular Sanitize
		'src/etc/audi-ui/global/bower-components/angular-sanitize/angular-sanitize.min.js',

		// Angular Mocks
		'src/etc/audi-ui/global/bower-components/angular-mocks/angular-mocks.js',

		// Angular lazy load
		'src/etc/audi-ui/global/bower-components/oclazyload/dist/ocLazyLoad.min.js',

		// HTML placeholder attr him for angular
		'src/etc/audi-ui/global/scripts/placeholder-support.js',

		// Salmon Utils for Angular
		'src/etc/audi-ui/global/scripts/salmon.utils/salmon.utils.angular.js',

		// Angulartics
		'src/etc/audi-ui/global/scripts/angulartics/salmon-angulartics.js',
		'src/etc/audi-ui/global/scripts/angulartics/salmon-angulartics-adobe.js',

	],
	dest: 'src/etc/audi-ui/global/scripts/dist/global.js',
};
