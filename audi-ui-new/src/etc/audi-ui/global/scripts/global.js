define(
	[
		'ng',
		'ng-ui-router',
		'ng-route',
		'ng-resource',
		'ng-animate',
		'ng-sanitize',
		'ng-mocks',
		'ng-ui-bootstrap',
		'placeholder-support',
		'angulartics',
		'angulartics-adobe',
		'ocLazyLoad'
	],
	function() {

		'use strict';

		if (window.console) console.log("global : loaded");

		return {
			loadApps: function() {

				jQuery(document).ready(function() {

					if (window.console) console.log("load apps");

					if ( document.getElementById('modelCarousel') != null ) {
						if (window.console) console.log('modelCarousel-bootstrap')
						require(['modelCarousel-bootstrap']);
					}


					if (document.getElementById('myAudi') != null ) {
						if (window.console) console.log('myAudi-bootstrap')
						require(['myAudi-bootstrap']);
					}

					/*if (document.getElementById('myAudiHeader') != null ) {
						if (window.console) console.log('myAudiHeader-bootstrap')
						require(['myAudiHeader-bootstrap']);
					}*/

					if (document.getElementById('myaudi-login') != null ) {
						if (window.console) console.log('myAudiLogin-bootstrap');
						require(['myAudiLogin-bootstrap']);
					}

					if ( document.getElementById('finance-calculator') != null ) {
						if (window.console) console.log('financeCalculator-bootstrap')
						require(['financeCalculator-bootstrap']);
					}

					if ( document.getElementById('osb') != null ) {
						if (window.console) console.log('osb-bootstrap')
						require(['osb-bootstrap']);
					}


				});
			}
		}
	}
);
