/**
 * @license Angulartics v0.16.3
 * (c) 2014 Luis Farzati http://luisfarzati.github.io/angulartics
 * Adobe analytics(Omniture) update contributed by http://github.com/ajayk
 * License: MIT
 */
(function(angular) {
'use strict';

/**
 * @ngdoc overview
 * @name angulartics.adobe.analytics
 * Enables analytics support for Adobe Analytics (http://adobe.com/analytics)
 */
angular.module('angulartics.adobe.analytics', ['angulartics'])
.config(['$analyticsProvider', function ($analyticsProvider) {

  $analyticsProvider.settings.trackRelativePath = true;


  // ***** THE ALMIGHTY CONVERSION TABLE *****
  $analyticsProvider.conversionTable = {
    // special values
    pageTitle: 'pageName',

    // props and eVars
    url: 'prop1',
    financeCalculatorFilters: ['prop45', 'eVar60'],
    invalidDeposit: 'prop4',
    quoteRefreshedFinanceCalculatorFilters: 'prop45',

    // events
    eventFinanceCalculatorStart: 'event72',
    eventQuoteRefreshed: 'event64',

    osbSeachLocation: ['prop59', 'eVar61'],
    osbVrm: ['prop52', 'eVar73'],
    osbSelectDealer: ['prop50', 'eVar72'],
    osbSelectedServices: ['prop51', 'eVar74'],
    osbSelectedCustomerCareOption:'prop47',
    fieldError: 'prop4',
    // modelCarousel
    modelCarousel:'prop35'
  }



  /**
   * Track PageLoad Event in Adobe Analytics
   * @name pageTrack
   *
   * @param {path} URL of the page loaded
   *
   *
   */
  $analyticsProvider.registerPageTrack(function (path, properties) {
    /*console.log(this);
    if (window.s) s.t([path]);*/

    var omniData = {
          props : {},
          evars : {},
          events: {},
          other: {}
        },
        desc = properties ? (properties['description'] ? properties['description'] : '') : '';

    // Set omniture data if we were passed properties
    if(typeof properties === 'object') {
      setProperties(omniData, properties);
    }

    //console.log('Firing pageLoad tag: description: ' + desc + ', omniData: ', omniData,  ' props: ', properties);

    salmon.globalTagging.getTrackingProperties('load', null, null, desc, omniData);
  });



  /**
   * Track Event in Adobe Analytics
   * @name eventTrack
   *
   * @param {string} action Required 'action' (string) associated with the event
   *
   *
   */
  $analyticsProvider.registerEventTrack(function (action, $event, properties) {
    var omniData = {
          props : {},
          evars : {},
          events: {},
          other : {}
        },
        desc = properties ? (properties['description'] ? properties['description'] : 'custom event') : 'custom event';

    // Set omniture data if we were passed properties
    if(typeof properties === 'object') {
      setProperties(omniData, properties);
    }

    //console.log('Firing event tag: desc: ' + desc + ', omniData: ', omniData, ' props: ', properties);

    if (window.s) {
      if(typeof action === 'string') {
        if(action.toUpperCase() === "DOWNLOAD") {
          salmon.globalTagging.getTrackingProperties.apply($event.target, ['click', 'action', 'o', desc, omniData]);
          //s.tl(this,'d',action);
        }
        else if(action.toUpperCase() === "EXIT") {
          salmon.globalTagging.getTrackingProperties.apply($event.target, ['click', 'action', 'o', desc, omniData]);
          //s.tl(this,'e',action);
        }
        else {
          salmon.globalTagging.getTrackingProperties.apply(($event ? $event.target : document), ['click', 'action', 'o', desc, omniData]);
        }
      }
      else {
        salmon.globalTagging.getTrackingProperties.apply(($event ? $event.target : document), ['click', 'action', 'o', desc, omniData]);
      }
    }
  });


  // Private functions
  var setProperties = function(omniData, properties) {
    var desc = '',
        dataTag, convTag, setTag, i;

    setTag = function(convTag) {
      switch(convTag.substring(0,4)) {
        case 'prop':
          omniData['props'][convTag] = properties[dataTag];
          break;
        case 'eVar':
          omniData['evars'][convTag] = properties[dataTag];
          break;
        case 'even': // event
          omniData['events'][convTag] = properties[dataTag];
          break;
        default:
          omniData['other'][convTag] = properties[dataTag];
          break;
      }
    }

    // Run through properties, converting them to omniture properties and setting the data in omniData
    for(dataTag in properties) {
      if(properties.hasOwnProperty(dataTag)) {
        if(typeof $analyticsProvider.conversionTable[dataTag] !== 'undefined') {
          convTag = $analyticsProvider.conversionTable[dataTag];
          if(typeof convTag === 'string') {
            setTag(convTag);
          } else {
            for(i=0; i<convTag.length; i++) {
              setTag(convTag[i]);
            }
          }
        }
      }
    }

    return desc;
  }

}]);
})(angular);
