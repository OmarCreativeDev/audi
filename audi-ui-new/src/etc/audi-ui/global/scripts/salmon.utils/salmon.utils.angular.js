/*! 
 * Salmon Angular utils
 Use for directives etc
 */


(function() {

'use strict';


	angular.module('sa.utils',[])

		.directive('saFocus', function () {
			var FOCUS_CLASS = "sa-focused";
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function (scope, element, attrs, ctrl) {
					ctrl.$focused = false;
					ctrl.$hadFocus = false;
					element.bind('focus', function (evt) {
						element.addClass(FOCUS_CLASS);
						scope.$apply(function () {
							ctrl.$focused = true;
						});
					}).bind('blur', function (evt) {
						element.removeClass(FOCUS_CLASS);
						scope.$apply(function () {
							ctrl.$focused = false;
							ctrl.$hadFocus = true;
						});
					});
				}
			}
		})

		.filter('address', function() {
        return function(address) {
            var address = String(address).replace(/[,]+/g, ",").split(",");
			var html = "";
			var i;
			for (i = 0; i < address.length; i++) {
				if(i < address.length-1)
                    html += address[i] + ",<br/>\n";
                else html += address[i] + ".";
			}
			return html; 
        };
    })


		    .factory('templateCache', ['$http', '$templateCache',function($http,$templateCache){

		        var get = function(url){
		                    $http.get(url, {cache:$templateCache});
		        }
		        return {
		            get: get
		        }


		    }])

		.filter('to_trusted', ['$sce', function($sce){
		        return function(text) {
		            return $sce.trustAsHtml(text);
		        };
		    }])

         .directive('ngEnter', function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    if(event.which === 13) {
                        scope.$apply(function (){
                            scope.$eval(attrs.ngEnter);
                        });

                        event.preventDefault();
                    }
                });
            };
        })


		.directive("audiLoader", 
			function() {
				return {
		            restrict: 'A',
		            link: function (scope, element, attrs) {
					
					var frameWidth = 80;
					var frameHeight = 28;
					var spriteWidth = 3360;
					var spriteHeight = 28;
					var backgroundOffset = 0;
					var timing;
					
					scope.init = function () { 
						element.addClass("audi-loader");
						console.log("loader init")
						timing = setInterval(scope.animateSprite, 60);
					};

					scope.animateSprite = function() {

						if(backgroundOffset >= 3360) {	
							backgroundOffset = 0; 
							element.css('background-position', '0px 0px'); 
						}
						else {
							backgroundOffset = backgroundOffset + frameWidth;
							element.css('background-position', backgroundOffset+'px 0px');
						}

						// console.log("backgroundOffset", backgroundOffset);
						// console.log("background-position", element.css('background-position'));

					};

					scope.init();
			    }
	        };
		})

		.directive('saFocusOn', ['$timeout', function($timeout) {
		    return {
		        restrict : 'A', 
		        link : function( $scope, $element, $attr ) {
		            $scope.$watch( $attr.saFocusOn, function( focusVal ) {
		                if(focusVal === true) {
		                    $timeout(function() {
		                        $element.focus();
		                    }, 50);
		                }
		            });
		        }
		    }
		}])

		.directive('saDisabler', function($compile) {
		  return {
		    link: function(scope, elm, attrs) {
		      var btnContents = $compile(elm.contents())(scope);
		      scope.$watch(attrs.ngModel, function(value) {
		        if (value) {
		          elm.html(scope.$eval(attrs.saDisabler));
		          elm.attr('disabled',true);
		          elm.addClass('icon-upload icon-white')
		        } else {
		          elm.html('').append(btnContents);
		          elm.attr('disabled',false);
		          elm.removeClass('icon-upload icon-white')
		        }
		      });
		    }
		  }
		})



		.factory('utilFunctions', function() {
            return {
               updatePasswordPopover : function(scope) {

				var popover = "";
				if ( scope.signupForm ) {
					
					if ( scope.signupForm.password.$error.chars ) {
						popover += '<p><span class="glyphicon glyphicon-remove"></span> 10 or more characters</p>';
					} else {
						popover += '<p><span class="glyphicon glyphicon-ok"></span> 10 or more characters</p>';
					}
					if ( scope.signupForm.password.$error.numbers ) {
						popover += '<p><span class="glyphicon glyphicon-remove"></span> at least 2 digits</p>';
					} else {
						popover += '<p><span class="glyphicon glyphicon-ok"></span> at least 2 digits</p>';
					}
					if ( scope.signupForm.password.$error.ucase ) {
						popover += '<p><span class="glyphicon glyphicon-remove"></span> at least 1 uppercase letter</p>';
					} else {
						popover += '<p><span class="glyphicon glyphicon-ok"></span> at least 1 uppercase letter</p>';
					}
					if ( scope.signupForm.password.$error.lcase ) {
						popover += '<p><span class="glyphicon glyphicon-remove"></span> at least 1 lowercase letter</p>';
					} else {
						popover += '<p><span class="glyphicon glyphicon-ok"></span> at least 1 lowercase letter</p>';
					}
				}
				scope.passwordPopover = '<div class="password-popover-content"><h3>Password validation</h3>' + popover + '</div>';
				return scope;
			}
            };
        });


	


angular.module('ui.bootstrap.affix', [])
.directive('affix', ['$window', function ($window) {
    return {
        $scope: true,
        link: function ($scope, $element, $attrs) {
            var relativeTo = // overide or use default: body (html for IE) element
                !!$attrs.relativeTo ? angular.element(window.document.getElementById($attrs.relativeTo)) : 
                    !$scope.$root.isIE ? // specify your own isIE check will default to body element
                        angular.element(window.document.getElementsByTagName('body')) : 
                        angular.element(window.document.getElementsByTagName('html')), // required by IE
                win = !!$attrs.relativeTo ? relativeTo : angular.element($window),
                // if a relativeTo other than the default is set we will 
                // offset the affixed position by it's offset
                relativeToOffset = null, 
                fixedAt = null;
            
            // boolean
            $scope.affixed = false;
            $scope.offset = 0;

            // Obviously, whenever a scroll occurs, we need to check and possibly 
            // adjust the position of the affixed $element.
            win.bind('scroll', checkPosition);

            // on resize recalculate fixedAt position 
            win.bind('resize', function () {
                $scope.$apply(function () {
                    $scope.affixed = false;
                });
                relativeToOffset = null;
                fixedAt = null;
                checkPosition();
            });

            // calculate if we need to affix element
            function checkPosition() {

                relativeToOffset = relativeToOffset || win[0].offsetTop;
                fixedAt = fixedAt || $element[0].offsetTop - relativeToOffset;
                
                $scope.$apply(function () {
                    $scope.offset = {top:relativeToOffset+'px'};
                    $scope.affixed = fixedAt <= relativeTo[0].scrollTop;                   
                });
            }
        }
    };
}]);




})();     
