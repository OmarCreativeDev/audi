require.config({
	baseUrl: '/etc/audi-ui',
	urlArgs: 'v=0.0.34' + (new Date()).getTime(), // Prevent caching
	paths: {
		'async': 'global/bower-components/requirejs-plugins/src/async',

		// Global

		// moved jQuery outside of RequireJS
		//'jquery':                 'global/bower-components/jquery/dist/jquery.min',

		'ng':                     'global/bower-components/angular/angular',
		'ng-ui-router':           'global/bower-components/angular-ui-router/release/angular-ui-router.min',
		'ng-route':               'global/bower-components/angular-route/angular-route.min',
		'ng-resource':            'global/bower-components/angular-resource/angular-resource.min',
		'ng-touch':            'global/bower-components/angular-touch/angular-touch.min',
		'ng-animate':             'global/bower-components/angular-animate/angular-animate.min',
		'ng-sanitize':            'global/bower-components/angular-sanitize/angular-sanitize.min',
		'ng-mocks':               'global/bower-components/angular-mocks/angular-mocks',
		'ng-ui-bootstrap':        'global/bower-components/angular-bootstrap/ui-bootstrap-tpls',
		
		'bootstrap-slider':       'global/scripts/bootstrap-plugins/bootstrap-slider',
		'ng-bootstrap-slider':    'global/scripts/bootstrap-plugins/angular-bootstrap-slider',

        'placeholder-support':      'global/scripts/placeholder-support',
		'angulartics':              'global/scripts/angulartics/salmon-angulartics',
		'angulartics-adobe':        'global/scripts/angulartics/salmon-angulartics-adobe',
		'omniture-scode':           'global/scripts/s_code',
		'omniture-global-tagging':  'global/scripts/globalTagging',

		// iLightBox dependency...

		'iLightbox':				'global/third-party-plugins/iLightBox/scripts/ilightbox.packed',
		'jMousewheel':				'global/third-party-plugins/iLightBox/scripts/jquery.mousewheel',
		'jAnimationFrame':			'global/third-party-plugins/iLightBox/scripts/jquery.requestAnimationFrame',

       	'angular-wizard':   'global/bower-components/angular-wizard/dist/angular-wizard',
        'angular-translate':   'global/bower-components/angular-translate/angular-translate.min',
        'angular-dialog-service' : 'global/bower-components/angular-dialog-service/dialogs.min',
        'lodash-underscore':      'global/bower-components/lodash/dist/lodash.underscore.min',

		'angular-img-fallback':   	'global/bower-components/angular-img-fallback/angular.dcb-img-fallback.min',

		// myAudiLogin App
		'sa-utils':     'global/scripts/salmon.utils/salmon.utils.angular',
		'sa-utils-modules-locationMap': 'global/scripts/salmon.utils.modules/locationMap/directive',
		'sa-utils-modules-addressLookup': 'global/scripts/salmon.utils.modules/addressLookup/addressLookupDirective',
		/// carousel
		'microfiche_carousel': 	'global/third-party-plugins/microfiche/microfiche',



	


		/* ****************************
				 ======= my Audi ========
			***************************
		*/





		/* ======= myAudiHeader ======== */
		'myAudiHeader': 						'apps/myaudi-header/myaudiheader',
		'myAudiHeader-bootstrap': 				'apps/myaudi-header/myaudiheader-bootstrap',
		'myAudiHeader-config': 					'apps/myaudi-header/myaudiheader-config',
		'myAudiHeader-ngViews-headerCtrl' : 	'apps/myaudi-header/ng-views/headerCtrl',
		'myAudiHeader-headerService': 			'apps/myaudi-header/ng-services/headerService',
		'myAudiHeader-apiService': 				'apps/myaudi-header/ng-services/apiService',		
		'myAudiHeader-apiServiceMocks': 		'apps/myaudi-header/ng-services/apiServiceMocks',


		/* ======= myAudiLogin ======== */
		'myAudiLogin':            'apps/myaudi-login/myaudilogin',
		'myAudiLogin-bootstrap':  'apps/myaudi-login/myaudilogin-bootstrap',
		'myAudiLogin-config':     'apps/myaudi-login/myaudilogin-config',
		'myAudiLogin-routes':     'apps/myaudi-login/myaudilogin-routes',

		'myAudiLogin-ngViews-mainCtrl':          	   'apps/myaudi-login/ng-views/mainCtrl',
		'myAudiLogin-accountLogin-loginCtrl':          'apps/myaudi-login/ng-components/accountLogin/loginCtrl',
		'myAudiLogin-accountLogin-logoutCtrl': 		   'apps/myaudi-login/ng-components/accountLogin/logoutCtrl',
		//'myAudiLogin-accountLogin-verifiedCtrl':       'apps/myaudi-login/ng-components/accountLogin/verifiedCtrl',
		'myAudiLogin-accountVerifyCtrl':               'apps/myaudi-login/ng-components/accountLogin/accountVerifyCtrl',

		'myAudiLogin-accountSignup-signupCtrl':        'apps/myaudi-login/ng-components/accountSignup/signupCtrl',

		'myAudiLogin-resetPassword-forgotPasswordCtrl':'apps/myaudi-login/ng-components/resetPassword/forgotPasswordCtrl',
		'myAudiLogin-resetPassword-resetPasswordCtrl': 'apps/myaudi-login/ng-components/resetPassword/resetPasswordCtrl',

		'myAudiLogin-apiService':                      'apps/myaudi-login/ng-services/apiService',
		'myAudiLogin-apiServiceMocks':                 'apps/myaudi-login/ng-services/apiServiceMocks',
		'myAudiLogin-userService':                     'apps/myaudi-login/ng-services/userService',

		'myAudiLogin-directive-salFocus':              'apps/myaudi-login/ng-directives/salFocus',
		'myAudiLogin-directive-salEmailUnique':        'apps/myaudi-login/ng-directives/salEmailUnique',
		'myAudiLogin-directive-salFormFeedbackIcons':  'apps/myaudi-login/ng-directives/salFormFeedbackIcons/directive',
		'myAudiLogin-directive-salPopoverHtmlUnsafe':  'apps/myaudi-login/ng-directives/salPopoverHtmlUnsafe/directive',
		'myAudiLogin-directive-salPasswordRepeat':     'apps/myaudi-login/ng-directives/salPasswordRepeat',
		'myAudiLogin-directive-salPasswordValidate':   'apps/myaudi-login/ng-directives/salPasswordValidate',
		'myAudiLogin-directive-salPasswordPopover':    'apps/myaudi-login/ng-directives/salPasswordPopover',
		'myAudiLogin-directive-salPasswordFocus':    'apps/myaudi-login/ng-directives/salPasswordFocus',





		/* ======= financecalculator ======== */
		'financeCalculator':            'apps/finance-calculator/financecalculator',
		'financeCalculator-bootstrap':  'apps/finance-calculator/financecalculator-bootstrap',
		'financeCalculator-config':     'apps/finance-calculator/financecalculator-config',
		'financeCalculator-routes':     'apps/finance-calculator/financecalculator-routes',
		'financeCalculator-calculatorService':     'apps/finance-calculator/ng-services/calculatorService',
		'financeCalculator-taggingService':        'apps/finance-calculator/ng-services/taggingService',
		'financeCalculator-calculateToolCtrl':     'apps/finance-calculator/ng-components/calculateTool/calculateToolCtrl',
        'financeCalculator-termsCtrl':             'apps/finance-calculator/ng-components/calculateTool/termsCtrl',
		'financeCalculator-selectVehicleCtrl':     'apps/finance-calculator/ng-components/selectVehicle/selectModelCtrl',
		'financeCalculator-emailQuoteCtrl':        'apps/finance-calculator/ng-components/calculateTool/emailQuoteCtrl',
		'financeCalculator-errorCtrl':             'apps/finance-calculator/ng-components/error/errorCtrl',




		/* ======= myAudi ======== */
		'myAudi':            			'apps/myAudi/myAudi',
		'myAudi-bootstrap':  			'apps/myAudi/myAudi-bootstrap',
		'myAudi-config':     			'apps/myAudi/myAudi-config',
		'myAudi-routes':  				'apps/myAudi/myAudi-routes',

		'myAudi-addCar-addCarCtrl': 		'apps/myAudi/ng-components/addCar/addCarCtrl',
		'myAudi-addOrder-addOrderCtrl': 	'apps/myAudi/ng-components/addOrder/addOrderCtrl',
		'myAudi-dashboard-dashboardCtrl': 	'apps/myAudi/ng-components/dashboard/dashboardCtrl',
		'myAudi-myGarage-myGarageCtrl': 	'apps/myAudi/ng-components/myGarage/myGarageCtrl',

		'myAudi-myAccountCtrl':         'apps/myAudi/ng-components/myAccount/myAccountCtrl',
		'myAudi-messageCtrl':         'apps/myAudi/ng-components/messages/messagesCtrl',
		'myAudi-sliceFilter':         'apps/myAudi/ng-filters/sliceFilter',
		'myAudi-communicationPreferencesCtrl': 'apps/myAudi/ng-components/communicationPreferences/communicationPreferencesCtrl',
		'myAudi-manageCarCtrl':         'apps/myAudi/ng-components/manageCar/manageCarCtrl',
		'myAudi-manageOrderCtrl':         'apps/myAudi/ng-components/manageOrder/manageOrderCtrl',

		'myAudi-apiService':            'apps/myAudi/ng-services/apiService',
		'myAudi-profileService':        'apps/myAudi/ng-services/profileService',
		'myAudi-messageService':        'apps/myAudi/ng-services/messageService',
		'myAudi-communicationPreferencesService':  'apps/myAudi/ng-services/communicationPreferencesService',
		'myAudi-carService': 				'apps/myAudi/ng-services/carService',

		'myAudi-apiServiceMocks':       'apps/myAudi/ng-services/apiServiceMocks',
		'myAudi-vehiclesMocks': 		'apps/myAudi/ng-services/mocks/vehiclesMocks',
		'myAudi-ordersMocks': 		    'apps/myAudi/ng-services/mocks/ordersMocks',

		'myAudi-directive-salEmailUnique':        'apps/myAudi/ng-directives/salEmailUnique',
		'myAudi-directive-salFormFeedbackIcons':  'apps/myAudi/ng-directives/salFormFeedbackIcons/directive',
		'myAudi-directive-salPopoverHtmlUnsafe':  'apps/myAudi/ng-directives/salPopoverHtmlUnsafe/directive',
		'myAudi-directive-salPasswordRepeat':     'apps/myAudi/ng-directives/salPasswordRepeat',
		'myAudi-directive-salPasswordValidate':   'apps/myAudi/ng-directives/salPasswordValidate',
		'myAudi-directive-salPasswordPopover':    'apps/myAudi/ng-directives/salPasswordPopover',
		'myAudi-directive-updateMileage':    'apps/myAudi/ng-directives/updateMileage/directive',
		'myAudi-directive-removeCar':    'apps/myAudi/ng-directives/removeCar/directive',
		'myAudi-directive-previousCar':    'apps/myAudi/ng-directives/previousCar/directive',
		'myAudi-directive-formattedInput':    'apps/myAudi/ng-directives/formattedInput',
		'myAudi-directive-contentLoader':    'apps/myAudi/ng-directives/contentLoader',
		'myAudi-directive-imgLoader':    'apps/myAudi/ng-directives/imageLoader',

		'myAudi-addressFilter':         'apps/myAudi/ng-filters/addressFilter',
		'myAudi-isEmptyFilter':         'apps/myAudi/ng-filters/isEmptyFilter',
		'myAudi-directive-addCar':   'apps/myAudi/ng-directives/addCar/directive',
		'myAudi-directive-addOrder':   'apps/myAudi/ng-directives/addOrder/directive',
		'myAudi-directive-carSummary':   'apps/myAudi/ng-directives/carSummary/directive',
		'myAudi-directive-orderSummary':   'apps/myAudi/ng-directives/orderSummary/directive',
		'myAudi-directive-centreLocator':   'apps/myAudi/ng-directives/centreLocator/directive',
		'myAudi-directive-centreStatus':    'apps/myAudi/ng-directives/centreLocator/centreStatus',
		'myAudi-directive-locationMap': 	'apps/myAudi/ng-directives/locationMap/directive',


		'myAudi-myCentresCtrl':         'apps/myAudi/ng-components/myCentres/myCentresCtrl',
		'myAudi-centresService':         'apps/myAudi/ng-services/centresService',
	//	'myAudi-centreSearchCtrl': 		'apps/myAudi/ng-components/myCentres/centreSearchCtrl',

		/* ======= osb ======== */
		'osb':            			'apps/osb/osb',
		'osb-bootstrap':  			'apps/osb/osb-bootstrap',
		'osb-routes':  				'apps/osb/osb-routes',

		'osb-landingCtrl': 'apps/osb/ng-components/landing/landingCtrl',
		'osb-wizardCtrl': 		'apps/osb/ng-components/wizard/wizardCtrl',
		'osb-service': 		'apps/osb/ng-services/osbService',

		'osb-directive-centreLocator':   'apps/osb/ng-directives/centreLocator/directive',
		'osb-centresService':         'apps/osb/ng-services/centresService',

		'osb-step-yourDetailsCtrl':  'apps/osb/ng-components/wizard/steps/yourDetailsCtrl',
		'osb-step-servicesCtrl':  'apps/osb/ng-components/wizard/steps/servicesCtrl',
		'osb-step-selectCentreCtrl':  'apps/osb/ng-components/wizard/steps/selectCentreCtrl',
		'osb-step-dateTimeCtrl':  'apps/osb/ng-components/wizard/steps/dateTimeCtrl',



			/* ======= modelCarousel ======== */
		'modelCarousel':            			'apps/modelCarousel/modelCarousel',
		'modelCarousel-bootstrap':  			'apps/modelCarousel/modelCarousel-bootstrap',
		'modelCarousel-config':     			'apps/modelCarousel/modelCarousel-config',

				/* ======= modelCarousel ======== */
		'modelCarousel-Ctrl': 		'apps/modelCarousel/ng-views/modelCarouselCtrl',
		'modelCarousel-Service': 	'apps/modelCarousel/ng-services/modelCarouselService'


	},
	shim: {
		'ng': { exports: 'angular' },
		'ng-ui-router': [ 'ng' ],
		'ng-route': [ 'ng' ],
		'ng-resource': [ 'ng' ],
		'ng-animate': [ 'ng' ],
		'ng-sanitize': [ 'ng' ],
		'ng-mocks': {
			deps: ['ng'],
			exports: 'angular.mock'
		},
		'ng-ui-bootstrap': [ 'ng' ],
		'placeholder-support': [ 'ng' ],
		'angulartics': [ 'ng' ],
		'angulartics-adobe': [ 'ng', 'angulartics' ]
	}
});

define('jquery', [], function() {

	if ( typeof jQuery == 'function' ) {
		return jQuery;
	} else {
		throw new Error('jQuery must be loaded before this');
	}

});
