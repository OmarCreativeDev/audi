RF.Class.ConfigPromo = new Class({
	baseUrl : "",
	modelDropDownId : "modelDropDown",
	colorDropDownId : "colorDropDown",
	fuelDropDownId : "fuelDropDown",
	configAnchorId : "configAnchor",
	colorFuelSuffix : ".colors_fuels",
	configCodesSuffix : ".config_codes",
	disabledClass : "disabled",
	currentCarLine : "",
	
	initialize : function(base, mid, cid, fid, aid, cfs, ccs) {
		this.baseUrl = base;
		this.modelDropDownId = mid;
		this.colorDropDownId = cid;
		this.fuelDropDownId = fid;
		this.configAnchorId = aid;
		this.colorFuelSuffix = cfs;
		this.configCodesSuffix = ccs;
	},
	
	refreshDropDowns : function() {
		this.refreshColorsAndFuels(this.colorFuelSuffix, this.colorDropDownId, this.fuelDropDownId);
	},
	
	refreshColorsAndFuels : function(urlSuffix, targetDropDownId1, targetDropDownId2) {
		var carLine = $(this.modelDropDownId).value;
		var targetDropDown1 = $(targetDropDownId1);
		var targetDropDown2 = $(targetDropDownId2);
		
		this.currentCarLine = "";
		this.updateStates();
		if (carLine == "") {
			return;
		}
		
		new Request.JSON({
	        url : this.baseUrl + urlSuffix,
	        onSuccess : function(data) {
	        	var selectedCarLine = $(this.modelDropDownId).value;
	        	if (carLine == selectedCarLine) {
	        		this.currentCarLine = carLine;
	        		this.clearDropDown(targetDropDown1);
	        		this.clearDropDown(targetDropDown2);
	        		if ($defined(data)) {
	        			if ($defined(data["colors"])) {
	        				this.fillDropDown(targetDropDown1, data["colors"]);
	        			}
	        			if ($defined(data["fuelTypes"])) {
	        				this.fillDropDown(targetDropDown2, data["fuelTypes"]);
	        			}
	        		}
	        		this.updateStates();
	        	}
	        }.bind(this)
	    }).get({ "car_line" : carLine });
	},
	
	clearDropDown : function(dropDown) {
		while (dropDown.length > 1) {
			dropDown.remove(1);
		}
	},
	
	fillDropDown : function(dropDown, data) {
		for (var i = 0; i < data.length; ++i) {
			var element = data[i];
			var option = document.createElement("option");
			option.text = element["text"];
			option.value = element["value"];
			dropDown.options.add(option);
		}
		if (data.length == 1) {
			dropDown.selectedIndex = 1;
		}
	},
	
	openConfigurator : function() {
		var carLine = $(this.modelDropDownId).value;
		var color = $(this.colorDropDownId).value;
		var fuelType = $(this.fuelDropDownId).value;
		if (carLine == "" || color == "" || fuelType == "") {
			return;
		}
		new Request.JSON({
	        url : this.baseUrl + this.configCodesSuffix,
	        onSuccess : function(data) {
	        	window.open("http://configurator.audi.co.uk/entry?mandant=accx-uk&vc=" +
		    			data["car_line"] + "&pr=" + color +
		    			"&next=model-page&partner=selected&v_fuel=" + data["fuel_type"],
	        			"AUDICONFIGURATOR",
	        			"scrollbars=no,directories=no,menubar=no,toolbar=no,width=1014,height=700,status=yes,resizable=yes");
	        	
	        	
	        }
	    }).get({ "car_line" : carLine, "fuel_type" : fuelType });
	},
	
	updateStates : function() {
		var colorDropDown = $(this.colorDropDownId);
		var fuelDropDown =  $(this.fuelDropDownId);
		var configAnchor = $(this.configAnchorId);
		
		if (this.currentCarLine == "") {
			this.clearDropDown(colorDropDown);
			this.clearDropDown(fuelDropDown);
			colorDropDown.disabled = true;
			colorDropDown.addClass(this.disabledClass);
		} else {
			colorDropDown.disabled = false;
			colorDropDown.removeClass(this.disabledClass);
		}
		
		if (colorDropDown.value == "") {
			fuelDropDown.disabled = true;
			if (fuelDropDown.getChildren().length > 2) {
				fuelDropDown.selectedIndex = 0;
			}
			fuelDropDown.addClass(this.disabledClass);
		} else {
			fuelDropDown.disabled = false;
			fuelDropDown.removeClass(this.disabledClass);
		}
		
		if (this.currentCarLine == "" || fuelDropDown.value == "" || colorDropDown.value == "") {
			configAnchor.addClass(this.disabledClass);
		} else {
			configAnchor.removeClass(this.disabledClass);
		}
		
		configAnchor.addEvent('click', function(){ 
			return (fuelDropDown.value == ""); 
		});
	}
})