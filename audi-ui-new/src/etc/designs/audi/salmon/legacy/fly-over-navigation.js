(function($){

RF.Class.FlyOverNavigation = new Class({
	Implements: [RF.Class.Utilities, Events, Options],
	options: {
		id: false,
		flyover: false,
		maintabs: false
	},
	initialize:function(options){
		try {
			this.setOptions(options);
			this.id = document.id(this.options.id); if(!(this.id)) return;
			this.flyover= document.id(this.options.flyover); if(!(this.flyover)) return;
			this.maintabs= $$(this.options.maintabs); if(!(this.maintabs)) return;
			var flyover = this.flyover;
			this.slideFx = new Fx.Slide(RF.Moo.idE('#'+ this.flyover.get('id') + ' .sub-menu'), {
				fps:100,
				duration:500,
				transition: Fx.Transitions.Quad.easeOut,
				link: 'cancel',
				onStart: function(){
					flyover.addClass('open')
				},
				onComplete: function(){
					if(this.open != true) flyover.removeClass('open');	
				}
			});

			this.addMainEvents();
			this.slideFx.hide();
			this.createClose();
			this.flyover.removeClass('hide');
		} catch(e) {
			this.ErrorHelper({method:"FlyOverNav Class : initialize()",error:e});
		}
	},
	slideClose: function(){
		this.slideFx.slideOut();
	},
	slideOpen: function(){
		this.slideFx.slideIn();
	},
	addMainEvents:function(){
		this.id.addEvents({
			'mouseover':function(e){
				this.flyoverEventTarget = e.target;
				var isTab = document.id(this.flyoverEventTarget).getParent().hasClass('tab');
				if (isTab != false || (this.flyoverEventTarget) != this.id) {
					if (isTab != false && (this.flyoverEventTarget) != this.flyover) this.slideOpen();
				} else this.ignoreAreas(e);
			}.bind(this),
			'mouseleave':function(e){
				this.ignoreAreas(e);
			}.bind(this)
		});
	},
	ignoreAreas: function(e){
		this.slideClose();
		this.flyoverEventTarget = e.target;
		if((this.flyoverEventTarget) == this.id || (this.flyoverEventTarget) == this.flyover) this.flyover.removeClass('open');
	},
	createClose:function(){
		this.closeButton = new Element('a',{
			id: 'close' + this.options.flyover,
			href: '#',
			html: (this.options.flyover.contains('mn-')) ? 'Close main site navigation' : 'Close car line site navigation'
		}).addClass('closeflyover').addEvents({
			'click':function(e){e.stop();},
			'focus':function(){this.slideClose();}.bind(this)
		}).inject(this.id, 'after');
	
	}
});

})(document.id);