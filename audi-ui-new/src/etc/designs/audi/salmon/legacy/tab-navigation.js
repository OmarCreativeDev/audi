(function($){

RF.Class.TabNavigation = new Class({
	Implements: [Chain, Events, Options, RF.Class.Utilities, RF.Class.LoadingSpinner],
	Binds:['successCallback','failureCallback','requestCallback'],
	options: {
		id: 'tabnav-area',
		tablinks: '#tabnav-area .tab-nav li a',
		content: 'tab-content',
		tabscontent: '#tabnav-area .tab-content',
		oncss: 'on',
		hidecss: 'js-hide',
		ajax: false,
		method: 'get',
		urls: [],
		fixtransparentpngs: {
			set: true
		},	
		loading:{
			'id':'loading',
			'src':'/etc/designs/audi/img/loader.gif',
			'width':'32',
			'height':'32',
			'styles':{
				'position':'absolute',
				'top':'200px',
				'left':'48%'
			}
		},
		page:null
	},
	initialize:function(options){
		try {
			this.setOptions(options);
			this.id = document.id(this.options.id); if(!(this.id)) return;
			this.tablinks = $$(this.options.tablinks); if(!(this.tablinks)) return;
			this.content = document.id(this.options.content); if(!(this.content)) return;
			this.tabscontent = $$(this.options.tabscontent);
			this.tabparents = [];
			this.tablinks.each(function(t){
				this.tabparents.push(t.getParent('li'));
			},this);
			
			this.tabEvents(this.tablinks);	
			
			var defaultPage = options.page || 0;
			if(this.tablinks && this.tablinks[defaultPage]) this.tablinks[defaultPage].fireEvent('click');	
			
            Element.Events.tabNavigationSuccess = {};

		} catch(e) {
			this.ErrorHelper({method:"TabNavigation Class : initialize()",error:e});
		}
	},
	applyOnState:function(t){
		this.ClassBasics.clearClassFromAll(this.tabparents, this.options.oncss);
		if(t.getParent('li')) t.getParent('li').addClass(this.options.oncss);
	},
	tabEvents:function(tabs){
		if(this.options.ajax == false){
			// hide/show (css) version
			tabs.each(function(t, i){
				t.addEvents({
					'click':function(e){
						if(e) e.stop();
						this.ClassBasics.addClassToAll(this.tabscontent, this.options.hidecss);
						this.tabscontent[i].removeClass(this.options.hidecss);
						this.applyOnState(t);
					}.bind(this)	
				});
			},this);	
		} else {
			// ajax call version
			tabs.each(function(t, i){
				t.addEvents({
					'click':function(e){
						if(e) e.stop();
						this.requestCallback();
						this.content.get('tween',{property : 'opacity'}).start(0).chain(function() {
							try {
								new Request.HTML({
									url:this.options.urls[i], 
									method:this.options.method,
									options:this.options,
									onSuccess:this.successCallback,
									onFailure:this.failureCallback
								}).send();
							} catch(e) {
								this.ErrorHelper({method:"TabNavigation Class : tabEvents()",error:e});
							}
							this.applyOnState(t);
						}.bind(this));
						
					}.bind(this)	
				});
			},this);
		}
	},
	requestCallback: function() {
		this.addSpinner.pass([document.id(this.options.content).getParent(),'loading-tab-content'],this)();
	},
	successCallback: function(tree, elements, html, js){
		this.content = document.id(this.options.content);
		if(!(this.content)) return;
		/* test the content to see if it is actually trying to drop in an error page by testing for the header image/span content of h1 */
		var anyH1s = elements.filter('h1');
		if(anyH1s.length > 0 && anyH1s[0].getElements('img[src*=error]')){
			this.content.set('html', '<div class="ajax-tab-error"><h2>Content error</h2><p>Unfortunately, for technical reasons we are not currently able to load the content you requested. This will be looked at as soon as possible.</p></div>');		
			this.content.getElements('div')[0].removeClass(this.options.hidecss);
		}else{
			this.content.set('html', html);	
			this.content.getElements('div')[0].removeClass(this.options.hidecss);
			RF.Global.ReinstateScriptsAfterAjaxCall();
		}
		this.fixTransparentPngsInIE();
        if(document.id('loading-tab-content')) document.id('loading-tab-content').fade('hide'); /* ACOUK-6692 Like button not displaying in Heritage IE8 */
		this.content.get('tween', {property:'opacity'}).start(1);
        if (window.FB) FB.XFBML.parse(document.getElementById(this.options.content));
		
        document.fireEvent('tabNavigationSuccess');
        if (window.FB) FB.XFBML.parse(this.content);
	},
	failureCallback: function(){
		this.ErrorHelper({method:"TabNavigation Class : failureCallback()",error:e});
	},
	fixTransparentPngsInIE: function(){
		/* IE7 and 8 don't fade tranparent pngs content well: it loses the alpha channel and defaults to black until the opacity is 100% or 0% 
		This is a known IE bug. 
		The simplest fix is to apply a solid background colour to the image/parent div; i'm adding to the image
		If this isn't possible, then apply the AlphaLoader fix, this will reduce the black, but won't necessarily remove it entirely */
		if (window.XMLHttpRequest && document.all && this.options.fixtransparentpngs.set) {
			// test for IE7 & 8
			this.pngs = $$('.tab-content img.png');
			// use the AlphaLoader fix suggested by http://www.snutt.net/testzon/ie_alpha/
			this.pngs.each(function(p){
				
				var src = p.getProperty('src');
				p.set({
					width: p.getSize().x,
					height: p.getSize().y,
					src: '@img.file.src.path@/blank.gif'
				})
				.setStyle('filter', 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\''+ src +'\')');

			}, this);
		}	
	}
});

})(document.id);