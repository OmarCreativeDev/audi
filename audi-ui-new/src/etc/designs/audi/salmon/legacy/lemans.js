/**
 * Lemans2012FormStage
 *
 * Basic functionality for displaying different stages in the Le Mans form.
 */
RF.Class.Lemans2012FormStage = new Class({
    Implements:[Options],
    options: {
        id: null
    },

    initialize: function(options){
        this.setOptions(options);
    },

    show: function() {
        $(this.options.id).setStyle('display', 'block');
    },

    hide: function() {
        $(this.options.id).setStyle('display', 'none');
    }
});

/**
 * Lemans2012Form
 *
 * Multi-Stage form to submit videos for the LeMans 2012 race.
 */
RF.Class.Lemans2012Form = new Class({

    Implements:[Options],

    options: {
        selectors:[]
    },

    overlayId:'',
    formId:'',

    overlay: null,
    cta: null,
    form: null,

    stages: {},

    // Form stages ids
    detailsStage: 'lemans2012-details',
    fileSelectionStage: 'lemans2012-file-selection', // selection + error
    confirmStage: 'lemans2012-confirm',
    uploadStage: 'lemans2012-upload', // uploading + complete
    thankYouStage: 'lemans2012-thank-you',


    // Current stage id
    currentStage: null,
    timeoutId:null,
    progressTimeoutId:null,
    blockPolling:false,

    // Validation
    fields: {
            'fullname':{name:'fullname', rules:'required|callback_checkinvalidchars|max_length[39]', display:'full name'},
            'phonenumber':{name:'phonenumber', rules:'required|callback_checkphonenumber|min_length[9]|max_length[15]', display:'phone number'},
            'emailaddress':{name:'emailaddress', rules:'required|valid_email', display:'email address'},
            'termsagree':{name:'termsagree', rules:'required', display:'terms agreement'},
            'fileupload':{name:'fileupload', rules:'required', display:'file upload'},
            'captcha-answer':{name:'captcha-answer', rules:'required', display:'captcha'}
    },

    uploadResponse:null,
    skipToConfirmStage: false,

    initialize: function(options){
        //----------------------------------
        //
        // Set up
        //
        //----------------------------------
        this.setOptions(options);

        // Elmements
        this.formTemplate = $(this.options.selectors[0]);
        this.cta = $(this.options.selectors[1]);

        // ID's
        this.overlayId = this.options.selectors[2];
        this.formId = this.options.selectors[3]

        //----------------------------------
        //
        // Overlay setup
        //
        //----------------------------------
        this.overlay = new RF.Class.Overlay(
            $(document.body),
            {
                id: this.overlayId,

                onShow: function(){
                    this.createForm();
                }.bind(this),

                onClose: function(){
                    this.disposeForm();
                }.bind(this)
            }
        );

        //----------------------------------
        //
        // Event handlers
        //
        //----------------------------------

        this.cta.addEvent('click', function(event){
            this.cta_clickHandler();
        }.bind(this));

    },

    createForm: function() {

        var form, id = this.formId+'-wrapper', captchaValidator, fileupload;

        form = new Element('div', {id:id});
        form.set('html', this.formTemplate.get('html'));
        $(document.body).grab(form);
        this.createStages();

        id = '#' + id;
        $$(id + ' .close').addEvent('click', function(event){
            event.stop();
            this.overlay.close();
        }.bind(this));

        $$(id + ' .form-stage .continue').addEvent('click', function(event){
            this.nextStage();
        }.bind(this));

        $$('input[type=file]').addEvent('change', function(event){
            this.nextStage();
        }.bind(this));

        $$('a.edit').addEvent('click', function(event){
            event.stop();
            if (event.target.hasClass('your-details')) {
                this.skipToConfirmStage = true;
                this.changeStage(this.detailsStage);
            } else if (event.target.hasClass('your-video')) {
                this.changeStage(this.fileSelectionStage);
            }
        }.bind(this));

        $$('.box .try-again')[0].addEvent('click', function(event){
            event.stop();
            this.uploadResponse = null;
            this.prepareStage(this.fileSelectionStage);
        }.bind(this));

        $('test-captcha').addEvent('click', function (event) {
            event.stop();
            this.sendCaptcha();
        }.bind(this));

        $$('.captcha-container input[type=text]')[0].addEvent('keypress', function(event){
            if (event.code === 13) { // Enter key
                event.stop();
            }
        });

        //addition of tracking for omniture;
        $('fileupload').addEvent('click', function(event){
            s.tl(this,'o','Choose A Video To Upload');
        }.bind(this));

        $('lemans2012-submit').addEvent('click', function(event){
            s.tl(this,'o','Submit');
        }.bind(this));

        //----------------------------------
        //
        // Validation
        //
        //----------------------------------

        // Dynamic validation

        $$('#lemans2012-details input').addEvent('blur', function(event){
            var name = event.target['name'], valid, el;
            if (name && name != undefined) {
                if (['fullname', 'phonenumber', 'emailaddress', 'captcha-answer'].contains(name)){
                    el = $$('#lemans2012-details-' + name)[0];
                    valid = this.validateField(this.fields[name], el);
                }
            }
        }.bind(this));

        $$('#lemans2012-details input').addEvent('change', function(event){
            var name = event.target['name'], valid, el;
            if (name && name != undefined) {
                if (['termsagree','fullname', 'phonenumber', 'emailaddress', 'captcha-answer'].contains(name)){
                    el = $$('#lemans2012-details-' + name)[0];
                    valid = this.validateField(this.fields[name], el);
                }
            }
        }.bind(this));


        //----------------------------------
        //
        // File button hack
        //
        //----------------------------------

		this.stylizeFileUploadField();
    },

	stylizeFileUploadField: function(){
        SI_CBW.FileInputStyler.stylizeById('fileupload'); // @see /ui-templates/etc/designs/audi/js/alib/si.files.js
	},

    validateField: function(field, element, form) {
        var valid = false,
            validator = new FormValidator(form || 'lemans2012Form', [], function(){});

        validator.registerCallback('checkinvalidchars', function(value){
            var invalidCharsRegex = /^.*[\"£\$%\^&\*\(\)\!@;:\?\<\>0-9]+.*$/;
            return !invalidCharsRegex.test(value);
        });

        validator.registerCallback('checkphonenumber', function(value){
            var numericSpacesRegex = /^[0-9\s]+$/;
            return numericSpacesRegex.test(value);
        });

        validator.validateField(field);
        valid = !validator.errors.length;
        this.toggleError(valid, element);
        validator = null;
        return valid;
    },

    toggleError: function(value, element) {
        if (value) {
            element.removeClass('lemans-error');
            element.addClass('lemans-valid');
        } else {
            element.addClass('lemans-error');
            element.removeClass('lemans-valid');
        }
    },

    validateDetails: function(){
        var validations = [],
            names = ['fullname', 'phonenumber', 'emailaddress', 'termsagree'],
            i,
            el = null,
            name = null;

        for (i = 0; i < names.length; i++) {
            name = names[i];
            el = $$('#lemans2012-details-' + name)[0];
            validations.push(this.validateField(this.fields[name], el));
        }

        return !validations.contains(false);
    },

    nextStage: function () {
        var next = null;
        switch (this.currentStage) {
            case this.detailsStage:
                if (this.validateDetails()){
                    if (this.skipToConfirmStage) {
                        next = this.confirmStage;
                        this.skipToConfirmStage = false;
                    } else {
                        next = this.fileSelectionStage;
                    }
                    this.changeStage(next);
                }
                break;
            case this.fileSelectionStage:
                next = this.confirmStage;
                this.changeStage(next);
                break;
            case this.confirmStage:
                next = this.uploadStage;
                this.changeStage(next);
                break;
            case this.uploadStage:
                next = this.thankYouStage;
                this.changeStage(next);
                break;
            case this.thankYouStage:
            default:
                this.overlay.close();
        }
    },

    createStages: function() {
        var stage;
        $$('.form-stage').each(function(s){
            stage = new RF.Class.Lemans2012FormStage({
                id: s.id
            });
            this.stages[s.id] = stage;
            stage.hide();
        }.bind(this));

        this.changeStage(this.detailsStage);
    },

    changeStage: function(stageId) {
        if (this.currentStage) {
            this.cleanupStage(this.currentStage);
            this.stages[this.currentStage].hide();
        }
        this.currentStage = stageId;
        this.prepareStage(this.currentStage);
        this.stages[this.currentStage].show();
    },

    cleanupStage: function(stageId) {
        switch(stageId) {
            case this.confirmStage:
                this.destroyCaptcha();
                break;
            case this.uploadStage:
                this.clearProgressPolling();
                break;
            default:
        }
    },

    getFilename: function (field) {
        var filename = '', splitter, rgx = /\/+/i;
        if (field.value) {
            splitter = rgx.test(field.value) ? '/' : '\\';
            filename = field.value.split(splitter).pop();
        }
        return filename;
    },

    prepareStage: function(stageId){
        var nameElement, phonenumberElement, emailaddressElement, fileuploadElement, captchaParentElement,
            nameField, phonenumberField, emailaddressField, fileuploadField, captchaField,
            continueButton, fileUploadBox, fileFakePath;

        fileuploadField = $$('#lemans2012Form input[name=fileupload]')[0];

        switch(stageId) {
            case this.fileSelectionStage:
                continueButton = $$('#lemans2012Form #lemans2012-file-selection .continue')[0];
                fileUploadBox = $$('#lemans2012-file-selection .box')[0];

                if (fileuploadField.value!==''){
                    continueButton.fade('show');
                }else{
                    continueButton.fade('hide');
                }

                if (this.uploadResponse && (this.uploadResponse === 'invalid')){
                    this.toggleError(false, fileUploadBox);
                    continueButton.fade('hide');
                    fileuploadField.value = '';
                    this.uploadResponse = null;
                    this.getiFrameDoc().document.body.innerHTML = '';
                }
                else {
                    this.toggleError(true, fileUploadBox);
                }

                break;

            case this.confirmStage:
                this.enableCaptcha();

                nameElement = $$('#lemans2012-confirm .fullname')[0];
                phonenumberElement = $$('#lemans2012-confirm .phonenumber')[0];
                emailaddressElement = $$('#lemans2012-confirm .emailaddress')[0];
                fileuploadElement = $$('#lemans2012-confirm .fileupload')[0];

                nameField = $$('#lemans2012Form input[name=fullname]')[0];
                phonenumberField = $$('#lemans2012Form input[name=phonenumber]')[0];
                emailaddressField = $$('#lemans2012Form input[name=emailaddress]')[0];
                captchaField = this.captchaField();

                nameElement.set('text', nameField.value);
                phonenumberElement.set('text', phonenumberField.value);
                emailaddressElement.set('text', emailaddressField.value);
                fileuploadElement.set('text', this.getFilename(fileuploadField));

                captchaField.set('value', '')

                this.createCaptcha();

                if (this.uploadResponse && (this.uploadResponse === 'mismatching_captcha')){
                    this.toggleError(
                        false, // means has an error :P
                        $$('#lemans2012-confirm .captcha-container')[0]
                    );
                }
                break;
            case this.uploadStage:
                fileuploadElement = $$('#lemans2012-upload .fileupload')[0];
                fileuploadElement.set('text', this.getFilename(fileuploadField));
                this.blockPolling = false;
                this.pollResponse();

                if (RF.Browser.ie) {
                    this.setUploadProgress(100);
                    if (RF.Browser.ie8) {
                        this.setUploadProgress(1);
                        this.pollProgress();
                    }
                } else {
                    this.pollProgress();
                }

                break;
            default:
        }

    },

    captchaField: function (){
        return $$('input[name=captcha-answer]')[0];
    },

    clearCaptcha: function() {
        this.captchaField().set('value', '');
    },

    getCaptcha: function() {
        return this.captchaField().get('value');
    },

    getiFrameDoc: function(){
        var iFrame, iFrameDoc;
        iFrame = $('lemans2012-form-target');
        iFrameDoc = iFrame['contentWindow'] || iFrame['contentDocuement'];
        return iFrameDoc
    },

    getHandlerURL: function() {
        return '/lemans/videoFormHandler'
    },

    setUploadProgress: function (value) {
        var w = (value / 100 ) * 350, sel = '#lemans2012FormContainer #lemans2012-upload .progress .animation';
        $($$(sel)[0]).setStyle('width', w + 'px');
    },

    pollProgress: function () {
        var timeInterval = 100, r;

        if (this.blockPolling) {
            return;
        }

        r = new Request({
            method:'get',
            url: this.getHandlerURL() + '?progress=true',

            onSuccess: function (response) {
                try {
                    response = this.parseJSON(response);

                    if (response.upload_progress) {
                        this.setUploadProgress(parseInt(response.upload_progress));
                    }

                    this.progressTimeoutId = setTimeout(
                        function(){
                            this.pollProgress();
                        }.bind(this),
                        timeInterval
                    );
                }
                catch (error){

                }
            }.bind(this),

            onFailure: function (response) {

            }.bind(this)
        });

        r.send();
    },

    pollResponse: function(){
        var response = null, isPre = false, parse,
        iFrameDoc = this.getiFrameDoc();
        response = iFrameDoc.document.body;

        if (response) {

            if (response.getElementsByTagName('pre')[0]) {
                response = response.getElementsByTagName('pre')[0];
                isPre = true;
            } else { // IE will return the response directly in the body
                response = response.innerHTML;
            }

            if (response) {
                try {
                    if (isPre) {
                        response = response.innerHTML;
                    }
                    response = this.parseJSON(response);
                    this.handleResponse(response.result);
                } catch (e) {
                    // something failed parsing the response, just start over
                    // this.changeStage(this.detailsStage);
                }
            } else {
                // ACOUK-7716 : There seems to be a race condition in FF when the
                // time it gets to send a file and then receive the response from
                // the backend, render the iframe, matches with the execution
                // of the polling and the interval isn't called again. Randomly
                // setting the interval time allows to reduce the changes that
                // the same file will fail twice.
                var i = 1000 + (Math.random() * 2000);
                this.timeoutId = setTimeout(
                    function(){
                        this.pollResponse();
                    }.bind(this),
                    i
                );
            }
        }
    },

    handleResponse: function(response){
        var next;
        this.uploadResponse = response;

        switch (response) {
            case 'success':
                $$('#' + this.uploadStage + ' .progress')[0].addClass('complete');
                next = this.thankYouStage;
                this.clearProgressPolling();
                this.blockPolling = true;
                break;
            case 'mismatching_captcha':
                next = this.confirmStage;
                break;
            case 'invalid':
                next = this.fileSelectionStage;
                this.clearProgressPolling();
                this.blockPolling = true;
                break;
            default:
                next = this.detailsStage;
        }

        setTimeout(function(){
            this.changeStage(next);
        }.bind(this), 1000);
    },

    clearProgressPolling: function () {
        if (this.progressTimeoutId) {
            clearTimeout(this.progressTimeoutId);
            this.progressTimeoutId = null;
        }
    },

    clearUploadPolling: function () {
        if (this.timeoutId) {
            clearTimeout(this.timeoutId);
            this.timeoutId = null;
        }
    },

    disposeForm: function(){
        var form = $(this.formId+'-wrapper');
        form.destroy();

        this.clearProgressPolling();
        this.clearUploadPolling();
        this.currentStage = null;
        this.uploadResponse = null;
    },

    cta_clickHandler: function() {
        this.overlay.open();
    },

    sendCaptcha: function(){
        var data, response;

        if (this.validateCaptcha()) {

            data = {'captcha-answer': this.getCaptcha()};

            new Request({
                method:'get',
                url:this.getHandlerURL(),
                data: data,

                onSuccess: function(response) {
                    try {
                        response = this.parseJSON(response);
                        switch (response['result']) {
                            case 'captcha_accepted':
                                this.enableSubmit();
                                break;
                            case 'mismatching_captcha':
                            default:
                                this.resetCaptcha();
                        }
                    }
                    catch (error){
                        this.resetCaptcha();
                    }
                }.bind(this),

                onFailure: function(response) {
                    this.resetCaptcha();
                }.bind(this)

            }).send();
        }
    },

    enableSubmit: function(){
        $$('#lemans2012FormContainer #lemans2012-confirm.form-stage h4')[0].addClass('submit');
        $($$('input[type=submit]')[0]).setStyle('display', 'block');
        $($$('.captcha-container')[0]).setStyle('display', 'none');
    },

    enableCaptcha: function () {
        $$('#lemans2012FormContainer #lemans2012-confirm.form-stage h4')[0].removeClass('submit');
        $($$('input[type=submit]')[0]).setStyle('display', 'none');
        $($$('.captcha-container')[0]).setStyle('display', 'block');
    },

    validateCaptcha: function() {
        return this.validateField(
            this.fields['captcha-answer'],
            $$('#lemans2012-confirm .captcha-container')[0]
        );
    },

    resetCaptcha: function(){
        this.destroyCaptcha();
        this.createCaptcha();
        this.clearCaptcha();
        this.validateCaptcha();
    },

    createCaptcha: function() {
        var captcha = new Element('img', {
                    src: '/lemans/captcha?boost=' + new Date().getTime()
                });
        $$('#lemans2012-confirm .captcha-container')[0].grab(captcha, 'top');
    },

    destroyCaptcha: function() {
        var captchaElement = $$('#lemans2012-confirm .captcha-container img')[0];
        if (captchaElement){
            captchaElement.destroy();
        }
    },

    parseJSON: function(value) {
        var parse = JSON['parse'] || JSON['decode']; // IE again
        return response = parse(value);
    }
});

window.addEvent('domready', function(){
    var selectors = [
        // Selectors required for this component to work
        'template-lemans2012Form',
        'enterNow',

        // ID selectors using on runtime
        'lemans2012overlay',
        'lemans2012Form',
    ];

    if (document.id(selectors[0]) && document.id(selectors[0])){

        $$('#lemans2012 .no-js')[0].setStyle('display', 'none');

        if (RF.Browser.iPad) {
            $$('#lemans2012 .i-pad')[0].setStyle('display', 'block');
        }
        else {
            $('enterNow').setStyle('display', 'block');
            RF.Init.Lemans2012 = new RF.Class.Lemans2012Form({
                selectors: selectors
            });
        }
    }

});
