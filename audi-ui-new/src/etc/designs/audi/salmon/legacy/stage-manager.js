(function($){

/*	Project: Audi.co.uk

* 	Name: StageManager Class
* 	Version: v1.0
* 	Author: Razorfish London
*/
RF.Class.StageManager = new Class({
	Implements: [Options,RF.Class.Utilities],
	Binds:['successCallback','failureCallback'],
	options:{
		mainStage:'main-stage',
		requestURL:null,
		model:null,
		stageFormat:null,
		callBack:false,
		showGallery:false
	},
	initialize: function(options){
		try {
			this.setOptions(options);
			this.storeBackground();
			if($defined(this.options.requestURL)) {
				this.requestNewEvents();
			} else {
				this.scrollToTopAndRunFunction.pass(this.stageTransitionsAndCallback,this)();
			}
		} catch(e) {
			this.ErrorHelper({method:"RF.Class.StageManager : initialize()",error:e});
		}
	},
	requestNewEvents: function() {
		new Request.JSON({
			url:this.options.requestURL, 
			method:'GET',
			onSuccess:this.successCallback,
			onFailure:this.failureCallback
		}).send();
	},
	successCallback: function(jsonObj) {
		this.jsonObj = jsonObj;
		$each($H(this.jsonObj).get(this.options.model), function(eventValue, eventKey) { // for each 'Event' object
			if($defined(eventValue) && eventValue.EventType != "") { // if there is an event assigned, e.g. 'click'
				$$('.'+eventValue.EventTrigger).each(function(triggerElement) {
					triggerElement.store(eventKey,eventValue);
					triggerElement.addEvents({
						'click':function(event) {
							if(event) {
								event.stop(); 
								JStoSWF_playInteractiveFeature('audishowroom',triggerElement.retrieve(eventKey).EventData);
							}
						},
						'keydown':function(event) {
							if(event && event.keyCode == 13) {
								event.stop(); 
								JStoSWF_playInteractiveFeature('audishowroom',triggerElement.retrieve(eventKey).EventData);
							}
						}
					});
				},this);
			}
		},this);
		delete this.jsonObj;
	},
	failureCallback: function(e) {
		$defined(e.message) ? e = e : e.message="JSON request failure";
		this.ErrorHelper({method:"RF.Class.StageManager : failureCallback()",error:e});
	},
	stageTransitionsAndCallback: function() {
		try {
			if(this.options.stageFormat) {
				document.id('navigation-panels').set('morph', {duration:500, transition:'sine:out'}).morph('.RF_NavigationPanels_'+this.options.stageFormat);
				if(!RF.Browser.ie) document.id('audishowroom').set('morph', {duration:500, transition:'sine:out'}).morph('.RF_FlashStage_'+this.options.stageFormat);
				document.id('flash-stage').set('morph', {duration:500, transition:'sine:out', onComplete: function() {
					if(this.options.callBack) JStoSWF_resizeComplete('audishowroom',true);
				}.bind(this)}).morph('.RF_FlashStage_'+this.options.stageFormat);
				(function() {
					document.id('flash-gallery-links').set('morph', {duration:500, transition:'sine:out', 
						onStart: function() {
							if(this.options.showGallery) document.id('flash-gallery-links').removeClass('RF_OffsetStage');
						}.bind(this),
						onComplete: function() {
							if(!this.options.showGallery) document.id('flash-gallery-links').addClass('RF_OffsetStage');
						}.bind(this)
					}).morph(this.options.showGallery ? ".RF_FadeIn" : ".RF_FadeOut");
				}.bind(this)).delay(this.options.showGallery ? 2000 : 550);
			
				this.options.stageFormat == "Reset" ? this.setShowroomBackground(true) : this.setShowroomBackground(false);
			}
		} catch(e) {
			this.ErrorHelper({method:"RF.Class.StageManager : stageTransitionsAndCallback()",error:e});
			if(this.options.callBack) JStoSWF_resizeComplete('audishowroom',false);
		}
	},
	setShowroomBackground: function(blnSetBackground) {
		/*
		blnSetBackground ? (function() {document.getElement('body').addClass('main-stage-background')}).delay(2000) : document.getElement('body').removeClass('main-stage-background');
		*/
		/*
		*	ACOUK-4236: the 'main-stage-background' class no longer provides the background
		*	We have to set or remove a STYLE Element in the HEAD
		*/
		try {
			if(blnSetBackground && document.getElement('body').retrieve("backgroundStyleElement") != null) {
				(function() {
					document.getElement('head').adopt(document.getElement('body').retrieve("backgroundStyleElement"))
				}).delay(2000);
			} else {
				if(document.getElement('head').getElement('style#background')) {
					document.getElement('head').getElement('style#background').dispose();
				}
			}
		} catch(e) {
			this.ErrorHelper({method:"RF.Class.StageManager : setShowroomBackground()", error:e});
		}
	},
	storeBackground: function() {
		try {
			if(document.getElement('style#background')) {
				document.getElement('body').store("backgroundStyleElement", document.getElement('style#background'));
			}
		} catch(e) {
			this.ErrorHelper({method:"RF.Class.StageManager : storeBackground()", error:e});
		}		
	},
	setShowroomSpecificBackground: function(objData) {
		/*
		*	@arguments:
		*		objData [Object]
		*		objData.range: Range name [String]
		*		objData.model: Model name [String]
		*/
		if(objData && objData.range && objData.range != "") {
			if(document.id(this.options.mainStage)) {
				document.id(this.options.mainStage).set({
					'styles':{
						'background':'url(/content/dam/audi/static/img/bgs/'+objData.range+'/showroom-background.jpg) no-repeat top center'
					}
				});
				/*
				*	Remove the STYLE element for the Page/Showroom Background, as this still has a BODY background-color statement
				*/
				this.setShowroomBackground(false);
			}
		}
	}
});

})(document.id);
