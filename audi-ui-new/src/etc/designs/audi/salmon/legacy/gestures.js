(function($){

/*	Project: Audi.co.uk
* 	Name: Gestures Class
* 	Version: v1.0
* 	Author: Stuart Thorne, Razorfish London
*/
RF.Class.Gestures = new Class({
	iPhoneDefinitions:[
		'touchstart',
		'touchmove',
		'touchend',
		'gesturestart',
		'gesturechange',
		'gestureend',
		'webkitAnimationStart',
		'webkitAnimationEnd',
		'webkitAnimationIteration',
		'webkitTransitionEnd'
	],
	iPhoneTouches:[
		'touchstart',
		'touchmove',
		'touchend'
	],
	implementTouchAndGestureEvents: function() {
		this.touchevents = {
			touchDo: function(e,type) {
				this.fireEvent(type,e);
			}
		}
		Element.implement(this.touchevents);

		$each(this.iPhoneDefinitions, function(item) {
			Element.Events[item] = {
				onAdd: function() {
					this.addEventListener(item, function(e) {
						this.touchDo(e,item);
					},false)
				},
				onRemove: function() {
					this.removeEventListener(item, function(e) {},false)
				}
			}
		}) 

		$extend(document,this.touchevents);
		document.Events=[];

		$each(this.iPhoneTouches, function(item) {
			document.Events[item] = {
				onAdd: function() {
					this.addEventListener(item, function(e) {
						document.touchDo(e,item);
					},false)
				},
				onRemove: function() {
					this.removeEventListener(item, function(e) {},false)
				}
			}
		});
	}
});

})(document.id);
