(function($){

/*

*	@ClassName: RF_TrackingHound
*	@Author: Stuart Thorne
*	@Company: Razorfish UK
* 	@Version: 1.1
*	@Date: 2009-10-22
*	@Description: Class checks to see whether any elements exist with the vendor CSS class names, and then processes them
*/
RF.Class.TrackingHound = new Class({
	Implements:[Options,RF.Class.Utilities],
	options:{
		elements:null,
		singleAction:null
	},
	initialize: function(options) {
		try {
			this.setOptions(options);
			if($defined(this.options.singleAction)) {
				if(this.options.singleAction.contains('EyeBlaster')) this.eyeBlasterNewImpression.pass(this.options.singleAction,this)();
				if(this.options.singleAction.contains('Atlas')) this.atlasNewImpression.pass(this.options.singleAction,this)();
			}
			if($defined(this.options.elements)) {
				$$(this.options.elements).each(function(element) {
					element.getProperty('class').split(' ').each(function(cssClassName) {
						if(cssClassName.contains('EyeBlaster')) this.addEvents.pass([element,this.eyeBlasterNewImpression,cssClassName],this)();
						if(cssClassName.contains('Atlas')) this.addEvents.pass([element,this.atlasNewImpression,cssClassName],this)();
						if(cssClassName.contains('s3_')) this.addEvents.pass([element,this.sophusNewImpression,cssClassName],this)();
					},this);
				},this);
			}
		} catch(e) {
			this.ErrorHelper({method:"RF.Class.TrackingHound : initialize()",error:e});
		}
	},
	addEvents: function() {
		var args=arguments; // leon.coto@razorfish.com
							// using this.args causes the this.args variable to be overwritten over and over again,
							// while setting var args constrains the variable to a closure.
		args[0].addEvents({
			'click':function(event){if(event) args[1].pass([args[2],args[0].get('href')])()}.bind(this),
			'keydown':function(event){if(event && event.keyCode == 13) args[1].pass([args[2],args[0].get('href')])()}.bind(this)
		});
	},
	eyeBlasterNewImpression: function() {
		// arguments[0] example = 'EyeBlaster_12345'
		var ebRand = Math.random()+'';
		ebRand = ebRand * 1000000;
		new Element('img', {
			src:window.location.protocol+'//bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&amp;ActivityID='+arguments[0].substring(arguments[0].indexOf('_')+1)+'&amp;ns=1'+ebRand,
			id:arguments[0],
			alt:'',
			width:1,
			height:1
		}).inject(document.body,'bottom');
	},
	atlasNewImpression: function() {
		// arguments[0] example = 'Atlas_9lmadb_PDFAudiTTRS2010_1'
		this.atlasTagReference = arguments[0].substring(arguments[0].indexOf('_')+1);
		/* If the tag reference already includes the '9l' prefix, do not include it in the Script src */
		if((/9l/.test(this.atlasTagReference)) == false) {
			this.atlasTagReference = '9lmadb_'+this.atlasTagReference;
		}
		new Element('script', {
			id: arguments[0],
			src: window.location.protocol+'//view.atdmt.com/jaction/'+this.atlasTagReference
		}).inject(document.body,'bottom');
	},
    sophusNewImpression: function() {
        if (arguments[0] == 's3_log') {
            s3_log(arguments[1]);
        }
    }
});

})(document.id);
