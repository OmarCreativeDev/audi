RF.Moo.idE = function (a, b) {
    return (document.id(b) || document).getElement(a)
};
RF.Moo.idES = function (a, b) {
    return (document.id(b) || document).getElements(a)
}; 
RF.Global.CorrectSafariTabbing = function () {
    if (document.id("skip-nav")) {
        document.id("skip-nav").addEvents({
            focus: function () {
                document.id("skip-nav").setStyle("left", "0")
            },
            blur: function () {
                document.id("skip-nav").setStyle("left", "-10000px")
            },
            click: function () {
                if ((navigator.appVersion).match(/safari/i)) {
                    RF.Init.hrf = document.id("skip-nav").getProperty("href");
                    if (RF.Init.hrf.indexOf("#") == 0) {
                        RF.Init.hrf = RF.Init.hrf.substring(1, RF.Init.hrf.length)
                    }
                    if (!document.id("faux-target")) {
                        RF.Init.a = new Element("a").setStyles({
                            position: "absolute",
                            left: "-10000px"
                        }).set("html", "").setProperties({
                            id: "faux-target",
                            title: "Entered main content of page",
                            href: ""
                        }).injectTop(document.id(RF.Init.hrf))
                    }
                    document.id("faux-target").focus()
                }
            }
        })
    }
    if ((navigator.appVersion).match(/safari/i)) {
        $$(".target-top").each(function (a) {
            a.addEvent("click", function () {
                if (!document.id("faux-target-top")) {
                    RF.Init.b = new Element("a").setStyles({
                        position: "absolute",
                        left: "-10000px"
                    }).set("html", "").setProperties({
                        id: "faux-target-top",
                        title: "Top of the page",
                        href: ""
                    }).injectTop(document.id("top"))
                }
                document.id("faux-target-top").focus()
            })
        })
    }
};
RF.Global.PngFix = function () {
    RF.Init.allPNGs = $$("img[src$=.png]");
    RF.Init.filtered = RF.Init.allPNGs.filter(function (b, a) {
        if (b.getParents("#navigation-panels").length <= 0) {
            return b
        }
    });
    RF.Init.filtered.each(function (a) {
        if (a.hasClass("png") != true) {
            a.addClass("png")
        }
    })
};
RF.Global.ReinstateScriptsAfterAjaxCall = function () {
    RF.Global.PngFix();
    new RF.Class.TargetBlankReplace()
};
RF.Global.Facebook = function () {
    if (window.FB == undefined) {
        return
    }
    FB.Event.subscribe("edge.create", function (a) {
        RF.Init.OmnitureNavPanelSingleAction = new RF.Class.OmnitureTrackingManager({
            singleAction: {
                eVars: "eVar46",
                props: "prop52",
                events: "event41",
                callToAction: "Facebook:Add Like"
            }
        })
    });
    FB.Event.subscribe("edge.remove", function (a) {
        RF.Init.OmnitureNavPanelSingleAction = new RF.Class.OmnitureTrackingManager({
            singleAction: {
                eVars: "eVar46",
                props: "prop52",
                events: "event41",
                callToAction: "Facebook:Remove Like"
            }
        })
    });
    FB.Event.subscribe("comment.create", function (a) {
        RF.Init.OmnitureNavPanelSingleAction = new RF.Class.OmnitureTrackingManager({
            singleAction: {
                eVars: "eVar46",
                props: "prop52",
                events: "event41",
                callToAction: "Facebook:Add Comment"
            }
        })
    });
    FB.Event.subscribe("comment.remove", function (a) {
        RF.Init.OmnitureNavPanelSingleAction = new RF.Class.OmnitureTrackingManager({
            singleAction: {
                eVars: "eVar46",
                props: "prop52",
                events: "event41",
                callToAction: "Facebook:Remove Comment"
            }
        })
    })
};
window.addEvents({
    domready: function () {
        RF.Global.CorrectSafariTabbing();
        RF.Global.PngFix();
        RF.Init.TargetBlankReplace = new RF.Class.TargetBlankReplace();
        RF.Init.TrackingHound = new RF.Class.TrackingHound({
            elements: "*[class*=Atlas],a[class*=EyeBlaster],a[class*=s3_]"
        })
    },
    load: function () {
        RF.Global.Facebook()
    }
});
String.prototype.contains = function (a, b) {
    return (b) ? (b + this + b).indexOf(b + a + b) > -1 : this.indexOf(a) > -1
};
window.enableAlphaFilter = false;
/*
if (@_jscript_version == 5.5 || @_jscript_version==5.6 ||
   (@_jscript_version==5.7 &&
      navigator.userAgent.toLowerCase().indexOf("msie 6.") != -1)) {
  //ie6 or IE5.5 code
  window.enableAlphaFilter = true;
}
*/
Element.Properties.opacity.set = function (d, c) {
    if (!c) {
        if (d == 0) {
            if (this.style.visibility != "hidden") {
                this.style.visibility = "hidden"
            }
        } else {
            if (this.style.visibility != "visible") {
                this.style.visibility = "visible"
            }
        }
    }
    if (!this.currentStyle || !this.currentStyle.hasLayout) {
        this.style.zoom = 1
    }
    if (window.enableAlphaFilter) {
        this.style.filter = (d == 1) ? "" : "alpha(opacity=" + d * 100 + ")"
    }
    this.style.opacity = d;
    this.store("opacity", d)
};

(function (n, o, d) {
    var g = {
        messages: {
            required: "The %s field is required.",
            matches: "The %s field does not match the %s field.",
            valid_email: "The %s field must contain a valid email address.",
            valid_emails: "The %s field must contain all valid email addresses.",
            min_length: "The %s field must be at least %s characters in length.",
            max_length: "The %s field must not exceed %s characters in length.",
            exact_length: "The %s field must be exactly %s characters in length.",
            greater_than: "The %s field must contain a number greater than %s.",
            less_than: "The %s field must contain a number less than %s.",
            alpha: "The %s field must only contain alphabetical characters.",
            alpha_numeric: "The %s field must only contain alpha-numeric characters.",
            alpha_dash: "The %s field must only contain alpha-numeric characters, underscores, and dashes.",
            numeric: "The %s field must contain only numbers.",
            integer: "The %s field must contain an integer.",
            decimal: "The %s field must contain a decimal number.",
            is_natural: "The %s field must contain only positive numbers.",
            is_natural_no_zero: "The %s field must contain a number greater than zero.",
            valid_ip: "The %s field must contain a valid IP.",
            valid_base64: "The %s field must contain a base64 string."
        },
        callback: function (t) {}
    };
    var l = /^(.+)\[(.+)\]$/,
        k = /^[0-9]+$/,
        h = /^\-?[0-9]+$/,
        a = /^\-?[0-9]*\.?[0-9]+$/,
        q = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,6}$/i,
        c = /^[a-z]+$/i,
        i = /^[a-z0-9]+$/i,
        f = /^[a-z0-9_-]+$/i,
        b = /^[0-9]+$/i,
        r = /^[1-9][0-9]*$/i,
        p = /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})$/i,
        m = /[^a-zA-Z0-9\/\+=]/i;
    var j = function (v, t, y) {
        this.callback = y || g.callback;
        this.errors = [];
        this.fields = {};
        this.form = o.forms[v] || {};
        this.messages = {};
        this.handlers = {};
        for (var u = 0, x = t.length; u < x; u++) {
            var w = t[u];
            if (!w.name || !w.rules) {
                continue
            }
            this.fields[w.name] = {
                name: w.name,
                display: w.display || w.name,
                rules: w.rules,
                type: null,
                value: null,
                checked: null
            }
        }
        this.form.onsubmit = (function (z) {
            return function (A) {
                try {
                    return z._validateForm(A)
                } catch (B) {}
            }
        })(this)
    };
    j.prototype.setMessage = function (u, t) {
        this.messages[u] = t;
        return this
    };
    j.prototype.registerCallback = function (t, u) {
        if (t && typeof t === "string" && u && typeof u === "function") {
            this.handlers[t] = u
        }
        return this
    };
    j.prototype._validateForm = function (u) {
        this.errors = [];
        for (var t in this.fields) {
            if (this.fields.hasOwnProperty(t)) {
                var v = this.fields[t] || {};
                this._validateField(v)
            }
        }
        if (typeof this.callback === "function") {
            this.callback(this.errors, u)
        }
        if (this.errors.length > 0) {
            if (u && u.preventDefault) {
                u.preventDefault()
            } else {
                return false
            }
        }
        return true
    };
    j.prototype.validateField = function (t) {
        this.errros = [];
        this._validateField(t);
        return this
    };
    j.prototype._validateField = function (A) {
        var B = A.rules.split("|"),
            x = this.form[A.name];
        if (x && x != d) {
            A.type = x.type;
            A.value = x.value;
            A.checked = x.checked
        }
        if (A.rules.indexOf("required") === -1 && (!A.value || A.value === "" || A.value === d)) {
            return
        }
        for (var y = 0, v = B.length; y < v; y++) {
            var u = B[y],
                w = null,
                z = false;
            if (parts = l.exec(u)) {
                u = parts[1];
                w = parts[2]
            }
            if (typeof this._hooks[u] === "function") {
                if (!this._hooks[u].apply(this, [A, w])) {
                    z = true
                }
            } else {
                if (u.substring(0, 9) === "callback_") {
                    u = u.substring(9, u.length);
                    if (typeof this.handlers[u] === "function") {
                        if (this.handlers[u].apply(this, [A.value]) === false) {
                            z = true
                        }
                    }
                }
            } if (z) {
                var t = this.messages[u] || g.messages[u];
                if (t) {
                    var C = t.replace("%s", A.display);
                    if (w) {
                        C = C.replace("%s", (this.fields[w]) ? this.fields[w].display : w)
                    }
                    this.errors.push(C)
                } else {
                    this.errors.push("An error has occurred with the " + A.display + " field.")
                }
                break
            }
        }
    };
    j.prototype._hooks = {
        required: function (u) {
            var t = u.value;
            if (u.type === "checkbox") {
                return (u.checked === true)
            }
            return (t !== null && t !== "")
        },
        matches: function (u, t) {
            if (el = this.form[t]) {
                return u.value === el.value
            }
            return false
        },
        valid_email: function (t) {
            return q.test(t.value)
        },
        valid_emails: function (v) {
            var t = v.value.split(",");
            for (var u = 0; u < t.length; u++) {
                if (!q.test(t[u])) {
                    return false
                }
            }
            return true
        },
        min_length: function (u, t) {
            if (!k.test(t)) {
                return false
            }
            return (u.value.length >= t)
        },
        max_length: function (u, t) {
            if (!k.test(t)) {
                return false
            }
            return (u.value.length <= t)
        },
        exact_length: function (u, t) {
            if (!k.test(t)) {
                return false
            }
            return (u.value.length === t)
        },
        greater_than: function (t, u) {
            if (!a.test(t.value)) {
                return false
            }
            return (parseFloat(t.value) > parseFloat(u))
        },
        less_than: function (t, u) {
            if (!a.test(t.value)) {
                return false
            }
            return (parseFloat(t.value) < parseFloat(u))
        },
        alpha: function (t) {
            return (c.test(t.value))
        },
        alpha_numeric: function (t) {
            return (i.test(t.value))
        },
        alpha_dash: function (t) {
            return (f.test(t.value))
        },
        numeric: function (t) {
            return (a.test(t.value))
        },
        integer: function (t) {
            return (h.test(t.value))
        },
        decimal: function (t) {
            return (a.test(t.value))
        },
        is_natural: function (t) {
            return (b.test(t.value))
        },
        is_natural_no_zero: function (t) {
            return (r.test(t.value))
        },
        valid_ip: function (t) {
            return (p.test(t.value))
        },
        valid_base64: function (t) {
            return (m.test(t.value))
        }
    };
    n.FormValidator = j
})(window, document);


 SI_CBW.FileInputStyler = {
    htmlClass: "SI-FILES-STYLIZED",
    fileInputFieldClass: "fileInputField",
    fileInputWrapperClass: "fileInputFieldWrapper",
    initialised: false,
    fileInputCanBeStyled: false,
    fileInputField: null,
    fileInputFieldWrapper: null,
    fileInputFieldFakeButton: null,
    init: function () {
        this.initialised = true;
        var b = 0;
        if (window.opera || (b && b < 5.5) || !document.getElementsByTagName) {
            return
        }
        this.fileInputCanBeStyled = true;
        var a = document.getElementsByTagName("html")[0];
        a.className += (a.className != "" ? " " : "") + this.htmlClass
    },
    stylize: function (b) {
        this.fileInputField = b;
        this.fileInputFieldWrapper = this.fileInputField.parentNode;
        this.fileInputFieldFakeButton = this.fileInputFieldWrapper.parentNode;
        var a = this;
        this.fileInputFieldFakeButton.onmousemove = function (j) {
            if (typeof j == "undefined") {
                j = window.event
            }
            if (typeof j.pageY == "undefined" && typeof j.clientX == "number" && document.documentElement) {
                j.pageX = j.clientX + document.documentElement.scrollLeft;
                j.pageY = j.clientY + document.documentElement.scrollTop
            }
            var f = oy = 0;
            var i = this;
            if (i.offsetParent) {
                f = i.offsetLeft;
                oy = i.offsetTop;
                while (i = i.offsetParent) {
                    f += i.offsetLeft;
                    oy += i.offsetTop
                }
            }
            var c = j.pageX - f;
            var k = j.pageY - oy;
            var d = a.fileInputField.offsetWidth;
            var g = a.fileInputField.offsetHeight;
            if (!(0 < c && c < 392) || !(0 < k && k < 60)) {
                return
            }
            if (RF.Browser.ie7 || RF.Browser.ie8) {
                a.fileInputFieldWrapper.style.clip = "rect(10px,220px,15px,160px)";
                a.fileInputFieldWrapper.style.top = (k - (g / 2)) + 95 + "px";
                a.fileInputFieldWrapper.style.left = (c - (d - 30)) + 50 + "px"
            } else {
                a.fileInputFieldWrapper.style.top = k - (g / 2) + 100 + "px";
                a.fileInputFieldWrapper.style.left = c - (d - 30) + 20 + "px"
            }
        }
    },
    stylizeById: function (a) {
        if (!this.initialised) {
            this.init()
        }
        if (!this.fileInputCanBeStyled) {
            return
        }
        this.stylize(document.getElementById(a))
    }
};
