(function($){

RF.Init.Utils = new RF.Class.Utilities();
RF.Tracking.ObjectOmnitureTracking = {
	eVar1: function() {
		/*
		*	Type: singleAction
		*	Description: SignUp for myAudi
		*/
		s.eVar1="Signup with Address/Signup no Address";
		RF.Init.Utils.log("s.eVar1: SignUp for myAudi: "+s.eVar1);
	},
	eVar2: function() {
		//
		//	Type: singleAction
		//	Description: Contact requests
		//
		s.eVar2=arguments[0];
		RF.Init.Utils.log("s.eVar2: Contact requests: "+s.eVar2);
	},
	eVar3: function() {
		/*
		*	Type: singleAction
		*	Description: Internal search term
		*/
		s.eVar3=(($defined(document.id('q')) && $defined(document.id('q').value) && document.id('q').value != "") ? document.id('q').value : "Search term unavailable");
		RF.Init.Utils.log("s.eVar3: Internal search term: "+s.eVar3);
	},
	eVar4: function() {
		/*
		*	Type: singleAction
		*	Description: Login to myAudi
		*/
		s.eVar4="Login myAudi";
		RF.Init.Utils.log("s.eVar4: Login to myAudi: "+s.eVar4);
	},
	eVar5: function() {
		/*
		*	Type: singleAction
		*	Description: Search result success (string value: "successful" : "not successful")
		*/
		s.eVar5=arguments[0];
		RF.Init.Utils.log("s.eVar5: Search result success: "+s.eVar5);
	},
	eVar6: function() {
		/*
		*	Type: singleAction
		*	Description: On successful ebrochure request
		*/
		s.eVar6="Ebrochure Request";
		RF.Init.Utils.log("s.eVar6: Ebrochure Request: "+s.eVar6);
	},
	eVar7: function() {
		s.eVar7="Newsletter Signup";
		RF.Init.Utils.log("s.eVar7: Newsletter Signup: "+s.eVar7);
		// On successful newsletter signup
		// include event6 newsletter signup
	},
	eVar8: function() {
		/*
		*	Type: singleAction
		*	Description: On successful request a quote submit
		*/
		s.eVar8="Quote Request";
		RF.Init.Utils.log("s.eVar8: Quote Request: "+s.eVar8);
	},
	eVar9: function() {
		/*
		*	Type: singleAction
		*	Description: When internal search is performed (on submit)
		*/
		s.eVar9="Internal Search Submit";
		RF.Init.Utils.log("s.eVar9: Internal Search Submit: "+s.eVar9);
	},
	eVar10: function() {
		s.eVar10="Trade in Appraisal Submission";
		RF.Init.Utils.log("s.eVar10: Trade in Appraisal Submission: "+s.eVar10);
		// On successful trade-in appraisal submission
		// inlude event13
	},
	eVar11: function() {
		s.eVar11="Send to a Friend";
		RF.Init.Utils.log("s.eVar11: Send to a Friend: "+s.eVar11);
		// On successful send to a friend
		// include event14
	},
	eVar12: function() {
		s.eVar12="Print";
		RF.Init.Utils.log("s.eVar12: Print: "+s.eVar12);
		// On print page open
		// include event15
	},
	eVar13: function() {
		s.eVar13="Monthly Payment Estimator";
		RF.Init.Utils.log("s.eVar13: Monthly Payment Estimator: "+s.eVar13);
		// On button click for monthly payment estimator
		// include event16
	},
	eVar14: function() {
		/*
		*	Type: singleAction
		*	Description: On dealer locator submission
		*/
		s.eVar14="Dealer Locator Submit";
		RF.Init.Utils.log("s.eVar13: Dealer Locator Submit: "+s.eVar13);
	},
	eVar15: function() {
		s.eVar15="Model Comparison Tool Click";
		RF.Init.Utils.log("s.eVar13: Model Comparison Tool Click: "+s.eVar13);
		// On button click for model comparison tool
		// include event18
	},
	eVar16: function() {
		s.eVar16=arguments[0];
		// ACC - Carline ID
	},
	eVar17: function() {
		s.eVar17=arguments[0];
		// ACC - Carline
	},
	eVar18: function() {
		s.eVar18=arguments[0];
		// ACC - Conflict
	},
	eVar19: function() {
		s.eVar19=arguments[0];
		// ACC - Engine Extension
	},
	eVar20: function() {
		s.eVar20=arguments[0];
		// ACC - Engine ID
	},
	eVar21: function() {
		s.eVar21=arguments[0];
		// ACC - Engine name
	},
	eVar22: function() {
		s.eVar22=arguments[0];
		// ACC - Exterior colour
	},
	eVar23: function() {
		s.eVar23=arguments[0];
		// ACC - Successful configuration
	},
	eVar24: function() {
		s.eVar24=arguments[0];
		// ACC - Instance
	},
	eVar25: function() {
		s.eVar25=arguments[0];
		// ACC - Interior colour
	},
	eVar26: function() {
		s.eVar26=arguments[0];
		// ACC - Model extension
	},
	eVar27: function() {
		s.eVar27=arguments[0];
		// ACC - Selected
	},
	eVar28: function() {
		s.eVar28=arguments[0];
		// A/B Testing
	},
	eVar29: function() {
		s.eVar29=arguments[0];
		// A/B Testing
	},
	eVar30: function() {
		/*
		*	Type: singleAction
		*	Description: User ID
		*/
		s.eVar30=arguments[0];
		RF.Init.Utils.log("s.eVar30: User ID: "+s.eVar30);
	},
	eVar31: function() {
		/*
		*	Type: singleAction
		*	Description: Video name, when video starts playing
		*/
		s.eVar31=arguments[0];
		RF.Init.Utils.log("s.eVar31: Video Name: "+s.eVar31);
	},
	eVar32: function() {
		/*
		*	Type: singleAction
		*	Description: Used with form plugin (unsure of purpose here for Phase 1 launch, so unlikely to be in use)
		*/
		s.eVar32=arguments[0];
	},
	eVar34: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Image Gallery: Image Gallery viewed, value RANGE MODEL e.g. "A1 Sportback Concept"
		*/
		s.eVar34=arguments[0];
		RF.Init.Utils.log("s.eVar34: Image Gallery viewed: "+s.eVar34);
	},
	eVar35: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Request a Test Drive (goes to Arrange a test drive webapp)
		*/
		s.eVar35=arguments[0];
		RF.Init.Utils.log("s.eVar35: Showroom - Request a Test Drive: "+s.eVar35);
	},
	eVar36: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Specifications selected
		*/
		s.eVar36=arguments[0];
		RF.Init.Utils.log("s.eVar36: Showroom - Specifications: "+s.eVar36);
	},
	eVar37: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Request a Brochure
		*/
		s.eVar37=arguments[0];
		RF.Init.Utils.log("s.eVar37: Showroom - Request a Brochure: "+s.eVar37);
	},
	eVar38: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Guided Tour : "RANGE MODEL"
		*/
		s.eVar38=arguments[0];
		RF.Init.Utils.log("s.eVar38: Showroom - Guided Tour: "+s.eVar38);
	},
	eVar39: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Switch Model
		*/
		s.eVar39=arguments[0];
		RF.Init.Utils.log("s.eVar39: Showroom - Switch Model: "+s.eVar39);
	},
	eVar40: function() {
		s.eVar40=arguments[0];
		RF.Init.Utils.log("s.eVar40: Showroom - View Full Range: "+s.eVar40);
		// eVar40: Flex SWF Showroom: View Full Range
	},
	eVar41: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Hotspot Started
		*/
		s.eVar41=arguments[0];
		RF.Init.Utils.log("s.eVar41: Showroom - Hotspot Started: "+s.eVar41);
	},
	eVar42: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF Showroom: Hotspot Completed
		*/
		s.eVar42=arguments[0];
		RF.Init.Utils.log("s.eVar42: Showroom - Hotspot Completed: "+s.eVar42);
	},
	eVar43: function() {
		/*
		*	Type: singleAction
		*	Description: Flex SWF A5 Cabriolet Campaign - Call To Action
		*/
		s.eVar43=arguments[0];
		RF.Init.Utils.log("s.eVar43: Flex SWF A5 Cabriolet Campaign - Call To Action: "+s.eVar43);
	},
	eVar44:function(){
	    s.eVar44=arguments[0];
	    RF.Init.Utils.log("s.eVar44: Factory Tour - Select Area: "+s.eVar44) 
	},
	eVar45:function(){
	    s.eVar45=arguments[0];
	    RF.Init.Utils.log("s.eVar45: Form Section - Car Model Type: "+s.eVar45) 
	},
	eVar46: function() {
		s.eVar46=arguments[0];
		RF.Init.Utils.log("s.eVar46: Social Media Event: "+s.eVar46);
	},

	event1: function() {
		/*
		*	Type: singleAction
		*	Description: Login to myAudi
		*/
		s.events="event1";
		RF.Init.Utils.log("s.events: Login to myAudi (event1): "+s.events);
	},
	event2: function() {
		/*
		*	Type: singleAction
		*	Description: SignUp my Audi with full address
		*/
		s.events="event2";
		RF.Init.Utils.log("s.events: SignUp my Audi with full address (event2): "+s.events);
	},
	event3: function() {
		/*
		*	Type: singleAction
		*	Description: SignUp my Audi with no address
		*/
		s.events="event3";
		RF.Init.Utils.log("s.events: SignUp my Audi with no address (event3): "+s.events);
	},
	event4: function() {
		/*
		*	Type: singleAction
		*	Description: Download of document
		*/
		s.events="event4";
		RF.Init.Utils.log("s.events: Download of document (event4): "+s.events);
	},
	event5: function() {
		/*
		*	Type: singleAction
		*	Description: EBrochure Request
		*/
		s.events="event5";
		RF.Init.Utils.log("s.events: Ebrochure Request (event5): "+s.events);
	},
	event6: function() {
		/*
		*	Type: singleAction
		*	Description: Newsletter Signup
		*/
		s.events="event6";
		RF.Init.Utils.log("s.events: Newsletter Signup (event6): "+s.events);
	},
	event7: function() {
		//
		//	Type: singleAction
		//	Description: ACC - Configuration started
		//
		s.events="event7";
		RF.Init.Utils.log("s.events: ACC - Configuration started (event7): "+s.events);
	},
	event8: function() {
		//
		//	Type: singleAction
		//	Description: ACC - Successful configuration
		//
		s.events="event8";
		RF.Init.Utils.log("s.events: ACC - Successful configuration (event8): "+s.events);
	},
	event9: function() {
		//
		//	Type: singleAction
		//	Description: ACC - Extras chosen
		//
		s.events="event9";
		RF.Init.Utils.log("s.events: ACC - Extras chosen (event9): "+s.events);
	},
	event10: function() {
		//
		//	Type: singleAction
		//	Description: ACC - Accessory chosen
		//
		s.events="event10";
		RF.Init.Utils.log("s.events: ACC - Accessory chosen (event10): "+s.events);
	},
	event11: function() {
		//
		//	Type: singleAction
		//	Description: Quote request
		//
		s.events="event11";
		RF.Init.Utils.log("s.events: Quote request (event11): "+s.events);
	},
	event12: function() {
		/*
		*	Type: singleAction
		*	Description: Internal search submit
		*/
		s.events="event12";
		RF.Init.Utils.log("s.events: Internal search submit (event12): "+s.events);
	},
	event13: function() {
		//
		//	Type: singleAction
		//	Description : Trade in Appraisal Submission
		//
		s.events="event13";
		RF.Init.Utils.log("s.events: Trade in Appraisal Submission (event13): "+s.events);
	},
	event14: function() {
		//
		//	Type: singleAction
		//	Description: Send to a friend
		//
		s.events="event14";
		RF.Init.Utils.log("s.events: Send to a friend (event14): "+s.events);
	},
	event15: function() {
		//
		//	Type: singleAction
		//	Description: Print
		//
		s.events="event15";
		RF.Init.Utils.log("s.events: Print (event15): "+s.events);
	},
	event16: function() {
		//
		//	Type: singleAction
		//	Description: Monthly Payment Estimator Submit
		//
		s.events="event16";
		RF.Init.Utils.log("s.events: Monthly Payment Estimator Submit (event16): "+s.events);
	},
	event17: function() {
		/*
		*	Type: singleAction
		*	Description: Dealer Locator Submit
		*/
		s.events="event17";
		RF.Init.Utils.log("s.events: Dealer Locator Submit (event17): "+s.events);
	},
	event18: function() {
		//
		//	Type: singleAction
		//	Description: Model Comparison Tool Selected
		//
		s.events="event18";
		RF.Init.Utils.log("s.events: Model Comparison Tool Selected (event18): "+s.events);
	},
	event19: function() {
		/*
		*	Type: singleAction
		*	Description: Video started
		*/
		s.events="event19";
		RF.Init.Utils.log("s.events: Video started (event19): "+s.events);
	},
	event20: function() {
		/*
		*	Type: singleAction
		*	Description: Video completed
		*/
		s.events="event20";
		RF.Init.Utils.log("s.events: Video completed (event20): "+s.events);
	},
	event21: function() {
		//
		//	Type: singleAction
		//	Description: Video share selected
		//
		s.events="event21";
		RF.Init.Utils.log("s.events: Video share (event21): "+s.events);
	},
	event22: function() {
		/*
		*	Type: singleAction
		*	Description: Video full screen selected
		*/
		s.events="event22";
		RF.Init.Utils.log("s.events: Video full screen (event22): "+s.events);
	},
	event23: function() {
		/*
		*	Type: singleAction
		*	Description: Video replay selected
		*/
		s.events="event23";
		RF.Init.Utils.log("s.events: Video replay (event23): "+s.events);
	},
	event24: function() {
		/*
		*	Type: singleAction
		*	Description: Form Abandon
		*/
		s.events="event24";
		RF.Init.Utils.log("s.events: Form Abandon (event24): "+s.events);
	},
	event25: function() {
		/*
		*	Type: singleAction
		*	Description: Form Success
		*/
		s.events="event25";
		RF.Init.Utils.log("s.events: Form Success (event25): "+s.events);
	},
	event26: function() {
		/*
		*	Type: singleAction
		*	Description: Form Error
		*/
		s.events="event26";
		RF.Init.Utils.log("s.events: Form Error (event26): "+s.events);
	},
 	event27: function() {
		/*
		*	Type: singleAction
		*	Description: Form Complete
		*/
		s.events="event27";
		RF.Init.Utils.log("s.events: Form Start (event27): "+s.events);
	},
 	event41: function() {
		/*
		*	Type: singleAction
		*	Description: Social Media Events
		*/
		s.events="event41";
		RF.Init.Utils.log("s.events: Social Media Event (event27): "+s.events);
	},
	prop5: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Search results link URL (or term, if URL is not available)
		*/
		s.prop5=(($defined(arguments[0]) && $defined(arguments[0].getProperty('href'))) ? arguments[0].getProperty('href') : arguments[0].retrieve("OmnitureObject").title);
		RF.Init.Utils.log("s.prop5: Search results link URL: "+s.prop5);
	},
	prop29: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Quick Link (Value: "PAGE TITLE - LINK TITLE")
		*/
		s.prop29=location.pathname + '_ql_'+arguments[0].retrieve("OmnitureObject").linkDepth+'_' + arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-');
		RF.Init.Utils.log("s.prop29: Quick Link: "+s.prop29);
	},
	prop30: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Main menu link title. Includes flyover links
		*/
		s.prop30=location.pathname + '_mm_' + arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-');
		RF.Init.Utils.log("s.prop30: Main menu link title: "+s.prop30);
	},
	prop31: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Homepage Teaser (Link title + URL), from HTML components OR Flash Carousel on Homepage
		*/
		if(arguments[0].retrieve) {
			s.prop31=($defined(arguments[0].getProperty('href')) ? ("\""+arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-')+"\" : "+arguments[0].getProperty('href')) : "\""+arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-')+"\"");
		} else {
			s.prop31 = 'HPH: ' + retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-');
		}
		RF.Init.Utils.log("s.prop31: Homepage teaser: "+s.prop31);
	},
	prop32: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Bottom (Footer) menu 1 link title
		*/
		if (arguments[0].retrieve("OmnitureObject").title.indexOf('Find us on ') > -1) {
			var socialNetworkName = arguments[0].retrieve("OmnitureObject").title;
			socialNetworkName = socialNetworkName.replace('Find us on ','');
			socialNetworkName = socialNetworkName.replace(': Opens in a new window','');
			socialNetworkName = socialNetworkName.toLowerCase().replace(new RegExp(' ', 'g'), '');
			s.prop32=location.pathname + '_bm1_' + socialNetworkName;			
		} else {
			s.prop32=location.pathname + '_bm1_' + arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-');			
		}
		RF.Init.Utils.log("s.prop32: Bottom (Footer) menu 1: "+s.prop32);
	},
	prop33: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Bottom (Footer) menu 2 link title
		*/
		s.prop33=location.pathname + '_bm2_' + arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-');
		RF.Init.Utils.log("s.prop33: Bottom (Footer) menu 2: "+s.prop33);
	},
	prop35: function() {
		/*
		*	Type: eventListener.elements
		*	Description: Top Menu (car model) link title. Includes flyover links
		*/
		s.prop35=location.pathname + '_tmc_' + arguments[0].retrieve("OmnitureObject").title.toLowerCase().replace(new RegExp(' ', 'g'), '-');
		RF.Init.Utils.log("s.prop35: Top Menu (car model) link title: "+s.prop35);
	},
	prop38: function() {
		/*
		*	Type: singleAction
		*	Description: Video Name, when video starts playing
		*/
		s.prop38=arguments[0];
		RF.Init.Utils.log("s.prop38: Video Name: "+s.prop38);
	},
	prop43: function() {
		/*
		*	Type: singleAction
		*	Description: Arrange A Test Drive - Login
		*/
		s.prop43=arguments[0];
		RF.Init.Utils.log("s.prop43: Arrange A Test Drive - Login: "+s.prop43);
	},
	prop44: function() {
		/*
		*	Type: singleAction
		*	Description: Arrange A Test Drive - Login
		*/
		s.prop44=arguments[0];
		RF.Init.Utils.log("s.prop44: Arrange A Test Drive - Confirmation: "+s.prop44);
	},
	prop45: function() {
		/*
		*	Type: singleAction
		*	Description: CO2 Calculator - Form
		*/
		s.prop45=arguments[0];
		RF.Init.Utils.log("s.prop45: CO2 Calculator - Results: "+s.prop45);
	},
	prop46: function() {
		/*
		*	Type: singleAction
		*	Description: CO2 Calculator - Form
		*/
		s.prop46=arguments[0];
		RF.Init.Utils.log("s.prop46: CO2 Calculator - Results: "+s.prop46);
	},
	prop49: function() {
		/*
		*	Type: singleAction
		*	Description: Navigation Panel mouseenter - The page & nav panel title (or number, e.g. 'Home - "nav_panel_1"'), where the user has performed a Navigation Panel mouseenter
		*/
		s.prop49=document.title.substring(0,document.title.indexOf('<')-1)+" - \""+arguments[0]+"\"";
		RF.Init.Utils.log("s.prop49: Navigation Panel mouseenter: "+s.prop49);
	},
	prop50: function() {
		/*
		*	Type: singleAction
		*	Description: A8 Campaign Hotspot and Highlight Feature Interaction
		*/
		s.prop50=arguments[0];
		RF.Init.Utils.log("s.prop50: A8 Campaign (Hotspots and Highlight Feature interaction): "+s.prop50);
	},
	prop52: function() {
		/*
		*	Type: singleAction
		*	Description: Social Media Event
		*/
		s.prop52=arguments[0];
		RF.Init.Utils.log("s.prop52: Social Media Event): "+s.prop52);
	}
}

})(document.id);