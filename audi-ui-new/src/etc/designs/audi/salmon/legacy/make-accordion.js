(function($){

RF.Class.MakeAccordion = new Class({
	Implements: [Events, Options, RF.Class.Utilities],
	options: {// default settings
		handle: null,
		expand: null,
		addHandleLinks: false, 
		triggerInnerTag: 'span',
		activeCss: 'active',
		closedCss: 'closed',
		hoverTrigger: false,
		allowToLink: false,
		show: -1,
		opacity: true, 
		fixedHeight:false, 
		fixedWidth: false, 
		alwaysHide: false, 
		initialDisplayFx: true,
		parentElement: null,
		parentClass: null
	},
	initialize: function(options){
		try {
			this.setOptions(options);
	
			if(this.options.handle != null && this.options.expand != null){
				
				this.handle = $$(this.options.handle);
				this.expand = $$(this.options.expand);
				
				this.handle.each(function(h){
					if (h.match('a')) {
						h.addEvent('click', function(e){
							if(this.options.hoverTrigger != true && this.options.allowToLink != true) e.stop();
						}.bind(this));
					}
				}, this);
				
				if(this.options.addHandleLinks != false) this.addHandleLinks();
				this.createAccordion();
				
			}
		} catch(e) {
			this.ErrorHelper({method:"MakeAccordion Class: initialize()",error:e});
		}
	},
	addHandleLinks: function(){
		this.handle.each(function(n){
			new Element('a', {'href':'#','title':n.get('text')}).injectInside(n).wraps(RF.Moo.idE(this.options.triggerInnerTag, n)).addEvent('click', function(e){
				e.stop(); 
				n.fireEvent('click', e);
			});
		}, this);
	},
	createAccordion: function(){
		this.acc = new Accordion(this.handle, this.expand, {
			opacity: this.options.opacity, 
			show: this.options.show,
			fixedHeight: this.options.fixedHeight,
			fixedWidth: this.options.fixedWidth,
			alwaysHide: this.options.alwaysHide,
			initialDisplayFx: this.options.initialDisplayFx,
			onActive: function(toggler, expand){
				toggler.swapClass(this.options.closedCss, this.options.activeCss);
				if(expand.hasClass('js-hide')) expand.removeClass('js-hide');
				if(this.options.parentElement != null && this.options.parentClass != null) toggler.getParent(this.options.parentElement).addClass(this.options.parentClass);
			}.bind(this),
			onBackground: function(toggler){
				toggler.swapClass(this.options.activeCss, this.options.closedCss);
				if(this.options.parentElement != null && this.options.parentClass != null) toggler.getParent(this.options.parentElement).removeClass(this.options.parentClass);
			}.bind(this)
		});
		if(this.options.hoverTrigger != false) this.handle.each(function(n){
			n.addEvent('mouseenter', function(e){
				this.fireEvent('click', e);
			});
		});
	}
});

})(document.id);