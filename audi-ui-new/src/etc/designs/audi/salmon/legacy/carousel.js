(function($){

/*
*	Carousel Class
*/
RF.Class.Carousel = new Class({
    Implements:[Options, Events, RF.Class.Utilities],
    options:{
        stage: false,	// the slide stage/containing div
        slides: [], 	// the individual slide element
        autoplay: true,
        autotime: 4000,
        loop: true,
        intime: 800,
        outtime: 600,
        transition:'sine:in:out',
        link: 'ignore',
        fixtransparentpngs: {
            set: false,
            bgcolour: null
        }
    },
    initialize:function(options){
        try {
            this.setOptions(options);
            this.stage = document.id(this.options.stage); if(!(this.stage)) return;
            this.slides = $$(this.options.slides); if(!(this.slides)) return;
            this.ctas = $$(this.options.slides + ' .link');

            this.ArrayBasics.addItems(this.slides);
            this.ArrayBasics.array.each(function(slide, i){
                if(i == 0) {
                    slide.setStyle('opacity', 0.999);
                } else {
                    slide.setStyle('opacity', 0);
                }
            }, this);

            if (this.ctas) {
                this.ctas.each(function (slide, i) {
                    if (i !== 0) {
                        slide.setStyle('display', 'none');
                    }
                }, this);
            }

            this.autotime = this.options.autotime;
            this.position = 0;
            if(this.options.autoplay != false) this.playthrough();
            this.fixTransparentPngsInIE();

        } catch(e) {
            this.ErrorHelper({method:"Carousel Class : initialize()",error:e});
        }
    },
    _transition:function(stack, from, to) {
        var fromer = stack[from], tooer = stack[to];
        fromer.get('tween', {property:'opacity', duration:this.options.intime, transition:this.options.transition}).start(0);
        tooer.get('tween', {property:'opacity', duration:this.options.outtime, transition:this.options.transition}).start(1);

        if (this.ctas && this.ctas.length>0) {
            this.ctas[from].setStyle('display', 'none');
            this.ctas[to].setStyle('display', 'block');
        }
     },
    next:function() {
        if(this.ArrayBasics.array.length > 1){
            var next = this.position + 1 > this.ArrayBasics.array.length - 1 ? 0 : this.position + 1;
            if(this.options.loop != true && next == this.ArrayBasics.array.length - 1) this.pause();
            this._transition(this.ArrayBasics.array, this.position, this.position = next);
        }
    },
    back:function() {
        if (this.ArrayBasics.array.length > 1) {
            var back = this.position - 1 < 0 ? this.ArrayBasics.array.length - 1 : this.position - 1;
            this._transition(this.ArrayBasics.array, this.position, this.position = back);
        }
    },
    to:function(n) {
        if (this.ArrayBasics.array.length > 1) {
            this._transition(this.ArrayBasics.array, this.position, this.position = (n - 1));
        }
    },
    playthrough:function(){
        if (this.ArrayBasics.array.length > 1) {
            this.auto = (function(){
                this.next();
            }).bind(this).periodical(this.autotime);
        }
    },
    pause:function(){
        $clear(this.auto);
    },

    fixTransparentPngsInIE: function(){
        /* IE7 and 8 don't fade tranparent pngs content well: it loses the alpha channel and defaults to black until the opacity is 100% or 0%
        This is a known IE bug.
        The simplest fix is to apply a solid background colour to the image/parent div; i'm adding to the image
        If this isn't possible, then apply the AlphaLoader fix, this will reduce the black, but won't necessarily remove it entirely */

        if(this.options.fixtransparentpngs.set != false){
            // test for IE7 & 8
            if (window.XMLHttpRequest && document.all) {
                this.pngs = [];
                this.ArrayBasics.array.each(function(slide, i){
                    var png = slide.getChildren('img[src$=.png]');
                    if(png != false) this.pngs.push(png);
                }, this);
                this.pngs = this.pngs.flatten();

                if(this.options.fixtransparentpngs.bgcolour != null){
                    // use the solid bgcolour fix
                    this.pngs.each(function(p){
                        p.setStyles({'backgroundColor': this.options.fixtransparentpngs.bgcolour});
                    }, this);
                } else {
                    // use the AlphaLoader fix suggested by http://www.snutt.net/testzon/ie_alpha/
                    this.pngs.each(function(p){
                        if (p.hasClass('png')) return;
                        p.setStyles({
                            'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\''+p.getProperty('src')+'\')'
                        });
                    }, this);
                }
            }
        }
    }
});

RF.Class.CarouselWithNavigation = new Class({
    Extends: RF.Class.Carousel,
    Implements:[Options, Events, RF.Class.Utilities],
    options:{
        parenttag: 'ul',
        parentid: 'slidenav',
        parentclasses: 'image-toggle',
        childtag: 'li',
        injectwhere: 'after',
        pausethenreplay: false
    },
    initialize:function(options){
        try {
            this.parent(options);
            this.setOptions(options);

            var navString = '';

            if (this.ArrayBasics.array.length > 1) {
                this.ArrayBasics.array.each(function(a, i){
                    navString += '<' + this.options.childtag + '><a href="#">' + (i + 1) + '</a></' + this.options.childtag + '>';
                }, this);
            }

            // wirte in the nav div regardless: helps protect styling/spacing
            this.controller = new Element(this.options.parenttag, {'class':this.options.parentclasses, 'id':this.options.parentid, 'html': navString}).inject(this.stage, this.options.injectwhere);
            this.navelement = document.id(this.options.parentid);

            if (this.ArrayBasics.array.length > 1) {
                this.addEvents();
                this.links = $$('#' + this.options.parentid + ' ' + this.options.childtag + ' a');

                var singleLink = this.links[0];
                singleLink.addClass('on');
                this.navelement.setStyle('width', (singleLink.getStyle('width').toInt() + singleLink.getStyle('marginLeft').toInt() + singleLink.getStyle('marginRight').toInt()) * this.links.length);
                //if(this.options.autoplay != false) this.changeOnStatePlayThrough();
            }


        } catch(e) {
            this.ErrorHelper({method:"CarouselWithNavigation Class : initialize()",error:e});
        }
    },
    changeOnState:function(){
        this.ClassBasics.clearClassFromAll(this.links, 'on');
        this.links[this.position].addClass('on');
    },
    changeOnStatePlayThrough: function(){
        this.onstateauto = (function(){this.changeOnState();}).bind(this).periodical(this.autotime);
    },
    next: function(){
        this.parent();
        this.changeOnState();
    },
    pauseOnStatePlayThrough:function(){
        $clear(this.onstateauto);
    },
    addEvents:function(){
        this.navelement.addEvents({
            'click':function(e){
                if (document.id((e.target)).getProperty('href') != null){
                    e.stop();
                    if (this.options.autoplay != false){
                        this.pause();
                        this.pauseOnStatePlayThrough();
                        if (this.options.pausethenreplay != false){
                            (function(){
                                this.playthrough();
                                //this.changeOnStatePlayThrough();
                            }).bind(this).delay(this.autotime);
                        }
                    }
                    this.ClassBasics.clearClassFromAll(this.links, 'on');
                    (e.target).addClass('on');
                    this.to((e.target).get('text').toInt());
                }
            }.bind(this)
        });
    }
});

var preloadImgs = new Class({
    Implements:[Options,Events],
    options:{
        imgs: [],
        width: false,
        height: false
    },
    initialize:function(options){
        this.setOptions(options);
        this.options.imgs.each(function(i){
            var imgObj = new Element('img',{
                'src': i.src,
                'width': i.width,
                'height': i.height
            });
        });
    }
});

})(document.id);
