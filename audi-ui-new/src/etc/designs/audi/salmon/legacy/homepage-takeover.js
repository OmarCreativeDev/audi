fadeIn = function() {
    new Fx.Tween(document.id('frame'),{
        property   : 'opacity',
        onComplete : function() {
            if (removeTakeover) removeTakeover();
        }
    }).start(1);
};

fadeOutIFrame = function() {
    new Fx.Tween(document.id('homepage-takeover'),{
        property   : 'opacity',
        onComplete : function() {
            if (removeTakeover) removeTakeover();
        }
    }).start(0);
};
removeTakeover = function() {
    if (document.id('homepage-takeover')) document.id('homepage-takeover').destroy();
};

function resize() {
    var scroll = window.getScrollSize(), takeover = document.id('homepage-takeover');
    if (takeover) {        
        takeover.setStyles({
            "width":scroll.x+"px",
            "height":scroll.y+"px"
        });        
    }
};

window.addEvents({
    "resize":resize  
});

