(function($){

/*	Project: Audi.co.uk

* 	Name: NavigationPanelsManager Class
* 	Version: v1.1
* 	Author: Razorfish London
*/
RF.Class.FlashGalleries = new Class({
	Implements: [Options,RF.Class.Utilities],
	Binds:['successCallback','failureCallback'],
	options:{
		range:null,
		model:null,
		jsonResourcePath:'/content/audi/utilities/json/flash-galleries.html?range=',
		jsonResourceParam:'&model=',
		SWF:{
			path:'/content/dam/audi/static/flash/AudiGallery/bin-release/AudiGallery.swf',
			params:{
				play:'true', 
				loop:'false', 
				quality:'high', 
				wmode:'transparent', 
				allowfullscreen: 'true', 
				allowScriptAccess: 'always'
			}
		}
	},
	initialize: function(options){
		try {
			this.setOptions(options);
			if($defined(this.options.range) && $defined(this.options.model))
				this.requestJSON();
			else
				this.failureCallback({message:"No range or model specified"});
		} catch(e) {
			this.ErrorHelper({method:"RF.Class.NavigationPanelsManager : initialize()",error:e});
		}
	},
	requestJSON: function() {
		new Request.JSON({
			url:this.options.jsonResourcePath+this.options.range+this.options.jsonResourceParam+this.options.model, 
			method:'GET',
			onSuccess:this.successCallback,
			onFailure:this.failureCallback
		}).send();
	},
	successCallback: function(jsonObj) {
		if($defined(jsonObj)) {
			$each(jsonObj.FlashGalleries, function(gallery, galleryKey) { // for each 'Gallery' object
				this.chain.apply(this,[
					this.createFlashGallery(gallery, galleryKey)
				]);
			},this);			
		} else {
			this.failureCallback({message:"Invalid JSON received"});
		}
	},
	createFlashGalleryLink: function(gallery, galleryKey) {
		/*
		*	@Operation1: If it's the Gold Experience (Showroom) we must present a SPAN element as the SWF gallery container, to avoid a title attribute tooltip
		*	@Operation2: If it's the Bronze Experience (HTML/Ajax) we must present an ANCHOR element as the SWF gallery container, to get title attribute a tooltip
		*/
		if(gallery.journeyType == 'gold') {
			this.galleryLink = new Element('span',{'id':galleryKey});
		} else {
			this.galleryLink = new Element('a',{'id':galleryKey,'title':gallery.linkText,'class':'eVar34'});
		}
		/*
		*	@Operation3: Inject the container inside the existing flash-gallery-links component container (if it exists), else create this component container and inject it inside the main-stage
		*/
		this.galleryLink.inject(this.existsContainerForInjection('div','flash-gallery-links','main-stage'));
	},
	createFlashGallery: function(gallery, galleryKey) {
		/*
		*	@Operation1: The flash gallery is either going to replace an existing one on the page, or be a new DOM element.
		*				Check to see if there is already a gallery with the same DOM ID, and if not create a DOM element as the container
		*/
		if(!$defined(document.id(galleryKey))) this.createFlashGalleryLink(gallery, galleryKey);
		/*
		*	@Operation2: Create a new SWF object, with the gallery ID as the container 
		*/
		new Swiff(this.options.SWF.path, {
           	container: document.id(galleryKey),
           	id: galleryKey+'_swf', 
			width: gallery.width, 
			height: gallery.height,
           	params: this.options.SWF.params,
           	vars: gallery.vars
        });
	},
	failureCallback: function(e) {
		this.ErrorHelper({method:"RF.Class.FlashGalleries : failureCallback()",error:e});
	}
});

})(document.id);
