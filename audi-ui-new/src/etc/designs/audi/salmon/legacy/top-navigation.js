/* IE8  Fix -  getElementsByClassName */

if(!document.addEventListener){
   // IE8 -	
	 document.getElementsByClassName = Element.prototype.getElementsByClassName = function(class_names) {
        // Turn input in a string, prefix space for later space-dot substitution
        class_names = (' ' + class_names)
            .replace(/[~!@$%^&*()_+\-=,./';:"?><[\]{}|`#]/g, '\\$&')
            .replace(/\s*(\s|$)/g, '$1')
            .replace(/\s/g, '.');
        return this.querySelectorAll(class_names);
    };
}

/* '--  */



var tnStatus = {taborder:[], currtab:0, subnavorder:[], currsubnav:0};
RF.Class.TopNavigation = new Class({
	Extends: RF.Class.NavigationContent,
	Implements: [RF.Class.Utilities, Events, Options, RF.Class.MeetTheRangePromo],
	Binds:['clickEvent','touchEventAppleMobile'],
	options: {
		model: null,
		promourl: null
	},
	initialize:function(options){
		try {
			this.parent(options);
			this.setOptions(options);
			this.model = this.options.model;
			this.list = document.id('model-list');
			this.price = document.id('price');
			this.pricelabel = document.id('price-label');
			this.currency = document.id('currency');
			this.otr = document.id('otr');
			//this.finance = document.id('finance');
			//this.months = document.id('months');
			this.copy = document.id('overview-copy');
			this.cta = document.id('overview-cta');
			this.promoarea = document.id('meet-the-range');
			
			// add range button to fly-out
			if (document.id('range-list') == null) {
				var flyoutListWrap = new Element('div', {id: 'flyoutListWrap'});
				flyoutListWrap.wraps(document.id('model-list'));
				document.id('flyoutListWrap').setStyle('float','left');
				document.id('model-list').getParent().grab(new Element('ul', {id: 'range-list'}), 'top');
				document.id('range-list').grab(new Element('li', {id: 'range-link'}), 'top');	
				$$('.model-overview').grab(new Element('div', {id: 'explore-label'}), 'top');
				//if (RF.Browser.ie6) {DD_belatedPNG.fix('#range-link a');}
			}			
			this.explorelabel = document.id('explore-label');
			tnStatus.taborder.push(this.tab);		
			this.createModelList(this.options.model);
	
			this.callEvents();
		
		} catch(e) {
			this.ErrorHelper({method:"RF.Class.FlyOverNavigation : initialize()",error:e});
		}
	},
	modelsStr: '',
	createModelList:function(models){
		models.each(function(m, i){
			this.modelsStr += '<li><a class="prop35" href="'+m.url+'" title="Explore the '+m.model+' model">' + m.model + '</a></li>';
		},this);	
	},
	getInfo: function(prop, html){
		prop.empty();
		prop.set('html', html);	
	},
	clickEvent: function() {
		this.flyout.ClassBasics.clearClassFromAll(this.flyout.maintabs, 'on');
		this.tab.addClass('on');
	},
	callEvents:function(){
	
		this.tab.addEvents({
			'click':this.clickEvent,
			'mouseenter':function(e){
				if(e) {e.preventDefault()}
				// set-up range button
				this.getInfo(this.list, this.modelsStr);
				var allModels = $$('#model-list a'), rangeTag = "";
				if (allModels.length > 1) {
					document.id('model-list').setStyle('visibility','visible');
					rangeTag = " range";
				}
				else {
					document.id('model-list').setStyle('visibility','hidden');
					rangeTag = "";
				}
				if (this.tab.id.toUpperCase() == "A1" || allModels.length > 1) {					
					document.id('range-link').set('html', '<a class="prop35 on" href="'+this.tab.href+'" title="Explore the '+this.tab.id.toUpperCase()+rangeTag+'">'+this.tab.id.toUpperCase()+rangeTag+'</a>');
				} else {
					document.id('range-link').set('html', '<a class="prop35 on" href="'+this.model[0].url+'" title="Explore the '+this.tab.id.toUpperCase()+rangeTag+'">'+this.tab.id.toUpperCase()+rangeTag+'</a>');
				}
				document.id('tn-flyover').removeEvents('mouseenter').removeEvents('mouseleave').addEvents({
					'mouseenter':function(e){
						e.preventDefault();
						document.id(this.tab.id).addClass('fly');
					}.bind(this),
					'mouseleave':function(e){
						e.preventDefault();
						document.id(this.tab.id).removeClass('fly');
					}.bind(this)
				});						
				document.id('range-list').removeEvents('mouseenter').addEvents({
					'mouseenter':function(){
						if (allModels.length > 1) {$$('#range-link a').addClass('on'); this.flyout.ClassBasics.clearClassFromAll(allModels, 'on');}
						else {this.flyout.ClassBasics.clearClassFromAll(allModels, 'on'); document.id(allModels[0]).addClass('on');}
						
						document.id('range-landing').set('styles',{'background-position': '0 0'});
						document.id('overview-img').set({
							'styles' : {'background-position': '0 0'},
							'text'   : this.options.model[0].model
						});
						document.id('explore-label').setStyle('display','block');
						if (allModels.length > 1) {
							this.getInfo(this.explorelabel, (this.tab.id.toUpperCase()));
							document.id('explore-label').setStyle('display','block');
							$$('.finance-copy').setStyle('display','none');
							this.getInfo(this.copy, (this.model[0]['range-copy']));
							this.getInfo(this.cta, "<a class='prop35 btn-flyover explore-range' href='"+this.tab.href+"' title='"+this.tab.title+"'>"+this.tab.id.toUpperCase()+rangeTag+"</a>");
						} else {
							this.getInfo(this.explorelabel, (""));
							document.id('explore-label').setStyle('display','none');
							$$('.finance-copy').setStyle('display','inline');
							this.getInfo(this.price, (this.model[0].price));
							this.getInfo(this.pricelabel, (this.model[0].priceLabel+" "));
							if(this.model[0].priceLabel == ""){
								this.pricelabel.setStyles({'display':'inline'});
							} else {
								this.pricelabel.setStyles({'display':'block'});
							}
							this.getInfo(this.currency, (this.model[0].currency));
							this.getInfo(this.otr, (this.model[0].otr));
							this.getInfo(this.copy, (this.model[0].copy));
							// ACOUK-6031
//							if (this.tab.id.toUpperCase() == "A1") {
//								this.getInfo(this.cta, "<a class='prop35 btn-flyover explore-model' href='"+this.tab.href+"' title='Explore this model'>"+this.tab.id.toUpperCase()+rangeTag+"</a>");
//							} else {
//								this.getInfo(this.cta, (this.model[0].cta));
//							}
							this.getInfo(this.cta, (this.model[0].cta));
						}
					}.bind(this),
					'mouseleave':function(e){
						e.preventDefault();
					}.bind(this)
				});				
				
				document.id('range-landing').removeProperty('class');
				document.id('range-landing').addClass('overview-' + this.tab.get('id'));
				
				this.promoarea.empty();
				this.makePromoRequest();		
				
				var allSubLinks = $$(this.submenulinks);
				
				tnStatus.subnavorder = [];
				tnStatus.currsubnav = 0;
				
				allModels.each(function(a, i){
					tnStatus.subnavorder.push(a);
					a.addEvents({
						'mouseenter':function(){
							$$('#range-link a').removeClass('on');
							this.flyout.ClassBasics.clearClassFromAll(allModels, 'on');
							a.addClass('on');
							document.id('range-landing').set('styles',{'background-position': '0 ' + ((i+1)*212)*-1 + 'px'});
							document.id('overview-img').set({
								'styles' : {'background-position': '0 ' + ((i+1)*212)*-1 + 'px'},
								'text'   : this.options.model[i].model
							});
							document.id('explore-label').setStyle('display','none');
							$$('.finance-copy').setStyle('display','inline');
							this.getInfo(this.price, (this.model[i].price));
							this.getInfo(this.pricelabel, (this.model[i].priceLabel+" "));
							if(this.model[i].priceLabel == ""){
								this.pricelabel.setStyles({'display':'inline'});
							} else {
								this.pricelabel.setStyles({'display':'block'});
							}
							this.getInfo(this.currency, (this.model[i].currency));
							this.getInfo(this.otr, (this.model[i].otr));
							//this.getInfo(this.finance, (this.model[i].finance));
							//this.getInfo(this.months, (this.model[i].months));
							this.getInfo(this.copy, (this.model[i].copy));	
							this.getInfo(this.cta, (this.model[i].cta));

							RF.Moo.idE('#overview-cta a').addEvents({
								'keydown':function(m){
									if (m.key == 'tab'){
										m.stop();
										if(!(m.shift)){
											if(tnStatus.currsubnav < tnStatus.subnavorder.length -1){
												tnStatus.currsubnav += 1;
												tnStatus.subnavorder[tnStatus.currsubnav].focus();
											}else{
												if(tnStatus.currtab < tnStatus.taborder.length){
													tnStatus.taborder[tnStatus.currtab].focus();
												}else {
													document.id('closetn-flyover').focus();
												}
											}	
										}else{
											if(tnStatus.currsubnav > 0){
												tnStatus.currsubnav -= 1;
												tnStatus.subnavorder[tnStatus.currsubnav].focus();	
											}
										}	
									}		
								}
							});
										
						}.bind(this),
						'focus':function(m){
							m.stop();
							(m.target).fireEvent('mouseenter');	
						},
						'keydown':function(m){
							if (m.key == 'tab'){
								m.stop();
								if(!(m.shift)){
									(i < allModels.length) ? RF.Moo.idE('#overview-cta a').focus() : tnStatus.taborder[tnStatus.currtab].focus();
								}else{	
									if(tnStatus.currsubnav > 0){
										tnStatus.currsubnav -= 1;
										tnStatus.subnavorder[tnStatus.currsubnav].focus();	
									} else {
										if(tnStatus.currtab > 0){
											tnStatus.currtab -= 1;
											tnStatus.taborder[tnStatus.currtab].focus();
										}
									}	
								}
								
							}
						}
					});	
				},this);
				//allModels[0].fireEvent('mouseenter');
				document.id('range-list').fireEvent('mouseenter');
				
				// Add Omniture event tracking, just to the links in the top navigation flyover
				RF.Init.OmnitureTopNavEvents = new RF.Class.OmnitureTrackingManager({eventListeners:{elements:'#top-nav-content a'}});
				delete RF.Init.OmnitureTopNavEvents;
				// Trigger mouseenter tracking for the tab item
			}.bind(this),
			'focus':function(e){
				e.stop();
				(e.target).fireEvent('mouseenter');
			}.bind(this),
			'keydown':function(e){
				if(e.key == 'tab'){
					e.stop();
					if(!(e.shift)){
						if (tnStatus.currtab < tnStatus.taborder.length){
							RF.Moo.idE(this.submenulinks).focus();
							tnStatus.currtab += 1;
						}	
					}else{
						if(tnStatus.currtab > 0){
							tnStatus.currtab -= 1;
							tnStatus.taborder[tnStatus.currtab].focus();	
						} else {
							RF.Moo.idE('#logo-audi a').focus();
							this.flyout.slideClose();	
						}	
					}	
				}
			}.bind(this),
			'keyup':function(e){
				if (e.key == 'tab') {
					e.stop();
					this.flyout.slideOpen();
				}
			}.bind(this)
		});	
		
		/* Remove the click event when in Apple Mobile */
		if(Browser.Platform.ipod) {
			this.tab.addEvents({
				'click':this.touchEventAppleMobile
			});
		}
	},
	touchEventAppleMobile: function() {
		this.appleMobileEvent = arguments[0];
		this.appleMobileEvent.preventDefault();	
		this.tab.fireEvent('mouseenter');
		this.removeTouchEvents();
	},
	removeTouchEvents: function() {
		this.tab.removeEvents({
			'click':this.touchEventAppleMobile
		});
	}
});


window.addEvents({
	'domready': function() {
		if (document.id('top-nav')){			
			RF.Init.FlyOverNavigation = new RF.Class.FlyOverNavigation({id:'top-nav', maintabs:'#top-nav dd.tab a', flyover:'tn-flyover'});			
			(audiModelsObj) ? parseTopNavigationData(audiModelsObj) : new Request.JSON({
																			url:"/content/audi/audi-models.html",
																			method:'get',
																			onSuccess:function(jsonObj){
																				parseTopNavigationData (jsonObj);
																			}
																		});
		}
	}
});



/*

window.addEvents({
	'load': function() {
		console.log("PAGE LOADED");
		if (document.id('top-nav')){			
			console.log("PAGE LOADED");
			var reqTopNavHTML = new Request.HTML({
				method:'get',
				url: "/etc/designs/audi/inc/top-nav-flyover.html",
				onSuccess: function(tree, elements, html, js){
					document.id('top-nav-content').empty();
					document.id('top-nav-content').set('html',html);
					RF.Init.FlyOverNavigation = new RF.Class.FlyOverNavigation({id:'top-nav', maintabs:'#top-nav dd.tab a', flyover:'tn-flyover'});			
					RF.Init.RequestTopNavigation = (audiModelsObj) ? parseTopNavigationData(audiModelsObj) : new Request.JSON({
						url:"/content/audi/audi-models.html",
						method:'get',
						onSuccess:function(jsonObj){
							parseTopNavigationData (jsonObj);
						}
					}).send();
				}.bind(this)
			}).send();
		}
		
	}
});

*/

function parseTopNavigationData (jsonObj) {
	$each(jsonObj, function(value, key) {
		new RF.Class.TopNavigation({model:value, tabLink:RF.Moo.idE('#model-'+key+' a'), flyoutVar:RF.Init.FlyOverNavigation, contentArea:'tn-flyover'})
	});
	document.getElementsByClassName("flyover")[0].style.background="#d6d7d7 url(/etc/designs/audi/img/bgs/flyover-gradient.gif) repeat-x 0 0";
	document.getElementsByClassName("flyover")[0].style.borderLeft="1px solid #fff";
	document.getElementsByClassName("sub-menu")[0].style.background="url(/etc/designs/audi/img/bgs/flyover-btm.png) repeat-x center bottom";
}