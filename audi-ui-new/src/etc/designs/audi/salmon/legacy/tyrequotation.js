/*
 *    @Author:  Geoff Kaile
 *    @Company: Razorfish London
 *    @Name:    TyreQuotation Class
 *    @Description: 
 *    Class to update search fields in the tyrequotation form, and filter/sort the results
 *   Lookups below:
 *       /tyrequotation/getModels.html?range=A4
 *       /tyrequotation/getTrims.html?model=A4%20Avant%20%28%2008%3E%20%29
 *
 *       /tyrequotation/getWheelWidths.html?wheelSize=15
 *       /tyrequotation/getRatios.html?wheelSize=14&wheelWidth=175
 *       /tyrequotation/getLoads.html?wheelSize=14&wheelWidth=175&ratio=70
 *       /tyrequotation/getSpeedIndexes.html?wheelSize=14&wheelWidth=175&ratio=70&load=84
 */

// For Mootols scripts
(function($){  

RF.Class.TyreTabs = new Class({
	Implements   : [Options,RF.Class.Utilities],
	Binds        : ['createTab','showTab'],
	options      : {
		container : 'tyrequotation',
		labelEl   : '#tyretab li',
		tabEl     : 'div.tab',
		show      : 0
	},
	initialize : function(options) {
		this.setOptions(options);
		this.labels = document.id(this.options.container).getElements(this.options.labelEl);
		this.tabs   = document.id(this.options.container).getElements(this.options.tabEl);
		if (this.labels.length === 0 || this.tabs.length === 0) return;
		this.labels.each(this.createTab);
	},
	createTab : function(label,i) {
		label.store('idx',i).addEvent('click',this.showTab);

		if (i != this.options.show) this.tabs[i].addClass('js-hide');
		else label.addClass('active');
	},
	showTab : function(e) {
		this.openTab($(e.target).retrieve('idx'));
	},
	openTab : function(index) {
		this.labels[this.options.show].removeClass('active');
		this.tabs[this.options.show].addClass('js-hide');
		this.options.show = index;
		this.labels[this.options.show].addClass('active');
		this.tabs[this.options.show].removeClass('js-hide');
	}
});
RF.Class.TyreQuotationSearch = new Class({
	Implements   : [Options,RF.Class.Utilities],
	Binds        : ['changeOptions','registerEvent'],
	options      : {
		defaultOption : '<option value="- Select -" selected="selected">- Select -</option>',
		img : 'tyrewall',
		regField      : 'vrm',
		lookup        : [
		{
			'name'  : 'range',
			'tab'   : 'model'
		},
		{
			'name'  : 'model',
			'url'   : 'getModels',
			'fields': [0],
			'tab'   : 'model'
		},
		{
			'name'    : 'trim',
			'url'    : 'getTrims',
			'fields': [1],
			'tab'    : 'model'
		},
		{
			'name'  : 'wheelSize',
			'tab'   : 'advanced',
			'src'   : '/content/dam/audi/production/RestOfSite/OwnersArea/tyres/Tyre-diagram-1.jpg'
		},           
		{            
			'name'  : 'wheelWidth',
			'url'   : 'getWheelWidths',
			'fields': [3],
			'src'   : '/content/dam/audi/production/RestOfSite/OwnersArea/tyres/Tyre-diagram-2.jpg',
			'tab'   : 'advanced'
		},          
		{           
			'name'  : 'ratio',
			'url'   : 'getRatios',
			'fields': [3,4],
			'src'   : '/content/dam/audi/production/RestOfSite/OwnersArea/tyres/Tyre-diagram-3.jpg',
			'tab'   : 'advanced'
		},          
		{           
			'name'  : 'load',
			'url'   : 'getLoads',
			'fields': [3,4,5],
			'src'   : '/content/dam/audi/production/RestOfSite/OwnersArea/tyres/Tyre-diagram-4.jpg',
			'tab'   : 'advanced'
		},          
		{           
			'name'  : 'speedIndex',
			'url'   : 'getSpeedIndexes',
			'fields': [3,4,5,6],
			'src'   : '/content/dam/audi/production/RestOfSite/OwnersArea/tyres/Tyre-diagram-5.jpg',
			'tab'   : 'advanced'
		}
		]
	},
	initialize : function(options) {
		this.setOptions(options);
		this._container = document.id(this.options.container);
		if (!this._container) this._container = document.getElement('form[name="'+this.options.container+'"]');
		if (!this._container) return;
		this.enableLog();
		this._img = document.id(this.options.img)
		this.tabs = new RF.Class.TyreTabs();
		this.addLookups();
		this.checkLookups();
	},
	addLookups : function() {
		this._select = this._container.getElements('select');
		this._select.each(this.addChangeEvent,this);
	},
	checkLookups : function() {
		this._registration = document.id(this.options.regField).addEvent('change',this.registrationEvent);
		for (var i=this._select.length-1,select; select=this._select[i]; i--) {
			if (select.get('value').indexOf('-') == -1) {
				this.tabs.openTab(this.options.lookup[i].tab == 'model' ? 1 : 2);
				this.performLookup(i+1);
				break;
			}
		}
	},
	registrationEvent : function() {
		if (!$defined(this._select)){
			return;
		}
		this.currentField = -1;
		this.currentTab   = null;
		this._select.each(this.reset,this);
	},
	addChangeEvent : function(select,index) {
		if ($defined(this.options.lookup[index]) && this.options.lookup[index].url) {
			this._select[index-1].addEvent('change',function() {
				this.performLookup(index);
			}.bind(this));
		}
	},
	performLookup : function(index) {
		this._change = this._select[index];
		var lookup = this.options.lookup[index];
		this._img.set('src',lookup.src);
		this.currentTab = lookup.tab;
		this.currentField = index;
		this._registration.set('value','');
		this._select.each(this.reset,this);
		this.request = new Request.JSON({
			url       : '/tyrequotation/'+lookup.url+'.html',
			onSuccess : this.changeOptions
		}).get(this.getParameters(lookup.fields));
	},
	reset : function(select,index) {
		var lookup = this.options.lookup[index];
		if (lookup.tab == this.currentTab) {
			if (index>this.currentField) select.set('value','- Select -');
		} else {
			if (lookup.fields) select.set('html',this.options.defaultOption);
			else select.set('value','- Select -');
		}
	},
	getParameters : function(fields) {
		var params = {};
		fields.each(function(field) {
			params[this.options.lookup[field].name] = this._select[field].get('value');
		},this);
		return params;
	},
	changeOptions : function(data) {
		var html = [this.options.defaultOption];
		data.each(function(option) {
			html.push('<option value="'+option.value+'">'+option.text+'</option>');
		});
		this._change.set('html',html.join(''));
	}
});
RF.Class.TyreQuotationResults = new Class({
	Implements   : [Options,RF.Class.Utilities],
	Binds        : ['getTabs','showTab','orderResults','buildRow','getResults','checkResult','postTyreSelection'],
	options      : {
		car       : 'input[name="experianModel"]',
		tabs      : '#tyretab li',
		brand     : 'select[name="filter.filterBrand"]',
		header    : 'thead tr',
		results   : 'tbody tr'
	},
	results      : [],
	initialize   : function(options) {
		this.setOptions(options);
		this._container = document.id(this.options.container);
		if (!this._container) return;
		this.enableLog();
		this.showFilterSort();
		this.setResults();
		this.initSortByPrice();
		this.start();
	},
	start : function() {
		if (this._brand.getElement('option[value="ALL"]')) this._brand.set('value','ALL');
		this.getResults();
	},
	showFilterSort : function() {
		this._container.getElements('.hide').removeClass('hide');
		this._container.getElement('#tyretab').addClass('js');
		this._brand = this._container.getElement(this.options.brand).addEvent('change',this.getResults);
		this._container.getElements(this.options.tabs).each(this.getTabs);
	},
	getTabs : function(tab,i) {
		tab.addEvent('click',function(e) {
			this.showTab(e.target);
		}.bind(this));
		if (i==0) this.showTab(tab);
	},
	showTab : function(target) {
		if (this._size) this._size.removeClass('active');
		this._size = target.addClass('active');
		this.size = this._size.get('text').replace(/\W/,'');
		this.getResults();
	},
	setResults : function() {
		this.car  = document.getElement(this.options.car).get('value');
		this._rows = this._container.getElements(this.options.results);
		this._rows.each(this.buildRow);
        
	},
	initSortByPrice:function(){

		var sortByPriceCheckbox = document.getElementById("sortByPrice");

		var sortByPriceClickedFunction = function() {

			var sortByPriceChecked = sortByPriceCheckbox.checked;

			function tyreObj(descRow, tyreRow) {
				this.descRow = descRow;
				this.tyreRow = tyreRow;
			}

			var temp_rows = new Array();
			var table = document.getElementById("results");
			var tableBody = document.getElementById("results").getElementsByTagName("TBODY")[0];

			for(var i = table.rows.length - 1; i >= 0; i -= 2){
				temp_rows.push(new tyreObj(table.rows[i - 1], table.rows[i]));
			}

			if (sortByPriceChecked) {
				temp_rows.sort(function(f, s) {
					return (parseFloat(f.tyreRow.getElement("td.ex").getElement("span").get("text").replace(",", ".")) - 
						parseFloat(s.tyreRow.getElement("td.ex").getElement("span").get("text").replace(",", ".")));
				});
			}
			else {
				temp_rows.sort(function(f, s) {
					return f.descRow.getElement("td.tyreDescription").get("text").localeCompare(s.descRow.getElement("td.tyreDescription").get("text"));
				});
			}

			for(i = 0; i < temp_rows.length;i++) {
				tableBody.appendChild(temp_rows[i].descRow);
				tableBody.appendChild(temp_rows[i].tyreRow);
			}
		};
		
		$(sortByPriceCheckbox).addEvent('click', sortByPriceClickedFunction);
	},
	buildRow : function(result,i) {
		if (/^part-/.test(result.id)) {
			var resObj = this.setResult(result,i);
            
			resObj.price.each(function(price) {
				var originalPrice = parseFloat(price.getProperty('title'));
				price.setProperty('title', originalPrice.toFixed(2));
				price.getElement('span').set('text',originalPrice.toFixed(2));
			}
			);
            
			resObj.quantity = new Element('select',{
				'name'   : 'quantity',
				'events' : {
					'change' : function(e) {
						this.updatePrice(resObj);
					}.bind(this)
				}
			}).inject(new Element('td',{
				'class' : 'quantity',
				'text'  : 'x'
			}).inject(result,'top'),'top');
			for (var i=1; i<5; i++) {
				new Element('option',{
					'value' : i,
					'text'  : i
				}).inject(resObj.quantity);
			}
			resObj.quantity.set('value',1);
			new Element('a',{
				'class'  : 'btn-sml btn-select',
				'href'   : '#',
				'title'  : 'Select',
				'text'   : 'Select',
				'events' : {
					'click' : function(e) {
						e.stop();
						this.postTyreSelection(resObj);
					}.bind(this)
				}
			}).inject(new Element('td').inject(result,'bottom'));
		} else result.getElement('td').setProperty('colspan','5');
	},
	postTyreSelection:function(a){
		method = "post";

		var form = document.createElement("form");
		form.setAttribute("method", method);
		form.setAttribute("action",'/content/audi/owners-area/servicing-maintenance-mot/audi-tyres/tyre-quotation.html?appAction=%2Ftyrequotation%2Fsearch.html');

		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "model");
		hiddenField.setAttribute("value", a.model.get("value"));

		form.appendChild(hiddenField);

		hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "tyreId");
		hiddenField.setAttribute("value", a.tyreId.get("value").replace(/\s/g, "" ).replace(",","").replace(".", ""));

		form.appendChild(hiddenField);
		
		hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "tqsAppLocation");
		hiddenField.setAttribute("value", a.tqsAppLocation.get("value"));

		form.appendChild(hiddenField);

		hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "quantity");
		hiddenField.setAttribute("value", a.quantity.get("value"));

		form.appendChild(hiddenField);

		hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "chooseTyre");
		hiddenField.setAttribute("value", "");

		form.appendChild(hiddenField);

		document.body.appendChild(form);
		form.submit();
	},
	updatePrice : function(result) {
		var value = result.quantity.get('value').toInt();
		result.price.each(function(price) {
			var newPriceVal = parseFloat(price.getProperty('title'))*value;
			price.getElement('span').set('text',newPriceVal.toFixed(2));
		},this);
	},
	setResult : function(details,i) {
		var description = this._rows[i-1];
		var result = {
			part        : details,
			description : description,
			tyreId        : details.getElement('input[name$="tyreId"]'),
			model        : details.getElement('input[name$="modelName"]'),
			tqsAppLocation : details.getElement('input[name$="tqsAppLocation"]'),
			brand       : description.getElement('strong').get('text').toUpperCase(),
			size        : details.getProperty('class').substr(1).replace(/\W/,''),
			price       : [details.getElement('td.ex'),details.getElement('td.inc')]
		}
		this.results.push(result);
		return result;
	},
	getResults : function() {
		this.brand = this._brand.get('value');
		this.results.each(this.checkResult);
		var rows = $$('tr[stlye$]');
		var noResultsFound = true;
		
		for (i = 0; i < rows.length; i++) {
			if (!(rows[i].style.display == 'none')) {
				noResultsFound = false;
				break;
			}
		}
		
		var noResultsPar = $$(document.getElementById('noResultsForSelection'));
		
		if (noResultsFound) {
			noResultsPar.setStyle('display', '');
		}
		else {
			noResultsPar.setStyle('display', 'none');
		}
	},
	checkResult : function(result) {
		var style = ((this.brand == 'All' || result.brand == this.brand) && this.size.indexOf(result.size) != -1 ? '' : 'none');
		result.part.setStyle('display',style);
		result.description.setStyle('display',style);
	}

});
document.addEvent('domready', function () {
    var printButton = $$('div.print-button a');
    RF.Init.tyreQuotationSearch = new RF.Class.TyreQuotationSearch({
        container: 'tyrequotation'
    });

    RF.Init.tyreQuotationResults = new RF.Class.TyreQuotationResults({
        container: 'tyre-quotation'
    });
    if (!!printButton.length) {
        printButton.addEvents({
            'click': function (e) {
                var s = s_gi('rsid'); // enter report suite id
                s.linkTrackVars = 'eVar12,events';
                s.linkTrackEvents = 'event15';
                s.eVar12 = '<Print>';
                s.events = 'event15';
                s.tl(this, 'o', 'Print');
            }.bind(this)
        });
        return false;
    }
/*
	jQuery('.pdf-download-link').click(function(e) {
		var pdfUrl = jQuery('.pdf-download-link').attr('href');
		var win = window.open("","temp","width=800,height=500,menubar=yes,toolbar=yes,location=yes,status=yes,scrollbars=auto,resizable=yes");
		win.location.href = pdfUrl;
		win.focus();
		e.preventDefault();
	});
*/	
});

})(document.id);
//^^^^^^^^^^^^^^ MooTools