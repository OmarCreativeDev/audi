(function($){

/*	Project: Audi.co.uk
* 	Name: LoadingSpinner Class
* 	Version: v1.0
* 	Author: Razorfish London
*/
RF.Class.LoadingSpinner = new Class({
	options:{
		'id'	 : 'loading',
		'src'	 : '@img.file.src.path@/loader.gif',
		'width'  : '32',
		'height' : '32',
		'styles' : {
			'position' 	: 'absolute',
			'top'		: '39%',
			'left'		: '48%',
			'zIndex'	: 100
		},
		'alt'	 : 'Loading...'
	},
	initialize: function () {
		this.setOptions(options);
	},
	addSpinner: function() {
		/*
		*	arguments[0]: 	HTML Container [Element}
		*	arguments[1]: 	Id [String]
		*/
		if(!document.retrieve("LoadingSpinner")) document.store("LoadingSpinner"+arguments[1],new Element('img',this.options).set('id',arguments[1]||this.options.id));
		document.retrieve("LoadingSpinner"+arguments[1]).inject(arguments[0],'top');
	}
});

})(document.id);