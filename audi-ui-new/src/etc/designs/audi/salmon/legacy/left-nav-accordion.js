(function($){

RF.Class.LeftNavAccordion = new Class({
	Extends: RF.Class.MakeAccordion,
	Implements:[Options, Events, RF.Class.Utilities],
	options:{
		handle: '#leftnav-accordion .accord .hdr a',
		expand: '#leftnav-accordion .accord .expand',
		alltoplevel: '#leftnav-accordion li .hdr a',
		parentElement: '.accord',
		parentClass: 'parent-active',
		show: -1
	},
	initialize: function(options){
		try {
			this.parent(options);
			this.setOptions(options);
			this.refactorIndex(this.options.show);
		} catch(e) {
			this.ErrorHelper({method:"LeftNavAccordion Class : initialize()",error:e});
		}
	},
	refactorIndex: function(index){	
		if(index != -1){	
			this.show = $$(this.options.alltoplevel)[index];
			if(this.show.getParent(this.options.parentElement) != null){
				$$(this.options.handle).each(function(a, i){
					if(this.show == a) this.acc.display(i);
				}, this);	
			} else this.acc.display(-1);
		}		
	}
});

})(document.id);