(function($){

/*
*	OpenQuickTimeVR Class
*/
RF.Class.QTLaunch = new Class({
	Implements:[Options,RF.Class.Utilities],
	options: {
		src:null,
		width:null,
		height:null
	},
	initialize: function(options) {
		this.setOptions(options);
		if($defined(this.options.src) && $defined(this.options.width) && $defined(this.options.height)){
			this.nwin = '';
			if(!this.nwin.closed && this.nwin.location){this.nwin.location.href = this.options.src;} else {this.nwin=window.open(this.options.src,'pop','"status,height=' + this.options.height + ',width=' + this.options.width + ',scrollbars=0"');if(!this.nwin.opener) this.nwin.opener = self;}
			if(window.focus) this.nwin.focus();
			return false;
		}
	}	
});

})(document.id);