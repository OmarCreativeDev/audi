(function($){

RF.Class.Drag = new Class({

	//	intialise properties and listeners
	initialize: function(id, data) {

		var touch;

		$extend(this, data);
		
		this.id = id;
		this.element = $(this.id);
		this.document = this.element.getDocument();
		
		this.mouse = {now: {}, pos: {}};
		//this.value = {now: {}};
		
		this.start = this.start.bind(this);
		this.drag = this.drag.bind(this);
		this.stop = this.stop.bind(this);
		
		touch = RF.isEventSupported("touchstart");
		
		this.events = {
			start: (touch ? "touchstart" : "mousedown"),
			drag: (touch ? "touchmove" : "mousemove"),
			stop: (touch ? "touchend" : "mouseup")
		};
				

		this.element.addEvent(this.events.start, this.start);

	},
	
	start: function(e) {

		if (e.rightClick) {
			return true;
		}
		e.preventDefault();

		if(e.targetTouches && e.targetTouches[0]) {
			this.mouse.start = {
				x: e.targetTouches[0].clientX,
				y: e.targetTouches[0].clientY
			};
		}
		else {
			this.mouse.start = e.page;
		}
		
		this.onStart();

		this.document.addEvent(this.events.drag, this.drag);
		this.document.addEvent(this.events.stop, this.stop);
		this.document.addEvent("mouseleave", this.stop);
		
	},
	
	drag: function(e) {
		e.preventDefault();
		if(e.targetTouches && e.targetTouches[0]) {
			this.mouse.now = {
				x:e.targetTouches[0].clientX,
				y:e.targetTouches[0].clientY
			};
		}
		else {
			this.mouse.now = e.page;
		}		

		this.onDrag(this.mouse.now.x - this.mouse.start.x, this.mouse.now.y - this.mouse.start.y);
	},
	
	stop: function(e) {
		
		this.onComplete();

		this.document.removeEvent(this.events.drag, this.drag);
		this.document.removeEvent(this.events.stop, this.stop);
		this.document.removeEvent("mouseleave", this.stop);		
	}
	
});


RF.fx = function () {

	var self,
		looping = false,
		frameRate = 60,
		tweens = [],
		eventLoop,
		getComputedStyle,
		setOptions,
	    bezier2,
		bezier3,
		defaultOptions = {
			duration: 1000,
		    delay: 0,
			easing: false,
		    prefix: {},
		    suffix: {},
		    onStart: undefined,
		    onStartParams: [],
		    onUpdate: undefined,
		    onUpdateParams: [],
		    onComplete: undefined,
		    onCompleteParams: []
		};


	setOptions = function(fx, options) {
		var key;
	    for (key in defaultOptions) {
	        if (typeof options[key] !== 'undefined') {
	            fx[key] = options[key];
				if(/Params$/.test(key)) {
					options[key].unshift(0);
				}
				delete options[key];
	        }
			else {
	            fx[key] = defaultOptions[key];
	        }
	    }

		fx.easing = fx.easing || self.linear;

	};

	getComputedStyle = function(el, property) {

		var css, value;

		if (document.defaultView && document.defaultView.getComputedStyle) {
			css = document.defaultView.getComputedStyle(el, null);
		}
		else {
		    css = el.currentStyle
		}

		value = /^(.+)(px)$/.exec(css[property]);

		return {
			value: parseFloat(value[1], 10),
			suffix: value[2]
		};

	};

	//	bezier
    bezier2 = function(t, p0, p1, p2) {
		return (1-t) * (1-t) * p0 + 2 * t * (1-t) * p1 + t * t * p2;
	};

    bezier3 = function(t, p0, p1, p2, p3) {
         return Math.pow(1-t, 3) * p0 + 3 * t * Math.pow(1-t, 2) * p1 + 3 * t * t * (1-t) * p2 + t * t * t * p3;
    };

	eventLoop = function() {

        var i, fx, t, now = (new Date() - 0);

        for (i = 0; i < tweens.length; i++) {

            fx = tweens[i];
            t = now - fx.startTime;

            fx.update(t, fx.duration);

            if (typeof fx.onUpdate === 'function') {
				fx.onUpdateParams[0] = t;
                fx.onUpdate.apply(fx, fx.onUpdateParams);
            }

            if (t >= fx.duration) {

                tweens.splice(i, 1);
    
				if (typeof fx.onComplete === 'function') {
                    fx.onComplete.apply(fx, fx.onCompleteParams);
                }
            }
        }

        if (tweens.length > 0) {
            setTimeout(eventLoop, 1000/frameRate);
        }
		else {
            looping = false;
        }

	};

	self = {
		//	start an fx off
		add: function(fx) {

		    setTimeout(function() {

		        fx.startTime = (new Date() - 0);

		        if (typeof fx.onStart === 'function') {
	                fx.onStart.apply(fx, fx.onStartParams);
		        }

		        tweens.push(fx);

		        if (!looping) { 
		            looping = true;
		            eventLoop();
		        }

		    }, fx.delay);
			return fx;

		},

		//	fx
		morph: function (el, options) {

			if(options.path) {

				return this.path(el, options, true);
			}

			var fx, key, start;

			fx = {
				target: el.style,
				properties: {},
				update: function(t) {
					var p, property, value, d = this.duration;
					t = (t >= d) ? d : t;
			        for (property in this.properties) {
			            p = this.properties[property];
						value = this.easing(t, p.start, p.change, d);
			            try {
			                this.target[property] = value + this.suffix[property];
							p.current = value
			            }
						catch(e) {}
			        }

				}
			};

			setOptions(fx, options);

		    for (key in options) {
				start = getComputedStyle(el, key);
				fx.properties[key] = {
		            start: start.value,
					current: start.value,
					change: options[key] - start.value
		        };
				fx.suffix[key] = start.suffix;
		    }

			this.add(fx);

			return fx;
		},

		path: function(el, options, defineP0) {

			var fx,	x, y;

			if(defineP0) {
				x = getComputedStyle(el, "left");
				y = getComputedStyle(el, "top");

				options.path.splice(0, 0, {
					x: x.value,
					y: y.value
				});
				options.suffix = {
					left: x.suffix,
					top: y.suffix
				};
			}
			else {
				options.suffix = {
					left: "px",
					top: "px"
				};	
			}
			

			fx = {
				target: el.style,
				path: options.path
			};

			if(fx.path.length === 3) {
				fx.update = function() {
					var t = Math.min(((new Date() - 0)-this.startTime)/(this.duration), 1);
		            try {
		            	this.target.left = bezier2(t, this.path[0].x, this.path[1].x, this.path[2].x) + this.suffix.left;
		            	this.target.top = bezier2(t, this.path[0].y, this.path[1].y, this.path[2].y) + this.suffix.top;
		            }
					catch(e){};
				};
			}
			else if(fx.path.length === 4) {
				fx.update = function() {
					var t = Math.min(((new Date() - 0)-this.startTime)/(this.duration), 1);
		            try {
		            	this.target.left = bezier3(t, this.path[0].x, this.path[1].x, this.path[2].x, this.path[3].x) + this.suffix.left;
		            	this.target.top = bezier3(t, this.path[0].y, this.path[1].y, this.path[2].y,  this.path[3].y) + this.suffix.top;
		            }
					catch(e){};
				};
			}
			else {
				throw "Bezier beware"
			}

			setOptions(fx, options);

			this.add(fx);

			return fx;
		},

		throb: function(el, options, defineP0) {

			var fx,	w, h;

			if(defineP0) {
				w = getComputedStyle(el, "width");
				h = getComputedStyle(el, "height");

				options.path.splice(0, 0, {
					w: w.value,
					h: h.value
				});
				options.suffix = {
					width: w.suffix,
					height: h.suffix
				};
			}
			else {
				options.suffix = {
					width: "px",
					height: "px"
				};	
			}
			

			fx = {
				target: el.style,
				path: options.path
			};

			if(fx.path.length === 3) {
				fx.update = function() {
					var t = Math.min(((new Date() - 0)-this.startTime)/(this.duration), 1);
		            try {
		            	this.target.width = bezier2(t, this.path[0].w, this.path[1].w, this.path[2].w) + this.suffix.width;
		            	this.target.height = bezier2(t, this.path[0].h, this.path[1].h, this.path[2].h) + this.suffix.height;
		            }
					catch(e){};
				};
			}
			else if(fx.path.length === 4) {
				fx.update = function() {
					var t = Math.min(((new Date() - 0)-this.startTime)/(this.duration), 1);
		            try {
		            	this.target.width = bezier3(t, this.path[0].w, this.path[1].w, this.path[2].w, this.path[3].w) + this.suffix.width;
		            	this.target.height = bezier3(t, this.path[0].h, this.path[1].h, this.path[2].h,  this.path[3].h) + this.suffix.height;
		            }
					catch(e){};
				};
			}
			else {
				throw "Bezier beware"
			}

			setOptions(fx, options);

			this.add(fx);

			return fx;
		},


		//	easing
		linear: function(t, b, c, d) {
		    return c*t/d + b;
		},    
		easeInQuad: function(t, b, c, d) {
		    return c*(t/=d)*t + b;
		},    
		easeOutQuad: function(t, b, c, d) {
		    return -c *(t/=d)*(t-2) + b;
		},    
		easeInOutQuad: function(t, b, c, d) {
		    if((t/=d/2) < 1) return c/2*t*t + b;
		    return -c/2 *((--t)*(t-2) - 1) + b;
		},    
		easeInCubic: function(t, b, c, d) {
		    return c*(t/=d)*t*t + b;
		},    
		easeOutCubic: function(t, b, c, d) {
		    return c*((t=t/d-1)*t*t + 1) + b;
		},    
		easeInOutCubic: function(t, b, c, d) {
		    if((t/=d/2) < 1) return c/2*t*t*t + b;
		    return c/2*((t-=2)*t*t + 2) + b;
		},    
		easeOutInCubic: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutCubic(t*2, b, c/2, d);
		    return self.easeInCubic((t*2)-d, b+c/2, c/2, d);
		},    
		easeInQuart: function(t, b, c, d) {
		    return c*(t/=d)*t*t*t + b;
		},    
		easeOutQuart: function(t, b, c, d) {
		    return -c *((t=t/d-1)*t*t*t - 1) + b;
		},    
		easeInOutQuart: function(t, b, c, d) {
		    if((t/=d/2) < 1) return c/2*t*t*t*t + b;
		    return -c/2 *((t-=2)*t*t*t - 2) + b;
		},    
		easeOutInQuart: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutQuart(t*2, b, c/2, d);
		    return self.easeInQuart((t*2)-d, b+c/2, c/2, d);
		},    
		easeInQuint: function(t, b, c, d) {
		    return c*(t/=d)*t*t*t*t + b;
		},    
		easeOutQuint: function(t, b, c, d) {
		    return c*((t=t/d-1)*t*t*t*t + 1) + b;
		},    
		easeInOutQuint: function(t, b, c, d) {
		    if((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
		    return c/2*((t-=2)*t*t*t*t + 2) + b;
		},    
		easeOutInQuint: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutQuint(t*2, b, c/2, d);
		    return self.easeInQuint((t*2)-d, b+c/2, c/2, d);
		},    
		easeInSine: function(t, b, c, d) {
		    return -c * Math.cos(t/d *(Math.PI/2)) + c + b;
		},    
		easeOutSine: function(t, b, c, d) {
		    return c * Math.sin(t/d *(Math.PI/2)) + b;
		},    
		easeInOutSine: function(t, b, c, d) {
		    return -c/2 *(Math.cos(Math.PI*t/d) - 1) + b;
		},    
		easeOutInSine: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutSine(t*2, b, c/2, d);
		    return self.easeInSine((t*2)-d, b+c/2, c/2, d);
		},    
		easeInExpo: function(t, b, c, d) {
		    return(t==0) ? b : c * Math.pow(2, 10 *(t/d - 1)) + b - c * 0.001;
		},    
		easeOutExpo: function(t, b, c, d) {
		    return(t==d) ? b+c : c * 1.001 *(-Math.pow(2, -10 * t/d) + 1) + b;
		},    
		easeInOutExpo: function(t, b, c, d) {
		    if(t==0) return b;
		    if(t==d) return b+c;
		    if((t/=d/2) < 1) return c/2 * Math.pow(2, 10 *(t - 1)) + b - c * 0.0005;
		    return c/2 * 1.0005 *(-Math.pow(2, -10 * --t) + 2) + b;
		},    
		easeOutInExpo: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutExpo(t*2, b, c/2, d);
		    return self.easeInExpo((t*2)-d, b+c/2, c/2, d);
		},    
		easeInCirc: function(t, b, c, d) {
		    return -c *(Math.sqrt(1 -(t/=d)*t) - 1) + b;
		},    
		easeOutCirc: function(t, b, c, d) {
		    return c * Math.sqrt(1 -(t=t/d-1)*t) + b;
		},    
		easeInOutCirc: function(t, b, c, d) {
		    if((t/=d/2) < 1) return -c/2 *(Math.sqrt(1 - t*t) - 1) + b;
		    return c/2 *(Math.sqrt(1 -(t-=2)*t) + 1) + b;
		},    
		easeOutInCirc: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutCirc(t*2, b, c/2, d);
		    return self.easeInCirc((t*2)-d, b+c/2, c/2, d);
		},    
		easeInElastic: function(t, b, c, d, a, p) {
		    var s;
		    if(t==0) return b;  if((t/=d)==1) return b+c;  if(!p) p=d*.3;
		    if(!a || a < Math.abs(c)) { a=c; s=p/4; } else s = p/(2*Math.PI) * Math.asin(c/a);
		    return -(a*Math.pow(2,10*(t-=1)) * Math.sin((t*d-s)*(2*Math.PI)/p )) + b;
		},    
		easeOutElastic: function(t, b, c, d, a, p) {
		    var s;
		    if(t==0) return b;  if((t/=d)==1) return b+c;  if(!p) p=d*.3;
		    if(!a || a < Math.abs(c)) { a=c; s=p/4; } else s = p/(2*Math.PI) * Math.asin(c/a);
		    return(a*Math.pow(2,-10*t) * Math.sin((t*d-s)*(2*Math.PI)/p ) + c + b);
		},    
		easeInOutElastic: function(t, b, c, d, a, p) {
		    var s;
		    if(t==0) return b;  if((t/=d/2)==2) return b+c;  if(!p) p=d*(.3*1.5);
		    if(!a || a < Math.abs(c)) { a=c; s=p/4; }       else s = p/(2*Math.PI) * Math.asin(c/a);
		    if(t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin((t*d-s)*(2*Math.PI)/p )) + b;
		    return a*Math.pow(2,-10*(t-=1)) * Math.sin((t*d-s)*(2*Math.PI)/p )*.5 + c + b;
		},    
		easeOutInElastic: function(t, b, c, d, a, p) {
		    if(t < d/2) return self.easeOutElastic(t*2, b, c/2, d, a, p);
		    return self.easeInElastic((t*2)-d, b+c/2, c/2, d, a, p);
		},    
		easeInBack: function(t, b, c, d, s) {
		    if(s == undefined) s = 1.70158;
		    return c*(t/=d)*t*((s+1)*t - s) + b;
		},    
		easeOutBack: function(t, b, c, d, s) {
		    if(s == undefined) s = 1.70158;
		    return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
		},    
		easeInOutBack: function(t, b, c, d, s) {
		    if(s == undefined) s = 1.70158;
		    if((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		    return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
		},    
		easeOutInBack: function(t, b, c, d, s) {
		    if(t < d/2) return self.easeOutBack(t*2, b, c/2, d, s);
		    return self.easeInBack((t*2)-d, b+c/2, c/2, d, s);
		},    
		easeInBounce: function(t, b, c, d) {
		    return c - self.easeOutBounce(d-t, 0, c, d) + b;
		},    
		easeOutBounce: function(t, b, c, d) {
		    if((t/=d) <(1/2.75)) {
		        return c*(7.5625*t*t) + b;
		    } else if(t <(2/2.75)) {
		        return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
		    } else if(t <(2.5/2.75)) {
		        return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
		    } else {
		        return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
		    }
		},    
		easeInOutBounce: function(t, b, c, d) {
		    if(t < d/2) return self.easeInBounce(t*2, 0, c, d) * .5 + b;
		    else return self.easeOutBounce(t*2-d, 0, c, d) * .5 + c*.5 + b;
		},    
		easeOutInBounce: function(t, b, c, d) {
		    if(t < d/2) return self.easeOutBounce(t*2, b, c/2, d);
		    return self.easeInBounce((t*2)-d, b+c/2, c/2, d);
		}


	};

	return self;
	
}();



var Group = new Class({
	initialize: function(){
		this.instances = Array.flatten(arguments);
		this.events = {};
		this.checker = {};
	},
	addEvent: function(type, fn){
		this.checker[type] = this.checker[type] || {};
		this.events[type] = this.events[type] || [];
		if (this.events[type].contains(fn)) return false;
		else this.events[type].push(fn);
		this.instances.each(function(instance, i){
			instance.addEvent(type, this.check.bind(this, [type, instance, i]));
		}, this);
		return this;
	},
	check: function(type, instance, i){
		
		//	Google Chrome has it's own Function.prototype.bind which doesn't accept the sam earguments as Moo's.
		//	Hence we need to test the value of type and force it to be what we want if it's incorrect
		if($type(type) === "array") {
			i = type[2];
			type = type[0];
		}

		this.checker[type][i] = true;
		var every = this.instances.every(function(current, j){
			return this.checker[type][j] || false;
		}, this);
		if (!every) return;
		this.checker[type] = {};
		this.events[type].each(function(event){
			event.call(this, this.instances, instance);
		}, this);
	}
});


/*
	mootools more event delegation functions
	extended to be a bit more user friendly.

	to fire the click event on all href's with class linky that are decendents of element with id
	
	$("someId").on("click", "a.linky", function(){});
	
	this will return a listener object and NOT the mootools extended element.
	
	listener = $("someId").on("click", "a.linky", function(){});
	
	to stop/remove the listener
	
	listener.stop();
	
	to add/restart
	
	listener.add();
	
	
*/
(function(addEvent, removeEvent){

	var match = /(.*?):relay\(([^)]+)\)$/,
		combinators = /[+>~\s]/,
		splitType = function(type){
			var bits = type.match(match);
			return !bits ? {event: type} : {
				event: bits[1],
				selector: bits[2]
			};
		},
		check = function(e, selector){
			var t = e.target;
			if (combinators.test(selector = selector.trim())){
				var els = this.getElements(selector);
				for (var i = els.length; i--; ){
					var el = els[i];
					if (t == el || el.hasChild(t)) return el;
				}
			} else {
				for ( ; t && t != this; t = t.parentNode){
					if (Element.match(t, selector)) return document.id(t);
				}
			}
			return null;
		};

	Element.implement({

		addEvent: function(type, fn){

			var splitted = splitType(type);
			if (splitted.selector){
				var monitors = this.retrieve('$moo:delegateMonitors', {});
				if (!monitors[type]){
					var monitor = function(e){
						var el = check.call(this, e, splitted.selector);
						if (el) this.fireEvent(type, [e, el], 0, el);
					}.bind(this);
					monitors[type] = monitor;
					addEvent.call(this, splitted.event, monitor);
				}
			}
			return addEvent.apply(this, arguments);
		},

		removeEvent: function(type, fn){
			var splitted = splitType(type);
			if (splitted.selector){
				var events = this.retrieve('events');
				if (!events || !events[type] || (fn && !events[type].keys.contains(fn))) return this;

				if (fn) removeEvent.apply(this, [type, fn]);
				else removeEvent.apply(this, type);

				events = this.retrieve('events');
				if (events && events[type] && events[type].keys.length == 0){
					var monitors = this.retrieve('$moo:delegateMonitors', {});
					removeEvent.apply(this, [splitted.event, monitors[type]]);
					delete monitors[type];
				}
				return this;
			}
			return removeEvent.apply(this, arguments);
		},

		fireEvent: function(type, args, delay, bind){
			var events = this.retrieve('events');
			if (!events || !events[type]) return this;
			events[type].keys.each(function(fn){
				fn.create({bind: bind || this, delay: delay, arguments: args})();
			}, this);
			return this;
		},
		
		on: function(type, selector, fn) {

			var self = this, ev = type + ":relay(" + selector + ")";
			
			this.addEvent(ev, fn);

			return {
				start: function() {
					self.addEvent(ev, fn);
				},
				stop: function() {
					self.removeEvent(ev, fn);
				}
			}
		}

	});

})(Element.prototype.addEvent, Element.prototype.removeEvent);


/*
*	@name: 			RF.onThese
*	@description: 	Test to see if event is supported
*
*/
RF.isEventSupported = (function(){
	var tagNames = {
		"select":"input","change":"input",
		"submit":"form","reset":"form",
		"error":"img","load":"img","abort":"img"
	};
	return function (eventName) {
		var el = document.createElement(tagNames[eventName] || "div");
		eventName = "on" + eventName;
		var isSupported = (eventName in el);
		if (!isSupported) {
			el.setAttribute(eventName, "return;");
			isSupported = typeof el[eventName] === "function";
		}
		el = null;
		return isSupported;
	};
})();


/*
*	@name: 			RF.onThese
*	@description: 	Fires the callabck when all the deferred have fired
*
*/
RF.onThese = function() {
	var i = 0, l = arguments.length - 2, func, f, args = $splat(arguments);
	func = arguments[arguments.length - 1];

	f = function() {
		args[i++].addCallbacks(function(data) {
			if(i <= l) {
				f();
			}
			else {
				func(data);
			}
			return data;
		},
		function(e) {
			return e;
		});
	};
	f();

};


/*
*	@name: 			RF.Class.Deferred
*	@description: 	Callback API to handle asynchronous loading
*						
*
*/
RF.Class.Deferred = new Class({

	initialize: function() {
		this.status = 0;
		this.stack = [];
		this.result = false;
	},

	addCallbacks: function(callback, errback){
		this.stack.push([callback, errback]);
		if (this.status === 1) {
			this.fire();
		}
	},
	
	fire: function(res){

		if (this.status === -1) {
			return;
		}
		this.status = 1;

		res = res || this.result;

		var stack = this.stack, result = this.result;

		while (stack.length > 0) {
			(function(){
				var f = stack.shift()[res instanceof Error ? 1 : 0];
				if (!f) {
					return;
				}
				res = f(res) || result;
			})();
		}
		this.result = res;

	},
	cancel: function(){
		this.status = -1;
	}

});



/*
*	@name: 			Events
*	@description: 	Razorfish events Class that acts as a message passing bridge to allow different ojects to comunicate with each other.
*						
*
*/
RF.Class.Events = function() {
	
	var stack = {}, id = 0, debug;

	debug = (function() {

		var i, l,
			qs = window.location.search.substring(1),
			kv = qs.split("&");
		for (i = 0, l = kv.length; i < l; i++) {
			if (kv[i] === "debug=true") {
				return true;
			}
		}
		return false
	})();
	
	return new Class({
		
		addListener: function(event, listener) {

			var guid = "__" + (id++), self = this;

			if(!stack[event]) {
				stack[event] = {};
			}
			stack[event][guid] = function(data){
				listener.apply(self, [data]);
			}
			
			return {

				start: function() {
					stack[event][guid] = function(data){
						listener.apply(self, [data]);
					}
				},
				
				stop: function() {
					delete stack[event][guid];
				}
			}
		},
		
		fire: function(event, data) {

			if(debug) {
				console.log(event, data);
			}
			
			var i, listeners = stack[event];

			if(typeof listeners === "undefined") {
				return false;
			}

			for(i in listeners) (function() {
				listeners[i](data);
			})();
			
			
		}
		
	});
	
}();



/*
 *	Array: Convenience methods. 
 *	This is implemented in a 'safe' way for Element collections as well, where getPrevious and getNext are Element matches
 */
Array.implement({
	getIndex: function(currentItemIndex,direction) {
		/*
		*	currentItemIndex: zero-based index integer
		*	direction: positive/negative integer
		*/
		if($defined(this[currentItemIndex+direction])) {
			return currentItemIndex+direction;
		} else {
			return direction>0 ? 0 : this.indexOf(this.getLast());
		}
	},
	getNextIndex: function(currentItemIndex) {
		return $defined(this[currentItemIndex+1]) ? currentItemIndex+1 : 0;
	},
	getPreviousIndex: function(currentItemIndex) {
		return $defined(this[currentItemIndex-1]) ? currentItemIndex-1 : this.indexOf(this.getLast());
	},
	getNextItem: function(currentItem) {
		return $defined(this[currentItemIndex+1]) ? this[currentItemIndex+1] : this[0];
	},
	getPreviousItem: function(currentItem) {
		return $defined(this[currentItemIndex-1]) ? this[currentItemIndex-1] : this[this.indexOf(this.getLast())];
	}
});

RF.Class.Utilities = new Class({
	Implements:[Options,Log,Chain],
	options:{
		imageResetData : 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7', // image GIF binary data code [String]
		viewport : {
			width:null,
			height:null,
			iphone:{
				portrait:{
					width:980,
					height:1091					
				},
				landscape:{
					width:980,
					height:425				
				}
			},
			ipad:{
				portrait:{
					width:980,
					height:1208					
				},
				landscape:{
					width:980,
					height:660				
				}
			}
		}
	},
	ArrayBasics: {
		array: [],
		addItems:function(items){
			$$(items).each(function(item){this.array.include($(item));}, this);
		},
		addItem:function(item){
			this.addItems($splat($(item)));
		}
	},
	ClassBasics: {
		addClassToAll: function(items, css){
			items.each(function(i){
				i.addClass(css);
			});
		},
		clearClassFromAll: function(items, css){
			items.each(function(i){
				i.removeClass(css);
			});
		},
		swapClassFromAll:function(items, removecss, addcss){
			// requires moo more
			items.each(function(i){
				i.swapClass(removecss, addcss);
			});	
		}
	},
	EffectsHelper: {
		Overlay: function(overlayObject){
			this.overlay = new Element('div',{
				'id':'RF_Overlay_'+new Date().getTime(),
				'class': ($defined(overlayObject.cssClass) ? overlayObject.cssClass : ''),
				'styles':{
                    'opacity':0,
                    'visibility':'hidden'
                }
			}).inject(document.body,'top');
			this.overlay.style.cssText = this.FullScreen();
			return this.overlay;
		},
		FullScreen: function() {
	        var style = [];
			if(RF.Browser.ie6) {
                style.push('position:absolute');
                style.push('left   : expression((ignoreMe = document.documentElement.scrollLeft   ? document.documentElement.scrollLeft   : document.body.scrollLeft))');
                style.push('top    : expression((ignoreMe = document.documentElement.scrollTop    ? document.documentElement.scrollTop    : document.body.scrollTop))');
                style.push('width  : expression((ignoreMe = document.documentElement.clientWidth  ? document.documentElement.clientWidth  : window.innerWidth))');
                style.push('height : expression((ignoreMe = document.documentElement.clientHeight ? document.documentElement.clientHeight : window.innerHeight))');
            } else {
                style.push('position:fixed');
                style.push('left:0');
                style.push('top:0');
                style.push('width:100%');
                style.push('height:100%');
            }
            return style.join(';');
		}
	},
	ErrorHelper: function(errorObject) {
		/* Object Schema
		* errorObject : {
		* 	method:"MyClass : myFunction()", // string to help you identify which js file and function caused the error
		* 	error:e // the captured error object
		* }
		*/

		if (errorObject.error instanceof TypeError) {
			if(errorObject.method) {
				this.enableLog().log("JS ERROR : "+errorObject.method+" : TypeError; the type of a variable is not as expected : "+errorObject.error.message);
			}
		} else if (errorObject.error instanceof RangeError) {
			if(errorObject.method) {
				this.enableLog().log("JS ERROR : "+errorObject.method+" : RangeError; numeric variable has exceeded its allowed range : "+errorObject.error.message);
			}
		} else if (errorObject.error instanceof SyntaxError) {
			if(errorObject.method) {
				this.enableLog().log("JS ERROR : "+errorObject.method+" : SyntaxError; syntax error occurred while parsing : "+errorObject.error.message);
			}
		} else if (errorObject.error instanceof ReferenceError) {
			if(errorObject.method) {
				this.enableLog().log("JS ERROR : "+errorObject.method+" : ReferenceError; invalid reference used : "+errorObject.error.message);
			}
		} else {
			if(errorObject.method) {
				this.enableLog().log("JS ERROR : "+errorObject.method+" : Unidentified Error Type : "+errorObject.error.message);
			}
		}
	},
	RegexHelper: {
		replacer: function(expression,startValue,replacementValue) {
			return startValue.replace(new RegExp(expression,"i"), replacementValue);
		}
	},
	createSWFCloseButton: function(object) {
		this.closeButton = new Element('a',{
			'class': object.cssClass,
			'title': object.title,
			'tabIndex': object.tabIndex
		}).addEvents({
			'click': function(event) {if(event) object.eventFunction.pass(null,this)()},
			'keydown': function(event) {if(event && event.keyCode == 13) object.eventFunction.pass(null,this)()}
		});
		return this.closeButton;
	},
	scrollToTopAndRunFunction: function() {
		this.functionArg = arguments[0];
		if(window.getScroll && window.getScroll().y > 75) {
			new Fx.Scroll(window).toTop().chain(function(){
				this.functionArg();
				delete this.functionArg;
			}.bind(this));
		} else {
			this.functionArg();
			delete this.functionArg;
		}
	},
	existsContainerForInjection: function(elementType, id, parentId) {
		if($defined(document.id(id))) {
			return document.id(id);
		} else {
			return new Element(elementType,{'id':id}).inject(document.id(parentId));
		}
	},
	
	cleanUpMemory: function() {
		/*
		*	@description:
		*		iOS4 (iPod, iPhone & iPad) hog memory, typically with a 5Mb download limit in Safari Webkit, particularly noticeable
		*		if you have large filesize images
		*		Using this technique we can free up almost all available memory per Safari instance
		*		This is likely to leave a 43byte footprint for each 1x1 pixel GIF we create, which is minimal
		*	@arguments:
		*		arguments[0]: Element or array of Elements
		*/
		$each($splat(arguments[0]).flatten(), function(element) {
			this.destroyFromMemory(element);
		},this);	
	},
	destroyFromMemory: function() {
		/*
		*	@arguments:
		*		arguments[0]: Element
		*/
		/*
		*	If the nodeName of the element is an image, then set it's src to a 1x1 pixel GIF data binary value (see options of RF.Class.Utilities)
		*	first, before using Moo's destory() method to clean it from the DOM
		*/
		if(arguments[0].nodeName && arguments[0].nodeName=='IMG') arguments[0].src=this.options.imageResetData;
		arguments[0].destroy();
	},
	evaluateViewport: function() {
		/*
		*	On load, and before any orientation change, we need to detect the viewport dimensions
		*	This is so that we can provide device specific styling
		*/
		if (typeof window.innerWidth != 'undefined' && Browser.Platform.ipod) {
			this.options.viewport.width = window.innerWidth;
		    this.options.viewport.height = window.innerHeight;
			if(this.options.viewport.height == this.options.viewport.iphone.portrait.height || this.options.viewport.height == this.options.viewport.iphone.landscape.height) {
				document.getElement('html').addClass('iphone');
			}
			else if(this.options.viewport.height >= this.options.viewport.ipad.portrait.height || this.options.viewport.height >= this.options.viewport.ipad.landscape.height) {
				document.getElement('html').addClass('ipad');
			}
		}		
	},
	
    fixTransparentPngsInIE: function(){
		/* IE7 and 8 don't fade tranparent pngs content well: it loses the alpha channel and defaults to black until the opacity is 100% or 0% 
		This is a known IE bug. 
		The simplest fix is to apply a solid background colour to the image/parent div; i'm adding to the image
		If this isn't possible, then apply the AlphaLoader fix, this will reduce the black, but won't necessarily remove it entirely */
		if (window.XMLHttpRequest && document.all) {
			// test for IE7 & 8
			this.pngs = $$('img.png');
			// use the AlphaLoader fix suggested by http://www.snutt.net/testzon/ie_alpha/
			this.pngs.each(function(p){
                if (p.hasClass('png')) return;
                p.setStyles({
					'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\''+p.getProperty('src')+'\')'	
				});
			}, this);
		}	
	},
	getUrlVar : function (name, source){
	    source = source || window.location.href;
	    var query = new RegExp("[\\?&]" + name + "=([^&#]*)").exec(source);
	    if (query)
	        return query[1];
	    else
	        return null;
	}

});


/*
*	ACOUK-3409: Detect QuickTime
*	Browser.Plugins.QuickTime.isInstalled will return true if it's installed
*/
Browser.Plugins.QuickTime = {
	isInstalled:false,
	version:0
};

if (window.ActiveXObject) {
	var qtObject = null;
	/*
	*	Internet Explorer
	*/
	try {
		qtObject = new ActiveXObject('QuickTime.QuickTime');
		if(qtObject) {
			// In case QuickTimeCheckObject.QuickTimeCheck does not exist
			Browser.Plugins.QuickTime.isInstalled = true;
		}
	} catch (e) {
    	/*
 		* do not throw error
		*/
	}
	try {
		qtObject = new ActiveXObject('QuickTimeCheckObject.QuickTimeCheck');
	    // This generates a user prompt in Internet Explorer 7    
		if(qtObject) {
		   	// In case QuickTime.QuickTime does not exist
			Browser.Plugins.QuickTime.isInstalled = true;

		    // Get version
			
		    var qtVersion = qtObject.QuickTimeVersion.toString(16); // Convert to hex
			// Get the version major release
		    qtVersion = qtVersion.substring(0, 1);
		    Browser.Plugins.QuickTime.version = parseInt(qtVersion);
		}
	} catch (e) {
		/*
 		* do not throw error
		*/
	}	
} else if(navigator.mimeTypes && navigator.mimeTypes.length > 0) {
	/*
	*	Mozilla, Safari, Chrome
	*/

	/*
	* iOS Safari does not recognise 'enabledPlugin.version', only .name is available
	*/
	if(navigator.mimeTypes["video/quicktime"] && navigator.mimeTypes["video/quicktime"].enabledPlugin != null) {
		Browser.Plugins.QuickTime = {
			isInstalled:true,
			version:navigator.mimeTypes["video/quicktime"].enabledPlugin.name.match(/\d+/g) ? parseInt(navigator.mimeTypes["video/quicktime"].enabledPlugin.name.match(/\d+/g)[0]) : 0,
			revision:navigator.mimeTypes["video/quicktime"].enabledPlugin.name
		}
	}
}

RF.isEventSupported = (function(){
    var tagNames = {
        "select":"input","change":"input",
        "submit":"form","reset":"form",
        "error":"img","load":"img","abort":"img"
    };
    return function (eventName) {
        var el = document.createElement(tagNames[eventName] || "div");
        eventName = "on" + eventName;
        var isSupported = (eventName in el);
        if (!isSupported) {
            el.setAttribute(eventName, "return;");
            isSupported = typeof el[eventName] === "function";
        }
        el = null;
        return isSupported;
    };
})();

})(document.id);
