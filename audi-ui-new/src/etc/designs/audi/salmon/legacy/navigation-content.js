(function($){

RF.Class.NavigationContent = new Class({
	Implements: [RF.Class.Utilities, Events, Options],
	options: {
		tabLink: false,
		flyoutVar: false,
		contentArea: false
	},
	initialize:function(options){
		try {
			this.setOptions(options);
			this.tab = this.options.tabLink; if(!(this.tab)) return;
			this.flyout = this.options.flyoutVar; if(!(this.flyout)) return;
			this.content = document.id(this.options.contentArea); if(!(this.content)) return;
			this.submenulinks = '#' + this.options.contentArea + ' a';
		} catch(e) {
			this.ErrorHelper({method:"NavContent Class : initialize()",error:e});
		}
	}
});

})(document.id);