(function($){

/*	Project: Audi.co.uk

* 	Name: NavigationPanelManager Class
* 	Version: v1.1
* 	Author: Razorfish London
*/
RF.Class.NavigationPanelManager = new Class({
	Implements: [Options,Events,RF.Class.Utilities, RF.Class.Events],
	options:{
		navPanels:'#navigation-panels div[id^=nav_panel_][class*=panel]',
		navPanelContainer:'#navigation-panels div.nav-panel',
		navPanelResourcePath:'/content/audi/new-cars/',
		navPanelFilePath:'.render.html',
		range:null,				// The Range name [String]
		model:null,				// The Model name [String]
		navPanelTween:{
			property:'opacity',
			duration:500
		},
		navPanelCallback:null,	// callback function to run after hiding the Nav Panels [Function]
		xhrURL:'/content/audi/utilities/json/nav-panel-events.html',
		xhrParam1:'?range=',
		xhrParam2:'&model=',
		xhrParam4:'&panels=true'
	},
	initialize: function(options){
		this.setOptions(options);
		this.enableLog();
		if(!$defined(document.getElement(this.options.navPanelContainer))) return;

		// Preload hover images for nav list items
		(new Image).src = '/etc/designs/audi/img/bgs/li-dark-grey-arrow.gif';
	},
	requestNavPanelsByRangeAndModel: function(options) {
		/*
		*	@description:
		*		Request a new set of Nav Panels by Range & Model
		*	@arguments:
		*		options[Object]
		*			options.range: Range [String]
		*			options.model: Model [String]
		*/
		this.setOptions(options);
		
		/*
		*	If the Navigation Panels do not exist, return
		*/
		if(!$defined(document.getElement(this.options.navPanelContainer))) return;
		
		document.getElement(this.options.navPanelContainer).get('tween',this.options.navPanelTween)
		.addEvent('complete', function() {
			/*
			*	Clean up the memory from images that are now being disposed of
			*/
			if(RF.Init.NavigationPanel) {
				$each(RF.Init.NavigationPanel.navPanelImages, function(img) {
					img.removeEvents();
				},this);
				this.cleanUpMemory(RF.Init.NavigationPanel.navPanelImages);
			}

			/*
			*	Request a new set of Nav Panels
			*/
			this.requestNavPanels();
		}.bind(this)).start(0);
	},
	requestNavPanels: function() {
		/*
		*	Request the nav panels
		*/
		this.request = new Request.HTML({
			url: 		this.options.xhrURL+this.options.xhrParam1+this.options.range+this.options.xhrParam2+this.options.model+this.options.xhrParam4,
			method: 	'GET',
			update: 	document.getElement(this.options.navPanelContainer),
			onRequest: 	null,
			onSuccess: 	this.navPanelSuccessCallback.bind(this),
			onFailure: 	this.navPanelFailureCallback.bind(this)
		}).send();
	},
	navPanelSuccessCallback: function() {
		this.onNavPanelLoad();
	},
	navPanelFailureCallback: function(xhr) {
		/*
		*	Operation: On failure, the original nav panels should fade back in, but report the failure
		*/
		$defined(xhr.message) ? xhr = xhr : xhr.message="XHR failure";
		this.ErrorHelper({method:"RF.Class.NavigationPanelsManager : failureCallback()",error:xhr});
	},
	onNavPanelLoad: function() {
		/**
		 * Initialise new kickup instances
		 */
	    RF.Init.kickups = {};
	    $$('.kickup').each(function ($el) {
	        RF.Init.kickups[$el.id] = new RF.Class.Kickup($el, RF.Browser.ie ? 'hover' : 'click');
	    });
		/*
		*	Show the Nav Panels
		*/
		this.fire("displayNavPanels");
		this.showNavPanels();
	},
	
	showNavPanels: function() {
		document.getElement(this.options.navPanelContainer).get('tween',this.options.navPanelTween).start(1);
	}

	/* ASK ST
	showNavPanels: function() {
		//	ExternalInterface API method call, from a previously instatiated instance of this class
		(function() {
			$$(this.options.navPanels).each(function(navPanel,index){
				(RF.Browser.ie ? navPanel : navPanel.getParent()).get('tween',{property:'opacity',duration:300}).start(1);
			},this);
		}.bind(this)).delay(500);
	}
	*/
	
});

})(document.id);
