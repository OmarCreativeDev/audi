(function($){

RF.Class.MeetTheRangePromo = new Class({

	html: null,
	Implements:[RF.Class.Utilities],
	makePromoRequest:function(promourl){
		try {
			if(this.html != null){
				this.getContent();
			}else{
				if(this.options.promourl != null){
					this.req = new Request.HTML({
						method:'get',
						url: this.options.promourl,
						onSuccess: function(tree, elements, html, js){
							this.html = html;
							this.getContent();
						}.bind(this)	
					}).send();
				}				
			}
		} catch(e) {
			this.ErrorHelper({method:"MeetTheRangePromo Class : makePromoRequest()",error:e});
		}
	},
	getContent:function(){
		try {
			if(this.options.promourl != null) this.promoarea.set('html',this.html);
		} catch(e) {
			this.ErrorHelper({method:"MeetTheRangePromo Class : getContent()",error:e});
		}
	}
});

})(document.id);
