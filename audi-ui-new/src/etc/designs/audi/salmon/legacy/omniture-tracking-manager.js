(function($){

/*	Project: Audi.co.uk
* 	Name: OmnitureTrackingManager Class
* 	Version: v1.4
* 	Author: Razorfish London
*/
RF.Class.OmnitureTrackingManager = new Class({
	Implements:[Options,RF.Class.Utilities],
	options:{
		eventListeners:{
			elements:null			
		},
		singleAction:{
			callToAction:null,
			eVars:null,
			events:null,
			props:null,
			linkDepth:null,
			linkType:'o'
		}
	},
	initialize: function(options){
		try {
			this.enableLog();
			this.setOptions(options);
			if($defined(this.options.eventListeners.elements)) this.processElements()
			if($defined(this.options.singleAction.eVars) || $defined(this.options.singleAction.events) || $defined(this.options.singleAction.props)) {
				this.options.singleAction.eVarsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(this.options.singleAction.eVars);
				this.options.singleAction.eventsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(this.options.singleAction.events);
				this.options.singleAction.propsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(this.options.singleAction.props);
				this.options.singleAction.propsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(this.options.singleAction.linkDepth);

				this.track.pass(this.options.singleAction,this)();
			}
		} catch(e) {
			this.ErrorHelper({method:"RF.Class.OmnitureTrackingManager : initialize()",error:e});
		}
	},
	processElements: function() {
		$$(this.options.eventListeners.elements).each(function(element,index) {
			this.elementOmnitureObject={};

			this.elementOmnitureObject.linkDepth = element.getProperty("data-link-position");
			this.elementOmnitureObject.title = (element.getProperty("title") || element.getProperty("value") || element.getProperty("alt") || "Value unavailable");
			/*
			*	Remove any potential link title clutter
			*/
			this.elementOmnitureObject.title = this.elementOmnitureObject.title.replace(": Opens in a new window", "");
			this.elementOmnitureObject.title = this.elementOmnitureObject.title.replace(" PDF document.", "");
			this.elementOmnitureObject.title = this.elementOmnitureObject.title.replace("Audi ", "");
			/*
			*	Store the linkType for processed HTML elements that are PDF documents
			*/
			if(element.getProperty('href') && element.getProperty('href') != "") {
				this.elementOmnitureObject.linkType = element.getProperty('href').test("/.pdf/","gi") ? 'd' : 'o';
			}
			
			element.getProperty('class').split(' ').each(function(className){
				if($H(RF.Tracking.ObjectOmnitureTracking).has(className)) {
					if(className.contains('eVar')) {
						this.elementOmnitureObject.eVars = className;
						this.elementOmnitureObject.eVarsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(className);
					} else if(className.contains('event')) {
						this.elementOmnitureObject.events = className;
						this.elementOmnitureObject.eventsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(className);
					} else if(className.contains('prop')) {
						this.elementOmnitureObject.props = className;
						this.elementOmnitureObject.propsFunction = $H(RF.Tracking.ObjectOmnitureTracking).get(className);
					}
				}
			},this);

			element.store("OmnitureObject",this.elementOmnitureObject);
			
			if($defined(element.retrieve("OmnitureObject"))) {
				element.addEvents({
					'click': function(event) {
						this.track.pass([element.retrieve("OmnitureObject"),element],this)();
					}.bind(this),
					'keydown': function(event) {
						if(event && event.type=='keydown' && event.keyCode == 13) {
							this.track.pass([element.retrieve("OmnitureObject"),element],this)();
						}
					}.bind(this)
				});
			}
		},this);
		delete this.elementOmnitureObject;
	},
	track: function() {
		/*
		*	singleAction (in-page code)
		*	arguments[0]: Object
		*		.eVars: String (the eVar name)
		*		.eVarsfunction: Function
		*		.events: String (the event name)
		*		.eventsFunction: Function
		*		.props: String (the prop name)
		*		.propsFunction: Function
		*		.callToAction: String OR arguments[1]: Element (with OmnitureObject in Hash store)
		*		.linkType: String - 'd' for custom download or 'o' for custom link
		*
		*	OR
		*	
		*	eventListeners (target)
		*	arguments[0]: target.retrieve("OmnitureObject")
		*		.eVars: String (the eVar name)
		*		.eVarsfunction: Function
		*		.events: String (the event name)
		*		.eventsFunction: Function
		*		.props: String (the prop name)
		*		.propsFunction: Function
		*		.title: The element title attribute
		*	arguments[1]: target
		*/
		var s=s_gi(s_account); // set Omniture account
		
		/* Call any props tracking function */
		if(arguments[0].propsFunction) {
			arguments[0].propsFunction.pass(arguments[0].callToAction||arguments[1])(); // Pass either the callToAction if there is one, or the element itself
			this.linkTrackVarsProps = arguments[0].props;
		} else {
			this.linkTrackVarsProps = "";
		}
		
		/* Check to see if we have ANY event, and if so add the 'events' suffix to the linkTrackVars */
		if(arguments[0].eventsFunction) {
			this.linkTrackVarsEvents=",events";
		} else {
			this.linkTrackVarsEvents = "";
		}
		
		/* Call any eVars tracking function */
		if(arguments[0].eVarsFunction) {
			/*
			*	Pass the function either:
			*		The callToAction, sent by a singleAction request
			*		Or the Title attribue, sent by a processed HTML element event
			*		Or the whole HTML Event Target, sent by a processed HTML element event
			*/
			arguments[0].eVarsFunction.pass(arguments[0].callToAction||arguments[0].title||arguments[1])();
			
			/* Additionally, if any events tracking function exists we have to declare 'events' in linkTrackVars */
			this.linkTrackVarsEVars=(this.linkTrackVarsProps!='' ? "," : "")+arguments[0].eVars;
		} else {
			this.linkTrackVarsEVars="";
		}
		
		/* Write out linkTrackVars statement: if eVars, or props, or events exist : else 'None' is the default */
		s.linkTrackVars=(this.linkTrackVarsProps!='' || this.linkTrackVarsEVars!='' || this.linkTrackVarsEvents!='') ? (this.linkTrackVarsProps+this.linkTrackVarsEVars+this.linkTrackVarsEvents) : 'None';
		//this.log("s.linkTrackVars("+s.linkTrackVars+")");
		
		/* Call any events tracking function */
		if(arguments[0].events) {
			arguments[0].eventsFunction.pass(arguments[0].callToAction||arguments[1])(); // Pass either the callToAction if there is one, or the element itself
			s.linkTrackEvents=arguments[0].events;
		} else {
			s.linkTrackEvents="None";
		}
		//this.log("s.linkTrackEvents(\'"+s.linkTrackEvents+"\')");
		/* Track:
		*	singleAction:
		*		arg1[Target Element]: null - there is no target element, as we've triggered this using 'in-page' code
		*		arg2[Link Type]: 'o' - Omniture's Custom Link property is the option to go for (others are 'd' for download or 'e' for exit link)
		*		arg3[Link Title]: arguments[0].callToAction
		*	eventListeners:
		*		arg1[Target Element]: arguments[1] - the target element (Anchor link)
		*		arg2[Link Type]: 'o' - Omniture's Custom Link property is the option to go for (others are 'd' for download or 'e' for exit link)
		*		arg3[Link Title]: arguments[1].retrieve("OmnitureObject").title - link title stored in the element's properties Hash
		*/
		this.trackTarget = $defined(arguments[1]) ? arguments[1] : true; // boolean true prevents 500ms delay for image request, and seems a sensible alternative to not having a 'this' object
		this.trackTitle = $defined(arguments[1]) ? arguments[1].retrieve("OmnitureObject").title : arguments[0].callToAction;
		
		/*
		*	Set the default linkType to 'o' for singleAction tracking where no linkType was specified.
		*	Processed HTML elements should have the linkType set already
		*/
		s.tl(this.trackTarget,(arguments[0].linkType||'o'),this.trackTitle);
		
		//this.log("s.tl("+this.trackTarget+","+(arguments[0].linkType||'o')+","+this.trackTitle+");");
	}
});

window.addEvents({
	'load': function() {
		RF.Tracking.OmnitureTrackingManager = new RF.Class.OmnitureTrackingManager({
			eventListeners:{
				elements:'a[class*=eVar],a[class*=event],a[class*=prop],input[type=submit]'
			}
		});
	}
});

})(document.id);