/* Audi.co.uk
*  rf.namespace.js
*  Razorfish Namespace javascript
*  Author: Razorfish, London
*  Date: 11-11-2009 */

var RF = 			{}; // Our RF empty namespace
RF.Moo = 			{}; // Any additional Moo specific objects
RF.Browser = 		{}; // All browser specific objects
RF.Browser.ie, RF.Browser.ie6 = false;

/*@cc_on
	@if (@_jscript_version >= 5.0)
		RF.Browser.ie = true;
		
   	@end
@*/
// Test for SP3 IE6
RF.Browser.ie   = /msie/i.test(navigator.userAgent);
RF.Browser.ie6  = /msie 6/i.test(navigator.userAgent);
RF.Browser.ie7  = /msie 7/i.test(navigator.userAgent);
RF.Browser.ie8  = /msie 8/i.test(navigator.userAgent);
// Test for iPad
RF.Browser.iPad = navigator.userAgent.match(/iPad/i) != null;

RF.Global = 		{}; // Global sitewide objects
RF.SWF = 			{}; // All SWF related objects
RF.SWF.Showroom = 	{}; // The Showroom object
RF.Tracking = 		{}; // All tracking and analytics related objects
RF.Tracking.Showroom = {}; // Tracking and analytics for the Showroom & Visualizer
RF.Experience = 	{}; // All RF experience related objects, e.g. journeyType (Gold / Bronze)
RF.Site = 			{}; // All site specific objects, e.g. currentPage
RF.Class = 			{}; // All RF Classes
RF.Class.Showroom = {}; // All Showroom Classes
RF.Class.Audi = 	{};	//	All Audi visualiser classes
RF.Init = 			{}; // instantiate classes in here
RF.Init.API =		{}; // instantiate classes from flash/javascript interface
RF.Campaign = 		{};	//	All campaign code that does not need to be a class

var SI_CBW = {}; // STYLING FILE INPUTS 1.0 | Shaun Inman <http://www.shauninman.com/> | 2007-09-07 - updated by ChrisBWard