RF.Class.ClearLabel = new Class({
	Implements : Options,
  options : {
    field : 'input.text'
  },
	initialize : function(options) {
    this.setOptions(options);
    this.elements = document.getElements(this.options.field);
    if (this.elements.length > 0) this.addEvents();
  },
    addEvents : function() {
        for (var i=0,field; field=this.elements[i]; i++) {
            field.addEvents({
                'focus' : function() {
                    if (this.value == this.defaultValue) this.value = '';
                },
                'blur' : function() {
                    if (this.value == '') this.value = this.defaultValue;
                }
            });	
            field.getParents('form').addEvent('submit',function() {
                if (this.value == this.defaultValue) this.value = '';
            }.bind(field));
        }
    }
});
window.addEvent('domready',function() {
    RF.Init.UCLHomepage = new RF.Class.ClearLabel({
        field : '.action-ucl-promo input.text'
    });
});
