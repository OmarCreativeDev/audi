var mnStatus = {taborder:[], currtab:0};
RF.Class.MainNavigation = new Class({
	Extends: RF.Class.NavigationContent,
	options:{
		model: false
	},
	Implements: [RF.Class.Utilities, Events, Options],
	Binds:['clickEvent','touchEventAppleMobile'],
	initialize:function(options){
		try {
			this.parent(options);
			this.model = this.options.model; if(!(this.model)) return;
			this.callEvents();
			mnStatus.taborder.push(this.tab);
		} catch(e) {
			this.ErrorHelper({method:"MainNavigation Class : initialize()",error:e});
		}
	},
	html: function(){
		var getCols = $H(this.model);
		var colsStr = '';
		var newwin = ' title="Opens in a new window" target="_blank"';
		
		getCols.each(function(value, key){
			if(key != 'promo'){
				colsStr += '<div class="col"><ul>';
				value.each(function(e){
					
					if(e.newwindow == 'true'){
						(e.duplicateparent == 'true') ?  colsStr += '<li class="duplicate-parent"><a class="prop30" '+ newwin +' href="'+ e.href +'" title="'+e.listpoint+'">'+ e.listpoint +'</a>' : colsStr += '<li><a class="prop30" '+ newwin +' href="'+ e.href +'" title="'+e.listpoint+'">'+ e.listpoint +'</a>';
					}else{
						(e.duplicateparent == 'true') ?  colsStr += '<li class="duplicate-parent"><a class="prop30" href="'+ e.href +'" title="'+e.listpoint+'">'+ e.listpoint +'</a>' : colsStr += '<li><a class="prop30" href="'+ e.href +'" title="'+e.listpoint+'">'+ e.listpoint +'</a>';	
					}

					if(e.sublist){
						colsStr += '<ul>';
						e.sublist.each(function(s){
							(s.newwindow == 'true') ? 	colsStr += '<li><a class="prop30" '+ newwin +' href="'+ s.href +'" target="_blank" title="'+s.listpoint+'">'+ s.listpoint +'</a></li>' : 
								colsStr += '<li><a class="prop30" href="'+ s.href +'" title="'+s.listpoint+'">'+ s.listpoint +'</a></li>';
						});
						colsStr += '</ul>';
					}

					colsStr += '</li>';
				});
				colsStr += '</ul></div>';
			}	
		});

		// create promo div and put in place
		this.content.empty();
		
		var promoStr = '<h2><img src="'+ this.model.promo.h2img +'" alt="'+ this.model.promo.ctacopy +'"/><span>'+ this.model.promo.h2copy +'</span></h2><p><a class="prop30 btn-sml '+ this.model.promo.ctacss +'" href="'+ this.model.promo.ctahref +'" title="' + this.model.promo.ctatext + (this.model.promo.newwindow == 'true' ? ' [Opens in a new window]" target="_blank' : '') + '">'+ this.model.promo.ctacopy +'</a></p>';
		
		var createPromo = new Element('div', {
			'class': 'promo ' + this.model.promo.promocss,
			'html': promoStr						
		}).inject(this.content);
		
		var createCols = new Element('div',{'class': 'cols', 'html': colsStr}).inject(this.content);
		// Add Omniture event tracking, just to the links in the main navigation flyover
		RF.Init.OmnitureMainNavEvents = new RF.Class.OmnitureTrackingManager({eventListeners:{elements:'#main-nav-content a'}});
		delete RF.Init.OmnitureMainNavEvents;
	},
	clickEvent: function() {
		this.flyout.ClassBasics.clearClassFromAll(this.flyout.maintabs, 'on');
		this.tab.addClass('on');
	},
	callEvents:function(){
		this.tab.addEvents({
			'click':this.clickEvent,
			'mouseenter':function(){
				this.getContent();
				this.callSubMenuEvents();
			}.bind(this),
			'focus':function(e){
				e.stop();
				(e.target).fireEvent('mouseenter');
			}.bind(this),
			'keydown':function(e){
				if(e.key == 'tab'){
					e.stop();
					if(!(e.shift)){
						if (mnStatus.currtab < mnStatus.taborder.length){
							RF.Moo.idE(this.submenulinks).focus();
							mnStatus.currtab += 1;
						}
					}else{
						if(mnStatus.currtab > 0){
							mnStatus.currtab -= 1;
							mnStatus.taborder[mnStatus.currtab].focus();	
						} else {
							(document.id('top-nav')) ? RF.Moo.idE('#top-nav a').focus() : RF.Moo.idE('#logo-audi a').focus();
							this.flyout.slideClose();	
						}	
					}
				}	
			}.bind(this),
			'keyup':function(e){
				if (e.key == 'tab') {
					e.stop();
					this.flyout.slideOpen();
				}
			}.bind(this)
		});	
		
		/* Remove the click event when in Apple Mobile */
		if(Browser.Platform.ipod) {
			/*
			this.tab.addEvents({
				'click':this.touchEventAppleMobile
			});
			*/
		}
	},
	touchEventAppleMobile: function() {
		this.appleMobileEvent = arguments[0];
		this.appleMobileEvent.preventDefault();
		this.tab.fireEvent('mouseenter');
		this.removeTouchEvents();
	},
	removeTouchEvents: function() {
		this.tab.removeEvents({
			'click':this.touchEventAppleMobile
		});
	},
	getContent:function(){
		this.html();
	},
	callSubMenuEvents:function(){
		var allSubLinks = $$(this.submenulinks);
		var flyout = this.flyout;
		allSubLinks.getLast().addEvents({
			'keydown':function(e){
				if (e.key == 'tab' && !(e.shift)) {
					e.stop();
					if (mnStatus.currtab < mnStatus.taborder.length) {
						mnStatus.taborder[mnStatus.currtab].focus();
					} else {
						flyout.slideClose();
						if(RF.Moo.idE('#locate-dealer a')) RF.Moo.idE('#locate-dealer a').focus();
					}
				}						
			}
		});
		allSubLinks[0].addEvents({
			'keydown':function(e){
				if (e.key == 'tab' && (e.shift)) {
					e.stop();
					mnStatus.currtab -= 1;
					mnStatus.taborder[mnStatus.currtab].focus();
				}							
			}	
		});
	}	
});

window.addEvents({
	'domready': function() {
		if(document.id('main-nav')){

			RF.Init.MainNavSlide = new RF.Class.FlyOverNavigation({id:'main-nav', maintabs:'#main-nav dd.tab a', flyover:'mn-flyover'});
			RF.Init.MainNavContent = (mainNavObj) ? parseMainNavigationData(mainNavObj) : new Request.JSON({
				method:'get',
				url: "/content/audi/main_nav_flyover.html",
				onSuccess: function(jsonObj){
					parseMainNavigationData(jsonObj)
				}
			}).send();

			if(RF.Moo.idE('#locate-dealer a')) {
				RF.Moo.idE('#locate-dealer a').addEvents({
					'mouseenter': function(e){
						RF.Init.MainNavSlide.slideClose();
					},
					'focus': function(e){
						(e.target).fireEvent('mouseenter');
					},
					'keydown': function(e){
						if (e.key == 'tab' && !(e.shift)) {
							e.stop();
							document.id('closemn-flyover').focus();
						}
					}	
				});
			}
		}
	}
});


function parseMainNavigationData(jsonObj) {
	var i = 0;
	$each(jsonObj, function(value, key){
		new RF.Class.MainNavigation({model:value, tabLink: RF.Init.MainNavSlide.maintabs[i], flyoutVar:RF.Init.MainNavSlide, contentArea:'main-nav-content'});
		i+=1;
	});	
	document.getElementsByClassName("flyover")[1].style.background="#d6d7d7 url(/etc/designs/audi/img/bgs/flyover-gradient.gif) repeat-x 0 0";
	document.getElementsByClassName("flyover")[1].style.borderLeft="1px solid #fff";
	document.getElementsByClassName("sub-menu")[1].style.background="url(/etc/designs/audi/img/bgs/flyover-btm.png) repeat-x center bottom";
	document.getElementById("mn-flyover").getElementsByClassName("promo")[0].style.borderLeft="1px solid #ccc";
}
