/**
 * Kickup
 * 
 * Shows/hides a nav menu when a toggler element is clicked
 * @param {DOMElement} $el The outer container element of the kickup
 * @param {String} triggerEvent The event that triggers the opening/closing of the nav. click|hover
 * 
 * Expects structure
 * 
 * <div class="kickup" id="kickup-identifier">
 *     <a class="toggle">Toggle</a>
 *     <ul>
 *         <li><a href="#">Menu item 1</a></li>
 *         <li><a href="#">Menu item 2</a></li>
 *     </ul>
 * </div>
 */
RF.Class.Kickup = function ($el, triggerEvent) {

    // Default trigger event to click
    triggerEvent = triggerEvent || 'click';

    /**
     * Open/close animation
     * @type {Fx}
     */
    var tween = new Fx.Tween($el.getElement('.nav'), {
        property: 'opacity',
        duration: 250
    });

    // Start opacity at 0
    tween.set(0);

    // Set display:none on element if hidden to avoid blocking click events
    tween.addEvent('complete', function ($el) {
        if ($el.getStyle('opacity') === 0) {
            $el.hide();
        }
    });

    /**
     * Shows the nav menu
     */
    function open () {
        $el.addClass('open');
        $el.getElement('.nav').show();
        tween.cancel().start(1);
    }

    /**
     * Hides the nav menu
     */
    function close () {
        $el.removeClass('open');
        tween.cancel().start(0);
    }

    /**
     * Checks whether the nav is currently open
     * @return {Boolean} Whether the nav is open or not
     */
    function isOpen () {
        return $el.hasClass('open');
    }

    /**
     * Toggles the open state of the nav menu
     */
    function toggle () {
        (isOpen() ? close : open)();
    }

    // Bind to the .toggle element
    if (triggerEvent === 'click') {
        $el.getElement('.toggle').addEvent('click', toggle);
    } else if (triggerEvent === 'hover') {
        $el.getElement('.toggle').addEvent('mouseenter', open);
        $el.addEvent('mouseleave', close);
    }

    // Return interface
    return {
        open: open,
        close: close,
        isOpen: isOpen,
        $el: $el
    }

};


// Implementation
(function () {

    // Declare empty object to store kickups
    RF.Init.kickups = {};

    /**
     * Determines whether an element is a child of another element
     * @param  {DOMElement}  $potentialChild  The potential child element
     * @param  {DOMElement}  $potentialParent The potential parent element
     * @return {Boolean}    Whether $potentialParent is a descendant of $potentialChild
     */
    function isChildOf ($potentialChild, $potentialParent) {
        return $potentialParent.getElements('*').indexOf($potentialChild) > -1;
    }

    /**
     * Closes all open kickups
     */
    function closeAllKickups () {
        $each(RF.Init.kickups, function (kickup) {
            if (kickup.isOpen()) {
                kickup.close();
            }
        });
    }

    // Close kickup when there's a click outside of it
    document.addEvent('mousedown', function (e) {
        $each(RF.Init.kickups, function (kickup) {
            if (kickup.isOpen() && !isChildOf(this, kickup.$el)) {
                kickup.close();
            }
        }, e.target);
    });

    // Close kickups on escape key press
    document.addEvent('keyup', function (e) {
        if (e.code === 27) {
            closeAllKickups();
        }
    });
    
})();

window.addEvent('domready', function () {
    $$('.kickup').each(function ($el) {
        RF.Init.kickups[$el.id] = new RF.Class.Kickup($el, RF.Browser.ie ? 'hover' : 'click');
    });
});