(function($){

RF.Class.TargetBlankReplace = new Class({
	// call <a href="http://www.website-url.com" title="A title" rel="external">Link text</a>
	Implements: [Options,RF.Class.Utilities],
	options: {
		warning: 'Opens in a new window',
		warningseparator: ': '
	},
	initialize: function(options){
		try {
			this.setOptions(options);
			$$('a[rel="external"],a[href$=".pdf"]').each(function(a) {
                if (a.getProperty('target') === '_blank') return; //Already performed on this link
                var title = (a.getProperty('title') != null ? a.getProperty('title') + this.options.warningseparator + this.options.warning : this.options.warning);
				a.setProperties({
					'title'  : title,
					'target' : '_blank'
				});
                if (a.getElement('img')) a.getElement('img').setProperty('alt',title);
			}, this);
		} catch(e) {
			this.ErrorHelper({method:"TargetBlankReplacement Class : initialize()",error:e});
		}
	}
});

})(document.id);