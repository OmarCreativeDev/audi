(function($){

/* Audi.co.uk
*  global.js
*  Main site javascript
*  Author: Razorfish, London
*  Date: 12-05-2009 
*  v1.1
*/
document.write('<link rel="stylesheet" type="text/css" href="@css.file.src.path@/js.css" media="all"/>');

RF.Moo.idE = function(selector, filter){return (document.id(filter) || document).getElement(selector);};
RF.Moo.idES = function(selector, filter){return (document.id(filter) || document).getElements(selector);};

RF.Global.CorrectSafariTabbing = function(){
	if (document.id('skip-nav')) {
		document.id('skip-nav').addEvents({
			'focus': function(){document.id('skip-nav').setStyle('left', '0')},
			'blur': function(){document.id('skip-nav').setStyle('left', '-10000px')},
			'click': function(){
				if ((navigator.appVersion).match(/safari/i)) {
					RF.Init.hrf = document.id('skip-nav').getProperty('href');
					if (RF.Init.hrf.indexOf('#') == 0) RF.Init.hrf = RF.Init.hrf.substring(1, RF.Init.hrf.length);
					if (!document.id('faux-target')) {
						RF.Init.a = new Element('a').setStyles({
							position: 'absolute', 
							left: '-10000px'
						}).set('html', '').setProperties({
							id: 'faux-target', 
							title: 'Entered main content of page', 
							href: ''
						}).injectTop(document.id(RF.Init.hrf));
					}
					document.id('faux-target').focus();
				}
			}
		});
	}

	if ((navigator.appVersion).match(/safari/i)) {
		$$('.target-top').each(function(e){
			e.addEvent('click', function(){
				if (!document.id('faux-target-top')) {
					RF.Init.b = new Element('a').setStyles({
						position: 'absolute', 
						left: '-10000px'
					}).set('html', '').setProperties({
						id: 'faux-target-top', 
						title: 'Top of the page', href: ''
					}).injectTop(document.id('top'));
				}
				document.id('faux-target-top').focus();
			});
		});
	}
};

RF.Global.PngFix = function(){	
	RF.Init.allPNGs = $$('img[src$=.png]');
	RF.Init.filtered = RF.Init.allPNGs.filter(function(item, index){
		// ignore any pngs in the navigation-panels (ACOUK-1524: IE6 display issue)
		if(item.getParents('#navigation-panels').length <= 0) return item;
	});
	RF.Init.filtered.each(function(i){
		if (i.hasClass('png') != true) {
			i.addClass('png');
		}
	});
};
RF.Global.ReinstateScriptsAfterAjaxCall = function() {
	RF.Global.PngFix();
	new RF.Class.TargetBlankReplace();
}
RF.Global.Facebook = function() {
    if (window.FB == undefined) return;
    FB.Event.subscribe('edge.create',function(response) {
        RF.Init.OmnitureNavPanelSingleAction = new RF.Class.OmnitureTrackingManager({
            singleAction:{
                eVars        : 'eVar46',
                props        : 'prop52',
                events       : 'event41',
                callToAction : 'Facebook:Add Like'
            }
        });
    });
    FB.Event.subscribe('edge.remove',function(response) {
        RF.Init.OmnitureNavPanelSingleAction = new RF.Class.OmnitureTrackingManager({
            singleAction:{
                eVars        : 'eVar46',
                props        : 'prop52',
                events       : 'event41',
                callToAction : 'Facebook:Remove Like'
            }
        });
    });
    FB.Event.subscribe('comment.create',function(response) {
        RF.Init.OmnitureNavPanelSingleAction = new RF.Class.OmnitureTrackingManager({
            singleAction:{
                eVars        : 'eVar46',
                props        : 'prop52',
                events       : 'event41',
                callToAction : 'Facebook:Add Comment'
            }
        });
    });
    FB.Event.subscribe('comment.remove',function(response) {
        RF.Init.OmnitureNavPanelSingleAction = new RF.Class.OmnitureTrackingManager({
            singleAction:{
                eVars        : 'eVar46',
                props        : 'prop52',
                events       : 'event41',
                callToAction : 'Facebook:Remove Comment'
            }
        });
    });
}
window.addEvents({
	'domready': function(){
		RF.Global.CorrectSafariTabbing();
		RF.Global.PngFix();
		RF.Init.TargetBlankReplace = new RF.Class.TargetBlankReplace();
		RF.Init.TrackingHound = new RF.Class.TrackingHound({elements:'*[class*=Atlas],a[class*=EyeBlaster],a[class*=s3_]'});
	},
    'load' : function() {
        RF.Global.Facebook();
    }
});

// Firefox 18 introduces ES6 ''.contains method, which is incompatible with (the previously used over the website) Mootools ''.contains
// Make sure native .contains is overwritten by mootools .contains
String.prototype.contains = function(string, separator){
    return (separator) ? (separator + this + separator).indexOf(separator + string + separator) > -1 : this.indexOf(string) > -1;
};

})(document.id);