(function($){

/*
* UI Interface methods, e.g. between the Flex SWF and UI JavaScript (used for Gold Showroom) or between JS and UI
*/

RF.Init.APIUtils = new RF.Class.Utilities();
var JStoSWF_playInteractiveFeature = function(SWFId,ObjectToSWF) {
	/* 
	* @Direction: JS to SWF
	* @Name: playInteractiveFeature(swfId, obj)
	* @Description: Play a Showroom interactive feature for the current model
	*/
	try {
		document.id(SWFId).playInteractiveFeature(ObjectToSWF);		
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"JStoSWF_playInteractiveFeature()",error:e});
	}
}

var JStoSWF_resizeComplete = function(SWFId,success) {
	/* 
	* @Direction: JS to SWF
	* @Name: resizeComplete(swfId, boolean)
	* @Description: Confirm a setStage() method call has completed in the UI, and the SWF may now change content
	*/
	try {
		document.id(SWFId).resizeComplete(success);
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"JStoSWF_resizeComplete()",error:e});
	}
}

var JStoSWF_stopVideo = function(SWFId) {
	/* 
	* @Direction: JS to SWF
	* @Name: stopVideo(swfId)
	* @Description: JS to SWF - Fire the mediaPlayer_stop() method on the selected SWF.
	*/
	try {
		if (document.id(SWFId).mediaPlayer_stop) {
			document.id(SWFId).mediaPlayer_stop();
		}
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"JStoSWF_stopVideo()",error:e});
	}
}

var JStoSWF_pauseVideo = function(SWFId) {
	/* 
	* @Direction: JS to SWF
	* @Name: pauseVideo(SWFId)
	* @Description: JS to SWF - Fire the mediaPlayer_pause() method on the selected SWF.
	*/
	try {
		if (document.id(SWFId).mediaPlayer_pause) {
			document.id(SWFId).mediaPlayer_pause();
		}
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"JStoSWF_pauseVideo()",error:e});
	}
}

var SWFtoJS_unloadModel = function(ObjectFromSWF) {
	/*
	* @Direction: SWF to JS
	* @Name: SWFtoJS_unloadModel(obj)
	* @Description: This method calls the RF_NavigationPanelsManager class
	* @ObjectParams:
	*	range: String (e.g. 'a4')
	*	moddel: String (e.g. 'a4-saloon')
	*/
	try {
		RF.Init.APINavPanelManager = new RF.Class.NavigationPanelManager(ObjectFromSWF);
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_loadModel()",error:e});
	}
}

var SWFtoJS_loadModel = function() {
	/*
	* @Direction: SWF to JS
	* @Name: SWFtoJS_loadModel(obj)
	* @Description: This method calls an existing instance of the RF_NavigationPanelsManager class, to fade in the nav panels
	*/
	try {
		RF.Init.APINavPanelManager.showNavPanels();
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_loadModel()",error:e});
	}
}

var SWFtoJS_setFlashGalleries = function(ObjectFromSWF) {
	/*
	* @Direction: SWF to JS
	* @Name: SWFtoJS_setFlashGalleries(obj)
	* @Description: This method calls the RF.Class.FlashGalleries class
	* @ObjectParams:
	*	range: String (e.g. 'a4')
	*	moddel: String (e.g. 'a4-saloon')
	*/
	try {
		RF.Init.APIFlashGalleries = new RF.Class.FlashGalleries(ObjectFromSWF);
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_loadModel()",error:e});
	}
}

var SWFtoJS_setStage = function(ObjectFromSWF) {
	/* 
	* @Direction: SWF to JS
	* @Name: SWFtoJS_setStage()
	* @Description: Set the stage according to the stageFormat (e.g. 'temp_copy_heavy'),
	* which will correspond to a css class
	*/
	try {
		RF.Init.APIStageManager = new RF.Class.StageManager(ObjectFromSWF);
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_setStage()",error:e});		
	}	
}

var SWFtoJS_setShowroomBackground = function(ObjectFromSWF) {
	/* 
	* @Direction: SWF to JS
	* @Name: SWFtoJS_setShowroomBackground()
	* @Description: Sets a Showroom background
	*/
	try {
		if(!$defined(RF.Init.APIStageManager)) RF.Init.APIStageManager = new RF.Class.StageManager(ObjectFromSWF);
		RF.Init.APIStageManager.setShowroomSpecificBackground(ObjectFromSWF);
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_setShowroomBackground()",error:e});		
	}	
}

var SWFtoJS_openQuickTimeVR = function(ObjectFromSWF) {
	/* 
	* @Direction: SWF to JS
	* @Name: SWFtoJS_openQuickTimeVR()
	* @Description: Launch a QuickTime movie in a new window
	*/
	try {
		RF.Init.QTLaunch = new RF.Class.QTLaunch({src:ObjectFromSWF.src, width:ObjectFromSWF.width, height:ObjectFromSWF.height});
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_openQuickTimeVR()",error:e});
	}	
}

var SWFtoJS_changeFocus = function() {
	/* 
	* @Direction: SWF to JS
	* @Name: SWFtoJS_changeFocus()
	* @Description: For Audi mediaplayer to help Mac OS flash regain focus after fullscreen mode has been closed
	*/
	try {
		this.window.focus();
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_changeFocus()",error:e});
	}
}

var SWFtoJS_guidedTour = function(ObjectFromSWF) {
	/* 
	* @Direction: SWF to JS
	* @Name: SWFtoJS_guidedTour()
	* @Description: Triggered by the Guided Tour Call To Action (CTA) within the Showroom SWF only
	*/
	try {
		if(!$defined(ObjectFromSWF.SWF)){
			RF.Init.tempObjectFromSWF = {
				SWF:{
					vars:ObjectFromSWF
				}
			}
			ObjectFromSWF = RF.Init.tempObjectFromSWF;
			delete RF.Init.tempObjectFromSWF;
		}
		RF.Init.APIGuidedTour = new RF.Class.GuidedTour(ObjectFromSWF);
		delete RF.Init.APIGuidedTour;
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_guidedTour()",error:e});
	}
}

var SWFtoJS_mediaPlayer = function(ObjectFromSWF) {
	/* 
	* @Direction: SWF to JS
	* @Name: SWFtoJS_videoOverlay()
	* @Description: Video Overlay method
	*/
	try {
		RF.Init.APIMediaPlayer = new RF.Class.MediaPlayer(ObjectFromSWF);
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_mediaPlayer()",error:e});
	}
}

var SWFtoJS_trackCallToAction = function(ObjectFromSWF) {
	/* 
	* @Direction: SWF to JS
	* @Name: SWFtoJS_trackCallToAction()
	* @Description: (Omniture SiteCatalyst) Call to the HTML UI tracking framework to track calls to actions that have been clicked from within a SWF
	*/
	console.log('In SWFtoJS_trackCallToAction');
	try {
		console.log('In SWFtoJS_trackCallToAction try');
		RF.Tracking.APIOmnitureTrackingManager = new RF.Class.OmnitureTrackingManager(ObjectFromSWF);
	} catch(e) {
		console.log('In SWFtoJS_trackCallToAction catch');
		RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_trackCallToAction()",error:e});
	}
}

var SWFtoJS_selectHTMLAnchor = function(ObjectFromSWF) {
	/* 
	* @Direction: SWF to JS
	* @Name: SWFtoJS_trackHTMLLink()
	* @Description: (Omniture SiteCatalyst) Call to the HTML UI to fire a click event on HTML links, so that the s_code JS from Omniture will track document downloads, for example
	* @Arguments:
	* 	ObjectFromSWF [Object]
	* 		ObjectFromSWF.url [String]
	* @Example: SWFtoJS_selectHTMLAnchor({url:'/content/dam/audi/production/PDF/PriceAndSpecGuides/r8.pdf'})
	*/
	try {	
		if(ObjectFromSWF.url != "") {
			if(document.getElement('a[href="'+ObjectFromSWF.url+'"]')) {
				document.getElement('a[href="'+ObjectFromSWF.url+'"]').addEvent('click', $lambda(true)).fireEvent('click');
			} else {
			}
		}
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_trackCallToAction()",error:e});
	}
}

var SWFtoJS_trackMediaCom = function(tagString) {
	/* 
	* @Direction: SWF to JS
	* @Name: SWFtoJS_trackMediaCom(tag[String])
	* @Description: MediaCom (Atlas, EyeBlaster,...) call to the HTML UI tracking framework
	*/	
	try {
		RF.Tracking.APIHound = new RF.Class.TrackingHound({singleAction:tagString, elements:null});
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_trackMediaCom()",error:e});
	}
}

var SWFtoJS_onShowroomInitializationComplete = function() {
	/* 
	* @Direction: SWF to JS
	* @Name: SWFtoJS_showroomInitializationComplete()
	* @Description: Showroom initialization is complete, so enable the navigation panels
	*/
	try {
		if ($('navigation-panels')) {
			RF.Init.NavigationPanel1 = new RF.Class.NavigationPanel('#nav_panel_1-accordion h2 a', '#nav_panel_1-accordion .expand', {accordion:'nav_panel_1-accordion', navpanel:'nav_panel_1', trigger:'nav_panel_1-trigger', blnRevealPanelOnLoad:false});
	        RF.Init.NavigationPanel2 = new RF.Class.NavigationPanel('#nav_panel_2-accordion h2 a', '#nav_panel_2-accordion .expand', {accordion:'nav_panel_2-accordion', navpanel:'nav_panel_2', trigger:'nav_panel_2-trigger', blnRevealPanelOnLoad:false});
	        RF.Init.NavigationPanel3 = new RF.Class.NavigationPanel('#nav_panel_3-accordion h2 a', '#nav_panel_3-accordion .expand', {accordion:'nav_panel_3-accordion', navpanel:'nav_panel_3', trigger:'nav_panel_3-trigger', blnRevealPanelOnLoad:false});
		}
	} catch(e) {
		RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_showroomInitializationComplete()",error:e});
	}
}

var SWFtoJS_loadHeritageArticle = function(callerId, articleURL) {
    /* 
    *   @Direction: SWF to JS
    *   @Name: SWFtoJS_heritageMenu
    *   @Description: Open a Heritage SWF Menu item (link URL)
    *   @arguments: 
    *       callerId: 'gallery', 'menu' [String]
    *       articleUrl: Article URL Path [String]
    */
    try {
        /* If we have an instance of the HeritageArticleManager Class... Load the Article */
        if(RF.Init.HeritageArticleManager) {
            RF.Init.HeritageArticleManager.updateContent(callerId,articleURL);
        } else {
            RF.Init.HeritageArticleManager = new RF.Class.HeritageArticleManager({article:articleURL,callerId:callerId});
            RF.Init.HeritageArticleManager.updateContent(callerId,articleURL);
        }           
    } catch(e) {
        RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_loadHeritageArticle()",error:e});
    }
}
var SWFtoJS_closeHeritageGallery = function() {
    /* 
    *   @Direction: SWF to JS
    *   @Name: SWFtoJS_heritageMenu
    *   @Description: Open a Heritage SWF Menu item (link URL)
    *   @arguments: none
    */
    try {
        // close the Heritage Gallery SWF
        if(RF.Init.HeritageArticleManager) RF.Init.HeritageArticleManager.closeHeritageGallery();
    } catch(e) {
        RF.Init.APIUtils.ErrorHelper({method:"SWFtoJS_loadHeritageArticle()",error:e});
    }   
}

})(document.id);
