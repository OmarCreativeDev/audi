document.write('<style type="text/css">#centresPlaceholder { visibility: hidden; }</style>');

/*
*	@Author:  Geoff Kaile
*	@Company: Razorfish London
*	@Name:    Centre Sidebar Class
*	@Description:
*	Class to manage the sidebar overlay on the centres map.
*/
RF.Class.Sidebar = new Class({
    Implements : [Options, RF.Class.Utilities],
    Binds      :    [   'showSidebar',
                        'hideSidebar',
                        'showDirections',
                        'getDirections',
                        'displayDirections',
                        'setCurrentState',
                        'addCentreListeners',
                        'switchDirectionsInputs'
                    ],
    dirService      : null,
    dirResultField  : 'destination',
    panel           : 'all',
    currentState    : 'closed', // 'closed', 'searchResults', 'allCentres', 'directions'
    prevState       : null,
    selectedCentreId    : null,
    originalOrigin      : null,
    originalDestination : null,
    dirService          : null,
    directionsResults   : null,

    initialize : function(options) {
        this.enableLog();

        this.setOptions(options);
        this.options.container.fade('hide');
        this.createOpenBtn();
        this.addEvents();

        this.directionsResults =
            this.options.directions.getElement('#directions-results');
    },
    createOpenBtn : function() {
        this.openSidebarButton = new Element('a',{
            'class'  : this.options.open,
            'text'   : 'Results list'
        }).inject(this.options.container,'after');
        this.openSidebarButton.fade('hide');

        this.openSidebarButton.addEvent('click', function (e){
            e.stop();
            this.openSidebarButton_clickHandler();
        }.bind(this));

    },
    addEvents : function() {

        this.options.tabBarControls.searchResults.addEvent('click', function (e){
            e.stop();
            this.setCurrentState('searchResults');
            RF.centre.resetMarkers();
        }.bind(this));

        this.options.tabBarControls.allCentres.addEvent('click', function (e){
            e.stop();
            this.setCurrentState('allCentres');
            RF.centre.showAllMarkers();
        }.bind(this));

        this.options.origin.addEvent('focus', function(e){
            e.stop();
            this.originalOrigin = this.options.origin.value;
        }.bind(this));

        this.options.origin.addEvent('blur', function(e){
            e.stop();
            var newVal = this.options.origin.value;

            if (this.originalOrigin != newVal)
                this.options.actualOrigin.set('value', newVal);

        }.bind(this));

        this.options.destination.addEvent('focus', function(e){
            e.stop();
            this.originalDestination = this.options.destination.value;
        }.bind(this));

        this.options.destination.addEvent('blur', function(e){
            e.stop();
            var newVal = this.options.destination.value;
            if (this.originalDestination != newVal)
                this.options.actualDestination.set('value', newVal);
        }.bind(this));

        this.addCentreListeners(document.getElementById('centres-full-list'));

        this.addCloseFn();
        this.addDirFn();
    },
    addCloseFn : function() {
        document.getElements(this.options.close).each(function(closeBtn) {
            closeBtn.addEvent('click',function(e) {
                e.stop();
                this.closePanelClickHandler();
            }.bind(this));
        },this);
    },
    addDirFn : function() {
        this.directions = this.options.directions.getElement('form').addEvent('submit',function(e) {
            e.stop();
            this.getDirections();
        }.bind(this));
        this.options.switchDir.addEvent('click',function(e) {
            e.stop();
            this.switchDirectionsInputs();
            this.getDirections();
        }.bind(this));
    },
    showResultsButton : function(){
        this.openSidebarButton.fade('in');
    },
    hideResultsButton : function (){
        this.openSidebarButton.fade('out');
    },
    showSidebar : function() {
        RF.centre.centre = RF.centre.map.getCenter();
        this.options.container.fade('in');
        $(RF.centre.options.container).addClass('sidebar');
        google.maps.event.trigger(RF.centre.map,'resize');
    },
    hideSidebar : function() {
        RF.centre.centre = RF.centre.map.getCenter();
        this.options.container.fade('out');
        $(RF.centre.options.container).removeClass('sidebar');
        google.maps.event.trigger(RF.centre.map,'resize');
    },
    closePanelClickHandler : function () {
        this.setCurrentState('closed');
    },
    showDirections : function(destination, title) {
        var directionsResults =
            this.options.directions.getElement('#directions-results');
        directionsResults.empty();

        var lastSearch, actualOrigin, actualDestination, displayOrigin, displayDestination;

        lastSearch = RF.centreSearch.lastSearch;
        actualDestination = destination;
        displayDestination = title;

        if (    lastSearch.valid &&
                (lastSearch.searchTerm.toLowerCase().trim() != title.toLowerCase().trim())
        )
        {
            // search term as start, selected centre as end, origin highlighted
            actualOrigin = lastSearch.searchTerm;
            displayOrigin = lastSearch.searchTerm;
        }
        else
        {
            // origin empty, selected centre as end, cursor in origin
            actualOrigin = "";
            displayOrigin = "";
        }

        this.setDirectionsInputs(
            actualOrigin,
            displayOrigin,
            actualDestination,
            displayDestination
        );

        this.setInputsAppearance(
            this.options.origin,
            this.options.destination,
            'origin'
        );

    },
    setInputsAppearance : function(editable, nonEditable, dirResultField){
        editable.removeProperties('readonly', 'style');
        nonEditable.setProperty('readonly', 'readonly').setStyle('border', 'none');
        this.dirResultField = dirResultField;
    },
    setDirectionsInputs : function(actualOrigin, displayOrigin, actualDestination, displayDestination){
        this.options.actualOrigin.set('value', actualOrigin);
        this.options.origin.set('value', displayOrigin);
        this.options.actualDestination.set('value', actualDestination);
        this.options.destination.set('value', displayDestination);
    },
    switchDirectionsInputs : function (){
        this.setDirectionsInputs(
            this.options.actualDestination.value,
            this.options.destination.value,
            this.options.actualOrigin.value,
            this.options.origin.value
        );

        if (this.dirResultField == 'destination')
            this.setInputsAppearance(
                this.options.origin,
                this.options.destination,
                'origin'
            );
        else
            this.setInputsAppearance(
                this.options.destination,
                this.options.origin,
                'destination'
            );
    },
    getDirections : function() {

        if (!this.options.actualOrigin.value || !this.options.actualDestination.value)
        {
            this.directionsResults.set('html', this.getEmptyDirections());
            return;
        }

        if (this.dirService == null) {
            this.dirService = {
                service  : new google.maps.DirectionsService(),
                renderer : new google.maps.DirectionsRenderer({suppressMarkers:true})
            };
        }

        this.dirService.renderer.setMap(RF.centre.map);
        this.dirService.renderer.setPanel(this.options.results);

        this.dirService.service.route({
            origin      : this.options.actualOrigin.value,
            destination : this.options.actualDestination.value,
            travelMode  : google.maps.DirectionsTravelMode.DRIVING,
            unitSystem  : google.maps.DirectionsUnitSystem.IMPERIAL,
            region      : 'GB' // @see http://code.google.com/apis/maps/documentation/javascript/services.html#GeocodingRegionCodes
                               // @see http://www.iana.org/assignments/language-subtag-registry
        },this.displayDirections);
    },
    displayDirections : function(result,status) {
        this.directionsResults.empty();
        document.getElement('.get-directions').blur();

        if (status != google.maps.DirectionsStatus.OK)
        {
            this.directionsResults.set('html', this.getInvalidDirections());
            this.clearRoute();
        }
        else
        {
            var leg = result.routes[0].legs[0];
            this.dirService.renderer.setDirections(result);
            this.directionsResults.set(
                'html',
                this.getDirectionsResults(leg)
            );

            RF.centre.showDirectionsMarkers([
                {position:leg.start_location, title:leg.start_address},
                {position:leg.end_location, title: leg.end_address}
            ]);
        }
    },

    /* methods */
    renderSidebar : function (html){
        this.options.results.set('html', html.join(''));
    },
    getDirectionsResults : function(leg){

        var steps = leg.steps;

        var html = [
            '<h3>Directions to: <br />',
            '<strong>', this.options.destination.value, '</strong><br />',
            leg.distance.text, ' (', leg.duration.text, ')</h3>',
            '<p><span class="direction-point">A </span>',
            leg.start_address,
            '</p>'
        ];

        html.push('<ol>');
        steps.each(function(step){
            html.push(
                '<li>',
                '<span>',
                step.distance.text,
                '</span>',
                step.instructions,
                '</li>'
            );
        }, this);
        html.push(
            '</ol>',
            '<p><span class="direction-point">B </span>',
            this.options.destination.value,
            '</p>',
            '<p class="directions"><a href="javascript:window.print()" title="Print Directions">Print Directions</a></p>'
        );
        return html;
    },
    getSearchResults : function(results){
        var html =
        [
            '<h3>Centres near ',
            document.getElement('input[name="search-centres"]').get('value'),
            '</h3>',
            '<dl class="centre-list">'
        ];

        results = results.slice(0, 10);

                results.each(function(result, i) {
                    var centre = RF.centre.getCentreById('c'+result.id);

                    if (i == 0)
                        html.push('<dd class="vcard selected" id="c', result.id,'">');
                    else
                        html.push('<dd class="vcard" id="c', result.id,'">');

                    html.push(
                        '<p class="fn org">',
                        '<span class="item">',
                        (i + 1), ' ',
                        '</span>',
                        centre.data.title,
                        '</p>',
                        '<p class="address">', centre.data.address.join(', '), '</p>',
                        '<p class="distance">',
                        result.distance,
                        '</p>',
                        '<div class="border-bottom"></div>',
                        '</dd>'
                    );
                },this);

        html.push('</dl>');
        return html;
    },
    getEmptyDirections : function (){
        var html = [
            '<h3>Please enter a location in the text box above.</h3>'
        ];
        return html;
    },
    getInvalidDirections : function (){
        var html = [
            '<h3>We could not understand at least one of these locations.</h3>',
            '<p>Suggestions:</p>',
            '<ul>',
            '<li>Make sure all town and city names are spelt correctly and within the United Kingdom.</li>',
            '<li>Try entering a valid postcode.</li>',
            '</ul>'
        ];

        return html;
    },
    getMultipleMatches : function (results){
        var html = [
            '<div class="centre-error">',
            '<h3>Your search returned multiple results.</h3>',
            '<p>Please select a location from the following list:</p>',
            '<ul>'
        ];

        var searchOrigins = [];

        /**
         * BUSINESS LOGIC
         *
         * We were requested by the client to move the TOWN to the first place
         * when the search term matches with the town name.
         */
        results.each(function(result){
            result.searchOrigins.each(function(origin){
                origin.term = result.term;
                if (origin.name.toLowerCase().trim() == RF.centreSearch.getSearchTerm().toLowerCase())
                    searchOrigins.unshift(origin);
                else
                    searchOrigins.push(origin);
            }, this);
        }, this);

        searchOrigins.each(function(origin){
            html.push(
                '<li class="vcard">',
                '<a href="#">',this.getOriginName(origin),'</a>',
                '<span class="hide">', origin.gridNorth,'</span>',
                '<span class="hide">', origin.gridEast,'</span>',
                '<span class="hide">', origin.term, '</span>',
                '</li>'
            );
        }, this);

        html.push('</ul>', '</div>');

        return html;
    },
    getOriginName : function (origin){
        var centre = RF.centre.getCentreById(origin.id);
        var name;
        if (centre)
            name = centre.data.title;
        else
            name = origin.name;
        return name.toUpperCase();
    },
    getValidationError : function (){
        var html = this.getNoMatches();

        return html;
    },
    getNoMatches : function (){
        var html = [
            '<div class="centre-error">',
            '<h3>There were no Audi Centres found in: </h3>',
            '<p><strong>',
            RF.centreSearch.getSearchTerm(),
            '</strong></h3>',
            '<p>Suggestions:</p>',
            '<ul>',
            '<li>Please enter a full postcode.</li>',
            '<li>Make sure all town and city names are spelt correctly and within the United Kingdom.</li>',
            '<li>Try changing the filters above.</li>',
            '<ul>',
            '</div>'
        ];

        return html;
    },
    getEmptySearch : function (){
        var html = [
            '<div class="centre-error">',
            '<h3>Please enter a ',
            RF.centreSearch.options.searchPrompt,
            ' to perform a search.</h3>'
        ];
        return html;
    },
    addCentreListeners : function (container) {
        container.getElements('.vcard').each(function(centre){
            centre.addEvent('click', function(e){
                e.stop();
                var centreId = centre.id;
                this.selectCentreById(centreId);

                RF.centre.lazyOpenInfoWindow = true;
                RF.centre.centreMapOnCentre(RF.centre.getCentreById(centreId));
            }.bind(this))
        }, this);
    },
    addMultipleMatchesListeners : function (){
        this.options.results.getElements('.vcard').each(function(origin){
            origin.addEvent('click', function (e){
                e.stop();
                var grid = origin.getElements('span');
                RF.centreSearch.searchFromGrid(
                    origin.getElement('a').get('text'),
                    {   north:grid[0].get('text'),
                        east:grid[1].get('text'),
                        term:grid[2].get('text')
                    }
                );
            }.bind(this))
        }, this);
    },
    selectCentreById : function (id) {
        if (!RF.centreSearch.lastSearch.valid)
            return;
        this.clearSelectedCentre();
        this.selectedCentreId = id;
        $(this.selectedCentreId).addClass('selected');
    },
    clearSelectedCentre : function () {
        if (this.selectedCentreId)
        {
            $(this.selectedCentreId).removeClass('selected');
            this.selectedCentreId = null;
        }
    },
    clearRoute : function (){
        if (this.dirService)
        {
            this.dirService.renderer.setMap(null);
            this.dirService.renderer.setPanel(null);
        }

        RF.centre.hideDirectionsMarkers();
    },
    resetSidebar : function (data, status){

        this.clearRoute();

        switch (status)
        {
            case ('onematch'):
            {
                this.hideSidebar();
                this.renderSidebar(this.getSearchResults(data));
                this.addCentreListeners(this.options.results);
                this.openSidebarButton.set('text', 'Results List');
                this.setCurrentState('searchResults');
                break;
            }
            case ('multiplematches'):
            {
                this.renderSidebar(this.getMultipleMatches(data));
                this.setCurrentState('searchResults');
                this.addMultipleMatchesListeners();
                break;
            }
            case ('error'):
            {
                this.renderSidebar(this.getValidationError());
                this.setCurrentState('searchResults');
                break;
            }
            case ('nomatches'):
            {
                this.renderSidebar(this.getNoMatches());
                this.setCurrentState('searchResults');
                break;
            }
            case ('emptysearch'):
            {
                this.renderSidebar(this.getEmptySearch());
                this.setCurrentState('searchResults');
                break;
            }
            case ('directions'):
            {
                RF.centre.hideMarkers();
                RF.centre.closeInfoWindow();
                RF.centre.directionsResults = true; // sets the map to directions mode
                this.showDirections(data.position, data.title);
                this.setCurrentState('directions');
                this.getDirections();
                break;
            }
        }
    },

    setCurrentState : function (state){
        switch (state)
        {
            case ('closed'):
            {
                this.hideSidebar();
                this.showResultsButton();
                break;
            }

            case ('searchResults'):
            {
                // tabs
                this.options.tabBarControls.searchResults.setProperty('class', 'selected');
                this.options.tabBarControls.allCentres.setProperty('class', '');
                this.options.tabBarControls.directions.setProperty('class', 'hide');

                // panels
                this.options.results.setStyle('display', 'block');
                this.options.full.setStyle('display', 'none');
                this.options.directions.setStyle('display', 'none');

                break;
            }

            case ('allCentres'):
            {
                // tabs
                this.options.tabBarControls.searchResults.setProperty('class', '');
                this.options.tabBarControls.allCentres.setProperty('class', 'selected');
                this.options.tabBarControls.directions.setProperty('class', 'hide');

                // panels
                this.options.results.setStyle('display', 'none');
                this.options.full.setStyle('display', 'block');
                this.options.directions.setStyle('display', 'none');

                this.clearSelectedCentre();
                RF.centre.closeInfoWindow();
                break;
            }

            case ('directions'):
            {
                // tabs
                this.options.tabBarControls.searchResults.setProperty('class', 'hide');
                this.options.tabBarControls.allCentres.setProperty('class', 'hide');
                this.options.tabBarControls.directions.setProperty('class', 'selected');

                // panels
                this.options.results.setStyle('display', 'none');
                this.options.full.setStyle('display', 'none');
                this.options.directions.setStyle('display', 'block');

                this.clearSelectedCentre();
                break;
            }
        }

        switch(state)
        {
            case ('searchResults'):
            case ('allCentres'):
            case ('directions'):
            {
                this.showSidebar();
                this.hideResultsButton();
                break;
            }
        }

        switch(state)
        {
            case ('searchResults'):
            case ('allCentres'):
            {
                this.openSidebarButton.set('text', 'Results List');
                break;
            }
            case ('directions'):
            {
                this.openSidebarButton.set('text', 'Directions');
                break;
            }
        }

        this.prevState = this.currentState;
        this.currentState = state;
    },

    /* event handlers */
    openSidebarButton_clickHandler : function() {
        this.setCurrentState(this.prevState);
    }

});
/*
*	@Author:  Geoff Kaile
*	@Company: Razorfish London
*	@Name:    Centre Search Class
*	@Description:
*	Class to search the centre webservice for the 10 closest centres to the
*   search location with the given facilities.
*/
RF.Class.CentreSearch = new Class({
    Implements  : [Options, RF.Class.Utilities],
    Binds       :   [
                        'searchFromOrigin',
                        'searchFromGrid',
                        'search_successHandler',
                        'checkBox_changeHandler',
                        'getCookie'
                    ],
    jsonRequest : null,
    lastSearch : {
        'kind'       : null,
        'valid'      : false,
        'searchTerm' : null,
        'grid'       : {
            'north' : null,
            'east'  : null,
            'term'  : null
        },
        'newcar'    : false,
        'authrep'   : false,
        'usedcar'   : false
    },
    searchInput : null,

    initialize : function(options) {
        this.enableLog();

        this.setOptions(options);
        this.container = $(this.options.container);

        this.searchInput = this.container.getElement('input[type="text"]');

        this.container.addEvent('submit',function(e) {
            e.stop();
            this.searchFromOrigin();
        }.bind(this));

        this.setSearchTerm(this.options.searchPrompt, true);

        this.searchInput.addEvent('focus', function(e){
            e.stop();
            if (this.searchInput.value == this.options.searchPrompt)
                this.setSearchTerm("");
            else
                this.searchInput.select();
        }.bind(this));

        this.searchInput.addEvent('blur', function(e){
            e.stop();
            if (this.searchInput.value == "")
                this.setSearchTerm(this.options.searchPrompt, true);
            else
                this.setSearchTerm(this.getSearchTerm()); // cleanup and upper case
        }.bind(this));
    },
    getSearchParams : function() {
        var params = {};
        params.format = "json";

        if ($('filter-new').checked)
            params.newcar = 1;

        if ($('filter-service').checked)
            params.authrep = 1;

        if ($('filter-used').checked)
            params.usedcar = 1;

        this.lastSearch.newcar = params.newcar;
        this.lastSearch.authrep = params.authrep;
        this.lastSearch.usedcar = params.usedcar;

        return params;
    },
    getSearchTerm : function () {
        return this.searchInput.value.trim();
    },
    setSearchTerm : function (searchTerm, isPrompt) {
        if (isPrompt)
        {
            this.searchInput.value = searchTerm;
            this.searchInput.addClass('search-prompt');
        }
        else
        {
            this.searchInput.value = this.cleanupSearchTerm(searchTerm);
            this.searchInput.removeClass('search-prompt');
        }
    },
    /**
     * HACK
     * manually removing " (Service Centre)" from the search term before
     * setting the search input
     */
    cleanupSearchTerm : function (searchTerm){
        return searchTerm.replace(/ \(Service Centre\)$/i, '').toUpperCase();
    },
    searchFromOrigin : function(){
        RF.centre.directionsResults = false;

        if (
            (this.getSearchTerm() == this.options.searchPrompt) ||
            (this.getSearchTerm() == "")
        )
        {
            RF.sidebar.resetSidebar(null, 'emptysearch');
            return;
        }

        this.lastSearch.kind = 'originSearch';
        this.lastSearch.valid = false;
        this.lastSearch.searchTerm = this.getSearchTerm();
        this.lastSearch.grid = null;

        var params = this.getSearchParams();
        params.searchParam = this.lastSearch.searchTerm;

        this.setCookie();

        this.jsonRequest = new Request.JSON({
            url:this.options.originSearchUrl,
            onSuccess:this.search_successHandler,
            method:'get'
        }).get(params);

        this.setSearchTerm(this.getSearchTerm());
    },
    searchFromGrid : function (searchTerm, grid){

        this.lastSearch.kind = "gridSearch";
        this.lastSearch.valid = false;
        this.lastSearch.searchTerm = searchTerm;
        this.lastSearch.grid = grid;
        this.setSearchTerm(this.lastSearch.searchTerm);

        var params = this.getSearchParams();
        params.gridn =  grid.north;
        params.gride = grid.east;
        params.term = grid.term;

        this.setCookie();

        this.jsonRequest = new Request.JSON({
            url:this.options.gridSearchUrl,
            onSuccess:this.search_successHandler,
            method:'get'
        }).get(params);
    },
    setCookie : function() {
        this.cookie = Cookie.write('lastSearch',JSON.encode(this.lastSearch));
    },
    getCookie : function() {
        this.cookie = Cookie.read('lastSearch');
        if (this.cookie != null) {
            this.lastSearch = JSON.decode(this.cookie);

            if (this.lastSearch.newcar)
                $('filter-new').setProperty('checked', true);

            if (this.lastSearch.authrep)
                $('filter-service').setProperty('checked', true);

            if (this.lastSearch.usedcar)
                $('filter-used').setProperty('checked', true);

            this.repeatLastSearch();
        }
    },
    repeatLastSearch : function (){
        if (this.lastSearch.kind == 'originSearch')
        {
            this.setSearchTerm(this.lastSearch.searchTerm);
            this.searchFromOrigin();
        }
        else if (this.lastSearch.kind == 'gridSearch')
        {
            this.searchFromGrid(this.lastSearch.searchTerm,this.lastSearch.grid);
        }
    },
    /* event handlers */
    search_successHandler : function (result){

        var data;
        var status = result.status;
        RF.centre.directionsResults = false;

        switch (result.status)
        {
            case ('onematch'):
            {
                data = result.results.centres;
                this.lastSearch.valid = true;
                break;
            }
            case ('multiplematches'):
            {
                data = result.results;
                break;
            }
            case ('nomatches'):
            {
                data = null;
                break;
            }
            case ('error'):
            {
                data = null;
                break;
            }
        }

        this.jsonRequest = null;

        /**
         * BUSINESS LOGIC
         * Handles accuracy on search results
         */
        var resolution = null;

        if (result.status == 'multiplematches')
            resolution = this.resolveMultipleMatches(result, data);

        if (resolution)
        {
            this.searchFromGrid(resolution.id, resolution.grid);
        }
        /**
         * This is the normal behavior of search_successHandler
         */
        else
        {
            RF.sidebar.resetSidebar(data, status);
            RF.centre.showResults(data, status);
        }
    },
    /**
     * BUSINESS LOGIC
     * Handles accuracy on search results
     */
    resolveMultipleMatches : function (result, data){
        var resolution = this.resolveExactMatchOnTown(result, data);

        if (!resolution)
            resolution = this.resolveOneCentreAndOneTown(result, data);

        return resolution;
    },
    /**
     * BUSINESS LOGIC
     * If there is a exact match in the TOWN array, the data to perform a grid
     * search with the town location is returned
     */
    resolveExactMatchOnTown : function (result, data){
        var townResults = [], term;

        for (var i = 0, results; results = data[i]; i++)
        {
            if (results.term == 'TOWN')
            {
                townResults = results.searchOrigins;
                term = results.term;
                break;
            }
        }

        if (townResults.length)
        {
            var searchTerm = RF.centreSearch.getSearchTerm().toLowerCase();

            for (var i = 0, origin; origin = townResults[i]; i++)
            {

                if(origin.name.toLowerCase() == searchTerm)
                {
                    return {
                        id : origin.name,
                        grid : {
                            north:origin.gridNorth,
                            east:origin.gridEast,
                            term:term
                        }
                    };
                }
            }
        }

        return null;
    },
    /**
     * BUSINESS LOGIC
     *
     * If there is only 1 CENTRE result and 1 TOWN result, we
     * ASSUME they point to the same centre, the data to perform a grid
     * search with the town location is returned
     * Altough, this is not necessarily true. Results in the data Array
     * can point to 2 different centres.
     */
    resolveOneCentreAndOneTown : function (result, data){

        var CENTRE_INDEX = 0;
        var TOWN_INDEX = 1;
        var centreResults = [];
        var townResults = [];

        if (data.length == 2)
        {
            centreResults = data[CENTRE_INDEX].searchOrigins;
            townResults = data[TOWN_INDEX].searchOrigins;

            if (centreResults.length == 1 && townResults.length == 1)
            {
                var origin = townResults[0];

                return {
                    id : origin.name,
                    grid : {
                        north:origin.gridNorth,
                        east:origin.gridEast,
                        term:data[TOWN_INDEX].term
                    }
                };
            }
        }

        return null;
    }
});
/*
*	@Author:  Geoff Kaile
*	@Company: Razorfish London
*	@Name:    Centre display Class
*	@Description:
*	Class to display centre locations on a google map and interact with it
*/
RF.Class.CentreDisplayManager = new Class({
    Implements : [Options,RF.Class.Utilities],
    Binds       :[
                    'createRegionMarkers',
                    'createCentreMarkers',
                    'selectCentre',
                    'showResults',
                    'map_zoomChangedHandler',
                    'map_dragendHandler',
                    'infoWindow_closeclickHandler',
                    'getSmallMarker',
                    'getNormalMarker',
                    'getSearchMarker',
                    'getMarkerImage',
                    'updateMarkers',
                    'showAllMarkers',
                    'showSearchMarkers'
                ],

    json                    : null,
    centre                  : null,
    centres                 : [],
    markerSize              : 'small',
    searchResults           : null,
    directionsResults       : false,
    infoWindow              : null,
    directionsMarkers       : [],
    directionsMarkersData   : [],
    lazyOpenInfoWindow      : false,
    lazyCentre              : null,
    firstRun                : true,

    initialize : function(options) {
        this.enableLog();
        this.setOptions(options);
        this.getInitialSettings();
    },
    getInitialSettings : function() {
        var src = this.options.mapSrc.split('&');
        src.each(function(url) {
            var urlVariable = url.split('=');
            if (/center$/.test(urlVariable[0])) this.centre = urlVariable[1].split(',');
            if (urlVariable[0] == 'zoom')       this.zoom = parseInt(urlVariable[1]);
            if (urlVariable[0] == 'maptype')    this.mapType = urlVariable[1].toUpperCase();
        },this);
        src = null;
    },
    loadMap : function() {
        this.map = new google.maps.Map(this.options.container,{
            mapTypeId               : google.maps.MapTypeId[this.mapType],
            zoom                    : this.zoom,
            center                  : new google.maps.LatLng(this.centre[0],this.centre[1]),
            mapTypeControlOptions   : {
                mapTypeIds : [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE]
            },
            streetViewControl       : false
        });

        google.maps.event.addListener(
            this.map,'zoom_changed', this.map_zoomChangedHandler);

        google.maps.event.addListener(this.map,'resize',function() {
            this.map.setCenter(this.centre);
        }.bind(this));

        google.maps.event.addListener(this.map, 'idle', function(){
            if (this.firstRun)
            {
                this.firstRun = false;
                this.boot();
            }

            if (this.lazyOpenInfoWindow)
            {
                this.displayCentreById(this.lazyCentre.id);
                this.lazyOpenInfoWindow = false;
            }

            this.updateMarkersOnIE6();
        }.bind(this));

        this.createCentreMarkers();
        this.createDirectionsMarkers();
        this.displayMarkers();

    },
    boot : function(){
        var action = this.getUrlVar(this.options.actionVar);
        var centre;
        var centreId = this.getUrlVar(this.options.centreIdVar);

        if (centreId)
            centre = RF.centre.getCentreById(centreId);

        if ((action == 'getdirections') && centre)
        {
            this.bootToDirections(centre);
        }
        else if ((action == 'showcentre') && centre)
        {
            this.bootToCentre(centre);
        }
        else
        {
            this.bootFromCookie();
        }
    },
    bootToDirections : function(centre){
        RF.sidebar.resetSidebar(
            {position:centre.data.position, title:centre.data.title},
            'directions'
        );
    },
    bootToCentre : function(centre){
        this.map.setCenter(centre.marker.position);
        this.map.setZoom(this.options.zoomSearchResults);
        this.displayCentre(centre);
    },
    bootFromCookie : function(){
        RF.centreSearch.getCookie();
    },
    getUrlVar : function (name){
        var query =
            new RegExp("[\\?&]" + name + "=([^&#]*)").exec(window.location.href);

        var value = null;
        if (query)
            value = query[1];

        return value;
    },
    createDirectionsMarkers : function (){
        for (var i = 0; i < 2; i++)
            this.directionsMarkers.push(
                new google.maps.Marker({
                    icon : this.getDirectionsMarker(i),
                    clickable : false
                })
            );
    },
    /**
     * @data: Array of google.maps.MarkerOptions
     */
    showDirectionsMarkers : function (data){
        this.directionsMarkersData = data;
        this.refreshDirectionsMarkers();
    },
    refreshDirectionsMarkers : function (){
        var data = this.directionsMarkersData;

        if (!data)
            return;

        for (var i = 0, markerOptions; markerOptions = data[i]; i++)
            this.directionsMarkers[i].setOptions({
                map : this.map,
                position : markerOptions.position,
                title : markerOptions.title
            });

    },
    hideDirectionsMarkers : function (data){
        for (var i = 0; i < 2; i++)
            this.directionsMarkers[i].setMap(null);
    },
    updateMarkers : function(size){
        this.hideMarkers();
        this.markerSize = size;

        if (RF.sidebar.currentState == 'searchResults')
            this.showSearchMarkers();
        else
            this.showAllMarkers();
    },
    hideMarkers : function () {
        this.centres.each(function(centre){
            centre.marker.setMap(null);
        }, this);
    },
    showAllMarkers : function (){

        var markerFunction = this.markerSize == 'small' ?
            this.getSmallMarker : this.getNormalMarker;

        this.centres.each(function(centre, index){
            centre.data.distance = 0;
            centre.marker.setMap(this.map);
            centre.marker.setIcon(markerFunction());
        }.bind(this));
    },
    showSearchMarkers : function (){
        var i, marker, markerFunction, result, t, centre;

        markerFunction = this.markerSize == 'small' ?
            this.getSmallMarker : this.getNormalMarker;

        for (i = 0, result; result = this.searchResults[i]; i++)
        {
            centre = this.getCentreById('c'+result.id);
            marker = centre.marker;

            centre.data.distance = result.distance;

            if ((i < 10) && (this.markerSize == 'normal'))
                marker.setIcon(this.getSearchMarker(i));
            else
                marker.setIcon(markerFunction());
            marker.setMap(this.map);
        }
    },
    getSmallMarker : function(){
        return this.getMarkerImage(
            new google.maps.Size(8, 8),
            null,
            new google.maps.Point(4, 4)
        );
    },
    getNormalMarker : function (){
        return this.getMarkerImage(
            new google.maps.Size(30, 54),
            new google.maps.Point(100, 0),
            new google.maps.Point(14, 54)
        );
    },
    getSearchMarker : function (index){
        return this.getMarkerImage(
            new google.maps.Size(30, 54),
            new google.maps.Point(200, (index * 54)),
            new google.maps.Point(14, 54)
        );
    },
    getDirectionsMarker : function (offset){
        if (offset > 1)
            return null;

        return this.getMarkerImage(
            new google.maps.Size(30, 54),
            new google.maps.Point(300, (offset * 54)),
            new google.maps.Point(14, 54)
        );
    },
    getMarkerImage : function (size, origin, anchor){
        return new google.maps.MarkerImage(
            this.options.markersSpriteUrl,
            size, origin, anchor
        );
    },
    createCentreMarkers : function() {
        this.markerSize = 'small';
        this.options.centres.each(function(centre) {

            var title    = centre.getElement('.fn').get('text');

            var position =
                [
                    parseFloat(centre.getElement('.latitude').get('text')),
                    parseFloat(centre.getElement('.longitude').get('text'))
                ];

            var paddedCentreId = centre.getElement('.extra .centreId').get('text');
            while (paddedCentreId.length < 5) {
                paddedCentreId = ' ' + paddedCentreId;
            }

            this.centres.push({
                id     : centre.getProperty('id'),
                marker : new google.maps.Marker({
                    position : new google.maps.LatLng(position[0],position[1]),
                    title    : title,
                    icon     : this.getSmallMarker()
                }),
                data  : {
                    title           : title,
                    distance        : 0,
                    address         : this.cleanupAddress(centre.getElements('.address')),
                    phone           : centre.getElement('.tel span.value').get('text'),
                    opening         : centre.getElement('.opening-times').get('html'),
                    position        : position,
                    url             : centre.getElement('.extra .url').get('text'),
                    centreId        : paddedCentreId,
                    serviceBookingUrl : centre.getElement('.extra .serviceBookingURL').get('text'),
                    latestOffersURL : centre.getElement('.extra .latestOffersURL').get('text'),
                    services : {
                        newCarSales         : centre.getElement('.newCarSales').get('text') == 'true',
                        usedCarSales        : centre.getElement('.usedCarSales').get('text') == 'true',
                        authorisedRepairer  : centre.getElement('.authorisedRepairer').get('text') == 'true',
                        r8Servicing         : centre.getElement('.r8Servicing').get('text') == 'true',
                        r8Dealer            : centre.getElement('.r8Dealer').get('text') == 'true'
                    }
                }
            });
            title = null;
        },this);
    },
    cleanupAddress : function(address){
        var county = address.getElement('.county');
        var city = address.getElement('.city');
        if (city.get('text')[0] == county.get('text')[0])
            county.dispose();
        return address.get('html');
    },
    displayMarkers : function() {
        this.centres.each(function(centre) {
            centre.marker.setMap(this.map);
            google.maps.event.addListener(centre.marker,'click',function() {
                this.displayCentre(centre);
            }.bind(this));
        },this);
    },
    closeInfoWindow : function (){
        if (this.infoWindow)
        {
            this.infoWindow.close();
            this.infoWindow = null;
        }
    },
    getCentreById : function (centreId){
        for (var i=0, centre; centre = this.centres[i]; i++)
            if (centre.id == centreId)
                return centre;
        return null;
    },
    createInfoWindow : function(content) {
        var w = new google.maps.InfoWindow({
            content : content
        });
        google.maps.event.addListener(
            w, 'closeclick', this.infoWindow_closeclickHandler
        );
        return w;
    },
    displayCentre : function(centre) {
        this.closeInfoWindow();
        var html = this.createCentreContent(centre.id, centre.data);
        var w = this.createInfoWindow(html);
        RF.sidebar.selectCentreById(centre.id);
        this.infoWindow = w;
        w.open(this.map, centre.marker);
    },
    displayCentreById : function(id){
        this.displayCentre(this.getCentreById(id));
    },
    getParam: function(name){
        if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search)) {
            return decodeURIComponent(name[1]);
        }
    },
    createCentreContent : function(id, data) {
        var modelCode = this.getParam('modelCode');
        var carLineGroup = this.getParam('carLineGroup');
        var carLine = this.getParam('carLine');
        var isFleet = $$('body')[0].hasClass('fleet');
        var isDirect = $$('body')[0].hasClass('direct');
        var fleetParamsNames = ['employerPaysFuel', 'financialYearStartDate', 'modelCode', 'monthlyContribution', 'oneOffContribution', 'optionsCost', 'taxRate'];
        var fleetUrl, fleetParam, fleetParamName, fleetParams = [];
        if (isFleet) {
            for (var i = 0; i < fleetParamsNames.length; i += 1) {
                fleetParamName = fleetParamsNames[i];
                fleetParam = this.getParam(fleetParamName);
                if (fleetParam) {
                    fleetParams.push(fleetParamName + '=' + fleetParam);
                }
            }
            fleetUrl = 'contact-dealer-centre.html?centreId=' + data.centreId + '&' + fleetParams.join('&');
        }
        var html =
        [
            '<h2 class="info-title">',
            data.title
        ];

        if (data.distance) html.push(' <em>(',data.distance,')</em>');

        var address = data.address.join('');

        html.push
        (
            '</h2>',
            '<div class="centre-info">',
            '<div class="info-col-left">',
            '<p>',address,'</p>',
            '<p>',data.phone,'</p>'
        );

        // if it is a touch device there will be no room (in the iPad...) for the
        // opening hours, and scrolling isn't working inside the infowindow
        if (!RF.isEventSupported("touchstart"))
            html.push('<div class="opening-hours">',data.opening,'</div>');

        html.push(
            '</div>',
            '<div class="info-col-right">'
        );

        if (    data.services.newCarSales ||
                data.services.usedCarSales ||
                data.services.authorisedRepairer ||
                data.services.r8Servicing ||
                data.services.r8Dealer)
        {
            html.push ('<p>Services offered:</p><ul class="services-offered">');
            if (data.services.newCarSales) html.push ('<li>New cars</li>');
            if (data.services.usedCarSales) html.push ('<li>Used cars</li>');
            if (data.services.authorisedRepairer) html.push ('<li>Service & repairs</li>');
            if (data.services.r8Dealer && data.services.r8Servicing)
            {
                html.push('<li>R8 Centre & servicing</li>');
            }
            else
            {
                if (data.services.r8Dealer) html.push ('<li>R8 Centre</li>');
                if (data.services.r8Servicing) html.push ('<li>R8 servicing</li>');
            }
            html.push ('</ul>');
        }

        html.push
        (
            '<ul>',

            '<li><a href="#" title="Get Directions" ',
            'onclick="RF.sidebar.resetSidebar({position:\'',data.position,'\',title:\'', data.title, '\'}, \'directions\')"',
            '>Get Directions</a></li>',

            '<li><a href="',
            data.serviceBookingUrl,
            '" title="Book a Service">Book a Service</a></li>'
        );

        if (data.latestOffersURL != '')
        {
            html.push
            (
                '<li><a href="',
                data.latestOffersURL,
                '" title="Latest Offers">Latest Offers</a></li>'
            );
        }

        html.push('</ul>');

        if (data.url != "")
        {
          var viewDetailsLink =
            data.url +
            '?lat=' +
            data.position[0] +
            '&lng=' +
            data.position[1];
        }

        if (isFleet && fleetUrl) {
            fleetUrl = fleetUrl + '&lat=' +
            data.position[0] +
            '&lng=' +
            data.position[1];
            html.push
            (
                '<p><a href="',
                fleetUrl,
                '" title="Contact this centre" class="btn-sml contact-this-centre">Contact this centre</a></p>'
            );
        } else if (isDirect) {
            var $lbdm = $$('#centres-full-list dd.vcard').filter(function(item){
                return item.id == id;
            })[0];
            var lbdm = {};
            if ($lbdm) {
                lbdm.name = $lbdm.getElement('.lbdmName').get('text');
                lbdm.number = $lbdm.getElement('.lbdmTel').get('text').replace(/\s/g, '');
            }
            html.push('<div class="lbdm">');
            if (lbdm.name) html.push('<p>'+ lbdm.name +'</p>');
            if (lbdm.number) html.push('<p>'+ lbdm.number + '</p>');
            html.push(
                '<p style="padding-top:10px;">',
                        '<a class="btn-sml contact-this-centre" href="/fleet-sales/small-business/contact-your-local-centre.html?centreId='+ data.centreId +'&'+ window.location.search.replace('?', '') +'">Contact this centre</a>',
                    '</p>',
                '</div>'
            );
        } else if (viewDetailsLink) {
            html.push
            (
                '<p><a href="',
                viewDetailsLink,
                '" title="View Details" class="btn-sml view-details">View Details</a></p>'
            );
        }


        html.push
        (
            '</div></div>'
        );

        return new Element('div', {
            'class' : 'info-window',
            'html' : html.join('')
        });
    },
    /**
     * diplays a search results
     **/
    showResults : function(results, status) {
        if (status != 'onematch')
            this.searchResults = null;
        else
            this.searchResults = results;

        this.resetMarkers();
    },
    centreMapOnCentre : function (centre){
        this.lazyCentre = centre;
        this.map.setCenter(centre.marker.getPosition());
    },
    resetMarkers : function () {
        this.closeInfoWindow();

        if (this.searchResults)
        {
            this.lazyOpenInfoWindow = true; // will call diaplayCentre in the maps
                                            // 'idle' handler

            if (this.map.getZoom() != this.options.zoomSearchResults)
            {
                this.updateMarkers(this.getNewMarkerSize());
                this.map.setZoom(this.options.zoomSearchResults);
            }
            else
            {
                this.updateMarkers(this.markerSize);
            }
            var centre = this.getCentreById('c'+this.searchResults[0].id);
            this.centreMapOnCentre(centre);
        }
        else
        {
            this.hideMarkers();
        }
    },
    getNewMarkerSize : function () {
        return this.map.getZoom() < this.options.zoomThreshold ? 'small' : 'normal';
    },
    updateMarkersOnIE6 : function (){
        if (!RF.Browser.ie6 || this.markerSize != 'normal')
            return;

        if (RF.sidebar.currentState == 'directions')
            this.refreshDirectionsMarkers();
        else
            this.updateMarkers(this.markerSize);
    },
    /**
     * event handlers
     */
    infoWindow_closeclickHandler : function (){
        RF.sidebar.clearSelectedCentre();
        this.closeInfoWindow();
    },
    map_zoomChangedHandler : function  (){
        if (this.directionsResults)
        {
            this.updateMarkersOnIE6();
            return;
        }
        var newMarkerSize = this.getNewMarkerSize();

        if (this.markerSize != newMarkerSize)
            this.updateMarkers(newMarkerSize);
    }
});
document.addEvent('domready',function() {
    if (document.getElementById('centres')) {
        RF.sidebar = new RF.Class.Sidebar({
            container   : $('centres-sidebar'),
            open        : 'open-sidebar',
            close       : '.close-sidebar',
            tabs        : $('centres-tabs'),
            directions  : $('centre-directions'),
            origin      : document.getElement('input[name="origin"]'),
            destination : document.getElement('input[name="destination"]'),
            actualOrigin      : document.getElement('input[name="actualOrigin"]'),
            actualDestination : document.getElement('input[name="actualDestination"]'),
            switchDir   : $('switch-directions'),
            results     : $('centres-closest'),
            full        : $('centres-full-list'),
            tabBarControls  : {
                            'searchResults' : $('resultsTabButton'),
                            'allCentres'    : $('allCentresTabButton'),
                            'directions'    : $('directionsTabButton')
                        }
        });
        RF.centre = new RF.Class.CentreDisplayManager({
            mapSrc              : document.getElement('#centres img').getProperty('src'),
            container           : document.getElementById('centres'),
            centres             : document.getElements('#centres-full-list .centre-list dd'),
            sidebar             : RF.sidebar,
            markersSpriteUrl    : '/etc/designs/audi/img/gmap/gmap-marker-sprite.png',
            markerGeneric       : '/etc/designs/audi/img/gmap/gmap-marker-sprite-generic.png',
            zoomThreshold       : 8,
            zoomSearchResults   : 10,
            centreIdVar         : 'centreId',
            actionVar           : 'action'
        });
        RF.centreSearch = new RF.Class.CentreSearch({
            container       : document.getElementById('search-centre-form'),
            originSearchUrl :'/centrelocator/searchoriginsearch.html',
            gridSearchUrl   :'/centrelocator/grid.html',
            searchPrompt    : 'Postcode, Town or Centre'
        });
        RF.gMap = new RF.Class.GoogleMap({
            callback : 'RF.centre.loadMap'
        });
    }
});
