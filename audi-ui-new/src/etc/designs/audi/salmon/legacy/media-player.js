(function($){

/*

*	@Author: Stuart Thorne
*	@Company: Razorfish London
*	@Name: MediaPlayer Class
*	@Description: 
*	Class for launching and closing the Media Player. 
*	In-page Embed and Overlay (includes close function) executions exist
*	SWF or QuickTime
*/
RF.Class.MediaPlayer = new Class({
	Implements: [
		Options,
		RF.Class.Utilities
	],
    options:{
		SWF:null,
		QT:null
    },
    initialize: function(options) {
        try {
			this.setOptions(options);
			this.enableLog();
			if($defined(this.options.SWF)) {
				this.movieWidth = this.options.SWF.vars.width;
				this.movieHeight = this.options.SWF.vars.height;
				this.overlayRequired = this.options.SWF.setup.overlay;
				this.setMovieContainer();
				this.createSWF();
			}
			else if($defined(this.options.QT)) {
				this.movieWidth = this.options.QT.width;
				this.movieHeight = this.options.QT.height;
				this.overlayRequired = this.options.QT.setup.overlay;
				this.setMovieContainer();
				this.createQT();
			}
        } catch(e) {
            this.ErrorHelper({method:"RF.Class.MediaPlayer : initialize()",error:e});
        }
    },
	setMovieContainer: function() {
		/*
		*	@description:
		*		Set the HTML Container for the movie
		*/		
		if(this.overlayRequired) {
			this.overlay = this.EffectsHelper.Overlay({cssClass:'RF_Overlay'});
			this.createCloseButton();
			
			this.container = new Element('div',{
				'id':'movie-'+new Date().getTime(),
				'class':'RF_SWFContainer_VideoLoading',
				'styles':{
					'width':this.movieWidth+'px',
					'height':this.movieHeight+'px',
					'top':'50%',
					'margin':'-'+this.movieHeight/2+'px auto'
				}
			}).hide();
			
			this.wrapper = new Element('div', {
	            'id':'movie-wrapper-'+new Date().getTime(),
				'class':'RF_Wrapper'			
			}).adopt(
	            this.container
	        ).inject(document.body,'top').hide();
			this.wrapper.style.cssText = this.EffectsHelper.FullScreen();
			
			this.overlay.get('tween',{property:'opacity',duration:400}).start(0.5).chain(function() {
				this.wrapper.show();
				this.container.show();
			}.bind(this));
		} else {
			if(this.options.SWF) {
				this.container = document.id(this.options.SWF.setup.container);
			}
			else if(this.options.QT) {
				this.container = document.id(this.options.QT.container);
			}			
		}
		if(!this.container) {
			this.ErrorHelper({method:"RF.Class.MediaPlayer : setMovieContainer()", error:{message:"The HTML Container for the movie is null: "+this.container}});
		}
	},
	createCloseButton: function() {
		this.closeButton = this.createSWFCloseButton({cssClass:'RF_Overlay_CloseButton',title:'Close the media player',tabIndex:1,eventFunction:this.closePlayer.bind(this)});
		if(RF.Browser.ie6) DD_roundies.addRule('.RF_Overlay_CloseButton');
	},
	createSWF: function() {
		/*
		*	@description:
		*		Create a SWF object for all browsers
		*/		
		
		this.movie = new Swiff(this.options.SWF.setup.url, {
			container: this.container,
			id: this.options.SWF.setup.id, 
			width: this.options.SWF.vars.width,
			height: this.options.SWF.vars.height,
			params: {
				play: this.options.SWF.params.play, 
				loop: this.options.SWF.params.loop,
				quality: this.options.SWF.params.quality, 
				wMode: this.options.SWF.params.wMode, 
				allowFullScreen: this.options.SWF.params.allowFullScreen, 
				allowScriptAccess: this.options.SWF.params.allowScriptAccess
			},
			vars: {
				width: this.options.SWF.vars.vwidth,
                height: this.options.SWF.vars.vheight,
				offset: this.options.SWF.vars.offset, 
				playOnStartup: this.options.SWF.vars.playOnStartup, 
				controls: this.options.SWF.vars.controls, 
				displayType: this.options.SWF.vars.displayType,
				emo: this.options.SWF.vars.emo, 
				loop: this.options.SWF.vars.loop, 
				fullScreen: this.options.SWF.vars.fullScreen, 
				discoverMoreURL: this.options.SWF.vars.discoverMoreURL, 
				posterFrameURL: this.options.SWF.vars.posterFrameURL,
				prefixURL: this.options.SWF.vars.prefixURL, 
				videoURL: this.options.SWF.vars.videoURL
			}
		});	
		
		this.container.adopt(this.closeButton);
	},
	createQT: function() {
		/*
		*	@description:
		*		Create a QuickTime object for all browsers
		*	@arguments:
		*		None
		*/
		
		var qtHTML = '';
		/*
		*	For all browsers except Internet Explorer
		*/
		if (navigator.plugins && navigator.plugins.length) {
	        qtHTML += '<embed type="video/quicktime" src="' + this.options.QT.movie + '" width="' + this.options.QT.width + '" height="' + this.options.QT.height + '" id="' + this.options.QT.id + '"';
	        for (var param in this.options.QT.params) {
	            qtHTML += ' ' + param + '="' + this.options.QT.params[param] + '"';
	        }
	        qtHTML += '></embed>';
	    }
		/*
		*	For Internet Explorer
		*/
	    else { 
	        qtHTML += '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" width="' + this.options.QT.width + '" height="' + this.options.QT.height + '" id="' + this.options.QT.id + '">';
	        this.options.QT.params['src'] = this.options.QT.movie;
			var paramTags = '';
		    for (var param in this.options.QT.params) {
		        paramTags += '<param name="' + param + '" value="' + this.options.QT.params[param] + '" />';
		    }
			if (paramTags != '') {
	            qtHTML += paramTags;
	        }
	        qtHTML += '</object>';
	    }
	
		this.container.set('html', qtHTML);
		this.movie = this.container.getFirst();
		
		this.container.adopt(this.closeButton);
	},
	closePlayer: function() {
		/*
		*	Operation All: delete the SWF container, Wrapper and Overlay elements, then bring back undesriable elements
		*	Operation IE: we must dispose of the SWF's immediate container first, or IE throws an error. 
		*				Do not use Moo's .destroy() method on DOM elements that contain embedded objects
		*/
		
		/*
		*	If the movie is a SWF, stop the movie
		*/
		if(this.options.SWF && this.movie && this.movie.mediaPlayer_stop) {
			this.movie.mediaPlayer_stop();
		}
		
		if(RF.Browser.ie) {
			while (this.container.hasChildNodes()){
				this.container.removeChild(this.container.firstChild);
			}
			this.container.dispose();
			this.wrapper.dispose();
			this.overlay.dispose();
		} else {
			this.wrapper.destroy();
			this.overlay.get('tween', {
				property:'opacity',
				duration:400
			}).start(0).chain(function() {
				this.overlay.destroy();
			}.bind(this));
		}
	}
});

})(document.id);