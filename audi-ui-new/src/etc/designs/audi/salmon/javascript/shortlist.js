
var myShortlist = (function () {

    var _carShortListObj = [];
    var _carShortlistCookieArray = [];
    var _modelData = [];
    var currentpage;
    var modelspath = $(".shortListWrapper").data('modelpagepath'); 

    var _init = function () {
        var pathArray = window.location.pathname.split( '/' );
        currentpage = pathArray[pathArray.length-1];

        // get cookie stored data.
        _carShortlistCookieArray = _getShortListCookie();

        try{
            salmon.vehicleCarousel.updateCTA();
        }catch(e){ }

        // get models from webservice
        _getAllModelData();

        // make shortlist sticky 
        _stickyShortlist();

        // events 
        $('body').on('click', 'a.shortlistAction',function(event) {
            event.preventDefault(); 
            event.stopPropagation(); 
            msTagging.addRemoveCTA($(this));
            _updateShortList($(this).data('range'), $(this).data('carid'));
            // update the CTA for range carousel belt
            if($(this).closest("#vehicleRange").length) {
                _updateCTAshortlist($(this));
            } else {
                var item = $(this);
                if($('#vehicleRange')) {
                    try {
                        $('#vehicleRange li a.shortlistAction').each( function(){
                            if(item.data('carid') == $(this).data('carid') && item.data('range') == $(this).data('range'))
                                _updateCTAshortlist($(this));
                        });
                    } catch (e){
                        //
                    }
                }
            }
        });

        $('.infoPanel').parent().hover(function(){
            var img = $(this).find('img');
            img.data('thumbnail', img.attr('src'));
            img.attr('src',img.data('thumbnailhover'));
        },function(){
            var img = $(this).find('img');
            img.attr('src',img.data('thumbnail'));
        });

    };

    var _renderShortlist = function(range, model, action){

        var templateData = "";
        var $carList = $('#vehicleShortlist ul');

        if(!$('.carShortlist .vehicleCarouselHolder').is(':visible')){
            _openCloseShortlist();
        }  

        switch(action) {

            case "addCar" :     _removeEmptySlot();
                                $carList.prepend(_templateRender(range, model, true));
                                $carList.find("li:first").animate({ 
                                    marginLeft : "0px" 
                                    }, 500, function() { 
                                        $(this).removeClass('justAddedToShortlist');
                                        try{ 
                                            $('#vehicleShortlist').microfiche({  refresh: true  });
                                        }catch(e){
                                            //
                                        }
                                    });
                                break;

            case "removeCar" :  $carList.find("li.carId_"+model).animate({
                                        marginTop : "200px",
                                        "opacity": "0"
                                    }, 500, function() {
                                        $(this).remove();
                                        _addEmptySlot();
                                        try{ 
                                            $('#vehicleShortlist').microfiche({  refresh: true  });
                                        }catch(e){
                                            //
                                        }
                                    

                                    });  

                                break;

            case "updateCar" :  break;

            default : 
                                $.each(_carShortListObj, function(i, item) {
                                    templateData+= _templateRender(_carShortlistCookieArray[i].range, item, false);
                                                });     
                                $carList.html(templateData);
                                 _addEmptySlots();

                                try{ 
                                    $('#vehicleShortlist').microfiche();
                                    $(".shortListWrapper .vehicleCarouselHolder").hide();
                                }catch(e){
                                    //
                                }
        }
    };


    var _templateRender = function(range, model, justAdded) {
        var html;

        var removeClass = " remove";
        var addRemoveText = "<span></span>Remove from my shortlist";
        var justAddedToShortlist = (justAdded ? " justAddedToShortlist" : "");
        var isSmall = ((model.name.length > 17) ? " small" : "");
        var url = modelspath + range.toLowerCase() +"/"+ model.pageUrl;
        var isActive = (currentpage == model.pageUrl ? " active" : "");

        html = '<li class="carId_'+ model.configCode + removeClass + isActive + justAddedToShortlist +'" >';
        html+= '<a href="'+url+'" class="infoPanel" >';
        html+= '<div class="itemTitle'+isSmall+'">' + model.name + '</div>';
        html+= '<div class="fromPrice">Starting from &pound;' + model.otrPriceMin + '</div>';
        html+= '<img src="' + model.cdnImageSidePath + '" data-thumbnailhover="' + model.cdnImageFrontPath + '" data-thumbnail="' + model.cdnImageSidePath + '" width="167" height="67">';
        html+= '</a>';             
        html+= '<div class="shortListCnt">';
        html+= '<a href="#" class="shortlistAction" data-range="' + range + '" data-carid="' + model.configCode + '">';
        html+= '<span class="addRemoveLabel">' + addRemoveText + '</span>';
        html+= '</a>';
        html+= '</div>';
        html+= '</li>';
  
        return html;
    };


    var _addEmptySlots = function() {
       if(_carShortListObj.length < 5) {
            var i =0;
            while(_carShortListObj.length +i < 5) {
                _addEmptySlot();
                i++;
            }
        } 
    }

    var _addEmptySlot = function() {
        if(_carShortListObj.length < 5)
            $('#vehicleShortlist ul').append('<li class="emtpyItem"></li>');
    }

    var _removeEmptySlot = function() {
        $('#vehicleShortlist ul').find('.emtpyItem:first').remove();
    }

    var _openCloseShortlist = function(){
        $(".shortListWrapper .vehicleCarouselHolder").slideToggle();
        $(".shortListWrapper a.shotlistBtn").toggleClass('up');
    };

    
    var _stickyShortlist = function () {

        var $shortlistWrapper = $(".shortListWrapper"),
            $shortListButton = $(".shortListWrapper a.shotlistBtn"),
            $shortListCarousel = $(".shortListWrapper .vehicleCarouselHolder");
        var lastScrollPos = 0;

        $shortlistWrapper.addClass('fixFoot');                                                            
        $shortListCarousel.hide();                                                
                
        // events     
        $shortListButton.click(function () {
            if ($shortlistWrapper.hasClass('fixFoot')) {      // fix shortlist to window
                _openCloseShortlist();
                if($shortListButton.hasClass('up')) { msTagging.isopen("hide");}
                else { msTagging.isopen("open");}
            }
        });

        $(window).scroll(function(){
            var winHeight =     $(window).outerHeight(),
            scrollPos =     $(window).scrollTop(),
            footerOffset = $('#footer2').offset();
            footerHeight = $('#footer2').outerHeight();
      
            try {
                
                if ((scrollPos+winHeight) > (footerOffset.top + footerHeight -2)) {
                    $shortlistWrapper.removeClass('fixFoot');
                    $shortListCarousel.show();
                } else {  
                    if ($shortListCarousel.is(':visible')) {                            // Hide content if scrolling
                        if ($shortlistWrapper.hasClass('fixFoot')) {   
                                $shortListCarousel.slideUp('fast');
                            }
                    }                                       
                    if (!$shortlistWrapper.hasClass('fixFoot')) {                     // fix shortlist to window
                        $shortlistWrapper.addClass('fixFoot');
                        $shortListCarousel.hide();
                    }
                }
            } catch(e) {
                // console.log(e);
            }

        });

    };

    var _getModel = function (configCode) {
        var _model = false;
        // loop through model data
        $.each(_modelData, function (i, model) {
            if (model.configCode == configCode) {
                _model = model;
            }
        });
        return _model;
    };

    var _getAllModelData = function () {

       // var queryURL = "/content/audi/audi-models/models-data-for-shortlist.html";
        var queryURL = $(".shortListWrapper").data('modelsurl');

       // console.log("queryURL : "+ queryURL);

        var http_request = new XMLHttpRequest();
        try{
            // Opera 8.0+, Firefox, Chrome, Safari
            http_request = new XMLHttpRequest();
        }catch (e){
            // Internet Explorer Browsers
            try{
                http_request = new ActiveXObject("Msxml2.XMLHTTP");
            }catch (e) {
                try{
                http_request = new ActiveXObject("Microsoft.XMLHTTP");
                }catch (e){
                    console.log("MODEL DATA NOT LOADED. Please refresh the page.");
                    return false;
                }
            }
        }
       
        http_request.onreadystatechange  = function(){
            if (http_request.readyState == 4  ){
                // Javascript function $.parseJSON( to parse JSON data
                var jsonObj = $.parseJSON(http_request.responseText);
                _modelData = jsonObj;
                _mergeCookieAndServerData();
            }
        }
        http_request.open("GET", queryURL, true);
        http_request.send();
    };

   var _mergeCookieAndServerData = function () {
        if(_modelData.length > 0) {
            _carShortListObj = [];
            if(_carShortlistCookieArray.length > 0) {
                for(var i=0; i< _carShortlistCookieArray.length; i++) {
                    var item = _carShortlistCookieArray[i];
                    for(var j=0; j< _modelData.length; j++){
                        var model = _modelData[j];
                        //
                        if(item.configCode == model.configCode){
                          //add to car shortlist.
                          _carShortListObj.push(model);
                        }
                    }
                }
            }
        } 
        _renderShortlist(false, false, "onload");
    }
    /* COOKIES
    *******************************************************/
    var _getShortListCookie = function () {
        if(_getCookie("audi_car_shortlist") == "") _setCookie("audi_car_shortlist", "[]",30); 
        return $.parseJSON(decodeURIComponent(_getCookie("audi_car_shortlist")));
    }
    var _getSessionCookie = function () {
        var randomString = "Audi" + Math.ceil(Math.random()*100);
        if(_getCookie("JSESSIONID") == "") _setCookie("JSESSIONID", randomString,30);
        return decodeURIComponent(_getCookie("JSESSIONID"));
    }

    /*  Cookies GET/ SET
    ********************************************************/
    var _setCookie = function(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toGMTString()+"; path=/";
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    var _getCookie = function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    var _updateCTAshortlist = function ($elem) {
        var carid = $elem.data('carid');
        var range = $elem.data('range');
        var $item = $elem.parent().parent(); // list element which holds model data.
        var $addRemoveLabel = $elem.find('.addRemoveLabel');

        $item.toggleClass('remove');
        if($item.hasClass('remove'))
            $addRemoveLabel.html('<span></span>Remove from my shortlist');
        else 
            $addRemoveLabel.html('<span></span>Add to my shortlist');
    }

    // add, remove, modify a car in the shortlist
    var _updateShortList = function (range, configCode) {
        
        var action = "none";
        var model = configCode;
        
        if(_carShortlistCookieArray.length > 0) {
            for(var i=0; i<_carShortlistCookieArray.length; i++) {
                var item = _carShortlistCookieArray[i];
                if (item.configCode == configCode) {
                   // model exists 
                    if(item.range != range) {
                        // model exists but range is different - update range for model
                        _carShortListObj[i].range = range;
                        item.range = range;
                        _saveToServer(range, configCode, "remove");
                        _saveToServer(range, configCode, "add"); 

                        action = "updateCar";

                    } else {
                        // model exists same range - pop model
                        _carShortListObj.splice(i, 1);
                        _carShortlistCookieArray.splice(i, 1);
                        _saveToServer(range, configCode, "remove");

                        action = "removeCar";
                    }  
                }
            }
        }

        if(action == "none") { 
            // model doesn't exists - add model
            model = _getModel(configCode);
            if(typeof(model) == "object") {
                _carShortListObj.unshift(model); 
                var obj = { configCode: configCode, range: range };
                _carShortlistCookieArray.unshift(obj);
                _saveToServer(range, configCode, "add");
                action = "addCar";
            }
            
        }
        // shortlist cookie
        _setCookie("audi_car_shortlist", _carShortlistCookieArray.toJSON(),30);
        _renderShortlist(range, model, action);

    };

    var _saveToServer = function(range, configCode, action){
        var _url = "";
        switch(action) {
            case "add" :   // _url = location.protocol+"//webservices.audi.co.uk.uat.nativ-systems.com/audi-recommendations/services/shortlist/"+_getSessionCookie()+"/"+range+"/"+configCode;
                            _url = location.protocol+"//"+$(".shortListWrapper").data('addurl')+_getSessionCookie()+"/"+range+"/"+configCode;
                            break;
            case "remove" : // _url = location.protocol+"//webservices.audi.co.uk.uat.nativ-systems.com/audi-recommendations/services/shortlist/delete/"+_getSessionCookie()+"/"+range+"/"+configCode;
                            _url = location.protocol+"//"+$(".shortListWrapper").data('removeurl')+_getSessionCookie()+"/"+range+"/"+configCode;
                            break;
        }

         //TOD0
        if(navigator.userAgent.match(/msie [6]/i)){
            if (window.XDomainRequest) {
                var xdr = new XDomainRequest();
                if (xdr) {
                      xdr.onload = function() { console.log("saved to server : " + action); }
                      xdr.onerror = function() {  }
                      xdr.open('POST', _url);
                      xdr.send();
                }
            }
        }else {
         // ajax call
            $.ajax({
              type: "POST",
              crossDomain: true,
              url: _url,
              success: function() {
                    console.log("saved to server : " + action);
                }          
            });
        }
    };

    

    return {
        init: _init,
        updateCTAshortlist: _updateCTAshortlist,
        getShortListCookie: _getShortListCookie
    }
})();

var msTagging = (function() {

    var _init = function () {
        
    };

    var _addRemoveCTA = function($elem) {

        var from = "ms-tk";
        if($elem.closest("#vehicleRange").length) {
            from = "atsl";
        }

        var $item = $elem.parent().parent(); // list element which holds model data.
        var addRemoveLabel = $elem.find('.addRemoveLabel').text();
        var cta = "";
        var carid = $elem.data("carid");
        var carName = $item.find(".itemTitle").text();
       // var range = $elem.data("range");

        (addRemoveLabel == "Add to my shortlist" ? cta = "added" :  cta = "removed" );

        var listPos = $item.index() + 1 ;
        var prop29 = s.pageName +"-"+from+"-"+carName+"-"+ cta +"-" + listPos;
        siteTagging.set($elem, "Shortlist", {
                prop29 : prop29
            });   

    };

    var _isopen = function(state) {

        var prop29 = s.pageName + "-ms-tk-" + state;
        siteTagging.set($(".carShortlist"), "Shortlist", {
                prop29 : prop29
            });   
    };
   
    
    return {
        init: _init,
        addRemoveCTA: _addRemoveCTA,
        isopen : _isopen
    }


})();

var siteTagging = (function() {

    var _set = function(element, linkName, properties, delay){
        // Disable links by using in click event as "return salmon.tracking.set(blah...);"
        // Add extra delay for tagging to happen by specifying a delay (ms) time.
        try { 
            var s = s_gi(s_account);

            // reset the lists.
            s["linkTrackEvents"] = "";
            s["linkTrackVars"] = "";

            for( var p in properties ) {
                if(String(p) == "events") {
                    s["linkTrackEvents"] += String("," + properties[p]);
                }
                s[p] = properties[p];
                s["linkTrackVars"] += String("," + p);
            }
             s.tl(element, "o", linkName);
            if(arguments.length > 3) {
                setTimeout(function() {
                    if (element["href"]) {
                        location.href = element.href;
                    }
                }, delay);
            }
        }
        catch(e) {
            console.log("s_code or s variable not present.");
        }
        return false;
    }

    var _trimForTagging = function(str) {
        var temp = str.replace(/[^\w]/gi, '');
            temp = temp.charAt(0).toLowerCase() + temp.slice(1);

        return temp;
    }

    return {
        set: _set,
        trimForTagging: _trimForTagging
    }

})();


/* Check for JQuery and then load the ShortList  
***************************************************************/

var loadJquery = function() {
    var script     = document.createElement('script');
        script.src = "/etc/designs/audi/salmon/javascript/third-party/jquery-1.10.2.min.js";
    
    var head = document.getElementsByTagName('head')[0],
    done = false;
    
    script.onload = script.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
            done = true;
            initShortList();
            script.onload = script.onreadystatechange = null;
            head.removeChild(script);
        };
    };
    head.appendChild(script);
};

// Only do anything if jQuery isn't defined
if (typeof jQuery == 'undefined') { loadJquery(); } 
else { // jQuery was already loaded
    initShortList();
};

function initShortList() {
    $.getScript( "/etc/designs/audi/salmon/javascript/third-party/jquery.xdomainrequest.min.js");
    $.getScript( "/etc/designs/audi/salmon/javascript/third-party/carouselPlugin.js", function(){
        myShortlist.init(); 
        msTagging.init();
    });
}