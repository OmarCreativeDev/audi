<%@include file="/libs/wcm/global.jsp" %>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />

    <cq:include script="/apps/audi/components/meta-data/meta-data.jsp"/> 
    <cq:include script="/libs/wcm/init/init.jsp"/>

    <link rel="shortcut icon" type="image/x-icon" href="/etc/designs/audi/img/favicon.ico"/>

    <% // pull in css & js based on the template name
    String templateName = currentPage.getTemplate().getName(); 
    String rootPath = "";
    if (WCMMode.fromRequest(request) == WCMMode.EDIT || WCMMode.fromRequest(request) == WCMMode.DESIGN) { 
    	rootPath = "https://"+request.getServerName();     
    %>   
       	<style>
			div.parsys_column .section {
				overflow:hidden;
				width: 100%;
				height: 100%;
			}
		</style>

    <% }  %> 

    <!-- console fix -->
    <script>
    	if (typeof console !== 'object') console = {};
    	if (typeof console.log !== 'function') console.log = function(){};
    </script>

	<!-- NAVIGATION + MooTools COMBINED - top and main navigation combined code  -->
	<script type="text/javascript" src="/audi-ui/global/scripts/navigation-combined.js"></script>

	<!--
	 	##################### Styles #######################
	 -->

	<link rel="stylesheet" type="text/css" href="/etc/designs/audi/salmon/css/owners-area/owners-reset.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="/etc/designs/audi/salmon/css/legacy.css" media="all"/>


	<!-- local definitions for css/javascript to be included in the <head> of the <html> file -->
	<!-- base styles -->
	<link rel="stylesheet" href="<%=rootPath%>/audi-ui/global/styles/css/style.css">

	<!-- app styles -->
	<link rel="stylesheet" href="<%=rootPath%>/audi-ui/apps/myaudi-login/styles/css/myaudi-login.css">
          
	<cq:include script="title.jsp"/>

</head>
