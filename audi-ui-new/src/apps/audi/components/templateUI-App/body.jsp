<%@include file="/libs/wcm/global.jsp" %><%@page import="com.day.cq.wcm.api.WCMMode"%><%
    WCMMode mode = WCMMode.fromRequest(request);
    String pageTitle = (currentPage.getTitle()!=null ? currentPage.getTitle():currentPage.getName());
    int modelLandingDepth = 5;
    String HOMEPAGE_TAKEOVER_PATH = "/content/audi/home/takeover";    
    Boolean HomepageShow = new Boolean(request.getParameter("dont-show") == null && resourceResolver.getResource(HOMEPAGE_TAKEOVER_PATH) != null);
%>
<%@page import="com.day.text.Text, 
    java.util.*,  
    com.day.cq.wcm.api.Page,
    com.day.cq.wcm.api.PageFilter,
    uk.co.audi.cq5.web.SlingRequestUtils" %>
<!--[if IE 8]>
    <body class="ie ie8">
<![endif]-->
<!--[if IE 9]>
    <body class="ie ie9">    
<![endif]-->
<!--[if gt IE 8]><!-->
    <body>    
<!--<![endif]-->
  
    <div id="top"></div>
    <div class="skip-links">
        <a name="notes-about-this-content">This page contains content which may update dynamically, depending on links or controls you operate. The dynamically updated content will be within the main content area, and you may use the following link to skip to this main content.</a>
        <a id="skip-nav" href="#content">Skip to main content</a>
    </div>

    <link rel="stylesheet" href="/audi-ui/apps/myaudi-header/styles/css/myaudi-header.css">
    <div class="appContainer" id="myAudiHeader">
        <div ui-view></div>
    </div>
    
    <div>       
      <cq:include script="navigation.jsp"/> 
    </div>

    <div class="container">
        <noscript> <div id="update-javascript"> <h2>See the site in all its glory</h2> <p>To make the most of the Audi website, you need to have JavaScript enabled in your browser.</p> </div> </noscript>
    </div>
    
    <!-- APPLICATIONs CONTAINER BEGIN -->
    <cq:include path="appsContainer" resourceType="foundation/components/parsys" />

    <!-- APPLICATIONs CONTAINER END -->

    <cq:include script="app-includes.jsp"/>

    <div>       
        <cq:include path="footer" resourceType="audi/components/footer" /> 
    </div>
    <cq:include path="tracking-and-analytics" resourceType="audi/components/tracking-and-analytics" />    
    
</body>


  