<%@include file="/libs/wcm/global.jsp"%>
<%@page import="com.day.cq.wcm.api.WCMMode"%>

<!-- standard APP INCLUDES -->

<!--
<script src="/audi-ui/global/bower-components/bootstrap-sass-official/js/..."></script>
-->

<!--[if lt IE 9]>
<script src="/audi-ui/global/bower-components/es5-shim/es5-shim.js"></script>
<script src="/audi-ui/global/bower-components/json3/lib/json3.min.js"></script>
<![endif]-->

<% 
    String rootPath = "";
    if (WCMMode.fromRequest(request) == WCMMode.EDIT) { 
    	rootPath = "https://"+request.getServerName();     
    }
%>   

<script src="<%=rootPath%>/audi-ui/global/bower-components/requirejs/require.js"></script>
<script src="<%=rootPath%>/audi-ui/require-config.js"></script>
<script type="text/javascript">

require(['global/scripts/global'], function (global) {
    global.loadApps();
});

</script>
