'use strict';

module.exports = function (grunt) {

	var gruntConfig = {
		bowerComponents: 'src/etc/audi-ui/global/bower-components'
	};


	/* to add a new app add to the following areas: (look for "APPS" in each section)
	 - watch
	 - compassMultiple
	 - concat
	
	 */

	

	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-bower-task');
	grunt.loadNpmTasks('grunt-compass-multiple');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-compass');
	//grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-concat-sourcemaps');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-requirejs');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-ng-annotate');
	grunt.loadNpmTasks('grunt-protractor-runner');
	grunt.loadNpmTasks('grunt-protractor-webdriver');
	grunt.loadNpmTasks('protractor-html-screenshot-reporter');
	grunt.loadNpmTasks('requirejs');
	grunt.loadNpmTasks('time-grunt');

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Define the configuration for all the tasks
	grunt.initConfig({

		// Project settings
		config: gruntConfig,

		protractor_webdriver: {
			all: {}
		},
		protractor: {
			options: {
				keepAlive: false,
				configFile: 'src/etc/audi-ui/apps/myaudi-login/tests/e2e/conf.js'
			},
			run: {}
		},

		bower: {
			install: {
				options: {
					targetDir: gruntConfig.bowerComponents,
					layout: 'byComponent',
					install: true,
					verbose: false,
					cleanTargetDir: false,
					cleanBowerDir: false,
					bowerOptions: {}
				}
			}
		},

		// Watches files for changes and runs tasks based on the changed files
		watch: {
			audiApp: {
				options: {
					livereload: true
				},
				files: ['src/etc/audi-ui/apps/audi-app/src/**/*.js'],
				tasks: ['build:audiApp']
			},
			/* APPS */
			app1: {
				options: {
					livereload: true
				},
				files: ['src/etc/audi-ui/apps/example-apps/app1/src/**/*.js'],
				tasks: ['build:app1']
			},
			app2: {
				options: {
					livereload: true
				},
				files: ['src/etc/audi-ui/apps/example-apps/app2/src/**/*.js'],
				tasks: ['build:app2']
			},
			myAudi: {
				options: {
					livereload: true
				},
				files: ['src/etc/audi-ui/apps/myAudi/src/**/*.js'],
				tasks: ['build:myAudi']
			},
			
			myAudiLogin: {
				options: {
					livereload: true
				},
				files: ['src/etc/audi-ui/apps/myaudi-login/**/*.js',
						'!src/etc/audi-ui/apps/myaudi-login/*.min.js'],
				tasks: ['build:myAudiLogin']
			},
			myAudiHeader: {
				options: {
					livereload: true
				},
				files: ['src/etc/audi-ui/apps/myaudi-header/**/*.js',
				'!src/etc/audi-ui/apps/myaudi-header/*.min.js'],
				tasks: ['build:myAudiHeader']
			},
			osb: {
				options: {
					livereload: true
				},
				files: ['src/etc/audi-ui/apps/osb/**/*.js','!src/etc/audi-ui/apps/osb/*.min.js'],
				tasks: ['build:osb']
			},
			modelCarousel: {
				options: {
					livereload: true
				},
				files: ['src/etc/audi-ui/apps/modelCarousel/src/**/*.js'],
				tasks: ['build:modelCarousel']
			},
			financeCalculator: {
				options: {
					livereload: true
				},
				files: ['src/etc/audi-ui/apps/finance-calculator/**/*.js',
				'!src/etc/audi-ui/apps/finance-calculator/*.min.js'],
				tasks: ['build:financeCalculator']
			},



			
			compass: {
				files: ['src/**/*.scss'],
				tasks: ['compassMultiple', 'autoprefixer']
			},
			livereload: {
				options: {
					livereload: '<%= connect.options.livereload %>'
				},
				files: [
					'src/**/*.html',
					'src/**/*.css',
					'src/**/*.{png,jpg,jpeg,gif,webp,svg}'
				]
			}
		},

		// The actual grunt server settings
		connect: {
			options: {
				//open: 'https://youraudi:9090',
				base: 'src',
				port: 9090,
				// Jamie's IP for IE testing
				//hostname: '172.17.18.194',
				hostname: 'localhost',
				livereload: 35729,
				// uncomment to run sit in secure mode
				// protocol: 'https'
			},
			livereload: {
				options: {
					open: true,
					base: 'src'
				}
			},
			dist: {
				options: {
					port: 9095,
					base: 'dist',
					keepalive: true
				}
			},
			test: {
				options: {
					port: 9090,
					base: 'dist',
					keepalive: false
				}
			}
		},

		// Empties folders to start fresh
		clean: {
			dist: [ 'dist/*' ],
			tmp:  [ '.tmp/*' ]
		},

		// Add vendor prefixed styles
		autoprefixer: {
			options: {
				browsers: ['last 1 version']
			},
			dist: {
				files: [{
					expand: true,
					cwd: '.tmp/styles/',
					src: '{,*/}*.css',
					dest: '.tmp/styles/'
				}]
			}
		},

		// Compiles Sass to CSS and generates necessary files if requested
		compassMultiple: {
			all: {
				options: {
					multiple: [
						{
							// global settings
							sassDir:    'src/etc/audi-ui/global/styles/scss',
							cssDir:     'src/etc/audi-ui/global/styles/css',
							importPath: '<%= config.bowerComponents %>'
						},
						/* APPS */
						{
							// myAudi
							sassDir:    'src/etc/audi-ui/apps/myAudi/src/styles/scss',
							cssDir:     'src/etc/audi-ui/apps/myAudi/dist/styles/css',
							importPath: '<%= config.bowerComponents %>'
						},
						{
							// myAudi login
							sassDir:    'src/etc/audi-ui/apps/myaudi-login/styles/scss',
							cssDir:     'src/etc/audi-ui/apps/myaudi-login/styles/css',
							importPath: '<%= config.bowerComponents %>'
						},
						{
							// myAudi header
							sassDir:    'src/etc/audi-ui/apps/myaudi-header/styles/scss',
							cssDir:     'src/etc/audi-ui/apps/myaudi-header/styles/css',
							importPath: '<%= config.bowerComponents %>'
						},
						{
							// TT-2014 page
							sassDir:    'src/etc/audi-ui/pages/TT-2014/styles/scss',
							cssDir:     'src/etc/audi-ui/pages/TT-2014/styles/css',
							importPath: '<%= config.bowerComponents %>'
						},
						{
							// Header page
							sassDir:    'src/etc/audi-ui/pages/header/styles/scss',
							cssDir:     'src/etc/audi-ui/pages/header/styles/css',
							importPath: '<%= config.bowerComponents %>'
						},
						{
							// Footer
							sassDir:    'src/etc/audi-ui/pages/footer/styles/scss',
							cssDir:     'src/etc/audi-ui/pages/footer/styles/css',
							importPath: '<%= config.bowerComponents %>'
						},
						{
							// modelCarousel
							sassDir:    'src/etc/audi-ui/apps/modelCarousel/src/styles/scss',
							cssDir:     'src/etc/audi-ui/apps/modelCarousel/dist/styles/css',
							importPath: '<%= config.bowerComponents %>'
						},
						{
							// osb
							sassDir:    'src/etc/audi-ui/apps/osb/styles/scss',
							cssDir:     'src/etc/audi-ui/apps/osb/styles/css',
							importPath:  '<%= config.bowerComponents %>'
						},
						{
							// financeCalculator
							sassDir:    'src/etc/audi-ui/apps/finance-calculator/styles/scss',
							cssDir:     'src/etc/audi-ui/apps/finance-calculator/styles/css',
							importPath:  '<%= config.bowerComponents %>'
						}
					]
				}
			}
		},
		compass: {
			server: {
				options: {
					debugInfo: true
				}
			}
		},

		// Copies remaining files to places other tasks can use
		copy: {
			tmpToDist: {
				files: [
					{
						expand: true,
						dot: true,
						cwd: '.tmp',
						dest: 'dist',
						src: [
							'**',
							'!**/sass/**',
							'!**/scss/**'
						]
					}
				]
			}
		},

/*
		requirejs: {
			compile: {
				options: require('./requirejs-build-config')
			}
		},

		ngAnnotate: {
			options: {
				singleQuotes: true,
			},
			all: {
				files: {
					'.tmp/etc/audi-ui/apps/myAudi/myAudi-bootstrap.js': ['.tmp/etc/audi-ui/apps/myAudi/myAudi-bootstrap.js'],
					'.tmp/etc/audi-ui/apps/modelCarousel/modelCarousel-bootstrap.js': ['.tmp/etc/audi-ui/apps/modelCarousel/modelCarousel-bootstrap.js'],
					'.tmp/etc/audi-ui/apps/myaudi-login/myaudilogin-bootstrap.js': ['.tmp/etc/audi-ui/apps/myaudi-login/myaudilogin-bootstrap.js'],
					'.tmp/etc/audi-ui/apps/myaudi-header/myaudiheader-bootstrap.js': ['.tmp/etc/audi-ui/apps/myaudi-header/myaudiheader-bootstrap.js'],
					'.tmp/etc/audi-ui/global/scripts/global.js': ['.tmp/etc/audi-ui/global/scripts/global.js']
				},
			}
		},

		uglify: {
			options: {
				compress: {
					drop_console: true
				}
			},
			all: {
				files: {
					'.tmp/etc/audi-ui/apps/myAudi/myAudi-bootstrap.js': ['.tmp/etc/audi-ui/apps/myAudi/myAudi-bootstrap.js'],
					'.tmp/etc/audi-ui/apps/myaudi-login/myaudilogin-bootstrap.js': ['.tmp/etc/audi-ui/apps/myaudi-login/myaudilogin-bootstrap.js'],
					'.tmp/etc/audi-ui/global/scripts/global.js': ['.tmp/etc/audi-ui/global/scripts/global.js'],
					'.tmp/etc/audi-ui/apps/myaudi-header/myaudiheader-bootstrap.js': ['.tmp/etc/audi-ui/apps/myaudi-header/myaudiheader-bootstrap.js'],
					'src/etc/designs/audi/salmon/legacy-combined.min.js': ['src/etc/designs/audi/salmon/legacy-combined.js'],
					'.tmp/etc/audi-ui/apps/modelCarousel/modelCarousel-bootstrap.js': ['.tmp/etc/audi-ui/apps/modelCarousel/modelCarousel-bootstrap.js']
				}
			}
		},
*/

		concat: {
			options: {
				separator: ';'
			},

			legacy: {
				src: [
					'src/etc/designs/audi/salmon/legacy/rf-namespace.js',
					'src/etc/designs/audi/salmon/legacy/mooTools.js',
					'src/etc/designs/audi/salmon/legacy/mooToolsUtilities.js',
					'src/etc/designs/audi/salmon/legacy/utilities.js',
					'src/etc/designs/audi/salmon/legacy/tracking-hound.js',
					'src/etc/designs/audi/salmon/legacy/meet-the-range-promo.js',
					'src/etc/designs/audi/salmon/legacy/navigation-content.js',
					'src/etc/designs/audi/salmon/legacy/top-navigation.js',
					'src/etc/designs/audi/salmon/legacy/main-navigation.js',
					'src/etc/designs/audi/salmon/legacy/target-blank-replace.js',
					'src/etc/designs/audi/salmon/legacy/omniture-tracking-object.js',
					'src/etc/designs/audi/salmon/legacy/omniture-tracking-manager.js',
					'src/etc/designs/audi/salmon/legacy/fly-over-navigation.js',
					'src/etc/designs/audi/salmon/legacy/navigation-panel.js',
					'src/etc/designs/audi/salmon/legacy/unknown(temp).js'
				],
				dest: 'src/etc/audi-ui/global/scripts/navigation-combined.js',
			},

			// APPS
			globalJs: require('./src/etc/audi-ui/global/scripts/build-config'),
			audiApp: require('./src/etc/audi-ui/apps/audi-app/build-config'),
			app1: require('./src/etc/audi-ui/apps/example-apps/app1/build-config'),
			app2: require('./src/etc/audi-ui/apps/example-apps/app2/build-config'),
			myAudi: require('./src/etc/audi-ui/apps/myAudi/build-config'),
			osb: require('./src/etc/audi-ui/apps/osb/build-config'),
			modelCarousel: require('./src/etc/audi-ui/apps/modelCarousel/build-config'),
			financeCalculator: require('./src/etc/audi-ui/apps/finance-calculator/build-config'),
			myAudiLogin: require('./src/etc/audi-ui/apps/myaudi-login/build-config'),
			myAudiHeader: require('./src/etc/audi-ui/apps/myaudi-header/build-config')




		}

	});


	grunt.registerTask('serve', function (target) {
		if (target === 'dist') {
			grunt.task.run([
				'build',
				'connect:dist'
			]);
		} else {
			grunt.task.run([
				'bower:install',    // Install dependencies
				'build',
				'connect:livereload',
				'watch'
			]);
		}
	});

	grunt.registerTask('build', function (target) {

		if (target === 'audiApp') {
			grunt.task.run([
				'concat:audiApp'
			]);
		} else if (target === 'app1') {
			grunt.task.run([
				'concat:app1'
			]);
		} else if (target === 'app2') {
			grunt.task.run([
				'concat:app2' 
			]);
		} else if (target === 'myAudi') {
			grunt.task.run([
				'concat:myAudi'
			]);
		} else if (target === 'myAudiLogin') {
			grunt.task.run([
				'concat:myAudiLogin'
			]);
		}  else if (target === 'myAudiHeader') {
			grunt.task.run([
				'concat:myAudiHeader'
			]);
		} else if (target === 'osb') {
			grunt.task.run([
				'concat:osb'
			]);
		} else if (target === 'financeCalculator') {
			grunt.task.run([
				'concat:financeCalculator'
			]);
		} else if (target === 'modelCarousel') {
			grunt.task.run([
				'concat:modelCarousel'
			]);
		} else if (target === 'dist') {
			grunt.task.run([
				'bower:install',    // Install dependencies
				'clean:tmp',		// Clean the "dist" directory
				'clean:dist',       // Clean the ".tmp" directory
				'compassMultiple',  // Run compass to regenerate CSS files
				'concat:legacy',    // Concatenate legacy scripts
				'requirejs',
				'ngAnnotate',
				'uglify',
				'copy:tmpToDist'    // Copy files from ".tmp" to "dist"
			]);
		} else {
			grunt.task.run([
				'concat:globalJs',
				'concat:audiApp',
				'concat:app1',
				'concat:app2',
				'concat:myAudi',
				'concat:myAudiLogin',
				'concat:myAudiHeader',
				'concat:osb',
				'concat:financeCalculator',
				'concat:myAudi',
				'concat:modelCarousel',
				'compassMultiple'   // Run compass to regenerate CSS files
			]);
		}
	});

	grunt.registerTask('legacy', [
		'concat:legacy',    // Concatenate legacy scripts
	]);

	grunt.registerTask('default', [
		'build'
	]);

	grunt.registerTask('test', function (target) {

		grunt.task.run([
			'build'
		]);

		if (target === 'e2e') {

			grunt.task.run([
				'connect:test',
				'protractor_webdriver',
				'protractor:run'
			]);

		}
	});
};