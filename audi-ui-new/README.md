# AudiUI

## Installation

To install all required components run:

	npm install
	bower install


	MUST HAVE ALREADY INSTALLED:
	1) Node.js  AND GIT   then run the following from git bash cmd
		2) $ npm install -g grunt
		3) $ npm install -g grunt-cli
		4) $ npm install -g bower
		5) $ npm compass
		6) navigate to the folder that this folder is in!
		7) $ npm install
		8) $ bower install
		9) then kick it all off with    "$ grunt serve"


## Grunt tasks

Start a local server:

	grunt serve

Build the project:

	grunt build


## Troubleshooting

Error in console = Running "watch" task Waiting...Warning: EMFILE, too many open files 'src'

Try this

1.	delete the node_modules dir
2.	delete the bower components
3.	npm install
4.	bower install



## Protractor

TAKEN FROM : https://github.com/angular/protractor/blob/master/docs/tutorial.md 
Prerequisites

Protractor is a Node.js program. To run, you will need to have Node.js installed. You will download Protractor package using npm, which comes with Node.js. Check the version of Node.js you have by running node --version. It should be greater than v0.10.0.

By default, Protractor uses the Jasmine test framework for its testing interface. This tutorial assumes some familiarity with Jasmine.

This tutorial will set up a test using a local standalone Selenium Server to control browsers. You will need to have Java Development Kit (JDK) installed to run the Selenium Server. Check this by running java -version from the command line.


Setup

Use npm to install Protractor globally with:

								$ npm install -g protractor

This will install two command line tools, protractor and webdriver-manager. Try running protractor --version to make sure it's working.

The webdriver-manager is a helper tool to easily get an instance of a Selenium Server running. Use it to download the necessary binaries with:

								$ webdriver-manager update


Now start up a server with:

								$ webdriver-manager start


then
 $ protractor  path_to_file.conf





This will start up a Selenium Server and will output a bunch of info logs. Your Protractor test will send requests to this server to control a local browser. Leave this server running throughout the tutorial. You can see information about the status of the server at http://localhost:4444/wd/hub.









/SETTING UP A NEW APP
///////////////////////
Add in the Gruntfile.js under compassMultiple the app to enable compass to work on the css files

under /scripts/global.js add in the app 

if(document.getElementById('finance-calculator') !=null){
							console.log('financeCalculator-bootstrap')
						require(['financeCalculator-bootstrap']);
					}
					
