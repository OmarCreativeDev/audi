module.exports = {
	appDir:          'src',
	baseUrl:         'etc/audi-ui/',
	mainConfigFile:  'src/etc/audi-ui/require-config.js',
	dir:             '.tmp',
	optimize:        'none',
	preserveLicenseComments: false,
	modules: [
		{
			name: 'global/scripts/global',
		},
		{
			name: 'apps/myaudi-login/myaudilogin-bootstrap',
			exclude: ['global/scripts/global']
		},
		{
			name: 'apps/myAudi/myAudi-bootstrap',
			exclude: ['global/scripts/global']
		},
		{
			name: 'apps/myaudi-header/myaudiheader-bootstrap',
			exclude: ['global/scripts/global']
		},
		{
			name: 'apps/modelCarousel/modelCarousel-bootstrap',
			exclude: ['global/scripts/global']
		}
	]
};