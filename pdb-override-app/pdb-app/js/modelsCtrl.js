
Cars.controller('modelsCtrl', function ($scope,$rootScope, $filter, modelService, oldPdbService, environmentService, $state, $modal, dialogs,$timeout) {
 


 	var primaryModelService = new modelService($state.params.environment);
    

  	$scope.Models = primaryModelService.modelsDataObj; 
	
    $scope.init = function () {
    	    dialogs.wait('Loading models','Loading models...',50);
   
 			primaryModelService.getModels().then(function(){
 				$rootScope.$broadcast('dialogs.wait.complete');
 			});
		}
	$scope.init();


    $scope.isModelsLoaded = function() {
    	try{
       		 return typeof $scope.Models[0].name === "string" ? true: false;
    	}catch(e){
    		return false;
    	}
    }


    var createNewModel = function(model){
 
		dialogs.wait('Loading models','Fetching model data from old PDB...',25);

		oldPdbService.getModel(model.configCode).then(function() {

			$rootScope.$broadcast('dialogs.wait.progress',{'progress' : 50});

			angular.copy(oldPdbService.modelDataObj, primaryModelService.modelDataObj);

			$rootScope.$broadcast('dialogs.wait.progress',{ msg:'Creating new entry...', 'progress' : 75});

			primaryModelService.saveModel().then(
				function(){
					$rootScope.$broadcast('dialogs.wait.progress',{msg:'Completed. Redirecting to new model page', progress : 100});

					$state.go('environment.tabs.models.model', {model:primaryModelService.modelDataObj.configCode});
				},function(){
					$rootScope.$broadcast('dialogs.wait.complete');
					noty({
						text: 'Error saving model: ' +primaryModelService.modelDataObj.configCode,
						type:'error'
					});
			})
		});


    }

    $scope.checkModelsInOldPDB = function(){



 		var modalInstance = $modal.open({
	        templateUrl: 'partials/oldPdbModels.html',
	        scope:false,
	        controller: function ($scope, $state, $modalInstance, environmentService, oldPdbService) {
	      	console.log('$scope.Models',$scope.Models)
	        	$scope.oldModels = oldPdbService.modelsDataObj;
	       
	        	oldPdbService.getModels().then(function(){
	        		angular.forEach(oldPdbService.modelsDataObj,function(oldModel){
	        			angular.forEach(primaryModelService.modelsDataObj,function(newModel){
	        			
	        					if(oldModel.configCode == newModel.configCode){
	        						oldModel.hidden =  true;
	        					}
	        					else if(oldModel.hidden !=true){
	        								oldModel.hidden =false;
	        							}
	        			})
	        		});
	        
	        			console.log('oldModels',oldPdbService.modelsDataObj)
	        	});



	        	$scope.selectOldModel=function(model){
            				
								createNewModel(model);
	        			         $modalInstance.close();

	        	}

	        	$scope.createAll = function(){

	        		angular.forEach($scope.oldModels,function(model,idx){
	        			$timeout(function(){
	        				console.log('create',model.configCode)
	        					if(model.configCode){
								createNewModel(model);
							}
	        			},5000*idx);
	        		


	        		});

	        	}



            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        },
        size: 'sm'
    }); 


    };
});
