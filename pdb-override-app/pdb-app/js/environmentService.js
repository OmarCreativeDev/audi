// 

Cars.factory('environmentService', function ($resource,$filter, $http, $q, $location, authService,$timeout,$modal) {

   var _getEnvironments = function(){


/*[
  {
    "name": "PDB Web Services",
    "environmentUrls": [
      {
        "name": "UAT",
        "url": "https://webservices.audi.co.uk.uat.nativ-systems.com/pdb-webservices"
      },
      {
        "name": "PRELIVE",
        "url": "https://webservices.audi.co.uk.prelive.nativ-systems.com/pdb-webservices"
      },
      {
        "name": "LIVE",
        "url": "https://webservices.audi.co.uk/pdb-webservices"
      }
    ]
  },
  {
    "name": "PDB Override",
    "environmentUrls": [
      {
        "name": "UAT",
        "url": "https://webservices.audi.co.uk.uat.nativ-systems.com/pdb-override"
      },
      {
        "name": "PRELIVE",
        "url": "https://webservices.audi.co.uk.prelive.nativ-systems.com/pdb-override"
      },
      {
        "name": "LIVE",
        "url": "https://webservices.audi.co.uk/pdb-override"
      }
    ]
  }
]

*/

                    var deferred = $q.defer();
                        $http({
                            method: 'GET',
                            url: 'json/environments.json'
                        })
                            .success(function (result, status) {
                                console.log('environments loaded', result);
                                // copy onto the object

                                 var selectedOld = $filter('filter')(result, {
                                            name: "PDB Web Services"
                                        }, false);
                                 _oldPdb = selectedOld[0].environmentUrls;

                                 var selectedNew = $filter('filter')(result, {
                                            name: "PDB Override"
                                        }, false);

                                        angular.forEach(selectedNew[0].environmentUrls,function(EUrls){
                                            EUrls.urls  = {
                                                ranges: EUrls.url +"/rest/ranges/",
                                                models: EUrls.url +"/rest/models/",
                                                model: EUrls.url +"/rest/models/"
                                             };
                                        });
                                   

                          

                                         _environments = selectedNew[0].environmentUrls;
                                
                            
                                    console.log('_environments',_environments);
                                deferred.resolve();
                            }).
                        error(function (data, status) {
                            deferred.reject();
                        });

                        return deferred.promise;




    }




var _oldPdb =
    {
        name:"Old PDB",
        urls:{
          ranges: "",
          models: "https://webservices.audi.co.uk.uat.nativ-systems.com/pdb-webservices/services/rest/pdb/getCarlineReferenceValues",
          model: "https://webservices.audi.co.uk.uat.nativ-systems.com/pdb-webservices/services/rest/pdb/getModelDetails?model="
             
        }
    };
/*
   var _oldPdb =
    {
        name:"Old PDB",
        urls:{
          ranges: "",
          models: "json/oldpdb/oldmodels.json",
          model: "json/oldpdb/oldmodel.json?"
             
        }
    };
*/



 
	var _environmentsOL = [
    {
        name:"UAT",
        urls:{
            ranges: "https://webservices.audi.co.uk.uat.nativ-systems.com/pdb-override/rest/v1/ranges/",
            models: "https://webservices.audi.co.uk.uat.nativ-systems.com/pdb-override/rest/v1/models/",
            model : "https://webservices.audi.co.uk.uat.nativ-systems.com/pdb-override/rest/v1/models/"
        }
    },
    {
        name:"Yasin",
        urls:{
            ranges: "http://172.17.18.116:8081/pdb-override/rest/ranges/",
            models: "http://172.17.18.116:8081/pdb-override/rest/models/",
            model: "http://172.17.18.116:8081/pdb-override/rest/models/"

        }
    },
    {
        name:"Local",
        urls:{
             ranges: "/pdb-app/json/ranges.json",
             models: "json/models.json?",
             model: "json/trimData_clean.json?"
        }
    }
    ];


var _environments = [
      {
        "name": "UAT",
        "url": "https://webservices.audi.co.uk.uat.nativ-systems.com/pdb-override/rest/v1",
      },
      {
        "name": "PRELIVE",
        "url": "https://webservices.audi.co.uk.prelive.nativ-systems.com/pdb-override/rest/v1"
      },
      {
        "name": "LIVE",
        "url": "https://webservices.audi.co.uk/pdb-override/rest/v1"
      },
      {
        "name": "LOCAL",
        "url": "/pdb-app/json"
      }
    ];


    var _getEnvironment = function(name){
      if(_environments.length <1){
       return false;
      }
           var selectedEnv = $filter('filter')(_environments, {
                name: name
            },false);




           return  selectedEnv[0];
    };


     var _setLogin = function(name,loginObj){
   
       angular.forEach(_environments,function(item){
        if(item.name==name){
            item.login = loginObj;
            returnOb  = item;
        }
       });




           return  returnOb;
    };

    var logout = function(){
        angular.forEach(_environments,function(item){
          try{
              item.login = {unauthorised:true};
          }catch(e){}
        })

    };



    var checkAuthorisation = function(environment){
       var deferred = $q.defer();
                var promtForLogin = true;
                        try{
                            if( environment.login.unauthorised==false || typeof environment.login.unauthorised=="undefined"){
                                promtForLogin = false;
                            }
                        }catch(e){ }

                         if(promtForLogin) {

                            environment.login = {
                                unauthorised:true
                            };


                     var modalInstance = $modal.open({
                                templateUrl: 'partials/login.html',
                                controller: function($scope, $state, $modalInstance,environmentService) {
                                    $scope.environment = environment;
                                    
                                        $scope.username ="admin";
                                        $scope.password = "admin";
                                  
                                        $scope.setAuth = function(){

                                                $scope.environment.login = {
                                                    username : $scope.username,
                                                    password: $scope.password,
                                                    unauthorised: false
                                                }
                                                environmentService.setLogin($scope.environment.name,$scope.environment.login);
                                                  deferred.resolve();
                                                $modalInstance.close();
                                        }
                                    $scope.cancel = function() {
                                        $modalInstance.dismiss('cancel');
                                            deferred.reject();
                                    };
                                },
                                size: 'sm'
                            });










     /*                 
    var person = prompt("Please enter your name", "Harry Potter");

    if (person!=null) {
         deferred.resolve();
    }else{
            deferred.reject();

    }*/


                              
                         }else{
                             deferred.resolve();
                         } 
           return deferred.promise;

    }
	



    return {
        getEnvironment: _getEnvironment,
        environments:_environments,
        oldPdb : _oldPdb,
        getEnvironments:_getEnvironments,
        setLogin:_setLogin,
        checkAuthorisation:checkAuthorisation,
        logout:logout
    }


});