Cars.controller('rangeCtrl', function ($scope, $rootScope, $filter, $state, modelService, dialogs, rangeService, oldPdbService, authService, $location) {

  var whichEnvironment = $state.params.environment;
  // setup the modelService as an instance
  var primaryModelService = new modelService(whichEnvironment);
  var primaryRangeService = new rangeService(whichEnvironment);


  $scope.Models = primaryModelService.modelsDataObj;
  $scope.ranges = primaryRangeService.getRanges(); //
  $scope.authenticated = authService.authenticated();

  $scope.init = function () {
    primaryModelService.getModels();

    //     if($routeParams.search){
    //      $scope.rangeNameFilter = $routeParams.search;
    //  }
  };
  $scope.init();


  /***********************
    * Range 
    =================== */

  /* Range CRUD stuff */
  $scope.createRange = function () {
    if ($scope.newRangeName) {

      dialogs.wait('Creating range', 'Creating range', 50);


      primaryRangeService.createRange($scope.newRangeName).then(function () {
        $scope.newRangeName = "";

        $rootScope.$broadcast('dialogs.wait.progress', {
          msg: 'Range created. Updating view...',
          'progress': 75
        });
        primaryRangeService.fetchRanges().then(function () {
          $rootScope.$broadcast('dialogs.wait.complete');
        });

      }, function () {

        $rootScope.$broadcast('dialogs.wait.complete');
      });

    }
  };
  var totalRanges, totalRangesPercent, currentRange;

  $scope.saveRanges = function () {
    /* save ranges - API only allows for a range to be saved at a time, so lets que them up! */
    // TODO: move so callback triggers next request instead of timeout
    totalRanges = $scope.ranges.length;
    totalRangesPercent = 100 / totalRanges;
    currentRange = 0;

    $scope.saveRange();

    dialogs.wait('Saving ranges', 'Saving all range data...', 0);


  };


  $scope.sortOrder = function (range, item, direction) {
    console.log('sortOrder from range', range, item, direction)
    sortOrder(range.modelDetails, item, direction);

    primaryRangeService.saveRange(range).then(function () {

      noty({
        text: 'Updated model sort order on ' + range.rangeName,
        type: 'success'
      });
    });
  }


  $scope.saveRange = function () {


    range = $scope.ranges[currentRange];

    if (currentRange < totalRanges) {



      noty({
        text: 'Saving range ' + $scope.ranges[currentRange].rangeName
      });


      $rootScope.$broadcast('dialogs.wait.progress', {
        'progress': (totalRangesPercent * currentRange) + totalRangesPercent
      });
      primaryRangeService.saveRange(range).then(function () {

        noty({
          text: 'Successfully saved range ' + $scope.ranges[currentRange].rangeName,
          type: 'success'
        });

        currentRange++;
        $scope.saveRange();


      }, function () {
        noty({
          text: 'Error saving range ' + $scope.ranges[currentRange].rangeName,
          type: 'error'
        });


        $rootScope.$broadcast('dialogs.wait.complete');
      });
    } else {
      $rootScope.$broadcast('dialogs.wait.complete');
    }

  }

  $scope.deleteRange = function (range) {




    var dlg = dialogs.confirm('Delete range?', 'Delete ' + range.originalRangeName + '? Cannot be un-done', {
      size: 'sm'
    });
    dlg.result.then(function (btn) {

      var index = $scope.ranges.indexOf(range);
      if (index > -1) {
        console.log('scope.ranges', $scope.ranges.length)
        $scope.ranges.splice(index, 1);
        console.log('scope.ranges 2', $scope.ranges.length)

      }

      dialogs.wait('Delete range', 'Deleting range', 0);

      primaryRangeService.deleteRange(range.originalRangeName).then(function () {
        $rootScope.$broadcast('dialogs.wait.complete');
      }, function () {
        noty({
          text: 'Error deleting range.',
          type: 'error'
        });
        $rootScope.$broadcast('dialogs.wait.complete');
      })


    });


  };

  /* link a model to a range */
  $scope.linkModelToRange = function (range, model) {
    // @link : bool
    noty({
      text: 'Linking model ' + model.name + ' to range ' + range.rangeName
    });

    range.modelDetails.push(model); // provides instant view update
    primaryRangeService.linkModelToRange(range.originalRangeName, model.configCode, true).then(
      function () {
        noty({
          text: 'Model ' + model.name + ' linked to range ' + range.rangeName,
          type: 'success'
        });

      },
      function () {
        noty({
          text: 'Error linking model to the range. Please try again.',
          type: 'error'
        });
        range.modelDetails.pop();
      }); //persist the data
  };

  $scope.unlinkModelFromRange = function (range, model) {
    // @link : bool
    noty({
      text: 'Un-linking model ' + model.name + ' from range ' + range.rangeName
    });

    var index = range.modelDetails.indexOf(model);

    if (index > -1) {
      range.modelDetails.splice(index, 1);
    }

    //  range.modelDetails.push(model); // provides instant view update
    primaryRangeService.linkModelToRange(range.originalRangeName, model.configCode, false).then(
      function () {
        noty({
          text: 'Model ' + model.name + ' unlinked from range ' + range.rangeName,
          type: 'success'
        });

      },
      function () {
        noty({
          text: 'Error unlinking model from the range. Please try again.',
          type: 'error'
        });
        // failed so insert it back in
        range.modelDetails.splice(index, 0, model);
      }); //persist the data
  };



  /* range utils 
    ----------------*/
  /* drag 'n' drop sorting for the models in a range 
     TODO : might need rework due to the way a model can be in many ranges 
    $scope.sortModelsInRange = {
        stop:function(){ // when sort has finished, loop through and set a int on the orderNumber (sort order!)
             angular.forEach($scope.ranges,function(range,indexRange){
                angular.forEach(range.modelDetails,function(model,indexModel){
                    model.orderNumber = indexModel;
                });
                range.orderNumber = indexRange;
             });
                 noty({
                    text: 'Range ordered'
                });
        }
    };

*/




  var _removeNewPdbModelsFromOld = function () {
    log('_removeNewPdbModelsFromOld');
    if (environmentService.environment.secondary.name == "oldPDB") {
      angular.forEach($scope.Models, function (newModel, newIndex) {
        log('1')
        angular.forEach($scope.secScrModels, function (oldModel, oldIndex) {
          log('2')
          oldModel.name = oldModel.displayValue;
          if (oldModel.configCode == newModel.configCode) {
            log(oldModel.configCode)
            $scope.secScrModels.splice($scope.secScrModels.indexOf(oldModel), 1);
          }

        });

      });
    }

    $scope.$apply()
  }


  $scope.showJson = function (title, json) {
    console.log(title, json);
    jsonWindow(json);
  }



});