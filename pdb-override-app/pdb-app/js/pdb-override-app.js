'use strict';


// Declare app level module which depends on filters, and services
var Cars = angular.module('Cars', [
                                'ui.router',
                                'ngResource',
                                'ngAnimate',
                                'xeditable',
                                'angular-loading-bar',
                                'ui.sortable',
                                'ui.bootstrap',
                                'pascalprecht.translate',
                                'dialogs.main'
                         ]);
//'ngRoute'

/**
 * Route config
 * handles routes to the views
 */


 /*
Cars.config(['$routeProvider',
    function ($routeProvider) {

         $routeProvider.when('/:environment/models', {
            templateUrl: 'partials/homePage.html'
        });
        $routeProvider.when('/:environment/models/:modelcode', {
            templateUrl: 'partials/modelPage.html'
        });
          $routeProvider.when('/:environment/ranges', {
            templateUrl: 'partials/rangesPage.html'
        });
        $routeProvider.when('/ranges/:search', {
            templateUrl: 'partials/rangesPage.html'
        });
       $routeProvider.otherwise({
            redirectTo: '/Local/home'
        });
    }
]);

*/
Cars.config(function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /state1

  //
  // Now set up the states
  $stateProvider
    .state('environment', {
      url: "/Environment/:environment",

       views: {
          'masterView@': {
            controller:function($state){
              log('state: environment');
              $state.go('environment.tabs.models')
            } 
          }
        }

    })
      .state('environment.tabs', {
        views: {
          'masterView@': {
            templateUrl: "partials/modelsPage.html"
          }
        }

      })
     .state('environment.tabs.models', {
        url: "/Models",
         views: {
          'masterView@': {
              templateUrl: "partials/modelsPage.html",
              controller: "modelsCtrl"
            }
          }
      })
          .state('environment.tabs.models.model', {
            url: "/:model",
              resolve: {
              secondaryEnvironment :  function(){
                    return false;
                  }
            },
             views: {
              'masterView@': {
                  templateUrl: "partials/modelPage.html",
                  controller: "modelCtrl"
                }
               }

          })
           .state('environment.tabs.models.model.environment2', {
            url: "/:environment2",
            views: {
              'masterView@': {
                templateUrl: "partials/modelComparisonPage.html",
                controller: function($scope) {
                  $scope.dualView = true;
                }
              },
              'modelOne@environment.tabs.models.model.environment2': {
                templateUrl: "partials/modelPage.html",
                controller: "modelCtrl"
              },
              'modelTwo@environment.tabs.models.model.environment2': {
                templateUrl: "partials/modelPage.html",
                controller: "modelCtrl",
                resolve: {
                  secondaryEnvironment: function(){
                    return true;
                  }
                },
              }
            }
          })
      .state('environment.tabs.ranges', {
        url: "/Ranges",
         views: {
          'masterView@': {
              templateUrl: "partials/rangesPage.html",
              controller:"rangeCtrl"
            }
          }
      })


     $urlRouterProvider.otherwise("/Environment/UAT");
  });


Cars.config(function ($httpProvider) {
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
});





Cars.run(function (editableOptions) {



    // theme for xeditable plugin
    editableOptions.theme = 'bs3';


    $.noty.defaults.timeout=5000;
    $.noty.defaults.layout='topRight';

});


/* URL for the RESTful service */
var restfulUrl = "http://audipdb.apiary-mock.com/";

/* Service */

/* handles request to the API along with providing some persistance in the view */

Cars.directive('authenticationBox', function() {
    return {
      templateUrl: 'partials/authentication-box.html',
      controller: function($scope,authService){
            $scope.loginMsg = "";
            $scope.login = function(){

                if(!authService.login($scope.username,$scope.password)){
                    $scope.loginMsg = "Login failed. Please try again";
                }
            }

            $scope.authenticated = function(){
                return authService.authenticated();
            }

      }
    };
  });




Cars.directive('sortOrderButtons', function() {
    return {
      templateUrl: 'partials/directives/sortOrderButtons.html',
      replace: true,
       scope: {
            base: "=",
            item:"=",
            indx:"="
        },
      controller: function($scope){

                   $scope.sortOrder = function(baseArray,item,direction){
                        sortOrder(baseArray,item,direction);

                   }
    }
  }
  });

var sortOrder = function(baseArray,item,direction) {
                    console.log('sortOrder',baseArray, item);
                    var old_index = baseArray.indexOf(item);
                    var new_index = old_index;
                    if (direction == 'up' && old_index != 0) {
                        new_index--;
                    }
                    if (direction == 'down' && old_index != baseArray.length - 1) {
                        new_index++;
                    }

                    while (old_index < 0) {
                        old_index += baseArray.length;
                    }
                    while (new_index < 0) {
                        new_index += baseArray.length;
                    }
                    if (new_index >= baseArray.length) {
                        var k = new_index - baseArray.length;
                        while ((k--) + 1) {
                            baseArray.push(undefined);
                        }
                    }
                    baseArray.splice(new_index, 0, baseArray.splice(old_index, 1)[0]);

                    // now set the order back to the 'orderNumber' prop
                    setSortOrderIndex(baseArray);

      };

var setSortOrderIndex = function(baseArray) {
    angular.forEach(baseArray, function(item, idx) {
        item.sortOrder = idx+1;
    });
}


Cars.directive('hideDeleteButtons', function() {
    return {
      templateUrl: 'partials/directives/hideDeleteButtons.html',
      replace: true,
       scope: {
            item: "="
        },
      controller: function($scope){

                     $scope.hideRow = function(row, trueFalse) {

                      console.log('hiderow',row, trueFalse)
                          row.hidden = trueFalse;
                      log(row)
                          noty({
                          text: 'Row hidden'
                      });
                  };

                  /* delete the row */
                  $scope.deleteRow = function(row, trueFalse) {
                      console.log('deleteRow', trueFalse)
                          if (trueFalse) {
                          var c = confirm('Delete? Cannot be un-done once saved.\nUse \'hide\' to temporary hide the row')
                              if (c == true) {
                              row['delete'] = trueFalse;
                              noty({
                                  text: 'Row deleted',
                                  type: 'alert'
                              });
                          }
                      } else {
                          row['delete'] = trueFalse;
                      }
                      log(row['delete'])
                      };
      }
    }
  });







Cars.directive('environmentSelector', function() {
    return {
      templateUrl: 'partials/environmentSelector.html',
      replace: true,
      scope:{
        whichEnvironment: '@whichEnvironment'
      },
      controller: function($scope,$attrs,$modal, authService, modelService,oldPdbService, rangeService, environmentService){
            $scope.environment = environmentService.environment;
            $scope.environments = environmentService.environments;
          // change the environment
            $scope.setEnvironment = function(which,obj){


              environmentService.setEnvironment(which,obj.name)



             /*  var modalInstance = $modal.open({
                    templateUrl: 'partials/authentication-box.html',
                    resolve: {
                      environment: function () {
                        return obj;
                      }
                    },
                    controller: function ($scope, $modalInstance,environment) {
                      $scope.username="";
                      $scope.password="";
                      $scope.environment = environment;
                      $scope.ok = function () {

                         $modalInstance.close(authService.login($scope.username,scope.password));
                      };

                      $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                      };

                    },
                    size: 'sm'
                  });
*/
/*
                modalInstance.result.then(function (selectedItem) {
                  $scope.selected = selectedItem;
                }, function () {
                  console.log('Modal dismissed at: ' + new Date());
                });
*/



               console.log('$attrs.runFunction',$attrs.runFunction)
             eval($attrs.runFunction);

            }

      }
    };
  });





/* Controller */




Cars.filter("orderNames", function () {
    return function (input, sortBy) {
        var ordered = [];
        for (var key in sortBy) {
            ordered.push(input[sortBy[key]]);
        }

        return ordered;
    };
});

Cars.filter('partition', function($cacheFactory) {
  var arrayCache = $cacheFactory('partition')
  return function(arr, size) {
   try{

    var parts = [], cachedParts,
      jsonArr = JSON.stringify(arr);
    for (var i=0; i < arr.length; i += size) {
        parts.push(arr.slice(i, i + size));
    }
    cachedParts = arrayCache.get(jsonArr);
    if (JSON.stringify(cachedParts) === JSON.stringify(parts)) {
      return cachedParts;
    }
    arrayCache.put(jsonArr, parts);

}catch(e){
  parts = arr;
}
    return parts;

  };
});




Cars.directive('fallbackSrc', function () {
    var fallbackSrc = {
        link: function postLink(scope, iElement, iAttrs) {
            iElement.bind('error', function() {
                angular.element(this).attr("src", iAttrs.fallbackSrc);
            });
        }
    }
    return fallbackSrc;
});

/**
 * Move items inside an array
 *  Moves item from old postion to the new postion
 *
 * ArrayVar..move(3,5);
 */
Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this; // for testing purposes
};


function log(text) {
    try {
        console.log(text);
    } catch (e) {}
}



var guid = (function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return function () {
        return "_" + s4() + s4() + s4() + s4();
    };
})();










function syntaxHighlight(json) {
    if (typeof json != 'string') {
         json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}


function jsonWindow(json){

     var jsonHTML = syntaxHighlight(JSON.stringify(json,undefined,4));
      var w = window.open();
      var html = "<head><style>pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; } .string { color: green; } "; html+= " .number { color: darkorange; } .boolean { color: blue; } .null { color: magenta; } .key { color: red; }</style></head><body>";
      html+= "<pre>"+jsonHTML+"</pre>";
      w.document.writeln(html);


}



