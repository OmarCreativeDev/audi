/* oldPdbService */

Cars.factory('oldPdbService', function ($resource, $http, $q, modelService,environmentService) {


    var _modelsDataObj = [];

    var _modelDataObj = {};

    var _Models = function () {
        if (typeof _modelsDataObj[0] === "undefined") {
            _getModels();
        }
        return _modelsDataObj;
    }
    // models are like ranges!
    var _getModels = function (modelId) {
        log('fetch')
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url:environmentService.oldPdb.urls.models
        })
            .success(function (result, status) {
                console.log('oldPDB _getModels', result)
                angular.copy(result, _modelsDataObj);

                deferred.resolve();
            }).
        error(function (data, status) {
           // alert('Failed to load model info from '+environmentService.oldPdb.name);
           angular.copy([], _modelsDataObj);
            deferred.reject();
        });

        return deferred.promise;
    }


    var _Model = function () {
        if (typeof _modelDataObj[0] === "undefined") {
            _getModel();
        }
        return _modelDataObj;
    }


    var _reset = function(){
        angular.copy([],_modelsDataObj);
        _getModels();
    }

    var _getModel = function (modelId) {
        log('fetch')
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: environmentService.oldPdb.urls.model + modelId
        })
            .success(function (result, status) {
                console.log('model', result);
                var Model = result;
                console.log('OLDModel loaded', Model);
                // copy onto the object
               angular.copy(Model, _modelDataObj);
               // angular.copy(Model, modelService.modelDataObj);        
                deferred.resolve();
            }).
        error(function (data, status) {
            deferred.reject();
        });

        return deferred.promise;

    }












    return {
        Models: _Models,
        modelDataObj: _modelDataObj,
        modelsDataObj:_modelsDataObj,
        getModel: _getModel,
        getModels:_getModels,
        reset:_reset
    };



});
