
Cars.factory('modelService', function ($resource, $http, $q, $location, authService, environmentService) {

    return function(baseEnvironment){
       var environment =  environmentService.getEnvironment(baseEnvironment);


       console.log('environment',environment)
                    var _modelDataObj = {};  // main storage for the model data
                    var _modelDataObjOrginal = {}; //unsaved
                    var _modelsDataObj = [];
                    var _modelLoaded = false;

                    var _hasModelChanged = function(){

                        if(angular.equals(_modelDataObj,_modelDataObjOrginal)){
                            return false;
                        }else{
                            return true;
                        }


                    }

                    /* the trim status options 
                    - could be moved to an API call */
                    var _trimsStatus = [{
                            availability: 0,
                            text: 'Not available',
                            img: 'not_available',
                            icon:'remove'
                        }, 
                         {
                            availability: 1,
                            text: 'Standard',
                            img: 'standard',
                            icon:'ok'
                        },
                        {
                            availability: 2,
                            text: 'Available',
                            img: 'available',
                            icon:'plus'
                        }
                    ];



                    /* roll back the changes */
                    var _reset = function(){
                        angular.copy(_modelDataObjOrginal,_modelDataObj);
                    }

                    /* **********************
                    * Singe car model CRUD
                    =================== */
                    
                    var _getModel = function (modelId) {


                        console.log('fetch',environment.url + "/models/" + modelId)
                        var deferred = $q.defer();
                            $http({
                                method: 'GET',
                                url: environment.url +"/models/"+ modelId
                            })
                            .success(function (result, status) {
                                console.log('Model loaded', result);
                                // copy onto the object
                                angular.copy(result, _modelDataObj);
                                angular.copy(result, _modelDataObjOrginal);
                                
                                deferred.resolve();
                            }).
                        error(function (data, status) {
                            deferred.reject();
                        });

                        return deferred.promise;

                    }


                    var _saveModel = function () {
                 
                      var deferred = $q.defer();

                        // login check wrapper for this service.
                    environmentService.checkAuthorisation(environment).then(function(){

                        // Set sortOrder for all nodes
                        setSortOrderIndex(_modelDataObj.trimLines);
                        setSortOrderIndex(_modelDataObj.groups);
                        angular.forEach(_modelDataObj.groups, function(group) {
                            setSortOrderIndex(group.trimOptions);
                        });

                         console.log('_saveModel: '+_modelDataObj.configCode,_modelDataObj)
                                $.ajax({
                                type: 'POST',
                                cache: false,
                                url: environment.url + "/models/" + _modelDataObj.configCode,
                                data: "username="+ environment.login.username  + "&password=" +environment.login.password + "&jsonData=" + encodeURIComponent(angular.toJson(_modelDataObj))
                            })
                            .success(function (result, status) {
                                angular.copy(_modelDataObj,_modelDataObjOrginal);
                                deferred.resolve();
                            }).
                            error(function (data, status) {

                                // alert('Error saving data. Please try again.');
                                deferred.reject('Unable to save data to server.');
                            });
    
                    },function(){
                        //login failed so the whole thing failed.
                         deferred.reject('Login failed');
                         environmentService.setLogin(environment.name,{});
                    });

           
                    return deferred.promise;


                    }


                    var _deleteModel = function(model){
                        // @model = [object]
                            var deferred = $q.defer();

                        // login check wrapper for this service.
                    environmentService.checkAuthorisation(environment).then(function(){

                        // $.ajax used instead of $http or $q for x-domain issus 
                            $.ajax({
                                type: 'POST',
                                cache: false,
                                url: environment.url +"/models/" + model.configCode + "/delete",
                                data: "username="+ environment.login.username + "&password=" + environment.login.password
                            })
                            .success(function (result, status) {
                                     console.log('model deleted',result);
                                      _getModels();
                                       deferred.resolve();
                            }).
                        error(function (data, status) {
                             alert('Error saving data. Please try again.');
                            deferred.reject();
                        });

                         },function(){
                        //login failed so the whole thing failed.
                         deferred.reject('Login failed');
                         environmentService.setLogin(environment.name,{});
                    });

           
                    return deferred.promise;
                    }



                /* **********************
                    * All models 
                    =================== */

                    var _getModels = function (modelId) {
                        log('fetch')
                        var deferred = $q.defer();
                        _modelLoaded = false
                      
                        console.log('_modelsDataObj.length:',_modelsDataObj.length)
                        if(_modelsDataObj.length > 0){
                                deferred.resolve();
                        }else{

                        $http({
                            method: 'GET',
                            url: environment.url+"/models/"
                        })
                            .success(function (result, status) {
                                console.log('models result',result)
                                angular.copy(result, _modelsDataObj);
                                _modelLoaded = true;
                                deferred.resolve();
                            }).
                        error(function (data, status) {
                         //   alert('Failed to load model from old PDB');
                            angular.copy({}, _modelsDataObj);
                            deferred.reject();
                        });
                            }
                        return deferred.promise;
                    };

                /* **********************
                    * Trim items
                    =================== */
                    
                    /* new trim item object */
                    var _trimItem = function () {
                        var returnOb = {
                            "displayName": "[New]",
                            "description": "[test]",
                            "trimLines": [],
                            "isNew": true
                        };

                        // add in the trims options with the availabiltiy to 0
                        angular.forEach(_modelDataObj.trimLines, function (item) {
                            returnOb.trimLines.push({
                                trimLine: item.trimLine,
                                availability: 0
                            });
                        });
                    }







    var deepCompareModel_OLD_DONT_USE = function(a,b){
        //a = current
        //b = old pdb
        console.info(a,b);
        var c = {};

        if (a.configCode != b.configCode) {
            //alert('Model codes do not match');
               noty({text: 'Model codes do not match',type:'error'});
            return false;
        }

        if (a.originalName != b.name) c.name = b.name;

        c.groups = [];
        angular.forEach(a.groups, function (a_group) {
            var tempGroup = {
                originalTitle: a_group.originalTitle
            };
            angular.forEach(b.groups, function (b_group) {
                // find the match
                if (a_group.originalTitle == b_group.originalTitle) {

                    if (a_group.title != b_group.title) { tempGroup.title = b_group.title; }
                        
                        tempGroup.trimOptions = [];
                         var tempGrouptrimOptions = {};
                        angular.forEach(a_group.trimOptions,function(a_trimOptions){
                            angular.forEach(b_group.trimOptions,function(b_trimOptions){

                             if (b_trimOptions.displayName == a_trimOptions.displayName) {

                               if (a_trimOptions.displayName != b_trimOptions.displayName) { tempGrouptrimOptions.displayName = b_trimOptions.displayName; }
                               if (a_trimOptions.emissionsCo2 != b_trimOptions.emissionsCo2) { tempGrouptrimOptions.emissionsCo2 = b_trimOptions.emissionsCo2; }
                               if (a_trimOptions.fuelConsumptionCombined != b_trimOptions.fuelConsumptionCombined) { tempGrouptrimOptions.fuelConsumptionCombined = b_trimOptions.fuelConsumptionCombined; }

                               tempGrouptrimOptions.trimLineOptions =[];
                                var tempTrimLineOptions = {};
                                     angular.forEach(a_trimOptions.trimLineOptions,function(a_trimLineOptions){
                                         angular.forEach(b_trimOptions.trimLineOptions,function(b_trimLineOptions){
                                            if (b_trimOptions.originalTrimLineName == a_trimOptions.originalTrimLineName) {

                                                if (b_trimLineOptions.originalTrimLineName != a_trimLineOptions.originalTrimLineName) { tempTrimLineOptions.originalTrimLineName = b_trimLineOptions.originalTrimLineName; }
                                                if (b_trimLineOptions.availability != a_trimLineOptions.availability) { tempTrimLineOptions.availability = b_trimLineOptions.availability; }
                                                
                                            } // if b_trimOptions.displayName == a_trimOptions.displayNam
                                        });//b_trimLineOptions forEach
                                     });//a_trimLineOptions forEach
                                     tempGrouptrimOptions.trimLineOptions.push(tempTrimLineOptions);
                                                tempTrimLineOptions={};

                                 }// if b_trimOptions.displayName = a_trimOptions.displayName

                             });//b_trimOptions forEach
                        });//a_trimOptions forEach

                    tempGroup.trimOptions.push(tempGrouptrimOptions);
                }// if (a_group.originalTitle == b_group.originalTitle) 

            }); //forEach b.groups
            c.groups.push(tempGroup);
        }); //forEach a.groups

      //  console.log('c', c);
        console.log('c', c);
        jsonWindow(c);
    }







   var deepCompareData = [];

    var cP = function(key,id,data){
        deepCompareData.push({id:id+'_'+key, data:data});
    }


    var deepCompareModel = function(a,b){
        //a = current
        //b = old pdb
        console.info(a,b);

        if (a.configCode != b.configCode) {
            //alert('Model codes do not match');
               noty({text: 'Model codes do not match',type:'error'});
            return false;
        }

       // if (a.originalName != b.name) c.name = b.name;
        
        angular.forEach(a.groups, function (a_group) {

            angular.forEach(b.groups, function (b_group) {
                // find the match
                if (a_group.originalTitle == b_group.originalTitle) {
                    a_group.matched = true;
                    if (a_group.title != b_group.title) {  cP('group',a_group.id,b_group.title); }
                      

                        angular.forEach(a_group.trimOptions,function(a_trimOptions){
                            angular.forEach(b_group.trimOptions,function(b_trimOptions){

                             if (b_trimOptions.displayName == a_trimOptions.displayName) {
                             a_trimOptions.matched = true;

                               if (a_trimOptions.displayName != b_trimOptions.displayName) { cP('displayName',a_trimOptions.id,b_trimOptions.displayName); }
                               if (a_trimOptions.emissionsCo2 != b_trimOptions.emissionsCo2) { cP('emissionsCo2',a_trimOptions.id,b_trimOptions.emissionsCo2); }
                               if (a_trimOptions.fuelConsumptionCombined != b_trimOptions.fuelConsumptionCombined) { cP('fuelConsumptionCombined',a_trimOptions.id,b_trimOptions.fuelConsumptionCombined); }
                              
                              angular.forEach(a_trimOptions.trimLineOptions,function(a_trimLineOptions){
                                         angular.forEach(b_trimOptions.trimLineOptions,function(b_trimLineOptions){
                                            if (a_trimLineOptions.originalTrimLineName == b_trimLineOptions.originalTrimLineName) {
                                               a_trimLineOptions.matched = true;
                                                console.log('match - ',a_trimLineOptions.availability , b_trimLineOptions.availability)
                                                if (b_trimLineOptions.availability != a_trimLineOptions.availability) {  cP('availability',a_trimLineOptions.id,b_trimLineOptions.availability); }
                                                
                                            } // if b_trimOptions.displayName == a_trimOptions.displayNam
                                        });//b_trimLineOptions forEach
                                     });//a_trimLineOptions forEach
       

                                 }// if b_trimOptions.displayName = a_trimOptions.displayName

                             });//b_trimOptions forEach
                        });//a_trimOptions forEach

                }// if (a_group.originalTitle == b_group.originalTitle) 

            }); //forEach b.groups
        
        }); //forEach a.groups

      //  console.log('c', c);
        console.log('deepCompareData', deepCompareData);
        jsonWindow(deepCompareData);
        jsonWindow(a.groups);
    }









                    return {
                        modelDataObj: _modelDataObj,
                        getModel: _getModel,
                        modelsDataObj: _modelsDataObj,
                        getModels: _getModels, //_local
                        trimsStatus: _trimsStatus,
                        trimItem: _trimItem,
                        saveModel: _saveModel,
                        reset:_reset,
                        deleteModel:_deleteModel,
                        environment:environment,
                        deepCompareModel:deepCompareModel,
                        hasModelChanged: _hasModelChanged
                    };

            }

});