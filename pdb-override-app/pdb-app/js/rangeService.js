
Cars.factory('rangeService', function ($resource, $http, $q, $location, environmentService) {
    return function(baseEnvironment){
       var environment =  environmentService.getEnvironment(baseEnvironment);
    var _ranges = [];



    var _getRanges = function () {
        if (typeof _ranges.ranges === "undefined") {
            _fetchRanges();
        }
        return _ranges;
    }


    var _fetchRanges = function () {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: environment.url + "/ranges/"
        })
            .success(function (result, status) {
                _ranges = angular.copy(result, _ranges);
                deferred.resolve();
            }).
        error(function (data, status) {
            deferred.reject();
        });

        return deferred.promise;
    };



    var _createRange = function(RangeName){

                   var deferred = $q.defer();


    environmentService.checkAuthorisation(environment).then(function(){


     var dataObj  = {  
            originalRangeName: RangeName,     
            rangeName: RangeName 
          };
                $.ajax({
                        type: 'POST',
                        cache: false,
                        url: environment.url + "/ranges/" + RangeName,
                        data: "username="+ environment.login.username  + "&password=" +environment.login.password+"&jsonData=" + encodeURIComponent(angular.toJson(dataObj))
                    })
                        .success(function (result, status) {
             
                                       deferred.resolve();
                
                        }).
                    error(function (data, status) {
                         deferred.reject('Unable to save data to server.');

                    });

          },function(){
            //login failed so the whole thing failed.
             deferred.reject('Login failed');
             environmentService.setLogin(environment.name,{});
        });
    
        return deferred.promise;

    }


        var _saveRange = function(range){
                   var deferred = $q.defer();


    environmentService.checkAuthorisation(environment).then(function(){

                $.ajax({
                        type: 'POST',
                        cache: false,
                        url: environment.url + "/ranges/"+ range.originalRangeName,
                        data: "username=admin&password=admin&jsonData=" + encodeURIComponent(angular.toJson(range))
                    })
                        .success(function (result, status) {
                           // _fetchRanges();
                            deferred.resolve();
                        }).
                    error(function (data, status) {
                           deferred.reject('Unable to save data to server.');

                    });
             },function(){
            //login failed so the whole thing failed.
             deferred.reject('Login failed');
             environmentService.setLogin(environment.name,{});
        });
    
        return deferred.promise;


    }

    var _deleteRange = function(RangeName){
                         var deferred = $q.defer();


    environmentService.checkAuthorisation(environment).then(function(){

     var dataObj  = {  
            originalRangeName: RangeName,     
            rangeName: RangeName 
          };
                    $.ajax({
                            type: 'POST',
                            cache: false,
                            url: environment.url + "/ranges/" + RangeName + "/delete",
                            data: "username=admin&password=admin&jsonData=" + encodeURIComponent(angular.toJson(dataObj))
                        })
                            .success(function (result, status) {
                                _fetchRanges();
                                  deferred.resolve();
                            }).
                        error(function (data, status) {
                                     deferred.reject('Unable to save data to server.');

                        });
                },function(){
            //login failed so the whole thing failed.
             deferred.reject('Login failed');
             environmentService.setLogin(environment.name,{});
        });
    
        return deferred.promise;

    }

    var _reset = function(){
                angular.copy([],_ranges);
                _getRanges();
    }


    var _linkModelToRange = function(range,model,link){
        var isDelete = "";
         if(!link){
                isDelete = "/delete"
        }

         var deferred = $q.defer();


         console.log('environment',environment)

                        // login check wrapper for this service.
                    environmentService.checkAuthorisation(environment).then(function(){

                    
                            $.ajax({
                                type: 'POST',
                                cache: false,
                                url: environment.url + "/ranges/" + range + "/"+model + isDelete,
                                data: "username="+ environment.login.username  + "&password=" +environment.login.password
                            })
                            .success(function (result, status) {
                                angular.copy(_modelDataObj,_modelDataObjOrginal);
                                deferred.resolve();
                            }).
                            error(function (data, status) {

                                // alert('Error saving data. Please try again.');
                                deferred.reject('Unable to save data to server');
                            });
    
                    },function(){
                        //login failed so the whole thing failed.
                         deferred.reject('Login failed');
                         environmentService.setLogin(environment.name,{});
                    });

           
                    return deferred.promise;





    }


    return {
        getRanges: _getRanges,
        createRange: _createRange,
        linkModelToRange: _linkModelToRange,
        deleteRange:_deleteRange,
        saveRange:_saveRange,
        reset: _reset,
        fetchRanges: _fetchRanges

    }
}

});
