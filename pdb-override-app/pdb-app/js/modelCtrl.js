Cars.controller('modelCtrl', function($scope,$rootScope, $timeout, secondaryEnvironment, $filter, $modal, modelService, oldPdbService, authService, environmentService, $location, $state, $translate, dialogs) {

    // find out which environment  this controler is editing
    var currentEnvironment = $state.params.environment; // store as a global
    $scope.environment = currentEnvironment;

    // is this the second instance on the page - compare view
    if (secondaryEnvironment) {
        $scope.environment= $state.params.environment2;
        $scope.parentS = $scope.$parent;
        $scope.Model2 = $scope.$parent.Model;
        $scope.isSecondaryModel = secondaryEnvironment;
    }

    // setup the modelService as an instance
    var ModelService = new modelService(currentEnvironment);

    $scope.init = function() {
        $scope.Model.configCode = $state.params.model;
        $scope.stateParams = $state.params;

        if ($scope.Model.configCode) {
            // load in the model data
            dialogs.wait('Loading model data','Loading model data...',50);
            ModelService.getModel($scope.Model.configCode).then(function() {
                noty({
                    text: 'Model ' + $scope.Model.configCode + ' loaded'
                });

               _checkTrims();
               $timeout(function(){
                     $('.trimMatix').floatThead()
               },2000);
                        $rootScope.$broadcast('dialogs.wait.complete');
            },function(){
                    $rootScope.$broadcast('dialogs.wait.complete');
                  noty({
                    text: 'Failed to load model data.'
                });

            });
        }

    };


    $scope.Model = ModelService.modelDataObj;
    $scope.hasModelChanged = ModelService.hasModelChanged;
   // $rootScope.hasModelChanged = ModelService.hasModelChanged; // set to global for nav to pick up changes
    $scope.trimStatus = ModelService.trimsStatus; // hold the trim line status types (NOT the trim lines for the model!)


    /* **********************
    * Model (car)
    =================== */

    $scope.copyModel = function() {

        if ($scope.hasModelChanged()) {
            var dlg = dialogs.notify('Unsaved changes', 'Please save your changes before copying the data', {
                size: 'sm'
            });
        } else {
               $scope.Model.configCode = "";
                $scope.Model.id = null;
                $scope.Model.name = "[NEW]";
                $scope.Model.originalName = "[NEW]";
        }
    }

    $scope.deleteModel = function(model) {
        var dlg = dialogs.confirm('Delete?', 'Do you wish to delete this model? This can not be un-done. <b>CQ page linked to this configCode will break!</b>', {
            size: 'sm'
        });
        dlg.result.then(function(btn) {
            ModelService.deleteModel(model).then(function(){
                     $state.go('environment.tabs.models');
            })
        });
    }
    $scope.saveModel = function() {
        noty({
            text: 'Saving model...'
        });
        $scope.savingModel = true;

        dialogs.wait('Saving model','Saving model data...',50);


        ModelService.saveModel().then(function() {
            noty({
                text: 'Model saved!',
                type: 'success'
            });
               $rootScope.$broadcast('dialogs.wait.complete');
            $scope.savingModel = false;
        }, function(errorMsg) {
            noty({
                text: 'Failed to save model. ('+errorMsg+')',
                type: 'error'
            });
            $timeout(function(){
                 $rootScope.$broadcast('dialogs.wait.complete');
             },1000);

            $scope.savingModel = false;
        });;
    }

    $scope.cancelModel = function() {
          dialogs.confirm('Cancel changes', 'Do you wish to cancel your changes?', {
                size: 'sm'
            }).result.then(function(btn) {
               ModelService.reset();
            });
    }

    $scope.isModelLoaded = function() {
        return typeof $scope.Model.name === "string" ? true: false;
    }

    /** push data to another enviroment */
    $scope.modalDataPush = function() {
        log('modalDataPush');
        var modalInstance = $modal.open({
            templateUrl: 'partials/select-environment.html',
            controller: function($scope, $state, $modalInstance, environmentService) {
                $scope.environments = environmentService.environments;
                $scope.parentEnvironment = $state.params.environment;
                $scope.environment={};
                    $scope.username ="admin";
                    $scope.password = "admin";

                $scope.setEnvironment = function(environmentName) {




                     dialogs.wait('Pushing data','Pusing model data to '+environmentName + ".",50);

                    var pushModelService = new modelService(environmentName);
                    angular.copy(ModelService.modelDataObj, pushModelService.modelDataObj);

                    pushModelService.saveModel(pushModelService.modelDataObj.configCode).then(function() {
                                    $rootScope.$broadcast('dialogs.wait.progress',{ msg:'Data saved!', 'progress' : 100});
                    }, function(reason) {
                        $rootScope.$broadcast('dialogs.wait.progress',{ msg:'Error saving data. Please try again. ('+reason+')', 'progress' : 0});

                    });


                       $modalInstance.close();
                };




                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                };
            },
            size: 'sm'
        });

    };

    /** pull data from another environment */
    $scope.modalDataPull = function() {
        log('modalDataPull');

        var modalInstance = $modal.open({
            templateUrl: 'partials/select-environment.html',
            controller: function($scope, $state, $modalInstance, environmentService) {
                $scope.environments = environmentService.environments;
                $scope.parentEnvironment = $state.params.environment;

                $scope.setEnvironment = function(environmentName) {

                    var pullModelService = new modelService(environmentName);

                    //  $scope.ModelSync = pullModelService.modelDataObj;
                    $scope.loadingMsg = "Fetching data for " + ModelService.modelDataObj.configCode + " from " + environmentName + "...";
                    pullModelService.getModel(ModelService.modelDataObj.configCode).then(function() {
                        angular.copy(pullModelService.modelDataObj, ModelService.modelDataObj);
                        $scope.loadingMsg = "Data fetched!";
                        noty({
                            text: 'Data fetched!'
                        });
                        $timeout(function() {
                            $modalInstance.close();
                        }, 900);

                    });

                };
                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                };
            },
            size: 'sm'
        });
    }

    $scope.modalDataPullFromOldPdb = function() {
        log('modalDataPullFromOldPdb');

        var modalInstance = $modal.open({
            templateUrl: 'partials/pdb-override-modal.html',
            controller: function($scope, $state, $modalInstance, environmentService) {

                $scope.okReplace = function() {
                  /*
                    oldPdbService.getModel(ModelService.modelDataObj.configCode).then(function() {
                            oldPdbService.modelDataObj.id= ModelService.modelDataObj.id;
                        angular.copy(oldPdbService.modelDataObj, ModelService.modelDataObj);
                        noty({
                            text: 'Data fetched from old PDB!'
                        });

                        $modalInstance.close();

                    })
                    */
                    };

                $scope.okCompare = function() {
                    $scope.oldPdbModel = oldPdbService.modelDataObj;
                    oldPdbService.getModel().then(function() {

                        noty({
                            text: 'Data fetched from old PDB!',
                            type: 'success'
                        });
                        noty({
                            text: 'Comparing data...'
                        });
                        ModelService.deepCompareModel(ModelService.modelDataObj, oldPdbService.modelDataObj);

                        $modalInstance.close();

                    })

                    };

                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                };
            },
            size: 'm'
        });

    }

    /* **********************
    * Compare primary and secondary data
    =================== */
    // TODO : all of it!
    /* COMPARE OBJECTS */
    $scope.compareObject = {};

    $scope.compareModel = function() {
        /*   oldPdbService.getModel($scope.Model.configCode);
        setTimeout(function(){
             $scope.compareModelRun($scope.Model,$scope.secSrcModel);
        },3000,$scope);
*/


        //  secondaryModelService.getModel($scope.Model.configCode);
        console.log('$scope.stateParams.environment2', $scope.stateParams.environment2)
            if (typeof $scope.stateParams.environment2 === "string") {
            //hide
            $state.go('environment.tabs.models.model', {}, {
                reload: true
            });
        } else {
            //show

            var modalInstance = $modal.open({
                templateUrl: 'partials/select-environment.html',

                controller: function($scope, $modalInstance, environmentService) {

                    $scope.parentEnvironment  = currentEnvironment;
                    $scope.environments = environmentService.environments;
                    $scope.setEnvironment = function(environmentName) {
                        console.log('compare ',environmentName)
                        $state.go('environment.tabs.models.model.environment2', {
                            environment2: environmentName
                        }, {
                            reload: true
                        });
                        $modalInstance.close();
                    };
                    $scope.cancel = function() {
                        $modalInstance.dismiss('cancel');
                    };
                },
                size: 'sm'
            });
        }
    }

    /* **********************
    * Trim lines
    =================== */


    var _checkTrims = function(){
        log('_checkTrims')
        // as data from the old pdb etc is not very clean we just need to run some checks on it to clean it up.
        angular.forEach($scope.Model.trimLines, function(trimLineOptionOrginal) {
            // fix null values
                if(trimLineOptionOrginal.originalTrimLineName==null){
                     trimLineOptionOrginal.originalTrimLineName = guid();
                }

          });




        // logic that *should* be done on the backend.
        angular.forEach($scope.Model.groups, function(group) {
            angular.forEach(group.trimOptions, function(trimOption) {

                    // loop through all aviable trim lines
                angular.forEach($scope.Model.trimLines, function(trimLineOptionOrginal) {

                    var selected = $filter('filter')(trimOption.trimLineOptions, {
                             originalTrimLineName: trimLineOptionOrginal.originalTrimLineName
                            }, false);

                     if(selected.length == 0) {
                        log('add trim)');
                         trimOption.trimLineOptions.push({
                                availability: 2,
                                originalTrimLineName: trimLineOptionOrginal.originalTrimLineName
                         });

                     }


                })
                    //check to see if the two array match in length

                    // if the group items have more items than the base
                    if($scope.Model.trimLines.length < trimOption.trimLineOptions.length){

                        angular.forEach(trimOption.trimLineOptions, function(trimLineOption) {

                            var selected = $filter('filter')($scope.Model.trimLines, {
                                 originalTrimLineName: trimLineOption.originalTrimLineName
                                }, false);

                                if(selected.length == 0) {
                                    log('remove trim)');
                                     trimLineOption  = {};
                                }
                             });
                    }


            })
        })





    angular.forEach($scope.Model.trimLines, function(trimLineOptionOrginal) {
        if(trimLineOptionOrginal.originalTrimLineName.charAt(0) != "_"){
            var currentOriginalTrimLineName = trimLineOptionOrginal.originalTrimLineName;
            var newGuid = guid();
        trimLineOptionOrginal.originalTrimLineName = newGuid;
        log('currentOriginalTrimLineName:'+currentOriginalTrimLineName)
        angular.forEach($scope.Model.groups, function(group) {
            angular.forEach(group.trimOptions, function(trimOption) {
                angular.forEach(trimOption.trimLineOptions, function(trimLineOption) {

                    if(trimLineOption.originalTrimLineName==currentOriginalTrimLineName){
                        trimLineOption.originalTrimLineName = newGuid;
                    }
                      })
            })
        })
        }
    });


    }

    /* Add a new trim line */
    $scope.addTrimLine = function() {
        var trimGuid = guid();
        $scope.Model.trimLines.push({
            trimLineName: "",
            otrPriceMin: "0.00",
            originalTrimLineName: trimGuid
        });

        //inject a new object into the current trim options
        angular.forEach($scope.Model.groups, function(group) {
            angular.forEach(group.trimOptions, function(trimOption) {
                log('trimOption');
                log(trimOption)
                trimOption.trimLineOptions.push({
                    availability: 2,
                    originalTrimLineName: trimGuid
                });
            });
        });

     //   

    };

    /* remove / hide trim options */
    $scope.mangeTrimLineVisability = function(trimLine, hideOrDelete, trueFalse) {
        if (hideOrDelete == 'delete' && trueFalse) {
            var c = confirm('Delete trim? Cannot be un-done.\nUse \'hide\' to temporary hide the row')
            }
        if (c == false)
            return;
        var currentTrimLine = trimLine.originalTrimLineName;
        // change to use ID
        /* delete the trim line from all trim options */
        angular.forEach($scope.Model.groups, function(group) {
            angular.forEach(group.trimOptions, function(trimOption) {
                angular.forEach(trimOption.trimLineOptions, function(trimLineOption, index) {
                    if (trimLineOption.originalTrimLineName == currentTrimLine) {
                        trimLineOption[hideOrDelete] = trueFalse;
                    }
                });
            });
        });

        trimLine[hideOrDelete] = trueFalse
    }

    /* **********************
    * Trim lines options grid
    =================== */

    // show/hide the sub children of a group on the page
    $scope.toggleTrimOptionsChildren = function(group) {
        if (typeof group.showChildren === "undefined") {
            group.showChildren = false;
        }
        if (group.showChildren) {
            group.showChildren = false;
        } else {
            group.showChildren = true;
        }
    }

    // add a new top group to the trim options
    $scope.addTrimGroup = function() {
        $scope.Model.groups.push({
            title: "[New title]",
            originalTitle: guid(),
            trimOptions: [],
            isNew: true
        });
    };

    // add a child item to the trim line group
    $scope.addTrimOption = function(group) {
        var returnOb = {
            "displayName": "[New]",
            "description": "[test]",
            "trimLineOptions": [],
            "isNew": true
        };

        // add in the trims options with the availabiltiy to 0
        angular.forEach($scope.Model.trimLines, function(item) {
            //add in the trimlines set to NOT available (=2)
            returnOb.trimLineOptions.push({
                originalTrimLineName: item.originalTrimLineName,
                availability: 2
            });
        });
        // push it to the object to render
        group.push(returnOb);
    };

    // hide trim row


    /* **********************
    * Utils
    =================== */

    $scope.modelLoadingInterval;

    // check to see if the config code has already been used by another model
    $scope.checkConfigCode = function(data) {
        console.log('checkConfigCode', data);

    }

    /*
     ***   TRIM AVAILABILITY
     */




    $scope.showTrimStatus = function(originalTrimLineName_Guid, trimLineOptions, what) {
     // originalTrimLineName_Guid : GUID TO FIND
    // trimLineOptions:  the options to pick it from
     var returnItem="";
            // first get the right once from the object;
          var selected =   $filter('filter')(trimLineOptions, {
                originalTrimLineName:originalTrimLineName_Guid
            }, true);

          var availability  = selected[0].availability;
                angular.forEach($scope.trimStatus,function(item){
                if(item.availability== availability){
                    returnItem =  item[what];
                }
            });

        return returnItem
    }

    $scope.changeTrimStatus = function(originalTrimLineName_Guid, trimLineOptions) {
       console.log(originalTrimLineName_Guid);
            angular.forEach(trimLineOptions,function(item,index){
                if(item.originalTrimLineName == originalTrimLineName_Guid){
                   var currentAvilability = item.availability;
                   currentAvilability++;
                       if(currentAvilability>2){
                        currentAvilability = 0;
                       }
                    item.availability=currentAvilability;
                }
            });
    }


    $scope.isDataDiffClass = function(a, b, what) {
        try {
            if (a[what] != b[what]) {
                return 'glyphicon glyphicon-arrow-left';
            } else {
                return '';
            }
        } catch(e) {}
    }

    /* Model tab utils */
    // remove models that are in the primary
    // system from the list of models in the old PDB
    $scope.showJson = function(title, json) {
        console.log(title, json);
        jsonWindow(json);
    }

    /* $scope.sortTrimGroupOptions = {
            update: function(e, ui) {
                log('sortTrimGroupOptions');
            },
            axis: 'y'
        };
*/


    $scope.sortTrimGroupOptions = {
         cancel:".disabledSort",
         
        stop: function() {
           
            // when sort has finished, loop through and set a int on the sortOrder (sort order!)
            angular.forEach($scope.Model.groups, function(group, indexgroup) {
                angular.forEach(group.trimOptions, function(trimOption, indexTrimOption) {
               log(indexTrimOption+1);
                trimOption.sortOrder = indexTrimOption+1;
               });
            });
           // angular.copy(TheGroup,$scope.Model.groups);

            noty({
                text: 'Trimlines ordered'
            });
        }
    };

    $scope.backToModels = function() {
    // stop the user leaving the page if there are unsaved changes */
        if ($scope.hasModelChanged()) {
            var dlg = dialogs.confirm('Unsaved changes', 'Do you wish to leave this page without saving your changes?', {
                size: 'sm'
            });
            dlg.result.then(function(btn) {
                $state.go('environment.tabs.models');
            });

        } else {
            $state.go('environment.tabs.models');
        }
    }


    $scope.info = function(header,body){

        dialogs.notify(header, body,{});
    }



    $scope.init();

});